module.exports = function (grunt) {
  grunt.initConfig({
    clean: {
      prod: ['services-dist']
    },
    babel: {
      prod: {
        files: [
          {
            expand: true,
            cwd: 'services',
            src: ['**/*.js', '!front/static/demo/**/*.js'],
            dest: 'services-dist'
          }
        ],
        options: {
          sourceMap: false,
          presets: [
            [
              '@babel/preset-env',
              {
                targets: {
                  node: '8.11.1'
                }
              }
            ],
            '@babel/preset-react'
          ],
          plugins: [
            '@babel/plugin-syntax-object-rest-spread',
            '@babel/plugin-proposal-class-properties',
            '@babel/plugin-proposal-optional-chaining'
          ]
        }
      }
    },

    copy: {
      prod: {
        files: [
          {
            expand: true,
            cwd: 'services/',
            src: '**',
            dest: 'services-dist/'
          }
        ]
      }
    }
  });

  grunt.loadNpmTasks('grunt-babel');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-clean');

  grunt.registerTask('prod', ['clean:prod', 'copy:prod', 'babel:prod']);
};

# Customer app

#### Development

###### Prerequisites
- Install MySQL, and import dump located on **database_snapshot.sql**
- Install redis (the easiest way is **brew install redis** and **brew services start redis**)
- You need node 10 or higher
- **nodemon** must be a global dependency (**npm i -g nodemon**)

- Add a pre-commit hook to keep prettier working:
In file **.git/hooks/pre-commit** add this commands
```
#!/bin/sh
FILES=$(git diff --cached --name-only --diff-filter=ACM "*.js" "*.jsx" | sed 's| |\\ |g')
[ -z "$FILES" ] && exit 0

# Prettiflint all selected files
echo "$FILES" | xargs ./node_modules/.bin/prettier-eslint --write

# Add back the modified/prettified files to staging
echo "$FILES" | xargs git add

exit 0
```
Then Run `chmod +x pre-commit` to make it executable


- In order to develop multi-domain requests and cookies we need to set virtual hosts, pointing to 127.0.0.1
1) Run **sudo vi /etc/hosts**
2) Add these 2 lines
	127.0.0.1       client2
	127.0.0.1       client
	127.0.0.1       api
3) Run **sudo apachectl restart**


###### Getting started
- Clone the repo
- npm install
- npm test

- if everything goes well you'll need 3 ternminals to debug all services:
--- **npm run proxy** (proxies everything to port 8000)
--- **npm run back** (backend service)
--- **npm run front** (frontend service)

We should be able to open in the browser with different domains, so we can test different sets of cookies in multuple domains:
localhost:8000/             // should be used to test the app
api:3001/                   // should be used to test the public api from a client app
client:8000/embed-widget    //should be used to test the client app embedding the widget
client2:8000/embed-widget    //should be used to test the client app embedding the widget in a second domain



##### Production
- Follow these steps to install NGINX
https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-18-04

- Landing page files must be copied to /var/www/html/landing (npm run copy-landing)

- Nginx  confg:
vi /etc/nginx/sites-enabled/default
sudo systemctl restart nginx

```
server {
    listen 80 default_server;
    listen [::]:80 default_server;

		root /var/www/html;
    #root /root/zimov/landing;

    # Add index.php to the list if you are using PHP
    index index.html index.htm index.nginx-debian.html;

    server_name _;

    location /api/socket.io/ {
           proxy_pass http://localhost:3001/api/socket.io/;
           proxy_http_version 1.1;
           proxy_set_header Upgrade $http_upgrade;
           proxy_set_header Connection "Upgrade";
    }

    location /api/ {
           proxy_pass http://localhost:3001/api/;
    }

    location /backend/ {
           proxy_pass http://localhost:3001/backend/;
    }

    location / {
           proxy_pass http://localhost:3000/;
    }

    /////////////////////////////////// THIS IS DEPRECARTED BUT IT'S AN EXAMPLE OF HOW TO SERVE STATIC FILES
    location /landing/ {
      root /var/www/html;
    }
}
```

- Restarting the services:
  - npm run prod (if there are no new dependencies)
  - npm run full-prod (install before building)

- MySQL
  -- Restart: `sudo service mysql restart`
  -- SQL_MODE: `sql_mode                = "STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION"`
  -- Main config file: `/etc/mysql/my.cnf`

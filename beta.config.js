module.exports = {
  apps: [
    {
      name: 'back',
      script: './services-dist/back/index-prod.js',

      env: {
        NODE_ENV: 'production',
      },
    },
    {
      name: 'front',
      script: './services-dist/front/index-prod.js',

      env: {
        NODE_ENV: 'production',
      },
    },
  ],
};

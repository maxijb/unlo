module.exports = {
  apps: [
    {
      name: 'back watch',
      script: './services-dist/back/index-prod.js',
      args: 'ftp-watch',
      env: {
        NODE_ENV: 'production',
      },
    },
  ],
};

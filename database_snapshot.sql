-- MySQL dump 10.13  Distrib 5.7.31, for Linux (x86_64)
--
-- Host: localhost    Database: customer
-- ------------------------------------------------------
-- Server version	5.7.31-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ab-tests`
--

DROP TABLE IF EXISTS `ab-tests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab-tests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `description` text,
  `status` enum('not-started','running','closed','full-on') DEFAULT 'not-started',
  `threshold` varchar(20) DEFAULT NULL,
  `maxVariable` tinyint(4) DEFAULT NULL,
  `chosenVariable` tinyint(4) DEFAULT NULL,
  `version` tinyint(4) DEFAULT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ab-tests`
--
-- WHERE:  1 limit 20

LOCK TABLES `ab-tests` WRITE;
/*!40000 ALTER TABLE `ab-tests` DISABLE KEYS */;
INSERT INTO `ab-tests` VALUES (1,'test',NULL,'not-started',NULL,2,NULL,NULL,'2018-10-13 13:19:49','2018-10-13 13:19:49'),(2,'test_run',NULL,'running',NULL,3,NULL,NULL,'2018-10-13 14:22:56','2018-10-13 14:22:56'),(3,'test_closed',NULL,'closed',NULL,2,2,1,'2018-10-13 13:19:57','2018-10-13 13:19:57'),(5,'test-fullon',NULL,'full-on',NULL,2,2,1,'2018-10-13 14:22:05','2018-10-13 14:22:05');
/*!40000 ALTER TABLE `ab-tests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset-relationships`
--

DROP TABLE IF EXISTS `asset-relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset-relationships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ownerType` enum('user','team','campaign','app') DEFAULT NULL,
  `ownerId` int(11) DEFAULT NULL,
  `assetType` enum('app','team','campaign') DEFAULT NULL,
  `assetId` int(11) DEFAULT NULL,
  `status` enum('invited','requested','member') DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `role` varchar(15) DEFAULT 'manager',
  `sender` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset-relationships`
--
-- WHERE:  1 limit 20

LOCK TABLES `asset-relationships` WRITE;
/*!40000 ALTER TABLE `asset-relationships` DISABLE KEYS */;
INSERT INTO `asset-relationships` VALUES (1,'user',14,'app',1,'member','2018-10-28 16:28:58','2019-08-31 10:41:26','analyst',NULL),(2,'user',14,'team',6,'member','2018-10-28 16:30:02','2018-10-28 16:30:02','manager',NULL),(3,'team',6,'campaign',2,'member','2018-10-28 16:30:31','2018-10-28 16:30:31','manager',NULL),(4,'app',1,'campaign',3,'member','2018-10-28 16:31:24','2018-10-28 16:31:24','manager',NULL),(5,'app',1,'campaign',2,'member','2018-10-28 16:32:05','2018-10-28 16:32:05','manager',NULL),(6,'team',6,'campaign',2,'member','2018-10-28 16:32:30','2018-10-28 16:32:30','manager',NULL),(7,'user',14,'app',7,'member','2018-10-28 16:28:58','2018-10-28 16:28:58','operator',NULL),(8,'user',5,'app',1,'member','2019-06-02 13:49:40','2019-06-02 13:49:40','manager',NULL),(9,'user',12,'app',1,'invited','2019-06-08 14:05:41','2019-06-09 00:31:34','owner',NULL),(41,'user',40,'app',1,'member','2019-06-09 07:16:56','2019-06-18 18:03:32','operator',NULL),(42,'user',44,'app',1,'member','2019-06-09 07:16:56','2019-06-18 18:03:32','owner',NULL),(43,'user',13,'app',1,'invited','2019-08-23 08:27:52','2019-08-31 10:41:19','manager',NULL),(44,'user',45,'app',1,'member','2019-08-23 08:28:35','2019-08-23 08:28:35','owner',NULL),(45,'user',47,'app',1,'member','2019-08-23 17:18:37','2019-08-23 17:31:46','owner',NULL),(46,'user',48,'app',1,'invited','2019-08-31 10:45:31','2019-08-31 10:45:31','operator',NULL),(47,'user',51,'app',8,'member','2019-10-21 02:30:59','2019-10-21 02:30:59','owner',NULL),(48,'user',50,'app',9,'member','2019-10-24 17:53:11','2019-10-24 17:53:11','owner',NULL),(49,'user',52,'app',10,'member','2019-10-29 06:55:55','2019-10-29 06:55:55','owner',NULL),(50,'user',52,'app',11,'member','2019-10-29 06:56:30','2019-11-21 20:08:01','owner',NULL),(51,'user',52,'app',12,'member','2019-10-29 06:56:51','2019-10-29 06:56:51','owner',NULL);
/*!40000 ALTER TABLE `asset-relationships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assets`
--

DROP TABLE IF EXISTS `assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(15) DEFAULT 'app',
  `parent` int(11) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `external` tinyint(1) DEFAULT NULL,
  `cc` varchar(255) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `permissions` varchar(10) DEFAULT NULL,
  `options` text,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `timezone` varchar(40) DEFAULT NULL,
  `avatar` varchar(30) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assets`
--
-- WHERE:  1 limit 20

LOCK TABLES `assets` WRITE;
/*!40000 ALTER TABLE `assets` DISABLE KEYS */;
INSERT INTO `assets` VALUES (1,'app',NULL,'Johan Steneros',NULL,NULL,NULL,NULL,'{\n    \"defaultLang\": \"es\",\n    \"supportedLanguages\": [\n        \"es\",\n        \"en\"\n    ],\n    \"conversationScript\": [\n        \"How can we help you today?\",\n        {\n            \"ui\": \"options\",\n            \"options\": [\n                \"Can I see the menu?\",\n                \"Where are you located?\",\n                \"What time do you open?\",\n                \"I would like to place a reservation\",\n                \"I like to see the wine list\"\n            ],\n            \"name\": \"firstOptions\",\n            \"loop\": true\n        },\n        {\n            \"wait\": \"input\"\n        },\n        {\n            \"switch\": \"selected\",\n            \"cases\": {\n                \"Can I see the menu?\": [\n                    \"Yes of course. Here you are.\",\n                    {\n                        \"ui\": \"menu\"\n                    }\n                ],\n                \"Where are you located?\": [\n                    \"Here are the details and map of our location.\",\n                    {\n                        \"ui\": \"map\"\n                    }\n                ],\n                \"What time do you open?\": [\n                    \"These are out opening hours.\",\n                    {\n                        \"ui\": \"open-hours\"\n                    }\n                ],\n                \"I would like to place a reservation\": [\n                    \"Great. Just fill in the details below and we\'ll take care of it.\",\n                    {\n                        \"ui\": \"booking\"\n                    },\n                    {\n                        \"wait\": \"input\"\n                    }\n                ],\n                \"I like to see the wine list\": [\n                    \"Here is the link\",\n                    {\n                        \"ui\": \"menu\"\n                    }\n                ]\n            }\n        },\n        {\n            \"if\": \"conversation.previouslySelected.firstOptions![]=5&&!conversation.hasClientMessage\",\n            \"affirmative\": [\n                {\n                    \"ui\": \"options\",\n                    \"options\": [\n                        \"!@okShowMeMoreOptions\"\n                    ],\n                    \"optionsTitle\": \"\"\n                },\n                {\n                    \"wait\": \"input\"\n                },\n                {\n                    \"conditional\": \"true\",\n                    \"goto\": \"firstOptions\",\n                    \"while\": \"!conversations.hasClientMessage\"\n                }\n            ]\n        },\n        {\n            \"if\": \"!conversation.hasClientMessage\",\n            \"affirmative\": [\n                \"!@anythingElseICanHelp\"\n            ]\n        }\n    ],\n    \"openHours\": [\n        [\n            \"0:00\",\n            \"23:30\"\n        ],\n        [],\n        [],\n        [],\n        [\n            \"2:00\",\n            \"22:45\"\n        ],\n        [\n            \"0:30\",\n            \"22:45\"\n        ],\n        [\n            \"1:15\",\n            \"10:15\",\n            \"10:15\",\n            \"24:00\"\n        ]\n    ],\n    \"bookings\": {\n        \"labels\": [\n            {\n                \"display\": \"Bar\",\n                \"maxConcurrentPeople\": \"67\",\n                \"maxConcurrentBookings\": \"65\"\n            },\n            {\n                \"display\": \"VIP\",\n                \"maxConcurrentBookings\": \"5\",\n                \"maxConcurrentPeople\": \"40\"\n            },\n            {\n                \"display\": \"Terrace\",\n                \"maxConcurrentPeople\": \"23\",\n                \"maxConcurrentBookings\": \"2\"\n            },\n            {\n                \"display\": \"Main room\",\n                \"maxConcurrentPeople\": \"12\",\n                \"maxConcurrentBookings\": \"34\"\n            }\n        ],\n        \"duration\": 40\n    },\n    \"screens\": [\n        [\n            {\n                \"ui\": \"welcome\",\n                \"isOn\": true\n            },\n            {\n                \"ui\": \"chat-invitation\",\n                \"steps\": [\n                    [\n                        {\n                            \"ui\": \"chat\"\n                        }\n                    ]\n                ],\n                \"isOn\": true\n            },\n            {\n                \"ui\": \"booking-invitation\",\n                \"actionName\": \"booking-invitation\",\n                \"steps\": [\n                    {\n                        \"ui\": \"booking\"\n                    }\n                ],\n                \"action\": \"action-step\",\n                \"isOn\": true\n            },\n            {\n                \"id\": \"rate\",\n                \"shortcut\": \"rate\",\n                \"ui\": \"rate\",\n                \"standAlone\": true,\n                \"isOn\": true\n            },\n            {\n                \"id\": \"menu\",\n                \"shortcut\": \"menu\",\n                \"ui\": \"menu\",\n                \"configurePage\": \"/dashboard/settings/menu\",\n                \"standAlone\": true,\n                \"isOn\": true\n            },\n            {\n                \"ui\": \"open-hours\",\n                \"isOn\": true\n            },\n            {\n                \"id\": \"map\",\n                \"shortcut\": \"map-invitation\",\n                \"ui\": \"map-invitation\",\n                \"standAlone\": true,\n                \"isMacro\": false,\n                \"isOn\": true\n            },\n            {\n                \"title\": \"Valentines Menu\",\n                \"text\": \"Fall in love with our Valentine\'s set menu. Full of combinations that match just as perfectly as you. Inludes bottle of Marquez Reserva Red wine. Set menu for 2 only for 50€.\",\n                \"isOn\": true,\n                \"rules\": \"Offer only valid for the 14th of February 2019. Extra items ordered will be charged separetly.\",\n                \"image\": \"1556523284207.png\",\n                \"button\": \"Get deal\",\n                \"enableReservations\": true,\n                \"inlineTitle\": \"Valentine\'s Menu\",\n                \"ui\": \"promotion\",\n                \"action\": \"action-step\",\n                \"actionName\": \"promotion-Valentine\'s Menu\",\n                \"steps\": [\n                    {\n                        \"ui\": \"booking\"\n                    }\n                ]\n            },\n            {\n                \"title\": \"Happy hour\",\n                \"text\": \"2x1 in drinks every wednesday\",\n                \"isOn\": true,\n                \"rules\": \"From 12:00 to 23:00 hs\",\n                \"image\": \"1563768482574.png\",\n                \"button\": \"Get drinks now!\",\n                \"enableReservations\": false,\n                \"inlineTitle\": \"Happy Hour\",\n                \"ui\": \"promotion\",\n                \"action\": \"action-step\",\n                \"actionName\": \"promotion-Happy Hour\",\n                \"steps\": [\n                    {\n                        \"ui\": \"booking\"\n                    }\n                ]\n            }\n        ]\n    ],\n    \"macros\": [\n        {\n            \"shortcut\": \"hi\",\n            \"message\": \"Hello there!\"\n        },\n        {\n            \"shortcut\": \"wineType\",\n            \"message\": \"Do you prefer white wine?\"\n        },\n        {\n            \"shortcut\": \"seeyou\",\n            \"message\": \"Come back later\"\n        },\n        {\n            \"shortcut\": \"bye\",\n            \"message\": \"Thanks for coming\"\n        }\n    ],\n    \"actionMacros\": {\n        \"contact\": {\n            \"active\": true\n        },\n        \"booking\": {\n            \"active\": false\n        },\n        \"hours\": {\n            \"active\": true\n        }\n    },\n    \"styleVars\": {\n        \"theme-color\": \"#392195\",\n        \"theme-text-color\": \"#ffffff\",\n        \"theme-color-hover\": \"#573fb3\",\n        \"theme-color-active\": \"#1b0377\"\n    },\n    \"offers\": [\n        {\n            \"title\": \"Valentines Menu\",\n            \"text\": \"Fall in love with our Valentine\'s set menu. Full of combinations that match just as perfectly as you. Inludes bottle of Marquez Reserva Red wine. Set menu for 2 only for 50€.\",\n            \"isOn\": true,\n            \"rules\": \"Offer only valid for the 14th of February 2019. Extra items ordered will be charged separetly.\",\n            \"image\": \"1556523284207.png\",\n            \"button\": \"Get deal\",\n            \"enableReservations\": true,\n            \"inlineTitle\": \"Valentine\'s Menu\"\n        },\n        {\n            \"title\": \"Happy hour\",\n            \"text\": \"2x1 in drinks every wednesday\",\n            \"isOn\": true,\n            \"rules\": \"From 12:00 to 23:00 hs\",\n            \"image\": \"1563768482574.png\",\n            \"button\": \"Get drinks now!\",\n            \"enableReservations\": false,\n            \"inlineTitle\": \"Happy Hour\"\n        },\n        {\n            \"title\": \"\",\n            \"text\": \"\",\n            \"isOn\": true,\n            \"rules\": \"\",\n            \"image\": \"\",\n            \"inlineTitle\": \"Tero\"\n        }\n    ],\n    \"menuTitle\": \"Want to see our menu?\",\n    \"menuDescription\": \"Our speciality is french cuousine with asain fusion. We also have vegan and gluten free options.\",\n    \"menuList\": [\n        {\n            \"name\": \"menu español\",\n            \"link\": \"\",\n            \"isOn\": true\n        },\n        {\n            \"name\": \"menu1\",\n            \"link\": \"/user-files/1556593615410.png\",\n            \"isOn\": true\n        }\n    ],\n    \"menuImage\": \"wiri-owned/1566077883539.svg\",\n    \"business\": {\n        \"name\": \"The Mandrake\",\n        \"address\": \"333 Harrison St\",\n        \"city\": \"San Francisco\",\n        \"state\": \"NH\",\n        \"telephone\": \"4159098542\",\n        \"zip\": \"1066GE\",\n        \"country\": \"United States\",\n        \"email\": \"infoprisma1@gmail.com\",\n        \"currency\": \"EUR\",\n        \"timezone\": \"America/Argentina/ComodRivadavia\",\n        \"directions\": \"Next to the train station.\"\n    },\n    \"map\": {\n        \"latitude\": 4.895168,\n        \"longitude\": 52.370216\n    },\n    \"mainBotQuestions\": [\n        {\n            \"question\": \"Can I see the menu?\",\n            \"answer\": \"Yes of course. Here you are.\",\n            \"action\": {\n                \"ui\": \"menu\"\n            },\n            \"active\": true,\n            \"ui\": \"menu\"\n        },\n        {\n            \"question\": \"Where are you located?\",\n            \"answer\": \"Here are the details and map of our location.\",\n            \"action\": {\n                \"ui\": \"map\"\n            },\n            \"active\": true,\n            \"ui\": \"map\"\n        },\n        {\n            \"question\": \"What time do you open?\",\n            \"answer\": \"These are out opening hours.\",\n            \"action\": null,\n            \"active\": true,\n            \"ui\": \"open-hours\"\n        },\n        {\n            \"question\": \"I would like to place a reservation\",\n            \"answer\": \"Great. Just fill in the details below and we\'ll take care of it.\",\n            \"action\": {\n                \"ui\": \"booking\"\n            },\n            \"active\": true,\n            \"ui\": \"booking\"\n        },\n        {\n            \"question\": \"I like to see the wine list\",\n            \"answer\": \"Here is the link\",\n            \"ui\": \"menu\"\n        }\n    ],\n    \"botImage\": \"1565488518669.jpg\",\n    \"specialOpenHours\": {},\n    \"botName\": \"Big Mac Bot\",\n    \"wizardWidgetStep\": true,\n    \"wizardSnippetStep\": true,\n    \"messages\": {\n        \"welcomeTitle\": \"Hi! Welcome\",\n        \"botGreeting\": \"How can we help you today?\"\n    }\n}','2019-02-18 13:56:51','2020-03-02 06:42:32','Europe/Andorra','1569272792577.jpeg',NULL),(2,'campaign',1,'testCampaign',NULL,NULL,NULL,NULL,'{\"supportedLanguages\": [\"es\", \"en\", \"fr\"], \"messages\": {\"rateTitle\": \"How was your experience?\"}\n}','2019-03-10 13:26:47','2019-03-10 13:26:47',NULL,NULL,NULL),(5,'campaign',NULL,'fail',NULL,NULL,NULL,NULL,NULL,'2019-03-10 14:52:18','2019-03-10 14:52:18',NULL,NULL,NULL),(6,'team',NULL,'testteam',NULL,NULL,NULL,NULL,NULL,'2019-03-25 20:00:30','2019-03-25 20:00:30',NULL,NULL,NULL),(7,'app',NULL,'The Mandrake',NULL,NULL,NULL,NULL,'{\"defaultLang\":\"es\",\"supportedLanguages\":[\"es\",\"en\"],\"conversationScript\":[\"Hey, good to see you!\",\"How can I help you today?\",{\"ui\":\"options\",\"options\":[\"I want to make a reservation\",\"Show me the menu\",\"what time do you close?\"],\"name\":\"firstOptions\",\"loop\":true},{\"wait\":\"input\"},{\"switch\":\"selected\",\"skip\":true,\"cases\":{\"I want to make a reservation\":[{\"ui\":\"booking\"},{\"wait\":\"input\"}],\"Show me the menu\":[\"This is the menu <LINK>\"],\"what time do you close?\":[\"These are our open hours\",{\"ui\":\"open-hours\"}]}},{\"conditional\":\"true\",\"goto\":\"firstOptions\",\"while\":\"conversation.previouslySelected.firstOptions![]=3&&message.owner!=client&&!message.message.skip\"},\"anything else?\",\"how can i contact you?\",{\"ui\":\"contact\"},{\"wait\":\"input\"},{\"ui\":\"options\",\"options\":[\"yes\",\"no\"],\"prev\":\"Have you been to our restaurant yet?\"},{\"wait\":\"input\"},{\"if\":\"selected=yes\",\"affirmative\":[\"How was your experience the previous time?\",{\"ui\":\"rate\"}],\"negative\":[\"You selected NO\"]},{\"wait\":\"input\"},\"Hope to see you soon!\"],\"openHours\":[[\"0:15\",\"16:15\"],[],[\"1:00\",\"22:45\"],[],[\"2:00\",\"12:15\",\"17:15\",\"23:30\"],[\"0:30\",\"12:45\"],[\"1:15\",\"23:15\"]],\"bookings\":{\"labels\":[{\"display\":\"Bar\",\"maxConcurrentPeople\":\"67\",\"maxConcurrentBookings\":\"65\"},{\"display\":\"VIP\",\"maxConcurrentBookings\":\"5\",\"maxConcurrentPeople\":\"40\"},{\"display\":\"Terrace\",\"maxConcurrentPeople\":\"23\",\"maxConcurrentBookings\":\"2\"},{\"display\":\"Main room\",\"maxConcurrentPeople\":\"12\",\"maxConcurrentBookings\":\"34\"}],\"duration\":40},\"screens\":[[{\"ui\":\"welcome\",\"isOn\":true},{\"ui\":\"chat-invitation\",\"steps\":[[{\"ui\":\"chat\"}]],\"isOn\":true},{\"ui\":\"open-hours\",\"title\":\"Open hours\",\"isOn\":true},{\"ui\":\"booking-invitation\",\"actionName\":\"booking-invitation\",\"steps\":[{\"ui\":\"booking\"}],\"action\":\"action-step\",\"isOn\":true},{\"id\":\"menu\",\"shortcut\":\"menu\",\"ui\":\"menu\",\"configurePage\":\"/dashboard/settings/menu\",\"standAlone\":true,\"isOn\":true},{\"id\":\"rate\",\"shortcut\":\"rate\",\"ui\":\"rate\",\"standAlone\":true,\"isOn\":true}]],\"macros\":[{\"shortcut\":\"hi\",\"message\":\"Hello there!\"},{\"shortcut\":\"thanks\",\"message\":\"Thanks you\"},{\"shortcut\":\"seeyou\",\"message\":\"Come back later\"}],\"actionMacros\":{\"contact\":{\"active\":true},\"booking\":{\"active\":false},\"hours\":{\"active\":true}},\"styleVars\":{\"theme-color\":\"#956e21\",\"theme-text-color\":\"#ffffff\"},\"offers\":[{\"title\":\"Valentines Menu\",\"text\":\"Fall in love with our Valentine\'s set menu. Full of combinations that match just as perfectly as you. Inludes bottle of Marquez Reserva Red wine. Set menu for 2 only for 50€.\",\"isOn\":true,\"rules\":\"Offer only valid for the 14th of February 2019. Extra items ordered will be charged separetly.\",\"image\":\"1556523284207.png\",\"button\":\"Get deal\",\"enableReservations\":true}],\"menuTitle\":\"deedd\",\"menuDescription\":\"dedeedde\",\"menuList\":[{\"name\":\"menu español\",\"link\":\"\",\"isOn\":true},{\"name\":\"menu1\",\"link\":\"/user-files/1556593615410.png\",\"isOn\":true}],\"menuImage\":\"1556593790496.png\",\"business\":{\"name\":\"deedd\",\"address\":\"Duke Ellingtonstraat 5\",\"city\":\"Amsterdam\",\"state\":\"nh\",\"telephone\":\"0636529044\",\"zip\":\"1066GE\",\"country\":\"Netherlands\",\"email\":\"infoprisma1@gmail.com\",\"image\":\"1556594774414.png\",\"currency\":\"EUR\",\"timezone\":\"America/Argentina/ComodRivadavia\"},\"mainBotQuestions\":[{\"question\":\"Can I see the menu?\",\"answer\":\"Yes of course. Here you are.\",\"action\":{\"ui\":\"menu\"},\"active\":true},{\"question\":\"Where are you located?\",\"answer\":\"Here are the details and map of our location.\",\"action\":{\"ui\":\"map\"},\"active\":true},{\"question\":\"What kind of food do you serve?\",\"answer\":\"We serve traditional india food.\",\"action\":null,\"active\":true},{\"question\":\"I would like to place a reservation\",\"answer\":\"Great. Just fill in the details below and we\'ll take care of it.\",\"action\":{\"ui\":\"booking\"},\"active\":true}],\"botImage\":\"1556645585458.png\",\"specialOpenHours\":{\"2019-05-22\":[\"0\",\"24\"],\"2019-05-25\":[]},\"messages\":{}}','2019-02-18 13:56:51','2019-05-18 04:13:51','Europe/Andorra','1556594514669.png',NULL),(8,'app',NULL,'xwss',NULL,NULL,51,NULL,'{\n    \"specialOpenHours\": {},\n    \"openHours\": [\n        [],\n        [],\n        [],\n        [],\n        [],\n        [],\n        []\n    ],\n    \"wizardWidgetStep\": true,\n    \"messages\": {}\n}','2019-10-21 02:30:59','2019-10-21 02:31:20','Etc/UTC',NULL,NULL),(9,'app',NULL,'Cats Kitchen',NULL,NULL,50,NULL,NULL,'2019-10-24 17:53:11','2019-10-24 17:53:11',NULL,NULL,NULL),(10,'app',NULL,'',NULL,NULL,52,NULL,'{\n    \"specialOpenHours\": {},\n    \"openHours\": [\n        [],\n        [],\n        [],\n        [],\n        [],\n        [],\n        []\n    ],\n    \"business\": {\n        \"name\": \"Feliciano\"\n    },\n    \"messages\": {}\n}','2019-10-29 06:55:55','2019-11-03 08:09:32','Etc/UTC',NULL,NULL),(11,'app',NULL,'Cat\'s Kitchen',NULL,NULL,52,NULL,'{\n    \"specialOpenHours\": {},\n    \"openHours\": [\n        [\n            \"9:00\",\n            \"20:00\"\n        ],\n        [\n            \"9:00\",\n            \"18:00\"\n        ],\n        [\n            \"9:00\",\n            \"18:00\"\n        ],\n        [\n            \"9:00\",\n            \"18:00\"\n        ],\n        [\n            \"9:00\",\n            \"18:00\"\n        ],\n        [\n            \"9:00\",\n            \"23:30\"\n        ],\n        [\n            \"9:00\",\n            \"23:30\"\n        ]\n    ],\n    \"business\": {\n        \"name\": \"Cat\'s Kitchen\",\n        \"address\": \"Agrojardin, N-340,\",\n        \"zip\": \"29680\",\n        \"city\": \"Estepona\",\n        \"state\": \"Malaga\",\n        \"country\": \"Spain\",\n        \"latitude\": 36.4562778,\n        \"longitude\": -5.060873700000002,\n        \"telephone\": \"+34 659 92 04 21\",\n        \"currency\": \"EUR\",\n        \"email\": \"cats@lacatarina.beer\",\n        \"directions\": \"Driving on the n-340 highway towards Estepona take turn right just before Rio Guadalmansa just after Cancelada.\"\n    },\n    \"wizardWidgetStep\": true,\n    \"styleVars\": {\n        \"theme-color\": \"#F2AB37\",\n        \"theme-text-color\": \"#000000\",\n        \"theme-color-hover\": \"#ffc955\",\n        \"theme-color-active\": \"#d48d19\"\n    },\n    \"position\": \"left\",\n    \"screens\": [\n        [\n            {\n                \"ui\": \"welcome\",\n                \"isOn\": true\n            },\n            {\n                \"ui\": \"chat-invitation\",\n                \"steps\": [\n                    [\n                        {\n                            \"ui\": \"chat\"\n                        }\n                    ]\n                ],\n                \"isOn\": true\n            },\n            {\n                \"ui\": \"open-hours\",\n                \"isOn\": true\n            },\n            {\n                \"id\": \"booking-invitation\",\n                \"shortcut\": \"booking-invitation\",\n                \"ui\": \"booking-invitation\",\n                \"standAlone\": true,\n                \"isMacro\": false,\n                \"actionName\": \"booking-invitation\",\n                \"steps\": [\n                    {\n                        \"ui\": \"booking\"\n                    }\n                ],\n                \"isOn\": true\n            },\n            {\n                \"id\": \"map\",\n                \"shortcut\": \"map-invitation\",\n                \"ui\": \"map-invitation\",\n                \"standAlone\": true,\n                \"isMacro\": false,\n                \"isOn\": true\n            },\n            {\n                \"id\": \"menu\",\n                \"shortcut\": \"menu\",\n                \"ui\": \"menu\",\n                \"configurePage\": \"/dashboard/settings/menu\",\n                \"standAlone\": true,\n                \"isOn\": true\n            }\n        ]\n    ],\n    \"menuList\": [\n        {\n            \"name\": \"Main Menu\",\n            \"link\": \"http://lacatarina.beer/catskitchen/menu/\",\n            \"isOn\": true\n        }\n    ],\n    \"menuTitle\": \"Want to see our menu\",\n    \"menuDescription\": \"We are a Steakhouse, Brew Pub we have great hamburgers, pork ribs and many other comfort food dishes.\",\n    \"bookings\": {\n        \"labels\": [\n            {\n                \"display\": \"Indoors\",\n                \"maxConcurrentPeople\": \"100\",\n                \"maxConcurrentBookings\": \"20\"\n            },\n            {\n                \"display\": \"Terrace\",\n                \"maxConcurrentPeople\": \"40\",\n                \"maxConcurrentBookings\": \"10\"\n            }\n        ],\n        \"duration\": 90\n    },\n    \"mainBotQuestions\": [\n        {\n            \"question\": \"Can I see the menu?\",\n            \"answer\": \"Yes of course. Here you are.\",\n            \"action\": {\n                \"ui\": \"menu\"\n            },\n            \"active\": true,\n            \"ui\": \"menu\"\n        },\n        {\n            \"question\": \"Where are you located?\",\n            \"answer\": \"Here are the details and map of our location.\",\n            \"action\": {\n                \"ui\": \"map\"\n            },\n            \"active\": true,\n            \"ui\": \"map\"\n        },\n        {\n            \"question\": \"What kind of food do you serve?\",\n            \"answer\": \"We are steakhouse serving home made confort food.\",\n            \"action\": null,\n            \"active\": true,\n            \"ui\": \"menu\"\n        },\n        {\n            \"question\": \"I would like to place a reservation\",\n            \"answer\": \"Great. Just fill in the details below and we\'ll take care of it.\",\n            \"action\": {\n                \"ui\": \"booking\"\n            },\n            \"active\": true,\n            \"ui\": \"booking\"\n        }\n    ],\n    \"botName\": \"Cat\'s Kitchen Helper\",\n    \"conversationScript\": [\n        \"How can we help you today?\",\n        {\n            \"ui\": \"options\",\n            \"options\": [\n                \"Can I see the menu?\",\n                \"Where are you located?\",\n                \"What kind of food do you serve?\",\n                \"I would like to place a reservation\"\n            ],\n            \"name\": \"firstOptions\",\n            \"loop\": true\n        },\n        {\n            \"wait\": \"input\"\n        },\n        {\n            \"switch\": \"selected\",\n            \"cases\": {\n                \"Can I see the menu?\": [\n                    \"Yes of course. Here you are.\",\n                    {\n                        \"ui\": \"menu\"\n                    }\n                ],\n                \"Where are you located?\": [\n                    \"Here are the details and map of our location.\",\n                    {\n                        \"ui\": \"map\"\n                    }\n                ],\n                \"What kind of food do you serve?\": [\n                    \"We are steakhouse serving home made confort food.\",\n                    {\n                        \"ui\": \"menu\"\n                    }\n                ],\n                \"I would like to place a reservation\": [\n                    \"Great. Just fill in the details below and we\'ll take care of it.\",\n                    {\n                        \"ui\": \"booking\"\n                    },\n                    {\n                        \"wait\": \"input\"\n                    }\n                ]\n            }\n        },\n        {\n            \"if\": \"conversation.previouslySelected.firstOptions![]=4&&!conversation.hasClientMessage\",\n            \"affirmative\": [\n                {\n                    \"ui\": \"options\",\n                    \"options\": [\n                        \"!@okShowMeMoreOptions\"\n                    ],\n                    \"optionsTitle\": \"\",\n                    \"kind\": \"small\"\n                },\n                {\n                    \"wait\": \"input\"\n                },\n                {\n                    \"conditional\": \"true\",\n                    \"goto\": \"firstOptions\",\n                    \"while\": \"!conversation.hasClientMessage\"\n                }\n            ]\n        },\n        {\n            \"if\": \"!conversation.hasClientMessage\",\n            \"affirmative\": [\n                \"!@anythingElseICanHelp\"\n            ]\n        }\n    ],\n    \"messages\": {}\n}','2019-10-29 06:56:30','2020-02-03 06:25:44','Europe/Madrid','1572771884007.png','https://lacatarina.beer/catskitchen/'),(12,'app',NULL,'Rooftop Restaurant',NULL,NULL,52,NULL,'{\n    \"specialOpenHours\": {},\n    \"openHours\": [\n        [\n            \"12:00\",\n            \"22:00\"\n        ],\n        [],\n        [\n            \"12:00\",\n            \"16:00\",\n            \"18:00\",\n            \"22:00\"\n        ],\n        [\n            \"12:00\",\n            \"16:00\",\n            \"18:00\",\n            \"22:00\"\n        ],\n        [\n            \"12:00\",\n            \"16:00\",\n            \"18:00\",\n            \"22:00\"\n        ],\n        [\n            \"12:00\",\n            \"22:00\"\n        ],\n        [\n            \"12:00\",\n            \"22:00\"\n        ]\n    ],\n    \"business\": {\n        \"name\": \"Rooftop Restaurant\",\n        \"address\": \"432 Wyandotte Ave\",\n        \"city\": \"Lakewood\",\n        \"state\": \"Ohio\",\n        \"zip\": \"44107\",\n        \"country\": \"USA\",\n        \"latitude\": 41.475038102861184,\n        \"longitude\": -81.78347142695316,\n        \"telephone\": \"012 44 (216) 785-9346\",\n        \"currency\": \"USD\",\n        \"directions\": \"From Franklin Park On Madison Avenue  take the first on the right and you should see us. \"\n    },\n    \"wizardWidgetStep\": true,\n    \"styleVars\": {\n        \"theme-color\": \"#96875f\",\n        \"theme-text-color\": \"#ffffff\",\n        \"theme-color-hover\": \"#b4a57d\",\n        \"theme-color-active\": \"#786941\"\n    },\n    \"position\": \"left\",\n    \"screens\": [\n        [\n            {\n                \"ui\": \"welcome\",\n                \"isOn\": true\n            },\n            {\n                \"ui\": \"chat-invitation\",\n                \"steps\": [\n                    [\n                        {\n                            \"ui\": \"chat\"\n                        }\n                    ]\n                ],\n                \"isOn\": true\n            },\n            {\n                \"id\": \"hours\",\n                \"shortcut\": \"hours\",\n                \"ui\": \"open-hours\",\n                \"configurePage\": \"/dashboard/settings/hours\",\n                \"standAlone\": true,\n                \"isOn\": true\n            },\n            {\n                \"id\": \"booking-invitation\",\n                \"shortcut\": \"booking-invitation\",\n                \"ui\": \"booking-invitation\",\n                \"standAlone\": true,\n                \"isMacro\": false,\n                \"actionName\": \"booking-invitation\",\n                \"steps\": [\n                    {\n                        \"ui\": \"booking\"\n                    }\n                ],\n                \"isOn\": true\n            },\n            {\n                \"id\": \"map\",\n                \"shortcut\": \"map-invitation\",\n                \"ui\": \"map-invitation\",\n                \"standAlone\": true,\n                \"isMacro\": false,\n                \"isOn\": true\n            }\n        ]\n    ],\n    \"menuList\": [\n        {\n            \"name\": \"Main Menu\",\n            \"link\": \"https://wiri.io/static/demo/rooftop/menu.html\",\n            \"isOn\": true\n        }\n    ],\n    \"menuTitle\": \"See our Menu\",\n    \"menuDescription\": \"We specialise in real home American dinner food. Have a look at our menu.\",\n    \"bookings\": {\n        \"labels\": [\n            {\n                \"display\": \"Main Room\",\n                \"maxConcurrentPeople\": \"40\",\n                \"maxConcurrentBookings\": \"5\"\n            },\n            {\n                \"display\": \"Terrace\",\n                \"maxConcurrentPeople\": \"20\",\n                \"maxConcurrentBookings\": \"5\"\n            }\n        ],\n        \"duration\": 50\n    },\n    \"mainBotQuestions\": [\n        {\n            \"question\": \"Can I see the menu?\",\n            \"answer\": \"Yes of course. Here you are.\",\n            \"action\": {\n                \"ui\": \"menu\"\n            },\n            \"active\": true,\n            \"ui\": \"menu\"\n        },\n        {\n            \"question\": \"Where are you located?\",\n            \"answer\": \"Here are the details and map of our location.\",\n            \"action\": {\n                \"ui\": \"map\"\n            },\n            \"active\": true,\n            \"ui\": \"map\"\n        },\n        {\n            \"question\": \"What kind of food do you serve?\",\n            \"answer\": \"We serve traditional American dinner  food.\",\n            \"action\": null,\n            \"active\": true,\n            \"ui\": \"\"\n        },\n        {\n            \"question\": \"I would like to place a reservation\",\n            \"answer\": \"Great. Just fill in the details below and we\'ll take care of it.\",\n            \"action\": {\n                \"ui\": \"booking\"\n            },\n            \"active\": true,\n            \"ui\": \"booking\"\n        }\n    ],\n    \"conversationScript\": [\n        \"How can we help you today?\",\n        {\n            \"ui\": \"options\",\n            \"options\": [\n                \"Can I see the menu?\",\n                \"Where are you located?\",\n                \"What kind of food do you serve?\",\n                \"I would like to place a reservation\"\n            ],\n            \"name\": \"firstOptions\",\n            \"loop\": true\n        },\n        {\n            \"wait\": \"input\"\n        },\n        {\n            \"switch\": \"selected\",\n            \"cases\": {\n                \"Can I see the menu?\": [\n                    \"Yes of course. Here you are.\",\n                    {\n                        \"ui\": \"menu\"\n                    }\n                ],\n                \"Where are you located?\": [\n                    \"Here are the details and map of our location.\",\n                    {\n                        \"ui\": \"map\"\n                    }\n                ],\n                \"What kind of food do you serve?\": [\n                    \"We serve traditional American dinner  food.\"\n                ],\n                \"I would like to place a reservation\": [\n                    \"Great. Just fill in the details below and we\'ll take care of it.\",\n                    {\n                        \"ui\": \"booking\"\n                    },\n                    {\n                        \"wait\": \"input\"\n                    }\n                ]\n            }\n        },\n        {\n            \"if\": \"conversation.previouslySelected.firstOptions![]=4&&!conversation.hasClientMessage\",\n            \"affirmative\": [\n                {\n                    \"ui\": \"options\",\n                    \"options\": [\n                        \"!@okShowMeMoreOptions\"\n                    ],\n                    \"optionsTitle\": \"\",\n                    \"kind\": \"small\"\n                },\n                {\n                    \"wait\": \"input\"\n                },\n                {\n                    \"conditional\": \"true\",\n                    \"goto\": \"firstOptions\",\n                    \"while\": \"!conversation.hasClientMessage\"\n                }\n            ]\n        },\n        {\n            \"if\": \"!conversation.hasClientMessage\",\n            \"affirmative\": [\n                \"!@anythingElseICanHelp\"\n            ]\n        }\n    ],\n    \"botName\": \"Rooftop Bot\",\n    \"botImage\": \"1573937023054.png\",\n    \"macros\": [\n        {\n            \"shortcut\": \"hi\",\n            \"message\": \"Hello there! How can we help you?\"\n        }\n    ],\n    \"actionMacros\": [],\n    \"messages\": {}\n}','2019-10-29 06:56:51','2020-02-03 03:24:18','US/Central','1572769515972.png','https://wiri.io/static/demo/rooftop/index.html'),(13,'app',NULL,'',NULL,NULL,53,NULL,NULL,'2019-11-07 18:36:15','2019-11-07 18:36:15',NULL,NULL,NULL),(14,'app',NULL,'',NULL,NULL,54,NULL,'{\n    \"specialOpenHours\": {},\n    \"openHours\": [\n        [],\n        [],\n        [],\n        [],\n        [],\n        [],\n        []\n    ],\n    \"wizardWidgetStep\": true,\n    \"messages\": {}\n}','2019-11-07 18:57:17','2019-11-07 19:12:26','Etc/UTC',NULL,NULL),(15,'app',NULL,'Feliciano',NULL,NULL,46,NULL,'{\n    \"specialOpenHours\": {},\n    \"openHours\": [\n        [\n            \"10:00\",\n            \"24:00\"\n        ],\n        [],\n        [],\n        [\n            \"10:00\",\n            \"14:00\",\n            \"17:00\",\n            \"24:00\"\n        ],\n        [\n            \"10:00\",\n            \"14:00\",\n            \"17:00\",\n            \"24:00\"\n        ],\n        [\n            \"10:00\",\n            \"24:00\"\n        ],\n        [\n            \"10:00\",\n            \"24:00\"\n        ]\n    ],\n    \"business\": {\n        \"address\": \"Calle Sagasta 56\",\n        \"city\": \"Madrid\",\n        \"country\": \"Spain\",\n        \"latitude\": 40.4272884835842,\n        \"longitude\": -3.6952487991974294,\n        \"telephone\": \"+ 1235 2355 98\",\n        \"name\": \"Feliciano\",\n        \"zip\": \"28004\",\n        \"email\": \"info@feliciano.com\",\n        \"directions\": \"We are located in the heart of Madrid. Next to Alonso Martinez Tube station.\",\n        \"currency\": \"EUR\"\n    },\n    \"wizardWidgetStep\": true,\n    \"styleVars\": {\n        \"theme-color\": \"#212121\",\n        \"theme-text-color\": \"#ffffff\",\n        \"theme-color-hover\": \"#3f3f3f\",\n        \"theme-color-active\": \"#303030\"\n    },\n    \"menuTitle\": \"Want to see our menu ?\",\n    \"menuDescription\": \"We specialize in real authentic Italian comfort food. Pizza, Pasta, Anti Pasti\",\n    \"menuList\": [\n        {\n            \"name\": \"Main menu\",\n            \"link\": \"https://wiri.io/static/demo/feliciano/menu.html\",\n            \"isOn\": true\n        }\n    ],\n    \"bookings\": {\n        \"labels\": [\n            {\n                \"display\": \"Main Room\",\n                \"maxConcurrentPeople\": \"20\",\n                \"maxConcurrentBookings\": \"10\"\n            }\n        ],\n        \"duration\": 40\n    },\n    \"mainBotQuestions\": [\n        {\n            \"question\": \"Can I see the menu?\",\n            \"answer\": \"Yes of course. Here you are.\",\n            \"action\": {\n                \"ui\": \"menu\"\n            },\n            \"active\": true,\n            \"ui\": \"menu\"\n        },\n        {\n            \"question\": \"Where are you located?\",\n            \"answer\": \"Here are the details and map of our location.\",\n            \"action\": {\n                \"ui\": \"map\"\n            },\n            \"active\": true,\n            \"ui\": \"map\"\n        },\n        {\n            \"question\": \"What kind of food do you serve?\",\n            \"answer\": \"Real Italian home made pasta and pizza\",\n            \"action\": null,\n            \"active\": true\n        },\n        {\n            \"question\": \"I would like to place a reservation\",\n            \"answer\": \"Great. Just fill in the details below and we\'ll take care of it.\",\n            \"action\": {\n                \"ui\": \"booking\"\n            },\n            \"active\": true,\n            \"ui\": \"booking\"\n        }\n    ],\n    \"conversationScript\": [\n        \"How can we help you today?\",\n        {\n            \"ui\": \"options\",\n            \"options\": [\n                \"Can I see the menu?\",\n                \"Where are you located?\",\n                \"What kind of food do you serve?\",\n                \"I would like to place a reservation\"\n            ],\n            \"name\": \"firstOptions\",\n            \"loop\": true\n        },\n        {\n            \"wait\": \"input\"\n        },\n        {\n            \"switch\": \"selected\",\n            \"cases\": {\n                \"Can I see the menu?\": [\n                    \"Yes of course. Here you are.\",\n                    {\n                        \"ui\": \"menu\"\n                    }\n                ],\n                \"Where are you located?\": [\n                    \"Here are the details and map of our location.\",\n                    {\n                        \"ui\": \"map\"\n                    }\n                ],\n                \"What kind of food do you serve?\": [\n                    \"Real Italian home made pasta and pizza\"\n                ],\n                \"I would like to place a reservation\": [\n                    \"Great. Just fill in the details below and we\'ll take care of it.\",\n                    {\n                        \"ui\": \"booking\"\n                    },\n                    {\n                        \"wait\": \"input\"\n                    }\n                ]\n            }\n        },\n        {\n            \"if\": \"conversation.previouslySelected.firstOptions![]=4&&!conversation.hasClientMessage\",\n            \"affirmative\": [\n                {\n                    \"ui\": \"options\",\n                    \"options\": [\n                        \"!@okShowMeMoreOptions\"\n                    ],\n                    \"optionsTitle\": \"\",\n                    \"kind\": \"small\"\n                },\n                {\n                    \"wait\": \"input\"\n                },\n                {\n                    \"conditional\": \"true\",\n                    \"goto\": \"firstOptions\",\n                    \"while\": \"!conversation.hasClientMessage\"\n                }\n            ]\n        },\n        {\n            \"if\": \"!conversation.hasClientMessage\",\n            \"affirmative\": [\n                \"!@anythingElseICanHelp\"\n            ]\n        }\n    ],\n    \"offers\": [\n        {\n            \"title\": \"\",\n            \"text\": \"Enjoy a complimentary bottle of wine of us, next Tuesday when you order any two main courses!*\\n\\n\",\n            \"isOn\": true,\n            \"rules\": \"Table for two before 20:00 and must order two main courses \",\n            \"image\": \"resized/1579454736493.jpeg\",\n            \"inlineTitle\": \"Free wine on Tuesday\",\n            \"enableReservations\": false,\n            \"button\": \"I want this deal!\"\n        }\n    ],\n    \"screens\": [\n        [\n            {\n                \"ui\": \"welcome\",\n                \"isOn\": true\n            },\n            {\n                \"id\": \"hours\",\n                \"shortcut\": \"hours\",\n                \"ui\": \"open-hours\",\n                \"configurePage\": \"/dashboard/settings/hours\",\n                \"standAlone\": true,\n                \"isOn\": true\n            },\n            {\n                \"title\": \"\",\n                \"text\": \"Enjoy a complimentary bottle of wine of us, next Tuesday when you order any two main courses!*\\n\\n\",\n                \"isOn\": true,\n                \"rules\": \"Table for two before 20:00 and must order two main courses \",\n                \"image\": \"resized/1579454736493.jpeg\",\n                \"inlineTitle\": \"Free wine on Tuesday\",\n                \"enableReservations\": false,\n                \"ui\": \"promotion\",\n                \"action\": \"action-step\",\n                \"actionName\": \"promotion-Free wine on Tuesday\",\n                \"steps\": [\n                    {\n                        \"ui\": \"booking\"\n                    }\n                ],\n                \"button\": \"I want this deal!\"\n            },\n            {\n                \"id\": \"menu\",\n                \"shortcut\": \"menu\",\n                \"ui\": \"menu\",\n                \"configurePage\": \"/dashboard/settings/menu\",\n                \"standAlone\": true,\n                \"isOn\": true\n            },\n            {\n                \"id\": \"map\",\n                \"shortcut\": \"map-invitation\",\n                \"ui\": \"map-invitation\",\n                \"standAlone\": true,\n                \"isMacro\": false,\n                \"isOn\": true\n            },\n            {\n                \"id\": \"chat-invitation\",\n                \"shortcut\": \"chat-invitation\",\n                \"ui\": \"chat-invitation\",\n                \"standAlone\": true,\n                \"isMacro\": false,\n                \"isOn\": true\n            },\n            {\n                \"id\": \"booking-invitation\",\n                \"shortcut\": \"booking-invitation\",\n                \"ui\": \"booking-invitation\",\n                \"standAlone\": true,\n                \"isMacro\": false,\n                \"actionName\": \"booking-invitation\",\n                \"steps\": [\n                    {\n                        \"ui\": \"booking\"\n                    }\n                ],\n                \"isOn\": true\n            }\n        ]\n    ],\n    \"messages\": {}\n}','2019-11-14 19:55:34','2020-02-05 07:05:03','Etc/UTC','1573761603781.png','www.felciano.com'),(16,'app',NULL,'Wiri',NULL,NULL,60,NULL,'{\n    \"specialOpenHours\": {},\n    \"openHours\": [\n        [],\n        [\n            \"9:00\",\n            \"18:00\"\n        ],\n        [\n            \"9:00\",\n            \"18:00\"\n        ],\n        [\n            \"9:00\",\n            \"18:00\"\n        ],\n        [\n            \"9:00\",\n            \"18:00\"\n        ],\n        [\n            \"9:00\",\n            \"18:00\"\n        ],\n        []\n    ],\n    \"wizardWidgetStep\": true,\n    \"business\": {\n        \"email\": \"hello@wiri.io\",\n        \"address\": \"Somewhere in Delaware 1\",\n        \"city\": \"Delaware\",\n        \"country\": \"USA\",\n        \"latitude\": 38.9366433,\n        \"longitude\": -75.427551,\n        \"telephone\": \"+34 686953142\"\n    },\n    \"mainBotQuestions\": [\n        {\n            \"question\": \"How do i set up an account?\",\n            \"answer\": \"Make sure you sign up to the waitlist. As soon as we are ready for the beta launch we will email you an invitation link.\",\n            \"ui\": \"\",\n            \"active\": true\n        },\n        {\n            \"question\": \"How much does it cost?\",\n            \"answer\": \"We have not determined the price yet. But there will always be a freemium option, with limited features.\",\n            \"ui\": \"\",\n            \"active\": true\n        },\n        {\n            \"question\": \"Where can i get personal support?\",\n            \"answer\": \"Just leave you questions here in the chatbox. One of our founders will read it and get back to you as soon as they can.\",\n            \"ui\": \"openHours\",\n            \"active\": true\n        }\n    ],\n    \"botName\": \"Wiri Bot\",\n    \"conversationScript\": [\n        \"How can we help you today?\",\n        {\n            \"ui\": \"options\",\n            \"options\": [\n                \"How do i set up an account?\",\n                \"How much does it cost?\",\n                \"Where can i get personal support?\"\n            ],\n            \"name\": \"firstOptions\",\n            \"loop\": true\n        },\n        {\n            \"wait\": \"input\"\n        },\n        {\n            \"switch\": \"selected\",\n            \"cases\": {\n                \"How do i set up an account?\": [\n                    \"Make sure you sign up to the waitlist. As soon as we are ready for the beta launch we will email you an invitation link.\"\n                ],\n                \"How much does it cost?\": [\n                    \"We have not determined the price yet. But there will always be a freemium option, with limited features.\"\n                ],\n                \"Where can i get personal support?\": [\n                    \"Just leave you questions here in the chatbox. One of our founders will read it and get back to you as soon as they can.\"\n                ]\n            }\n        },\n        {\n            \"if\": \"conversation.previouslySelected.firstOptions![]=3&&!conversation.hasClientMessage\",\n            \"affirmative\": [\n                {\n                    \"ui\": \"options\",\n                    \"options\": [\n                        \"!@okShowMeMoreOptions\"\n                    ],\n                    \"optionsTitle\": \"\",\n                    \"kind\": \"small\"\n                },\n                {\n                    \"wait\": \"input\"\n                },\n                {\n                    \"conditional\": \"true\",\n                    \"goto\": \"firstOptions\",\n                    \"while\": \"!conversation.hasClientMessage\"\n                }\n            ]\n        },\n        {\n            \"if\": \"!conversation.hasClientMessage\",\n            \"affirmative\": [\n                \"!@anythingElseICanHelp\"\n            ]\n        }\n    ],\n    \"macros\": [\n        {\n            \"shortcut\": \"hi\",\n            \"message\": \"Hello there!\"\n        }\n    ],\n    \"actionMacros\": {\n        \"rate\": {\n            \"active\": false\n        },\n        \"contact\": {\n            \"active\": true\n        },\n        \"hours\": {\n            \"active\": false\n        }\n    },\n    \"menuTitle\": \"See a Demo\",\n    \"menuImage\": \"1574605603302.png\",\n    \"menuDescription\": \"Click on the following demo links to see our widget in action.\",\n    \"menuList\": [\n        {\n            \"name\": \"Feliciano Demo\",\n            \"link\": \"https://wiri.io/static/demo/feliciano/index.html\",\n            \"isOn\": true\n        },\n        {\n            \"name\": \"Rooftop Demo\",\n            \"link\": \"https://wiri.io/static/demo/rooftop/index.html\",\n            \"isOn\": true\n        }\n    ],\n    \"botImage\": \"1574605989064.png\",\n    \"screens\": [\n        [\n            {\n                \"ui\": \"welcome\",\n                \"isOn\": true\n            },\n            {\n                \"ui\": \"chat-invitation\",\n                \"steps\": [\n                    [\n                        {\n                            \"ui\": \"chat\"\n                        }\n                    ]\n                ],\n                \"isOn\": true\n            },\n            {\n                \"id\": \"menu\",\n                \"shortcut\": \"menu\",\n                \"ui\": \"menu\",\n                \"configurePage\": \"/dashboard/settings/menu\",\n                \"standAlone\": true,\n                \"isOn\": true\n            }\n        ]\n    ],\n    \"messages\": {}\n}','2019-11-21 20:40:41','2019-12-07 11:34:01','Etc/UTC','1574605883893.png','https://wiri.io'),(17,'app',NULL,'Marbella Tech Angels',NULL,NULL,68,NULL,'{\n    \"specialOpenHours\": {},\n    \"openHours\": [\n        [\n            \"10:00\",\n            \"18:00\"\n        ],\n        [\n            7,\n            24\n        ],\n        [\n            7,\n            24\n        ],\n        [\n            7,\n            24\n        ],\n        [\n            7,\n            24\n        ],\n        [\n            7,\n            24\n        ],\n        [\n            7,\n            24\n        ]\n    ],\n    \"wizardWidgetStep\": true,\n    \"menuDescription\": \"\",\n    \"menuList\": [],\n    \"messages\": {}\n}','2019-12-16 16:08:19','2019-12-16 16:10:11','Etc/UTC',NULL,'www.marbellatechangels.com'),(18,'app',NULL,'',NULL,NULL,70,NULL,'{\n    \"specialOpenHours\": {},\n    \"openHours\": [\n        [\n            7,\n            24\n        ],\n        [\n            7,\n            24\n        ],\n        [\n            7,\n            24\n        ],\n        [\n            7,\n            24\n        ],\n        [\n            7,\n            24\n        ],\n        [\n            7,\n            24\n        ],\n        [\n            7,\n            24\n        ]\n    ],\n    \"wizardWidgetStep\": true,\n    \"messages\": {\n        \"welcomeTitle\": \"\"\n    }\n}','2019-12-29 08:51:45','2019-12-30 08:14:14','Etc/UTC',NULL,NULL),(19,'app',NULL,'Paloma Blanca',NULL,NULL,67,NULL,'{\n    \"specialOpenHours\": {},\n    \"business\": {\n        \"name\": \"Paloma Blanca\",\n        \"address\": \"Urb. Cortijo Blanco, Calle Pablo Picasso 13,\",\n        \"city\": \" San Pedro de Alcantara\",\n        \"country\": \"Spain\",\n        \"latitude\": 36.4821943,\n        \"longitude\": -4.9764154,\n        \"email\": \"info@palomablancahotel.com\",\n        \"telephone\": \"+34 951 562 111\",\n        \"zip\": \"29670\",\n        \"state\": \"Malaga\",\n        \"directions\": \"Paloma Blanca is conveniently located along the main A7 dual carriageway, which links the city of Malaga to the east, Marbella, Puerto Banus and Estepona to the west.\",\n        \"currency\": \"EUR\"\n    },\n    \"wizardWidgetStep\": true,\n    \"styleVars\": {\n        \"theme-color\": \"#375390\",\n        \"theme-text-color\": \"#ffffff\",\n        \"theme-color-hover\": \"#5571ae\",\n        \"theme-color-active\": \"#193572\"\n    },\n    \"openHours\": [\n        [\n            \"13:00\",\n            \"22:30\"\n        ],\n        [\n            \"12:00\",\n            \"22:30\"\n        ],\n        [\n            \"12:00\",\n            \"22:30\"\n        ],\n        [\n            \"12:00\",\n            \"22:30\"\n        ],\n        [\n            \"12:00\",\n            \"22:30\"\n        ],\n        [\n            \"12:00\",\n            \"22:30\"\n        ],\n        [\n            \"12:00\",\n            \"22:30\"\n        ]\n    ],\n    \"menuDescription\": \"The Dora Maar restaurant is open for lunch and dinner with a range of creative and fresh food, with vegetarian, vegan and gluten-free dishes also available.\",\n    \"menuList\": [\n        {\n            \"name\": \"Lunch Menu\",\n            \"link\": \"https://wiri.io/user-files/resized/1582048304782.pdf\",\n            \"isOn\": true\n        },\n        {\n            \"name\": \"Kids Menu\",\n            \"link\": \"https://wiri.io/user-files/resized/1582048354668.pdf\",\n            \"isOn\": true\n        },\n        {\n            \"name\": \"Wine Menu\",\n            \"link\": \"https://wiri.io/user-files/resized/1582048380382.pdf\",\n            \"isOn\": true\n        },\n        {\n            \"name\": \"Sunday Roast Menu\",\n            \"link\": \"https://wiri.io/user-files/resized/1583351264748.pdf\",\n            \"isOn\": true\n        }\n    ],\n    \"wizardSnippetStep\": true,\n    \"bookings\": {\n        \"labels\": [\n            {\n                \"display\": \"Indoors\"\n            },\n            {\n                \"display\": \"Terrace\"\n            }\n        ],\n        \"duration\": 40\n    },\n    \"screens\": [\n        [\n            {\n                \"ui\": \"welcome\",\n                \"isOn\": true\n            },\n            {\n                \"ui\": \"chat-invitation\",\n                \"steps\": [\n                    [\n                        {\n                            \"ui\": \"chat\"\n                        }\n                    ]\n                ],\n                \"isOn\": true\n            },\n            {\n                \"ui\": \"booking-invitation\",\n                \"actionName\": \"booking-invitation\",\n                \"steps\": [\n                    {\n                        \"ui\": \"booking\"\n                    }\n                ],\n                \"action\": \"action-step\",\n                \"isOn\": true\n            },\n            {\n                \"ui\": \"open-hours\",\n                \"isOn\": true\n            },\n            {\n                \"id\": \"menu\",\n                \"shortcut\": \"menu\",\n                \"ui\": \"menu\",\n                \"configurePage\": \"/dashboard/settings/menu\",\n                \"standAlone\": true,\n                \"isOn\": true\n            },\n            {\n                \"id\": \"map\",\n                \"shortcut\": \"map-invitation\",\n                \"ui\": \"map-invitation\",\n                \"standAlone\": true,\n                \"isMacro\": false,\n                \"isOn\": true\n            }\n        ]\n    ],\n    \"mainBotQuestions\": [\n        {\n            \"question\": \"Can I see the menu?\",\n            \"answer\": \"Yes of course. Here you are.\",\n            \"ui\": \"menu\",\n            \"active\": false\n        },\n        {\n            \"question\": \"Where are you located?\",\n            \"answer\": \"Here are the details and map of our location.\",\n            \"ui\": \"map\",\n            \"active\": false\n        },\n        {\n            \"question\": \"What time is the restaurant open?\",\n            \"answer\": \"These are our opening hours\",\n            \"ui\": \"open-hours\",\n            \"active\": false\n        },\n        {\n            \"question\": \"I would like to place a table reservation\",\n            \"answer\": \"Great. Just fill in the details below and we\'ll take care of it.\",\n            \"ui\": \"booking\",\n            \"active\": false\n        }\n    ],\n    \"botName\": \"Paloma Blanca Bot\",\n    \"conversationScript\": [\n        \"How can we help you today?\",\n        {\n            \"ui\": \"options\",\n            \"options\": [\n                \"Can I see the menu?\",\n                \"Where are you located?\",\n                \"What time is the restaurant open?\",\n                \"I would like to place a table reservation\"\n            ],\n            \"name\": \"firstOptions\",\n            \"loop\": true\n        },\n        {\n            \"wait\": \"input\"\n        },\n        {\n            \"switch\": \"selected\",\n            \"cases\": {\n                \"Can I see the menu?\": [\n                    \"Yes of course. Here you are.\",\n                    {\n                        \"ui\": \"menu\"\n                    }\n                ],\n                \"Where are you located?\": [\n                    \"Here are the details and map of our location.\",\n                    {\n                        \"ui\": \"map\"\n                    }\n                ],\n                \"What time is the restaurant open?\": [\n                    \"These are our opening hours\",\n                    {\n                        \"ui\": \"open-hours\"\n                    }\n                ],\n                \"I would like to place a table reservation\": [\n                    \"Great. Just fill in the details below and we\'ll take care of it.\",\n                    {\n                        \"ui\": \"booking\"\n                    },\n                    {\n                        \"wait\": \"input\"\n                    }\n                ]\n            }\n        },\n        {\n            \"if\": \"conversation.previouslySelected.firstOptions![]=4&&!conversation.hasClientMessage\",\n            \"affirmative\": [\n                {\n                    \"ui\": \"options\",\n                    \"options\": [\n                        \"!@okShowMeMoreOptions\"\n                    ],\n                    \"optionsTitle\": \"\",\n                    \"kind\": \"small\"\n                },\n                {\n                    \"wait\": \"input\"\n                },\n                {\n                    \"conditional\": \"true\",\n                    \"goto\": \"firstOptions\",\n                    \"while\": \"!conversation.hasClientMessage\"\n                }\n            ]\n        },\n        {\n            \"if\": \"!conversation.hasClientMessage\",\n            \"affirmative\": [\n                \"!@anythingElseICanHelp\"\n            ]\n        }\n    ],\n    \"macros\": [\n        {\n            \"shortcut\": \"hi\",\n            \"message\": \"Hi. How can i help you?\"\n        }\n    ],\n    \"actionMacros\": [],\n    \"messages\": {\n        \"welcomeSubtitle\": \"Our restaurant Dora Mar is open. Book a table or let us know what you need. We are here to help you! \"\n    }\n}','2020-02-18 17:44:40','2020-03-04 20:55:26','Europe/Amsterdam','resized/1582048040971.png','https://www.palomablancahotel.com/'),(20,'app',NULL,'Hustle N´Flow',NULL,NULL,72,NULL,'{\n    \"specialOpenHours\": {},\n    \"business\": {\n        \"name\": \"Hustle N´Flow\",\n        \"address\": \"Calle Lagasca, Avda Andalucía #1\",\n        \"city\": \"San Pedro de Alcántara\",\n        \"country\": \"Spain\",\n        \"latitude\": 36.4829176,\n        \"longitude\": -4.989627899999999,\n        \"email\": \"hustle@hustlenflow.com\",\n        \"telephone\": \"+34603196737\",\n        \"state\": \"Malaga\"\n    },\n    \"wizardWidgetStep\": true,\n    \"styleVars\": {\n        \"theme-color\": \"#C59D5F\",\n        \"theme-text-color\": \"#000000\",\n        \"theme-color-hover\": \"#e3bb7d\",\n        \"theme-color-active\": \"#a77f41\"\n    },\n    \"position\": \"right\",\n    \"openHours\": [\n        [\n            \"9:00\",\n            \"16:00\"\n        ],\n        [],\n        [\n            \"9:00\",\n            \"16:30\"\n        ],\n        [\n            \"9:00\",\n            \"16:30\"\n        ],\n        [\n            \"9:00\",\n            \"16:30\"\n        ],\n        [\n            \"9:00\",\n            \"16:30\"\n        ],\n        [\n            \"9:00\",\n            \"16:00\"\n        ]\n    ],\n    \"menuTitle\": \"Check out our Menu?\",\n    \"menuDescription\": \"Sexiest menu of the coast!\",\n    \"menuList\": [\n        {\n            \"name\": \"Lunch Menu\",\n            \"link\": \"https://wiri.io/user-files/resized/1582988738050.pdf\",\n            \"isOn\": true\n        }\n    ],\n    \"wizardSnippetStep\": true,\n    \"screens\": [\n        [\n            {\n                \"ui\": \"welcome\",\n                \"isOn\": true\n            },\n            {\n                \"ui\": \"chat-invitation\",\n                \"steps\": [\n                    [\n                        {\n                            \"ui\": \"chat\"\n                        }\n                    ]\n                ],\n                \"isOn\": true\n            },\n            {\n                \"ui\": \"open-hours\",\n                \"isOn\": true\n            },\n            {\n                \"ui\": \"booking-invitation\",\n                \"actionName\": \"booking-invitation\",\n                \"steps\": [\n                    {\n                        \"ui\": \"booking\"\n                    }\n                ],\n                \"action\": \"action-step\",\n                \"isOn\": true\n            },\n            {\n                \"id\": \"menu\",\n                \"shortcut\": \"menu\",\n                \"ui\": \"menu\",\n                \"configurePage\": \"/dashboard/settings/menu\",\n                \"standAlone\": true,\n                \"isOn\": true\n            },\n            {\n                \"id\": \"map\",\n                \"shortcut\": \"map-invitation\",\n                \"ui\": \"map-invitation\",\n                \"standAlone\": true,\n                \"isMacro\": false,\n                \"isOn\": true\n            }\n        ]\n    ],\n    \"messages\": {\n        \"welcomeTitle\": \"Welcome Hustlers!\"\n    }\n}','2020-02-29 15:00:36','2020-03-03 17:52:23','Etc/UTC','resized/1582988500844.jpeg','https://hustlenflow.com'),(21,'app',NULL,'',NULL,NULL,74,NULL,'{\n    \"specialOpenHours\": {},\n    \"messages\": {}\n}','2020-09-25 05:41:52','2020-09-25 05:41:59','Etc/UTC',NULL,NULL);
/*!40000 ALTER TABLE `assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bookings`
--

DROP TABLE IF EXISTS `bookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appId` int(11) DEFAULT NULL,
  `interactionId` bigint(20) DEFAULT NULL,
  `messageId` bigint(20) DEFAULT NULL,
  `publicId` bigint(20) DEFAULT NULL,
  `identityId` bigint(20) DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `time` varchar(11) DEFAULT NULL,
  `label` varchar(32) DEFAULT NULL,
  `value` float DEFAULT NULL,
  `price` float DEFAULT NULL,
  `notes` text,
  `status` varchar(12) DEFAULT 'PENDING',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `promotion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=253 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookings`
--
-- WHERE:  1 limit 20

LOCK TABLES `bookings` WRITE;
/*!40000 ALTER TABLE `bookings` DISABLE KEYS */;
INSERT INTO `bookings` VALUES (51,1,397,NULL,59,1,'2019-05-10 02:15:00','2019-05-10 02:55:00','4:15','Bar',2,NULL,NULL,'CANCELLED','2019-05-10 04:59:14','2019-05-11 21:13:23',NULL),(52,1,398,NULL,59,1,'2019-05-10 08:00:31','2019-05-10 08:40:31','10:00','VIP',2,NULL,NULL,'PENDING','2019-05-10 10:16:40','2019-05-10 10:16:40',NULL),(53,1,399,NULL,59,1,'2019-05-11 03:30:00','2019-05-11 04:10:00','5:30','VIP',2,NULL,NULL,'PENDING','2019-05-10 10:17:38','2019-05-10 10:17:38',NULL),(54,1,400,NULL,59,1,'2019-05-11 03:00:00','2019-05-11 03:40:00','5:00','VIP',2,NULL,NULL,'PENDING','2019-05-10 10:18:03','2019-05-15 17:32:52',NULL),(55,1,401,NULL,59,1,'2019-05-11 03:00:00','2019-05-11 03:40:00','5:00','VIP',2,NULL,NULL,'PENDING','2019-05-10 10:22:13','2019-05-10 10:22:13',NULL),(56,1,402,NULL,59,1,'2019-05-11 03:15:00','2019-05-11 03:55:00','5:15','Bar',2,NULL,NULL,'PENDING','2019-05-10 10:22:26','2019-05-10 10:22:26',NULL),(57,1,403,NULL,59,1,'2019-05-11 04:00:00','2019-05-11 04:40:00','6:00','VIP',2,NULL,NULL,'NOSHOW','2019-05-10 10:22:43','2019-05-10 10:22:43',NULL),(58,1,404,NULL,59,1,'2019-05-11 04:45:00','2019-05-11 05:25:00','6:45','VIP',2,NULL,NULL,'PENDING','2019-05-10 10:22:54','2019-05-17 11:16:38',NULL),(59,1,405,NULL,59,1,'2019-05-11 05:45:00','2019-05-11 06:25:00','7:45','Bar',2,NULL,NULL,'PENDING','2019-05-10 10:23:08','2019-05-10 10:23:08',NULL),(60,1,406,NULL,59,1,'2019-05-11 05:45:00','2019-05-11 06:25:00','7:45','Main room',2,NULL,NULL,'PENDING','2019-05-10 10:23:20','2019-05-10 10:23:20',NULL),(61,1,407,NULL,59,1,'2019-05-11 06:30:00','2019-05-11 07:00:00','8:30','Bar',2,NULL,NULL,'PENDING','2019-05-10 10:23:39','2019-05-10 10:23:39',NULL),(62,1,408,NULL,59,1,'2019-05-11 07:00:00','2019-05-11 08:00:00','9:00','Terrace',2,NULL,NULL,'PENDING','2019-05-10 10:23:51','2019-05-10 10:23:51',NULL),(63,1,409,NULL,59,1,'2019-05-10 08:00:29','2019-05-10 08:40:29','10:00','Bar',2,NULL,NULL,'PENDING','2019-05-10 10:59:52','2019-05-10 10:59:52',NULL),(64,1,410,NULL,59,1,'2019-05-22 02:30:00','2019-05-22 03:10:00','4:30','Bar',2,NULL,NULL,'PENDING','2019-05-12 08:39:54','2019-05-12 08:39:54',NULL),(65,1,NULL,NULL,59,44,'2019-05-13 23:00:00','2019-05-13 23:40:00','1:00','Bar',2,NULL,NULL,'CANCELLED','2019-05-12 09:00:50','2019-05-15 17:42:41',NULL),(66,1,NULL,NULL,59,1,'2019-05-20 23:00:00','2019-05-20 23:40:00','1:00','Bar',2,NULL,NULL,'PENDING','2019-05-12 09:12:02','2019-05-12 09:12:02',NULL),(67,1,NULL,NULL,59,1,'2019-05-20 23:00:00','2019-05-20 23:40:00','1:00','Bar',2,NULL,NULL,'PENDING','2019-05-12 09:12:47','2019-05-12 09:12:47',NULL),(68,1,NULL,NULL,59,1,'2019-05-21 22:00:00','2019-05-21 22:40:00','0:00','Bar',2,NULL,NULL,'PENDING','2019-05-12 09:12:59','2019-05-12 09:12:59',NULL),(69,1,NULL,NULL,59,1,'2019-05-09 00:00:00','2019-05-09 00:40:00','2:00','Bar',2,NULL,NULL,'PENDING','2019-05-12 09:17:48','2019-05-12 09:17:48',NULL),(70,1,NULL,NULL,59,1,'2019-05-09 00:00:00','2019-05-09 00:40:00','2:00','Bar',2,NULL,NULL,'PENDING','2019-05-12 09:18:23','2019-05-12 09:18:23',NULL);
/*!40000 ALTER TABLE `bookings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `identities`
--

DROP TABLE IF EXISTS `identities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `identities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appId` int(11) DEFAULT NULL,
  `publicId` bigint(20) DEFAULT NULL,
  `email` varchar(120) DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  `firstName` varchar(40) DEFAULT NULL,
  `lastName` varchar(40) DEFAULT NULL,
  `company` varchar(30) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `validated` tinyint(1) DEFAULT '0',
  `password` varchar(50) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `notes` text,
  `status` varchar(100) DEFAULT NULL,
  `createdBy` varchar(40) DEFAULT NULL,
  `preferredContactMethod` enum('email','phone','facebook','google') DEFAULT 'email',
  `stripeId` varchar(30) DEFAULT NULL,
  `consent` tinyint(1) DEFAULT NULL,
  `extendedConsent` tinyint(1) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `appId` (`appId`,`publicId`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `identities`
--
-- WHERE:  1 limit 20

LOCK TABLES `identities` WRITE;
/*!40000 ALTER TABLE `identities` DISABLE KEYS */;
INSERT INTO `identities` VALUES (1,1,59,'kurt@test.com','Kurt Russel',NULL,NULL,NULL,'7447643',0,NULL,NULL,'just like in LA dcjnjdndc',NULL,NULL,'email',NULL,1,1,'2019-04-20 12:59:25','2019-05-21 17:34:04'),(32,1,62,'mbeen@gmail.com','Maximiliano Benedetto',NULL,NULL,NULL,'415',0,NULL,NULL,NULL,NULL,NULL,'email',NULL,1,0,'2019-05-12 10:37:59','2019-05-12 10:42:13'),(35,1,62,'dcjndj@nj.com','Maximiliano Benedetto',NULL,NULL,NULL,'415232323',0,NULL,NULL,NULL,NULL,NULL,'email',NULL,1,0,'2019-05-12 10:42:35','2019-05-12 10:42:35'),(36,1,49,'noelialilli@gmail.com','Noelia Lilli',NULL,NULL,NULL,'726337',0,NULL,NULL,'no sushi cold or jskdsahzczcz',NULL,NULL,'email',NULL,NULL,NULL,'2019-05-12 17:55:39','2019-08-25 16:43:54'),(37,1,85,'infoprisma1@gmail.com','Maximiliano Benedetto',NULL,NULL,NULL,'78343486',0,NULL,NULL,NULL,NULL,NULL,'email',NULL,1,0,'2019-05-18 04:23:00','2019-05-19 21:31:00'),(43,1,85,'maxi@test.com','Maximiliano Benedetto',NULL,NULL,NULL,'78343486',0,NULL,NULL,NULL,NULL,NULL,'email',NULL,1,0,'2019-05-18 04:23:00','2019-05-19 21:59:45'),(44,1,85,'chilo@ter.com','Maximiliano Benedetto',NULL,NULL,NULL,'78343486',0,NULL,NULL,NULL,NULL,NULL,'email',NULL,1,0,'2019-05-18 04:23:00','2019-05-19 22:02:44'),(45,1,85,'chuvo@uber.com','Maximiliano Benedetto',NULL,NULL,NULL,'78343486',0,NULL,NULL,NULL,NULL,NULL,'email',NULL,1,0,'2019-05-18 04:23:00','2019-05-19 22:05:51'),(46,1,86,'urei@ubc.com','maxi',NULL,NULL,NULL,'48434',0,NULL,NULL,'',NULL,NULL,'email',NULL,1,0,'2019-05-19 22:21:48','2019-08-25 16:53:03'),(47,NULL,NULL,'uyu@uber.com','heor',NULL,NULL,NULL,'88789',0,NULL,NULL,NULL,NULL,NULL,'email',NULL,NULL,NULL,'2019-05-20 00:23:32','2019-05-20 00:23:32'),(48,NULL,NULL,'uyu@uber.com','heor',NULL,NULL,NULL,'88789',0,NULL,NULL,NULL,NULL,NULL,'email',NULL,NULL,NULL,'2019-05-20 00:24:24','2019-05-20 00:24:24'),(49,NULL,NULL,'dede@jcd.com','uiuq',NULL,NULL,NULL,'222',0,NULL,NULL,NULL,NULL,NULL,'email',NULL,NULL,NULL,'2019-05-20 00:49:56','2019-05-20 00:49:56'),(50,NULL,NULL,'dedeed','eded',NULL,NULL,NULL,'edde',0,NULL,NULL,NULL,NULL,NULL,'email',NULL,NULL,NULL,'2019-05-20 00:51:56','2019-05-20 00:51:56'),(51,NULL,NULL,'xssx','sx',NULL,NULL,NULL,'sxsx',0,NULL,NULL,NULL,NULL,NULL,'email',NULL,NULL,NULL,'2019-05-20 00:53:49','2019-05-20 00:53:49'),(52,NULL,NULL,'xsxsx','xdx',NULL,NULL,NULL,'ddc',0,NULL,NULL,NULL,NULL,NULL,'email',NULL,NULL,NULL,'2019-05-20 00:54:02','2019-05-20 00:54:02'),(53,NULL,NULL,'infoprisma1@gmail.com','Maximiliano Benedetto',NULL,NULL,NULL,'4159098542',0,NULL,NULL,NULL,NULL,NULL,'email',NULL,NULL,NULL,'2019-05-20 00:59:03','2019-05-20 00:59:03'),(54,NULL,NULL,'infoprisma1@gmail.com','eded',NULL,NULL,NULL,'4159098542',0,NULL,NULL,NULL,NULL,NULL,'email',NULL,NULL,NULL,'2019-05-20 01:20:08','2019-05-20 01:20:08'),(55,NULL,NULL,'infoprisma1@gmail.com','Maximiliano Benedetto',NULL,NULL,NULL,'4159098542',0,NULL,NULL,NULL,NULL,NULL,'email',NULL,NULL,NULL,'2019-05-20 01:23:19','2019-05-20 01:23:19'),(56,NULL,NULL,'infoprisma1@gmail.com','Maximiliano Benedetto',NULL,NULL,NULL,'4159098542',0,NULL,NULL,NULL,NULL,NULL,'email',NULL,NULL,NULL,'2019-05-20 01:24:09','2019-05-20 01:24:09'),(57,NULL,NULL,'infoprisma1@gmail.com','Maximiliano Benedetto',NULL,NULL,NULL,'4159098542',0,NULL,NULL,NULL,NULL,NULL,'email',NULL,NULL,NULL,'2019-05-20 01:24:49','2019-05-20 01:24:49');
/*!40000 ALTER TABLE `identities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interactions`
--

DROP TABLE IF EXISTS `interactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interactions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent` bigint(20) DEFAULT NULL,
  `appId` int(11) DEFAULT NULL,
  `campaignId` int(11) DEFAULT NULL,
  `lang` varchar(5) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `scale` int(11) DEFAULT NULL,
  `user` bigint(20) DEFAULT NULL,
  `userType` varchar(15) DEFAULT NULL,
  `appUser` varchar(35) DEFAULT NULL,
  `appUserType` varchar(20) DEFAULT NULL,
  `type` varchar(15) DEFAULT NULL,
  `trigger` varchar(20) DEFAULT NULL,
  `object` varchar(50) DEFAULT NULL,
  `hasMessage` tinyint(1) DEFAULT '0',
  `claimedBy` int(11) DEFAULT NULL,
  `pendingChat` tinyint(1) DEFAULT '0',
  `message` varchar(255) DEFAULT NULL,
  `messageOwner` varchar(15) DEFAULT NULL,
  `details` text,
  `lastSeenByOperator` bigint(11) DEFAULT NULL,
  `lastSeenByUser` bigint(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `session` bigint(20) DEFAULT NULL,
  `lastMessageId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=728 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interactions`
--
-- WHERE:  1 limit 20

LOCK TABLES `interactions` WRITE;
/*!40000 ALTER TABLE `interactions` DISABLE KEYS */;
INSERT INTO `interactions` VALUES (1,NULL,1,1,NULL,NULL,NULL,51,NULL,NULL,NULL,'chat',NULL,NULL,1,NULL,0,'deded',NULL,NULL,NULL,NULL,'2019-03-10 20:02:26','2019-03-10 20:02:52',NULL,NULL),(2,NULL,1,1,NULL,NULL,NULL,51,NULL,NULL,NULL,'chat',NULL,NULL,1,NULL,0,'e',NULL,NULL,NULL,NULL,'2019-03-10 20:04:37','2019-03-13 05:55:13',NULL,NULL),(3,NULL,1,1,NULL,2,NULL,51,NULL,NULL,NULL,'chat',NULL,NULL,1,NULL,0,'Operator showed location',NULL,NULL,NULL,NULL,'2019-03-10 20:05:35','2019-03-21 03:41:29',NULL,NULL),(4,NULL,1,1,NULL,NULL,NULL,51,NULL,NULL,NULL,'chat',NULL,NULL,1,NULL,0,'eddeed',NULL,NULL,NULL,NULL,'2019-03-10 20:06:42','2019-03-10 20:06:50',NULL,NULL),(5,NULL,1,1,NULL,NULL,NULL,51,NULL,NULL,NULL,'chat',NULL,NULL,1,NULL,0,'frrf',NULL,NULL,NULL,NULL,'2019-03-10 20:07:02','2019-03-10 20:07:09',NULL,NULL),(6,NULL,1,1,NULL,NULL,NULL,51,NULL,NULL,NULL,'chat',NULL,NULL,1,NULL,0,'no',NULL,NULL,NULL,NULL,'2019-03-10 20:08:46','2019-03-10 20:11:24',NULL,NULL),(7,NULL,1,1,'en',NULL,NULL,51,NULL,NULL,NULL,'chat',NULL,NULL,1,NULL,0,'con lang',NULL,NULL,NULL,NULL,'2019-03-10 20:14:22','2019-03-10 20:14:22',NULL,NULL),(8,NULL,1,2,'en',NULL,NULL,51,NULL,NULL,NULL,'chat',NULL,NULL,1,NULL,0,'deed',NULL,NULL,NULL,NULL,'2019-03-11 04:57:13','2019-03-11 08:43:07',NULL,NULL),(9,NULL,1,2,'en',NULL,NULL,53,NULL,NULL,NULL,'chat',NULL,NULL,1,NULL,0,'er2',NULL,NULL,NULL,NULL,'2019-03-11 05:00:16','2019-03-11 05:00:16',NULL,NULL),(10,NULL,1,2,'en',NULL,NULL,54,NULL,NULL,NULL,'chat',NULL,NULL,1,NULL,0,'zzz',NULL,NULL,NULL,NULL,'2019-03-12 17:29:02','2019-03-13 04:14:35',NULL,NULL),(11,NULL,1,2,'en',3,NULL,51,NULL,NULL,NULL,'chat',NULL,NULL,1,NULL,0,'Operator asked contact information',NULL,NULL,NULL,NULL,'2019-03-13 06:09:31','2019-03-18 06:37:14',NULL,NULL),(12,NULL,1,2,'en',2,5,51,NULL,NULL,NULL,'rate',NULL,NULL,0,NULL,0,NULL,NULL,NULL,NULL,NULL,'2019-03-16 21:03:18','2019-03-16 21:03:18',NULL,NULL),(13,NULL,1,2,'en',2,5,51,NULL,NULL,NULL,'rate',NULL,NULL,0,NULL,0,NULL,NULL,NULL,NULL,NULL,'2019-03-16 21:04:03','2019-03-16 21:04:16',NULL,NULL),(14,NULL,1,2,'en',2,5,51,NULL,NULL,NULL,'rate',NULL,NULL,0,NULL,0,NULL,NULL,NULL,NULL,NULL,'2019-03-16 21:04:39','2019-03-16 21:04:47',NULL,NULL),(15,NULL,1,2,'en',2,5,51,NULL,NULL,NULL,'rate',NULL,NULL,0,NULL,0,NULL,NULL,NULL,NULL,NULL,'2019-03-16 21:05:31','2019-03-16 21:05:31',NULL,NULL),(16,NULL,1,2,'en',2,5,51,NULL,NULL,NULL,'rate',NULL,NULL,0,NULL,0,NULL,NULL,NULL,NULL,NULL,'2019-03-16 21:12:07','2019-03-16 21:14:24',NULL,NULL),(17,NULL,1,2,'en',4,5,51,NULL,NULL,NULL,'rate',NULL,NULL,0,NULL,0,NULL,NULL,NULL,NULL,NULL,'2019-03-16 21:15:01','2019-03-16 21:15:01',NULL,NULL),(18,NULL,1,2,'en',3,5,51,NULL,NULL,NULL,'rate',NULL,NULL,0,NULL,0,NULL,NULL,NULL,NULL,NULL,'2019-03-16 21:15:42','2019-03-16 21:15:42',NULL,NULL),(19,NULL,1,2,'en',3,5,51,NULL,NULL,NULL,'rate',NULL,NULL,0,NULL,0,NULL,NULL,NULL,NULL,NULL,'2019-03-16 21:21:39','2019-03-16 21:21:39',NULL,NULL),(20,NULL,1,2,'en',2,5,51,NULL,NULL,NULL,'rate',NULL,NULL,0,NULL,0,NULL,NULL,NULL,NULL,NULL,'2019-03-16 21:25:09','2019-03-16 21:25:09',NULL,NULL);
/*!40000 ALTER TABLE `interactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ip2location_db3`
--

DROP TABLE IF EXISTS `ip2location_db3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ip2location_db3` (
  `ip_from` int(10) unsigned DEFAULT NULL,
  `ip_to` int(10) unsigned DEFAULT NULL,
  `country_code` char(2) COLLATE utf8_bin DEFAULT NULL,
  `country_name` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `region_name` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `city_name` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  KEY `idx_ip_from` (`ip_from`),
  KEY `idx_ip_to` (`ip_to`),
  KEY `idx_ip_from_to` (`ip_from`,`ip_to`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ip2location_db3`
--
-- WHERE:  1 limit 20

LOCK TABLES `ip2location_db3` WRITE;
/*!40000 ALTER TABLE `ip2location_db3` DISABLE KEYS */;
INSERT INTO `ip2location_db3` VALUES (16793600,16797695,'JP','Japan','Hiroshima','Hiroshima\r'),(16785408,16793599,'CN','China','Guangdong','Guangzhou\r'),(16781312,16785407,'JP','Japan','Tokyo','Tokyo\r'),(16779264,16781311,'CN','China','Guangdong','Guangzhou\r'),(16778240,16779263,'AU','Australia','Victoria','Melbourne\r'),(16777472,16778239,'CN','China','Fujian','Fuzhou\r'),(16777216,16777471,'US','United States','California','Los Angeles\r'),(0,16777215,'-','-','-','-\r'),(16797696,16797951,'JP','Japan','Okayama','Okayama\r'),(16797952,16798207,'JP','Japan','Tottori','Kurayoshi\r'),(16798208,16798719,'JP','Japan','Okayama','Okayama\r'),(16798720,16798975,'JP','Japan','Tottori','Tottori\r'),(16798976,16799231,'JP','Japan','Shimane','Izumo\r'),(16799232,16800511,'JP','Japan','Okayama','Okayama\r'),(16800512,16800767,'JP','Japan','Hiroshima','Hiroshima\r'),(16800768,16801023,'JP','Japan','Tottori','Kurayoshi\r'),(16801024,16801279,'JP','Japan','Okayama','Okayama\r'),(16801280,16801535,'JP','Japan','Tottori','Yonago\r'),(16801536,16802047,'JP','Japan','Okayama','Okayama\r'),(16802048,16802303,'JP','Japan','Tottori','Tottori\r');
/*!40000 ALTER TABLE `ip2location_db3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `interactionId` bigint(20) DEFAULT NULL,
  `owner` varchar(15) DEFAULT NULL,
  `message` text,
  `data` text,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `value` float DEFAULT NULL,
  `question` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3290 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--
-- WHERE:  1 limit 20

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,118,'client','rr',NULL,'2019-03-30 09:10:47','2019-03-30 09:10:47',NULL,NULL),(2,118,'client','qq',NULL,'2019-03-30 09:10:56','2019-03-30 09:10:56',NULL,NULL),(3,118,'client','e',NULL,'2019-03-30 09:14:28','2019-03-30 09:14:28',NULL,NULL),(4,118,'client','er',NULL,'2019-03-30 09:14:30','2019-03-30 09:14:30',NULL,NULL),(5,118,'14','eff',NULL,'2019-03-30 09:14:34','2019-03-30 09:14:34',NULL,NULL),(6,118,'client','rffr',NULL,'2019-03-30 09:14:37','2019-03-30 09:14:37',NULL,NULL),(7,118,'14','rfr',NULL,'2019-03-30 09:14:41','2019-03-30 09:14:41',NULL,NULL),(8,118,'14','{\"ui\":\"open-hours\",\"display\":\"Operator informed opening hours\",\"config\":{\"title\":\"Open hours\",\"hours\":[[\"10\",\"23.30\"],[],[\"10\",\"15\",\"19\",\"23\"],[\"10\",\"15\",\"19\",\"23\"],[\"10\",\"15\",\"19\",\"23\"],[\"10\",\"15\",\"19\",\"23\"],[\"10\",\"23.30\"]]},\"prev\":\"\",\"params\":null}',NULL,'2019-03-30 09:14:47','2019-03-30 09:14:47',NULL,NULL),(9,118,'client','ed',NULL,'2019-03-30 09:15:56','2019-03-30 09:15:56',NULL,NULL),(10,118,'14','{\"ui\":\"options\",\"display\":\"Operator provided options\",\"config\":{\"options\":[\"Yes\",\"No\"]},\"prev\":\"\",\"params\":[\"si\",\"no\"]}','{\"selected\":\"si\"}','2019-03-30 09:16:07','2019-03-30 09:16:13',NULL,NULL),(11,118,'14','{\"ui\":\"options\",\"display\":\"Operator provided options\",\"config\":{\"options\":[\"Yes\",\"No\"]},\"prev\":\"\",\"params\":[\"si\",\"noq\"]}','{\"selected\":\"si\"}','2019-03-30 09:17:36','2019-03-30 09:35:25',NULL,NULL),(12,118,'14','{\"ui\":\"options\",\"display\":\"Operator provided options\",\"config\":{\"options\":[\"Yes\",\"No\"]},\"prev\":\"\",\"params\":[\"ddd\",\"sssss\"]}','{\"selected\":\"sssss\"}','2019-03-30 09:38:11','2019-03-30 09:38:20',NULL,NULL),(13,118,'14','{\"ui\":\"options\",\"display\":\"Operator provided options\",\"config\":{\"options\":[\"Yes\",\"No\"]},\"prev\":\"\",\"params\":[\"ssss\",\"aaa\"]}','{\"selected\":\"aaa\"}','2019-03-30 09:39:03','2019-03-30 09:40:23',NULL,NULL),(14,118,'14','{\"ui\":\"rate\",\"display\":\"Operator asked your satisfaction level\",\"prev\":\"\",\"params\":[\"dknkcd\"]}','{\"value\":3}','2019-03-30 09:41:56','2019-03-30 09:42:00',NULL,NULL),(15,118,'14','{\"ui\":\"options\",\"display\":\"Operator provided options\",\"config\":{\"options\":[\"Yes\",\"No\"]},\"prev\":\"\",\"params\":[\"njn\",\"jnjsnsx\"]}','{\"selected\":\"jnjsnsx\"}','2019-03-30 09:42:10','2019-03-30 09:42:30',NULL,NULL),(16,118,'14','{\"ui\":\"rate\",\"display\":\"Operator asked your satisfaction level\",\"prev\":\"\",\"params\":[\"njsx\",\"xsmx\"]}','{\"value\":4}','2019-03-30 09:42:44','2019-03-30 09:42:59',NULL,NULL),(17,118,'14','ii',NULL,'2019-03-30 09:43:03','2019-03-30 09:43:03',NULL,NULL),(18,118,'14','{\"ui\":\"options\",\"display\":\"Operator provided options\",\"config\":{\"options\":[\"Yes\",\"No\"]},\"prev\":\"\",\"params\":[\"nj\",\"jnxs\"]}','{\"selected\":\"nj\"}','2019-03-30 09:43:34','2019-03-30 09:47:23',NULL,NULL),(19,118,'14','{\"ui\":\"options\",\"display\":\"Operator provided options\",\"config\":{\"options\":[\"Yes\",\"No\"]},\"prev\":\"\",\"params\":[\"loplo\"]}','{\"selected\":\"loplo\"}','2019-03-30 09:49:58','2019-03-30 09:50:13',NULL,NULL),(20,118,'14','{\"ui\":\"options\",\"display\":\"Operator provided options\",\"config\":{\"options\":[\"Yes\",\"No\"]},\"prev\":\"\",\"params\":[\"jui\",\"komo\"]}','{\"selected\":\"jui\"}','2019-03-30 09:51:54','2019-03-30 09:52:00',NULL,NULL);
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` varchar(20) DEFAULT NULL,
  `appId` int(11) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `value` text,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--
-- WHERE:  1 limit 20

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` VALUES (13,'submitBooking',12,'email','jsteneros@gmail.com','2019-12-24 03:39:33','2019-12-24 03:39:33'),(14,'message',12,'email','infoprisma1@gmail.com','2019-12-24 03:39:33','2019-12-24 03:39:33'),(15,'submitBooking',11,'email','cats@lacatarina.beer, florin@lacatarina.beer','2019-12-24 08:43:00','2019-12-24 08:43:00'),(16,'message',11,'email','florin@lacatarina.beer','2019-12-24 08:43:00','2019-12-24 08:43:00'),(19,'submitBooking',20,'email','checkboxproductions@gmail.com','2020-02-29 15:22:19','2020-02-29 15:22:19'),(20,'message',20,'email','checkboxproductions@gmail.com','2020-02-29 15:22:19','2020-02-29 15:22:19'),(21,'submitBooking',19,'email','managerpalomablanca@gmail.com, reservas@palomablancahotel.es','2020-03-04 19:48:28','2020-03-04 19:48:28'),(22,'message',19,'email','managerpalomablanca@gmail.com, reservas@palomablancahotel.es','2020-03-04 19:48:28','2020-03-04 19:48:28');
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `token` varchar(40) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--
-- WHERE:  1 limit 20

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` VALUES (1,14,3499,'test payment','ch_1DOz3lHUX9pHC2VdUZWhwIGQ',NULL,'2018-10-25 02:58:52','2018-10-25 02:58:52'),(2,14,3499,'test payment','ch_1DOzIuHUX9pHC2VdlOqQhe7C',NULL,'2018-10-25 03:14:37','2018-10-25 03:14:37'),(3,14,3499,'test payment','ch_1DOzksHUX9pHC2VdEKSe5vpd',NULL,'2018-10-25 03:43:31','2018-10-25 03:43:31');
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `public-clients`
--

DROP TABLE IF EXISTS `public-clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `public-clients` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(30) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `validated` tinyint(1) DEFAULT '0',
  `password` varchar(50) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `notes` text,
  `status` varchar(100) DEFAULT NULL,
  `createdBy` varchar(40) DEFAULT NULL,
  `preferredContactMethod` enum('email','phone','facebook','google') DEFAULT 'email',
  `stripeId` varchar(30) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9259 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `public-clients`
--
-- WHERE:  1 limit 20

LOCK TABLES `public-clients` WRITE;
/*!40000 ALTER TABLE `public-clients` DISABLE KEYS */;
INSERT INTO `public-clients` VALUES (1,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email',NULL,'2019-02-11 06:18:41','2019-02-11 06:18:41'),(2,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email',NULL,'2019-02-11 06:24:27','2019-02-11 06:24:27'),(3,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email',NULL,'2019-02-11 06:36:41','2019-02-11 06:36:41'),(4,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email',NULL,'2019-02-12 04:59:33','2019-02-12 04:59:33'),(5,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email',NULL,'2019-02-12 05:01:16','2019-02-12 05:01:16'),(6,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email',NULL,'2019-02-12 05:04:18','2019-02-12 05:04:18'),(7,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email',NULL,'2019-02-12 05:17:03','2019-02-12 05:17:03'),(8,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email',NULL,'2019-02-12 05:19:04','2019-02-12 05:19:04'),(9,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email',NULL,'2019-02-12 05:19:16','2019-02-12 05:19:16'),(10,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email',NULL,'2019-02-12 05:19:22','2019-02-12 05:19:22'),(11,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email',NULL,'2019-02-12 05:19:47','2019-02-12 05:19:47'),(12,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email',NULL,'2019-02-12 05:21:16','2019-02-12 05:21:16'),(13,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email',NULL,'2019-02-12 05:22:00','2019-02-12 05:22:00'),(14,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email',NULL,'2019-02-12 05:22:18','2019-02-12 05:22:18'),(15,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email',NULL,'2019-02-12 05:22:23','2019-02-12 05:22:23'),(16,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email',NULL,'2019-02-12 05:22:58','2019-02-12 05:22:58'),(17,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email',NULL,'2019-02-12 05:23:45','2019-02-12 05:23:45'),(18,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email',NULL,'2019-02-12 05:24:06','2019-02-12 05:24:06'),(19,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email',NULL,'2019-02-12 05:55:44','2019-02-12 05:55:44'),(20,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email',NULL,'2019-02-12 05:56:36','2019-02-12 05:56:36');
/*!40000 ALTER TABLE `public-clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `segment-customers`
--

DROP TABLE IF EXISTS `segment-customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `segment-customers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `segmentId` int(11) DEFAULT NULL,
  `publicId` bigint(20) DEFAULT NULL,
  `appId` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `segmentUserApp` (`segmentId`,`appId`,`publicId`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `segment-customers`
--
-- WHERE:  1 limit 20

LOCK TABLES `segment-customers` WRITE;
/*!40000 ALTER TABLE `segment-customers` DISABLE KEYS */;
INSERT INTO `segment-customers` VALUES (1,1,59,1,'2019-05-12 16:28:38','2019-05-12 16:28:38'),(2,2,62,1,'2019-05-12 16:28:50','2019-05-12 16:28:50'),(4,1,62,1,'2019-05-17 22:33:18','2019-05-17 22:33:18'),(23,1,49,1,'2019-05-18 23:14:10','2019-05-18 23:14:10'),(27,1,61,1,'2019-05-18 23:27:24','2019-05-18 23:27:24'),(35,1,NULL,1,'2019-05-20 05:06:34','2019-05-20 05:06:34'),(36,2,NULL,1,'2019-05-20 05:06:36','2019-05-20 05:06:36'),(37,2,NULL,1,'2019-05-20 05:07:03','2019-05-20 05:07:03'),(41,1,85,1,'2019-05-21 17:19:28','2019-05-21 17:19:28'),(43,1,88,1,'2019-05-26 07:04:02','2019-05-26 07:04:02'),(44,2,88,1,'2019-05-28 17:11:38','2019-05-28 17:11:38'),(45,1,89,1,'2019-06-04 06:17:43','2019-06-04 06:17:43'),(53,1,90,1,'2019-06-06 03:13:43','2019-06-06 03:13:43'),(75,2,90,1,'2019-06-06 03:28:32','2019-06-06 03:28:32'),(79,NULL,90,1,'2019-06-06 07:18:15','2019-06-06 07:18:15'),(80,NULL,90,1,'2019-06-06 07:19:02','2019-06-06 07:19:02'),(81,NULL,90,1,'2019-06-06 07:19:14','2019-06-06 07:19:14'),(86,10,90,1,'2019-06-06 07:27:46','2019-06-06 07:27:46'),(91,2,96,1,'2019-07-30 00:02:05','2019-07-30 00:02:05'),(92,11,94,1,'2019-07-30 00:19:32','2019-07-30 00:19:32');
/*!40000 ALTER TABLE `segment-customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `segment-interactions`
--

DROP TABLE IF EXISTS `segment-interactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `segment-interactions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `segmentId` int(11) DEFAULT NULL,
  `interactionId` bigint(20) DEFAULT NULL,
  `appId` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `segmentInteractionApp` (`segmentId`,`interactionId`,`appId`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `segment-interactions`
--
-- WHERE:  1 limit 20

LOCK TABLES `segment-interactions` WRITE;
/*!40000 ALTER TABLE `segment-interactions` DISABLE KEYS */;
INSERT INTO `segment-interactions` VALUES (1,5,472,1,'2019-06-03 22:53:59','2019-06-03 22:53:59'),(2,NULL,NULL,1,'2019-06-06 03:43:13','2019-06-06 03:43:13'),(3,NULL,NULL,1,'2019-06-06 03:43:37','2019-06-06 03:43:37'),(4,5,NULL,1,'2019-06-06 03:44:33','2019-06-06 03:44:33'),(5,5,NULL,1,'2019-06-06 03:45:04','2019-06-06 03:45:04'),(6,5,NULL,1,'2019-06-06 03:46:30','2019-06-06 03:46:30'),(7,5,NULL,1,'2019-06-06 03:46:35','2019-06-06 03:46:35'),(8,6,NULL,1,'2019-06-06 03:46:37','2019-06-06 03:46:37'),(9,5,NULL,1,'2019-06-06 03:46:39','2019-06-06 03:46:39'),(10,6,NULL,1,'2019-06-06 03:46:42','2019-06-06 03:46:42'),(11,5,NULL,1,'2019-06-06 03:48:45','2019-06-06 03:48:45'),(12,5,NULL,1,'2019-06-06 03:49:24','2019-06-06 03:49:24'),(13,5,NULL,1,'2019-06-06 03:49:56','2019-06-06 03:49:56'),(14,5,NULL,1,'2019-06-06 03:50:06','2019-06-06 03:50:06'),(15,6,NULL,1,'2019-06-06 03:50:09','2019-06-06 03:50:09'),(16,5,NULL,1,'2019-06-06 03:51:12','2019-06-06 03:51:12'),(20,6,473,1,'2019-06-06 03:56:07','2019-06-06 03:56:07'),(21,6,456,1,'2019-06-06 03:56:14','2019-06-06 03:56:14'),(22,5,456,1,'2019-06-06 03:56:19','2019-06-06 03:56:19'),(25,13,473,1,'2019-06-06 07:36:51','2019-06-06 07:36:51');
/*!40000 ALTER TABLE `segment-interactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `segments`
--

DROP TABLE IF EXISTS `segments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `segments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appId` int(11) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `type` enum('tag','segment','label') DEFAULT 'segment',
  PRIMARY KEY (`id`),
  UNIQUE KEY `appId` (`appId`,`name`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `segments`
--
-- WHERE:  1 limit 20

LOCK TABLES `segments` WRITE;
/*!40000 ALTER TABLE `segments` DISABLE KEYS */;
INSERT INTO `segments` VALUES (1,1,'VIP',NULL,NULL,'2019-05-12 16:27:50','2019-05-12 16:27:50','segment'),(2,1,'Spain','Guests in spain',NULL,'2019-05-12 16:28:11','2019-05-12 16:28:11','segment'),(3,NULL,'huge',NULL,NULL,'2019-05-13 02:38:21','2019-05-13 02:38:21','segment'),(4,NULL,'huge',NULL,NULL,'2019-05-13 02:38:33','2019-05-13 02:38:33','segment'),(5,1,'Talk to manager',NULL,NULL,'2019-06-03 22:14:27','2019-06-03 22:14:27','tag'),(6,1,'Special Request',NULL,NULL,'2019-06-03 23:12:48','2019-06-03 23:12:48','tag'),(7,1,'Testewe',NULL,NULL,'2019-06-06 07:19:37','2019-06-06 07:19:37','segment'),(8,1,'qq',NULL,NULL,'2019-06-06 07:26:22','2019-06-06 07:26:22','segment'),(9,1,'ww',NULL,NULL,'2019-06-06 07:26:48','2019-06-06 07:26:48','segment'),(10,1,'maxi',NULL,NULL,'2019-06-06 07:27:45','2019-06-06 07:27:45','segment'),(11,1,'test',NULL,NULL,'2019-06-06 07:28:12','2019-06-06 07:28:12','segment'),(12,1,'aaa',NULL,NULL,'2019-06-06 07:28:25','2019-06-06 07:28:25','segment'),(13,1,'test',NULL,NULL,'2019-06-06 07:36:51','2019-06-06 07:36:51','tag'),(14,1,'maxi',NULL,NULL,'2019-06-06 07:37:02','2019-06-06 07:37:02','tag'),(15,1,'ty',NULL,NULL,'2019-06-06 07:40:53','2019-06-06 07:40:53','tag'),(16,1,'test2',NULL,NULL,'2019-06-06 07:40:58','2019-06-06 07:40:58','segment'),(23,1,'uhu',NULL,NULL,'2019-06-07 04:28:53','2019-06-07 04:28:53','tag'),(24,1,'hello',NULL,NULL,'2019-08-23 17:45:02','2019-08-23 17:45:02','tag'),(25,1,'VIP',NULL,NULL,'2019-08-23 17:45:13','2019-08-23 17:45:13','tag'),(26,1,'jnjn',NULL,NULL,'2019-08-25 17:01:18','2019-08-25 17:01:18','tag');
/*!40000 ALTER TABLE `segments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `publicId` bigint(20) DEFAULT NULL,
  `appId` int(11) DEFAULT NULL,
  `url` varchar(512) DEFAULT NULL,
  `ip` int(11) DEFAULT NULL,
  `browserName` varchar(20) DEFAULT NULL,
  `browserMajor` varchar(4) DEFAULT NULL,
  `osName` varchar(15) DEFAULT NULL,
  `deviceType` varchar(15) DEFAULT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9643 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--
-- WHERE:  1 limit 20

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES (1,85,1,'http://client:8000/embed-widget',0,'Chrome','74','Mac OS',NULL,'2019-05-18 03:13:34'),(2,59,1,'http://client:8000/embed-widget',1169507940,'Chrome','74','Mac OS',NULL,'2019-05-18 03:43:23'),(3,61,1,'http://client:8000/embed-widget',NULL,'Chrome','74','Mac OS',NULL,'2019-05-18 03:43:23'),(4,85,1,'http://client:8000/embed-widget',0,'Chrome','74','Mac OS',NULL,'2019-05-18 03:52:12'),(5,62,1,'http://client:8000/embed-widget',1169507940,'Chrome','74','Mac OS',NULL,'2019-05-18 03:52:45'),(6,85,1,'http://client:8000/embed-widget',0,'Chrome','74','Mac OS',NULL,'2019-05-18 03:53:03'),(7,85,1,'http://client:8000/embed-widget',1169507940,'Chrome','74','Mac OS',NULL,'2019-05-18 03:54:22'),(8,49,1,'http://client:8000/embed-widget',1169507940,'Chrome','74','Mac OS',NULL,'2019-05-18 04:15:40'),(9,61,1,'http://client:8000/embed-widget',NULL,'Chrome','74','Mac OS',NULL,'2019-05-16 04:20:12'),(10,NULL,NULL,'http://client:8000/embed-widget',0,'Chrome','74','Mac OS',NULL,'2019-05-18 04:20:20'),(11,NULL,1,'http://client:8000/embed-widget',0,'Chrome','74','Mac OS',NULL,'2019-05-18 04:21:06'),(12,85,1,'http://client:8000/embed-widget',1169507940,'Chrome','74','Mac OS',NULL,'2019-05-18 05:01:05'),(13,85,1,'http://client:8000/embed-widget',1169507940,'Chrome','74','Mac OS',NULL,'2019-05-18 06:37:32'),(14,85,1,'http://client:8000/embed-widget',1169507940,'Chrome','74','Mac OS',NULL,'2019-05-18 07:40:43'),(15,85,1,'http://client:8000/embed-widget',2130706433,'Chrome','74','Mac OS',NULL,'2019-05-19 07:03:18'),(16,85,1,'http://client:8000/embed-widget',2130706433,'Chrome','74','Mac OS',NULL,'2019-05-19 20:41:55'),(17,86,1,'http://client:8000/embed-widget',2130706433,'Chrome','74','Mac OS',NULL,'2019-05-19 22:21:15'),(18,86,1,'http://client:8000/embed-widget',2130706433,'Chrome','74','Mac OS',NULL,'2019-05-20 03:55:46'),(19,86,1,'http://localhost:8000/dashboard/configure-widget',2130706433,'Chrome','74','Mac OS',NULL,'2019-05-20 08:12:55'),(20,86,1,'http://localhost:8000/dashboard/configure-widget',2130706433,'Chrome','74','Mac OS',NULL,'2019-05-21 18:08:29');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social-connections`
--

DROP TABLE IF EXISTS `social-connections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social-connections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `socialId` varchar(30) DEFAULT NULL,
  `type` enum('facebook','google','twitter','linkedin') DEFAULT NULL,
  `token` varchar(40) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social-connections`
--
-- WHERE:  1 limit 20

LOCK TABLES `social-connections` WRITE;
/*!40000 ALTER TABLE `social-connections` DISABLE KEYS */;
INSERT INTO `social-connections` VALUES (4,13,'109191747391775875895','google',NULL,NULL,'2018-10-06 19:47:38','2018-10-06 19:47:38'),(5,14,'2014969431857232','facebook',NULL,NULL,'2018-10-08 04:07:49','2018-10-08 04:07:49'),(6,14,'115581736745796854822','google',NULL,NULL,'2018-10-14 07:44:28','2018-10-14 07:44:28'),(8,40,'117874110210314531297','google',NULL,NULL,'2019-06-09 07:45:03','2019-06-09 07:45:03'),(9,54,'104219398518671743746','google',NULL,NULL,'2019-11-07 18:57:12','2019-11-07 18:57:12'),(10,74,'103987877676656421191','google',NULL,NULL,'2020-09-25 05:41:47','2020-09-25 05:41:47');
/*!40000 ALTER TABLE `social-connections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `special-open-hours`
--

DROP TABLE IF EXISTS `special-open-hours`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `special-open-hours` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appId` int(11) DEFAULT NULL,
  `strDate` varchar(15) DEFAULT NULL,
  `openHour` varchar(5) DEFAULT NULL,
  `closeHour` varchar(5) DEFAULT NULL,
  `openHour2` varchar(5) DEFAULT NULL,
  `closeHour2` varchar(5) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `appDay` (`appId`,`strDate`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `special-open-hours`
--
-- WHERE:  1 limit 20

LOCK TABLES `special-open-hours` WRITE;
/*!40000 ALTER TABLE `special-open-hours` DISABLE KEYS */;
INSERT INTO `special-open-hours` VALUES (1,1,'2019-05-04',NULL,NULL,NULL,NULL,'2019-05-11 16:59:33','2019-05-12 02:16:40'),(2,1,'2019-05-08','0','24',NULL,NULL,'2019-05-11 18:35:20','2019-05-15 17:21:57'),(3,1,'2019-05-01',NULL,NULL,NULL,NULL,'2019-05-12 02:03:41','2019-05-12 02:03:41'),(10,1,'2019-05-15','0','24',NULL,NULL,'2019-05-12 02:18:35','2019-05-15 17:21:45'),(12,1,'2019-05-07','0','24',NULL,NULL,'2019-05-12 03:20:21','2019-05-12 03:21:06'),(16,1,'2019-05-02','0','24',NULL,NULL,'2019-05-12 03:38:04','2019-05-12 03:38:07'),(18,1,'2019-05-09','0','24',NULL,NULL,'2019-05-12 03:38:34','2019-05-12 03:38:40'),(19,1,'2019-05-10','0','24',NULL,NULL,'2019-05-12 03:38:36','2019-05-12 03:38:38'),(22,1,'2019-05-25',NULL,NULL,NULL,NULL,'2019-05-12 08:27:19','2019-05-12 08:35:21'),(24,1,'2019-05-22','0','24',NULL,NULL,'2019-05-12 08:37:36','2019-05-12 08:37:36'),(25,1,'2019-05-16','0','24',NULL,NULL,'2019-05-12 09:21:14','2019-05-12 09:21:16'),(31,1,'2019-08-12',NULL,NULL,NULL,NULL,'2019-08-18 02:26:30','2019-08-18 02:26:34'),(32,1,'2019-08-01','0','24',NULL,NULL,'2019-08-31 09:44:22','2019-08-31 09:44:25'),(34,1,'2019-08-31','0','24',NULL,NULL,'2019-08-31 09:50:18','2019-08-31 09:50:21'),(35,7,'2019-12-16','0','24',NULL,NULL,'2019-12-10 18:43:19','2019-12-10 18:43:19'),(36,7,'2019-12-18','0','24',NULL,NULL,'2019-12-10 18:45:04','2019-12-10 18:45:04'),(37,12,'2019-12-16','0','24',NULL,NULL,'2019-12-10 18:45:28','2019-12-10 18:45:28'),(38,12,'2019-12-23',NULL,NULL,NULL,NULL,'2019-12-27 17:54:17','2019-12-27 17:54:18'),(40,12,'2019-12-30','7:00','21:45','21:45','24:00','2019-12-29 09:06:13','2019-12-29 09:06:13'),(41,15,'2019-12-31','7:00','24:00',NULL,NULL,'2019-12-31 09:11:06','2019-12-31 09:11:06');
/*!40000 ALTER TABLE `special-open-hours` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unsubscribeds`
--

DROP TABLE IF EXISTS `unsubscribeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unsubscribeds` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appId` int(11) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `appId` (`appId`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unsubscribeds`
--
-- WHERE:  1 limit 20

LOCK TABLES `unsubscribeds` WRITE;
/*!40000 ALTER TABLE `unsubscribeds` DISABLE KEYS */;
INSERT INTO `unsubscribeds` VALUES (2,12,'infoprisma1@gmail.com','2020-01-04 21:43:00','2020-01-04 21:43:00');
/*!40000 ALTER TABLE `unsubscribeds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(120) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `firstName` varchar(30) DEFAULT NULL,
  `lastName` varchar(30) DEFAULT NULL,
  `company` varchar(30) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `validated` tinyint(1) DEFAULT '0',
  `password` varchar(50) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `notes` text,
  `status` varchar(100) DEFAULT NULL,
  `createdBy` varchar(40) DEFAULT NULL,
  `preferredContactMethod` enum('email','phone','facebook','google') DEFAULT 'email',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `stripeId` varchar(30) DEFAULT NULL,
  `superAdmin` tinyint(1) DEFAULT '0',
  `isInvite` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email_2` (`email`),
  UNIQUE KEY `username_2` (`username`),
  UNIQUE KEY `email_3` (`email`),
  UNIQUE KEY `username_3` (`username`),
  UNIQUE KEY `email_4` (`email`),
  UNIQUE KEY `username_4` (`username`),
  UNIQUE KEY `email_5` (`email`),
  UNIQUE KEY `username_5` (`username`),
  UNIQUE KEY `email_6` (`email`),
  UNIQUE KEY `username_6` (`username`),
  UNIQUE KEY `email_7` (`email`),
  UNIQUE KEY `username_7` (`username`),
  UNIQUE KEY `email_8` (`email`),
  UNIQUE KEY `username_8` (`username`),
  UNIQUE KEY `email_9` (`email`),
  UNIQUE KEY `username_9` (`username`),
  UNIQUE KEY `email_10` (`email`),
  UNIQUE KEY `username_10` (`username`),
  UNIQUE KEY `email_11` (`email`),
  UNIQUE KEY `username_11` (`username`),
  UNIQUE KEY `email_12` (`email`),
  UNIQUE KEY `username_12` (`username`),
  UNIQUE KEY `email_13` (`email`),
  UNIQUE KEY `username_13` (`username`),
  UNIQUE KEY `email_14` (`email`),
  UNIQUE KEY `username_14` (`username`),
  UNIQUE KEY `email_15` (`email`),
  UNIQUE KEY `username_15` (`username`),
  UNIQUE KEY `email_16` (`email`),
  UNIQUE KEY `username_16` (`username`),
  UNIQUE KEY `email_17` (`email`),
  UNIQUE KEY `username_17` (`username`),
  UNIQUE KEY `email_18` (`email`),
  UNIQUE KEY `username_18` (`username`),
  UNIQUE KEY `email_19` (`email`),
  UNIQUE KEY `username_19` (`username`),
  UNIQUE KEY `email_20` (`email`),
  UNIQUE KEY `username_20` (`username`),
  UNIQUE KEY `email_21` (`email`),
  UNIQUE KEY `username_21` (`username`),
  UNIQUE KEY `email_22` (`email`),
  UNIQUE KEY `username_22` (`username`),
  UNIQUE KEY `email_23` (`email`),
  UNIQUE KEY `username_23` (`username`),
  UNIQUE KEY `email_24` (`email`),
  UNIQUE KEY `username_24` (`username`),
  UNIQUE KEY `email_25` (`email`),
  UNIQUE KEY `username_25` (`username`),
  UNIQUE KEY `email_26` (`email`),
  UNIQUE KEY `username_26` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--
-- WHERE:  1 limit 20

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'a@a.com',NULL,NULL,NULL,NULL,NULL,0,'3eff832a77b78f3fbc73ae46159d95731a12dca6',NULL,NULL,NULL,NULL,'email','2018-10-04 23:38:29','2019-06-09 07:01:46',NULL,0,0),(2,'b@b.com',NULL,NULL,NULL,NULL,NULL,0,'630be44b28de042040f1b09f8250bee963a1236e',NULL,NULL,NULL,NULL,'email','2018-10-05 00:00:44','2018-10-05 00:00:44',NULL,0,0),(3,'c@c.com',NULL,NULL,NULL,NULL,NULL,0,'ef126576424755fa5ad2846cf359026bb18f4335',NULL,NULL,NULL,NULL,'email','2018-10-05 00:13:13','2018-10-05 00:13:13',NULL,0,0),(5,'d@d.com',NULL,'Richard','Nili',NULL,NULL,0,'183fc3f99d1986bdc22b7388553b8a9db29399a4',NULL,NULL,NULL,NULL,'email','2018-10-05 04:54:27','2018-10-05 04:54:27',NULL,0,0),(12,'dekd2@ck.com',NULL,NULL,NULL,NULL,NULL,0,'7dd56a6c57f2f61f6f3ed41ee344216b93f57d70',NULL,NULL,NULL,NULL,'email','2018-10-05 07:54:43','2018-10-05 07:54:43',NULL,0,0),(13,'40bitcoins@gmail.com',NULL,'40','Bitcoins',NULL,NULL,1,NULL,'https://lh3.googleusercontent.com/-mW2kNP5qJOA/AAAAAAAAAAI/AAAAAAAAAAA/AAN31DWD7ckW2cYSIOwoRUNXj2O4vdF0LA/s96-c/photo.jpg',NULL,NULL,NULL,'email','2018-10-06 19:47:38','2018-10-06 19:47:38',NULL,0,0),(14,'infoprisma1@gmail.com',NULL,'Maximiliano','Benedetto',NULL,'4159098542',1,'b084cfc57ae28932adebbbac5124334d1630a417','1559893255133.jpeg',NULL,NULL,NULL,'email','2018-10-07 19:31:37','2019-06-07 07:40:55','cus_Dqh9fZU3Cs92N3',1,0),(15,'uh@uh.com',NULL,NULL,NULL,NULL,NULL,0,'af69520acc41cb5ab3b7fd44f6c59907998f07fd',NULL,NULL,NULL,NULL,'email','2018-10-07 23:18:10','2018-10-07 23:18:10',NULL,0,0),(16,'dccdjn@kmk.com',NULL,NULL,NULL,NULL,NULL,0,'5e77ac58404e7c63be1c2fc9debe41580509386f',NULL,NULL,NULL,NULL,'email','2018-10-07 23:18:56','2018-10-07 23:18:56',NULL,0,0),(17,'edkmedkme@cdkd.com',NULL,NULL,NULL,NULL,NULL,0,'dcacc33a0cae524a752b6f1c1c07e16f8112d30b',NULL,NULL,NULL,NULL,'email','2018-10-09 08:03:10','2018-10-09 08:03:10',NULL,0,0),(18,'cdjcd@jndj.com',NULL,NULL,NULL,NULL,NULL,0,'aa66cd59d1c8192e0693e04e5006e78e6339e094',NULL,NULL,NULL,NULL,'email','2018-10-14 07:10:58','2018-10-14 07:10:58',NULL,0,0),(19,'eded@njdc.com',NULL,NULL,NULL,NULL,NULL,0,'893a937976b56c26eceb90e6330446886e2a5712',NULL,NULL,NULL,NULL,'email','2018-10-14 07:25:24','2018-10-14 07:25:24',NULL,1,0),(20,'u@u.com',NULL,NULL,NULL,NULL,NULL,0,'3a1c31ac0bd263814759c237dee5ad2f4a5883de',NULL,NULL,NULL,NULL,'email','2018-10-22 02:06:35','2018-10-22 02:06:35',NULL,0,0),(21,'ko@ko.com',NULL,NULL,NULL,NULL,NULL,0,'c5ff7d44c6a4f77209a8b9e492738432d400b1b2',NULL,NULL,NULL,NULL,'email','2018-12-18 02:56:17','2018-12-18 02:56:17',NULL,0,0),(22,'cddccd',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email','2019-06-08 23:32:44','2019-06-08 23:32:44',NULL,0,1),(23,'vffv',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email','2019-06-08 23:34:21','2019-06-08 23:34:21',NULL,0,1),(24,'tgtt',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email','2019-06-08 23:38:02','2019-06-08 23:38:02',NULL,0,1),(25,'ffvvfvf',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email','2019-06-08 23:38:24','2019-06-08 23:38:24',NULL,0,1),(26,'maxi',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email','2019-06-08 23:41:26','2019-06-08 23:41:26',NULL,0,1),(27,'maxi2',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'email','2019-06-08 23:41:45','2019-06-08 23:41:45',NULL,0,1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-04 23:26:28

A socket message will require some predefined schema:
{
	type: (is not actually part of the payload, but the message type -- the available types are in socket-status.js)
	value: (anything, this is actual content of the message, where as the rest is just context -- mostly strings though)
	timestamp: bigint (new Date().getTime() -- identifies the message)
	user: (`publicId` for customers, `userId` for operators)
	owner: (`client` for customers, `userId` for operators)

	tempInteractionId (ONLY when we're creating a new interaction and the client doesn't know yet the server-side id)
	interactionId (when available)
	key (hash of publicId, appId -- whenever we're referring to a client->operator or viceversa message)
}

On the other hand, `socketId` is an internal data structure and should never be shared with the clients/operators via a message. When a socket controller needs to find out about a socketId for a user, it can use use the `key`(publicId-app hash) stored in redis under the `namespace.userToSocket`.

------------------------------------------------------------------------------------------------------------------

On top of the message, client and operator controllers may need to enrich the socket object that keep in memory and that surrounds a socket.
For example, client controllers define their context with this schema (this is passed to every controller execution):

const context = {
  appId,
  campaignId,
  lang,
  publicId,
  socketId: socket.id,
  key (hash of publicId, appId)
  rooms: [(list of pertinent roooms: `appId`, `campaignId`, `appId-Lang`, `campaignId-lang`)],
  socket
};


Operators will hold a simpler state, and just join/leave rooms as different assets are selected (any of the possible combinatios: `appId`, `campaignId`, `appId-Lang`, `campaignId-lang`).
const context = {
	userId,
	socket
};

Both operator and client controllers will have access to mysql and redis models, via the `socket.request` object, which is populated by the connection middleware.

------------------------------------------------------------------------------------------------------------------

EVENTS:
Sintax: `cb(error, data)` refers to data returned by the callback of each event
In all cases when there is a `cb`  expected in case an error were to be produced by the controller the client will get an `error` key in the response
Event emmitters should assume that an error has ocurred, should the confirmation not to be received in `10 seconds` (configurable). In that case it will retry once and then inform the user of the error, allowing a `retry or delete` user action.

**Events sent by CLIENTS** (all of these will be propagated to the relevant operator rooms):
   -> Connect
          | cb(
          		key: hash pid and appId
          	)
   -> ConversationUpdate (triggered on load conversation or reconnect)
   				| cb(
   						key: hash pid and appId
   						conversationStatus: (if interactionId if defined, will update messages and lastViewedByOperator)
   					)
   -> Disconnect
   -> Message: can become either:
                   -> newMessage (if a chat interaction already exists)
                      | cb (empty | just received)
                   -> newInteraction (if a new interaction is created)
                   	  | cb (
                   	  		interactionId: newly createdId will replace a temporaryId generated in the client)
                   	  		tempInteractionId: previously temporaryID assigned at client-side, that will be replaced by this new one
                   	  	)
   -> MessageViewed


**Events handled by clients**:
   -> Reconnect -> triggerConversarionUpdate
   -> Connect -> get client key
   -> Disconnect -> show connection error if longer than 10 secs

   -> Message (sent by operator)
   -> MessageViewed (sent by operator)



-------

**Events sent by Operators**
	 -> Message
				| cb(empty)
	 -> MessageViewed
   -> SubscribeToAsset (when an asset is selected)
   			| cb(
   					pendingConversations: will require pagination
   					claimedOpenConversations: will require pagination
   					onlineUsers
   			  )
   -> ClaimInteraction
    		| cb(empty confirmation)
   -> UnclaimInteraction
    		| cb(empty confirmation)
   -> ResolveConversation
   			| cb(empty confirmation)



**Events handled by Operators**
   -> Connect
   -> Disconnect -> show connection error if longer than 10 secs
   -> Reconnect -> triggers SubscribeToAsset (if one is selected)
   -> ClientConnection -> (gets the user key and pid)
   -> ClientDisconnection -> (gets the user key and pid)
   -> ClientMessage
   -> ClientMessageViewed


---------------------------------------------------------------------------------------------

REDIS MODEL

Initially, only the user hash connected to socketIds will be held at Redis. 
We might choose to cache conversations for a short term later, to alleviate MySQL workload.

Redis keys will have a `{namespace}-{key}` format.
So far, namespaces are:
```
RedisNamespaces = {
  userToSocket: 'TID'
}
```

and the format for the namespaces keys' content is:

`[RedisNamespaces.userToSocket]: list[of socketIDs, associated with user key]`






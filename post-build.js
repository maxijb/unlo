const aws = require('aws-sdk');
const mime = require('mime-types');
const path = require('path');
const getGitHash = require('./services/common/utils/git-utils');

//const ShoouldResize = new Set(['png', 'jpg', 'webp', 'jpeg']);
//const AllowedExt = new Set(['png', 'jpg', 'webp', 'jpeg', 'pdf']);
//const folder = path.join(__dirname, `../../../../../${StaticFilesFolder}`);

const {promisify} = require('util');
const {resolve} = require('path');
const fs = require('fs');
const readdir = promisify(fs.readdir);
const stat = promisify(fs.stat);

async function getFiles(dir) {
  const subdirs = await readdir(dir);
  const files = await Promise.all(
    subdirs.map(async subdir => {
      const res = resolve(dir, subdir);
      return (await stat(res)).isDirectory() ? getFiles(res) : res;
    }),
  );
  return files.reduce((a, f) => a.concat(f), []);
}

const s3 = new aws.S3({});

const buildId = getGitHash().trim();
console.log('-', buildId.trim(), '-');

async function uploadToS3(file, key) {
  return new Promise((resolve, reject) => {
    // console.log('uploading to', path.join('builds', buildId, key));
    fs.readFile(file, (err, body) => {
      var params = {
        Bucket: 'incredible-public',
        Key: path.join('builds', buildId, key),
        Body: body,
        ACL: 'public-read',
        ContentType: mime.lookup(file) || 'text/plain',
      };

      s3.putObject(params, function (err, data) {
        if (err) {
          console.error(err, err.stack);
          reject();
        } else {
          resolve();
        }
      });
    });
  });
}

const base = path.join(__dirname, 'services-dist/front/.next');

async function doProcess() {
  getFiles(base)
    .then(async files => {
      for (const file of files) {
        console.log('processing', file);
        await uploadToS3(file, file.replace(base, '_next'));
      }
    })
    .catch(e => console.error(e));
}

doProcess();

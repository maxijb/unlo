import data from './moment-data-packed';
import {unpack} from '../../services/common/utils/moment';
import fs from 'fs';
import path from 'path';

/* This scripts generates the timezone data based on what's extracted from moment.js data fot 5-+ years from now
 * should be run every year!!!
 *
 * the saving part is disabled for test NODE_ENV (look at TODO)
 *
 * The generated file will be saved in /services/common/constants/timezones-data.js
 *
 * The data must be downloaded every year from https://momentjs.com/downloads/moment-timezone-with-data-10-year-range.min.js
 * and extract only the object starting with `{version:"2019a",zones:`, until the end of the file (must be a proper JS object)
 */

function unpackAll() {
  const zones = data.zones.reduce((prev, zone) => {
    const [name] = zone.split('|');
    const unpacked = unpack(zone);
    prev[name] = {
      unpacked,
      packed: zone
    };
    return prev;
  }, {});

  const links = data.links.reduce((prev, link) => {
    const [name, mirror] = link.split('|');
    prev[mirror] = name;
    return prev;
  }, {});

  const toSave = {zones, links};

  // TODO: find a way to run this differently
  if (process.env.NODE_ENV !== 'test') {
    const str = `export default ${JSON.stringify(toSave, null, 4)}`;
    fs.writeFileSync(
      path.join(__dirname, '../../services/common/constants', '/timezones-data.js'),
      str,
      'utf-8'
    );
  }

  return toSave;
}

export default {
  unpackAll
};

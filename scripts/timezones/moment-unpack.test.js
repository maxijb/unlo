import moment from './moment-unpack';

describe('unpackAll', () => {
  const results = moment.unpackAll();
  it('expect africa to be defined', () => {
    expect(results.zones['Africa/Abidjan']).toBeDefined();
  });
  it('expect africa to be defined', () => {
    expect(results.zones['Africa/Abidjan'].unpacked).toEqual({
      abbrs: ['GMT'],
      name: 'Africa/Abidjan',
      offsets: [0],
      population: 4800000,
      untils: [Infinity]
    });
    expect(results.zones['Africa/Abidjan'].packed).toEqual('Africa/Abidjan|GMT|0|0||48e5');
  });

  it('no zone should have empty mirror', () => {
    const valid = Object.keys(results.links).every(link => {
      return !!results.zones[results.links[link]];
    });
    expect(valid).toBeTruthy();
  });
});

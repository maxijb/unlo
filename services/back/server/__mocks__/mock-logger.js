export default {
  info: jest.fn(),
  log: jest.fn(),
  error: jest.fn()
};

const Controllers = {
  async getActiveAB(query, req) {
    return {activeAB: req.activeAB, cookieAB: req.cookieAB};
  }
};

export default Controllers;

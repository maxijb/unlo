import TimezonesData from '@Common/constants/timezones-data';
import fetch from 'node-fetch';
import getIphoneFeed, {sortByCapacity, sortByCarrier} from '../services/revamp';
import fs from 'fs';
import path from 'path';
import xml from 'xml';
import CrudControllers from './crud-controllers';
import EmailControllers from './email-controllers';
import {getPageLink} from '@Common/utils/urls';
import {Validators} from '@Common/utils/validations';
import {
  StaticFilesFolder,
  MultiplyCarrierItems,
  DropshippingSellerID,
  DropshippingMargin,
} from '@Common/constants/app';
import {arrayToMap, arrayToMultipleMap} from '@Common/utils/generic-utils';
import {
  consolidateAttributesAsName,
  getProductNameWithAttributesForGoogle,
} from '../../../front/shared/utils/attributes-utils';
const createCsvWriter = require('csv-writer').createObjectCsvStringifier;

const Controllers = {
  async getTimezonesList(query, req) {
    return Object.keys(TimezonesData.zones).concat(Object.keys(TimezonesData.links)).sort();
  },

  async getCarriers(_, req) {
    return await req.Models.Attribute.getAllCarriers();
  },

  async getMenuItems(query, req) {
    return await req.Models.Category.getMenuItems();
  },

  async findAllVisibleLeaves(query, req) {
    return await req.Models.Category.findAllVisibleLeaves();
  },

  async getHighlightedProducts(query, req) {
    const cats = await req.Models.Category.getHighlightedProducts();
    const products = await req.Models.Product.findCheapestAvailableVersionBatchWithAllColors(
      cats.map(it => it.id),
    );
    const highIndex = 0; //Math.floor(Math.random() * products.length);
    return {products, highlightedCategory: products[highIndex].category_id};
  },

  async getTradeInFeed(query, req) {
    let content = fs.readFileSync(
      path.join(__dirname, '..', 'scripts', 'revamp-processed.xml'),
      'utf8',
    );
    return {
      response: content,
      headers: {
        'Content-Type': 'text/xml',
      },
    };
  },

  async getTradeInOptions(query, req) {
    const {type, model, capacity} = query;
    const {tree, conditions} = await getIphoneFeed();

    if (!type || type === 'model') {
      return {options: Object.keys(tree)};
    }

    if (type === 'condition') {
      return {options: conditions};
    }

    if (type === 'capacity') {
      return {options: sortByCapacity(Object.keys(tree[model]))};
    }

    if (type === 'carrier') {
      return {options: sortByCarrier(Object.keys(tree[model]?.[capacity]))};
    }
    return {carriers, capacities, models, conditions};
  },

  async getTradeInPrediction(query, req) {
    const {tree, conditions} = await getIphoneFeed();
    const {carrier, model, capacity, condition} = query;

    if (!model || !capacity || !carrier || !condition) {
      return {price: null};
    }

    return {price: tree[model]?.[capacity]?.[carrier]?.[condition] || null};
  },

  async getStaticUrls(_, req) {
    return req.Models.UrlAlias.findAll({raw: true});
  },

  async getPublicKeys(query, req) {
    return {stripe: req.Config.get('social').stripe.public};
  },

  async getFacebookInventory(query, req) {
    const list = await CrudControllers.productsAllList({where: {visible: 1}, limit: 8000}, req);
    const categories = await req.Models.Category.findAll({raw: true});
    const minPrices = await req.Models.Product.findMinPricePerIncredibleId(null, true);

    const allCarriers = await req.Models.Attribute.findAll({where: {type: 'carrier'}, raw: true});
    const carriersMap = arrayToMap(allCarriers, 'name');

    const cat_map = arrayToMap(categories);
    const config = req.Config.get('server');
    const products = [];

    const csvWriter = createCsvWriter({
      header: [
        {id: 'id', title: 'id'},
        {id: 'title', title: 'title'},
        {id: 'description', title: 'description'},
        {id: 'availability', title: 'availability'},
        {id: 'condition', title: 'condition'},
        {id: 'price', title: 'price'},
        {id: 'link', title: 'link'},
        {id: 'image_link', title: 'image_link'},
        {id: 'brand', title: 'brand'},
        {id: 'google_product_category', title: 'google_product_category'},
        {id: 'fb_product_category', title: 'fb_product_category'},
        {id: 'quantity_to_sell_on_facebook', title: 'quantity_to_sell_on_facebook'},
        {id: 'color', title: 'color'},
      ],
    });

    Object.keys(list.products).forEach(key => {
      const it = list.products[key];

      const data = consolidateAttributesAsName(it.attributes, req.t);
      const cat = cat_map[it.category_id];
      // const specs = specsMap[it.category_id] || [];

      const alternativeCarriers = [data.carrier, ...(MultiplyCarrierItems[data.carrier] || [])];

      for (const carrier of alternativeCarriers) {
        if (!it.price) {
          continue;
        }
        const modifiedData = {
          ...data,
          carrier,
        };
        const longName = getProductNameWithAttributesForGoogle(it.name, modifiedData, req.t);

        const showCarriers = !carrier?.startsWith('Unlocked') ? [carrier] : [];

        showCarriers.push(...(MultiplyCarrierItems[data.carrier] || []).filter(c => c !== carrier));

        let allCarriersDescription = '';
        const carrierList = showCarriers.join(', ');
        if (data.carrier === 'Unlocked') {
          allCarriersDescription = `all carriers: ${carrierList}`;
        } else if (data.carrier === 'Unlocked GSM') {
          allCarriersDescription = `all GSM carriers: ${carrierList}`;
        } else if (data.carrier === 'Unlocked CDMA') {
          allCarriersDescription = `all CDMA carriers: ${carrierList}`;
        } else {
          allCarriersDescription = carrierList;
        }
        const carriersDescriptions = `${
          data.carrier === 'Unlocked' ? 'Fully unlocked' : 'Unlocked'
        } and compatible with ${allCarriersDescription}.`;

        const link = `${config.protocol}://${config.fullDomain}${getPageLink('variant', {
          product: it,
          attributes: modifiedData,
          t: req.t,
        })}`;

        const row = {
          id: `${it.id}${
            data.carrier !== carrier && carrier ? `-${carriersMap[carrier]?.id}` : ''
          }`,

          title: longName,

          description: `This professionally refurbished ${it.name} smartphone works like new. It Successfully passed a 20-point diagnostic test and is 100% certified as fully functional. It includes free shipping and a 1 year warranty. Tested for battery health and guaranteed to have a minimum battery capacity of 80%. ${carriersDescriptions} Headphones and SIM card not included. Wall adapter charging block and USB to lightning power charger cable sold separately. All our chargers are MFi (Made for iPhone) and meet Apple quality criteria (not a cheap iPhone chargers). 4G LTE Advanced - ${data.capacity} - ${data.appearance} condition.`,
          availability: it.stock > 0 && it.price > 0 ? 'in stock' : 'out of stock',
          condition: 'refurbished',
          price: `${it.price} USD`,

          link: `${link}?s=gsh${
            data.carrier !== carrier && carrier ? `&attr=${carriersMap[carrier]?.slug}` : ''
          }`,

          image_link: `${config.protocol}://${config.cdnDomain}/${StaticFilesFolder}/400x400/${it.image}`,

          brand: 'Apple',

          google_product_category:
            'Electronics > Communications > Telephony > Mobile Phones > Unlocked Mobile Phones',
          fb_product_category: 'electronics > cell phones & smart watches > cell phones',
          quantity_to_sell_on_facebook: it.stock,
          color: it.color,
        };

        products.push(row);
      }
    });

    const content = `${csvWriter.getHeaderString()}${csvWriter.stringifyRecords(products)}`;

    return {response: content, attachment: 'report.csv'};
  },
  async getInventoryXml(query, req) {
    const list = await CrudControllers.productsAllList({where: {visible: 1}, limit: 8000}, req);
    const categories = await req.Models.Category.findAll({raw: true});
    const minPrices = await req.Models.Product.findMinPricePerIncredibleId(null, true);

    //const allSpecs = await req.Models.Spec.findAll({
    //  where: {type: 'spec'},
    //  attributes: ['value', 'name', 'category_id'],
    //  raw: true,
    //});
    //const specsMap = arrayToMultipleMap(allSpecs, 'category_id');

    const allCarriers = await req.Models.Attribute.findAll({where: {type: 'carrier'}, raw: true});
    const carriersMap = arrayToMap(allCarriers, 'name');

    const cat_map = arrayToMap(categories);
    const config = req.Config.get('server');
    const products = [];
    const surfacesToExclude = ['Display ads', 'Shopping ads'];

    Object.keys(list.products).forEach(key => {
      const it = list.products[key];

      const data = consolidateAttributesAsName(it.attributes, req.t);
      const cat = cat_map[it.category_id];
      // const specs = specsMap[it.category_id] || [];

      const alternativeCarriers = [data.carrier, ...(MultiplyCarrierItems[data.carrier] || [])];

      for (const carrier of alternativeCarriers) {
        if (!it.price) {
          continue;
        }
        const modifiedData = {
          ...data,
          carrier,
        };
        const longName = getProductNameWithAttributesForGoogle(it.name, modifiedData, req.t);

        const showCarriers = !carrier?.startsWith('Unlocked') ? [carrier] : [];

        showCarriers.push(...(MultiplyCarrierItems[data.carrier] || []).filter(c => c !== carrier));

        let allCarriersDescription = '';
        const carrierList = showCarriers.join(', ');
        if (data.carrier === 'Unlocked') {
          allCarriersDescription = `all carriers: ${carrierList}`;
        } else if (data.carrier === 'Unlocked GSM') {
          allCarriersDescription = `all GSM carriers: ${carrierList}`;
        } else if (data.carrier === 'Unlocked CDMA') {
          allCarriersDescription = `all CDMA carriers: ${carrierList}`;
        } else {
          allCarriersDescription = carrierList;
        }
        const carriersDescriptions = `${
          data.carrier === 'Unlocked' ? 'Fully unlocked' : 'Unlocked'
        } and compatible with ${allCarriersDescription}.`;

        const link = `${config.protocol}://${config.fullDomain}${getPageLink('variant', {
          product: it,
          attributes: modifiedData,
          t: req.t,
        })}`;

        const row = {
          item: [
            {title: longName},
            {
              'g:id': `${it.id}${
                data.carrier !== carrier && carrier ? `-${carriersMap[carrier]?.id}` : ''
              }`,
            },
            {
              link: `${link}?s=gsh${
                data.carrier !== carrier && carrier ? `&attr=${carriersMap[carrier]?.slug}` : ''
              }`,
            },
            {
              'g:ads_redirect': `${link}?s=gsha${
                data.carrier !== carrier && carrier ? `&attr=${carriersMap[carrier]?.slug}` : ''
              }`,
            },
            {
              description: `This professionally refurbished ${it.name} smartphone works like new. It Successfully passed a 20-point diagnostic test and is 100% certified as fully functional. It includes free shipping and a 1 year warranty. Tested for battery health and guaranteed to have a minimum battery capacity of 80%. ${carriersDescriptions} Headphones and SIM card not included. Wall adapter charging block and USB to lightning power charger cable sold separately. All our chargers are MFi (Made for iPhone) and meet Apple quality criteria (not a cheap iPhone chargers). 4G LTE Advanced - ${data.capacity} - ${data.appearance} condition.`,
            },
            {
              'g:image_link': `${config.protocol}://${config.cdnDomain}/${StaticFilesFolder}/400x400/${it.image}`,
            },
            {price: `${it.price} USD`},
            {'g:price': `${it.price} USD`},
            {'g:availability': it.stock > 0 && it.price > 0 ? 'in stock' : 'out of stock'},
            {'g:condition': 'refurbished'},
            {'g:brand': 'Apple'},
            {'g:google_product_category': 543514},
            {
              'g:product_type':
                'Electronics > Communications > Telephony > Mobile Phones > Unlocked Mobile Phones',
            },
            {'g:color': it.color},
            {'g:mpn': cat.mpn},
            {'g:size_system': 'US'},
            {'g:shipping_length': `${cat.shipping_length} in`},
            {'g:shipping_width': `${cat.shipping_width} in`},
            {'g:shipping_height': `${cat.shipping_height} in`},
            {'g:shipping_weight': `${cat.shipping_weight} oz`},
            {'g:product_highlight': 'Free shipping and free returns'},
            {'g:product_highlight': 'One year warranty'},
            {'g:product_highlight': 'Tested by experts, certified to work like new'},
            //...specs.map(spec => ({
            //  'g:product_detail': [
            //    {'g:attribute_name': spec.name},
            //    {'g:attribute_value': spec.value},
            //  ],
            //})),
          ],
        };

        if (
          it.owner === DropshippingSellerID &&
          (!minPrices[it.incredible_id]?.min_price ||
            minPrices[it.incredible_id]?.min_price > it.price * DropshippingMargin)
        ) {
          row.item.push({'g:excluded_destination': 'Display ads'});
          row.item.push({'g:excluded_destination': 'Shopping ads'});
        }

        products.push(row);
      }
    });

    const content = xml(products, {
      indent: '  ',
    });
    return {
      response: `<?xml version='1.0' encoding='UTF-8' ?>
<rss version='2.0' xmlns:atom='http://www.w3.org/2005/Atom' xmlns:g='http://base.google.com/ns/1.0'>
  <channel>
    <title>Incredible Phones Products</title>
    <link>https://incrediblephones.com/</link>
    <description>IncrediblePhones inventory List RSS feed</description>
    <generator>IncrediblePhones.com API</generator>
    <atom:link href='https://incrediblephones.com/api/public/get-inventory-xml' rel='self' type='application/rss+xml' />
${content}
  </channel>
</rss>
    `,
      headers: {
        'Content-Type': 'text/xml',
      },
    };
  },

  async notifyAbandonedCarts(query, req) {
    const data = await req.Models.Contact.getAbandonedCarts();

    for (const cid of Object.keys(data.contacts)) {
      const contacts = data.contacts[cid];
      const carts = data.carts[cid];

      if (!carts?.length) {
        continue;
      }

      const products = await req.Models.Product.findAll({
        where: {
          id: carts.map(c => c.product_id),
        },
        raw: true,
      });
      const productsMap = arrayToMap(products, 'id');

      carts.forEach(c => {
        c.product = productsMap[c.product_id];
      });

      for (const contact of contacts) {
        if (Validators.validEmail('email', contact) === null) {
          await EmailControllers.sendAbandonedCartEmail(
            {
              contact,
              carts,
              promoCode: 'INC5OFF',
            },
            req,
          );
        }
      }

      await req.Models.Cart.update(
        {notified: 1},
        {
          where: {
            id: carts.map(c => c.id),
          },
        },
      );
    }

    return {};
  },

  async mapFacebookCatalog(query, req) {
    const social = req.Config.get('social');
    const {catalog_id, access_token} = social['facebook-commerce'];
    let after = null;
    let i = 0;
    let ii = 0;
    do {
      const response = await fetch(
        `https://graph.facebook.com/v11.0/${catalog_id}/products?access_token=${access_token}&summary=true&limit=2000${
          after ? `&after=${after}` : ''
        }`,
      );
      const json = await response.json();
      for (const item of json.data) {
        if (Number(item.retailer_id) && item.id) {
          await req.Models.Product.update(
            {facebook_id: item.id},
            {where: {id: Number(item.retailer_id)}},
          );
          console.log('Updating product', ii++);
        }
      }
      after = json.paging?.cursors?.after;
      console.log('Request', i++);
    } while (after);
  },
};

export default Controllers;

import {RequestError, RequestErrorMessage, CartErrorMessage} from '@Common/constants/errors';
import {waitMS} from '@Common/utils/generic-utils';
import {safeJsonParse} from '@Common/utils/json';
import {Actors} from '@Common/constants/app';
import {getShippingMethodDescription} from '@Common/utils/product-utils';
import {formatState} from '@Common/utils/format-utils';
import isEmpty from 'is-empty';
import {getClient as getBPClient} from '../services/brightpearl';
import {getClient as getSCClient} from '../services/sellercloud';
import {getClient as getSSClient} from '../services/shipstation';
import {getClient as getWHClient} from '../services/webhooks';
import {
  getCartDisplayProductId,
  getCartActualProductId,
  calculateDisplaySubtotalAndShipping,
} from '../../../front/shared/utils/cart-utils';

import {
  clientCookieName,
  CartStatus,
  CartType,
  OrderStatus,
  ShippingMethod,
  DefaultFastShippingRate,
  OrderType,
  DefaultShippingMethods,
  AllowedReturnCartStatus,
  CommissionRate,
  SellerConnectionStatus,
} from '@Common/constants/app';
import {ShipStationOrderStates} from '@Common/constants/seller-constants';
import {
  consolidateAttributesAsName,
  getProductNameWithAttributes,
  getProductNameWithAttributesForOrder,
} from '../../../front/shared/utils/attributes-utils';

import {
  AuthenticationError,
  AuthenticationErrorMessage,
  ProductErrorMessage,
} from '@Common/constants/errors';
import {getStripe} from '../services/stripe';

import {sendEmail} from '../services/sendgrid';
import {htmlTemplate, baseEmailTemplate, txtTemplate, p} from '../email/base-template';
import EmailControllers from './email-controllers';

import AppControllers from './app-controllers';
import {getClient} from '../services/taxjar';
import {canBeReturned, canBeCancelled} from '@Common/utils/order-utils';

const Controllers = {
  /* @query {
    product_id: product id
    quantity: integer
  }
  */
  async addForUser(query, req) {
    const added = await req.Models.Cart.addForUser(query, req);
    return {
      ok: true,
      cart_id: added.get('id'),
      seller_id: added.get('seller_id'),
      quantity: added.get('quantity'),
    };
  },

  async readActiveForUser(query, req) {
    return req.Models.Cart.readActiveForUser(req);
  },

  async changeQuantity(query, req) {
    const {cart_id, quantity} = query;
    const item = await req.Models.Cart.findOne({where: {id: cart_id}});

    if (
      item.get('cid') !== req.cookies[clientCookieName] &&
      (!req.auth?.id || req.auth?.id !== item.get('user'))
    ) {
      throw new AuthenticationError(AuthenticationErrorMessage.invalidToken);
    }

    const product = await req.Models.Product.findOne({
      where: {id: item.get('product_id')},
      attributes: ['stock'],
      raw: true,
    });

    if (!product.stock !== null && product.stock < quantity) {
      if (product.stock) {
        item.quantity = product.stock;
        await item.save();
      }
      throw new RequestError(CartErrorMessage.outOfStock, {max_stock: product.stock});
    }

    item.quantity = quantity;
    await item.save();
    return {ok: true};
  },

  async removeItem(query, req) {
    const {cart_id} = query;
    const item = await req.Models.Cart.findOne({where: {id: cart_id}});

    if (
      item.get('cid') !== req.cookies[clientCookieName] &&
      (!req.auth?.id || req.auth?.id !== item.get('user'))
    ) {
      throw new AuthenticationError(AuthenticationErrorMessage.invalidToken);
    }

    const seller_id = item.get('seller_id');
    const removed = [item.get('id')];
    await item.destroy();

    // if this was the only product of this seller check if we need to remove the shipping
    const {cart} = await req.Models.Cart.readActiveForUser(req);
    const othersFromSeller = cart.filter(it => it.seller_id === seller_id);
    let removeSeller = othersFromSeller.length === 1 && othersFromSeller[0].type === 'shipping';
    if (removeSeller) {
      await req.Models.Cart.destroy({
        where: {id: othersFromSeller[0].cart_id},
      });
    }

    return {ok: true, removed, removeSeller, seller_id};
  },

  async getTaxRateByZipCode(query, req) {
    if (!req.Config.get('server').callTaxJar) {
      return {taxRate: 0.1};
    }

    const {address} = query;
    const taxjar = getClient(req);

    const info = await taxjar.taxForOrder({
      from_country: 'US',
      from_zip: '54313',
      from_state: 'WI',
      from_city: 'Howard',
      from_street: '2820 Howard Cmns',
      to_country: 'US',
      to_zip: query.address.postal_code,
      to_state: formatState(address.administrative_area_level_1),
      to_city: address.locality,
      to_street: `${address.street_number} ${address.route}`,
      amount: query.subtotal,
      shipping: query.shipping || 0,
    });

    return {taxRate: info.tax.rate, ...info};
  },

  async prePlaceOrder(query, req) {
    let cid = null;
    let userId = null;
    if (!query.isApi) {
      cid = req.cookies[clientCookieName];
      userId = req.auth?.id;

      if (!cid && !userId) {
        throw new AuthenticationError(AuthenticationErrorMessage.missingUserId);
      }
    }

    const errors = [];
    const {address, shippingAddress, differentShippingAddress, storedAddressId} = query;

    if (!storedAddressId || !userId) {
      if (!address) {
        errors.push(CartErrorMessage.missingAddress);
      }
      if (address && !address.postal_code) {
        errors.push(CartErrorMessage.missingAddressPostalCode);
      }

      if (differentShippingAddress) {
        if (!shippingAddress) {
          errors.push(CartErrorMessage.missingShippingAddress);
        }
        if (!shippingAddress.postal_code) {
          errors.push(CartErrorMessage.missingShippingAddressPostalCode);
        }
      }
    }

    if (!userId) {
      if (!query.email && !query.isApi) {
        errors.push(CartErrorMessage.missingEmail);
      }
      if (!query.firstName) {
        errors.push(CartErrorMessage.missingFirstName);
      }
      if (!query.lastName) {
        errors.push(CartErrorMessage.missingLastName);
      }
    }

    if (errors.length) {
      return {ok: false, errors};
    }

    const addressObj =
      storedAddressId && userId
        ? await req.Models.Address.findOne({where: {id: storedAddressId, user: userId}})
        : await req.Models.Address.upsertAddress({
            cid,
            user: userId,
            address,
            type: 'billing',
          });

    let shippingAddressObj = null;
    if (differentShippingAddress) {
      shippingAddressObj = await req.Models.Address.upsertAddress({
        cid,
        user: userId,
        address: shippingAddress,
        type: 'shipping',
      });
    }

    const {cart, details: reference} = await req.Models.Cart.readActiveForUser(req, false, {
      isApi: query.isApi,
      carts_ids: query.carts_ids,
    });

    const products = cart.filter(c => c.get('type') === 'product');

    // Find errors for each product
    const stockErrors = [];

    for (const c of products) {
      let p = reference[getCartActualProductId(c)];

      // if actual product with no stock but display product does
      // in order to reenable this we need to make sure that we remove the shipping charges
      // if no other product from the seller
      // let display_p = reference[getCartDisplayProductId(c)];
      //if (p.stock < c.quantity && display_p.stock >= c.quantity) {
      //  c.product_id = getCartDisplayProductId(c);
      //  c.seller_id = display_p.owner;
      //  await c.save();
      //} else

      if (p.stock < c.quantity) {
        req.Logger.error('stock-error', {
          product_id: getCartActualProductId(c),
          display_product_id: getCartDisplayProductId(c),
          quantity: c.quantity,
          stock: p.stock,
        });
        stockErrors.push({
          variant_id: getCartDisplayProductId(c),
          error: CartErrorMessage.outOfStock,
          max_stock: p.stock,
        });
      }
    }

    if (stockErrors.length) {
      return {ok: false, errors: stockErrors};
    }

    const {subtotal, subtotalProducts} = calculateDisplaySubtotalAndShipping(cart, reference);

    const {taxRate} = await Controllers.getTaxRateByZipCode(
      {address: addressObj.get(), subtotal},
      req,
    );

    const tax = subtotal * taxRate;
    const total = tax + subtotal;
    return {
      ok: true,
      tax,
      total,
      subtotal,
      taxRate,
      cid,
      userId,
      reference,
      addressObj,
      shippingAddressObj,
      subtotalProducts,
      products,
      cart,
    };
  },

  async placeOrder(query, req) {
    const {
      ok,
      tax,
      total,
      subtotal,
      taxRate,
      cid,
      userId,
      reference,
      errors,
      addressObj,
      shippingAddressObj,
      subtotalProducts,
      products,
      cart,
    } = await Controllers.prePlaceOrder(query, req);

    if (!ok) {
      return {ok, errors};
    }

    const stripe = getStripe(req);
    let charge = null;
    let customer = null;

    try {
      customer = await stripe.customers.create({
        email: query.email,
        name: `${query.firstName} ${query.lastName}`,
        phone: query.phone,
        source: query.stripePaymentId,
      });
    } catch (e) {
      req.Logger.error(e);
      throw new RequestError(e.message);
    }

    try {
      charge = await stripe.charges.create({
        amount: Math.floor(total * 100),
        currency: 'usd',
        source: customer.default_source,
        customer: customer.id,
      });
    } catch (e) {
      req.Logger.error(e);
      throw new RequestError(e.message);
    }

    return Controllers.postPlaceOrder({
      query,
      req,
      ok,
      tax,
      total,
      subtotal,
      taxRate,
      cid,
      userId,
      addressObj,
      shippingAddressObj,
      subtotalProducts,

      stripe_payment_id: charge.id,
      payment_type: 'stripe',
      payment_description: `${charge.source?.brand || ''} ${charge.source?.last4 || ''}`,
    });
  },

  async postPlaceOrder({
    query,
    req,
    ok,
    tax,
    total,
    subtotal,
    taxRate,
    cid,
    userId,
    addressObj,

    shippingAddressObj,
    subtotalProducts,

    payment_description,
    stripe_payment_id,
    payment_type,
    order,
  }) {
    const {cart, details: reference} = await req.Models.Cart.readActiveForUser(req, false, {
      isApi: query.isApi,
      carts_ids: query.carts_ids,
    });
    const products = cart.filter(c => c.get('type') === 'product');

    if (!order) {
      order = await req.Models.Order.create({
        payment_type,
        stripe_payment_id: stripe_payment_id,
        address_id: addressObj.get('id'),
        shipping_address_id: !shippingAddressObj ? null : shippingAddressObj.get('id'),
        status: OrderStatus.payed,
        subtotal: subtotalProducts,
        subtotal_aggregates: subtotal,
        payment_description,
        tax,
        tax_rate: taxRate,
        total,
        user_id: req.auth?.id,
        cid,
        email: query.email,
        firstName: query.firstName,
        lastName: query.lastName,
        phone: query.phone,
        retailer_id: query.retailer_id || 0,
        retailer_order_id: query.retailer_order_id,
      });
    }

    //Update cart items
    const promises = cart.map(obj => {
      const isProduct = obj.get('type') === 'product';
      if (isProduct) {
        const ref = reference[getCartActualProductId(obj)];
        const display_ref = reference[getCartDisplayProductId(obj)];
        obj.price = ref.price;
        obj.display_price = display_ref.price;
      }

      obj.order_id = order.get('id');
      obj.status = CartStatus.inOrder;
      return obj.save();
    });

    const tradeInResults = await req.Models.Cart.getActiveTradeInForUser(req, false);
    const tradeInTotal = tradeInResults.reduce((agg, it) => {
      return agg + it.get('price');
    }, 0);
    if (tradeInResults.length) {
      const tradeInOrder = await req.Models.Order.create({
        address_id: addressObj.get('id'),
        shipping_address_id: !shippingAddressObj ? null : shippingAddressObj.get('id'),
        status: OrderStatus.tradeInStarted,
        subtotal: tradeInTotal,
        subtotal_aggregates: tradeInTotal,
        tax: 0,
        tax_rate: 0,
        total: tradeInTotal,
        user_id: req.auth?.id,
        cid,
        email: query.email,
        firstName: query.firstName,
        lastName: query.lastName,
        type: 'trade-in',
        parent_id: order.get('id'),
      });

      tradeInResults.forEach(result => {
        result.order_id = tradeInOrder.get('id');
        result.status = CartStatus.tradeInInOrder;
        promises.push(result.save());
      });
    }

    const stockPromises = products.map(async p => {
      const variant = await req.Models.Product.findOne({where: {id: getCartActualProductId(p)}});
      variant.stock = variant.get('stock') - p.quantity;
      return variant.save();
    });

    await Promise.all(promises);
    await Promise.all(stockPromises);

    try {
      await Controllers.orderHooks({id: order.get('id')}, req);
    } catch (e) {
      req.Logger.error('order-hooks-error', e);
    }

    return {ok: true, order_id: order.get('id'), errors: []};
  },

  async orderHooks(query, req) {
    const order = await req.Models.Order.getOrderDetails(query.id);

    // Mail initial email
    try {
      await EmailControllers.sendOrderConfirmationEmail({id: order.order.id}, req, order);
    } catch (e) {
      req.Logger.error('email-error', e);
    }

    const sellers = new Set();
    order.products.forEach(p => {
      if (p.seller_id) {
        sellers.add(p.seller_id);
      }
    });

    for (const seller_id of sellers) {
      try {
        const orderSeller = await req.Models.Order.getOrderDetails(query.id, seller_id);

        try {
          await EmailControllers.sendOrderConfirmationEmailSeller(
            {id: query.id, seller_id},
            req,
            orderSeller,
          );
        } catch (e) {
          req.Logger.error('seller-email-error', e);
        }

        const connection = await req.Models.SellerConnection.getConnectionMap({
          seller_id,
          status: SellerConnectionStatus.active,
          filter: {
            post_order: 1,
          },
        });

        if (connection?.brightpearl) {
          const BP = await getBPClient(
            req,
            connection?.brightpearl.account_name,
            connection?.brightpearl,
          );
          await BP.postOrder(orderSeller);
        }

        if (connection?.sellercloud) {
          const SC = await getSCClient(req, seller_id, connection?.sellercloud);
          await SC.postOrder(orderSeller);
        }

        if (connection?.shipstation) {
          const SS = await getSSClient(req, seller_id, connection?.shipstation);
          await SS.postOrder(orderSeller);
        }

        if (connection?.webhooks) {
          const WH = await getWHClient(req, seller_id, connection?.webhooks);
          await WH.postOrderCreate(orderSeller);
        }
      } catch (e) {
        req.Logger.error('seller-order-hooks-error', e);
      }
    }
  },

  async getPendingOrderDetails(query, req) {
    const order = await req.Models.Order.isOrderMine(query.id, req);
    if (!order) {
      return {order, products: [], address: null};
    }
    const address = !order?.address_id
      ? null
      : await req.Models.Address.getPublicFields(order.address_id);
    return {
      order,
      address,
    };
  },

  async isOrderMine(query, req) {
    const order = await req.Models.Order.isOrderMine(query.id, req);

    if (!order) {
      throw new RequestError(RequestErrorMessage.validationError);
    }

    return await req.Models.Order.getOrderDetails(query.id);
  },

  /* query: {shippingMethod: enum, seller_id: int} */
  async setShipping(query, req) {
    const searchQuery = {
      status: CartStatus.active,
      type: 'shipping',
      seller_id: query.seller_id,
    };
    if (req.auth?.id) {
      searchQuery.user = req.auth.id;
    } else {
      searchQuery.cid = req.cookies[clientCookieName];
    }

    const shipping_id = query.shipping_id;
    let shippingMethod = await req.Models.ShippingMethod.get(shipping_id, query.seller_id);

    let row = null;

    const tx = await req.Models.sequelize.transaction();
    try {
      await req.Models.Cart.destroy({where: searchQuery}, {transaction: tx});

      row = await req.Models.Cart.create(
        {
          ...searchQuery,
          cid: req.cookies[clientCookieName],
          description_seller:
            shippingMethod.name || getShippingMethodDescription(shippingMethod, req.t),
          description: getShippingMethodDescription(shippingMethod, req.t),
          price: shippingMethod.price,
          product_id: shipping_id,
          special_item: JSON.stringify({
            speed_days: shippingMethod.speed_days,
            carrier: shippingMethod.carrier,
          }),
        },
        {transaction: tx},
      );
      await tx.commit();
    } catch (e) {
      req.Logger.error(e);
      await tx.rollback();
    }

    return {
      ok: true,
      shippingMethod: shippingMethod,
      seller_id: query.seller_id,
      cart_id: row?.get('id'),
    };
  },

  async applyPromoCode(query, req) {
    const searchQuery = {
      status: CartStatus.active,
      type: 'code',
    };
    if (req.auth?.id) {
      searchQuery.user = req.auth.id;
    } else {
      searchQuery.cid = req.cookies[clientCookieName];
    }

    let promoCode = await req.Models.PromoCode.findOne({
      where: {
        name: query.promoCode,
        active: 1,
      },
      raw: true,
    });

    if (!promoCode) {
      return {ok: false};
    }

    const tx = await req.Models.sequelize.transaction();
    let row = null;
    try {
      await req.Models.Cart.destroy({where: searchQuery}, {transaction: tx});

      row = await req.Models.Cart.create(
        {
          ...searchQuery,
          cid: req.cookies[clientCookieName],
          description_seller: req.t(`common:promoCodeDescription.${query.promoCode}`, ''),
          description: req.t(`common:promoCodeDescription.${query.promoCode}`, ''),
          price: promoCode.price,
          special_item: JSON.stringify({
            name: query.promoCode,
          }),
        },
        {transaction: tx},
      );
      await tx.commit();
    } catch (e) {
      req.Logger.error(e);
      await tx.rollback();
    }

    return {
      ok: true,
      promoCode,
    };
  },

  async setTradeIn(query, req) {
    const data = await AppControllers.getTradeInPrediction(query.selections, req);
    if (!data.price) {
      throw new RequestError(RequestErrorMessage.validationError);
    }

    let row;
    if (!query.id) {
      row = await req.Models.Cart.create({
        price: data.price,
        status: CartStatus.tradeInActive,
        special_item: JSON.stringify(query.selections),
        type: CartType.tradeIn,
        user: req.auth?.id,
        cid: req.cookies[clientCookieName],
      });
    } else {
      // TODO: validate user or Cid
      row = await req.Models.Cart.findOne({where: {id: query.id}});
      if (!row || row.get('type') !== CartType.tradeIn) {
        throw new RequestError(RequestErrorMessage.validationError);
      }
      row.status = CartStatus.tradeInActive;
      row.price = data.price;
      row.special_item = JSON.stringify(query.selections);
      await row.save();
    }

    const lastOrder = await req.Models.Order.findMyLastOrder(req);
    let order_id = null;
    if (lastOrder) {
      const cid = req.cookies[clientCookieName];
      const tradeInOrder = await req.Models.Order.create({
        address_id: lastOrder.address_id,
        shipping_address_id: null,
        status: OrderStatus.tradeInStarted,
        subtotal: data.price,
        subtotal_aggregates: data.price,
        tax: 0,
        tax_rate: 0,
        total: data.price,
        user_id: req.auth?.id || null,
        cid,
        email: lastOrder.email,
        firstName: lastOrder.firstName,
        lastName: lastOrder.lastName,
        type: 'trade-in',
        parent_id: !lastOrder.type ? lastOrder.id : null,
      });
      row.order_id = order_id = tradeInOrder.get('id');
      row.status = CartStatus.tradeInInOrder;
      await row.save();

      try {
        await EmailControllers.sendTradeInOrderConfirmationEmail({id: tradeInOrder.get('id')}, req);
      } catch (e) {
        req.Logger.log('Email Error', e);
      }
    }

    return {ok: true, price: data.price, id: row.get('id'), order_id};
  },

  async getActiveTradeInForUser(query, req) {
    const results = await req.Models.Cart.getActiveTradeInForUser(req, true);
    if (!results.length) {
      return {item: null};
    }
    const selections = JSON.parse(results[0].special_item);
    return {item: {id: results[0].id, price: results[0].price, selections}};
  },

  /* query: {cart_id: [string]} */
  async returnProducts(query, req) {
    const {reason, explanation, cart_id} = query;
    const cartProducts = await req.Models.Cart.findAll({
      where: {
        id: cart_id,
        status: [AllowedReturnCartStatus],
        user: req.auth.id,
      },
    });

    if (!cartProducts.length) {
      throw new RequestError(RequestErrorMessage.validationError);
    }

    const orderIds = Array.from(new Set(cartProducts.map(p => p.get('order_id'))));
    const orders = await req.Models.Order.findAll({
      where: {
        id: orderIds,
        user_id: req.auth.id,
      },
      raw: true,
    });

    if (!orders.length || orders.length !== orderIds.length) {
      throw new RequestError(RequestErrorMessage.validationError);
    }

    const returnOrder = await req.Models.Order.create({
      type: OrderType.return,
      user_id: req.auth.id,
      notes: JSON.stringify({reason, explanation, cart_id}),
      parent_id: orderIds[0],
    });

    let returnAmount = {};

    await Promise.all(
      cartProducts.map(p => {
        const attrs = p.get();
        const isCancel = canBeCancelled(attrs);
        const isReturn = canBeReturned(attrs);
        if (!isCancel && !isReturn) {
          return;
        }
        p.status = isCancel ? CartStatus.cancelledByUser : CartStatus.requestedReturn;
        p.return_order_id = returnOrder.get('id');
        return p.save();
      }),
    );

    try {
      await Controllers.returnOrderHooks(
        {
          products: cartProducts.map(p => p.get()),
          returnOrder: {
            ...returnOrder.get(),
            reason,
            explanation,
          },
        },
        req,
      );
    } catch (e) {
      req.Logger.error('return-order-hooks-error', e);
    }

    /* We're not cretaing a bill for just starting the return
    await Promise.all(
      Object.keys(returnAmount).map(seller_id => {
        return req.Models.Billable.add({
          type: 'return',
          order_id: returnOrder.get('id'),
          debit: returnAmount[seller_id] * (1 - CommissionRate),
          seller_id,
        });
      }),
    );
    */

    return {
      products: cartProducts.map(p => ({id: p.id, product_id: p.product_id, status: p.status})),
      orders: orders.map(o => o.id),
    };
  },

  /*
  query{
    products: product[],
    returnOrder: order
  }
  */
  async returnOrderHooks(query, req) {
    const parentOrder = await req.Models.Order.findOne({
      where: {id: query.returnOrder.parent_id},
      raw: true,
    });

    const {
      relevantProducts,
      relevantProductsToReturn,
      relevantProductsToCancel,
      remainingProducts,
      seller_id,
      onlyShippingRemaining,
      shouldEntireOrderBeCancelled,
    } = await req.Models.Order.getReturnOrderExtendedDetails({
      modifiedProducts: query.products,
      return_order: query.returnOrder,
      seller_id: null,
      parentOrder,
    });

    await Controllers.sendReturnOrderConfirmationEmail(
      {
        returnOrder: query.returnOrder,
        products: relevantProductsToReturn,
        parentOrder,
        returnType: 'return',
        actor: Actors.buyer,
      },
      req,
    );
    await Controllers.sendReturnOrderConfirmationEmail(
      {
        returnOrder: query.returnOrder,
        products: relevantProductsToCancel,
        parentOrder,
        returnType: 'cancel',
        actor: Actors.buyer,
      },
      req,
    );

    const seller_ids = new Set(relevantProducts.map(p => p.seller_id).filter(i => !!i));

    for (const local_seller_id of seller_ids) {
      const {
        relevantProducts: local_relevantProducts,
        relevantProductsToReturn: local_relevantProductsToReturn,
        relevantProductsToCancel: local_relevantProductsToCancel,
        remainingProducts: local_remainingProducts,
        nothingRemaining: local_nothingRemaining,
        onlyShippingRemaining: local_onlyShippingRemaining,
        shouldEntireOrderBeCancelled: local_shouldEntireOrderBeCancelled,
        allProducts: local_allProducts,
      } = await req.Models.Order.getReturnOrderExtendedDetails({
        modifiedProducts: query.products,
        return_order: query.returnOrder,
        seller_id: local_seller_id,
        parentOrder,
      });

      const seller = await req.Models.Seller.findOne({where: {id: local_seller_id}, raw: true});

      if (seller.email) {
        await Controllers.sendReturnOrderConfirmationEmail(
          {
            returnOrder: query.returnOrder,
            products: local_relevantProductsToReturn,
            parentOrder,
            returnType: 'returnSeller',
            email: seller.email,
            actor: Actors.seller,
          },
          req,
        );
        await Controllers.sendReturnOrderConfirmationEmail(
          {
            returnOrder: query.returnOrder,
            products: local_relevantProductsToCancel,
            parentOrder,
            returnType: 'cancelSeller',
            email: seller.email,
            actor: Actors.seller,
          },
          req,
        );
      }

      const connections = await req.Models.SellerConnection.getConnectionMap({
        seller_id: local_seller_id,
        filter: {
          post_order: 1,
        },
      });

      if (connections?.brightpearl) {
        const BP = await getBPClient(
          req,
          connections?.brightpearl.account_name,
          connections?.brightpearl,
        );
        if (local_relevantProducts[0]?.remote_source === 'brightpearl') {
          if (local_shouldEntireOrderBeCancelled) {
            await BP.cancelOrder({
              remote_id: local_relevantProductsToCancel[0].remote_id,
              returnOrder: query.returnOrder,
            });
          } else if (local_relevantProductsToCancel.length) {
            await BP.cancelItemsInOrder({
              products: local_relevantProductsToCancel,
              remote_id: local_relevantProductsToCancel[0].remote_id,
              returnOrder: query.returnOrder,
            });
          }

          if (local_relevantProductsToReturn.length) {
            await BP.returnItemsInOrder({
              products: local_relevantProductsToReturn,
              remote_id: local_relevantProductsToReturn[0].remote_id,
              returnOrder: query.returnOrder,
              parentOrder,
            });
          }
        }
      }

      if (connections?.sellercloud) {
        const SC = await getSCClient(req, local_seller_id, connections?.sellercloud);
        if (local_relevantProducts[0]?.remote_source === 'sellercloud') {
          if (local_shouldEntireOrderBeCancelled) {
            await SC.cancelOrder({
              remote_id: local_relevantProductsToCancel[0].remote_id,
              returnOrder: query.returnOrder,
              products: local_relevantProductsToCancel,
            });
          } else if (local_relevantProductsToCancel.length) {
            await SC.cancelItemsInOrder({
              products: local_relevantProductsToCancel,
              remote_id: local_relevantProductsToCancel[0].remote_id,
              returnOrder: query.returnOrder,
            });
          }

          if (local_relevantProductsToReturn.length) {
            await SC.returnItemsInOrder({
              products: local_relevantProductsToReturn,
              remote_id: local_relevantProductsToReturn[0].remote_id,
              returnOrder: query.returnOrder,
            });
          }
        }
      }

      if (connections?.shipstation) {
        const SS = await getSSClient(req, local_seller_id, connections?.shipstation);
        if (local_relevantProducts[0]?.remote_source === 'shipstation') {
          const orderSeller = await req.Models.Order.getOrderDetails(
            parentOrder.id,
            local_seller_id,
          );
          if (local_shouldEntireOrderBeCancelled) {
            SS.postOrder({
              ...orderSeller,
              status: ShipStationOrderStates.cancelled,
            });
          } else if (local_relevantProductsToCancel.length) {
            SS.postOrder({
              ...orderSeller,
              products: local_remainingProducts,
            });
          }
        }
      }

      if (connections?.webhooks) {
        const WH = await getWHClient(req, local_seller_id, connections?.webhooks);
        await WH.postOrderUpdate({order_id: parentOrder.id});
      }
    }
  },

  async sendReturnOrderConfirmationEmail(
    {returnOrder, products, parentOrder, returnType, email, actor},
    req,
  ) {
    if (products.length) {
      try {
        await EmailControllers.sendReturnOrderConfirmationEmail(
          {
            order: returnOrder,
            products,
            parent: parentOrder,
            returnType,
            email,
            actor,
          },
          req,
        );
      } catch (e) {
        req.Logger.error('return-email-error', e);
      }
    }
  },
};

export default Controllers;

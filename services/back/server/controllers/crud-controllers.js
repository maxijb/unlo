import {arrayToMap} from '@Common/utils/generic-utils';
import {CartStatus, InviteTokenActions} from '@Common/constants/app';
import {RequestError, RequestErrorMessage} from '@Common/constants/errors';
import Seq from 'sequelize';
import isEmpty from 'is-empty';
import {
  ProductSourceNames,
  AccessoriesCategories,
  BillableTypes,
  OrderVisibleStatus,
} from '@Common/constants/app';
import EmailControllers from './email-controllers';
import {encode, decode} from '@Common/server/security/tokens';
import FormValidations from '@Common/constants/form-validations';
import {ValidateForm} from '@Common/utils/validations';
import {
  getIncredibleIdFromObjects,
  parseIncredibleId,
} from '../../../front/shared/utils/attributes-utils';
import fs from 'fs';
import csv from 'csv-parser';
import CartControllers from './cart-controllers';
import CompetitorsScript from '../scripts/csv-competitors-script';

const Controllers = {
  async attributesList(query, req) {
    return req.Models.Attribute.findAll();
  },
  async categoriesList(query, req) {
    const parent = query?.parent || null;

    return req.Models.Category.findAll({where: {parent}, order: ['year', 'order']});
  },
  async categoryGet(query, req) {
    return req.Models.Category.findOne({where: {id: query.id}});
  },
  async categoryImagesList(query, req) {
    const allImages = await req.Models.CategoryImage.findAll({
      where: {category_id: query.id},
      order: ['attr1', 'attr2', 'attr3', 'order'],
      raw: true,
    });
    const imageMap = await req.Models.Image.getMap(
      allImages.map(it => it.img_id),
      true,
    );

    return {allImages, imageMap};
  },
  async categoryCreate(query, req) {
    const created = await req.Models.Category.create(query);
    return created;
  },
  async categoryUpdate(query, req) {
    const {id, ...other} = query;
    await req.Models.Category.update(other, {where: {id}});
    return req.Models.Category.findOne({where: {id}});
  },

  async categoryDelete(query, req) {
    await req.Models.Category.destroy({where: {id: query.id}});
    return {ok: true};
  },

  async categoryImageCreate(query, req) {
    return req.Models.CategoryImage.create(query);
  },

  async imagesList(query, req) {
    const {limit = 20, offset = 0, search = ''} = query;
    return await req.Models.Image.findAndCountAll({
      where: !search ? {} : {name: {[Seq.Op.like]: `%${search}%`}},
      order: [['createdAt', 'DESC']],
      limit: Number(limit),
      offset: Number(offset),
    });
  },

  async categoryImageUpdate(query, req) {
    const {id, ...other} = query;
    await req.Models.CategoryImage.update(other, {where: {id}});
    return req.Models.CategoryImage.findOne({where: {id}});
  },

  async categoryImageDelete(query, req) {
    const {id} = query;
    await req.Models.CategoryImage.destroy({where: {id}});
    return {ok: true};
  },

  async productsList(query, req) {
    const user = query.isSitemap ? null : await req.Models.User.findOne({where: {id: req.auth.id}});
    const {
      category_id,
      limit = 20,
      offset = 0,
      attributes = [],
      source_id,
      buyBoxFilter,
      stockFilter,
    } = query;

    const childCats = category_id
      ? await req.Models.Category.getDeepChildCategories(category_id)
      : null;

    const categoryIds = category_id ? [category_id].concat(childCats).map(n => Number(n)) : null;

    let count;
    let products;
    if (!attributes?.length && !buyBoxFilter) {
      const where = category_id ? {category_id: categoryIds} : {};

      if (!query.isSitemap && !user.get('superAdmin')) {
        where.owner = user.get('seller_id');
      }

      if (source_id) {
        where.source_id = {
          [Seq.Op.like]: `%${source_id}%`,
        };
      }

      if (stockFilter === 'withStock') {
        where.stock = {[Seq.Op.gt]: 0};
      } else if (stockFilter === 'withoutStock') {
        where.stock = 0;
      }

      const {count: c, rows} = await req.Models.Product.findAndCountAll({
        where,
        limit: Number(limit),
        offset: Number(offset),
        attributes: ['id'],
        order: ['id'],
        raw: true,
      });
      count = c;
      products = rows;
    } else {
      const {count: c, rows} = await req.Models.Product.findAndCountAllWithAttibutes({
        owner: query.isSitemap || user?.get('superAdmin') ? null : user.get('seller_id'),
        attributes,
        source_id,
        categoryIds,
        limit: Number(limit),
        offset: Number(offset),
        buyBoxFilter,
        stockFilter,
      });
      count = c;
      products = rows;
    }

    const displayInfo = products?.length
      ? await req.Models.Product.getVariantsWithDisplayInformation(
          products.map(p => p?.id || null).filter(p => !!p),
          true,
        )
      : [];
    return {
      totalCount: count,
      products: displayInfo,
    };
  },

  async productsAllList(query, req) {
    const {count, rows: products} = await req.Models.Product.findAndCountAll({
      where: query.where || {},
      attributes: ['id'],
      order: ['id'],
      raw: true,
    });

    const displayInfo = products.length
      ? await req.Models.Product.getVariantsWithDisplayInformation(products.map(p => p.id))
      : [];
    return {
      totalCount: count,
      products: displayInfo,
    };
  },

  async productUpdate(query, req) {
    const model = await req.Models.Product.findOne({where: {id: query.id}});
    if (!model) {
      throw new RequestError(RequestErrorMessage.productNotFound);
    }
    model[query.field] = query.value;
    await model.save();
    return {ok: true};
  },
  async productCreate(query, req) {
    return req.Models.Product.create(query);
  },
  async productDelete(query, req) {
    const user = await req.Models.User.findOne({where: {id: req.auth.id}, raw: true});

    if (Array.isArray(query.id)) {
      throw new RequestError(RequestErrorMessage.validationError);
    }

    const where = {id: query.id};
    if (!user.superAdmin) {
      where.owner = user.seller_id;
    }

    const deleted = await req.Models.Product.destroy({where});

    if (deleted === 1) {
      await req.Models.AttributeProduct.destroy({where: {product_id: query.id}});
    }
    return {ok: true};
  },
  async attributeProductCreate(query, req) {
    return req.Models.AttributeProduct.create(query);
  },

  async attributeProductUpdate(query, req) {
    const {id, ...other} = query;
    await req.Models.AttributeProduct.update(other, {where: {id}});
    return req.Models.AttributeProduct.findOne({where: {id}});
  },

  async attributeProductDelete(query, req) {
    await req.Models.AttributeProduct.destroy({where: {id: query.id}});
    return {ok: true};
  },

  async ordersList(query, req) {
    const user = await req.Models.User.findOne({where: {id: req.auth.id}});
    if (!user.superAdmin) {
      query.seller_id = user.get('seller_id');
      query.isSeller = true;
    }

    if (query.visibleStatus) {
      query.visibleStatus = OrderVisibleStatus[query.visibleStatus];
    }
    return req.Models.Order.list(query);
  },

  async addReturnLabel(query, req) {
    if (!query.remote_return_label) {
      throw new RequestError(RequestErrorMessage.validationError);
    }

    return Controllers.updateOrderStatus(
      {
        ...query,
        newData: {
          remote_return_label: query.remote_return_label,
        },
        status: CartStatus.returnLabelSent,
        force: false,
      },
      req,
    );
  },

  async updateOrderStatus(query, req) {
    if (!query.order_id) {
      throw new RequestError(RequestErrorMessage.validationError);
    }

    const where = {
      order_id: query.order_id,
    };

    if (query.seller_id) {
      where.seller_id = query.seller_id;
    }

    const newData = {status: query.status};
    const carts = await req.Models.Cart.findAll({
      where,
      raw: true,
    });

    const cart_ids = await req.Models.Order.updateCartStatus(
      carts,
      newData.status,
      req,
      query.newData || {},
      typeof query.force === 'undefined' ? true : query.force,
    );

    return {
      ok: true,
      newData: {...newData, ...(query.newData || {})},
      cart_ids,
      order_id: query.order_id,
    };
  },

  async returnToSenderSeller(query, req) {
    return Controllers.updateOrderStatusBySeller(
      {
        ...query,
        previousStatus: CartStatus.inTransit,
        nextStatus: CartStatus.returnedToSenderSeller,
      },
      req,
    );
  },

  /* This endpoint is alwas called by other endpoints */
  async updateOrderStatusBySeller(query, req) {
    const where = {
      order_id: query.order_id,
      status: query.previousStatus,
      type: 'product',
      seller_id: req.seller_id,
    };

    const carts = await req.Models.Cart.findAll({
      where,
      raw: true,
    });

    const cart_ids = await req.Models.Order.updateCartStatus(carts, query.nextStatus, req);

    return {
      ok: true,
      newData: {status: query.nextStatus},
      cart_ids,
      order_id: query.order_id,
    };
  },

  async cancelOrderBySeller(query, req) {
    const user = await req.Models.User.findOne({where: {id: req.auth.id}});
    const where = {
      order_id: query.order_id,
      status: CartStatus.inOrder,
      type: 'product',
    };

    if (!user.superAdmin) {
      where.seller_id = user.get('seller_id');
    }

    const carts = await req.Models.Cart.findAll({
      where,
      raw: true,
    });

    const cart_ids = await req.Models.Order.updateCartStatus(
      carts,
      CartStatus.cancelledBySeller,
      req,
    );

    return {
      ok: true,
      newData: {status: CartStatus.cancelledBySeller},
      cart_ids,
      order_id: query.order_id,
    };
  },

  async saveTracking(query, req) {
    const user = await req.Models.User.findOne({where: {id: req.auth.id}});

    const response = await req.Models.Order.addTracking({
      order_id: query.order_id,
      cart_id: query.cart_id,
      seller_id: user.superAdmin ? query.seller_id || null : user.get('seller_id'),
      req,
      query,
    });

    return response;
  },

  async getNewSellerToken(query, req) {
    const data = {action: InviteTokenActions.newSeller};
    if (query.seller_id) {
      data.seller_id = query.seller_id;
    }
    const token = await encode(data, req.Config.get('server').session.secret, {
      expiresIn: '7d',
    });
    return {token, data};
  },

  async updateSeller(query, req) {
    const {seller_id} = await req.Models.User.findOne({
      where: {id: req.auth.id},
      attributes: ['seller_id'],
      raw: true,
    });

    const {
      address,
      shippingMethod,
      deleteShippingMethod,
      accessories,
      deleteAccessories,
      ...input
    } = query.input;
    const newData = {};
    if (address) {
      const addressObj = await req.Models.Address.upsertAddress({
        address,
        user: req.auth.id,
        type: 'seller',
      });
      input.address_id = addressObj.get('id');
    }

    if (shippingMethod) {
      newData.id = await req.Models.ShippingMethod.upsertMethodById(shippingMethod, seller_id);
    }

    if (accessories) {
      newData.id = await req.Models.Product.upsertProduct(
        accessories,
        seller_id,
        'seller_accessory',
        accessories?.source_id,
        true,
      );
    }

    if (deleteShippingMethod) {
      req.Models.ShippingMethod.destroy({where: {id: deleteShippingMethod, owner: seller_id}});
    }

    if (deleteAccessories) {
      req.Models.Product.destroy({where: {id: deleteAccessories, owner: seller_id}});
    }

    await req.Models.Seller.update(input, {
      where: {id: seller_id},
    });

    return {ok: true, ...newData};
  },

  async getSeller(query, req) {
    let seller_id = null;

    const usr = await req.Models.User.findOne({
      where: {id: req.auth.id},
      attributes: ['seller_id', 'superAdmin'],
      raw: true,
    });

    if (usr.superAdmin && query.seller_id) {
      seller_id = query.seller_id;
    } else {
      seller_id = usr.seller_id;
    }

    console.log('-----------------', usr, seller_id, query.seller_id);

    const seller = await req.Models.Seller.findOne({where: {id: seller_id}, raw: true});

    if (seller.address_id) {
      seller.address = await req.Models.Address.findOne({
        where: {id: seller.address_id},
        raw: true,
      });
    }

    seller.shippingPolicies = await req.Models.ShippingMethod.findAll({
      where: {seller_id},
    });

    seller.connections = await req.Models.SellerConnection.findAll({
      where: {seller_id},
      attributes: ['type', 'account_name', 'status', 'api_domain', 'token', 'refetch_token'],
    });

    seller.connections = arrayToMap(seller.connections, 'type');

    seller.accessoryCategories = await req.Models.Category.findAll({
      where: {id: AccessoriesCategories, active: 1},
      attributes: ['id', 'name', 'slug'],
      raw: true,
    });

    seller.accessories = await req.Models.Product.findAll({
      where: {category_id: AccessoriesCategories, active: 1, owner: seller_id},
      attributes: ['id', 'price', 'category_id', 'source_id'],
      raw: true,
    });

    seller.webhooks = await req.Models.SellerWebhook.findAll({
      where: {
        seller_id,
      },
    });

    return seller;
  },

  async sellerCreateProduct(query, req) {
    const errs = ValidateForm(query.input, FormValidations.newProductSellerServer);
    if (!isEmpty(errs)) {
      throw new RequestError(RequestErrorMessage.validationError, errs);
    }

    const {
      input: {category_id, price, stock, source_id, ...attrs},
    } = query;

    const user = await req.Models.User.findOne({where: {id: req.auth.id}, raw: true});
    const category = await req.Models.Category.findOne({where: {id: category_id}, raw: true});
    const attributes = await req.Models.Attribute.findAll({
      where: {id: Object.values(attrs)},
      raw: true,
    });

    const incredible_id = getIncredibleIdFromObjects(category, attributes);

    const product = await req.Models.Product.create({
      source_name: ProductSourceNames.portalManual,
      stock,
      price,
      source_id: source_id || null,
      category_id,
      visible: 1,
      active: 1,
      incredible_id,
      owner: user.seller_id,
    });

    await req.Models.AttributeProduct.bulkCreate(
      attributes.map(attr => ({
        attr_id: attr.id,
        product_id: product.id,
      })),
    );

    const fullProduct = await req.Models.Product.getVariantsWithDisplayInformation([product.id]);

    return {product: fullProduct[product.id]};
  },

  async loadSellerNames(query, req) {
    const sellers = await req.Models.Seller.findAll({
      raw: true,
      attributes: ['id', 'name'],
    });
    return {sellers: arrayToMap(sellers, 'id')};
  },

  async importIncredibleCSV(query, req) {
    const userObj = await req.Models.User.findOne({where: {id: req.auth.id}});
    if (!userObj.get('superAdmin')) {
      throw new RequestError('unauthorized');
    }
    const Input = fs.createReadStream(req.file.path);

    let i = 0;
    await new Promise((resolve, reject) => {
      Input.pipe(csv())
        .on('error', error => {
          console.error(error);
          errors.push(error);
        })
        .on('data', async row => {
          i++;
          //console.log(row['Regular price'], row['Meta: original price']);
          let item;

          item = {
            source_id: row[Object.keys(row)[0]],
            source_name: 'unlockd_wordpress',
            stock: row['Stock'] || 0,
            price: Number(row['Regular price']),
          };

          if (!item.source_id) {
            reject(new RequestError(`Line ${i} Source_id is a required field`));
          }

          const prod = await req.Models.Product.findOne({
            where: {source_id: item.source_id, source_name: 'unlockd_wordpress'},
            logging: false,
          });
          if (prod) {
            prod.stock = item.stock;
            prod.price = item.price;
            prod.original_price = item.original_price;
            try {
              await prod.save({logging: false});
            } catch (e) {
              reject(new RequestError(`Line ${i} ${e.message}`));
            }
          } else {
            reject(new RequestError(`Line ${i} Not found ${item.source_id}`));
          }
        })
        .on('end', () => {
          resolve();
        });
    });
    return {ok: true};
  },

  async importCompetitorsCSV(query, req) {
    CompetitorsScript(req.file.path);
    return {ok: true};
  },

  async importSellerCSV(query, req) {
    const user = await req.Models.User.findOne({where: {id: req.auth.id}, raw: true});
    if (user.superAdmin) {
      return Controllers.importIncredibleCSV(query, req);
    }

    const Input = fs.createReadStream(req.file.path);

    const [categories, attributes, seller] = await Promise.all([
      req.Models.Category.findAll({attributes: ['slug', 'id'], raw: true}),
      req.Models.Attribute.findAll({attributes: ['slug', 'id'], raw: true}),
      req.Models.Seller.findOne({where: {id: user.seller_id}, raw: true}),
    ]);

    // This is used for sellers that want to reset their product stocks
    // when the ID is not listed in the feed
    let reset_ids = new Set();
    if (seller.empty_null_products) {
      const allProducts = await req.Models.Product.findAll({
        where: {
          owner: seller.id,
        },
        attributes: ['id'],
      });
      reset_ids = new Set(allProducts.map(p => p.id));
    }

    const catsBySlug = arrayToMap(categories, 'slug');
    const attrsBySlug = arrayToMap(attributes, 'slug');
    const session_id = `${user.seller_id}-${Date.now()}`;
    const errors = [];
    const rows = [];
    let i = 0;

    await new Promise((resolve, reject) => {
      Input.pipe(
        csv({
          mapHeaders: ({header}) =>
            header.toLowerCase().replace(/\s/g, '').replace(/-/g, '').replace(/_/g, ''),
        }),
      )
        .on('headers', headers => {
          if (!headers.includes('incredibleid') && !headers.includes('id')) {
            reject(new RequestError('Either `Incredible_id` or `id` are required fields'));
          }
        })
        .on('error', error => {
          Logger.error(error);
          reject(new RequestError(error.message));
        })
        .on('data', async row => {
          rows.push(row);
        })
        .on('finish', () => {
          resolve();
        });
    });

    for (const row of rows) {
      i++;
      // if (!Number(row.price) && Number(row.stock)) {
      //   reject(
      //     new RequestError(`(Line ${i}) Price cannot be empty if the product is in stock`),
      //   );
      // }
      if (!row.incredibleid && !row.id) {
        errors.push(`(Line ${i}) 'IncredibleId' and 'Id' cannot be both empty`);
        continue;
      }

      let category, attributes, attr_ids;
      if (row.incredibleid) {
        const {category: cat, attributes: attrib} = parseIncredibleId(row.incredibleid);
        category = cat;
        attributes = attrib;

        if (!catsBySlug.hasOwnProperty(category)) {
          errors.push(`(Line ${i}) Unkonwn category slug ${category}`);
          continue;
        }

        let should_continue = false;
        attr_ids = attributes.map(at => {
          if (!attrsBySlug.hasOwnProperty(at.toLowerCase())) {
            errors.push(`(Line ${i}) Unkonwn attribute slug ${at}`);
            should_continue = true;
            return;
          }
          return attrsBySlug[at.toLowerCase()].id;
        });
        if (should_continue) {
          continue;
        }
      }

      const where = {
        owner: user.seller_id,
      };

      if (row.incredibleid) {
        where.incredible_id = row.incredibleid;
      }
      if (row.id) {
        where.source_id = row.id;
      }

      let product = await req.Models.Product.findOne({
        where,
      });

      if (!product) {
        if (!row.incredibleid) {
          errors.push(
            `(Line ${i}) Id ${row.id} is not mapped to any incredible_id and has been ignored.`,
          );
          //console.log(errors);
          continue;
        }

        const origPriceProduct = await req.Models.Product.findOne({
          where: {
            incredible_id: String(row.incredibleid).toLowerCase(),
            original_price: {
              [Seq.Op.ne]: null,
            },
          },
          raw: true,
          attributes: ['original_price'],
        });

        product = await req.Models.Product.create(
          {
            source_name: ProductSourceNames.sellerCSVUpload,
            source_id: row.id,
            price: row.price || 0,
            stock: row.stock || 0,
            original_price: origPriceProduct?.original_price || null,
            incredible_id: String(row.incredibleid).toLowerCase(),
            owner: user.seller_id,
            category_id: catsBySlug[category].id,
          },
          {
            ignoreDuplicates: true,
          },
        );
        if (product) {
          await req.Models.AttributeProduct.destroy({
            where: {
              product_id: product.id,
            },
          });
          await req.Models.AttributeProduct.bulkCreate(
            attr_ids.map(attr_id => ({
              attr_id,
              product_id: product.id,
            })),
            {
              ignoreDuplicates: true,
            },
          );
        }
      } else {
        product.stock = row.stock || 0;
        product.price = row.price || 0;
        product.source_id = row.id;
        product.source_name = ProductSourceNames.sellerCSVUpload;
        await product.save();
        reset_ids.delete(product.get('id'));
      }
    }

    // This is used for sellers that want to reset their product stocks
    // when the ID is not listed in the feed
    if (seller.empty_null_products && reset_ids.size) {
      errors.unshift(
        `Resetting ${reset_ids.size} products to stock 0 -- Example IDs: ${Array.from(reset_ids)
          .slice(0, 3)
          .toString()}`,
      );
      // reset stock to zero if ID hasn't been toched
      await req.Models.Product.update(
        {stock: 0},
        {
          where: {
            id: Array.from(reset_ids),
          },
        },
      );
    }
    return {ok: true, session_id, errors: errors.slice(0, 100), totalErrors: errors.length};
  },

  async sellersList(query, req) {
    const sellers = await req.Models.Seller.findAll();
    return {sellers: arrayToMap(sellers)};
  },

  async billablesList(query, req) {
    const {limit = 20, offset = 0} = query;
    const where = {};
    const user = await req.Models.User.findOne({where: {id: req.auth.id}});

    if (user.get('superAdmin')) {
      where.seller_id = query.seller;
    } else if (user.get('seller_id')) {
      where.seller_id = user.get('seller_id');
    } else {
      return {rows: [], count: 0};
    }

    return req.Models.Billable.findAndCountAll({
      where,
      limit: Number(limit),
      offset: Number(offset),
      order: [['id', 'DESC']],
    });
  },

  async saveBillable(query, req) {
    const {seller_id, type, credit, debit, description = ''} = query;
    if ((!Number(credit) && !Number(debit)) || !Number(seller_id) || !type) {
      throw new RequestError(RequestErrorMessage.validationError);
    }

    const billable = await req.Models.Billable.add({
      seller_id,
      credit: Number(credit) || 0,
      debit: Number(debit) || 0,
      description,
      type,
    });
    return {billable};
  },

  async setHooks(query, req) {
    const {hooks = {}} = query;
    const user = await req.Models.User.findOne({where: {id: req.auth.id}});
    const seller_id = user.get('seller_id');

    if (!seller_id) {
      throw new RequestError(RequestError.validationError);
    }

    await req.Models.SellerWebhook.destroy({where: {seller_id}});
    const newHooks = Object.keys(hooks)
      .map(key => {
        if (hooks[key]) {
          return {
            seller_id,
            type: key,
            uri: hooks[key],
          };
        }
        return null;
      })
      .filter(e => !!e);
    await req.Models.SellerWebhook.bulkCreate(newHooks);

    return {ok: true, newHooks};
  },
};

export default Controllers;

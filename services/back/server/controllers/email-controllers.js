import React from 'react';
import ReactDom from 'react-dom/server';
import isEmpty from 'is-empty';
import {encode, decode} from '@Common/server/security/tokens';
import FormValidations from '@Common/constants/form-validations';
import {ValidateForm} from '@Common/utils/validations';
import {arrayToMap} from '@Common/utils/generic-utils';
import Logger from '@Common/logger';
import {
  RequestError,
  RequestErrorMessage,
  AuthenticationError,
  AuthenticationErrorMessage,
} from '@Common/constants/errors';
import {Entities, AssetStatus, AssetRoleHierarchy} from '@Common/constants/assets';
import {DefaultLang} from '@Common/constants/input';
import {waitMS} from '@Common/utils/generic-utils';
import {formatUserAppRel} from '@Common/utils/assets-utils';
import getRedis from '../redis/redis';
import EmailTracking from '../../../front/shared/components/email/email-tracking';
import EmailOrder, {TextEmailOrder} from '../../../front/shared/components/email/email-order';
import EmailAbandonedCart from '../../../front/shared/components/email/email-abandoned-cart';
import EmailReturnLabel from '../../../front/shared/components/email/email-return-label';
import EmailReturnOrder, {
  TextEmailReturnOrder,
} from '../../../front/shared/components/email/email-return-order';
import EmailTradeInOrder, {
  TextEmailTradeInOrder,
} from '../../../front/shared/components/email/email-tradein-order';
import EmailContactUs from '../../../front/shared/components/email/email-contact-us';

import {sendEmail} from '../services/sendgrid';
import {inviteTeammateTemplate} from '../email/templates';
import {htmlTemplate, baseEmailTemplate, txtTemplate, p} from '../email/base-template';

const Controllers = {
  async send(query, req) {
    const data = await getDataForEmail(query, req);
    const template = await baseEmailTemplate(data, req);
    await sendEmail(template, req);
  },

  async testPreview(query, req) {
    const {type = 'order', id, format, seller_id, actor} = query;

    let body = '';

    if (type === 'tracking') {
      const order = await req.Models.Order.getOrderDetails(id, seller_id);
      body = ReactDom.renderToStaticMarkup(
        <EmailTracking t={req.t} order={order} config={req.Config.get('server')} />,
      );
    } else if (type === 'order') {
      const order = await req.Models.Order.getOrderDetails(id, seller_id);
      body = ReactDom.renderToStaticMarkup(
        <EmailOrder
          t={req.t}
          order={order}
          seller_id={seller_id}
          actor={actor}
          config={req.Config.get('server')}
        />,
      );

      if (format === 'txt') {
        const message = TextEmailOrder({order, t: req.t, config: req.Config.get('server')});
        return txtTemplate({message}, req);
      }
    } else if (type === 'abandon') {
      const contacts = await req.Models.Contact.findAll({where: {cid: query.cid}, raw: true});
      const carts = await req.Models.Cart.findAll({where: {cid: query.cid, status: 1}, raw: true});
      const products = await req.Models.Product.findAll({
        where: {
          id: carts.map(c => c.product_id),
        },
        raw: true,
      });
      const productsMap = arrayToMap(products, 'id');

      carts.forEach(c => {
        c.product = productsMap[c.product_id];
      });

      body = ReactDom.renderToStaticMarkup(
        <EmailAbandonedCart
          t={req.t}
          order={{products: carts, contact: contacts[0], promoCode: 'INC5OFF'}}
          config={req.Config.get('server')}
        />,
      );
      //return {contacts, carts};
    }
    if (type === 'return') {
      const order = await req.Models.Order.getReturnOrderDetails(id);
      body = ReactDom.renderToStaticMarkup(
        <EmailReturnOrder t={req.t} order={order} config={req.Config.get('server')} />,
      );

      if (format === 'txt') {
        const message = TextEmailReturnOrder({order, t: req.t, config: req.Config.get('server')});
        return txtTemplate({message}, req);
      }
    }
    return htmlTemplate({body, subject: req.t('techSpecs')}, req);
  },

  async sendContact(query, req) {
    const template = await baseEmailTemplate(
      {
        sendTo: ['contact@incrediblephones.com'],
        subject: `[Required Action] Incredible Contact Us Submission`,
        text: txtTemplate(
          {
            message: 'View html version',
          },
          req,
        ),
        html: ReactDom.renderToStaticMarkup(
          <EmailContactUs t={req.t} query={query} config={req.Config.get('server')} />,
        ),
      },
      req,
    );
    await sendEmail(template, req);
  },

  async sendOrderConfirmationEmailSeller(query, req, order = null) {
    if (!order) {
      order = await req.Models.Order.getOrderDetails(query.id, query.seller_id);
    }
    const seller = await req.Models.Seller.findOne({where: {id: query.seller_id}, raw: true});

    if (seller.email) {
      const template = await baseEmailTemplate(
        {
          sendTo: [seller.email],
          subject: `New Incredible Order: ${order.order.id} `,
          text: txtTemplate(
            {
              message: TextEmailOrder({
                order,
                t: req.t,
                config: req.Config.get('server'),
                actor: 'seller',
              }),
            },
            req,
          ),
          html: ReactDom.renderToStaticMarkup(
            <EmailOrder t={req.t} order={order} config={req.Config.get('server')} actor="seller" />,
          ),
        },
        req,
      );
      await sendEmail(template, req);
    }
  },

  async sendAbandonedCartEmail(query, req) {
    if (query.contact?.email) {
      const template = await baseEmailTemplate(
        {
          sendTo: [query.contact.email],
          subject: req.t('dashboard:email.abandonSubject'),

          // text: txtTemplate(
          //   {
          //     message: TextEmailOrder({
          //       order,
          //       t: req.t,
          //       config: req.Config.get('server'),
          //       actor: 'seller',
          //     }),
          //   },
          //   req,
          // ),
          html: ReactDom.renderToStaticMarkup(
            <EmailAbandonedCart
              t={req.t}
              order={{products: query.carts, contact: query.contact, promoCode: query.promoCode}}
              config={req.Config.get('server')}
            />,
          ),
        },
        req,
      );
      await sendEmail(template, req);
    }
  },

  async sendReturnLabel(query, req) {
    const {order_id, seller_id, newStatus, cart_ids} = query;
    const order = await req.Models.Order.findOne({
      where: {id: order_id},
      raw: true,
    });
    const carts = await req.Models.Cart.findAll({
      where: {
        seller_id,
        id: cart_ids,
        status: newStatus,
      },
      raw: true,
    });

    const template = await baseEmailTemplate(
      {
        sendTo: [order.email],
        subject: req.t('dashboard:email.returnLabelSubject', {order_id}),

        html: ReactDom.renderToStaticMarkup(
          <EmailReturnLabel
            t={req.t}
            order={{products: carts, order}}
            config={req.Config.get('server')}
          />,
        ),
      },
      req,
    );
    await sendEmail(template, req);
  },

  async sendOrderTrackingEmail(query, req) {
    const order = await req.Models.Order.getOrderDetails(query.id, query.seller_id);

    const template = await baseEmailTemplate(
      {
        sendTo: [order.order.email],
        subject: req.t('dashboard:email.orderShipped'),

        html: ReactDom.renderToStaticMarkup(
          <EmailTracking t={req.t} order={order} config={req.Config.get('server')} />,
        ),
      },
      req,
    );
    await sendEmail(template, req);
  },

  async sendOrderConfirmationEmail(query, req, order) {
    if (!order) {
      order = await req.Models.Order.getOrderDetails(query.id);
    }
    const template = await baseEmailTemplate(
      {
        sendTo: [order.order.email, 'notifications@incrediblephones.com'],
        subject: `Incredible Order Confirmation: ${order.order.id} `,
        bcc: ['incrediblephones.com+582fd73407@invite.trustpilot.com'],
        text: txtTemplate(
          {
            message: TextEmailOrder({order, t: req.t, config: req.Config.get('server')}),
          },
          req,
        ),
        html: ReactDom.renderToStaticMarkup(
          <EmailOrder t={req.t} order={order} config={req.Config.get('server')} />,
        ),
      },
      req,
    );

    await sendEmail(template, req);
  },

  async sendReturnOrderConfirmationEmail(query, req, order, actor) {
    const template = await baseEmailTemplate(
      {
        sendTo: query.email
          ? [query.email]
          : [query.parent.email, 'notifications@incrediblephones.com'],
        subject: `Incredible Order ${
          query.returnType.startsWith('return') ? 'Return' : 'Cancellation'
        } Confirmation: ${query.parent?.id} `,
        text: txtTemplate(
          {
            message: TextEmailReturnOrder({
              query,
              t: req.t,
              config: req.Config.get('server'),
              actor,
            }),
          },
          req,
        ),
        html: ReactDom.renderToStaticMarkup(
          <EmailReturnOrder
            t={req.t}
            order={query}
            config={req.Config.get('server')}
            actor={actor}
          />,
        ),
      },
      req,
    );

    await sendEmail(template, req);
  },

  async sendTradeInOrderConfirmationEmail(query, req) {
    const order = await req.Models.Order.getOrderDetails(query.id);

    if (order?.order?.type !== 'trade-in') {
      throw new RequestError(RequestErrorMessage.validationError);
    }

    const {order: tradeInOrder, products: tradeInProducts} = order;
    const template = await baseEmailTemplate(
      {
        sendTo: [tradeInOrder.email, 'notifications@incrediblephones.com'],
        subject: `Incredible Order Confirmation: ${tradeInOrder.id} `,
        text: txtTemplate(
          {
            message: TextEmailTradeInOrder({
              tradeInOrder,
              tradeInProducts,
              t: req.t,
              config: req.Config.get('server'),
            }),
          },
          req,
        ),
        html: ReactDom.renderToStaticMarkup(
          <EmailTradeInOrder
            t={req.t}
            tradeInOrder={tradeInOrder}
            tradeInProducts={tradeInProducts}
            config={req.Config.get('server')}
          />,
        ),
      },
      req,
    );
    await sendEmail(template, req);
  },
};

export default Controllers;

import Logger from '@Common/logger';
import {SellerConnectionStatus, CartStatus} from '@Common/constants/app';
import {RequestError, RequestErrorMessage} from '@Common/constants/errors';
import {getClient} from '../services/finale-inventory';

const Controllers = {
  async createConnection(query, req) {
    if (!query.account_name || !query.token || !query.api_domain || !query.refetch_token) {
      throw new RequestError(RequestErrorMessage.validationError);
    }

    let instance = await req.Models.SellerConnection.findOne({
      where: {
        type: 'finale',
        seller_id: req.seller_id,
      },
    });

    if (instance) {
      instance.account_name = query.account_name;
      instance.token = query.token;
      instance.api_domain = query.api_domain;
      instance.refetch_token = query.refetch_token;
      await instance.save();
    } else {
      instance = await req.Models.SellerConnection.create({
        type: 'finale',
        seller_id: req.seller_id,
        account_name: query.account_name,
        token: query.token,
        api_domain: query.api_domain,
        refetch_token: query.refetch_token,
      });
    }

    const FI = await getClient(req, req.seller_id);
    const token = await FI.getAuthToken();

    return {id: instance.get('id'), ok: !!token};
  },

  async getAuthToken(query, req) {
    const FI = await getClient(req);
    return FI.getAuthToken();
  },

  async getProduct(query, req) {
    const FI = await getClient(req);
    return await FI.getProduct(query);
  },

  async getProducts(query, req) {
    const FI = await getClient(req);
    return await FI.getProducts(query);
  },

  async getProductStore(query, req) {
    const FI = await getClient(req);
    return await FI.getProductStore(query);
  },
  async getProductConnection(query, req) {
    const FI = await getClient(req);
    return await FI.getProductConnection(query);
  },
  async getInventory(query, req) {
    const FI = await getClient(req);
    return await FI.getInventory(query);
  },

  async getProductPrice(query, req) {
    const BP = await getClient(req);
    const result = await BP.getProductPrice(query);
    return result;
  },

  async getPriceLists(query, req) {
    const BP = await getClient(req);
    const result = await BP.getPriceLists(query);
    return result;
  },

  async getShippingMethods(query, req) {
    const BP = await getClient(req);
    const result = await BP.getShippingMethods(query);
    return result;
  },

  async getProductSearch(query, req) {
    const BP = await getClient(req);
    const result = await BP.getProductSearch(query);
    return result;
  },

  async getProductAvailability(query, req) {
    const BP = await getClient(req);
    const result = await BP.getProductAvailability(query);
    return result;
  },

  async getWarehouses(query, req) {
    const BP = await getClient(req);
    const result = await BP.getWarehouses(query);
    return result;
  },

  async syncInventory(query, req) {
    Controllers.syncInventoryPrice(query, req);
    Controllers.syncInventoryStock(query, req);
    Controllers.setInventoryWebhook(query, req);
    return {ok: true};
  },
  async syncInventoryStock(query, req) {
    const BP = await getClient(req);

    const priceOptions = await BP.getProductPrice({});

    for (const option of priceOptions.response.getUris) {
      const parts = option.split('/');
      const range = parts[parts.length - 1];
      const avail = await BP.getProductAvailability({id: range});

      for (const source_id of Object.keys(avail.response)) {
        const row = avail.response[source_id];
        const quantity = Number(row.total.onHand);
        if (source_id && !Number.isNaN(quantity)) {
          await req.Models.Product.update(
            {stock: quantity},
            {
              where: {
                source_id,
                owner: req.seller_id,
              },
            },
          );
        }
      }
    }

    return {ok: true};
  },

  async syncInventoryPrice(query, req) {
    const BP = await getClient(req);
    const lists = await BP.getPriceLists(query);
    const list_id = lists.response.filter(
      l => l.code === 'RETAIL' && l.priceListTypeCode === 'SELL',
    )[0]?.id;

    if (!list_id) {
      throw new RequestError('Retail price list not found');
    }

    const priceOptions = await BP.getProductPrice({});

    for (const option of priceOptions.response.getUris) {
      const parts = option.split('/');
      const range = parts[parts.length - 1];
      const prices = await BP.getProductPrice({range, list_id});

      for (const row of prices.response) {
        const source_id = row.productId;
        const price = Number(row.priceLists[0]?.quantityPrice?.[1]);
        if (source_id && !Number.isNaN(price)) {
          await req.Models.Product.update(
            {price},
            {
              where: {
                source_id,
                owner: req.seller_id,
              },
            },
          );
        }
      }
    }
    return {ok: true};
  },

  async setInventoryWebhook(query, req) {
    const BP = await getClient(req);
    const result = await BP.listWebhooks(query);

    for (const rule of result?.response || []) {
      if (
        rule.subscribeTo === 'product.modified.on-hand-modified' ||
        rule.subscribeTo === 'order.modified.fully-shipped' ||
        rule.subscribeTo === 'goods-out-note.modified'
      ) {
        await BP.deleteWebhook({id: rule.id});
      }
    }

    const hook = {
      subscribeTo: 'product.modified.on-hand-modified',
      httpMethod: 'GET',
      uriTemplate:
        'https://incrediblephones.com/api/public/brightpearl/inventory-hook?accountCode=${account-code}&id=${resource-id}&event=${lifecycle-event}&resource=${resource-type}',
      idSetAccepted: true,
      qualityOfService: 1,
    };
    const response = await BP.setWebhook(hook);

    const hookGoods = {
      subscribeTo: 'order.modified.fully-shipped',
      httpMethod: 'GET',
      uriTemplate:
        'https://incrediblephones.com/api/public/brightpearl/order-shipped-hook?accountCode=${account-code}&id=${resource-id}&event=${lifecycle-event}&resource=${resource-type}',
      idSetAccepted: false,
      qualityOfService: 1,
    };
    await BP.setWebhook(hookGoods);

    // Re-enable in case we want to subscribe to this event
    // hookGoods.subscribeTo = 'goods-out-note.modified';
    // await BP.setWebhook(hookGoods);

    return {ok: true, hook: response};
  },

  async executeOrderShippedHook(query, req) {
    Controllers.updateOrderShipped(query, req);
    return {ok: true};
  },

  async listWebhooks(query, req) {
    const BP = await getClient(req);
    const result = await BP.listWebhooks(query);
    return result;
  },

  async executeInventoryHook(query, req) {
    Controllers.updateStock(query, req);
    return {ok: true};
  },

  async updateOrderShipped(query, req) {
    if (!query.id) {
      throw new RequestError(RequestErrorMessage.missingInteractionId);
    }

    const sellerConfig = await req.Models.SellerConnection.get({
      type: 'brightpearl',
      account_name: query.accountCode,
    });

    if (!sellerConfig?.seller_id) {
      throw new RequestError(RequestErrorMessage.missingUserId);
    }

    const products = await req.Models.Cart.findAll({
      where: {
        remote_source: 'brightpearl',
        remote_id: query.id,
        seller_id: sellerConfig.seller_id,
      },
      raw: true,
    });

    if (!products?.length) {
      return {ok: false};
    }

    const BP = await getClient(req, query.accountCode, sellerConfig);
    const goodNotes = await BP.getGoodsOutNotes(query);

    const notesIds = Object.keys(goodNotes.response);
    let reference = null;
    for (const noteId of notesIds) {
      reference = goodNotes.response[noteId]?.shipping?.reference || reference;
    }

    if (reference) {
      const {shippingMethod} = await req.Models.Order.getShippingMethodForSellerFromCart(
        products,
        sellerConfig.seller_id,
      );

      await req.Models.Order.addTracking({
        order_id: products[0].order_id,
        query: {
          tracking_carrier: shippingMethod?.carrier,
          tracking: reference,
        },
        seller_id: sellerConfig.seller_id,
        req,
      });
    }
  },
  async updateStock(query, req) {
    const sellerConfig = await req.Models.SellerConnection.findOne({
      where: {
        type: 'brightpearl',
        account_name: query.accountCode,
      },
      raw: true,
      attributes: ['seller_id'],
    });

    if (!sellerConfig?.seller_id) {
      throw new RequestError(RequestErrorMessage.missingUserId);
    }

    const availability = await Controllers.getProductAvailability(query, req, query.accountCode);
    for (const id of Object.keys(availability.response)) {
      const onHand = availability.response[id]?.total?.onHand;
      if (typeof onHand !== 'undefined' && onHand !== null) {
        await req.Models.Product.update(
          {stock: availability.response[id].total.onHand},
          {
            where: {
              owner: sellerConfig.seller_id,
              source_id: id,
            },
          },
        );
      }
    }
    return availability;
  },

  async cronSyncBrightpearlPrices(query, req) {
    Controllers.executeCronSyncBrightpearlPrices(query, req);
    return {ok: true};
  },

  async executeCronSyncBrightpearlPrices(query, req) {
    const connections = await req.Models.SellerConnection.findAll({
      where: {type: 'brightpearl', status: SellerConnectionStatus.active},
      raw: true,
    });

    for (const connection of connections) {
      req.seller_id = connection.seller_id;
      await Controllers.syncInventoryPrice(query, req);
    }
  },
};

export default Controllers;

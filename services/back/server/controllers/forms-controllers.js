import isEmpty from 'is-empty';
import {encode, decode} from '@Common/server/security/tokens';
import {clientCookieName} from '@Common/constants/app';
import FormValidations from '@Common/constants/form-validations';
import {ValidateForm} from '@Common/utils/validations';
import Logger from '@Common/logger';
import {
  RequestError,
  RequestErrorMessage,
  AuthenticationError,
  AuthenticationErrorMessage,
} from '@Common/constants/errors';
import {Entities, AssetStatus, AssetRoleHierarchy} from '@Common/constants/assets';
import {DefaultLang} from '@Common/constants/input';
import {waitMS} from '@Common/utils/generic-utils';
import {formatUserAppRel} from '@Common/utils/assets-utils';
import getRedis from '../redis/redis';

import {sendEmail} from '../services/sendgrid';
import {inviteTeammateTemplate} from '../email/templates';
import {htmlTemplate, baseEmailTemplate, txtTemplate, p} from '../email/base-template';
import EmailControllers from './email-controllers';

const Controllers = {
  async contact(query, req) {
    const errors = ValidateForm(query, FormValidations.contact);
    if (!isEmpty(errors)) {
      throw new RequestError(RequestErrorMessage.validationError, errors);
    }

    const {firstName, lastName, email, phone, notes} = query;
    await EmailControllers.sendContact(query, req);

    return {ok: true};
  },
};

export default Controllers;

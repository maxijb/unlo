/* This file is only included serve-side
 * It's the main entry point for all controllers exposed in the API
 */
import UserControllers from './user-controllers';
import ABControllers from './ab-controllers';
import PaymentsControllers from './payments-controllers';

import AppControllers from './app-controllers';
import EmailControllers from './email-controllers';
import ReportControllers from './report-controllers';
import ProductControllers from './product-controllers';
import CartControllers from './cart-controllers';
import OrderControllers from './order-controllers';
import CrudControllers from './crud-controllers';
import FormsControllers from './forms-controllers';
import PublicControllers from './public-controllers';
import BrightpearlControllers from './brightpearl-controllers';
import SellercloudControllers from './sellercloud-controllers';
import ShipStationControllers from './shipstation-controllers';
import FinaleControllers from './finale-controllers';
import i18n from 'i18next';

const Controllers = {
  ok: async (query, req) => {
    return 'ok';
  },
  okJson: async (query, req) => {
    return {status: 'ok', ok: true};
  },
  // ---------------------------- Sample controllers -----------------
  getId: async ({id}, req) => {
    const app = await req.Models.Asset.findOne({where: {id: 1}});

    const abVar = req.ab.track('test_run');

    return {
      test: id || 323,
      app: app.get(),
      activeAB: req.activeAB,
      abVar,
      translatedText: req.t('common:pureComponent'),
    };
  },

  getError: async (query, req) => {
    return {
      statusCode: 429,
      error: {
        name: 'rate_limiter',
        message: 'over quota',
        data: [90, 23],
      },
    };
  },

  v2GetId: async (query, req) => {
    return {test: 90};
  },

  v2GetUser: ({user}, req) => Promise.resolve({user, id: 30}),

  // ---------------------------- Actual controllers -----------------

  user: UserControllers,
  ab: ABControllers,

  payments: PaymentsControllers,

  app: AppControllers,

  product: ProductControllers,
  cart: CartControllers,

  email: EmailControllers,
  report: ReportControllers,
  order: OrderControllers,
  crud: CrudControllers,
  forms: FormsControllers,
  brightpearl: BrightpearlControllers,
  sellercloud: SellercloudControllers,
  shipstation: ShipStationControllers,
  public: PublicControllers,
  finale: FinaleControllers,
};

export default Controllers;

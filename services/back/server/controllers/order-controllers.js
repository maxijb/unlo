import {RequestError, RequestErrorMessage} from '@Common/constants/errors';
import {arrayToMap} from '@Common/utils/generic-utils';
import {safeJsonParse} from '@Common/utils/json';

const Controllers = {
  async listOrdersForUser(query, req) {
    if (!req.auth?.id) {
      throw new RequestError(RequestErrorMessage.missingUserId);
    }

    const orders = await req.Models.Order.listOrdersForUser(req);

    const carts = await req.Models.Cart.findAll({
      where: {order_id: orders.map(o => o.id)},
      order: [['order_id', 'DESC']],
      raw: true,
    });

    const products = await req.Models.Product.findAll({
      where: {id: carts.map(c => c.product_id)},
      raw: true,
      attributes: ['category_id', 'id'],
    });
    const productCategoryMap = products.reduce((agg, p) => {
      agg[p.id] = p.category_id;
      return agg;
    }, {});

    let cartIndex = 0;
    for (const order of orders) {
      order.products = [];
      while (cartIndex < carts.length && carts[cartIndex].order_id === order.id) {
        const item = carts[cartIndex];
        if (item.type === 'trade-in' && item.special_item) {
          item.selections = safeJsonParse(item.special_item);
        }
        order.products.push(item);
        item.category_id = productCategoryMap[item.product_id];
        cartIndex++;
      }
    }

    const addresses = await req.Models.Address.getPublicFieldsBatch(orders.map(o => o.address_id));

    return {orders, products, addresses: arrayToMap(addresses)};
  },

  async getReturnOrder(query, req) {
    const orders = await req.Models.Order.findAll({
      where: {
        type: 'return',
        parent_id: query.id,
      },
      attributes: ['notes', 'id'],
      raw: true,
    });

    return {
      orders: orders.map(o => {
        const {notes, ...other} = o;
        return {
          ...other,
          ...safeJsonParse(o.notes),
        };
      }),
    };
  },
};

export default Controllers;

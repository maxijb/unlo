import Logger from '@Common/logger';
import {clientCookieName} from '@Common/constants/app';
import {
  RequestError,
  AuthenticationError,
  RequestErrorMessage,
  AuthenticationErrorMessage,
} from '@Common/constants/errors';
import {OrderStatus} from '@Common/constants/app';

import {getClient} from '../services/paypal';
import CartControllers from './cart-controllers';
import Paypal from 'paypal-rest-sdk';
const payments = Paypal.v1.payments;

const Controllers = {
  async getPaypalUrl(query, req) {
    const {
      ok,
      tax,
      total,
      subtotal,
      taxRate,
      cid,
      userId,
      reference,
      addressObj,
      shippingAddressObj,
      subtotalProducts,
      products,
      cart,
      errors,
    } = await CartControllers.prePlaceOrder(query, req);

    if (!ok) {
      return {ok, errors};
    }

    const order = await req.Models.Order.create({
      payment_type: 'paypal',
      payment_description: 'Paypal',
      address_id: addressObj.get('id'),
      shipping_address_id: !shippingAddressObj ? null : shippingAddressObj.get('id'),
      status: OrderStatus.pending,
      subtotal: subtotalProducts,
      subtotal_aggregates: subtotal,
      tax,
      tax_rate: taxRate,
      total,
      user_id: req.auth?.id,
      cid,
      email: query.email,
      firstName: query.firstName,
      lastName: query.lastName,
      phone: query.phone,
    });

    const client = getClient(req);
    try {
      const response = await client.createOrder({
        total,
        query,
        tempOrderId: order.get('id'),
        address: addressObj.get(),
      });
      const url = response.links.find(it => it.rel === 'approve')?.href;
      order.paypal_payment_id = response.id;
      await order.save();
      return {total, approveUrl: url, ok: true};
    } catch (e) {
      Logger.error(e);
      return {ok: false, errors: [RequestErrorMessage.paymentError]};
    }
  },

  async capturePaypalOrder(query, req) {
    const client = getClient(req);
    const data = await client.getOrder({id: query.token});
    const order = await req.Models.Order.isOrderMine(query.tempOrderId, req, false);

    Logger.info('paypal-get-order-payment', data);

    const addressObj = await req.Models.Address.findOne({where: {id: order.get('address_id')}});
    const shippingAddressObj = await req.Models.Address.findOne({
      where: {id: order.get('shipping_address_id')},
    });

    if (!order || !data || order.get('status') != OrderStatus.pending) {
      throw new RequestError(RequestErrorMessage.paymentError);
    }

    if (
      order &&
      data &&
      data.status === 'APPROVED' &&
      data.intent === 'CAPTURE' &&
      query.token === order.get('paypal_payment_id') &&
      Number(data.purchase_units?.[0].amount?.value) - order.get('total') < 0.1
    ) {
      const captured = await client.captureOrder({id: query.token});
      Logger.info('paypal-captured-payment', data);

      if (captured.error) {
        throw new RequestError(RequestErrorMessage.paymentError);
      }

      order.status = OrderStatus.payed;
      await order.save();

      CartControllers.postPlaceOrder({
        query: order.get(),
        req,
        cid: req.cookies[clientCookieName],
        addressObj,
        shippingAddressObj,
        order,
        paymentType: 'paypal',
      });

      return {ok: true, order_id: order.get('id')};
    }
    throw new RequestError(RequestErrorMessage.paymentError);
  },
};

export default Controllers;

import Seq from 'sequelize';
import {RequestError, RequestErrorMessage} from '@Common/constants/errors';
import {arrayToMap, arrayToMultipleMap} from '@Common/utils/generic-utils';
import flatten from 'arr-flatten';
import {waitMS} from '@Common/utils/generic-utils';
import {UnlockedCarrierId, FallBackUnlocked} from '@Common/constants/app';

const Controllers = {
  async getCompetitorPrices(query, req) {
    return {
      competitorPrices: await req.Models.CompetitorPrice.findAllForCategory(
        query.category_id,
        query.attr_id,
      ),
      cheapestRelevantVariant: await req.Models.Product.findVariantForSelections({
        id: query.category_id,
        selections: {
          carrier: UnlockedCarrierId,
          capacity: query.attr_id,
        },
      }),
    };
  },

  async getVersionWithAttributes(query, req) {
    return {
      defaultVersion: await req.Models.Product.getVersionWithAttributes(query.variant_id),
    };
  },
  async getMinimumDetails(query, req) {
    const [category_id, variant_id] = String(query.id).split('-');
    const base = await req.Models.Category.findLeaf(query.id);

    if (!base) {
      throw new RequestError(RequestErrorMessage.productNotFound);
    }

    let attr_id = UnlockedCarrierId;

    let attr = null;

    // parsing attr from url
    let tempSelections = null;
    if (query.attr) {
      attr = await req.Models.Attribute.findAll({
        where: {slug: query.attr.split(',')},
        raw: true,
        attributes: ['id', 'type', 'name'],
      });

      if (attr.length && !variant_id) {
        tempSelections = attr.reduce((agg, a) => {
          agg[a.type] = a.id;
          return agg;
        }, {});
      }
    }

    let productPromise = !variant_id
      ? req.Models.Product.findVariantForSelectionsWithTags({
          id: category_id,
          selections: tempSelections || {carrier: UnlockedCarrierId},
        })
      : req.Models.Product.getVersionWithAttributes(variant_id);

    let [defaultVersion, allAttributes, allCategories] = await Promise.all([
      productPromise,
      req.Models.AttributeProduct.findAllWithNameForProductByType(category_id),
      req.Models.Category.findAllForLeaf(category_id),
    ]);

    // if no variant is specified, but no version has been found
    // try to get a non-unlocked phone
    if (!variant_id && !defaultVersion) {
      defaultVersion = req.Models.Product.findVariantForSelectionsWithTags({
        id: category_id,
        selections: {},
      });
    }

    const shippingMethodsBySeller = await req.Models.ShippingMethod.getAll(
      defaultVersion?.variant?.owner,
    );

    const seller = await req.Models.Seller.getPublicProfile(defaultVersion?.variant?.owner);

    const allImages = await req.Models.CategoryImage.findAllForCategories(
      allCategories.map(it => it.id),
    );
    const imageMap = await req.Models.Image.getMap(allImages.map(it => it.img_id));

    const specs = await req.Models.Spec.getSpecs(category_id);

    // if we need to apply selections regardless having a pre-selected variant
    if (variant_id && attr?.length && defaultVersion?.tags?.length) {
      console.log(attr, defaultVersion.tags);
      const tagsMap = arrayToMap(defaultVersion.tags, 'id');

      tempSelections = attr.reduce((agg, a) => {
        if (tagsMap.hasOwnProperty(a.id)) {
          agg[a.type] = a.id;
        } else if (a.type === 'carrier' && FallBackUnlocked.hasOwnProperty(a.name)) {
          for (const fallback_id of FallBackUnlocked[a.name]) {
            if (tagsMap.hasOwnProperty(fallback_id)) {
              agg[a.type] = a.id;
              return agg;
            }
          }
        }
        return agg;
      }, {});
    }

    return {
      product: base,
      imageMap,
      seller,
      allImages,
      allCategories,
      defaultVersion,
      allAttributes,
      shippingMethodsBySeller,
      peopleWatching: await req.Models.PeopleWatching.getForCategory(category_id),
      specs,
      applySelections: tempSelections || {},
    };
  },

  /* @query {id: category_id, selections: object}
   */
  async getMinimumPricePerCondition(query, req) {
    return req.Models.Product.getMinimumPricePerCondition(query.id, query.selections);
  },

  /* @query {id: category_id, selections: object}
   */
  async getMinimumPricePerCarrier(query, req) {
    const results = await req.Models.Product.getMinimumPricePerCarrier(query.id, query.selections);
    return {prices: results};
  },

  /* @query {
       selections: {carrier: id, color: id, appearance: id, capacity: id}
       id: ctageory_id
       lastClicked: last attribute selected
     }
  }
  */
  async loadWithAttributes(query, req) {
    let variant = await req.Models.Product.findVariantForSelections(query);

    // If out of stock fall back to a different condition
    if (!variant[0]) {
      const {selections, id} = query;
      delete selections.appearance;
      variant = await req.Models.Product.findVariantForSelections({selections, id});
    }

    // If out of stock fall back to just the last clicked attribute but keeping capacity
    if (!variant[0]) {
      variant = await req.Models.Product.findVariantForSelections({
        id: query.id,
        selections: {
          carrier: query.selections.carrier,
          capacity: query.selections.capacity,
          [query.lastClicked]: query.selections[query.lastClicked],
        },
      });
    }

    if (!variant[0]) {
      return {variant: null, tags: []};
    }

    const [seller, tags, shippingMethodsBySeller] = await Promise.all([
      req.Models.Seller.getPublicProfile(variant[0].owner),
      req.Models.AttributeProduct.findAllWithNameForVariant(variant[0].id),
      req.Models.ShippingMethod.getAll(variant[0].owner),
    ]);

    return {
      variant: variant[0],
      tags,
      seller,
      shippingMethodsBySeller,
    };
  },

  /* @query {
    id: product id
  }
  */
  async relatedProducts(query, req) {
    const categories = await req.Models.Category.findRelatedLeaves(query.id);

    const cats = categories.map(m => m.id);

    const products = await req.Models.Product.findCheapestAvailableVersionBatchWithAllColors(cats);

    return {products};
  },

  /*
    Find accessory products for a category
    query: {
      id: category_id,
      seller_id: seller of original product to prioritize
    }
  */
  async getAccessoryProducts(query, req) {
    const categories = await req.Models.Category.getDeepParentCategories(query.id);
    const assocCategories = await req.Models.AccessoryCategory.findAll({
      where: {accessory_to: categories.map(c => c.id)},
      attributes: [['category_id', 'id']],
      raw: true,
    });
    const associated = await req.Models.Category.findAll({
      where: {id: assocCategories.map(c => c.id)},
      order: ['accessory_order'],
      raw: true,
      attributes: ['id', 'name', 'accessory_attribute'],
    });

    const withAttr = [];
    const withoutAttr = [];

    associated.forEach(assoc => {
      if (assoc.accessory_attribute) {
        withAttr.push(assoc);
      } else {
        withoutAttr.push(assoc);
      }
    });

    const promises = withAttr.map(a => {
      return req.Models.Product.getAccessoryProductsWithAttributeType({
        ids: [a.id],
        attributeType: a.accessory_attribute,
        seller_id: query.seller_id,
      });
    });

    if (withoutAttr.length) {
      promises.push(
        req.Models.Product.getAccessoryProductsWithNoAttributeType({
          ids: withoutAttr.map(a => a.id),
          seller_id: query.seller_id,
        }),
      );
    }

    const list = await Promise.all(promises);

    const map = arrayToMultipleMap(flatten(list), 'category_id');

    return {
      accessoryProducts: associated.map(c => ({...c, variants: map[c.id]})),
    };
  },

  async getCategory(query, req) {
    const product = await req.Models.Product.findOne({where: {id: query.variant_id}});
    return {category_id: product.get('category_id')};
  },

  async getCategoryProducts(query, req) {
    const {limit = 20, offset = 0, id} = query;
    if (!id) {
      throw new RequestError(RequestErrorMessage.missingTarget);
    }

    const {rows: products, count} = await req.Models.Product.findAndCountAll({
      where: {
        active: 1,
        visible: 1,
        category_id: id,
        stock: {[Seq.Op.gt]: 0},
        price: {[Seq.Op.gt]: 0},
      },
      attributes: ['id'],
      raw: true,
      limit: Number(limit),
      offset: Number(offset),
    });
    const product_ids = products.map(p => p.id);
    const variants = await req.Models.Product.getVariantsWithDisplayInformation(product_ids);
    return {variants, pagination: {count, offset: Number(offset), limit: Number(limit)}};
  },
};

export default Controllers;

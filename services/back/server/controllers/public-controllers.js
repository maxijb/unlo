import Logger from '@Common/logger';
import {arrayToMultipleMap} from '@Common/utils/generic-utils';
import {getShippingMethodDescription} from '@Common/utils/product-utils';
import {UserFilesCdnURL} from '@Common/utils/statif-assets-utils';
import Seq from 'sequelize';
import {
  SellerConnectionStatus,
  CartStatusForApi,
  DefaultShippingMethod,
  CartStatus,
  TrackingLinks,
} from '@Common/constants/app';
import {SellerHttpHeader, ValidStatusTransitions} from '@Common/constants/seller-constants';
import {RequestError, RequestErrorMessage, ApiError} from '@Common/constants/errors';
import {getClient} from '../services/brightpearl';

import isEmpty from 'is-empty';
import {idSetToWhere} from '@Common/utils/api-utils';
import {arrayToMap} from '@Common/utils/generic-utils';
import {parseIncredibleId} from '@Common/../front/shared/utils/attributes-utils';
import {getActualOrderTotals} from '@Common/utils/order-utils';
import {
  getNumericStatusFromSellerEnum,
  formatOrder,
  formatOrderForRetailer,
  parseStatusValues,
  getRetailerPrice,
} from '@Common/utils/seller-api-utils';
import {
  consolidateAttributesAsName,
  getProductNameWithAttributesForGoogle,
  getProductNameWithAttributesForOrder,
} from '../../../front/shared/utils/attributes-utils';
import CartControllers from './cart-controllers';

export const DefaultLimit = 10;

const Controllers = {
  async log(query, req) {
    console.log('==================================================');
    console.log(query);
    console.log('==================================================');
    return {};
  },

  createOrder: errorWrapper(async (query, req) => {
    if (!query.retailer_order_id) {
      throw new ApiError('REQUIRED_FIELD_MISSING', '`retailer_order_id` cannot be empty');
    }
    if (!query.tax) {
      throw new ApiError('REQUIRED_FIELD_MISSING', '`tax` cannot be empty');
    }
    if (!query.tax_rate) {
      throw new ApiError('REQUIRED_FIELD_MISSING', '`tax_rate` cannot be empty');
    }

    const carts_ids = [];
    const shippingMethods = new Set();
    for (const product of query.products) {
      if (!product.shipping_method) {
        throw new ApiError(
          'REQUIRED_FIELD_MISSING',
          '`shipping_method` should be specified for every product',
        );
      }
      const cart = await req.Models.Cart.addForUser(
        {
          quantity: product.quantity,
          variant_id: product.id,
          isFromApi: true,
        },
        req,
      );
      carts_ids.push(cart.get('id'));
      shippingMethods.add(product.shipping_method);
    }

    for (const shipping of shippingMethods) {
      const shippingMethod = await req.Models.ShippingMethod.findOne({
        where: {id: shipping},
        raw: true,
      });
      const row = await req.Models.Cart.create({
        description_seller:
          shippingMethod.name || getShippingMethodDescription(shippingMethod, req.t),
        description: getShippingMethodDescription(shippingMethod, req.t),
        price: shippingMethod.price,
        product_id: shipping,
        type: 'shipping',
        special_item: JSON.stringify({
          speed_days: shippingMethod.speed_days,
          carrier: shippingMethod.carrier,
        }),
        status: 1,
      });
      carts_ids.push(row.get('id'));
    }

    const preOrder = await CartControllers.prePlaceOrder(
      {
        ...query,
        carts_ids,
        isApi: true,
      },
      req,
    );

    if (!preOrder.ok) {
      return preOrder;
    }

    return CartControllers.postPlaceOrder({
      query: {
        ...query,
        carts_ids,
        isApi: true,
        retailer_id: req.seller_id,
      },
      req,
      ok: true,
      tax: query.tax,
      total: preOrder.total,
      subtotal: preOrder.subtotal,
      taxRate: preOrder.taxRate,
      cid: null,
      userId: null,
      addressObj: preOrder.addressObj,
      shippingAddressObj: preOrder.shippingAddressObj,
      subtotalProducts: preOrder.shippingAddressObj,

      payment_type: 'retailer',
    });
  }),

  async getProductsRetailer(query, req) {
    const {limit, offset} = getLimitOffset(query);
    const where = {active: 1, visible: 1};
    if (query.id) {
      where.id = {id: idSetToWhere(query.id)};
    }
    if (query.category) {
      where.category_id = query.category;
    }

    const {rows, count} = await req.Models.Product.findAndCountAll({
      where,
      attributes: ['id', 'owner'],
      limit: limit,
      offset: offset,
      raw: true,
    });

    const ids = rows.map(r => r.id);
    const seller_ids = Array.from(new Set(rows.map(r => r.owner)));

    const products = await req.Models.Product.getVariantsWithDisplayInformation(ids);
    const shippingMethods = await req.Models.ShippingMethod.findAll({
      where: {seller_id: seller_ids},
      attributes: ['id', 'seller_id', 'speed_days', 'carrier', 'name', 'price'],
      raw: true,
    });

    const shippingMap = arrayToMultipleMap(shippingMethods, 'seller_id');

    const results = ids.map(key => {
      const product = products[key];
      const attrs = consolidateAttributesAsName(product.attributes, req.t);

      return {
        id: key,
        category: product.name,
        category_id: product.category_id,
        price: getRetailerPrice(product.price),
        stock: product.stock,
        ...attrs,
        description: getProductNameWithAttributesForOrder(product.name, attrs, req.t),
        attributes: Object.values(attrs),
        image: UserFilesCdnURL(`400x400/${product.image}`),
        shipping_methods: shippingMap[product.owner],
      };
    });

    return {ok: true, results, pagination: {totalCount: count, limit, offset}};
  },

  getProducts: errorWrapper(async (query, req) => {
    const seller = await req.Models.Seller.findOne({where: {id: req.seller_id}, raw: true});

    if (seller.is_retailer) {
      return Controllers.getProductsRetailer(query, req);
    }
    const {limit, offset} = getLimitOffset(query);

    const where = {owner: req.seller_id};
    if (query.sku || query.incredible_id || query.id) {
      where[Seq.Op.or] = [
        {source_id: idSetToWhere(query.sku)},
        {incredible_id: idSetToWhere(query.incredible_id)},
        {id: idSetToWhere(query.id)},
      ];
    }
    const {rows, count} = await req.Models.Product.findAndCountAll({
      where,
      attributes: ['id', 'price', 'stock', 'active', 'source_id', 'incredible_id'],
      limit: limit,
      offset: offset,
      raw: true,
    });

    const minPrices = await req.Models.Product.findMinPricePerIncredibleId(
      rows.map(r => r.incredible_id),
    );

    rows.forEach(r => {
      const buybox_price = minPrices[r.incredible_id]?.min_price || null;
      r.buybox_price = buybox_price;
      r.win_buybox = (!!buybox_price && buybox_price === r.price) || false;
    });

    return {ok: true, results: rows, pagination: {totalCount: count, limit, offset}};
  }),

  getOrdersRetailers: errorWrapper(async (query, req) => {
    const {limit, offset} = getLimitOffset(query);

    const ids = idSetToWhere(query.id);
    const retailer_order_ids = idSetToWhere(query.retailer_id);
    const statuses = parseStatusValues(query.status);

    const {orders, totalCount} = await req.Models.Order.list({
      isSeller: true,
      offset,
      limit,
      isApi: true,
      retailer_id: req.seller_id,
      retailer_order_ids,
      ids: ids?.length ? ids : null,
      forceIncludeCount: true,
      visibleStatus: statuses,
    });

    let i = 0;
    for (const o of orders) {
      orders[i++] = await formatOrderForRetailer(o, req);
    }

    return {ok: true, results: orders, pagination: {totalCount, limit, offset}};
  }),

  getOrders: errorWrapper(async (query, req) => {
    const {limit, offset} = getLimitOffset(query);

    const ids = idSetToWhere(query.id);
    const statuses = parseStatusValues(query.status);

    const {orders, totalCount} = await req.Models.Order.list({
      isSeller: true,
      offset,
      limit,
      isApi: true,
      seller_id: req.seller_id,
      ids: ids?.length ? ids : null,
      forceIncludeCount: true,
      visibleStatus: statuses,
    });

    let i = 0;
    for (const o of orders) {
      orders[i++] = await formatOrder(o, req);
    }

    return {ok: true, results: orders, pagination: {totalCount, limit, offset}};
  }),

  updateProducts: errorWrapper(async (query, req) => {
    const errors = [];

    let created = 0;
    let updated = 0;

    if (!query.data?.length) {
      throw new ApiError('MISSING_DATA', '`data` cannot be empty');
    }
    if (query.data.length > 500) {
      throw new ApiError(
        'DATA_OVERFLOW',
        '`data` cannot include more than 500 rows. Please break down your requests to smaller batches.',
      );
    }

    const [categories, attributes] = await Promise.all([
      req.Models.Category.findAll({attributes: ['slug', 'id'], raw: true}),
      req.Models.Attribute.findAll({attributes: ['slug', 'id'], raw: true}),
    ]);

    const catsBySlug = arrayToMap(categories, 'slug');
    const attrsBySlug = arrayToMap(attributes, 'slug');

    let i = -1;
    for (const row of query.data || []) {
      i++;
      row.incredible_id = (row.incredible_id || '').toLowerCase();

      let canBeCreation = false;
      const where = {};
      if (row.incredible_id) {
        canBeCreation = true;
        where.incredible_id = row.incredible_id;
      }
      if (row.sku) {
        where.source_id = row.sku;
      }
      if (row.id) {
        where.id = row.id;
      }

      if (isEmpty(where)) {
        errors.push({
          code: 'MISSING_TARGET',
          message: `Line ${i} doesn't include nor "id", "sku", or "incredible_id"`,
        });

        break;
      }

      where.owner = req.seller_id;

      const existingProduct = await req.Models.Product.findOne({
        where,
      });

      if (!existingProduct) {
        if (!canBeCreation) {
          errors.push({
            code: 'MISSING_INCREDIBLE_ID',
            message: `${findTarget(
              row,
              i,
            )} doesn't exist. Failed to create a new item due to a missing incredible_id"`,
          });

          break;
        }

        const duplicatedMaybe = await req.Models.Product.findOne({
          where: {
            incredible_id: row.incredible_id,
            owner: req.seller_id,
          },
        });

        if (duplicatedMaybe) {
          errors.push({
            code: 'DUPLICATED PRODUCT',
            message: `Incredible_id ${row.incredible_id} already exists for this seller`,
          });
          break;
        }

        const {category, attributes} = parseIncredibleId(row.incredible_id);

        if (!catsBySlug.hasOwnProperty(category)) {
          errors.push({
            code: 'UNKNOWN_CATEGORY',
            message: `${findTarget(row, i)}: Unkonwn category slug ${category}`,
          });
          break;
        }

        let shouldBreak = false;
        const attr_ids = attributes.map(at => {
          if (shouldBreak) {
            return null;
          }
          if (!attrsBySlug.hasOwnProperty(at.toLowerCase())) {
            errors.push({
              code: 'UNKNOWN ATTRIBUTE',
              message: `${findTarget(row, i)}: Unkonwn attribute slug "${at}"`,
            });
            shouldBreak = true;
            return;
          }
          return attrsBySlug[at.toLowerCase()].id;
        });

        if (shouldBreak) {
          break;
        }

        const newProduct = {
          category_id: catsBySlug[category].id,
          owner: req.seller_id,
          price: row.price || 0,
          stock: row.stock || 0,
          active: typeof row.active === 'undefined' || row.active === null ? 1 : row.active,
          incredible_id: row.incredible_id,
          source_id: row.sku,
          source_name: 'seller_api',
        };

        try {
          const product = await req.Models.Product.create(newProduct);
          await req.Models.AttributeProduct.destroy({
            where: {
              product_id: product.id,
            },
          });
          await req.Models.AttributeProduct.bulkCreate(
            attr_ids.map(attr_id => ({
              attr_id,
              product_id: product.id,
            })),
            {
              ignoreDuplicates: true,
            },
          );

          created++;
        } catch (e) {
          errors.push({code: 'INPUT_ERROR', message: `${findTarget(row, i)}: ${e.message}`});
        }
      } else {
        if (typeof row.active !== 'undefined' && row.active !== null) {
          existingProduct.active = row.active;
        }
        if (typeof row.price !== 'undefined') {
          existingProduct.price = row.price || 0;
        }
        if (typeof row.stock !== 'undefined') {
          existingProduct.stock = row.stock || 0;
        }

        try {
          await existingProduct.save();
          updated++;
        } catch (e) {
          errors.push({code: 'INPUT_ERROR', message: `${findTarget(row, i)}: ${e.message}`});
        }
      }
    }

    return {ok: !errors.length, created, updated, errors};
  }),

  updateOrderStatus: errorWrapper(async (query, req) => {
    if (!query.order_id || !Number.isFinite(Number(query.order_id))) {
      throw new ApiError('MISSING_ORDER_ID', '`order_id` is a required field (number)');
    }
    if (!query.status) {
      throw new ApiError('MISSING_STATUS', '`status` is a required field');
    }

    const {orders} = await req.Models.Order.list({
      ids: query.order_id,
      seller_id: req.seller_id,
      isSeller: true,
      isApi: true,
    });
    if (!orders?.[0]) {
      throw new ApiError('ORDER_NOT_FOUND', 'The order was not found');
    }

    let cart_ids = [];
    if (query.status === CartStatusForApi[CartStatus.inTransit]) {
      if (!query.tracking || !query.carrier) {
        throw new ApiError(
          'MISSING_TRACKING',
          '`tracking` and `carrier` are required fields to move status to SHIPPED',
        );
      }

      if (!TrackingLinks.hasOwnProperty(query.carrier)) {
        throw new ApiError(
          'MALFORMED_CARRIER',
          `'carrier' can only be one of the following values: ${Object.keys(TrackingLinks).join(
            ', ',
          )}`,
        );
      }

      const {cart_ids: c_ids} = await req.Models.Order.addTracking({
        order_id: query.order_id,
        seller_id: req.seller_id,
        req,
        query: {
          tracking: query.tracking,
          tracking_carrier: query.tracking_carrier,
        },
      });
      cart_ids = c_ids;
    } else {
      const numStatus = getNumericStatusFromSellerEnum(query.status);
      cart_ids = await req.Models.Order.updateCartStatus(orders[0].cart, numStatus, req);
    }

    const {orders: newOrders} = await req.Models.Order.list({
      ids: query.order_id,
      seller_id: req.seller_id,
      isSeller: true,
      isApi: true,
    });
    const order = await formatOrder(newOrders[0], req);
    const orderStatus = order.cart[0].status;
    const orderNumStatus = getNumericStatusFromSellerEnum(orderStatus);
    const numStatus = getNumericStatusFromSellerEnum(query.status);

    if (
      !cart_ids.length &&
      (!ValidStatusTransitions.hasOwnProperty(orderNumStatus) ||
        !ValidStatusTransitions[orderNumStatus].includes(numStatus))
    ) {
      throw new ApiError(
        'INVALID_TRANSITION',
        `Order with status ${orderStatus} cannot be moved to ${query.status}`,
      );
    }

    return {ok: true, order};
  }),
};

export default Controllers;

function errorWrapper(fn) {
  return async (query, req) => {
    const token = req.headers[SellerHttpHeader];

    req.seller_id = await req.Models.Seller.getSellerIdFromApiToken(token);

    if (!req.seller_id) {
      return {
        statusCode: 401,
        response: {
          ok: false,
          errors: [
            {code: 'UNAUTHORIZED', message: 'Unauthorized access. Missing or incorrect API token'},
          ],
        },
      };
    }
    try {
      const result = await fn(query, req);
      return result;
    } catch (e) {
      if (!req.test) {
        Logger.error(e);
      }
      return {ok: false, errors: [{code: e.name, message: e.message}]};
    }
  };
}

function findTarget(row, line) {
  const parts = [];
  if (row.sku) {
    parts.push(`Sku ${row.sku}`);
  }
  if (row.incredible_id) {
    parts.push(`Incredible_id ${row.incredible_id}`);
  }
  if (row.id) {
    parts.push(`Id ${row.id}`);
  }

  if (parts.length) {
    return parts.join(' - ');
  }

  return `Line ${line}`;
}

function getLimitOffset(query) {
  let {limit = DefaultLimit, offset = 0} = query;
  limit = Number(limit);
  offset = Number(offset);

  if (isNaN(limit)) {
    throw new ApiError('VALIDATION_ERROR', 'Limit is not a number');
  }
  if (isNaN(offset)) {
    throw new ApiError('VALIDATION_ERROR', 'Limit is not a number');
  }

  limit = Math.min(limit, 500);
  return {limit, offset};
}

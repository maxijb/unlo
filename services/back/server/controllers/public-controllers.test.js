import Controllers, {DefaultLimit} from './public-controllers';
import Seq from 'sequelize';
import {SellerHttpHeader} from '@Common/constants/seller-constants';
import {CartStatus, TrackingLinks} from '@Common/constants/app';

const req = {
  test: true,
  headers: {
    [SellerHttpHeader]: 'test',
  },
  Models: {
    AttributeProduct: {
      bulkCreate: jest.fn(),
      destroy: jest.fn(),
    },
    Cart: {
      update: jest.fn(),
    },
    Category: {
      findAll: () => [{slug: 'maxi', id: 3}],
    },
    Attribute: {
      findAll: () => [
        {slug: 'blue', id: 5},
        {slug: 'red', id: 6},
      ],
    },

    Product: {
      findMinPricePerIncredibleId: () => ({
        'maxi-blue': {min_price: 2.5},
      }),
      create: jest.fn(() => ({id: 13})),
      findOne: jest.fn(args => {
        if (
          args?.where?.id === 100 ||
          (args?.where?.incredible_id === 'maxi-red' && !args.where.source_id)
        ) {
          return {
            save: jest.fn(),
          };
        }
        return null;
      }),
      findAndCountAll: jest.fn(() => ({
        count: 2,
        rows: [{id: 1, incredible_id: 'maxi-blue'}, {id: 2}],
      })),
      findAll: jest.fn(() => [
        {id: 1, category_id: 5, source_id: 'aaa', incredible_id: 'maxi-test'},
      ]),
    },
    Seller: {
      getSellerIdFromApiToken: () => 1,
    },
    Order: {
      getShippingMethodForSellerFromCart: () => {
        return {
          shippingMethod: {id: -1, price: 0, name: 'test', speed_days: 5},
        };
      },
      addTracking: jest.fn(() => ({
        cart_ids: [],
      })),
      updateCartStatus: jest.fn(() => [{}]),
      list: jest.fn(args => {
        if (args.ids === 1000) {
          return [];
        }
        return {
          orders: [
            {
              tax_rate: 0.1,
              total: 2.2,
              subtotal: 2,
              tax: 0.2,
              cart: [
                {
                  type: 'product',
                  price: 1,
                  variant_id: 1,
                  quantity: 2,
                  status: 4,
                  source_id: 'aaa',
                },
              ],
            },
          ],
          totalCount: 1,
        };
      }),
    },
  },
};

describe('Get Orders', () => {
  it('With arguments', async () => {
    req.Models.Product.findAndCountAll.mockClear();
    const response = await Controllers.getOrders(
      {
        status: 'NEW_ORDER,SHIPPED',
        id: '1,2',
      },
      req,
    );
    expect(response).toEqual({
      ok: true,
      results: [
        {
          cart: [
            {
              incredible_id: undefined,
              price: 2,
              quantity: 2,
              sku: undefined,
              source_id: 'aaa',
              status: 'SHIPPED',
              tax: 0.4,
              unit_price: 1,
              variant_id: 1,
            },
          ],
          shipping: {
            id: -1,
            name: 'test',
            price: 0,
            speed_days: 5,
          },
          subtotal: 2,
          tax: 0.2,
          tax_rate: 0.1,
          total: 2.2,
        },
      ],
      pagination: {
        totalCount: 1,
        limit: DefaultLimit,
        offset: 0,
      },
    });
    expect(req.Models.Order.list).toHaveBeenLastCalledWith({
      forceIncludeCount: true,
      limit: DefaultLimit,
      offset: 0,
      isApi: true,
      isSeller: true,
      seller_id: 1,
      ids: ['1', '2'],
      visibleStatus: [CartStatus.inOrder, CartStatus.inTransit],
    });
  });
});

describe('Get products', () => {
  it('empty query', async () => {
    const response = await Controllers.getProducts({}, req);

    expect(response).toEqual({
      ok: true,
      results: [
        {id: 1, incredible_id: 'maxi-blue', buybox_price: 2.5},
        {id: 2, buybox_price: null},
      ],
      pagination: {
        totalCount: 2,
        limit: DefaultLimit,
        offset: 0,
      },
    });
  });

  it('With arguments', async () => {
    req.Models.Product.findAndCountAll.mockClear();
    const response = await Controllers.getProducts({sku: '1,2,3'}, req);
    expect(response).toEqual({
      ok: true,
      results: [
        {id: 1, incredible_id: 'maxi-blue', buybox_price: 2.5},
        {id: 2, buybox_price: null},
      ],
      pagination: {
        totalCount: 2,
        limit: DefaultLimit,
        offset: 0,
      },
    });
    expect(req.Models.Product.findAndCountAll).toHaveBeenLastCalledWith({
      where: {
        owner: 1,
        [Seq.Op.or]: [{source_id: ['1', '2', '3']}, {incredible_id: []}, {id: []}],
      },
      attributes: ['id', 'price', 'stock', 'active', 'source_id', 'incredible_id'],
      limit: DefaultLimit,
      offset: 0,
      raw: true,
    });
  });

  it('With multiple arguments', async () => {
    req.Models.Product.findAndCountAll.mockClear();
    const response = await Controllers.getProducts({sku: '1,2,3', incredible_id: 'maxi,uio'}, req);
    expect(response).toEqual({
      ok: true,
      results: [
        {id: 1, incredible_id: 'maxi-blue', buybox_price: 2.5},
        {id: 2, buybox_price: null},
      ],
      pagination: {
        totalCount: 2,
        limit: DefaultLimit,
        offset: 0,
      },
    });
    expect(req.Models.Product.findAndCountAll).toHaveBeenLastCalledWith({
      where: {
        owner: 1,
        [Seq.Op.or]: [{source_id: ['1', '2', '3']}, {incredible_id: ['maxi', 'uio']}, {id: []}],
      },
      attributes: ['id', 'price', 'stock', 'active', 'source_id', 'incredible_id'],
      limit: DefaultLimit,
      offset: 0,
      raw: true,
    });
  });

  it('limit and offset', async () => {
    const response = await Controllers.getProducts({limit: 100, offset: 20}, req);
    expect(response).toEqual({
      ok: true,
      results: [
        {id: 1, incredible_id: 'maxi-blue', buybox_price: 2.5},
        {id: 2, buybox_price: null},
      ],
      pagination: {
        totalCount: 2,
        limit: 100,
        offset: 20,
      },
    });
  });
});

describe('Update products', () => {
  it('missing data', async () => {
    const response = await Controllers.updateProducts({}, req);
    expect(response).toEqual({
      ok: false,
      errors: [{code: 'MISSING_DATA', message: '`data` cannot be empty'}],
    });
  });

  it('too much data', async () => {
    const data = [];
    for (let i = 0; i < 600; i++) {
      data.push({a: 1});
    }
    const response = await Controllers.updateProducts({data}, req);
    expect(response).toEqual({
      ok: false,
      errors: [
        {
          code: 'DATA_OVERFLOW',
          message:
            '`data` cannot include more than 500 rows. Please break down your requests to smaller batches.',
        },
      ],
    });
  });

  it('with creation data', async () => {
    const response = await Controllers.updateProducts(
      {
        data: [{incredible_id: 'maxi-blue', price: 3, stock: 5, sku: 'test'}],
      },
      req,
    );
    expect(response).toEqual({
      ok: true,
      created: 1,
      updated: 0,
      errors: [],
    });
    expect(req.Models.Product.create).toHaveBeenLastCalledWith({
      incredible_id: 'maxi-blue',
      active: 1,
      price: 3,
      stock: 5,
      source_id: 'test',
      owner: 1,
      category_id: 3,
      source_name: 'seller_api',
    });
  });

  it('missing target', async () => {
    const response = await Controllers.updateProducts(
      {
        data: [{price: 3, stock: 5, source_id: 'test'}],
      },
      req,
    );
    expect(response).toEqual({
      ok: false,
      created: 0,
      updated: 0,
      errors: [
        {
          code: 'MISSING_TARGET',
          message: 'Line 0 doesn\'t include nor "id", "sku", or "incredible_id"',
        },
      ],
    });
  });

  it('failed to create', async () => {
    const response = await Controllers.updateProducts(
      {
        data: [{price: 3, stock: 5, source_id: 'test', id: 5, sku: 'test'}],
      },
      req,
    );
    expect(response).toEqual({
      ok: false,
      created: 0,
      updated: 0,
      errors: [
        {
          code: 'MISSING_INCREDIBLE_ID',
          message:
            'Sku test - Id 5 doesn\'t exist. Failed to create a new item due to a missing incredible_id"',
        },
      ],
    });
  });

  it('update item', async () => {
    const response = await Controllers.updateProducts(
      {
        data: [{price: 3, stock: 5, source_id: 'test', id: 100, sku: 'test'}],
      },
      req,
    );
    expect(response).toEqual({
      ok: true,
      created: 0,
      updated: 1,
      errors: [],
    });
  });

  it('create item', async () => {
    const response = await Controllers.updateProducts(
      {
        data: [{price: 3, stock: 5, incredible_id: 'maxi-blue', sku: 'test'}],
      },
      req,
    );
    expect(response).toEqual({
      ok: true,
      created: 1,
      updated: 0,
      errors: [],
    });
    expect(req.Models.Product.create).toHaveBeenLastCalledWith({
      incredible_id: 'maxi-blue',
      source_id: 'test',
      owner: 1,
      price: 3,
      stock: 5,
      active: 1,
      category_id: 3,
      source_name: 'seller_api',
    });
  });

  it('duplicated item', async () => {
    const response = await Controllers.updateProducts(
      {
        data: [{price: 3, stock: 5, incredible_id: 'maxi-red', sku: 'red'}],
      },
      req,
    );
    expect(response).toEqual({
      ok: false,
      created: 0,
      updated: 0,
      errors: [
        {
          code: 'DUPLICATED PRODUCT',
          message: 'Incredible_id maxi-red already exists for this seller',
        },
      ],
    });
  });

  it('create inactive item', async () => {
    const response = await Controllers.updateProducts(
      {
        data: [{price: 3, stock: 5, incredible_id: 'maxi-blue', sku: 'test', active: 0}],
      },
      req,
    );
    expect(response).toEqual({
      ok: true,
      created: 1,
      updated: 0,
      errors: [],
    });
    expect(req.Models.Product.create).toHaveBeenLastCalledWith({
      incredible_id: 'maxi-blue',
      source_id: 'test',
      owner: 1,
      price: 3,
      stock: 5,
      active: 0,
      category_id: 3,
      source_name: 'seller_api',
    });
  });

  it('unknown attibute', async () => {
    const response = await Controllers.updateProducts(
      {
        data: [{price: 3, stock: 5, incredible_id: 'maxi-throw', sku: 'red', active: 0}],
      },
      req,
    );
    expect(response).toEqual({
      ok: false,
      created: 0,
      updated: 0,
      errors: [
        {
          code: 'UNKNOWN ATTRIBUTE',
          message: 'Sku red - Incredible_id maxi-throw: Unkonwn attribute slug "throw"',
        },
      ],
    });
  });

  it('unknown attibute', async () => {
    const response = await Controllers.updateProducts(
      {
        data: [{price: 3, stock: 5, incredible_id: 'who-blue', sku: 'red', active: 0}],
      },
      req,
    );
    expect(response).toEqual({
      ok: false,
      created: 0,
      updated: 0,
      errors: [
        {
          code: 'UNKNOWN_CATEGORY',
          message: 'Sku red - Incredible_id who-blue: Unkonwn category slug who',
        },
      ],
    });
  });
});

describe('Update Orders Status', () => {
  it('With no order id', async () => {
    const response = await Controllers.updateOrderStatus({}, req);
    expect(response).toEqual({
      ok: false,
      errors: [
        {
          code: 'MISSING_ORDER_ID',
          message: '`order_id` is a required field (number)',
        },
      ],
    });
  });

  it('With no status', async () => {
    const response = await Controllers.updateOrderStatus({order_id: 3}, req);
    expect(response).toEqual({
      ok: false,
      errors: [
        {
          code: 'MISSING_STATUS',
          message: '`status` is a required field',
        },
      ],
    });
  });

  it('With missing tracking', async () => {
    const response = await Controllers.updateOrderStatus({order_id: 3, status: 'SHIPPED'}, req);
    expect(response).toEqual({
      ok: false,
      errors: [
        {
          code: 'MISSING_TRACKING',
          message: '`tracking` and `carrier` are required fields to move status to SHIPPED',
        },
      ],
    });
  });

  it('With malformed carrier', async () => {
    const response = await Controllers.updateOrderStatus(
      {order_id: 3, status: 'SHIPPED', carrier: 'TY', tracking: '123456'},
      req,
    );
    expect(response).toEqual({
      ok: false,
      errors: [
        {
          code: 'MALFORMED_CARRIER',
          message: `'carrier' can only be one of the following values: ${Object.keys(
            TrackingLinks,
          ).join(', ')}`,
        },
      ],
    });
  });

  it('Order not found', async () => {
    const response = await Controllers.updateOrderStatus(
      {order_id: 1000, status: 'SHIPPED', carrier: 'UPS', tracking: '123456'},
      req,
    );
    expect(response).toEqual({
      ok: false,
      errors: [
        {
          code: 'ORDER_NOT_FOUND',
          message: `The order was not found`,
        },
      ],
    });
  });

  it('Invalid transition', async () => {
    const response = await Controllers.updateOrderStatus(
      {order_id: 100, status: 'SHIPPED', carrier: 'UPS', tracking: '123456'},
      req,
    );
    expect(response).toEqual({
      ok: false,
      errors: [
        {
          code: 'INVALID_TRANSITION',
          message: `Order with status SHIPPED cannot be moved to SHIPPED`,
        },
      ],
    });
  });

  it('Valid transition', async () => {
    const response = await Controllers.updateOrderStatus(
      {order_id: 100, status: 'RETURNED_TO_SENDER'},
      req,
    );

    expect(response.ok).toEqual(true);
    expect(req.Models.Order.updateCartStatus).toHaveBeenLastCalledWith(
      [{price: 1, quantity: 2, source_id: 'aaa', status: 4, type: 'product', variant_id: 1}],
      11,
      req,
    );
  });
});

import {
  getTimezoneOffset,
  formatMysqlTimezoneOffset,
  begginingOfDay,
  endOfDay,
  absUTCtoEmbeddedTZ,
  addTimezoneToStringDate,
  getAbsDateOnly,
  getUTCDateTimeAsString,
  stringDate,
} from '@Common/utils/date-utils';

import {getShippingMethodDescription} from '@Common/utils/product-utils';
import {DayMs} from '@Common/constants/dates';
import {RequestError, RequestErrorMessage} from '@Common/constants/errors';
import Seq from 'sequelize';
import {
  pricePlain,
  formatAddressFirstLine,
  formatAddressSecondLine,
} from '@Common/utils/format-utils';
import {arrayToMap, arrayToMultipleMap} from '@Common/utils/generic-utils';
import {CommissionRate, DefaultShippingMethod} from '@Common/constants/app';
const createCsvWriter = require('csv-writer').createObjectCsvStringifier;

const Controllers = {
  async interactionsChart(query, req) {
    const period = await normalizeReportPeriod(query, req);
    const items = await req.Models.Interaction.getChart(period);
    const previousPeriod = await req.Models.Interaction.countByDate({
      start: period.prevStart,
      end: period.start,
      appId: period.appId,
    });
    return normalizeResponse(items, period, previousPeriod);
  },

  async getSellerPaymentsCsv(query, req) {
    const user = await req.Models.User.findOne({where: {id: req.auth.id}});
    let seller_id = user.get('seller_id');

    if (user.get('superAdmin') && query?.seller_id) {
      seller_id = query?.seller_id;
    }

    if (!seller_id) {
      throw new RequestError(RequestErrorMessage.validationError);
    }

    const where = {seller_id};

    if (query.minDate || query.maxDate) {
      where.createdAt = {};
      if (query.minDate) {
        where.createdAt[Seq.Op.gte] = query.minDate;
      }
      if (query.maxDate) {
        where.createdAt[Seq.Op.lte] = query.maxDate;
      }
    }

    const rows = await req.Models.Billable.findAll({
      where,
      raw: true,
      order: [['createdAt', 'ASC']],
    });

    const csvWriter = createCsvWriter({
      header: [
        {id: 'id', title: 'TransactionId'},
        {id: 'date', title: 'Date'},
        {id: 'order', title: 'OrderId'},
        {id: 'description', title: 'Description'},
        {id: 'state', title: 'State'},
        {id: 'zip', title: 'Zip'},
        {id: 'gross', title: 'GrossCredit'},
        {id: 'fee', title: 'Fee'},
        {id: 'tax', title: 'Tax'},
        {id: 'credit', title: 'NetCredit'},
        {id: 'debit', title: 'Debit'},
        {id: 'balance', title: 'Balance'},
      ],
    });

    const order_ids = rows?.reduce((agg, r) => {
      if (r.order_id) {
        agg.push(r.order_id);
      }
      return agg;
    }, []);

    let [orders, carts] = await Promise.all([
      req.Models.Order.findAll({where: {id: order_ids}, raw: true}),
      req.Models.Cart.findAll({where: {order_id: order_ids, seller_id}, raw: true}),
    ]);

    let addresses = await req.Models.Address.findAll({
      where: {
        id: orders.map(o => o.address_id),
      },
      raw: true,
    });

    addresses = arrayToMap(addresses);
    orders = arrayToMap(orders);
    carts = arrayToMultipleMap(carts, 'order_id');

    let totalTax = 0;
    let totalTaxByState = {};
    let totalGrossByState = {};
    let totalFee = 0;
    let totalCredit = 0;
    let totalDebit = 0;
    let totalGross = 0;

    const data = rows.map(r => {
      const order = r.order_id ? orders[r.order_id] : null;
      const products = r.order_id ? carts[r.order_id] : null;
      const address = r.order_id ? addresses[orders[r.order_id].address_id] : null;
      const subtotal = products?.reduce((agg, p) => agg + p.price, 0);

      const tax = subtotal * (order?.tax_rate || 0);
      totalTax += Number(tax) || 0;
      if (address?.administrative_area_level_1) {
        if (!totalTaxByState.hasOwnProperty(address.administrative_area_level_1)) {
          totalTaxByState[address.administrative_area_level_1] = 0;
          totalGrossByState[address.administrative_area_level_1] = 0;
        }
        totalTaxByState[address.administrative_area_level_1] += tax;
        totalGrossByState[address.administrative_area_level_1] += r.gross;
      }
      totalFee += Number(r.commission) || 0;
      totalCredit += Number(r.credit) || 0;
      totalDebit += Number(r.debit) || 0;
      totalGross += Number(r.gross) || 0;

      return {
        id: r.id,
        date: getAbsDateOnly(r.createdAt),
        order: r.order_id,
        description:
          r.description || req.t(`dashboard:billableEvents.${r.type}`, {...r, context: 'plain'}),
        zip: address?.postal_code || '',
        state: address?.administrative_area_level_1 || '',
        gross: pricePlain(r.gross || ''),
        fee: pricePlain(r.commission),
        tax: pricePlain(tax),
        credit: pricePlain(r.credit || ''),
        debit: pricePlain(r.debit || ''),
        balance: pricePlain(r.balance || ''),
      };
    });

    const stateSummary =
      query.includeStateSummary !== 'true'
        ? ''
        : `
${Object.keys(totalTaxByState)
  .map(
    key =>
      `,,${key},${pricePlain(totalTaxByState[key])},Gross Sales,${pricePlain(
        totalGrossByState[key],
      )}`,
  )
  .join('\n')}
`;

    const summary =
      query.includeSummary !== 'true'
        ? ''
        : `
Total Gross Credit,${pricePlain(totalGross)}
Total Fees,${pricePlain(totalFee)}
Total Sales Tax,${pricePlain(totalTax)}${stateSummary}
Total Debits,${pricePlain(totalDebit)}
Total Net Credit,${pricePlain(totalCredit)}


`;

    const content = `${summary}${csvWriter.getHeaderString()}${csvWriter.stringifyRecords(data)}`;

    return {response: content, attachment: 'report.csv'};
  },

  async getSellerOrdersCsv(query, req) {
    const user = await req.Models.User.findOne({where: {id: req.auth.id}});
    let seller_id = user.get('seller_id');

    if (user.get('superAdmin') && query?.seller_id) {
      seller_id = query?.seller_id;
    }

    if (!seller_id) {
      throw new RequestError(RequestErrorMessage.validationError);
    }

    const {orders: rows} = await req.Models.Order.list({
      isSeller: true,
      limit: 99999999999,
      seller_id,
    });

    console.log(rows);

    const csvWriter = createCsvWriter({
      header: [
        {id: 'OrderID', title: 'OrderID'},
        {id: 'OrderDate', title: 'OrderDate'},
        {id: 'ShipFirstName', title: 'ShipFirstName'},
        {id: 'ShipLastName', title: 'ShipLastName'},
        {id: 'ShipAddress1', title: 'ShipAddress1'},
        {id: 'ShipAddress2', title: 'ShipAddress2'},
        {id: 'ShipCity', title: 'ShipCity'},
        {id: 'ShipStateCode', title: 'ShipStateCode'},
        {id: 'ShipZipCode', title: 'ShipZipCode'},
        {id: 'ShipPhone', title: 'ShipPhone'},
        {id: 'ShipEmail', title: 'ShipEmail'},
        {id: 'BillFirstName', title: 'BillFirstName'},
        {id: 'BillLastName', title: 'BillLastName'},
        {id: 'BillAddress1', title: 'BillAddress1'},
        {id: 'BillAddress2', title: 'BillAddress2'},
        {id: 'BillZipCode', title: 'BillZipCode'},
        {id: 'ProductID', title: 'ProductID'},
        {id: 'ProductDescription', title: 'ProductDescription'},
        {id: 'QtyPurchased', title: 'QtyPurchased'},
        {id: 'UnitPrice', title: 'UnitPrice'},
        {id: 'ShippingCost', title: 'ShippingCost'},
        {id: 'Tax', title: 'Tax'},
        {id: 'Order_Commission', title: 'Order_Commission'},
        {id: 'ShippingCarrierRequested', title: 'ShippingCarrierRequested'},
        {id: 'ShippingMethodRequested', title: 'ShippingMethodRequested'},
        {id: 'RushOrder', title: 'RushOrder'},
        {id: 'BillEmail', title: 'BillEmail'},
        {id: 'BillPhone', title: 'BillPhone'},
        {id: 'BillStateCode', title: 'BillStateCode'},
        {id: 'BillCity', title: 'BillCity'},
      ],
    });

    const shippingMethods = await req.Models.ShippingMethod.getAll([seller_id], false);
    const methods = arrayToMap(shippingMethods);
    console.log('---mthods', shippingMethods);

    const data = [];
    for (const r of rows) {
      const shipping = r.cart.find(c => c.type === 'shipping');
      const shippingMethod = methods[shipping?.product_id] || DefaultShippingMethod;
      console.log('---shipping', shipping);
      for (const c of r.cart) {
        if (c.type === 'product') {
          console.log(c);
          data.push({
            OrderID: r.id,
            OrderDate: getAbsDateOnly(r.createdAt),
            ShipFirstName: r.firstName,
            ShipLastName: r.lastName,
            ShipAddress1: formatAddressFirstLine(r.address),
            ShipAddress2: formatAddressSecondLine(r.address),
            ShipCity: r.address?.locality,
            ShipStateCode: r.address?.administrative_area_level_1,
            ShipZipCode: r.address?.postal_code,
            ShipPhone: r.phone,
            ShipEmail: 'notifications@incrediblephones.com',
            BillFirstName: r.firstName,
            BillLastName: r.lastName,
            BillAddress1: formatAddressFirstLine(r.address),
            BillAddress2: formatAddressSecondLine(r.address),
            BillZipCode: r.address?.postal_code,

            ProductID: c.product_id,
            ProductDescription: c.description_seller || c.description,
            QtyPurchased: c.quantity,
            UnitPrice: c.price,
            Tax: c.price * r.tax_rate,
            Order_Commission: c.price * CommissionRate,

            ShippingCost: shippingMethod?.price || 0,
            ShippingCarrierRequested: shippingMethod?.carrier || '',
            ShippingMethodRequested:
              shippingMethod?.name || getShippingMethodDescription(shippingMethod, req.t),
            RushOrder: shippingMethod?.price ? 'Y' : 'N',

            BillEmail: 'notifications@incrediblephones.com',
            BillPhone: r.phone,
            BillStateCode: r.address?.postal_code,
            BillCity: r.address?.locality,
          });
        }
      }
    }

    const content = `${csvWriter.getHeaderString()}${csvWriter.stringifyRecords(data)}`;

    return {response: content, attachment: 'report.csv'};
  },
};

export default Controllers;

function normalizeResponse(items, period, previousPeriod, includeTotal = true) {
  const response = {
    start: period.displayStart,
    end: period.displayEnd,
    items,
    previousPeriod,
  };

  if (includeTotal) {
    response.total = items.reduce((prev, {count}) => prev + (count || 0), 0);
  }

  return response;
}

async function normalizeReportPeriod(
  {
    start = new Date().getTime() - 30 * DayMs,
    end = new Date().getTime() + 2 * DayMs,
    appId,
    ...other
  },
  req,
) {
  const timezone = await req.Models.Asset.getTimezone(appId);

  const startDate = addTimezoneToStringDate(
    begginingOfDay(start),
    getTimezoneOffset(timezone, start),
  );

  const offsetMinutes = getTimezoneOffset(timezone, end);
  const endDate = addTimezoneToStringDate(endOfDay(end), offsetMinutes);

  const offset = formatMysqlTimezoneOffset(offsetMinutes);

  return {
    appId,
    displayStart: startDate.split(' ')[0],
    displayEnd: endDate.split(' ')[0],
    timezone,
    offset,
    offsetMinutes,
    start: getUTCDateTimeAsString(new Date(startDate)),
    end: getUTCDateTimeAsString(new Date(endDate)),
    prevStart: getUTCDateTimeAsString(new Date(start - 30 * DayMs)),
    ...other,
  };
}

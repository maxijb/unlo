import Logger from '@Common/logger';
import {SellerConnectionStatus, CartStatus} from '@Common/constants/app';
import {RequestError, RequestErrorMessage} from '@Common/constants/errors';
import {
  ValidStatusTransitions,
  SellercloudOrderStatusCode,
  SellercloudOrderShippingStatus,
} from '@Common/constants/seller-constants';
import {arrayToMultipleMap, arrayToMap} from '@Common/utils/generic-utils';
import {normalizeTrackingCarrier} from '@Common/utils/order-utils';
import {getClient} from '../services/sellercloud';
import Seq from 'sequelize';

const Controllers = {
  async createConnection(query, req) {
    if (!query.account_name || !query.token || !query.api_domain) {
      throw new RequestError(RequestErrorMessage.validationError);
    }

    let instance = await req.Models.SellerConnection.findOne({
      where: {
        type: 'sellercloud',
        seller_id: req.seller_id,
      },
    });

    if (instance) {
      instance.account_name = query.account_name;
      instance.token = query.token;
      instance.api_domain = query.api_domain;
      await instance.save();
    } else {
      instance = await req.Models.SellerConnection.create({
        type: 'sellercloud',
        seller_id: req.seller_id,
        account_name: query.account_name,
        token: query.token,
        api_domain: query.api_domain,
      });
    }

    const SC = await getClient(req, req.seller_id, instance);
    const access_token = await SC.getAuthToken();

    return {id: instance.get('id'), ok: !!access_token};
  },

  async getInventory(query, req) {
    const SC = await getClient(req);
    const result = await SC.getInventory(query);
    return result;
  },

  async syncInventory(query, req) {
    const SC = await getClient(req);

    const ids = await req.Models.Product.findAll({
      where: {
        owner: req.seller_id,
      },
      attributes: ['source_id'],
      raw: true,
    });

    for (let offset = 0; offset < ids.length; offset += SC.defaultQueryPagination) {
      const result = await SC.getInventoryById({
        id: ids.slice(offset, offset + SC.defaultQueryPagination).map(p => p.source_id),
      });
      for (const item of result.Items) {
        const customColumns = arrayToMap(
          item.CustomColumns,
          'ColumnName',
          undefined,
          it => it.Value,
        );

        await req.Models.Product.update(
          {
            stock: item.AggregateQty || 0,
            price:
              customColumns.INCR_ENABLED && customColumns.INCR_PRICE
                ? customColumns.INCR_PRICE
                : item.SitePrice || 0,
          },
          {
            where: {
              owner: req.seller_id,
              source_id: item.ID,
            },
          },
        );
      }
    }

    return {ok: true};
  },

  async syncOrders(query, req) {
    const orders = await req.Models.Cart.findAll({
      where: {
        seller_id: req.seller_id,
        status: Object.keys(ValidStatusTransitions),
        remote_source: 'sellercloud',
        remote_id: {
          [Seq.Op.ne]: null,
        },
      },
      raw: true,
    });

    const byOrder = arrayToMultipleMap(orders, 'remote_id');
    const remoteOrders = Object.keys(byOrder);

    const SC = await getClient(req);

    let last = 0;

    while (last < remoteOrders.length) {
      const next = last + SC.defaultQueryPagination;
      const data = await SC.getOrders({ids: remoteOrders.slice(last, next)});

      for (const item of data.Items) {
        let newStatus = null;

        // Status cancelled or with problems
        if (
          item.StatusCode === SellercloudOrderStatusCode.cancelled ||
          item.StatusCode === SellercloudOrderStatusCode.problemOrder
        ) {
          const newStatus =
            item.StatusCode === SellercloudOrderStatusCode.cancelled
              ? CartStatus.cancelledBySeller
              : CartStatus.withProblems;

          await req.Models.Order.updateCartStatus(byOrder[item.ID], newStatus, req);
          // Fully shipped
        } else if (
          item.ShippingStatus === SellercloudOrderShippingStatus.fullyShipped &&
          item.TrackingNumber
        ) {
          const info = {
            order_id: byOrder[item.ID][0].order_id,
            remote_id: item.ID,
            remote_source: 'sellercloud',
            seller_id: req.seller_id,
            req,
            query: {
              tracking: item.TrackingNumber,
              tracking_carrier: item.ShippingCarrier,
            },
          };

          await req.Models.Order.addTracking(info);
        }
      }
      last = next;
    }
    return {ok: true};
  },

  async cronSyncSellercloudInventory(query, req) {
    Controllers.executeCronSyncSellercloudInventory(query, req);
    return {ok: true};
  },

  async executeCronSyncSellercloudInventory(query, req) {
    const connections = await req.Models.SellerConnection.findAll({
      where: {type: 'sellercloud', status: SellerConnectionStatus.active},
      raw: true,
    });

    for (const connection of connections) {
      req.seller_id = connection.seller_id;
      await Controllers.syncInventory(query, req);
      await Controllers.syncOrders(query, req);
    }
  },
};

export default Controllers;

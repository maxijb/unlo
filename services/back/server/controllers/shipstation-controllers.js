import Logger from '@Common/logger';
import {SellerConnectionStatus, CartStatus} from '@Common/constants/app';
import {RequestError, RequestErrorMessage} from '@Common/constants/errors';
import {ValidStatusTransitions, ShipStationOrderStates} from '@Common/constants/seller-constants';
import {arrayToMultipleMap, arrayToMap} from '@Common/utils/generic-utils';
import {normalizeTrackingCarrier} from '@Common/utils/order-utils';
import {getClient} from '../services/shipstation';
import Seq from 'sequelize';
import moment from 'moment';

const Controllers = {
  async createConnection(query, req) {
    if (!query.account_name || !query.token || !query.api_domain) {
      throw new RequestError(RequestErrorMessage.validationError);
    }

    let instance = await req.Models.SellerConnection.findOne({
      where: {
        type: 'shipstation',
        seller_id: req.seller_id,
      },
    });

    if (instance) {
      instance.account_name = query.account_name;
      instance.token = query.token;
      instance.api_domain = query.api_domain;
      instance.refetch_token = query.refetch_token;
      await instance.save();
    } else {
      instance = await req.Models.SellerConnection.create({
        type: 'shipstation',
        seller_id: req.seller_id,
        account_name: query.account_name,
        token: query.token,
        api_domain: query.api_domain,
        refetch_token: query.refetch_token,
      });
    }

    const SS = await getClient(req, req.seller_id);
    await SS.getOrders();

    return {id: instance.get('id'), ok: true};
  },

  async getOrders(query, req) {
    const SS = await getClient(req);
    return SS.getOrders(query);
  },

  async getShipments(query, req) {
    const SS = await getClient(req);
    return SS.getShipments(query);
  },

  async syncOrders(query, req) {
    const orders = await req.Models.Cart.findAll({
      where: {
        seller_id: req.seller_id,
        status: Object.keys(ValidStatusTransitions),
        remote_id: {
          [Seq.Op.ne]: null,
        },
      },
      order: ['id'],
      raw: true,
    });

    if (!orders.length) {
      return {ok: true};
    }

    const byOrder = arrayToMultipleMap(orders, 'order_id');

    const SS = await getClient(req);
    const olderThan = SS.formatDate(
      moment(orders[0].createdAt).tz(SS.timezone).subtract(1, 'days'),
    );

    let page = 1;
    let ssOrders;
    do {
      ssOrders = await SS.getOrders({
        orderNumber: SS.orderPrefix,
        createDateStart: olderThan,
        page,
      });

      for (const order of ssOrders.orders) {
        const order_id = SS.getIncredibleOrderId(order.orderNumber);
        if (!byOrder.hasOwnProperty(order_id)) {
          continue;
        }
        const cart_ids = byOrder[order_id].map(p => p.id);

        let newStatus = null;
        if (order.orderStatus === ShipStationOrderStates.cancelled) {
          newStatus = CartStatus.cancelledBySeller;
        } else if (order.orderStatus === ShipStationOrderStates.on_hold) {
          newStatus = CartStatus.withProblems;
        }

        if (newStatus) {
          await req.Models.Order.updateCartStatus(byOrder[order_id], newStatus, req);

          // if shipped
        } else if (order.orderStatus === ShipStationOrderStates.shipped) {
          const shipments = await SS.getShipments({
            orderNumber: order.orderNumber,
          });

          if (shipments?.shipments?.[0]?.trackingNumber) {
            const info = {
              order_id: order_id,
              // Shipstation might include orders generated in Brightpealr
              // so we cannot filter by remote ids
              //remote_id: order.orderId,
              //remote_source: 'shipstation',
              seller_id: req.seller_id,
              cart_id: cart_ids,
              req,
              query: {
                tracking: shipments?.shipments?.[0]?.trackingNumber,
                tracking_carrier: shipments?.shipments?.[0].serviceCode,
              },
            };

            await req.Models.Order.addTracking(info);
          }
        }
      }

      page++;
    } while (ssOrders.pages >= page);

    return {
      ok: true,
    };
  },

  async cronSyncShipstationOrders(query, req) {
    Controllers.executeCronSyncShipstationOrders(query, req);
    return {ok: true, date: new Date(), url: req.originalUrl};
  },

  async executeCronSyncShipstationOrders(query, req) {
    const connections = await req.Models.SellerConnection.findAll({
      where: {type: 'shipstation', status: SellerConnectionStatus.active},
      raw: true,
    });

    for (const connection of connections) {
      req.seller_id = connection.seller_id;
      await Controllers.syncOrders(query, req);
    }
  },
};

export default Controllers;

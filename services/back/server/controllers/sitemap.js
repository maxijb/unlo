import xml from 'xml';
import CrudControllers from './crud-controllers';
import {arrayToMap} from '@Common/utils/generic-utils';
import {getPageLink} from '@Common/utils/urls';
import {getUTCDateAsString} from '@Common/utils/date-utils';
import {consolidateAttributesAsName} from '../../../front/shared/utils/attributes-utils';

export default async function (req, res) {
  const list = await CrudControllers.productsList(
    {category_id: 1, limit: 8000, isSitemap: true},
    req,
  );
  const categories = await req.Models.Category.findAll({
    raw: true,
    where: {
      isLeaf: 1,
      visible: 1,
    },
  });
  const carriers = await req.Models.Attribute.getAllCarriers();
  const cat_map = arrayToMap(categories);
  const config = req.Config.get('server');

  const categoriesPureItems = [];

  categories.forEach(c => {
    categoriesPureItems.push({
      url: [
        {
          loc: `${config.protocol}://${config.fullDomain}${getPageLink('category', {
            product: c,
            t: req.t,
          })}`,
        },
        {lastmod: getUTCDateAsString(new Date())},
      ],
    });

    for (const carrier of carriers) {
      categoriesPureItems.push({
        url: [
          {
            loc: `${config.protocol}://${config.fullDomain}${getPageLink('category', {
              product: {
                name: `${c.name} ${carrier.name}`,
                id: c.id,
              },
              t: req.t,
            })}?attr=${carrier.slug}`,
          },
          {lastmod: getUTCDateAsString(new Date())},
        ],
      });
    }
  });

  const categoriesItems = categories.map(c => {
    return {
      url: [
        {
          loc: `${config.protocol}://${config.fullDomain}${getPageLink('product', {
            product: c,
            t: req.t,
          })}`,
        },
        {lastmod: getUTCDateAsString(new Date())},
      ],
    };
  });

  const productItems = Object.keys(list.products).map(key => {
    const it = list.products[key];
    const data = consolidateAttributesAsName(it.attributes, req.t);
    return {
      url: [
        {
          loc: `${config.protocol}://${config.fullDomain}${getPageLink('variant', {
            product: it,
            attributes: data,
            t: req.t,
          })}`,
        },
        {lastmod: getUTCDateAsString(new Date())},
      ],
    };
  });

  const content = xml([...categoriesPureItems, ...categoriesItems, ...productItems], {
    indent: '  ',
  });

  res.send(`
    <?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    ${content}
    </urlset>
  `);
}

/* This file is only included serve-side
 * It's the main entry point for all controllers exposed in the API
 */
import isEmpty from 'is-empty';
import request from 'request-promise';
import {get} from 'lodash';
import {obfuscateEmailForDisplay} from '@Common/utils/security-utils';

import {
  RequestError,
  AuthenticationError,
  RequestErrorMessage,
  AuthenticationErrorMessage,
} from '@Common/constants/errors';
import {Entities, AssetStatus, AssetRole} from '@Common/constants/assets';
import FormValidations from '@Common/constants/form-validations';
import {ValidateForm} from '@Common/utils/validations';
import {encode, decode} from '@Common/server/security/tokens';
import {getHashWithKey} from '@Common/server/security/hashes';
import {createCsrfToken} from '@Common/server/security/auth';
import {uniqueAssets, categorizeAssets} from '@Common/utils/assets-utils';

import {sendEmail} from '../services/sendgrid';
import {forgotPasswordTemplate, validateEmailTemplate} from '../email/templates';
import {
  clientCookieName,
  tokenCookieName,
  FACEBOOK_API_BASE_URL,
  GOOGLE_API_BASE_URL,
  Social,
  CsrfTokenHeader,
  InviteTokenActions,
  ContactCaptureCookieName,
} from '../../../common/constants/app';
import {LoginActions} from '../../../common/constants/social';

const Controllers = {
  /* Create new user and store in DB, return cookie with token
   * @query email {string}
   * @query password {string}
   */
  signup: async (query, req) => {
    const errors = ValidateForm(query, FormValidations.signup);
    if (!isEmpty(errors)) {
      throw new RequestError(RequestErrorMessage.validationError, errors);
    }

    const secret = req.Config.get('server').session.secret;

    const existingUser = await req.Models.User.findOne({
      where: {email: query.email},
    });
    if (existingUser && !existingUser.get('isInvite')) {
      throw new RequestError(RequestErrorMessage.validationError, {
        form: 'singupExistingEmail',
      });
    }

    const password = getHashWithKey(query.password, secret);
    let newUser = null;
    if (existingUser) {
      await existingUser.update({password, isInvite: false});
      newUser = existingUser;
    } else {
      newUser = await req.Models.User.create({
        firstName: query.firstName,
        lastName: query.lastName,
        email: query.email,
        password,
      });
    }

    const isSeller = await updateSellerCondition(newUser, query.inviteToken, req);

    // -------------------- TEMPORARILY DISABLING VALIDATION EMAIL
    // -- Reenable when required, but make sure the validate page is properly themed for this app
    // Controllers.sendValidateEmailToUser(newUser, req);
    // ----------------------------------------------------
    const id = newUser.get('id');
    const token = await encode({id}, secret);
    const csrfToken = await createCsrfToken(req, id);
    await req.Models.Cart.syncUserSession(id, req);

    return {
      cookie: {[tokenCookieName]: [token, {httpOnly: true}]},
      headers: {
        [CsrfTokenHeader]: csrfToken,
      },
      response: {
        id,
        displayName: newUser.get('firstName') || obfuscateEmailForDisplay(newUser.get('email')),
        action: LoginActions.signup,
        isSeller,
        isSuperAdmin: newUser.get('superAdmin'),
      },
    };
  },

  /* Validates user email and sends email to reset password
   * @query email {string}
   */
  async sendForgotPassword(query, req) {
    const errors = ValidateForm(query, FormValidations.forgotPassword);
    if (!isEmpty(errors)) {
      throw new RequestError(RequestErrorMessage.validationError, errors);
    }

    const server = req.Config.get('server');
    const secret = server.session.secret;
    const actionToken = req.Config.get('actionTokens').forgotPassword;

    const user = await req.Models.User.findOne({
      where: {email: query.email},
    });

    if (!user) {
      throw new RequestError(RequestErrorMessage.validationError, {
        form: 'emailNotFound',
      });
    }

    if (!user.get('password')) {
      throw new RequestError(RequestErrorMessage.validationError, {
        form: 'userDoesntOwnPassword',
      });
    }

    const token = await encode({id: user.get('id'), action: actionToken}, secret, {
      expiresIn: '15m',
    });

    const template = await forgotPasswordTemplate({
      email: user.get('email'),
      token,
      server,
    });
    sendEmail(template, req);
    return {};
  },

  async parseToken(query, req) {
    const {token} = query;
    const secret = req.Config.get('server').session.secret;
    const {action, id} = await decode(token, secret);
    return {action, id};
  },
  /* Validates a reset-password token
   * @query token {string}
   */
  async validateForgotPasswordToken(query, req) {
    const {token} = query;
    const secret = req.Config.get('server').session.secret;

    let userId;
    try {
      const {action, id} = await decode(token, secret);
      if (action !== req.Config.get('actionTokens').forgotPassword || !id) {
        throw new AuthenticationError(AuthenticationErrorMessage.invalidToken);
      }
      userId = id;

      const user = await req.Models.User.findOne({where: {id}});
      if (!user) {
        throw new AuthenticationError(AuthenticationErrorMessage.userNotFound);
      }
    } catch (e) {
      throw new AuthenticationError(e.name);
    }

    return {id: userId, status: 'ok', token};
  },

  /* Validates a email-validation token
   * @query token {string}
   */
  async validateEmailToken(query, req) {
    const {token} = query;
    const secret = req.Config.get('server').session.secret;

    let userId;
    try {
      const {action, id} = await decode(token, secret);
      if (action !== req.Config.get('actionTokens').validateEmail || !id) {
        throw new AuthenticationError(AuthenticationErrorMessage.invalidToken);
      }
      userId = id;
    } catch (e) {
      throw new AuthenticationError(e.name);
    }

    const user = await req.Models.User.findOne({where: {id: userId}});
    if (!user) {
      throw new AuthenticationError(AuthenticationErrorMessage.userNotFound);
    }

    await user.update({validated: 1});

    return {id: userId, email: user.email, token};
  },

  /* Resets a password based on a token
   * @query token {string}
   * @query password {string} new pass
   * @query id {number} userId
   */
  async resetPassword(query, req) {
    const {token, password, id} = query;
    const secret = req.Config.get('server').session.secret;

    let userId;
    try {
      const data = await decode(token, secret);
      if (
        data.action !== req.Config.get('actionTokens').forgotPassword ||
        !data.id ||
        data.id !== query.id
      ) {
        throw new AuthenticationError(AuthenticationErrorMessage.invalidToken);
      }
      userId = data.id;
    } catch (e) {
      throw new AuthenticationError(e.name);
    }

    const user = await req.Models.User.findOne({where: {id: userId}});
    if (!user) {
      throw new AuthenticationError(AuthenticationErrorMessage.userNotFound);
    }

    await user.update({
      password: getHashWithKey(password, secret),
    });

    return {status: 'ok'};
  },

  /* Test login credentials and returns cookie with session token
   * @query email {string}
   * @query password {string}
   */
  login: async (query, req) => {
    const errors = ValidateForm(query, FormValidations.signup);
    if (!isEmpty(errors)) {
      throw new RequestError(RequestErrorMessage.validationError, errors);
    }

    const secret = req.Config.get('server').session.secret;

    const existingUser = await req.Models.User.findOne({
      where: {
        email: query.email,
        password: getHashWithKey(query.password, secret),
        isInvite: false,
      },
    });
    if (!existingUser) {
      throw new RequestError(RequestErrorMessage.validationError, {
        form: 'userNotFound',
      });
    }

    const {id, username, firstName, lastName, email} = existingUser.get();
    const isSeller = await updateSellerCondition(existingUser, query.inviteToken, req);

    const token = await encode({id}, secret);
    const csrfToken = await createCsrfToken(req, id);
    await req.Models.Cart.syncUserSession(id, req);

    return {
      cookie: {[tokenCookieName]: [token, {httpOnly: true}]},
      headers: {
        [CsrfTokenHeader]: csrfToken,
      },
      response: {
        id,
        displayName: firstName || obfuscateEmailForDisplay(email),
        action: LoginActions.signin,
        isSeller,
        isSuperAdmin: existingUser.get('superAdmin'),
      },
    };
  },

  /* Get public settings of a user
   * should alway exclude password
   * @query id {integer} user id
   */
  async getUserDetails(query, req) {
    if (!query.id) {
      throw new RequestError(RequestErrorMessage.missingUserId);
    }
    if (query.id.toString() !== req.auth.id.toString()) {
      throw new AuthenticationError(AuthenticationErrorMessage.invalidUserId);
    }

    const user = await req.Models.User.findOne({
      where: {id: query.id},
      attributes: {exclude: ['stripeId', 'password']},
    });

    if (!user) {
      throw new RequestError(RequestErrorMessage.userNotFound);
    }

    const {password, ...data} = user.get();
    return {...data};
  },

  async getUserOwnMinimumDetails(query, req) {
    const user = await req.Models.User.getUserOwnMinimumDetails(req);

    if (!user) {
      throw new RequestError(RequestErrorMessage.userNotFound);
    }

    const {firstName, email} = user;
    return user;
  },

  async getUserOwnExtendedDetails(query, req) {
    if (!req.auth?.id) {
      throw new RequestError(RequestErrorMessage.missingUserId);
    }

    const user = await req.Models.User.getUserOwnExtendedDetails(req);

    if (!user) {
      throw new RequestError(RequestErrorMessage.userNotFound);
    }

    const primaryAddress = await req.Models.Address.getPrimaryAddressForUser(req.auth.id);

    const {firstName, lastName, email, phone} = user;
    return {
      firstName,
      lastName,
      email,
      primaryAddress,
      phone,
    };
  },

  async resendValidationToken(query, req) {
    if (!req.auth.id) {
      throw new RequestError(RequestErrorMessage.missingUserId);
    }

    const user = await req.Models.User.findOne({where: {id: req.auth.id}});
    if (!user) {
      throw new RequestError(RequestErrorMessage.userNotFound);
    }

    if (user.get('validated')) {
      throw new RequestError(RequestErrorMessage.userAlreadyValidated);
    }

    Controllers.sendValidateEmailToUser(user, req);
    return {};
  },

  /* Login or create account based on facebook user
   * If user doesn't exist it'll create a new user
   * @query accessToken {string} facebook temporary user token
   * @query userID {string} facebook user id, coming from facebook client-side login SDK
   */
  async facebookLogin(query, req) {
    const fbKeys = req.Config.get('social').facebook;

    // 1st validate access_token to make sure it's valid with facebook
    // otherwise we might get fake tokens from malicious parties
    const url = `${FACEBOOK_API_BASE_URL}debug_token?input_token=${query.accessToken}&access_token=${fbKeys.public}|${fbKeys.secret}`;
    const fbResponse = await request({uri: url, json: true});
    // if invalid token
    if (fbResponse.data.error) {
      req.Logger.error(fbResponse.data.error);
      throw new AuthenticationError(AuthenticationErrorMessage.invalidFBToken);
    }
    // if user if doesn't match what we're sending from the client
    if (fbResponse.data.user_id !== query.userID) {
      req.Logger.error(fbResponse.data.user_id, '!==', query.userID);
      throw new AuthenticationError(AuthenticationErrorMessage.invalidFBUserId);
    }

    // check if socialConection exists
    const existingSocial = await req.Models.SocialConnection.findOne({
      where: {type: Social.facebook, socialId: fbResponse.data.user_id},
    });

    let id;
    let existingUser = true;
    if (!existingSocial) {
      // if it doesn't exits request my data from FB
      const me = await request({
        uri: `${FACEBOOK_API_BASE_URL}${fbResponse.data.user_id}?fields=first_name,last_name,email,name,picture.type(large)&access_token=${query.accessToken}`,
        json: true,
      });

      // ... and check for an existing user
      let user = await req.Models.User.findOne({where: {email: me.email, isInvite: false}});

      // create one if not found
      if (!user) {
        user = await req.Models.User.create({
          email: me.email,
          name: me.name,
          lastName: me.last_name,
          firstName: me.first_name,
          avatar: `https://graph.facebook.com/${fbResponse.data.user_id}/picture?type=large`,
          validated: 1,
          isInvite: false,
        });
        existingUser = false;
      } else if (!user.get('validated') || user.get('isInvite')) {
        await user.update({validated: 1, isInvite: false});
      }

      id = user.get('id');
      // finally store the new connection
      await req.Models.SocialConnection.create({
        type: 'facebook',
        socialId: fbResponse.data.user_id,
        userId: id,
      });
    } else {
      // if user exists just get his user id
      id = existingSocial.get('userId');
    }

    // generate token and return cookie and user id
    const token = await encode({id}, req.Config.get('server').session.secret);
    const csrfToken = await createCsrfToken(req, id);
    const newUser = await req.Models.User.findOne({where: {id}});
    const isSeller = await updateSellerCondition(newUser, query.inviteToken, req);
    await req.Models.Cart.syncUserSession(id, req);

    return {
      cookie: {[tokenCookieName]: [token, {httpOnly: true}]},
      headers: {
        [CsrfTokenHeader]: csrfToken,
      },
      response: {
        id,
        displayName: newUser.get('firstName') || obfuscateEmailForDisplay(newUser.get('email')),
        action: existingUser ? LoginActions.signin : LoginActions.signup,
        isSeller,
        isSuperAdmin: newUser.get('superAdmin'),
      },
    };
  },

  /* Login or create account based on google user
   * If user doesn't exist it'll create a new user
   * @query token {stirng} google temporary user token
   */
  async googleLogin(query, req) {
    const googleKeys = req.Config.get('social').google;
    // 1st validate access_token to make sure it's valid with google
    // otherwise we might get fake tokens from malicious parties
    const url = `${GOOGLE_API_BASE_URL}oauth2/v3/tokeninfo?id_token=${query.token}`;
    const response = await request({uri: url, json: true});
    // if invalid token
    if (response.error_description) {
      req.Logger.error(response.error_description);
      throw new AuthenticationError(AuthenticationErrorMessage.invalidGOOGLEToken);
    }

    const {
      email,
      name,
      aud: appClientId,
      sub: socialId,
      given_name: firstName,
      family_name: lastName,
      picture: avatar,
    } = response;

    if (appClientId !== googleKeys.public) {
      throw new AuthenticationError(AuthenticationErrorMessage.invalidGOOGLEToken);
    }

    // check if socialConection exists
    const existingSocial = await req.Models.SocialConnection.findOne({
      where: {type: Social.google, socialId},
    });

    let id;
    let existingUser = true;
    if (!existingSocial) {
      // if it doesn't exits create a new user
      let user = await req.Models.User.findOne({where: {email}});

      if (!user) {
        user = await req.Models.User.create({
          email,
          lastName,
          firstName,
          avatar,
          validated: 1,
          isInvite: false,
        });
        existingUser = false;
        if (!user) {
          throw new Error('Server Error');
        }
      } else if (!user.get('validated') || user.get('isInvite')) {
        await user.update({
          lastName: user.get('lastName') || lastName,
          firstName: user.get('firstName') || firstName,
          avatar: user.get('avatar') || avatar,
          validated: 1,
          isInvite: false,
        });
      }

      id = user.get('id');
      // finally store the new connection
      await req.Models.SocialConnection.create({
        type: 'google',
        socialId,
        userId: id,
      });
    } else {
      // if user exists just get his user id
      id = existingSocial.get('userId');
    }

    // generate token and return cookie and user id
    const token = await encode({id}, req.Config.get('server').session.secret);
    const csrfToken = await createCsrfToken(req, id);
    const newUser = await req.Models.User.findOne({where: {id}});
    const isSeller = await updateSellerCondition(newUser, query.inviteToken, req);
    await req.Models.Cart.syncUserSession(id, req);
    return {
      cookie: {[tokenCookieName]: [token, {httpOnly: true}]},
      headers: {
        [CsrfTokenHeader]: csrfToken,
      },
      response: {
        id,
        displayName: newUser.get('firstName') || obfuscateEmailForDisplay(newUser.get('email')),
        action: existingUser ? LoginActions.signin : LoginActions.signup,
        isSeller,
        isSuperAdmin: newUser.get('superAdmin'),
      },
    };
  },

  /*
   * Closes a session by removing the token id from the cookie
   * @query {} empty
   */
  async logout(query, req) {
    const csrfToken = await createCsrfToken(req, null);
    return {
      cookie: {[tokenCookieName]: ''},
      headers: {
        [CsrfTokenHeader]: csrfToken,
      },
      response: {
        status: 'ok',
        action: LoginActions.logout,
      },
    };
  },
  /* Sends a validation token to an user email
   * @param user {seq.User}
   * @param req {req}
   */
  async sendValidateEmailToUser(user, req) {
    const {email, id} = user.get();
    const action = req.Config.get('actionTokens').validateEmail;
    const server = req.Config.get('server');
    const secret = server.session.secret;
    const token = await encode({id, action}, secret, {expiresIn: '24h'});
    const template = await validateEmailTemplate({email, token, server});
    sendEmail(template, req);
  },

  async saveSettings(query, req) {
    await req.Models.User.update(query, {
      where: {id: req.auth.id},
    });
  },

  async createApp(query, req) {
    const {appName = ''} = query;
    const userId = req.auth.id;

    const app = await req.Models.Asset.create({
      name: appName,
      type: Entities.app,
      createdBy: userId,
    });
    await req.Models.AssetRelationship.create({
      ownerType: Entities.user,
      ownerId: userId,
      assetType: Entities.app,
      assetId: app.get('id'),
      status: AssetStatus.member,
      role: AssetRole.owner,
    });

    return {id: app.get('id')};
  },

  async unsubscribeEmail(query, req) {
    const {token} = query;

    if (!query.token) {
      throw new AuthenticationError(AuthenticationErrorMessage.missingToken);
    }

    let info = {};
    try {
      const {app, email} = await decode(token, req);
      if (!email) {
        throw new AuthenticationError(AuthenticationErrorMessage.invalidToken);
      }
      info = {app, email};
    } catch (e) {
      req.Logger.error(e);
      throw new AuthenticationError(AuthenticationErrorMessage.invalidToken);
    }

    if (info.app) {
      info.appData = await req.Models.Asset.getBasicDetails(info.app);
    }

    await req.Models.Unsubscribed.create(
      {appId: info.app || 0, email: info.email},
      {ignoreDuplicates: true},
    );

    return {...info, token};
  },

  async reenableEmail(query, req) {
    const {token} = query;

    if (!query.token) {
      throw new AuthenticationError(AuthenticationErrorMessage.missingToken);
    }

    let info = {};
    try {
      const {app, email} = await decode(token, req);
      if (!email) {
        throw new AuthenticationError(AuthenticationErrorMessage.invalidToken);
      }
      info = {app, email};
    } catch (e) {
      req.Logger.error(e);
      throw new AuthenticationError(AuthenticationErrorMessage.invalidToken);
    }

    await req.Models.Unsubscribed.destroy({where: {appId: info.app || 0, email: info.email}});

    return {...info, token};
  },

  async isSellerOrSuperAdmin(query, req) {
    if (!req.auth?.id) {
      return {isSellerOrSuperAdmin: false};
    }
    const {isSellerOrSuperAdmin} = await req.Models.User.isSellerOrSuperAdmin(req.auth.id);
    return {isSellerOrSuperAdmin};
  },

  async isSuperAdmin(query, req) {
    if (!req.auth?.id) {
      return {isSuperAdmin: false};
    }

    const isSuperAdmin = await req.Models.User.isSuperAdmin(req.auth.id);
    return {isSuperAdmin};
  },

  async trackActivity(query, req) {
    const data = {...query};

    if (!req.auth?.id && !req.cookies[clientCookieName] && !query.cid) {
      throw new Error(RequestErrorMessage.validationError);
    }

    if (req.auth?.id) {
      data.user_id = req.auth.id;
    }
    if (req.cookies[clientCookieName]) {
      data.cid = req.cookies[clientCookieName];
    }

    if (!query.type && !query.subtype) {
      throw new Error(RequestErrorMessage.validationError);
    }

    await req.Models.Activity.create(data);
  },

  async saveContact(query, req) {
    if (!query.email) {
      throw new RequestError(RequestErrorMessage.validationError);
    }

    query.cid = req.cookies[clientCookieName];

    await req.Models.Contact.add(query);
    return {
      response: {ok: true},
      cookie: {[ContactCaptureCookieName]: [1]},
    };
  },
};

export default Controllers;

async function updateSellerCondition(user, token, req) {
  if (user.get('isSeller')) {
    return true;
  }

  try {
    const {action, seller_id} = await decode(token, req);
    if (action === InviteTokenActions.newSeller) {
      if (!seller_id) {
        const seller = await req.Models.Seller.create({});
        user.seller_id = seller.get('id');
      } else {
        user.seller_id = seller_id;
      }
      user.isSeller = true;
      await user.save();
      return true;
    }
  } catch (e) {
    return false;
  }
  return false;
}

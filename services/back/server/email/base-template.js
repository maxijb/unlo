import React from 'react';
import {cdnURL, UserFilesCdnURL, ReplaceBigImage} from '@Common/utils/statif-assets-utils';
import {UnsubscribeLinkPattern} from '@Common/constants/notifications';
import {
  formatOperatorName,
  formatFullAddressTxt,
  formatFullAddressHtml,
  p,
  pbig,
  psmall,
  pframe,
} from '@Common/utils/format-utils';

export async function baseEmailTemplate(query, req) {
  const {protocol, domain, mailDomain, mailLinkDomain, appName} = req.Config.get('server');
  const {txt, html, subject, sendTo, includeGreetings, includeLinks, bcc} = query;
  if (!sendTo) {
    return null;
  }
  const to = multipleRecipients(sendTo);
  const msg = {
    to,
    from: `orders@${mailDomain}`,
    subject: subject,
    text: txt || txtTemplate(query, req, includeLinks, includeGreetings),
    html: html || htmlTemplate(query, req, includeLinks, includeGreetings),
  };
  if (bcc) {
    msg.bcc = bcc;
  }
  return msg;
}

export function txtTemplate(query, req) {
  const config = req.Config.get('server');

  return `${query.message}

${req.t('dashboard:dontLikeTheseEmails?')} ${req.t('dashboard:unsubscribeHere')}
${UnsubscribeLinkPattern}
`;
}

export function htmlTemplate(query) {
  return `
		<!DOCTYPE html>
			<html>
			  <head>
			    <meta name="viewport" content="width=device-width">
			    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			    <title>${query.subject}</title>
			</head>
			  <body class="" style="font-family: 'Open Sans', 'Helvetica Neue', arial, sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.5; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; margin: 0; padding: 0;" bgcolor="#f6f6f6">
			    ${query.body}
			  </body>
			</html>
	`;
}

export function htmlEmailBody(query, req, includeLinks = true, includeGreetings = false) {
  const {operator, app = {}, business} = query;
  const config = req.config || req.Config.get('server');

  const actionButton = getActionButton(query.link, query.actionButton, includeLinks);
  const appAddress = formatFullAddressHtml(business || app.business);

  const unsubscribeLink = includeLinks ? UnsubscribeLinkPattern : '#';

  const footerLink = includeLinks ? `${config.protocol}://${config.domain}` : '#';
  const appLogo = !app.avatar
    ? ''
    : `
    <img src="${ReplaceBigImage(UserFilesCdnURL(app.avatar))}" alt="${
        app.name
      }" style="-ms-interpolation-mode: bicubic; max-width: 100%; max-height: 122px; border: none;"/>
  `;

  const message =
    query.message && query.message.indexOf('</p>') === -1
      ? query.message
          .split('\n')
          .map(d => (!d ? '<br/>' : p(d)))
          .join('')
      : query.message;

  const message2 =
    query.message2 && query.message2.indexOf('</p>') === -1
      ? query.message2
          .split('\n')
          .map(d => (!d ? '<br/>' : p(d)))
          .join('')
      : query.message2;

  return `
		<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" bgcolor="#f6f6f6">
      <tr>
        <td style="font-family: 'Montserrat', sans-serif; font-size: 14px;" valign="top"> </td>
        <td class="container" style="font-family: 'Montserrat', sans-serif; font-size: 14px; display: block; max-width: 580px; margin: 0 auto; padding: 10px;" valign="top">
          <div class="content" style="box-sizing: border-box; display: block; max-width: 580px; margin: 0 auto; padding: 10px;">
            <!-- START LOGO HEADER -->
            <div class="header" style="clear: both; margin-top: 50px; margin-bottom: 20px; width: 100%;" align="center">
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
              <tr>
                  <td align="center" style="font-family: 'Montserrat', sans-serif; font-size: 14px;" valign="top">
                  ${appLogo}
                  </td>
                </tr>
              </table>
            </div>

            <!-- START CENTERED WHITE CONTAINER -->
            <table role="presentation" class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; border-radius: 6px; border-top-width: 10px; border-top-color: #4578DB; border-top-style: solid;" bgcolor="#ffffff">

              <!-- START MAIN CONTENT AREA -->
              <tr>
                <td class="wrapper" style="font-family: 'Montserrat', sans-serif; font-size: 14px; box-sizing: border-box; padding: 20px;" valign="top">
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                    <tr>
                      <td style="font-family: 'Montserrat', sans-serif; font-size: 14px;" valign="top">
                        ${message || ''}
                        ${actionButton}
                        ${message2 || ''}
                        ${
                          /*includeGreetings && operator
                            ? p(formatOperatorName(operator), 'margin: 20px 0 10px 0')
                            : */ ''
                        }
                        ${
                          includeGreetings && app.name
                            ? `${psmall(app.name, 'font-weight: bold;')}${appAddress}`
                            : ''
                        }
                        <p class="grey small" style="font-family: 'Montserrat', sans-serif; font-size: 12px; font-weight: normal; color: #999999; margin: 20px 0 15px;">
                       		<span>${req.t('dashboard:dontLikeTheseEmails?')} </span>
                       		<a href="${unsubscribeLink}" style="color: #999999; text-decoration: underline;">
                       			${req.t('dashboard:unsubscribe')}
                       		</a>
                        </p>
											</td>
                    </tr>
                  </table>
                </td>
              </tr>

            <!-- END MAIN CONTENT AREA -->
            </table>
            <!-- END CENTERED WHITE CONTAINER -->

            <!-- START FOOTER -->
            <div class="footer" style="clear: both; margin-top: 10px; width: 100%;" align="center">
              <table role="presentation" border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                <td class="content-block" style="font-family: 'Montserrat', sans-serif; font-size: 12px; padding-bottom: 10px; padding-top: 10px; color: #999999;" align="center" valign="top">
									<a href="${footerLink}" style="color: #999999; font-size: 12px; text-align: center; text-decoration: none;">
                		<img src="${cdnURL('/static/email/poweredbywiri@2x.jpg')}" alt="${req.t(
    'dashboard:poweredByWiri',
  )}" height="26" width="144" style="-ms-interpolation-mode: bicubic; max-width: 100%; border: none;"/>
									</a>
                </td>
              </table>
            </div>
            <!-- END FOOTER -->

          </div>
        </td>
        <td style="font-family: 'Montserrat', sans-serif; font-size: 14px;" valign="top"></td>
      </tr>
    </table>
	`;
}

function getActionButton(link, action, includeLinks) {
  if (!link) {
    return '';
  }

  return `
		<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box;">
      <tbody>
        <tr>
          <td align="left" style="font-family: 'Montserrat', sans-serif; font-size: 14px; padding-bottom: 15px;" valign="top">
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
              <tbody>
                <tr>
                  <td style="font-family: 'Montserrat', sans-serif; font-size: 14px; border-radius: 5px;" align="center" bgcolor="#4578DB" valign="top">
                  	<a href="${
                      includeLinks ? link : '#'
                    }" target="_blank" style="color: #ffffff; text-decoration: none; background-color: #4578DB; border-radius: 5px; box-sizing: border-box; cursor: pointer; display: inline-block; font-size: 14px; font-weight: bold; text-transform: capitalize; margin: 0; padding: 12px 75px; border: 1px solid #4578db;">
                  		${action}
                  	</a>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
	`;
}

export function multipleRecipients(list) {
  if (typeof list === 'string') {
    return list
      .split(',')
      .map(s => s.trim())
      .filter(Boolean);
  }
  return list;
}

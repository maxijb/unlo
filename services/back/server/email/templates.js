import {get} from 'lodash';
import {baseEmailTemplate} from './base-template.js';
import {p, pbig, pframe, psmall} from '@Common/utils/format-utils';
import {encode} from '@Common/server/security/tokens';
import {UITypes} from '@Common/constants/input';
import moment from 'moment';
import {getPageLink, getGoogleCalendarLinkFromStr} from '@Common/utils/urls';
import Defaults from '@Widget/config/defaults';

export const forgotPasswordTemplate = async query => {
  const {protocol, domain, mailDomain, mailLinkDomain, appName} = query.server;
  const msg = {
    to: query.email,
    from: `no-replay@${mailDomain}`,
    subject: `${appName}: Forgotten Password`,
    text: `
      Please go to ${protocol}://${mailLinkDomain || domain}/reset-password?token=${query.token}
    `,
    html: `
      <div>In order to reset your password, please go to</div>
      <div>
        <a href="${protocol}://${mailLinkDomain || domain}/reset-password?token=${query.token}">
          ${protocol}://${mailLinkDomain || domain}/reset-password?token=${query.token}
        </a>
      </div>
      <div>Thanks!</div>
    `,
  };
  return msg;
};

export const validateEmailTemplate = async query => {
  const {protocol, domain, mailDomain, mailLinkDomain, appName} = query.server;
  const link = `${protocol}://${mailLinkDomain || domain}/validate-email?token=${query.token}`;

  const msg = {
    to: query.email,
    from: `no-reply@${mailDomain}`,
    subject: `Welcome to ${appName}!`,
    text: `
      Hi there,

      Welcome to ${appName}, your account has been created and we just need you to confirm your email is valid.
      Please click on this link to complete your account setup:
      ${link}
    `,
    html: `
      ${p('Hi there,')}
      ${p(
        `Welcome to ${appName}, your account has been created and we just need you to confirm your email is valid.`,
      )}
      ${p(`Please click <a href="${link}">here</a> to complete your account setup.`)}
    `,
  };
  return msg;
};

export const inviteTeammateTemplate = async (query, req) => {
  const {
    server: {protocol, domain, mailDomain, mailLinkDomain, appName: wiri},
    sender,
    app,
  } = query;

  return baseEmailTemplate(
    {
      sendTo: query.email,
      subject: `${sender.firstName} has invited you to join ${app.name} on ${wiri}`,
      txt: `
      Join ${app.name} on ${wiri}.

      ${sender.firstName} ${sender.lastName} has invited you to join the ${
        app.name
      } workspace on ${wiri}.

      Click on the following link to accept the invite and create your account:

      ${protocol}://${mailLinkDomain || domain}/dashboard/invite?token=${query.token}

      Sent by wiri.io
    `,
      message: `
      ${p(`Join <b>${app.name}</b> on <b>${wiri}</b>`)}
      ${p(
        `<b>${sender.firstName} ${sender.lastName}</b> has invited you to join the <b>${app.name}</b> workspace on <b>${wiri}</b>`,
      )}
      ${p('Click on the following link to accept the invite and create your account:')}
    `,
      actionButton: 'JOIN NOW',
      link: `${protocol}://${mailLinkDomain || domain}/dashboard/invite?token=${query.token}`,
      app: app,
    },
    req,
  );
  return msg;
};

export const sendMessageNotificationTemplate = async (query, req) => {
  const {
    server: {protocol, domain, mailDomain, mailLinkDomain, appName: wiri},
    operator,
    app,
    user,
    message,
    interactionId,
  } = query;

  const token = await encode({action: UITypes.chat, id: interactionId, appId: app.appId}, req);

  return baseEmailTemplate(
    {
      sendTo: user.email,
      subject: `You've got a new message from ${app.name}`,
      txt: `
      ${operator.firstName} has replied to your message in ${app.name}.

      Click on the following link to continue the conversation:

      ${app.website}#${wiri.toLowerCase()}-action=${token}

      Sent by wiri.io
    `,
      message: `
      ${p(`${operator.firstName} has replied to your message in ${app.name}.`)}
      ${pframe(message)}
      ${p(`Click on the following link to continue the conversation:`)}
    `,
      actionButton: 'REPLY AND RESUME CONVERSATION',
      link: `${app.website}#${wiri.toLowerCase()}-action=${token}`,
      app,
      operator: operator,
      includeGreetings: true,
    },
    req,
  );
  return msg;
};

export const sendOperatorsMessageNotificationTemplate = async (query, req) => {
  const {
    server: {protocol, domain, mailDomain, mailLinkDomain, appName: wiri},
    message: {interactionId},
    app,
    sendTo,
  } = query;

  return baseEmailTemplate(
    {
      sendTo,
      subject: `We've got unattended messages for ${app.name}`,
      txt: `
      We have received new messages in ${app.name} website, and there is no online operators.

      Click here to reply to the user:

      ${protocol}://${domain}${getPageLink('chat')}?interactionId=${interactionId}

      Sent by wiri.io
    `,
      message: `
      We have received new messages in ${app.name} website, and there is no online operators.

      ${p(`Click here to reply to the user:`)}
    `,
      actionButton: 'REPLY PENDING MESSAGES',
      link: `${protocol}://${domain}${getPageLink('chat')}?interactionId=${interactionId}`,
      app,
      includeGreetings: true,
    },
    req,
  );
  return msg;
};

export const sendOperatorBookingNotificationTemplate = async (query, req) => {
  const {user, booking, app, admins} = query;
  const {protocol, domain, mailDomain, mailLinkDomain, appName: wiri} = req.Config.get('server');
  const token = await encode({action: UITypes.booking, id: booking.id, appId: app.appId}, req);

  const dataLine1 = moment(booking.startDate).tz(app.timezone).format('dddd, MMM D [at] hh:mm a');
  const dataLine2 = `Table for ${booking.value} in ${booking.label} area`;

  return baseEmailTemplate(
    {
      sendTo: admins,
      subject: `New table reservation for ${app.name}`,
      txt: `
      We have received a new reservation for ${app.name}.

      These are the details:

      ${dataLine1}
      ${dataLine2}

      To view this booking in your admin dashboard, please click here: ${protocol}://${domain}/dashboard/calendar?reservation=${token}

      Sent by ${domain}
    `,
      message: [
        p(`We have received a new reservation for ${app.name}.`),
        p('These are the details:'),
        pbig(dataLine1, 'margin-bottom: 0'),
        pbig(dataLine2),
        p(
          `To view this booking in your admin dashboard, please click <a href="${protocol}://${domain}/dashboard/calendar?reservation=${token}">here</a>.`,
        ),
        p(`Sent by <a href="${protocol}://${app.domain}">${domain}</a>.`),
      ].join('\n'),
      app,
      includeGreetings: false,
    },
    req,
  );
  return msg;
};

export const sendBookingNotificationTemplate = async (query, req) => {
  const {user, booking, app} = query;

  const {protocol, domain, mailDomain, mailLinkDomain, appName: wiri} = req.Config.get('server');
  const token = await encode({action: UITypes.booking, id: booking.id, appId: app.appId}, req);

  const dataLine1 = moment(booking.startDate).tz(app.timezone).format('dddd, MMM D [at] hh:mm a');
  const dataLine2 = `Table for ${booking.value} in ${booking.label} area`;

  const duration = get(app.options, 'bookings.duration') || Defaults.defaultBookingDuration;
  const googleLink = getGoogleCalendarLinkFromStr({
    strDate: moment(booking.startDate).tz(app.timezone).format('YYYYMMDD[T]hhmmss'),
    strDate2: moment(booking.startDate)
      .add(duration, 'minutes')
      .tz(app.timezone)
      .format('YYYYMMDD[T]hhmmss'),
    options: app.options,
    timezoneName: app.timezone,
  });
  const googleLinkElement = `<a href="${googleLink}" style="font-size: 11px; margin-left: 16px;">Add to Google Calendar</a>`;

  return baseEmailTemplate(
    {
      sendTo: user.email,
      subject: `Your reservation at ${app.name} is confirmed`,
      txt: `
      Hi ${user.name},

      We have received your reservation and look forward to seeing you here.

      Here are the details of your reservation:

      ${dataLine1}
      ${dataLine2}

      If you need to cancel or modify your reservation click here: ${
        app.website
      }#${wiri.toLowerCase()}-action=${token}

      If you have any questions, please contact us on our website ${app.website}

      Kind regards,
    `,
      message: [
        p('We have received your reservation and look forward to seeing you here.'),
        p('Here are the details of your reservation:'),
        pbig(dataLine1 + googleLinkElement, 'margin-bottom: 0'),
        pbig(dataLine2),
        p(
          `If you need to cancel or modify your reservation, click <a href="${
            app.website
          }#${wiri.toLowerCase()}-action=${token}">here</a>.`,
        ),
        p(
          `If you have any questions, please contact us on <a href="${app.website}">our website</a>.`,
        ),
      ].join('\n'),
      app,
      includeGreetings: true,
    },
    req,
  );
  return msg;
};

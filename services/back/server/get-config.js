import path from 'path';
import fetchConfig from 'zero-config';

let cache = null;

export default function getConfig() {
  if (!cache) {
    cache = fetchConfig(path.join(__dirname, '../../common'), {
      dcValue: 'dc',
      env: {NODE_ENV: process.env.NODE_ENV || 'development'},
      loose: false,
    });
  }
  return cache;
}

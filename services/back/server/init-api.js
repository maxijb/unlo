import {uniq} from 'lodash';
import api from '@Common/api/api';
import {formatUri} from '@Common/utils/urls';
import {getRequestQuery} from '@Common/utils/requests';
import Logger from '@Common/logger';
import {safeMiddleware, handleError} from '@Common/utils/requests';
import Controllers from './controllers';
import Middleware from './middleware';

let AssignedMiddlewares = [];
let ClientAssignedMiddlewares = [];

// Specifies which middlewares whould be run for sockets controllers as well
export const SocketAvailableMiddleware = {
  preHasAssetAccess: true,
};

export const ClientSocketAvailableMiddleware = {
  preAuthorize: true,
  preHasAssetAccess: true,
};

export default function (server) {
  startApiKeys(api, server);
}

export const getMiddlewares = () => AssignedMiddlewares;
export const getClientMiddlewares = () => ClientAssignedMiddlewares;

/* Recursive function that exposes a route for each api method */
function startApiKeys(base, server, prefix = '', prefixMiddlewares = []) {
  Object.keys(base).forEach(key => {
    // We ignore the key middleware as it's a protected keyword to define behavior in the api
    if (key === 'middleware' || key === 'method') {
      return;
    }

    if (!base[key].hasOwnProperty('controller') && !base[key].hasOwnProperty('socketController')) {
      /* .controller is where we define the handler for our api endpoint
       * if .controller in an item doesn't exist we assume that we need to recurse to its children.
       * The lack of controller doesn't mean that we cannot add a common middleware to all its children
       */
      let accumulatedMiddlewares = prefixMiddlewares;
      if (base[key].middleware) {
        const uri = formatUri(`${prefix}${key}.*`);
        // if no method is specified, middleware will be applied to all
        // This allows is to have different middleware per method on a global scal
        const {middleware, method = 'USE'} = base[key];

        Logger.info('Adding global middleware to', uri, method, middleware);
        accumulatedMiddlewares = prefixMiddlewares.concat(middleware);
        server[method.toLowerCase()](uri, resolveMiddleware(middleware).map(safeMiddleware));
      }
      // recurse
      startApiKeys(base[key], server, `${prefix}${key}.`, accumulatedMiddlewares);
    } else if (base[key].hasOwnProperty('controller')) {
      /* if there is .controller defined, we initiate thar route
       * with local middleware
       */
      const item = base[key];
      const {controller, method = 'GET', path = `${prefix}${key}`, middleware = []} = item;

      const controllerParts = controller.split('.');

      // Traverse the parts of the controller and find the right method
      let controllerMethod = Controllers;
      while (controllerParts.length) {
        const part = controllerParts.shift();
        controllerMethod = controllerMethod[part];
      }

      const uri = formatUri(path);

      // concat all middleware and add controller at the end
      const handlers = resolveMiddleware(middleware).map(safeMiddleware);
      handlers.push(genericController.bind(null, controllerMethod));

      const currentMiddlewares = uniq(prefixMiddlewares.concat(middleware));
      AssignedMiddlewares[controller] = currentMiddlewares.filter(m =>
        Boolean(SocketAvailableMiddleware[m]),
      );
      ClientAssignedMiddlewares[controller] = currentMiddlewares.filter(m =>
        Boolean(ClientSocketAvailableMiddleware[m]),
      );

      Logger.info('Adding route', prefix, key, uri, method, handlers);
      server[method.toLowerCase()](uri, handlers);
    }
  });
}

/* Generic controller for all API requests */
function genericController(controller, req, res) {
  const query = getRequestQuery(req);
  controller(query, req)
    .then(resp => {
      // disabled Ga tracking ofr API on march 2021
      // if (req.GA) {
      //   req.GA.send();
      // }

      if (!resp) {
        res.status(200).send({});
      } else if (!resp.response && !resp.error) {
        if (typeof resp === 'object') {
          res.set({'Content-type': 'application/json'});
        }
        res.send(resp);
      } else {
        if (resp.error) {
          return handleError(resp.error, req, res, resp.statusCode);
        }
        if (resp.cookie) {
          Object.keys(resp.cookie).forEach(key => {
            if (!resp.cookie[key]) {
              res.clearCookie(key);
            } else if (Array.isArray(resp.cookie[key])) {
              // if cookie has options
              res.cookie(key, resp.cookie[key][0], resp.cookie[key][1]);
            } else {
              res.cookie(key, resp.cookie[key]);
            }
          });
        }
        if (resp.headers) {
          res.set(resp.headers);
        }
        if (resp.attachment) {
          res.attachment(resp.attachment);
        }
        if (resp.statusCode) {
          res.status(resp.statusCode);
        }
        if (typeof resp.response === 'object') {
          res.set({'Content-type': 'application/json'});
        }
        res.send(resp.response);
      }
    })
    .catch(error => handleError(error, req, res));
}

/* Resolves middleware from string to the actual method
 * @param middleware {string or array}
 * @returns {array} of middleware functions
 */
function resolveMiddleware(middleware) {
  return Array.isArray(middleware) ? middleware.map(m => Middleware[m]) : [Middleware[middleware]];
}

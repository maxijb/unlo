import fetchConfig from 'zero-config';
import path from 'path';

export default () => {
  return fetchConfig(path.join(__dirname, '../../common'), {
    dcValue: 'dc',
    env: {NODE_ENV: process.env.NODE_ENV || 'development'},
    loose: false,
  });
};

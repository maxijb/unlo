import 'moment';
import 'moment-timezone';
import express from 'express';
import initApi from './init-api';
import BodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import fs from 'fs';
import fetchConfig from 'zero-config';
import path from 'path';
import sgMail from '@sendgrid/mail';
import {safeMiddleware} from '@Common/utils/requests';
import {default as Logger} from '@Common/logger';
import {CsrfTokenURL} from '@Common/constants/app';
import CommonMiddleware from '@Common/server/middleware/common-middleware';
import {initCDN} from '@Common/utils/statif-assets-utils';
import {initRedis} from './redis/redis';
import sitemap from './controllers/sitemap';
import {default as DB, Models} from './models';
import Middleware from './middleware';
import {getCsrfToken} from '@Common/server/security/auth';
import Scripts from './scripts';
import InitConfig from './init-config';

require('@Common/server/middleware/error-handlers');

import getRawBody from 'raw-body';

const i18n = require('i18next');
const i18nextMiddleware = require('i18next-express-middleware');
import {i18nBackendOptions} from '@Common/constants/i18n-config';
const Backend = require('i18next-node-fs-backend');

const dev = process.env.NODE_ENV !== 'production';
const server = express();

server.config = server.config = InitConfig();
initCDN(server.config.get('server'));
// ------------------------- EMAIL API --------------------------
sgMail.setApiKey(server.config.get('social').sendgrid.secret);

// ------------------------- SOCKETS --------------------------
const serverHandler = require('http').Server(server);
//initRedis(server);

i18n
  .use(Backend)
  .use(i18nextMiddleware.LanguageDetector)
  .init(i18nBackendOptions(server.config.get('server'), 'back'), () => {
    // ------------------------------- MYSQL -------------------------------
    DB(server)
      .sync({alter: false})
      .then(() => {
        server.use(function (err, req, res, next) {
          console.error(err.stack);
          res.status(500).send('Something broke!');
        });

        // enable middleware for i18next
        server.use(safeMiddleware(i18nextMiddleware.handle(i18n)));

        // ------------------------------- PARSERS AND AUTH -------------------------------

        //server.use(function (req, res, next) {
        //  getRawBody(
        //    req,
        //    {
        //      length: req.headers['content-length'],
        //      limit: '3mb',
        //    },
        //    function (err, reslt) {
        //      if (err) {
        //        return next(err)
        //      }
        //      next();
        //    }
        //  )
        //});
        // post body
        server.use(
          BodyParser.json({
            strict: false,
            limit: '3mb',
          }),
        );
        // parse cookies
        server.use(cookieParser());
        server.use(safeMiddleware(CommonMiddleware.addLogger));
        server.use((req, res, next) => {
          req.Models = Models();
          req.Config = server.config;
          next();
        });

        server.get('/sitemap.xml', sitemap);
        server.use('/api/public/*', safeMiddleware(Middleware.initPublicRequest));
        server.use(safeMiddleware(CommonMiddleware.preAuthorize));
        // check csrf only in prod
        // so we can test easily in dev
        if (server.config.get('server').useCsrf !== false) {
          server.use(/\/api\/((?!public|socket\.io).)*\/*/, safeMiddleware(Middleware.checkCsrf));
        }

        server.use(safeMiddleware(Middleware.readABTests));
        server.use(safeMiddleware(CommonMiddleware.initRequest));

        server.get(CsrfTokenURL, getCsrfToken);

        // ------------------------------- API -------------------------------
        try {
          initApi(server);
        } catch (e) {
          Logger.error(e);
        }

        // start the server
        const Port = server.config.get('server').ports.backend;
        serverHandler.listen(Port, err => {
          if (err) {
            throw err;
          }
          Logger.log(`> Ready on http://localhost:${Port}}`);

          // Running scripts
          const script = process.argv[process.argv.length - 1];
          if (Scripts.hasOwnProperty(script)) {
            Logger.log(`Initializing script ${script}...`);
            Scripts[script]();
          }
        });
      })
      .catch(e => {
        Logger.error(e);
      });
  });

import {
  ABCookieName,
  ABStatus,
  ABRefreshPeriod,
  ABForceHeaderName
} from '@Common/constants/ab-tests';
import {cookieParse, serializeCookie, reconcileCookie} from './ab-test-utils';

let abtests = null;
let abtestRefresh = 0;

export default {
  async readABTests(req, res, next) {
    try {
      // Check if we need to refresh the abtests
      const time = new Date().getTime();
      if (!abtests || abtestRefresh < time) {
        abtests = await req.Models.ABTest.getActiveMap();
        abtestRefresh = time + ABRefreshPeriod;
      }

      const cookieSource = req.headers[ABForceHeaderName] || req.cookies[ABCookieName];
      const cookieAB = cookieParse(cookieSource);
      const {newCookie, activeAB} = reconcileCookie(cookieAB, abtests);
      const serializedCookie = serializeCookie(newCookie);
      if (cookieAB !== serializedCookie) {
        res.cookie(ABCookieName, serializedCookie);
      }
      req.activeAB = activeAB;
      req.cookieAB = serializedCookie;
      next();
    } catch (e) {
      next(e);
    }
  }
};

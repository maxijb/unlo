import {ABStatus, ABSeparator, ABVariableSeparator} from '@Common/constants/ab-tests';

/* Parse a serialized cookie coming from the client
 * @param cookie {string}
 */
export function cookieParse(cookie) {
  if (!cookie) {
    return {};
  }
  return cookie.split(ABSeparator).reduce((prev, ab) => {
    const [name, variable] = ab.split(ABVariableSeparator);
    prev[name] = Number(variable);
    return prev;
  }, {});
}

/* Serializes ABdata to sent on a cookie
 * @param data {object}
    -> {
         {string}: value {integer}
       }
 */
export function serializeCookie(data) {
  return Object.keys(data || {})
    .map(name => {
      return `${name}${ABVariableSeparator}${data[name]}`;
    })
    .join(ABSeparator);
}

/* Reconciles the existing cookie with the curren AB testing settings
 * cookieAB {object}
 * abtests {object} stored in DB
 */
export function reconcileCookie(cookieAB = {}, abtests = {}) {
  const activeAB = {};
  const newCookie = {};

  Object.keys(abtests).forEach(name => {
    const db = abtests[name];
    if (db.status === ABStatus.fullOn) {
      activeAB[name] = Number(db.chosenVariable);
    } else {
      newCookie[name] = cookieAB[name] || getRandomPercentage();
      activeAB[name] = Number(getActiveVariable(newCookie[name], db));
    }
  });

  return {newCookie, activeAB};
}

/* Gets an ABVariable below max number
 * @param max {integer}
 */
export function getRandomPercentage() {
  return Math.floor(Math.random() * 100 + 1);
}

export function getActiveVariable(value, config) {
  if (!config.steps) {
    return Math.ceil(value / (100 / config.maxVariable));
  }
  const steps = config.steps;
  for (let i = 0, l = steps.length; i <= l; i++) {
    if (i === l || steps[i] >= value) {
      return i + 1;
    }
  }
}

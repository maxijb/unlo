import * as utils from './ab-test-utils';

describe('getActiveVariable', () => {
  it('simple comparison with default steps', () => {
    expect(utils.getActiveVariable(40, {maxVariable: 2})).toEqual(1);
  });

  it('comparison with 2 steps: last element', () => {
    expect(utils.getActiveVariable(40, {maxVariable: 2, steps: [5]})).toEqual(2);
  });

  it('comparison with 2 steps: first element', () => {
    expect(utils.getActiveVariable(4, {maxVariable: 2, steps: [5]})).toEqual(1);
  });

  it('comparison with 2 steps equal to value', () => {
    expect(utils.getActiveVariable(5, {maxVariable: 2, steps: [5]})).toEqual(1);
  });

  it('simple comparison with 3 steps, last element', () => {
    expect(utils.getActiveVariable(100, {maxVariable: 2, steps: [5, 60]})).toEqual(3);
  });

  it('simple comparison with 3 steps: middle element', () => {
    expect(utils.getActiveVariable(50, {maxVariable: 2, steps: [5, 60]})).toEqual(2);
  });
});

describe('getRandomPercentage', () => {
  it('gets a number between 1 and 100', () => {
    const num = utils.getRandomPercentage();
    expect(num >= 1 && num <= 100).toBeTruthy();
  });
});

describe('cookieParse', () => {
  it('parse empty cookie', () => {
    expect(utils.cookieParse()).toEqual({});
  });

  it('parse single ab in cookie', () => {
    expect(utils.cookieParse('maxi|2')).toEqual({maxi: 2});
  });

  it('parse double ab in cookie', () => {
    expect(utils.cookieParse('maxi|2/test|4')).toEqual({maxi: 2, test: 4});
  });
});

describe('serializeCookie', () => {
  it('serializeCookie undefined cookie', () => {
    expect(utils.serializeCookie()).toEqual('');
  });

  it('serializeCookie empty cookie', () => {
    expect(utils.serializeCookie({})).toEqual('');
  });

  it('serializeCookie single ab in cookie', () => {
    expect(utils.serializeCookie({maxi: 2})).toEqual('maxi|2');
  });

  it('serializeCookie double ab in cookie', () => {
    expect(utils.serializeCookie({maxi: 2, test: 4})).toEqual('maxi|2/test|4');
  });
});

describe('reconcileCookie', () => {
  const emptyCookie = {};
  const oneABInCookie = {test: 1};
  const twoABInCookie = {testFinished: 1};

  const dbSimple = {
    test: {maxVariable: 2}
  };

  const dbSimpleWithSteps = {
    test: {maxVariable: 2, steps: [101]}
  };

  const dbDoubleWithSteps = {
    test: {maxVariable: 2, steps: [101]},
    test2: {status: 'full-on', chosenVariable: 2}
  };

  it('case 1', () => {
    const {newCookie, activeAB} = utils.reconcileCookie(emptyCookie, dbSimple);
    expect(Object.keys(newCookie)).toEqual(['test']);
    expect(newCookie.test >= 1 && newCookie.test <= 100).toBeTruthy();
    expect(Object.keys(activeAB)).toEqual(['test']);
    expect(activeAB.test >= 1 && activeAB.test <= 2).toBeTruthy();
  });

  it('case 2', () => {
    const {newCookie, activeAB} = utils.reconcileCookie(emptyCookie, dbSimpleWithSteps);
    expect(Object.keys(newCookie)).toEqual(['test']);
    expect(newCookie.test >= 1 && newCookie.test <= 100).toBeTruthy();
    expect(Object.keys(activeAB)).toEqual(['test']);
    expect(activeAB.test >= 1 && activeAB.test <= 2).toBeTruthy();
  });

  it('case 3', () => {
    const {newCookie, activeAB} = utils.reconcileCookie(emptyCookie, dbDoubleWithSteps);
    expect(Object.keys(newCookie)).toEqual(['test']);
    expect(newCookie.test >= 1 && newCookie.test <= 100).toBeTruthy();
    expect(Object.keys(activeAB)).toEqual(['test', 'test2']);
    expect(activeAB.test >= 1 && activeAB.test <= 2).toBeTruthy();
    expect(activeAB.test2).toEqual(2);
  });

  it('case 4', () => {
    const {newCookie, activeAB} = utils.reconcileCookie(oneABInCookie, dbSimple);
    expect(Object.keys(newCookie)).toEqual(['test']);
    expect(newCookie.test >= 1 && newCookie.test <= 100).toBeTruthy();
    expect(Object.keys(activeAB)).toEqual(['test']);
    expect(activeAB.test >= 1 && activeAB.test <= 2).toBeTruthy();
  });

  it('case 5', () => {
    const {newCookie, activeAB} = utils.reconcileCookie(oneABInCookie, dbSimpleWithSteps);
    expect(Object.keys(newCookie)).toEqual(['test']);
    expect(newCookie.test >= 1 && newCookie.test <= 100).toBeTruthy();
    expect(Object.keys(activeAB)).toEqual(['test']);
    expect(activeAB.test >= 1 && activeAB.test <= 2).toBeTruthy();
  });

  it('case 6', () => {
    const {newCookie, activeAB} = utils.reconcileCookie(oneABInCookie, dbDoubleWithSteps);
    expect(Object.keys(newCookie)).toEqual(['test']);
    expect(newCookie.test >= 1 && newCookie.test <= 100).toBeTruthy();
    expect(Object.keys(activeAB)).toEqual(['test', 'test2']);
    expect(activeAB.test >= 1 && activeAB.test <= 2).toBeTruthy();
    expect(activeAB.test2).toEqual(2);
  });

  it('case 7', () => {
    const {newCookie, activeAB} = utils.reconcileCookie(twoABInCookie, dbSimple);
    expect(Object.keys(newCookie)).toEqual(['test']);
    expect(newCookie.test >= 1 && newCookie.test <= 100).toBeTruthy();
    expect(Object.keys(activeAB)).toEqual(['test']);
    expect(activeAB.test >= 1 && activeAB.test <= 2).toBeTruthy();
  });

  it('case 8', () => {
    const {newCookie, activeAB} = utils.reconcileCookie(twoABInCookie, dbSimpleWithSteps);
    expect(Object.keys(newCookie)).toEqual(['test']);
    expect(newCookie.test >= 1 && newCookie.test <= 100).toBeTruthy();
    expect(Object.keys(activeAB)).toEqual(['test']);
    expect(activeAB.test >= 1 && activeAB.test <= 2).toBeTruthy();
  });

  it('case 9', () => {
    const {newCookie, activeAB} = utils.reconcileCookie(twoABInCookie, dbDoubleWithSteps);
    expect(Object.keys(newCookie)).toEqual(['test']);
    expect(newCookie.test >= 1 && newCookie.test <= 100).toBeTruthy();
    expect(Object.keys(activeAB)).toEqual(['test', 'test2']);
    expect(activeAB.test >= 1 && activeAB.test <= 2).toBeTruthy();
    expect(activeAB.test2).toEqual(2);
  });
});

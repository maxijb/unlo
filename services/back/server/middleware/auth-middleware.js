/* This file is only included server-side
 * It's the main entry point for all middlewares defined for the API and pages
 */
import get from 'lodash.get';
import isEmpty from 'is-empty';
import {tokenCookieName, CsrfTokenHeader, sessionCookieName} from '@Common/constants/app';
import {
  AuthenticationError,
  AuthenticationErrorMessage,
  RequestError,
  RequestErrorMessage,
  formatError,
} from '@Common/constants/errors';
import {decode} from '@Common/server/security/tokens';
import {checkAuth} from '@Common/server/security/auth';

export default {
  authorize(req, res, next) {
    if (req.auth) {
      next();
    } else {
      return res.status(401).send(req.authError);
    }
  },

  async checkCsrf(req, res, next) {
    const secret = req.Config.get('server').session.secret;
    const userId = req.auth ? req.auth.id : null;
    const token = req.headers[CsrfTokenHeader];

    if (token === req.Config.get('server').session.whitelistedCsrfToken) {
      return next();
    }

    if (!token) {
      return res.status(403).send({error: 'csrf', message: 'missing_csrf_token'});
    }

    decode(token, secret)
      .then(({id, code}) => {
        if (id !== userId || !code) {
          return res.status(403).send({error: 'csrf', message: 'mismatch_csrf_token'});
        }
        next();
      })
      .catch(error => {
        return res.status(403).send({error: 'csrf', message: error.message});
        reject(err);
      });
  },

  async isSuperAdmin(req, res, next) {
    let isSuperAdmin = null;
    if (req.auth?.id) {
      isSuperAdmin = await req.Models.User.isSuperAdmin(req.auth.id);
      req.isSuperAdmin = isSuperAdmin;
    }
    if (!isSuperAdmin) {
      return res.status(403).send({error: 'unauthorized'});
    }
    next();
  },

  async isSellerOrSuperAdmin(req, res, next) {
    const {
      isSellerOrSuperAdmin,
      isSeller,
      seller_id,
      isSuperAdmin,
    } = await req.Models.User.isSellerOrSuperAdmin(req.auth.id);
    req.isSuperAdmin = isSuperAdmin;
    req.seller_id = seller_id;

    req.isSeller = isSeller;

    if (!isSellerOrSuperAdmin) {
      return res.status(404).send({error: 'not_found'});
    }
    next();
  },

  cronjob(req, res, next) {
    if (req.query.token === req.Config.get('server').cronjobToken) {
      next();
    } else {
      return res.status(404).send({error: 'not_found'});
    }
  },
};

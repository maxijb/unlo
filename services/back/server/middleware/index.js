/* This file is only included server-side
 * It's the main entry point for all middlewares defined for the API and pages
 */
import AuthMiddleware from './auth-middleware';
import CommonMiddleware from '@Common/server/middleware/common-middleware';
import ABMiddleware from './ab-test-middleware';
import PublicMiddleware from './public-middleware';
import UploadFileMiddleware from './upload-middleware';

export default {
  ...AuthMiddleware,
  ...ABMiddleware,
  ...PublicMiddleware,
  ...UploadFileMiddleware,
  ...CommonMiddleware
};

import {
  publicSessionCookieName,
  publicIdCookieName,
  sessionInactivityLimit
} from '@Common/constants/app';
import {createUow} from '@Common/utils/requests';

export default {
  initPublicRequest(req, res, next) {
    req.isPublic = true;
    next();
  },

  cors(req, res, next) {
    res.header('Access-Control-Allow-Origin', req.headers.origin || '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

    next();
  },

  async logSession(req, res, next) {
    //ignore pid on preflight requests
    if (req.method == 'OPTIONS') {
      return next();
    }

    if (!req.cookies[publicSessionCookieName]) {
      const session = await req.Models.Session.log(req);
      req.sessionId = session.get('id');
    } else {
      req.sessionId = req.cookies[publicSessionCookieName];
    }
    if (res) {
      res.cookie(publicSessionCookieName, req.sessionId, {maxAge: sessionInactivityLimit});
    }
    req.uow = createUow(req.sessionId, true);
    next();
  },

  /* Gets the target based on appId and campaignId
   * @param req {express request}
   * @cookie [publicIdCookieName] {integer}
   */
  async addPid(req, res, next) {
    //ignore pid on preflight requests
    if (req.method == 'OPTIONS') {
      return next();
    }
    let pid = Number(req.cookies[publicIdCookieName]);
    if (!pid) {
      req.isNewPid = true;
      const user = await req.Models.PublicClient.create({});
      pid = user.get('id');
      res.cookie(publicIdCookieName, pid);
    } else {
      req.isNewPid = false;
    }
    req.pid = pid;
    next();
  }
};

import multer from 'multer';
import path from 'path';
import sharp from 'sharp';
import fs from 'fs';
import {asyncForEach} from '@Common/utils/async';
import {StaticFilesFolder} from '@Common/constants/app';
import {processFile} from '../scripts/images-script';

const MaxSizes = {
  avatar: {
    resized: [
      160,
      160,
      {
        withoutEnlargement: true,
        fit: 'cover',
      },
    ],
  },
  logo: {
    big: [
      600,
      244,
      {
        withoutEnlargement: true,
        fit: 'inside',
      },
    ],
    resized: [
      160,
      160,
      {
        withoutEnlargement: true,
        fit: 'cover',
      },
    ],
  },
  // used for menu
  'module-side': {
    resized: [
      160,
      600,
      {
        withoutEnlargement: true,
        fit: 'inside',
      },
    ],
  },
  promotion: {
    resized: [
      712,
      1000,
      {
        withoutEnlargement: true,
        fit: 'inside',
      },
    ],
  },
  publicUpload: {
    resized: [
      1200,
      1200,
      {
        withoutEnlargement: true,
        fit: 'inside',
      },
    ],
  },
  chat: {
    resized: [
      800,
      800,
      {
        withoutEnlargement: true,
        fit: 'inside',
      },
    ],
  },
  default: {
    resized: [
      600,
      600,
      {
        withoutEnlargement: true,
        fit: 'inside',
      },
    ],
  },
};

const ShouldResize = {
  'application/pdf': false,
  'image/jpeg': 'jpeg',
  'image/jpg': 'jpeg',
  'image/raw': 'jpeg',
  'image/tiff': 'jpeg',
  'image/png': 'png',
  'image/webp': 'png',
  'image/gif': false,
  'image/svg': false,
};

const EncodeOptions = {
  jpeg: {quality: 50},
  webp: {quality: 80},
  png: {},
};

const UploadFileStorage = multer.diskStorage({
  destination: path.join(__dirname, `../../../../../${StaticFilesFolder}/`),
  filename: async (req, file, cb) => {
    //cb(null, `${Date.now()}${path.extname(file.originalname)}`);
    let finalName = file.originalname;
    let suffix = 0;
    let existingImg = null;
    do {
      existingImg = await req.Models.Image.findOne({where: {name: finalName}});
      if (existingImg) {
        finalName = `${path.parse(file.originalname).name}_${++suffix}${path.extname(finalName)}`;
      }
    } while (existingImg);
    cb(null, finalName);
  },
});

const UploadCSVFileStorage = multer.diskStorage({
  destination: path.join(__dirname, `../../../../../${StaticFilesFolder}/`),
  filename: async (req, file, cb) => {
    const parsed = path.parse(file.originalname);
    const finalName = `${parsed.name}_${Date.now()}_${req.auth.id}${path.extname(
      file.originalname,
    )}`;
    cb(null, finalName);
  },
});

const UploadFile = multer({
  storage: UploadFileStorage,
  limits: {
    fileSize: 1024 * 1024 * 10,
  },
});

const UploadCSVFile = multer({
  storage: UploadCSVFileStorage,
  limits: {
    fileSize: 1024 * 1024 * 30,
  },
});

export default {
  baseSingleUpload: (req, res, next) => {
    UploadFile.single('file')(req, res, async err => {
      if (err instanceof multer.MulterError) {
        return res.status(400).send(err.message);
      } else if (err) {
        return res.status(500).send('Upload failed');
      }

      const {filename: image} = req.file;
      const img_id = await processFile(image, req.Config);

      req.finalPath = image;
      req.finalImgId = img_id;

      next();
    });
  },

  csvSingleUpload: (req, res, next) => {
    UploadCSVFile.single('file')(req, res, async err => {
      if (err instanceof multer.MulterError) {
        return res.status(400).send(err.message);
      } else if (err) {
        return res.status(500).send('Upload failed');
      }
      next();
    });
  },

  uploadFile(req, res, next) {
    if (!req.file) {
      return res.send({error: 'upload_error'});
    }

    setTimeout(() => {
      res.send({filename: req.finalPath, size: req.file.size, img_id: req.finalImgId});
    }, 0);
  },
};

async function move(old, newFile) {
  return new Promise((resolve, reject) => {
    fs.rename(old, newFile, err => {
      err ? reject(err) : resolve();
    });
  });
}

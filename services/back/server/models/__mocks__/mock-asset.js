import {addModelHistory, dbMock} from './mock-utils';

const Interaction = dbMock.define('interaction', [
  {
    id: 1,
    type: 'app',
    name: 'testApp'
  },
  {
    id: 2,
    type: 'campaign',
    name: 'testCampaign'
  }
]);

Interaction.getConfiguration = jest.fn(target => {
  if (!target.appId && !target.campaignId) {
    throw new Error('missingTarget');
  }

  if (target.appId > 100) {
    throw new Error('targetNotFound');
  }
  return Promise.resolve({
    options: {
      defaultLang: 'es',
      supportedLanguages: ['es', 'en', 'fr']
    },
    messages: {rateTitle: 'test'},
    target: {appId: target.appId, campaignId: target.campaignId}
  });
});

export default addModelHistory(Interaction);

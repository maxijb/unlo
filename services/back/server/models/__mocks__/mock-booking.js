import {addModelHistory, dbMock} from './mock-utils';

const Booking = dbMock.define('booking', [
  {
    id: 1,
    startDate: new Date()
  }
]);

Booking.findForUserApp = jest.fn(target => {
  return Promise.resolve([]);
});

export default addModelHistory(Booking);

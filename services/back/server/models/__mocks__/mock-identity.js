import {addModelHistory, dbMock} from './mock-utils';

const Identity = dbMock.define('identity', [
  {id: 123, name: 'maxi', phone: '23873', email: 'maxi@test.com'}
]);

Identity.getObfuscatedIdentities = jest.fn(target => {
  return Promise.resolve({email: 'a@a.com', name: 'test name', id: 123, phone: 23873});
});

export default addModelHistory(Identity);

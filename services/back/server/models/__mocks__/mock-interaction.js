import {addModelHistory, dbMock} from './mock-utils';

const Interaction = dbMock.define('interaction', {
  id: 1,
  value: 3
});

export default addModelHistory(Interaction);

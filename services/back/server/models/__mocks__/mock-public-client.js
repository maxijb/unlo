import {addModelHistory, dbMock} from './mock-utils';

const PublicClient = dbMock.define('public-client', [{id: 123}, {id: 34}]);

PublicClient.getPublicUserProfile = jest.fn(target => {
  return Promise.resolve({email: 'a@a.com', name: 'test name'});
});

export default addModelHistory(PublicClient);

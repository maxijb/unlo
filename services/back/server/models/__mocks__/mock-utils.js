import SequelizeMock from 'sequelize-mock';
import Instance from 'sequelize-mock/src/instance';
export const dbMock = new SequelizeMock();

// store instance calls to update and destroy in Model spy history
// this is done extending the Instance prototype, so it needs to be done only once
for (const key of ['update', 'destroy']) {
  const cached = Instance.prototype[key];
  Instance.prototype[key] = function (...params) {
    this.Model.history('update', ...params, this.get());
    return cached.call(this, ...params);
  };
}

// add spy history to each model
export function addModelHistory(Model) {
  // init the spy on history
  Model.history = jest.fn();

  // mocks create to add history (create doesn't trigger $useHandler)
  const modelCreate = Model.create;
  Model.create = (...params) => {
    Model.history('create', ...params);
    return modelCreate.call(Model, ...params);
  };

  Model.$queryInterface.$useHandler(function (query, queryOptions, done) {
    Model.history(query, [...queryOptions]);
  });

  return Model;
}

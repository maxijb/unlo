/* eslint-disable camelcase */
import Seq from 'sequelize';
import {ABStatus} from '../../../common/constants/ab-tests';
import getRedis from '../redis/redis';

const REDIS_AB_KEY = 'wiri--redis-ab-key';

// Initialize model
export default function (sequelize) {
  const ABModel = sequelize.define(
    'ab-tests',
    {
      id: {
        type: Seq.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: Seq.STRING(30),
      },
      description: {
        type: Seq.TEXT,
      },
      status: {
        type: Seq.ENUM,
        values: [ABStatus.notStarted, ABStatus.running, ABStatus.closed, ABStatus.fullOn],
        index: true,
        defaultValue: ABStatus.uninitialized,
      },
      // Max number of variables being tested (1 based)
      maxVariable: {
        type: Seq.TINYINT,
      },
      // Version being run exclusively
      chosenVariable: {
        type: Seq.TINYINT,
      },
      // how many times it's run
      version: {
        type: Seq.TINYINT,
      },
      // separations between variables
      threshold: {
        type: Seq.STRING(20),
      },
    },
    {
      indexes: [{fields: ['status']}, {fields: ['name'], unique: true}],
    },
  );

  /* Gets an array of active AB Testings (running, fullOn) */
  ABModel.getActive = () => {
    return ABModel.findAll({
      where: {
        $or: [{status: ABStatus.running}, {status: ABStatus.fullOn}],
      },
      silent: true,
    });

    return response;
  };
  /* Gets a map of active AB Testings (running, fullOn)
   * Example:
   * {
   *   test_run: { status: 'running', maxVariable: 2, chosenVariable: null },
   *   'test-fullon': { status: 'full-on', maxVariable: 2, chosenVariable: 1 }
   * }
   */
  ABModel.getActiveMap = async () => {
    //const redis = getRedis();
    //const stored = await redis.getAsync(REDIS_AB_KEY);
    //
    //    //if (stored) {
    //  return JSON.parse(stored);
    //}

    const abs = await ABModel.getActive();
    const map = abs.reduce((prev, ab) => {
      const {name, status, maxVariable, chosenVariable, threshold} = ab.get();
      return {
        ...prev,
        [name]: {
          status,
          maxVariable,
          chosenVariable,
          steps: threshold ? threshold.split(',') : null,
        },
      };
    }, {});

    // redis.setAsync(REDIS_AB_KEY, JSON.stringify(map));

    return map;
  };

  return ABModel;
}
/* eslint-enable camelcase */

/* eslint-disable camelcase */
import Seq from 'sequelize';
import {Models} from './index';

// Initialize model
export default function (sequelize) {
  const AccessoryCategory = sequelize.define(
    'accessory-category',
    {
      category_id: {
        type: Seq.INTEGER,
      },
      accessory_to: {
        type: Seq.INTEGER,
      },
    },
    {
      indexes: [
        {
          unique: true,
          fields: ['category_id', 'accessory_to'],
        },
        {fields: ['accessory_to']},
      ],
    },
  );

  return AccessoryCategory;
}
/* eslint-enable camelcase */

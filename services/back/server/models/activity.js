/* eslint-disable camelcase */
import Seq from 'sequelize';
import {clientCookieName, ReservationDuration} from '@Common/constants/app';

// Initialize model
export default function (sequelize) {
  const Activity = sequelize.define(
    'activity',
    {
      id: {
        type: Seq.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },

      type: {
        type: Seq.STRING(25),
      },
      resolved: {
        type: Seq.BOOLEAN,
        defaultValue: 0,
      },
      subtype: {
        type: Seq.STRING(25),
      },
      cid: {
        type: Seq.STRING(30),
      },
      notes: {
        type: Seq.TEXT,
      },
      product_id: {
        type: Seq.INTEGER,
      },

      user_id: {
        type: Seq.INTEGER,
      },
      data: {
        type: Seq.TEXT,
      },
    },
    {
      indexes: [
        {fields: ['type']},
        {fields: ['subtype']},

        {fields: ['user_id']},
        {fields: ['cid']},

        {fields: ['product_id']},

        {fields: ['resolved']},
      ],
    },
  );

  return Activity;
}
/* eslint-enable camelcase */

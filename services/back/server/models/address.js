/* eslint-disable camelcase */
import Seq from 'sequelize';
import {clientCookieName} from '@Common/constants/app';

// Initialize model
export default function (sequelize) {
  const Address = sequelize.define(
    'address',
    {
      id: {
        type: Seq.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      street_number: {
        type: Seq.STRING(30),
      },
      route: {
        type: Seq.STRING(255),
      },
      locality: {
        type: Seq.STRING(255),
      },
      administrative_area_level_1: {
        type: Seq.STRING(255),
      },
      country: {
        type: Seq.STRING(255),
      },
      postal_code: {
        type: Seq.STRING(25),
      },
      unit: {
        type: Seq.STRING(25),
      },
      textName: {
        type: Seq.TEXT,
      },
      notes: {
        type: Seq.TEXT,
      },
      type: {
        type: Seq.STRING(10),
      },
      cid: {
        type: Seq.STRING(30),
      },
      user: {
        type: Seq.INTEGER,
      },
      primary: {
        type: Seq.STRING(255),
      },
    },
    {
      indexes: [{fields: ['primary']}, {fields: ['cid']}, {fields: ['user']}, {fields: ['type']}],
    },
  );

  Address.findForUser = async ({cid, user, type}) => {
    const where = !user ? {cid} : {[Seq.Op.or]: [{cid}, {user}]};
    if (type) {
      where.type = type;
    }
    return Address.findAll({where});
  };

  Address.upsertAddress = async ({cid, user, type, address}) => {
    const existingAddresses = await Address.findForUser({cid, user});
    for (const item of existingAddresses) {
      if (item.get('textName') === address.textName && item.get('unit') === address.unit) {
        return item;
      }
    }

    return Address.create({
      cid,
      user,
      type,
      primary: !existingAddresses.length,
      ...address,
    });
  };

  Address.getPrimaryAddressForUser = async id => {
    const address = Address.findOne({
      where: {
        primary: 1,
        user: id,
      },
      attributes: [
        'id',
        'textName',
        'unit',
        'postal_code',
        'locality',
        'administrative_area_level_1',
        'street_number',
        'route',
      ],
      raw: true,
    });
    return addShowName(address);
  };

  Address.syncUserSession = (id, req) => {
    return Address.update(
      {user: id},
      {
        where: {
          cid: req.cookies[clientCookieName],
          user: null,
        },
      },
    );
  };

  Address.getPublicFields = async id => {
    const address = await Address.findOne({
      where: {id},
      raw: true,
      attributes: [
        'id',
        'street_number',
        'route',
        'locality',
        'administrative_area_level_1',
        'country',
        'postal_code',
        'unit',
        'textName',
      ],
    });
    return addShowName(address);
  };

  Address.getPublicFieldsBatch = async id => {
    const addresses = await Address.findAll({
      where: {id},
      raw: true,
      attributes: [
        'id',
        'street_number',
        'route',
        'locality',
        'administrative_area_level_1',
        'country',
        'postal_code',
        'unit',
        'textName',
      ],
    });

    return addShowName(addresses);
  };

  Address.getPublicFieldsBatchForApi = async id => {
    return await Address.findAll({
      where: {id},
      raw: true,
      attributes: [
        'id',
        'street_number',
        'route',
        'locality',
        ['administrative_area_level_1', 'state'],
        'country',
        'postal_code',
        'unit',
      ],
    });
  };

  return Address;
}
/* eslint-enable camelcase */

function addShowName(address) {
  if (Array.isArray(address)) {
    return address.map(a => addShowName(a));
  }

  if (!address) {
    return null;
  }

  return {...address, showTextName: `${address.street_number} ${address.route}`};
}

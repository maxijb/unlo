/* eslint-disable camelcase */
import Seq from 'sequelize';
import {Models} from './index';

// Initialize model
export default function (sequelize) {
  const AttributeProduct = sequelize.define(
    'attribute-product',
    {
      attr_id: {
        type: Seq.INTEGER,
      },
      product_id: {
        type: Seq.INTEGER,
      },
    },
    {
      indexes: [
        {
          unique: true,
          fields: ['attr_id', 'product_id'],
        },
      ],
    },
  );

  AttributeProduct.findAllWithNameForVariantAndColors = async (ids, type) => {
    return sequelize.query(
      `SELECT product_id, attr_id, b.name, b.thumbnail FROM \`attribute-products\` a
        INNER JOIN attributes b ON a.attr_id = b.id AND b.type = 'color'
        WHERE product_id IN (:ids) ORDER BY product_id;
      `,
      {
        replacements: {ids},
        type: sequelize.QueryTypes.SELECT,
      },
    );
  };

  AttributeProduct.findAllWithNameForVariant = async (id, type = null) => {
    const tags = await AttributeProduct.findAll({
      where: {product_id: id},
      raw: true,
      attributes: ['attr_id'],
      order: ['product_id'],
    });

    const attrs = await Models().Attribute.getAttributes(
      tags.map(t => t.attr_id),
      type,
    );

    return attrs;
  };

  AttributeProduct.findAllWithNameForProduct = async id => {
    return sequelize.query(
      `SELECT id, name, type, thumbnail, description from attributes WHERE id IN (
  SELECT DISTINCT attr_id FROM products a LEFT JOIN \`attribute-products\` b ON a.id = b.product_id WHERE category_id = ? AND a.stock > 0 AND a.price > 0 AND a.visible = 1 AND a.active = 1
) AND VISIBLE = 1 ORDER BY type, id;`,
      {
        replacements: [id],
        type: sequelize.QueryTypes.SELECT,
      },
    );
  };

  AttributeProduct.findAllWithNameForVariantBatch = async id => {
    return sequelize.query(
      `SELECT a.id as id, product_id, attr_id, name, type, thumbnail  FROM \`attribute-products\` a
        JOIN attributes b ON a.attr_id = b.id
        WHERE a.product_id IN (:id)
        ORDER BY product_id`,
      {
        replacements: {id},
        type: sequelize.QueryTypes.SELECT,
      },
    );
  };

  AttributeProduct.findAllWithNameForProductByType = async id => {
    const attrs = await AttributeProduct.findAllWithNameForProduct(id);
    return attrs.reduce((agg, item) => {
      if (!agg.hasOwnProperty(item.type)) {
        agg[item.type] = [item];
      } else {
        agg[item.type].push(item);
      }

      return agg;
    }, {});
  };

  return AttributeProduct;
}
/* eslint-enable camelcase */

/* eslint-disable camelcase */
import Seq from 'sequelize';
import {UnlockedCarrierId} from '@Common/constants/app';

// Initialize model
export default function (sequelize) {
  const Attribute = sequelize.define(
    'attribute',
    {
      id: {
        type: Seq.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: Seq.STRING(100),
        index: true,
      },
      type: {
        type: Seq.STRING(15),
        index: true,
      },
      visible: {
        type: Seq.BOOLEAN,
        index: true,
      },
      thumbnail: {
        type: Seq.STRING(40),
      },
      description: {
        type: Seq.STRING(255),
      },
      slug: {
        type: Seq.STRING(8),
      },
    },
    {
      indexes: [
        {fields: ['type']},
        {fields: ['visible']},
        {fields: ['name']},
        {
          fields: ['slug'],
          unique: true,
        },
      ],
    },
  );

  Attribute.getAttributes = async (ids, type) => {
    const where = {
      id: ids,
      visible: 1,
    };
    if (type) {
      where.type = type;
    }

    return Attribute.findAll({
      where,
      attributes: ['id', 'name', 'type', 'thumbnail', 'description'],
      raw: true,
      order: [['id', 'ASC']],
    });
  };

  Attribute.getAllCarriers = async () => {
    return Attribute.findAll({
      where: {
        type: 'carrier',
        visible: 1,
        id: {
          [Seq.Op.ne]: UnlockedCarrierId,
        },
      },
      attributes: ['id', 'name', 'type', 'thumbnail', 'slug'],
      raw: true,
      order: [['id', 'ASC']],
    });
  };

  return Attribute;
}
/* eslint-enable camelcase */

/* eslint-disable camelcase */
import Seq from 'sequelize';
import {RequestError, RequestErrorMessage} from '@Common/constants/errors';

// Initialize model
export default function (sequelize) {
  const Billable = sequelize.define('billable', {
    id: {
      type: Seq.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    seller_id: {
      type: Seq.INTEGER,
      index: true,
    },
    order_id: {
      type: Seq.INTEGER,
      index: true,
    },
    gross: {
      type: Seq.FLOAT,
      defaultValue: 0,
    },
    commission: {
      type: Seq.FLOAT,
      defaultValue: 0,
    },
    balance: {
      type: Seq.FLOAT,
      defaultValue: 0,
    },
    credit: {
      type: Seq.FLOAT,
      defaultValue: 0,
    },
    debit: {
      type: Seq.FLOAT,
      defaultValue: 0,
    },
    direction: {
      type: Seq.STRING(7),
      defaultValue: 'credit',
      index: true,
    },
    type: {
      type: Seq.STRING(50),
      index: true,
    },
    description: {
      type: Seq.TEXT,
      index: true,
    },
    status: {
      type: Seq.INTEGER,
    },
  });

  Billable.originalCreate = Billable.create;

  Billable.create = () => {
    throw new Error(
      '`Billable.create` is a restricted method, as it does not generate balances. Use `Billable.add` instead',
    );
  };

  Billable.add = async input => {
    if (!input.seller_id) {
      throw new RequestError(RequestErrorMessage.missingUserId);
    }

    const existingBillable = await Billable.findOne({
      where: {
        type: input.type,
        order_id: input.order_id,
        seller_id: input.seller_id,
      },
    });

    if (existingBillable) {
      await existingBillable.set(input).save();
      return existingBillable;
    }

    const previous = await Billable.findOne({
      where: {
        seller_id: input.seller_id,
      },
      attributes: ['balance'],
      order: [['createdAt', 'DESC']],
    });
    const balance = previous?.balance || 0;

    const newBalance = balance + (Number(input.credit) || 0) - (Number(input.debit) || 0);

    return Billable.originalCreate({
      ...input,
      balance: newBalance,
    });
  };

  return Billable;
}
/* eslint-enable camelcase */

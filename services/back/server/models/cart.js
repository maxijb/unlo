/* eslint-disable camelcase */
import Seq from 'sequelize';
import {Models} from './index';
import {clientCookieName, CartStatus, DefaultFastShippingRate} from '@Common/constants/app';
import {arrayToMap, arrayToMultipleMap} from '@Common/utils/generic-utils';
import {DropshippingSellerID, DropshippingMargin} from '@Common/constants/app';
import {safeJsonParse} from '@Common/utils/json';
import {
  consolidateAttributesAsName,
  consolidateAttributes,
  getProductNameWithAttributesForOrder,
} from '../../../front/shared/utils/attributes-utils';
import {
  getCartDisplayProductId,
  getCartAllProductIds,
} from '../../../front/shared/utils/cart-utils';

// Initialize model
export default function (sequelize) {
  const Cart = sequelize.define(
    'cart',
    {
      id: {
        type: Seq.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      product_id: {
        type: Seq.INTEGER,
      },
      display_product_id: {
        type: Seq.INTEGER,
      },
      special_item: {
        type: Seq.STRING(150),
      },
      quantity: {
        type: Seq.INTEGER,
      },
      user: {
        type: Seq.INTEGER,
      },
      seller_id: {
        type: Seq.INTEGER,
      },
      order_id: {
        type: Seq.INTEGER,
      },
      return_order_id: {
        type: Seq.INTEGER,
      },
      cid: {
        type: Seq.STRING(60),
      },
      tracking: {
        type: Seq.STRING(30),
      },
      tracking_carrier: {
        type: Seq.STRING(10),
      },
      return_tracking_carrier: {
        type: Seq.STRING(10),
      },
      return_tracking: {
        type: Seq.STRING(30),
      },
      tracking_status: {
        type: Seq.INTEGER,
      },
      return_tracking_status: {
        type: Seq.INTEGER,
      },
      image: {
        type: Seq.STRING(255),
      },
      description: {
        type: Seq.STRING(255),
      },
      description_seller: {
        type: Seq.STRING(255),
      },
      type: {
        type: Seq.STRING(20),
        defaultValue: 'product',
      },
      price: {
        type: Seq.FLOAT,
      },
      display_price: {
        type: Seq.FLOAT,
      },
      status: {
        type: Seq.INTEGER,
        index: true,
      },
      source_id: {
        type: Seq.STRING(30),
        index: true,
      },
      remote_id: {
        type: Seq.STRING(60),
      },
      remote_source: {
        type: Seq.STRING(20),
      },
      remote_return_source: {
        type: Seq.STRING(20),
      },
      remote_return_id: {
        type: Seq.STRING(100),
      },
      remote_return_label: {
        type: Seq.STRING(255),
      },
      notified: {
        type: Seq.BOOLEAN,
        defaultValue: 0,
      },
    },
    {
      indexes: [
        {fields: ['status']},
        {fields: ['quantity']},
        {fields: ['order_id']},
        {fields: ['return_order_id']},
        {fields: ['seller_id']},
        {fields: ['type']},
        {fields: ['cid']},
        {fields: ['product_id']},
        {fields: ['user']},
        {fields: ['tracking_status']},
        {fields: ['return_tracking_status']},
        {fields: ['remote_id']},
        {fields: ['remote_source']},
        {fields: ['remote_return_source']},
        {fields: ['remote_return_id']},
        {fields: ['remote_return_label']},
        {fields: ['display_product_id']},
        {fields: ['display_price']},
        {fields: ['notified']},
      ],
    },
  );

  Cart.addForUser = async ({variant_id, quantity, selections, isFromApi = false}, req) => {
    const reference = await req.Models.Product.getVariantsWithDisplayInformation([variant_id]);
    const ref = reference[variant_id];
    const attrs = consolidateAttributesAsName(ref.attributes, req.t);

    let product_id = variant_id;
    let display_product_id = variant_id;
    let owner = ref.owner;
    let source_id = ref.source_id;

    // we check for attributes to ignore accessories
    if (ref.owner === DropshippingSellerID && ref.attributes.length && !isFromApi) {
      const alternatives = await req.Models.Product.findVariantForSelections({
        selections: consolidateAttributes(ref.attributes, 'attr_id'),
        id: ref.category_id,
        ignoreDropshipping: true,
      });
      if (alternatives.length) {
        if (alternatives[0].price <= ref.price * DropshippingMargin) {
          product_id = alternatives[0].id;
          owner = alternatives[0].owner;
          source_id = alternatives[0].source_id;
        }
      }
    }

    if (!isFromApi) {
      const existingCart = await Cart.getActiveForUser(req, false);
      for (const cart of existingCart) {
        if (
          cart.get('variant_id') === product_id &&
          cart.get('display_product_id') === display_product_id
        ) {
          cart.quantity = cart.get('quantity') + 1;
          await cart.save();
          return cart;
        }
      }
    }

    const obj = {
      product_id,
      display_product_id,
      image: ref.image,
      seller_id: owner || null,
      quantity,
      special_item: selections?.carrier
        ? JSON.stringify({overrideAttributes: {carrier: selections.carrier}})
        : '',
      status: CartStatus.active,
      cid: req.cookies[clientCookieName],
      user: req.auth?.id || null,
      description_seller: getProductNameWithAttributesForOrder(ref.name, attrs, req.t),
      description: getProductNameWithAttributesForOrder(ref.name, attrs, req.t),
      source_id,
    };

    if (selections?.carrier) {
      const newAttrs = await req.Models.Attribute.findOne({
        where: {id: selections.carrier},
        raw: true,
      });
      attrs.carrier = newAttrs?.name;
      obj.description = getProductNameWithAttributesForOrder(ref.name, attrs, req.t);
    }

    return Cart.create(obj);
  };

  Cart.getActiveForUser = async (req, isRaw = true, options) => {
    let where = req.auth?.id
      ? {
          [Seq.Op.or]: {
            cid: req.cookies[clientCookieName],
            user: req.auth.id,
          },
        }
      : {
          cid: req.cookies[clientCookieName],
        };

    if (options?.isApi && options?.carts_ids) {
      where = {id: options.carts_ids};
    }

    return await Cart.findAll({
      where: {
        status: CartStatus.active,
        ...where,
      },
      attributes: [
        ['product_id', 'variant_id'],
        'quantity',
        ['id', 'cart_id'],
        'id',
        'type',
        'price',
        'special_item',
        'seller_id',
        'display_price',
        'display_product_id',
        'description',
      ],
      order: ['id'],
      raw: isRaw,
    });
  };

  Cart.getActiveTradeInForUser = async (req, isRaw = true) => {
    const where = req.auth?.id
      ? {
          [Seq.Op.or]: {
            cid: req.cookies[clientCookieName],
            user: req.auth.id,
          },
        }
      : {
          cid: req.cookies[clientCookieName],
        };

    return await Cart.findAll({
      where: {
        status: CartStatus.tradeInActive,
        ...where,
      },
      attributes: [['id', 'cart_id'], 'id', 'type', 'price', 'special_item', 'status'],
      order: [['id', 'DESC']],
      raw: isRaw,
    });
  };

  /*
  Cart.readActiveFastShippingCharges = async (req, isRaw = true) => {
    const {cart, details} = await req.Models.Cart.readActiveForUser(req, true);
    const sellers = new Set();
    for (const key of Object.keys(details)) {
      sellers.add(details[key].seller);
    }

    const charges = await req.Models.User.findAll({
      where: {id: Array.from(sellers)},
      attributes: ['fast_shipping_rate'],
      raw: true,
    });

    let total = charges.reduce((agg, it) => {
      return agg + (it.fast_shipping_rate || DefaultFastShippingRate);
    }, 0);

    if (sellers.size > charges.length) {
      total += DefaultFastShippingRate * (sellers.size - charges.length);
    }

    return total;
  };
  */

  Cart.readActiveForUser = async (req, isRaw = true, options) => {
    let cart = await Cart.getActiveForUser(req, isRaw, options);
    if (!cart.length) {
      return {cart, details: [], shippingMethods: {}};
    }

    const details = await Models().Product.getVariantsWithDisplayInformation(
      getCartAllProductIds(cart),
    );

    ///////////////// Override carrier from selections
    if (isRaw) {
      for (const c of cart) {
        c.special_item = safeJsonParse(c.special_item);
        if (c.special_item?.overrideAttributes?.carrier) {
          const newCarrier = await req.Models.Attribute.findOne({
            where: {id: c.special_item?.overrideAttributes?.carrier},
            raw: true,
            attributes: ['name', ['id', 'attr_id'], 'type', 'thumbnail'],
          });
          const index =
            details[c.variant_id]?.attributes.findIndex(a => a.type === 'carrier') || -1;
          if (index != -1) {
            details[c.variant_id].attributes[index] = newCarrier;
          }
        }
      }
    }
    ////////////////////////

    const sellerIds = cart.map(it => (isRaw ? it.seller_id : it.get('seller_id'))).filter(s => !!s);
    const shippingMethodsList = await req.Models.ShippingMethod.getAll(sellerIds);

    return {
      cart,
      details,
      shippingMethodsBySeller: arrayToMultipleMap(shippingMethodsList, 'seller_id'),
    };
  };

  Cart.syncUserSession = async (id, req) => {
    await Promise.all([
      Cart.update(
        {user: id},
        {
          where: {
            cid: req.cookies[clientCookieName],
            user: null,
          },
        },
      ),
      req.Models.Address.syncUserSession(id, req),
      req.Models.Order.syncUserSession(id, req),
    ]);
  };

  return Cart;
}
/* eslint-enable camelcase */

/* eslint-disable camelcase */
import Seq from 'sequelize';

// Initialize model
export default function (sequelize) {
  const Category = sequelize.define(
    'category',
    {
      id: {
        type: Seq.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: Seq.STRING(100),
        index: true,
      },
      slug: {
        type: Seq.STRING(14),
        unique: true,
      },
      parent: {
        type: Seq.INTEGER,
        index: true,
      },
      isLeaf: {
        type: Seq.BOOLEAN,
        index: true,
      },
      visible: {
        type: Seq.BOOLEAN,
        index: true,
      },
      active: {
        type: Seq.BOOLEAN,
        index: true,
      },
      highlighted: {
        type: Seq.BOOLEAN,
        defaultValue: 0,
      },
      description: {
        type: Seq.STRING(100),
      },
      status: {
        type: Seq.INTEGER,
        index: true,
      },
      accessory_to: {
        type: Seq.INTEGER,
        index: true,
      },
      accessory_order: {
        type: Seq.INTEGER,
        index: true,
      },
      accessory_attribute: {
        type: Seq.STRING(20),
      },
      mpn: {
        type: Seq.STRING(15),
      },
      slug: {
        type: Seq.STRING(15),
      },
      shipping_length: {
        type: Seq.FLOAT,
      },
      shipping_width: {
        type: Seq.FLOAT,
      },
      shipping_height: {
        type: Seq.FLOAT,
      },
      shipping_weight: {
        type: Seq.FLOAT,
      },
    },
    {
      indexes: [
        {fields: ['status']},
        {fields: ['slug'], unique: true},
        {fields: ['active']},
        {fields: ['visible']},
        {fields: ['isLeaf']},
        {fields: ['parent']},
        {fields: ['highlighted']},
        {fields: ['name']},
        {fields: ['accessory_to']},
        {fields: ['accessory_order']},
      ],
    },
  );

  Category.getMenuItems = async () => {
    return Category.findAll({
      where: {
        isLeaf: 1,
        visible: 1,
        highlighted: 1,
      },
      attributes: ['id', 'name'],
      order: ['year', 'order'],
      raw: true,
    });
  };

  Category.findAllVisibleLeaves = async () => {
    return Category.findAll({
      where: {
        isLeaf: 1,
        visible: 1,
        active: 1,
      },
      attributes: ['id', 'name'],
      order: ['year', 'order'],
      raw: true,
    });
  };

  Category.findAllLeaves = async () => {
    return Category.findAll({
      where: {
        isLeaf: 1,
      },
      attributes: ['id', 'name'],
      order: ['year', 'order'],
      raw: true,
    });
  };

  Category.getHighlightedProducts = async () => {
    return Category.findAll({
      where: {
        isLeaf: 1,
      },
      attributes: ['id'],
      order: ['year', 'order'],
      raw: true,
    });
  };

  Category.findRelatedLeaves = async id => {
    return Category.findAll({
      where: {
        isLeaf: 1,
        id: {
          [Seq.Op.ne]: id,
        },
      },
      attributes: ['id', 'name'],
      order: ['year', 'order'],
      raw: true,
    });
  };

  Category.findLeaf = async id => {
    return Category.findOne({
      where: {id, isLeaf: true, active: 1, visible: 1},
      raw: true,
      attributes: ['id', 'name'],
    });
  };

  Category.findAllForLeaf = async id => {
    const cats = [];
    let current = id;

    do {
      const next = await Category.findOne({
        where: {id: current},
        raw: true,
        attributes: ['id', 'name', 'parent'],
      });

      if (next) {
        cats.push(next);
        current = next.parent || null;
      } else {
        current = null;
      }
    } while (current);

    return cats;
  };

  Category.getDeepChildCategories = async parent => {
    const result = new Set([parent]);
    let search = [parent];
    do {
      const children = await Category.findAll({
        where: {parent: search},
        attributes: ['id'],
        raw: true,
      });
      search = [];
      children.forEach(c => {
        result.add(c.id);
        search.push(c.id);
      });
    } while (search.length);
    return Array.from(result);
  };

  Category.getDeepParentCategories = async id => {
    const result = [];
    let search = id;
    do {
      const cat = await Category.findOne({
        where: {id: search},
        attributes: ['id', 'name', 'parent'],
        raw: true,
      });

      if (cat) {
        result.unshift(cat);
      }
      search = cat?.parent;
    } while (search);
    return result;
  };

  return Category;
}
/* eslint-enable camelcase */

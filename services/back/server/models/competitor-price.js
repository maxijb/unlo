/* eslint-disable camelcase */
import Seq from 'sequelize';

// Initialize model
export default function (sequelize) {
  const CompetitorPrice = sequelize.define(
    'competitor-price',
    {
      id: {
        type: Seq.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      category_id: {
        type: Seq.INTEGER,
      },
      attr_id: {
        type: Seq.INTEGER,
      },
      priority: {
        type: Seq.INTEGER,
      },
      source: {
        type: Seq.STRING(15),
      },
      price: {
        type: Seq.FLOAT,
      },
      primary: {
        type: Seq.BOOLEAN,
      },
      url: {
        type: Seq.TEXT,
      },
    },
    {
      indexes: [
        {fields: ['source']},
        {fields: ['category_id']},
        {fields: ['priority']},
        {fields: ['price']},
        {fields: ['attr_id']},
      ],
    },
  );

  CompetitorPrice.findAllForCategory = async (id, attrs) => {
    return CompetitorPrice.findAll({
      where: {category_id: id, attr_id: attrs},
      raw: true,
      attributes: ['category_id', 'source', 'price', 'url', 'attr_id'],
      order: [['priority', 'ASC']],
    });
  };

  return CompetitorPrice;
}
/* eslint-enable camelcase */

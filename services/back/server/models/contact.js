/* eslint-disable camelcase */
import Seq from 'sequelize';
import {obfuscateEmailForDisplay} from '@Common/utils/security-utils';
import {arrayToMultipleMap} from '@Common/utils/generic-utils';
import {DefaultFastShippingRate, CartStatus} from '@Common/constants/app';
import {RequestError, RequestErrorMessage} from '@Common/constants/errors';
import {Models} from './index';

// Initialize model
export default function (sequelize) {
  const Contact = sequelize.define(
    'contacts',
    {
      id: {
        type: Seq.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      email: {
        type: Seq.STRING(120),
        unique: true,
      },
      firstName: {
        type: Seq.STRING(40),
      },
      lastName: {
        type: Seq.STRING(40),
      },
      phone: {
        type: Seq.STRING(30),
      },
      cid: {
        type: Seq.STRING(30),
      },
    },
    {
      indexes: [
        {
          fields: ['email'],
          unique: true,
        },
        {
          fields: ['cid'],
        },
      ],
    },
  );

  Contact.add = async query => {
    let options = {ignoreDuplicates: true};
    if (query.firstName || query.lastName || query.phone) {
      options = {
        updateOnDuplicate: ['firstName', 'lastName', 'phone', 'cid'],
      };
    }
    return Contact.bulkCreate([query], options);
  };

  Contact.getAbandonedCarts = async () => {
    const contacts = await sequelize.query(
      `SELECT a.id, a.email, a.phone, a.firstName, a.cid, MAX(b.createdAt) as last_addition
        FROM contacts a
        JOIN carts b ON a.cid = b.cid
        WHERE b.status = 1 AND b.notified = 0 AND b.type = 'product'
        GROUP BY a.id
        HAVING last_addition < UTC_TIMESTAMP() - INTERVAL 15 MINUTE
        ORDER BY a.id;
    `,
      {
        type: sequelize.QueryTypes.SELECT,
      },
    );

    const byCid = arrayToMultipleMap(contacts, 'cid');

    const carts = await Models().Cart.findAll({
      where: {
        cid: Object.keys(byCid),
        status: CartStatus.active,
        type: 'product',
      },
      order: ['cid'],
      raw: true,
    });

    const cartsByCid = arrayToMultipleMap(carts, 'cid');

    return {contacts: byCid, carts: cartsByCid};
  };

  return Contact;
}
/* eslint-enable camelcase */

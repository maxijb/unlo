/* eslint-disable camelcase */
import Seq from 'sequelize';
import isEmpty from 'is-empty';
import {FieldsUserContact} from '@Common/constants/public-user';
import {CONTACT_ITEMS_PER_PAGE} from '@Common/constants/app';
import {obfuscateEmail, obfuscatePhone} from '@Common/utils/security-utils';
import {arrayToMap} from '@Common/utils/generic-utils';

// Initialize model
export default function (sequelize) {
  const Identity = sequelize.define('identity', {
    id: {
      type: Seq.BIGINT,
      autoIncrement: true,
      primaryKey: true
    },
    appId: {
      type: Seq.INTEGER,
      index: true,
      unique: 'appUserEmail'
    },
    publicId: {
      type: Seq.BIGINT,
      index: true,
      unique: 'appUserEmail'
    },
    email: {
      type: Seq.STRING(120),
      index: true,
      unique: 'appUserEmail'
    },
    name: {
      type: Seq.STRING(80),
      index: true
    },
    firstName: {
      type: Seq.STRING(40)
    },
    lastName: {
      type: Seq.STRING(40)
    },
    company: {
      type: Seq.STRING(30),
      index: true
    },
    phone: {
      type: Seq.STRING(30),
      index: true
    },
    validated: {
      type: Seq.BOOLEAN,
      defaultValue: false
    },
    password: {
      type: Seq.STRING(50),
      index: true
    },
    avatar: {
      type: Seq.STRING(255)
    },
    notes: {
      type: Seq.TEXT
    },
    status: {
      type: Seq.STRING(100)
    },
    createdBy: {
      type: Seq.STRING(40)
    },
    preferredContactMethod: {
      type: Seq.ENUM,
      values: ['email', 'phone', 'facebook', 'google'],
      defaultValue: 'email'
    },
    stripeId: {
      type: Seq.STRING(30)
    },
    consent: {
      type: Seq.BOOLEAN
    },
    extendedConsent: {
      type: Seq.BOOLEAN
    }
  });

  Identity.getIdentity = async ({identityId}) => {
    const identity = await Identity.findOne({
      where: {id: identityId},
      attributes: [...FieldsUserContact, 'publicId'],
      raw: true
    });
    // email is expected to be an array in the client
    identity.email = !identity.email ? [] : [identity.email];
    return identity;
  };

  Identity.countForApp = async ({appId, segment, search}) => {
    return sequelize.query(
      `SELECT COUNT(DISTINCT(publicId)) as count
        FROM identities
        ${segment ? 'LEFT JOIN `segment-customers` USING (publicId)' : ''}
        WHERE identities.appId = ?
        ${getSearchCondition(search)}
        ${segment ? 'AND segmentId = ' + segment : ''}
      `,
      {
        replacements: [appId],
        type: sequelize.QueryTypes.SELECT
      }
    );
  };

  Identity.listForApp = async ({
    limit = CONTACT_ITEMS_PER_PAGE,
    offset = 0,
    appId,
    segment,
    search
  }) => {
    const searchCondition = getSearchCondition(search);

    const identities = await sequelize.query(
      `SELECT
        a.publicId, a.id, a.name, a.phone, a.notes, GROUP_CONCAT(DISTINCT(a.email)) as email,
        GROUP_CONCAT(DISTINCT(segmentId)) as segments,
        MIN(c.timestamp) as firstVisit, MAX(c.timestamp) as lastVisit
        FROM identities a
        LEFT JOIN \`segment-customers\` USING (publicId, appId)
        LEFT JOIN sessions c USING (publicId)
        WHERE a.appId = ?
        ${searchCondition}
        GROUP BY(publicId)
        ${segment ? 'HAVING FIND_IN_SET(' + segment + ', segments)' : ''}
        LIMIT ?, ?`,
      {
        replacements: [appId, Number(offset) || 0, Number(limit) || CONTACT_ITEMS_PER_PAGE],
        type: sequelize.QueryTypes.SELECT
      }
    );

    identities.forEach(identity => {
      identity.email = identity.email.split(',');
      identity.segments = (identity.segments || '').split(',').filter(s => Boolean(s));
    });
    return identities;
  };

  /* List info for dayly chart */
  Identity.getChart = ({appId, start, end, offset}) => {
    return sequelize.query(
      `
      SELECT COUNT(*) as count, DATE(CONVERT_TZ(updatedAt, '+00:00', :offset)) as date
      FROM identities
      WHERE
        appId = :appId AND
        updatedAt >= :start AND
        updatedAt <= :end
      GROUP BY DATE(CONVERT_TZ(updatedAt, '+00:00', :offset))
      ORDER BY date
    `,
      {
        replacements: {
          appId,
          start,
          end,
          offset
        },
        type: sequelize.QueryTypes.SELECT
      }
    );
  };

  Identity.getObfuscatedIdentities = async ({identities}) => {
    if (isEmpty(identities)) {
      return {};
    }

    const data = await Identity.findAll({
      where: {id: identities},
      attributes: ['id', 'name', 'phone', 'email', 'consent', 'extendedConsent'],
      raw: true
    });

    return arrayToMap(
      data.map(item => ({
        ...item,
        phone: obfuscatePhone(item.phone),
        email: obfuscateEmail(item.email)
      }))
    );
  };

  Identity.countByDate = ({appId, start, end}) => {
    return Identity.count({
      where: {
        appId,
        createdAt: {[Seq.Op.between]: [start, end]}
      }
    });
  };

  Identity.search = async ({appId, txt}) => {
    if (!txt || txt.length < 3) {
      return [];
    }

    const contacts = await Identity.findAll({
      where: {
        appId,
        [Seq.Op.or]: [
          {name: {[Seq.Op.like]: `%${txt}%`}},
          {email: {[Seq.Op.like]: `%${txt}%`}},
          {phone: {[Seq.Op.like]: `%${txt}%`}}
        ]
      },
      attributes: ['publicId', 'name', 'email', 'phone', 'avatar'],
      raw: true
    });

    const seen = new Set();
    return contacts.filter(c => {
      if (seen.has(c.publicId)) {
        return false;
      }
      seen.add(c.publicId);
      return true;
    });
  };

  return Identity;
}
/* eslint-enable camelcase */

function getSearchCondition(search) {
  return search
    ? `AND (name LIKE '%${search}%' OR email LIKE '%${search}%' OR phone LIKE '%${search}%')`
    : '';
}

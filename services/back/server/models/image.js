/* eslint-disable camelcase */
import Seq from 'sequelize';

// Initialize model
export default function (sequelize) {
  const Image = sequelize.define(
    'image',
    {
      id: {
        type: Seq.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: Seq.STRING(255),
        index: true,
      },
      owner: {
        type: Seq.INTEGER,
        index: true,
      },
      type: {
        type: Seq.STRING(10),
        index: true,
      },
      visible: {
        type: Seq.BOOLEAN,
        index: true,
        defaultValue: 1,
      },
    },
    {
      indexes: [
        {
          unique: true,
          fields: ['name'],
        },
      ],
    },
  );

  Image.getMap = async (id, isFull) => {
    const images = await Image.findAll({
      where: {id, visible: 1},
      raw: true,
      attributes: !isFull ? ['id', 'name'] : null,
    });

    return images.reduce((agg, img) => {
      agg[img.id] = isFull ? img : img.name;
      return agg;
    }, {});
  };

  return Image;
}
/* eslint-enable camelcase */

import Sequelize from 'sequelize';
import User from './user';
import SocialConnection from './social-connection';
import ABTest from './ab-test';
import Payment from './payment';
import PublicClient from './public-client';
import IpLocation from './ip-location';
import Unsubscribed from './unsubscribed';
import Category from './category';
import Image from './image';
import Attribute from './attribute';
import AttributeProduct from './attribute-product';
import CategoryImage from './category-image';
import Product from './product';
import Cart from './cart';
import Order from './order';
import Spec from './spec';
import ShippingMethod from './shipping-method';
import Seller from './seller';
import SellerConnection from './seller-connection';
import SellerWebhook from './seller-webhook';
import Address from './address';
import CompetitorPrice from './competitor-price';
import AccessoryCategory from './accessory-category';
import Activity from './activity';
import Contact from './contact';
import PromoCode from './promo-code';
import Billable from './billable';
import UrlAlias from './url-alias';
import PeopleWatching from './people-watching';

// Add all models to be loaded here
const models = {
  PromoCode,
  AccessoryCategory,
  Category,
  ABTest,
  Activity,
  Billable,
  Payment,
  PublicClient,
  SocialConnection,
  User,
  IpLocation,
  Unsubscribed,
  CategoryImage,
  Image,
  Product,
  AttributeProduct,
  Attribute,
  Address,
  Cart,
  Spec,
  Seller,
  ShippingMethod,
  Order,
  CompetitorPrice,
  PeopleWatching,
  SellerConnection,
  SellerWebhook,
  Contact,
  UrlAlias,
};

let sequelize = null;

export default function getSequelize(server) {
  if (!sequelize) {
    const config = server.config.get('mysql');

    sequelize = new Sequelize(config.databaseName, config.user, config.password, {
      dialect: 'mysql',
      host: config.databaseHost,
      replication: {
        read: {
          port: config.databasePorts.slave,
        },
        write: {
          port: config.databasePorts.master,
        },
      },
    });

    // Load each model
    Object.keys(models).forEach(model => {
      sequelize.models[model] = models[model](sequelize);
    });
    sequelize.models.sequelize = sequelize;

    // DEFINE ASSOCIATIONS HERE
    // sequelize.models.Rate.hasMany(sequelize.models.Tag);
  }

  return sequelize;
}

export const Models = () => getSequelize().models;

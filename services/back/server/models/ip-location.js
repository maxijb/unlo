/* eslint-disable camelcase */
import Seq from 'sequelize';

// Initialize model
export default function (sequelize) {
  const IpLocation = sequelize.define(
    'ip2location_db3',
    {
      ip_from: {
        type: Seq.INTEGER
      },
      ip_to: {
        type: Seq.INTEGER
      },
      country_code: {
        type: Seq.STRING(2)
      },
      country_name: {
        type: Seq.STRING(64)
      },
      region_name: {
        type: Seq.STRING(128)
      },
      city_name: {
        type: Seq.STRING(128)
      }
    },
    {freezeTableName: true, timestamps: false}
  );

  IpLocation.findLocation = async ip => {
    if (!ip) {
      return Promise.resolve({});
    }

    return IpLocation.findOne({
      where: {
        ip_to: {[Seq.Op.gte]: ip}
      },
      attributes: ['country_code', 'country_name', 'region_name', 'city_name'],
      raw: true
    });
  };

  return IpLocation;
}
/* eslint-enable camelcase */

//SELECT * FROM ip2location_db3 WHERE ip_to >= 1169507940 LIMIT 1;

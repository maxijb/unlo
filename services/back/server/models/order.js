/* eslint-disable camelcase */
import {
  ProductSourceNames,
  AccessoriesCategories,
  BillableTypes,
  CommissionRate,
} from '@Common/constants/app';
import EmailControllers from '../controllers/email-controllers';
import CartControllers from '../controllers/cart-controllers';

import {getClient as getWHClient} from '../services/webhooks';

import Seq from 'sequelize';
import {Models} from './index';
import {
  clientCookieName,
  CartStatus,
  OrderStatus,
  OrderType,
  DefaultPerPage,
  OrderVisibleStatus,
} from '@Common/constants/app';
import {ValidStatusTransitions} from '@Common/constants/seller-constants';
import {safeJsonParse} from '@Common/utils/json';
import {arrayToMap, arrayToMultipleMap} from '@Common/utils/generic-utils';
import {
  canBeCancelled,
  canBeReturned,
  normalizeTrackingCarrier,
  getActualOrderTotals,
  getTotalsForBillable,
} from '@Common/utils/order-utils';
import {
  getCartDisplayProductId,
  getCartAllProductIds,
} from '../../../front/shared/utils/cart-utils';

// Initialize model
export default function (sequelize) {
  const Order = sequelize.define(
    'order',
    {
      id: {
        type: Seq.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      subtotal: {
        type: Seq.FLOAT,
      },
      subtotal_aggregates: {
        type: Seq.FLOAT,
      },
      tax_rate: {
        type: Seq.FLOAT,
      },
      tax: {
        type: Seq.FLOAT,
      },
      total: {
        type: Seq.FLOAT,
      },
      user_id: {
        type: Seq.INTEGER,
      },
      address_id: {
        type: Seq.INTEGER,
      },
      shipping_address_id: {
        type: Seq.INTEGER,
      },
      cid: {
        type: Seq.STRING(40),
      },
      email: {
        type: Seq.STRING(120),
      },
      firstName: {
        type: Seq.STRING(40),
      },
      lastName: {
        type: Seq.STRING(40),
      },
      type: {
        type: Seq.STRING(15),
      },
      payment_type: {
        type: Seq.STRING(15),
      },
      stripe_payment_id: {
        type: Seq.STRING(40),
      },
      paypal_payment_id: {
        type: Seq.STRING(40),
      },
      payment_description: {
        type: Seq.STRING(40),
      },
      parent_id: {
        type: Seq.INTEGER,
      },
      phone: {
        type: Seq.STRING(30),
      },
      notes: {
        type: Seq.TEXT,
      },
      status: {
        type: Seq.INTEGER,
        index: true,
      },
      retailer_id: {
        type: Seq.INTEGER,
        defaultValue: 0,
      },
      retailer_order_id: {
        type: Seq.INTEGER,
      },
    },
    {
      indexes: [
        {fields: ['retailer_order_id']},
        {fields: ['retailer_id']},
        {fields: ['status']},
        {fields: ['phone']},
        {fields: ['firstName']},
        {fields: ['lastName']},
        {fields: ['email']},
        {fields: ['user_id']},
        {fields: ['cid']},
        {fields: ['createdAt']},
        {fields: ['updatedAt']},
        {fields: ['parent_id']},
        {fields: ['type']},
        {fields: ['stripe_payment_id']},
        {fields: ['paypal_payment_id']},
      ],
    },
  );

  Order.listOrdersForUser = async req => {
    if (!req.auth?.id) {
      throw new RequestError(RequestErrorMessage.missingUserId);
    }

    return req.Models.Order.findAll({
      where: {
        user_id: req.auth.id,
        status: {
          [Seq.Op.ne]: OrderStatus.pending,
        },
        [Seq.Op.or]: [
          {type: null},
          {
            type: {
              [Seq.Op.ne]: OrderType.return,
            },
          },
        ],
      },
      order: [['id', 'DESC']],
      raw: true,
    });
  };

  Order.findMyLastOrder = async (req, raw = true) => {
    const where = mineWhere(req);
    return Order.findOne({where, order: [['createdAt', 'DESC']], raw});
  };

  Order.isOrderMine = async (id, req, raw = true) => {
    const where = mineWhere(req, true);
    where.id = id;

    return Order.findOne({where, raw});
  };

  Order.syncUserSession = (id, req) => {
    return Order.update(
      {user_id: id},
      {
        where: {
          cid: req.cookies[clientCookieName],
          user_id: null,
        },
      },
    );
  };

  Order.getReturnOrderDetails = async (id, seller_id) => {
    const order = await Order.findOne({where: {id}, raw: true});
    const where = {
      return_order_id: order.id,
    };
    if (seller_id) {
      where.seller_id = seller_id;
    }
    const products = order ? await Models().Cart.findAll({where}) : [];
    const parent = order ? await Order.findOne({where: {id: order.parent_id}, raw: true}) : null;
    return {order, products, parent};
  };

  Order.getReturnOrderExtendedDetails = async ({
    order,
    returnOrder,
    modifiedProducts,
    seller_id,
    parentOrder,
  }) => {
    const relevantProducts = seller_id
      ? modifiedProducts.filter(p => p.seller_id === seller_id)
      : modifiedProducts;

    const relevantProductsIds = new Set(relevantProducts.map(p => p.id));

    const allProducts = await Models().Cart.findAll({
      where: {
        seller_id,
        order_id: parentOrder.id,
      },
      raw: true,
    });

    const remainingProducts = [];
    allProducts.forEach(p => {
      if (!relevantProductsIds.has(p.id) && (canBeCancelled(p) || canBeReturned(p))) {
        remainingProducts.push(p);
      }
    });

    const nothingRemaining = !remainingProducts.length;
    const onlyShippingRemaining =
      nothingRemaining || remainingProducts.every(p => p.type !== 'product');

    const shouldEntireOrderBeCancelled =
      onlyShippingRemaining &&
      allProducts.every(
        p =>
          p.type !== 'product' ||
          p.status === CartStatus.cancelledByUser ||
          p.status === CartStatus.cancelledByPlatform ||
          p.status === CartStatus.cancelledBySeller,
      );

    const relevantProductsToReturn = relevantProducts.filter(
      p => p.status === CartStatus.requestedReturn,
    );
    const relevantProductsToCancel = relevantProducts.filter(
      p =>
        p.status === CartStatus.cancelledByUser ||
        p.status === CartStatus.cancelledByPlatform ||
        p.status === CartStatus.cancelledBySeller,
    );

    return {
      relevantProducts,
      remainingProducts,
      relevantProductsToReturn,
      relevantProductsToCancel,
      seller_id,
      nothingRemaining,
      onlyShippingRemaining,
      shouldEntireOrderBeCancelled,
      allProducts,
    };
  };

  Order.getOrderDetails = async (id, seller_id) => {
    const order = await Order.findOne({where: {id, status: OrderStatus.payed}, raw: true});

    if (!order) {
      return null;
    }

    const productWhere = {
      order_id: order.id,
    };
    if (seller_id) {
      productWhere.seller_id = seller_id;
    }
    const products = order ? await Models().Cart.findAll({where: productWhere, raw: true}) : [];

    if (!products.length) {
      return null;
    }

    const productDetails = await Models().Product.findAll({
      where: {id: getCartAllProductIds(products)},
      attributes: ['id', 'category_id'],
      raw: true,
    });

    const productMap = arrayToMap(productDetails);

    // Add category_id to each product and find promocode
    let promoCode = null;
    products.forEach(p => {
      if (p.type === 'product') {
        p.category_id = productMap[getCartDisplayProductId(p)]?.category_id || null;
      } else if (p.type === 'code') {
        promoCode = p;
      }
      p.special_item = safeJsonParse(p.special_item);
    });

    const tradeInOrder = await Models().Order.findOne({where: {parent_id: order.id}, raw: true});
    const tradeInProducts = tradeInOrder
      ? await Models().Cart.findAll({where: {order_id: tradeInOrder.id}, raw: true})
      : [];

    const address = !order?.address_id
      ? null
      : await Models().Address.getPublicFields(order.address_id);

    tradeInProducts.forEach(t => {
      t.selections = safeJsonParse(t.special_item);
    });

    let shipping = null;
    if (seller_id) {
      const {subtotal, tax, total} = getActualOrderTotals({
        cart: products,
        tax_rate: order.tax_rate,
      });

      order.subtotal = subtotal;
      order.tax = tax;
      order.total = total;

      let {shippingMethod} = await Models().Order.getShippingMethodForSellerFromCart(
        products,
        seller_id,
      );
      shipping = shippingMethod;
    } else {
    }

    return {
      order,
      promoCode,
      products,
      address,
      tradeInOrder,
      tradeInProducts,
      shipping,
    };
  };

  Order.list = async ({
    ids = null,
    isSeller,
    visibleStatus = '',
    isApi = false,
    offset = 0,
    limit = DefaultPerPage,
    seller_id = null,
    forceIncludeCount = false,
    start_date = null,
    end_date = null,
    search = '',
    retailer_id = null,
    retailer_order_ids = null,
  }) => {
    const orders = await sequelize.query(
      `SELECT ${
        !isSeller
          ? 'o.*'
          : 'o.status, o.id, o.createdAt, o.phone, o.firstName, o.lastName, o.notes, o.parent_id, o.tax_rate, o.type, o.user_id, o.address_id, o.shipping_address_id'
      }
        FROM orders as o
        INNER JOIN carts ON carts.order_id = o.id
        WHERE carts.type = 'product' AND o.status != -1 ${
          visibleStatus ? ` AND carts.status IN (:status) ` : ''
        } ${ids ? ` AND o.id IN (:ids) ` : ''}
        ${retailer_order_ids?.length ? ` AND o.retailer_order_id IN (:retailer_order_ids) ` : ''}
        ${retailer_id ? ` AND o.retailer_id = :retailer_id ` : ''}
        ${seller_id ? `AND carts.seller_id = :seller_id` : ''} ${
        start_date ? ' AND o.createdAt >= :start_date' : ''
      }
      ${end_date ? ' AND o.createdAt <= :end_date' : ''}
      ${
        !search
          ? ''
          : 'AND (o.firstName LIKE :search OR o.lastName LIKE :search OR o.phone LIKE :search OR o.email LIKE :search)'
      }
        AND o.type IS NULL AND o.status = 1
        GROUP BY o.id
        ORDER BY carts.order_id DESC LIMIT :offset, :limit
      `,
      {
        replacements: {
          status: visibleStatus,
          ids,
          retailer_id,
          retailer_order_ids,
          offset: Number(offset),
          limit: Number(limit),
          seller_id,
          start_date,
          end_date,
          search: `%${search}%`,
        },
        type: sequelize.QueryTypes.SELECT,
      },
    );

    const response = {orders};

    if (Number(offset) === 0 || forceIncludeCount) {
      const totalCount = await sequelize.query(
        `SELECT COUNT(DISTINCT(orders.id)) as count FROM orders
          INNER JOIN carts ON carts.order_id = orders.id
          WHERE carts.type = 'product' AND orders.status != -1 ${
            ids ? ` AND orders.id IN (:ids) ` : ''
          }${visibleStatus ? ` AND carts.status IN (:status) ` : ''} ${
          seller_id ? `AND carts.seller_id = :seller_id` : ''
        } ${start_date ? ' AND o.createdAt >= :start_date' : ''}
      ${end_date ? ' AND o.createdAt <= :end_date' : ''}
      `,
        {
          replacements: {
            status: visibleStatus,
            offset: Number(offset),
            limit: Number(limit),
            ids,
            seller_id,
            start_date,
            end_date,
          },
          type: sequelize.QueryTypes.SELECT,
        },
      );
      response.totalCount = totalCount[0].count;
    }

    if (!response.orders?.length) {
      return {...response, sellers: {}};
    }

    if (!isSeller) {
      const cids = orders.map(o => o.cid).filter(c => !!c);
      response.cids;

      const sources = await sequelize.query(
        `
        SELECT subtype, cid FROM activities a WHERE id IN (
          SELECT MIN(id) as min_id FROM activities b WHERE type = 'landing' AND b.cid IN (:cids) GROUP BY b.cid
        );
      `,
        {
          replacements: {
            cids,
          },
          type: sequelize.QueryTypes.SELECT,
        },
      );

      response.sources = sources.reduce((agg, s) => {
        agg[s.cid] = s.subtype;
        return agg;
      }, {});
    }

    const whereCart = {order_id: orders.map(o => o.id)};
    if (seller_id) {
      whereCart.seller_id = seller_id;
    }
    const [carts, addresses] = await Promise.all([
      Models().Cart.findAll({
        where: whereCart,
        attributes: isApi
          ? [
              'id',
              'product_id',
              'tracking',
              'tracking_carrier',
              'quantity',
              'price',
              'order_id',
              'status',
              'seller_id',
              'source_id',
              'type',
              'special_item',
              ['description_seller', 'description'],
            ]
          : null,
        raw: true,
      }),
      Models().Address[isApi ? 'getPublicFieldsBatchForApi' : 'getPublicFieldsBatch'](
        orders.map(o => o.address_id),
      ),
    ]);

    carts.forEach(c => {
      c.special_item = safeJsonParse(c.special_item);
    });

    const sellers = isSeller
      ? null
      : await Models().Seller.findAll({
          where: {
            id: carts.map(c => c.seller_id),
          },
        });

    response.sellers = sellers ? arrayToMap(sellers) : null;

    const productIds = carts.map(c => c.product_id).filter(w => !!w);
    const productsMap =
      productIds.length && !isApi
        ? await Models().Product.getVariantsWithDisplayInformation(productIds)
        : {};

    const addressMap = arrayToMap(addresses);
    const cartsMap = arrayToMultipleMap(
      carts,
      'order_id',
      e => e,
      item => {
        return {...productsMap[item.product_id], ...item};
      },
    );
    response.orders.forEach(o => {
      o.cart = cartsMap[o.id] || [];
      o.address = addressMap[o.address_id];
    });

    return response;
  };

  Order.getShippingMethodForSellerFromOrderId = async (order_id, seller_id) => {
    const cart = await Models().Cart.findAll({
      where: {
        order_id,
        seller_id,
      },
      raw: true,
    });

    return Models().Order.getShippingMethodForSellerFromCart(cart, seller_id);
  };

  Order.getShippingMethodForSellerFromCart = async (cart, seller_id) => {
    const shippingCart = cart.find(c => c.type === 'shipping');

    let shippingMethod = null;
    if (shippingCart) {
      shippingMethod = await Models().ShippingMethod.get(shippingCart.product_id, seller_id);
    } else {
      const sellerMethods = await Models().ShippingMethod.getAll(seller_id, false);
      if (sellerMethods?.length) {
        shippingMethod = sellerMethods[0];
      }
    }

    return {shippingCart, shippingMethod};
  };

  Order.addTracking = async ({
    order_id,
    seller_id,
    cart_id,
    req,
    query,
    remote_id,
    remote_source,
  }) => {
    if (!order_id) {
      throw 'order_id is required';
    }

    const where = {
      order_id,
      status: CartStatus.inOrder,
    };

    if (order_id) {
      where.order_id = order_id;
    }

    if (seller_id) {
      where.seller_id = seller_id;
    }
    if (cart_id) {
      where.id = cart_id;
    }
    if (remote_id) {
      where.remote_id = remote_id;
      where.remote_source = remote_source;
    }

    if (!query.tracking) {
      throw 'Tracking is required';
    }

    let tracking_carrier = normalizeTrackingCarrier(query.tracking_carrier);

    if (!tracking_carrier && order_id && seller_id) {
      const {shippingMethod} = await Models().Order.getShippingMethodForSellerFromOrderId(
        order_id,
        seller_id,
      );

      tracking_carrier = shippingMethod.tracking_carrier;
    }

    const newData = {
      tracking_carrier,
      tracking: query.tracking,
      status: CartStatus.inTransit,
    };

    const carts = await Models().Cart.findAll({
      where,
      raw: true,
      attributes: ['id', 'price', 'seller_id', 'type', 'status'],
    });

    if (!carts.length) {
      return {ok: false, newData: {}, cart_ids: [], order_id};
    }

    const {cart_ids, cart_products, cart_total} = getTotalsForBillable(carts);

    await Promise.all(
      Object.keys(cart_total).map(cart_seller_id => {
        if (!cart_seller_id) {
          return null;
        }
        if (seller_id && cart_seller_id != seller_id) {
          return null;
        }

        return req.Models.Billable.add({
          type: BillableTypes.order,
          gross: cart_total[cart_seller_id],
          commission: cart_products[cart_seller_id] * CommissionRate,
          credit: cart_total[cart_seller_id] - cart_products[cart_seller_id] * CommissionRate,
          order_id: order_id,
          seller_id: cart_seller_id,
        });
      }),
    );

    await Order.updateCartStatus(carts, newData.status, req, newData);

    try {
      await EmailControllers.sendOrderTrackingEmail(
        {
          id: order_id,
          seller_id: carts[0].seller_id,
        },
        req,
      );
    } catch (e) {
      req.Logger.error('tracking-email-error', e);
    }

    return {ok: true, newData, cart_ids, order_id};
  };

  // force = true -> should only be allowed for superadmins
  Order.updateCartStatus = async (carts, newStatus, req, newData = {}, force = false) => {
    if (!carts.length) {
      return;
    }
    const sellers = new Set();
    const cart_ids = [];
    const promises = await Promise.all(
      carts.map(cart => {
        if (ValidStatusTransitions[cart.status]?.includes(newStatus) || force) {
          sellers.add(cart.seller_id);
          cart_ids.push(cart.id);
          return Models().Cart.update({status: newStatus, ...newData}, {where: {id: cart.id}});
        }
        return null;
      }),
    );

    const order_id = carts[0].order_id;

    if (
      newStatus === CartStatus.cancelledBySeller ||
      newStatus === CartStatus.cancelledByUser ||
      newStatus === CartStatus.cancelledByPlatform
    ) {
      const order = await req.Models.Order.findOne({where: {id: order_id}, raw: true});
      const returnOrder = await req.Models.Order.create({
        type: 'return',
        parent_id: order_id,
        cid: order.cid,
        user_id: order.user_id,
      });
      await req.Models.Cart.update(
        {
          return_order_id: returnOrder.get('id'),
        },
        {where: {id: cart_ids}},
      );
      const products = await req.Models.Cart.findAll({where: {id: cart_ids}});
      try {
        CartControllers.returnOrderHooks(
          {
            products,
            returnOrder: {
              ...returnOrder.get(),
              reason: null,
              explanation: null,
            },
          },
          req,
        );
      } catch (e) {
        req.Logger.error('return-order-hooks-error', e);
      }
    } else {
      for (const seller_id of sellers) {
        try {
          if (newStatus === CartStatus.returnedToSenderSeller) {
            Order.refundBasedOnStatus({order_id, seller_id, req, newStatus});
          }

          if (newStatus === CartStatus.returnLabelSent) {
            EmailControllers.sendReturnLabel({order_id, seller_id, newStatus, cart_ids}, req);
          }
        } catch (e) {
          Logger.error('udpate-cart-status-error', e);
        }

        const WH = await getWHClient(req, seller_id);
        await WH.postOrderUpdate({order_id: order_id});
      }
    }

    return cart_ids;
  };

  Order.refundBasedOnStatus = async ({order_id, seller_id, req, newStatus}) => {
    const carts = await req.Models.Cart.findAll({where: {order_id, seller_id}, raw: true});
    const isEntireOrder = carts.every(c => c.type !== 'product' || c.status === newStatus);
    // if not entire order we'll filter only returned items
    const fn = isEntireOrder ? undefined : c => c.status === newStatus;
    const {cart_ids, cart_products, cart_total} = getTotalsForBillable(carts, fn);

    return req.Models.Billable.add({
      type: BillableTypes.returnedToSender,
      debit: isEntireOrder
        ? cart_total[seller_id] - cart_products[seller_id] * CommissionRate
        : cart_products[seller_id] * (1 - CommissionRate),
      order_id: order_id,
      seller_id: seller_id,
    });
  };

  return Order;
}
/* eslint-enable camelcase */

const mineWhere = (req, includeTest = false) => {
  const condition = req.auth?.id
    ? {
        [Seq.Op.or]: {
          cid: req.cookies[clientCookieName],
          user_id: req.auth.id,
        },
      }
    : {
        [Seq.Op.or]: {
          cid: req.cookies[clientCookieName],
        },
      };

  if (includeTest) {
    const whitelistedConfirmation = req.Config.get('server').whitelistedConfirmation;
    condition[Seq.Op.or].id = whitelistedConfirmation;
  }
  return condition;
};

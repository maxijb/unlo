/* eslint-disable camelcase */
import Seq from 'sequelize';

// Initialize model
export default function (sequelize) {
  return sequelize.define('payments', {
    id: {
      type: Seq.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    userId: {
      type: Seq.INTEGER,
      index: true
    },
    amount: {
      type: Seq.INTEGER
    },
    description: {
      type: Seq.STRING(100),
      index: true
    },
    token: {
      type: Seq.STRING(40)
    },
    status: {
      type: Seq.INTEGER
    }
  });
}
/* eslint-enable camelcase */

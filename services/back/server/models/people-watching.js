/* eslint-disable camelcase */
import Seq from 'sequelize';
import isEmpty from 'is-empty';

// Initialize model
export default function (sequelize) {
  const PeopleWatching = sequelize.define(
    'people-watching',
    {
      id: {
        type: Seq.BIGINT,
        autoIncrement: true,
        primaryKey: true,
      },
      category_id: {
        type: Seq.INTEGER,
      },
      number: {
        type: Seq.INTEGER,
      },
    },
    {
      indexes: [{fields: ['category_id']}, {fields: ['createdAt']}],
    },
  );

  PeopleWatching.getForCategory = async id => {
    let last = await PeopleWatching.findOne({
      where: {
        category_id: id,
        createdAt: {
          [Seq.Op.gte]: Seq.literal('DATE_SUB(NOW(), INTERVAL 30 MINUTE) '),
        },
      },
      order: [['createdAt', 'DESC']],
    });

    if (!last) {
      last = await PeopleWatching.create({category_id: id, number: Math.round(Math.random() * 30)});
    }

    return last.get('number');
  };

  return PeopleWatching;
}
/* eslint-enable camelcase */

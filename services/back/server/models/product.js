/* eslint-disable camelcase */
import Seq from 'sequelize';
import {Models} from './index';
import {
  UnlockedCarrierId,
  FallBackUnlocked,
  DefaultFastShippingRate,
  DropshippingSellerID,
  DropshippingMargin,
} from '@Common/constants/app';
import {arrayToMap} from '@Common/utils/generic-utils';

const PublicAttributes = [
  'id',
  'description',
  'owner',
  'price',
  'original_price',
  'stock',
  'rating',
  'facebook_id',
];

// Initialize model
export default function (sequelize) {
  const Product = sequelize.define(
    'product',
    {
      id: {
        type: Seq.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      description: {
        type: Seq.TEXT,
      },
      owner: {
        type: Seq.INTEGER,
        index: true,
      },
      category_id: {
        type: Seq.INTEGER,
        index: true,
      },
      visible: {
        type: Seq.BOOLEAN,
        index: true,
        defaultValue: 1,
      },
      source_id: {
        type: Seq.STRING(30),
        index: true,
      },
      source_name: {
        type: Seq.STRING(20),
        index: true,
      },
      active: {
        type: Seq.BOOLEAN,
        index: true,
        defaultValue: 1,
      },
      price: {
        type: Seq.FLOAT,
        index: true,
      },
      original_price: {
        type: Seq.FLOAT,
        index: true,
      },
      rating: {
        type: Seq.FLOAT,
      },
      stock: {
        type: Seq.INTEGER,
      },
      incredible_id: {
        type: Seq.STRING(150),
      },
      facebook_id: {
        type: Seq.STRING(20),
      },
    },
    {
      indexes: [
        {
          fields: ['stock'],
        },
        {
          fields: ['incredible_id'],
        },
        {
          fields: ['owner', 'source_id'],
          unique: true,
        },
        {
          fields: ['source_id'],
        },
        {
          fields: ['source_name'],
        },
        {
          fields: ['price'],
        },
        {
          fields: ['active'],
        },
        {
          fields: ['rating'],
        },
        {
          fields: ['category_id'],
        },
        {
          fields: ['owner'],
        },
        {
          fields: ['visible'],
        },
      ],
    },
  );

  Product.findCheapestAvailableVersionBatch = async ids => {
    return sequelize.query(
      `
      SELECT id, MIN(price) as price, original_price, stock, category_id, owner, rating FROM (
        select * from products WHERE category_id IN (:ids) AND active = 1 AND visible = 1 AND stock > 0 AND price > 0
        ORDER BY price
      ) as t
      group by category_id;
    `,
      {
        replacements: {ids},
        type: sequelize.QueryTypes.SELECT,
      },
    );
  };

  Product.findCheapestAvailableVersionBatchWithAllColors = async ids => {
    const [products, colors] = await Promise.all([
      Product.findCheapestAvailableVersionBatchWithColorAttributeOnly(ids),
      Product.findAllColorsForCategories(ids),
    ]);

    const aggregated = colors.reduce((agg, it) => {
      if (!agg.has(it.category_id)) {
        agg.set(it.category_id, []);
      }
      agg.get(it.category_id).push(it);
      return agg;
    }, new Map());

    products.forEach(it => {
      it.allColors = aggregated.get(it.category_id) || [];
    });

    return products;
  };

  Product.findCheapestAvailableVersionBatchWithColorAttributeOnly = async ids => {
    return sequelize.query(
      `
      SELECT products.id, f.name, products.owner, products.category_id, products.price, products.original_price, c.attr_id FROM products JOIN (

        SELECT MIN(p.id) as id, p.category_id FROM products p JOIN (
          SELECT category_id, MIN(price) as min_price FROM products pp
          JOIN \`attribute-products\` aa ON pp.id = aa.product_id AND aa.attr_id = :unlocked
          WHERE active = 1 AND stock > 0 AND price > 0 AND category_id IN (:ids) GROUP BY category_id
        ) a ON p.price = a.min_price AND a.category_id = p.category_id
        JOIN \`attribute-products\` b ON p.id = b.product_id AND b.attr_id = :unlocked
        GROUP BY p.category_id
      ) variant ON products.id = variant.id
      LEFT JOIN (
        SELECT aa.attr_id, name, product_id, thumbnail FROM \`attribute-products\` aa JOIN attributes bb ON aa.attr_id = bb.id AND bb.type = 'color'
      ) c ON products.id = c.product_id
      LEFT JOIN categories f ON products.category_id = f.id
      ORDER BY f.highlighted DESC, f.year, f.order
    `,
      {
        replacements: {ids, unlocked: UnlockedCarrierId},
        type: sequelize.QueryTypes.SELECT,
      },
    );
  };

  Product.findCheapestAvailableVersionBatchWithColor = async ids => {
    return sequelize.query(
      `
      SELECT p.id, f.name, p.owner, p.category_id, p.price, p.original_price, c.attr_id, c.name as color, c.thumbnail, d.img_id, d.order, e.name as image FROM products p JOIN (
        SELECT category_id, MIN(price) min_price FROM products WHERE active = 1 AND stock > 0 AND category_id IN (:ids) AND price > 0 GROUP BY category_id
      ) AS m ON m.min_price = p.price AND m.category_id = p.category_id
      LEFT JOIN (
        SELECT aa.attr_id, name, product_id, thumbnail FROM \`attribute-products\` aa JOIN attributes bb ON aa.attr_id = bb.id AND bb.type = 'color'
      ) c ON p.id = c.product_id
      LEFT JOIN (

          SELECT cc.img_id, cc.order, cc.category_id, cc.attr1 FROM \`category-images\` AS cc JOIN (
              SELECT attr1, category_id, MIN(\`order\`) min_order FROM \`category-images\` GROUP BY category_id, attr1
        ) AS dd ON cc.order = dd.min_order AND cc.category_id = dd.category_id AND cc.attr1 = dd.attr1

      ) as d ON d.category_id = p.category_id AND d.attr1 = c.attr_id
      LEFT JOIN images e ON d.img_id = e.id
      LEFT JOIN categories f ON p.category_id = f.id
    `,
      {
        replacements: {ids},
        type: sequelize.QueryTypes.SELECT,
      },
    );
  };

  /* Used in scripts/generate-incredible-ids.js */
  Product.getAttrsById = () => {
    return sequelize.query(
      'SELECT a.id, category_id, GROUP_CONCAT(b.attr_id) as attrs FROM products AS a LEFT JOIN `attribute-products` AS b ON a.id = b.product_id GROUP BY a.id',
      {type: sequelize.QueryTypes.SELECT},
    );
  };

  Product.getAccessoryProductsWithNoAttributeType = async ({ids, seller_id}) => {
    const results = await sequelize.query(
      `
      SELECT p.category_id, p.id as product_id, e.name as image, p.price, p.owner, d.img_id FROM products p JOIN (
        SELECT MIN(price) as min_price, category_id FROM products WHERE category_id IN (:ids) AND active = 1 AND stock > 0 AND price > 0 AND owner = :seller_id
        GROUP BY category_id
      ) AS a ON a.min_price = p.price AND a.category_id = p.category_id AND p.active = 1 AND p.stock > 0 AND p.price > 0 AND p.owner = :seller_id
      LEFT JOIN (
          SELECT cc.img_id, cc.order, cc.category_id FROM \`category-images\` AS cc JOIN (
              SELECT category_id, MIN(\`order\`) min_order FROM \`category-images\` GROUP BY category_id
        ) AS dd ON cc.order = dd.min_order AND cc.category_id = dd.category_id
      ) as d ON d.category_id = a.category_id
      LEFT JOIN images e ON d.img_id = e.id;
    `,
      {
        replacements: {ids, seller_id: seller_id || 9},
        type: sequelize.QueryTypes.SELECT,
      },
    );

    if (seller_id) {
      const remainingIds = new Set(ids);
      for (const res of results) {
        remainingIds.delete(res.category_id);
      }

      if (remainingIds.size) {
        const anySellerResults = await Product.getAccessoryProductsWithNoAttributeType({
          ids: Array.from(remainingIds),
        });

        return results.concat(anySellerResults);
      }
    }

    return results;
  };
  Product.getAccessoryProductsWithAttributeType = async ({ids, attributeType, seller_id}) => {
    const results = await sequelize.query(
      `
       SELECT a.category_id, a.attr_id as id, a.name, a.thumbnail, ppp.owner, d.img_id, e.name as image, min_price as price, ppp.product_id, ppp.attr_id FROM (
          SELECT category_id, b.attr_id, c.name, c.thumbnail, MIN(p.price) as min_price
          FROM (SELECT * FROM products WHERE category_id IN (:ids) AND stock > 0 AND price > 0 AND active = 1 ${
            seller_id ? 'AND owner = :seller_id' : ''
          }) AS p
          LEFT JOIN \`attribute-products\` b ON p.id = b.product_id
          JOIN attributes c ON b.attr_id = c.id AND c.type = :attributeType
          GROUP BY category_id, b.attr_id
          ORDER BY category_id
      ) AS a
      JOIN (
          SELECT cc.img_id, cc.order, cc.category_id, cc.attr1 FROM \`category-images\` AS cc JOIN (
              SELECT attr1, category_id, MIN(\`order\`) min_order FROM \`category-images\` GROUP BY category_id, attr1
        ) AS dd ON cc.order = dd.min_order AND cc.category_id = dd.category_id AND cc.attr1 = dd.attr1
      ) as d ON d.category_id = a.category_id AND d.attr1 = a.attr_id
      JOIN images e ON d.img_id = e.id
      JOIN (
         SELECT pp.owner, pp.id as product_id, category_id, price, attr_id FROM products as pp JOIN \`attribute-products\` bb ON pp.id = bb.product_id
      ) as ppp ON a.category_id = ppp.category_id AND a.min_price = ppp.price AND a.attr_id = ppp.attr_id AND a.stock > 0 AND a.price > 0 AND a.active = 1;
    `,
      {
        replacements: {ids, attributeType, seller_id: Number(seller_id)},
        type: sequelize.QueryTypes.SELECT,
      },
    );

    if (seller_id && !results?.length) {
      return Product.getAccessoryProductsWithAttributeType({ids, attributeType});
    }

    return results;
  };

  Product.getVariantsWithDisplayInformation = async (ids, includeBuyBox = false) => {
    const variantPromise = sequelize.query(
      `
      SELECT p.id, f.name, p.owner, p.source_id, p.incredible_id, p.category_id, p.price, p.stock, p.original_price, p.owner as seller, c.attr_id, c.name as color, c.thumbnail, d.img_id, d.order, e.name as image, g.fast_shipping_rate
      ${!includeBuyBox ? '' : `, p.incredible_id, h.min_price`}
      FROM (
        SELECT * FROM products WHERE id IN (:ids)
      ) AS p
      LEFT JOIN (
        SELECT aa.attr_id, name, product_id, thumbnail FROM \`attribute-products\` aa JOIN attributes bb ON aa.attr_id = bb.id AND bb.type = 'color'
      ) c ON p.id = c.product_id
      LEFT JOIN (

          SELECT cc.img_id, cc.order, cc.category_id, cc.attr1 FROM \`category-images\` AS cc JOIN (
              SELECT attr1, category_id, MIN(\`order\`) min_order FROM \`category-images\` GROUP BY category_id, attr1
        ) AS dd ON cc.order = dd.min_order AND cc.category_id = dd.category_id AND (cc.attr1 = dd.attr1 OR dd.attr1 IS NULL)

      ) as d ON d.category_id = p.category_id AND (d.attr1 = c.attr_id || d.attr1 IS NULL)
      LEFT JOIN images e ON d.img_id = e.id
      LEFT JOIN categories f ON p.category_id = f.id
      LEFT JOIN users g ON p.owner = g.id
      ${
        !includeBuyBox
          ? ''
          : `LEFT JOIN (
            SELECT MIN(price) as min_price, incredible_id FROM products WHERE stock > 0 AND price > 0 GROUP BY incredible_id
          ) h ON p.incredible_id = h.incredible_id
          `
      }
    `,
      {
        replacements: {ids},
        type: sequelize.QueryTypes.SELECT,
      },
    );

    const tagsPromise = Models().AttributeProduct.findAllWithNameForVariantBatch(ids);
    const [variants, tags] = await Promise.all([variantPromise, tagsPromise]);

    const details = variants.reduce((agg, v) => {
      agg[v.id] = v;
      agg[v.id].attributes = [];
      if (v.fast_shipping_rate === null) {
        v.fast_shipping_rate = DefaultFastShippingRate;
      }
      return agg;
    }, {});

    tags.forEach(t => {
      details[t.product_id]?.attributes?.push(t);
    });

    return details;
  };

  Product.findCheapestAvailableVersionBatchWithColor = async ids => {
    return sequelize.query(
      `
      SELECT p.id, f.name, p.owner, p.category_id, p.price, p.original_price, c.attr_id, c.name as color, c.thumbnail, d.img_id, d.order, e.name as image FROM products p JOIN (
        SELECT category_id, MIN(price) min_price FROM products WHERE active = 1 AND stock > 0 AND price > 0 AND category_id IN (:ids) GROUP BY category_id
      ) AS m ON m.min_price = p.price AND m.category_id = p.category_id
      LEFT JOIN (
        SELECT aa.attr_id, name, product_id, thumbnail FROM \`attribute-products\` aa JOIN attributes bb ON aa.attr_id = bb.id AND bb.type = 'color'
      ) c ON p.id = c.product_id
      LEFT JOIN (

          SELECT cc.img_id, cc.order, cc.category_id, cc.attr1 FROM \`category-images\` AS cc JOIN (
              SELECT attr1, category_id, MIN(\`order\`) min_order FROM \`category-images\` GROUP BY category_id, attr1
        ) AS dd ON cc.order = dd.min_order AND cc.category_id = dd.category_id AND cc.attr1 = dd.attr1

      ) as d ON d.category_id = p.category_id AND d.attr1 = c.attr_id
      LEFT JOIN images e ON d.img_id = e.id
      LEFT JOIN categories f ON p.category_id = f.id
    `,
      {
        replacements: {ids},
        type: sequelize.QueryTypes.SELECT,
      },
    );
  };

  Product.findAllColorsForCategories = async ids => {
    return sequelize.query(
      `
      SELECT a.category_id, a.attr_id as id, a.name, a.thumbnail, d.img_id, e.name as image FROM (
          SELECT category_id, b.attr_id, c.name, c.thumbnail
          FROM (SELECT * FROM products WHERE category_id IN (:ids)) AS p
          JOIN \`attribute-products\` b ON p.id = b.product_id
          JOIN attributes c ON b.attr_id = c.id AND c.type = 'color'
          GROUP BY category_id, b.attr_id
          ORDER BY category_id
      ) AS a
      JOIN (
          SELECT cc.img_id, cc.order, cc.category_id, cc.attr1 FROM \`category-images\` AS cc JOIN (
              SELECT attr1, category_id, MIN(\`order\`) min_order FROM \`category-images\` GROUP BY category_id, attr1
        ) AS dd ON cc.order = dd.min_order AND cc.category_id = dd.category_id AND cc.attr1 = dd.attr1
      ) as d ON d.category_id = a.category_id AND d.attr1 = a.attr_id
      JOIN images e ON d.img_id = e.id
    `,
      {
        replacements: {ids},
        type: sequelize.QueryTypes.SELECT,
      },
    );
  };

  Product.findAndCountAllWithAttibutes = async ({
    limit,
    offset,
    owner,
    source_id,
    attributes,
    categoryIds,
    buyBoxFilter,
    stockFilter,
  }) => {
    const rows = await sequelize.query(
      `
         SELECT id FROM (
          SELECT id, incredible_id, price FROM products WHERE ${
            categoryIds?.length ? 'category_id IN (:categoryIds)' : '1'
          } ${owner ? 'AND owner = :owner' : ''}
          ${source_id ? 'AND source_id LIKE :source_id' : ''}
          ${stockFilter === 'withStock' ? 'AND stock > 0' : ''}
          ${stockFilter === 'withoutStock' ? 'AND stock = 0' : ''}
        ) AS p
        ${
          !attributes?.length
            ? ''
            : `
            JOIN (
              SELECT product_id FROM \`attribute-products\` WHERE attr_id IN (:attributes)
              GROUP BY product_id
              HAVING COUNT(DISTINCT attr_id) >= :size
            ) AS a ON p.id = a.product_id
          `
        }

        ${
          !buyBoxFilter
            ? ''
            : `
            JOIN (
              SELECT MIN(price) as min_price, incredible_id FROM products WHERE stock > 0 AND price > 0 GROUP BY incredible_id
            ) h ON p.incredible_id = h.incredible_id AND p.price ${
              buyBoxFilter === 'in-buybox' ? '=' : '!='
            } h.min_price
          `
        }

        ORDER BY id
        LIMIT :offset, :limit
      `,
      {
        replacements: {
          limit,
          offset,
          owner,
          source_id: `%${source_id}%`,
          attributes,
          categoryIds,
          size: attributes.length,
        },
        type: sequelize.QueryTypes.SELECT,
      },
    );

    const countQuery = await sequelize.query(
      `
         SELECT COUNT(*) as count FROM (
          SELECT id, price, incredible_id FROM products WHERE ${
            categoryIds?.length ? 'category_id IN (:categoryIds)' : '1'
          } ${owner ? 'AND owner = :owner' : ''}
          ${source_id ? 'AND source_id LIKE :source_id' : ''}
        ) AS p
          ${
            !attributes?.length
              ? ''
              : `
            JOIN (
              SELECT product_id FROM \`attribute-products\` WHERE attr_id IN (:attributes)
              GROUP BY product_id
              HAVING COUNT(DISTINCT attr_id) >= :size
            ) AS a ON p.id = a.product_id
          `
          }

        ${
          !buyBoxFilter
            ? ''
            : `
            JOIN (
              SELECT MIN(price) as min_price, incredible_id FROM products WHERE stock > 0 AND price > 0 GROUP BY incredible_id
            ) h ON p.incredible_id = h.incredible_id AND p.price ${
              buyBoxFilter === 'in-buybox' ? '=' : '!='
            } h.min_price
          `
        }
      `,
      {
        replacements: {
          limit,
          offset,
          owner,
          source_id,
          attributes,
          categoryIds,
          size: attributes.length,
        },
        type: sequelize.QueryTypes.SELECT,
      },
    );

    return {
      rows,
      count: countQuery[0]?.count,
    };
  };

  Product.upsertProduct = async (accessory, owner, source_name, source_id = null, infinite) => {
    if (accessory.id) {
      const product = await Product.findOne({where: {id: accessory.id, owner}});
      product.price = accessory.price;
      product.category_id = accessory.category_id;
      product.source_name = source_name;
      product.source_id = source_id;
      await product.save();
      return product.get('id');
    }

    const product = await Product.create({
      ...accessory,
      stock: infinite ? 1000000 : accessory.stock || 0,
      owner,
      source_name,
      source_id,
    });
    return product.get('id');
  };

  Product.findCheapestAvailableVersion = async (id, onlyUnlocked = false) => {
    if (onlyUnlocked) {
      const result = await sequelize.query(
        `
          SELECT p.id, p.price, p.original_price, p.stock, p.owner, p.rating FROM (
            SELECT * FROM products WHERE stock > 0 AND price > 0 AND active = 1 and category_id = ?
          ) AS p
          JOIN \`attribute-products\` a ON p.id = a.product_id AND a.attr_id = ? ORDER BY price LIMIT 1;
        `,
        {
          replacements: [id, UnlockedCarrierId],
          type: sequelize.QueryTypes.SELECT,
        },
      );
      return result[0];
    }

    return Product.findOne({
      where: {
        category_id: id,
        active: 1,
        stock: {[Seq.Op.gt]: 0},
      },
      order: [['price', 'ASC']],
      raw: true,
      attributes: PublicAttributes,
    });
  };

  Product.getVersionWithAttributes = async id => {
    const variant = await Product.findOne({
      where: {id},
      attributes: PublicAttributes,
      raw: true,
    });
    if (!variant) {
      return null;
    }

    const tags = await Models().AttributeProduct.findAllWithNameForVariant(variant.id);

    return {
      variant,
      tags,
    };
  };

  Product.findCheapestAvailableVersionWithAttributes = async (id, onlyUnlocked = false) => {
    const variant = await Product.findCheapestAvailableVersion(id, onlyUnlocked);
    if (!variant) {
      return null;
    }

    const tags = await Models().AttributeProduct.findAllWithNameForVariant(variant.id);

    return {
      variant,
      tags,
    };
  };

  Product.getMinimumPricePerCarrier = async (id, selections = {}) => {
    // We'll find the cheapest appearance so we'll not include as a requirement in the calculation
    const {carrier, appearance, ...other} = selections;
    const attrs = Object.values(other);

    return sequelize.query(
      `SELECT aa.*, bb.name FROM (
        SELECT MIN(p.price) as min_price, b.attr_id FROM (
          SELECT price, id FROM products WHERE category_id = :id AND stock > 0 AND price > 0 AND active = 1
        ) AS p
          JOIN (
          SELECT product_id FROM \`attribute-products\` WHERE attr_id IN (:attrs)
          GROUP BY product_id
          HAVING COUNT(DISTINCT attr_id) >= :attrs_size
        ) AS a ON p.id = a.product_id

        JOIN \`attribute-products\` b ON p.id = b.product_id
        JOIN attributes c ON c.id = b.attr_id AND c.type = 'carrier'

        GROUP BY attr_id
      ) as aa JOIN attributes bb ON aa.attr_id = bb.id
      ORDER BY aa.min_price;
    `,
      {
        replacements: {
          id,

          attrs,

          attrs_size: attrs.length,
        },
        type: sequelize.QueryTypes.SELECT,
      },
    );
  };
  Product.getMinimumPricePerCondition = async (id, selections = {}) => {
    const {carrier, ...other} = selections;
    const attrs = Object.values(other);

    return sequelize.query(
      `SELECT aa.*, bb.name FROM (
        SELECT MIN(p.price) as min_price, b.attr_id FROM (
          SELECT price, id FROM products WHERE category_id = :id AND stock > 0 AND price > 0 AND active = 1
        ) AS p
          JOIN (
          SELECT product_id FROM \`attribute-products\` WHERE attr_id IN (:attrs)
          GROUP BY product_id
          HAVING COUNT(DISTINCT attr_id) >= :attrs_size
        ) AS a ON p.id = a.product_id

          ${
            !!carrier
              ? `JOIN \`attribute-products\` d ON p.id = d.product_id AND (d.attr_id IN (:unlocked) OR d.attr_id = :carrier)`
              : ''
          }

          JOIN \`attribute-products\` b ON p.id = b.product_id
        JOIN attributes c ON c.id = b.attr_id AND c.type = 'appearance'

          GROUP BY attr_id
      ) as aa JOIN attributes bb ON aa.attr_id = bb.id;
    `,
      {
        replacements: {
          id,
          carrier,
          attrs,
          unlocked: await getUnlockedFallback(carrier),
          attrs_size: attrs.length,
        },
        type: sequelize.QueryTypes.SELECT,
      },
    );
  };

  Product.findVariantForSelectionsWithTags = async ({selections, id}) => {
    const variant = await Product.findVariantForSelections({selections, id});

    if (variant?.[0]?.id) {
      const tags = await Models().AttributeProduct.findAllWithNameForVariant(variant[0]?.id);
      return {
        tags,
        variant: variant[0],
      };
    }

    return {variant: null, tags: []};
  };

  Product.findVariantForSelections = async ({selections, id, ignoreDropshipping = false}) => {
    const withDropshipping = await findVariantForSelectionsImpl({
      selections,
      id,
      ignoreDropshipping,
    });

    return withDropshipping;

    /*const withoutDropshipping = await findVariantForSelectionsImpl({
      selections,
      id,
      ignoreDropshipping: true,
    });
    console.log('with', withDropshipping);
    console.log('withOUT', withoutDropshipping);

    if (
      withoutDropshipping?.[0] &&
      withoutDropshipping[0].price <= withDropshipping[0].price * DropshippingMargin
    ) {
      withoutDropshipping[0].price = withDropshipping[0].price;
      return withoutDropshipping;
    }

    return withDropshipping;*/
  };

  Product.findMinPricePerIncredibleId = async (incredible_ids, only_sellers) => {
    const minPrices = await sequelize.query(
      `
      SELECT MIN(price) as min_price, incredible_id FROM products
      WHERE stock > 0 AND active = 1 AND owner IS NOT NULL AND incredible_id IS NOT NULL AND price > 0
      ${only_sellers ? 'AND owner != :DropshippingSellerID' : ''}
      ${incredible_ids?.length ? 'AND incredible_id IN (:incredible_ids)' : ''}
      GROUP BY incredible_id;
    `,
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          incredible_ids,
          DropshippingSellerID,
        },
      },
    );

    return arrayToMap(minPrices, 'incredible_id');
  };

  async function findVariantForSelectionsImpl({selections, id, ignoreDropshipping}) {
    const {joins, groupBy} = await getJoinsForAttributes(selections);

    return sequelize.query(
      `
      SELECT products.id, price, original_price, stock, owner, source_id, facebook_id FROM products
      ${joins}
      WHERE products.category_id = :id AND stock > 0 AND price > 0 AND products.active = 1 ${
        ignoreDropshipping ? 'AND products.owner != 1' : ''
      }
      ${groupBy}
    `,
      {
        replacements: {id, dropshippingId: DropshippingSellerID},
        type: sequelize.QueryTypes.SELECT,
      },
    );
  }

  return Product;
}
/* eslint-enable camelcase */

async function getJoinsForAttributes(selections) {
  const joins = [];
  if (selections.carrier) {
    const unlockedFallback = await getUnlockedFallback(selections.carrier);
    joins.push(
      `INNER JOIN \`attribute-products\` a ON products.id = a.product_id AND (a.attr_id = ${
        selections.carrier
      } || a.attr_id IN (${unlockedFallback.toString()}))`,
    );
  }

  let attrs = new Set(Object.values(selections));
  attrs.delete(selections.carrier);
  attrs.delete(null);

  if (attrs.size) {
    joins.push(
      `INNER JOIN \`attribute-products\` b ON products.id = b.product_id AND b.attr_id IN (${[
        ...attrs,
      ].toString()})`,
    );
  }

  const groupBy = attrs.size
    ? `
    GROUP BY products.id HAVING COUNT(DISTINCT b.attr_id) >= ${attrs.size}
    ORDER BY price ASC LIMIT 1
  `
    : `
    GROUP BY products.id HAVING COUNT(DISTINCT a.attr_id) >= 1
    ORDER BY price ASC LIMIT 1
  `;

  return {joins: joins.join(' '), groupBy};
}
async function getUnlockedFallback(carrierId) {
  const carrierData = await Models().Attribute.findOne({where: {id: carrierId}, raw: true});
  const unlockedFallback = FallBackUnlocked[carrierData?.name] || [carrierId];
  return unlockedFallback;
}

/* eslint-disable camelcase */
import Seq from 'sequelize';
import {obfuscateEmailForDisplay} from '@Common/utils/security-utils';
import {DefaultFastShippingRate} from '@Common/constants/app';
import {RequestError, RequestErrorMessage} from '@Common/constants/errors';

// Initialize model
export default function (sequelize) {
  const PromoCode = sequelize.define(
    'promo-codes',
    {
      id: {
        type: Seq.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: Seq.STRING(30),
        unique: true,
      },
      price: {
        type: Seq.FLOAT,
      },
      active: {
        type: Seq.BOOLEAN,
        defaultValue: 1,
      },
    },
    {
      indexes: [
        {
          fields: ['name'],
          unique: true,
        },
        {
          fields: ['active'],
        },
        {
          fields: ['price'],
        },
      ],
    },
  );

  return PromoCode;
}
/* eslint-enable camelcase */

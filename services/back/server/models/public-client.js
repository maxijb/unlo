/* eslint-disable camelcase */
import Seq from 'sequelize';
import isEmpty from 'is-empty';
import {Models} from './index';

import {obfuscateEmail, obfuscatePhone} from '@Common/utils/security-utils';
import {
  FieldsUserContact,
  FieldsTriggerNewIdentity,
  PublicAttributes
} from '@Common/constants/public-user';
import {PRISTINE_FIELD} from '@Common/constants/input';

// Initialize model
export default function (sequelize) {
  const PublicClient = sequelize.define('public-client', {
    id: {
      type: Seq.BIGINT,
      autoIncrement: true,
      primaryKey: true
    },
    company: {
      type: Seq.STRING(30),
      index: true
    },
    phone: {
      type: Seq.STRING(30)
    },
    validated: {
      type: Seq.BOOLEAN,
      defaultValue: false
    },
    password: {
      type: Seq.STRING(50),
      index: true
    },
    avatar: {
      type: Seq.STRING(255)
    },
    notes: {
      type: Seq.TEXT
    },
    status: {
      type: Seq.STRING(100)
    },
    createdBy: {
      type: Seq.STRING(40)
    },
    preferredContactMethod: {
      type: Seq.ENUM,
      values: ['email', 'phone', 'facebook', 'google'],
      defaultValue: 'email'
    },
    stripeId: {
      type: Seq.STRING(30)
    }
  });

  /* This is used in the dashboard and and it's NOT obfuscated
   * TODO: group by publicId and select last identity only
   */
  PublicClient.getMultiPublicUserProfile = async (ids, appId) => {
    return Models().Identity.findAll({
      where: {publicId: ids, appId},
      attributes: PublicAttributes,
      raw: true
    });
  };

  /* This is used in the dashboard and and it's NOT obfuscated */
  PublicClient.getInternalPublicUserProfile = async (id, appId) => {
    const result = await Models().Identity.findOne({
      where: {publicId: id, appId},
      order: [['id', 'desc']],
      attributes: PublicAttributes,
      raw: true
    });
    return result;
  };

  /* This is used for clients and it's obfuscated */
  PublicClient.getPublicUserProfile = async (id, appId) => {
    const result = await Models().Identity.findOne({
      where: {publicId: id, appId},
      order: [['id', 'desc']],
      attributes: [...FieldsUserContact, 'id'],
      raw: true
    });

    if (!result) {
      return {
        id,
        appId
      };
    }

    return {
      ...result,
      identityId: result.id,
      email: obfuscateEmail(result.email),
      // TODO: check with legal if we need to obfuscate the name as well
      //obfuscateName(result.name),
      phone: obfuscatePhone(result.phone),
      id,
      appId
    };
  };

  PublicClient.addUserContact = async ({appId, pid, contact = {}}) => {
    let previousIdentity = null;
    if (contact.identityId) {
      previousIdentity = await Models().Identity.findOne({where: {id: contact.identityId}});
    }

    let shouldCreateIdentity = false;
    const newData = {};
    FieldsUserContact.forEach(field => {
      // to store data we check that:
      // a) is deffined
      // b) it's not flagged as pristine
      // c) if there's a previous identity, it doesn't match what we previously had
      if (
        typeof contact[field] !== 'undefined' &&
        contact[field] !== PRISTINE_FIELD &&
        (!previousIdentity || previousIdentity.get(field) !== contact[field])
      ) {
        newData[field] = contact[field];

        if (FieldsTriggerNewIdentity[field]) {
          shouldCreateIdentity = true;
        }
      }
    });

    if (!isEmpty(newData)) {
      if (!shouldCreateIdentity) {
        return previousIdentity.update(newData);
      } else {
        const {id, ...previousData} = previousIdentity ? previousIdentity.get({plain: true}) : {};
        return Models().Identity.create({
          ...previousData,
          ...newData,
          appId,
          publicId: pid
        });
      }
    }

    return previousIdentity;
  };

  return PublicClient;
}
/* eslint-enable camelcase */

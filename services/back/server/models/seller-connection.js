/* eslint-disable camelcase */
import Seq from 'sequelize';
import {SellerConnectionStatus} from '@Common/constants/app';
import {arrayToMap} from '@Common/utils/generic-utils';
import {Models} from './index';

// Initialize model
export default function (sequelize) {
  const SellerConnection = sequelize.define(
    'seller-connections',
    {
      id: {
        type: Seq.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      seller_id: {
        type: Seq.INTEGER,
      },
      type: {
        type: Seq.STRING(12),
      },
      account_name: {
        type: Seq.STRING(50),
      },
      token: {
        type: Seq.STRING(50),
      },
      refetch_token: {
        type: Seq.STRING(50),
      },
      api_domain: {
        type: Seq.STRING(50),
      },
      status: {
        type: Seq.INTEGER,
        defaultValue: 0,
      },
      post_order: {
        type: Seq.BOOLEAN,
        defaultValue: 1,
      },
      company: {
        type: Seq.STRING(20),
      },
      order_status: {
        type: Seq.SMALLINT,
      },
    },
    {
      indexes: [
        {fields: ['account_name']},
        {fields: ['type']},
        {fields: ['seller_id']},
        {fields: ['seller_id', 'type'], unique: true},
      ],
    },
  );

  SellerConnection.getConnectionMap = async ({seller_id, isPublic = false, filter = {}}) => {
    const connections = await SellerConnection.findAll({
      where: {
        seller_id,
        status: SellerConnectionStatus.active,
        ...filter,
      },
      attributes: isPublic
        ? ['id', 'type', 'account_name', 'status', 'api_domain', 'company']
        : null,
    });

    const connectionsMap = arrayToMap(connections, 'type');

    const webhooks = await Models().SellerWebhook.findAll({
      where: {seller_id},
      attributes: ['type', 'uri'],
      raw: true,
    });
    connectionsMap.webhooks = webhooks.length ? arrayToMap(webhooks, 'type') : null;
    return connectionsMap;
  };

  SellerConnection.get = ({type, seller_id, account_name, filter = {}}) => {
    if (!seller_id && !account_name) {
      return null;
    }

    const where = {
      type,
      status: SellerConnectionStatus.active,
      ...filter,
    };

    if (seller_id) {
      where.seller_id = seller_id;
    }
    if (account_name) {
      where.account_name = account_name;
    }
    return SellerConnection.findOne({
      where,
      raw: true,
    });
  };

  return SellerConnection;
}
/* eslint-enable camelcase */

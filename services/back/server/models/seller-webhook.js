/* eslint-disable camelcase */
import Seq from 'sequelize';
import {obfuscateEmailForDisplay} from '@Common/utils/security-utils';
import {DefaultFastShippingRate} from '@Common/constants/app';

// Initialize model
export default function (sequelize) {
  const SellerWebhook = sequelize.define(
    'seller-webhooks',
    {
      id: {
        type: Seq.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      seller_id: {
        type: Seq.INTEGER,
      },
      type: {
        type: Seq.STRING(30),
      },
      uri: {
        type: Seq.STRING(255),
      },
      active: {
        type: Seq.BOOLEAN,
      },
    },
    {
      indexes: [{fields: ['seller_id']}, {fields: ['type']}],
    },
  );

  return SellerWebhook;
}
/* eslint-enable camelcase */

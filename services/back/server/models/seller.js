/* eslint-disable camelcase */
import Seq from 'sequelize';
import {obfuscateEmailForDisplay} from '@Common/utils/security-utils';
import {DefaultFastShippingRate} from '@Common/constants/app';

// Initialize model
export default function (sequelize) {
  const Seller = sequelize.define('sellers', {
    id: {
      type: Seq.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: Seq.STRING(120),
      unique: true,
    },
    email: {
      type: Seq.STRING(100),
    },
    description: {
      type: Seq.STRING(400),
      unique: true,
    },
    address_id: {
      type: Seq.INTEGER,
    },
    bank_name: {
      type: Seq.STRING(40),
    },
    bank_address: {
      type: Seq.STRING(255),
    },
    routing_number: {
      type: Seq.STRING(40),
    },
    account_number: {
      type: Seq.STRING(40),
    },
    cut_off_time: {
      type: Seq.INTEGER,
    },
    cut_off_timezone: {
      type: Seq.STRING(40),
    },
    cut_off_utc: {
      type: Seq.INTEGER,
    },
    phone: {
      type: Seq.STRING(30),
    },
    avatar: {
      type: Seq.INTEGER,
    },
    notes: {
      type: Seq.TEXT,
    },
    status: {
      type: Seq.INTEGER,
    },
    api_token: {
      type: Seq.STRING(50),
    },
    empty_null_products: {
      type: Seq.BOOLEAN,
    },
    is_retailer: {
      type: Seq.BOOLEAN,
    },
  });

  Seller.getSellerIdFromApiToken = async api_token => {
    if (!api_token) {
      return null;
    }
    const seller = await Seller.findOne({where: {api_token}, raw: true});
    return seller?.id;
  };

  Seller.getPublicProfile = async id => {
    if (!id) {
      return null;
    }

    return Seller.findOne({
      where: {
        id,
      },
      attributes: ['name', 'description', 'id'],
      raw: true,
    });
  };

  return Seller;
}
/* eslint-enable camelcase */

/* eslint-disable camelcase */
import Seq from 'sequelize';
import {obfuscateEmailForDisplay} from '@Common/utils/security-utils';
import {arrayToMap} from '@Common/utils/generic-utils';
import {addDeliveryDate} from '@Common/utils/date-utils';
import {DefaultFastShippingRate, DefaultShippingMethods} from '@Common/constants/app';
import {Models} from './index';

// Initialize model
export default function (sequelize) {
  const ShippingMethod = sequelize.define(
    'shipping_method',
    {
      id: {
        type: Seq.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      seller_id: {
        type: Seq.INTEGER,
      },
      speed_days: {
        type: Seq.INTEGER,
      },
      carrier: {
        type: Seq.STRING(10),
      },
      name: {
        type: Seq.STRING(40),
      },
      price: {
        type: Seq.FLOAT,
      },
      remote_id: {
        type: Seq.STRING(40),
      },
      remote_name: {
        type: Seq.STRING(60),
      },
      remote_source: {
        type: Seq.STRING(20),
      },
    },
    {
      indexes: [{fields: ['seller_id']}, {fields: ['speed_days']}, {fields: ['price']}],
    },
  );

  ShippingMethod.getAll = async (sellerIds, withDates = true) => {
    if (!Array.isArray(sellerIds)) {
      sellerIds = [sellerIds];
    }

    const sellerSet = new Set(sellerIds);

    let methods = [];
    let sellers = [];

    if (sellerIds.length) {
      [methods, sellers] = await Promise.all([
        ShippingMethod.findAll({
          where: {seller_id: sellerIds},
          order: [
            ['speed_days', 'DESC'],
            ['price', 'ASC'],
          ],
          attributes: {exclude: ['createdAt', 'updatedAt']},
          raw: true,
        }),
        !withDates ? null : Models().Seller.findAll({where: {id: sellerIds}, raw: true}),
      ]);
    }

    methods.forEach(m => sellerSet.delete(m.seller_id));

    for (const seller of sellerSet) {
      methods.push(...DefaultShippingMethods.map(m => ({...m, seller_id: seller})));
    }

    if (withDates) {
      const sellersMap = arrayToMap(sellers);
      methods.map(m => addDeliveryDate(m, sellersMap[m.seller_id]));
    }

    return methods;
  };

  ShippingMethod.get = async (id, seller_id) => {
    if (id < 0) {
      return DefaultShippingMethods.find(m => m.id === id);
    }
    return ShippingMethod.findOne({
      where: {
        id,
        seller_id,
      },
      attributes: {exclude: ['createdAt', 'updatedAt']},
      raw: true,
    });
  };

  ShippingMethod.upsertMethodById = async (data, seller_id) => {
    if (data.id) {
      ShippingMethod.update(data, {where: {id: data.id}});
      return data.id;
    } else {
      const method = ShippingMethod.create({...data, seller_id});
      return method.get('id');
    }
  };

  return ShippingMethod;
}
/* eslint-enable camelcase */

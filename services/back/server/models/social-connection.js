/* eslint-disable camelcase */
import Seq from 'sequelize';
import {Social} from '../../../common/constants/app';
// Initialize model
export default function (sequelize) {
  return sequelize.define('social-connections', {
    id: {
      type: Seq.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    userId: {
      type: Seq.INTEGER,
      index: true
    },
    socialId: {
      type: Seq.STRING(30),
      index: true
    },
    type: {
      type: Seq.ENUM,
      values: [Social.facebook, Social.google, Social.twitter, Social.linkedin],
      index: true
    },
    token: {
      type: Seq.STRING(40)
    },
    status: {
      type: Seq.INTEGER
    }
  });
}
/* eslint-enable camelcase */

/* eslint-disable camelcase */
import Seq from 'sequelize';

// Initialize model
export default function (sequelize) {
  const Spec = sequelize.define(
    'spec',
    {
      id: {
        type: Seq.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      category_id: {
        type: Seq.INTEGER,
      },
      name: {
        type: Seq.STRING(60),
      },
      value: {
        type: Seq.TEXT,
      },
      type: {
        type: Seq.STRING(10),
        index: true,
        defaultValue: 'spec',
      },
      visible: {
        type: Seq.BOOLEAN,
        index: true,
        defaultValue: 1,
      },
    },
    {
      indexes: [
        {
          unique: true,
          fields: ['category_id', 'name'],
        },
        {
          fields: ['category_id', 'type'],
        },
      ],
    },
  );

  Spec.getSpecs = id => {
    return Spec.findAll({
      where: {
        category_id: id,
        type: 'spec',
      },
      raw: true,
      attributes: ['name', 'value'],
    });
  };

  return Spec;
}
/* eslint-enable camelcase */

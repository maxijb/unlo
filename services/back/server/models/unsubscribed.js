/* eslint-disable camelcase */
import Seq from 'sequelize';
import {Social} from '../../../common/constants/app';
// Initialize model
export default function (sequelize) {
  return sequelize.define('unsubscribed', {
    id: {
      type: Seq.BIGINT,
      autoIncrement: true,
      primaryKey: true
    },
    appId: {
      type: Seq.INTEGER,
      allowNull: true,
      index: true
    },
    email: {
      type: Seq.STRING(200),
      index: true
    }
  });
}
/* eslint-enable camelcase */

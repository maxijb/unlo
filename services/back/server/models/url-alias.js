/* eslint-disable camelcase */
import Seq from 'sequelize';
import {Models} from './index';

// Initialize model
export default function (sequelize) {
  const UrlAlias = sequelize.define(
    'url-alias',
    {
      id: {
        type: Seq.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      url: {
        type: Seq.STRING(255),
        index: true,
      },
      page: {
        type: Seq.STRING(20),
        index: true,
      },
      data: {
        type: Seq.TEXT,
      },
    },
    {
      indexes: [
        {
          unique: true,
          fields: ['url'],
        },
        {
          fields: ['page'],
        },
      ],
    },
  );

  return UrlAlias;
}
/* eslint-enable camelcase */

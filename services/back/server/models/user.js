/* eslint-disable camelcase */
import Seq from 'sequelize';
import {obfuscateEmailForDisplay} from '@Common/utils/security-utils';
import {DefaultFastShippingRate} from '@Common/constants/app';
import {RequestError, RequestErrorMessage} from '@Common/constants/errors';

// Initialize model
export default function (sequelize) {
  const User = sequelize.define(
    'users',
    {
      id: {
        type: Seq.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      email: {
        type: Seq.STRING(120),
        unique: true,
      },
      username: {
        type: Seq.STRING(30),
        unique: true,
      },
      firstName: {
        type: Seq.STRING(40),
      },
      lastName: {
        type: Seq.STRING(40),
      },
      company: {
        type: Seq.STRING(30),
        index: true,
      },
      phone: {
        type: Seq.STRING(30),
      },
      validated: {
        type: Seq.BOOLEAN,
        defaultValue: false,
      },
      password: {
        type: Seq.STRING(50),
        index: true,
      },
      avatar: {
        type: Seq.STRING(255),
      },
      notes: {
        type: Seq.TEXT,
      },
      status: {
        type: Seq.STRING(100),
      },
      createdBy: {
        type: Seq.STRING(40),
      },
      preferredContactMethod: {
        type: Seq.ENUM,
        values: ['email', 'phone', 'facebook', 'google'],
        defaultValue: 'email',
      },
      stripeId: {
        type: Seq.STRING(30),
      },
      superAdmin: {
        type: Seq.BOOLEAN,
        defaultValue: false,
      },
      isInvite: {
        type: Seq.BOOLEAN,
        defaultValue: false,
      },
      isSeller: {
        type: Seq.BOOLEAN,
        defaultValue: false,
        index: true,
      },
      fast_shipping_rate: {
        type: Seq.FLOAT,
      },
      seller_id: {
        type: Seq.INTEGER,
      },
    },
    {
      indexes: [
        {
          fields: ['isSeller'],
        },
        {
          fields: ['superAdmin'],
        },
        {
          fields: ['password'],
        },
        {
          fields: ['seller_id'],
        },
      ],
    },
  );

  User.isSuperAdmin = async userId => {
    const user = await User.findOne({where: {id: userId}});
    return user ? Boolean(user.get('superAdmin')) : false;
  };
  User.isSellerOrSuperAdmin = async userId => {
    const user = await User.findOne({
      where: {id: userId},
      attributes: ['isSeller', 'seller_id', 'superAdmin'],
      raw: true,
    });
    return {
      isSellerOrSuperAdmin: user ? Boolean(user.superAdmin) || Boolean(user.isSeller) : false,
      seller_id: user?.seller_id || null,
      superAdmin: Boolean(user?.superAdmin) || false,
      isSeller: user?.isSeller || false,
    };
  };

  /* This profile is available to employees.
   * must never be exposed to any widget api
   */
  User.getPublicProfile = async ids => {
    return User.findAll({
      where: {id: ids},
      attributes: ['email', 'username', 'firstName', 'lastName', 'avatar', 'id'],
      raw: true,
    });
  };

  /* This profile is available to widget clients
   * Must contain ony display information
   */
  User.getClientPublicProfile = async ids => {
    return User.findAll({
      where: {id: ids},
      attributes: ['firstName', 'avatar', 'id'],
      raw: true,
    });
  };

  User.getUserOwnMinimumDetails = async req => {
    const id = req.auth?.id;
    if (!id) {
      return null;
    }

    const user = await User.findOne({
      where: {id: id},
      attributes: ['firstName', 'email', 'superAdmin'],
      raw: true,
    });

    if (!user) {
      return null;
    }

    const {firstName, email, superAdmin} = user;
    return {
      id,
      displayName: firstName || obfuscateEmailForDisplay(email),
      superAdmin,
    };
  };

  User.getUserOwnExtendedDetails = async req => {
    const id = req.auth?.id;
    if (!id) {
      return null;
    }

    return User.findOne({
      where: {id: id},
      //attributes: ['firstName', 'email', 'lastName', 'phone'],
      raw: true,
    });
  };

  User.getFastShippingRate = async id => {
    const user = await User.findOne({
      where: {id, isSeller: 1},
      raw: true,
      attributes: ['fast_shipping_rate'],
    });

    return user?.fast_shipping_rate || DefaultFastShippingRate;
  };

  User.getSellerId = async (req, throwOnNull = true) => {
    const user = await User.findOne({
      where: {id: req.auth.id},
      raw: true,
      attributes: ['seller_id'],
    });
    console.log(user);
    if (!user?.seller_id && throwOnNull) {
      throw new RequestError(RequestErrorMessage.missingUserId);
    }
    return user?.seller_id || null;
  };

  return User;
}
/* eslint-enable camelcase */

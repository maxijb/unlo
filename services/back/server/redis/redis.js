import redis from 'redis';
import bluebird from 'bluebird';

bluebird.promisifyAll(redis);

let redisInstance = null;

export const initRedis = server => {
  const config = server.config.get('redis');
  redisInstance = redis.createClient({
    host: config.host,
    port: config.port
  });
  return redisInstance;
};

export default function getRedis() {
  return redisInstance;
}

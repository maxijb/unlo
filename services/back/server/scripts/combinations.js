import {Models} from '../models/index';
import {arrayToMap, arrayToMultipleMap} from '@Common/utils/generic-utils';
import {findRelevantImages} from '../../../front/shared/utils/attributes-utils';

const createCsvWriter = require('csv-writer').createObjectCsvWriter;

const csvWriter = createCsvWriter({
  path: 'incredible-ids.csv',
  header: [
    {id: 'id', title: 'Id'},
    {id: 'product', title: 'Product'},
    {id: 'capacity', title: 'Capacity'},
    {id: 'color', title: 'Color'},
    {id: 'carrier', title: 'Carrier'},
    {id: 'appearance', title: 'Appearance'},
    {id: 'image', title: 'Image'},
  ],
});

const Valid = {
  'iPhone 6': {
    capacity: ['16GB', '32GB', '64GB', '128GB'],
    color: ['Silver', 'SpceGray', 'Gold'],
  },

  'iPhone 6 Plus': {
    capacity: ['16GB', '64GB', '128GB'],
    color: ['Silver', 'SpceGray', 'Gold'],
  },

  'iPhone 6S': {
    capacity: ['16GB', '32GB', '64GB', '128GB'],
    color: ['Silver', 'SpceGray', 'rsegold', 'Gold'],
  },
  'iPhone 6S Plus': {
    capacity: ['16GB', '32GB', '64GB', '128GB'],
    color: ['Silver', 'SpceGray', 'rsegold', 'Gold'],
  },

  'iPhone 7': {
    capacity: ['32GB', '128GB', '256GB'],
    color: ['Silver', 'rsegold', 'Red', 'Black', 'Gold', 'jetblck'],
  },
  'iPhone 7 Plus': {
    capacity: ['32GB', '128GB', '256GB'],
    color: ['Silver', 'rsegold', 'Red', 'Black', 'Gold', 'jetblck'],
  },

  'iPhone 8': {
    capacity: ['64GB', '128GB', '256GB'],
    color: ['Silver', 'Spcegray', 'Red', 'Gold'],
  },
  'iPhone 8 Plus': {
    capacity: ['64GB', '128GB', '256GB'],
    color: ['Silver', 'Spcegray', 'Red', 'Gold'],
  },
  'iPhone X': {
    capacity: ['64GB', '256GB'],
    color: ['Silver', 'SpceGray'],
  },
  'iPhone XR': {
    capacity: ['64GB', '128GB', '256GB'],
    color: ['Yellow', 'Red', 'Black', 'White', 'Blue', 'Coral'],
  },
  'iPhone XS': {
    capacity: ['64GB', '256GB', '512GB'],
    color: ['Silver', 'Spcegray', 'Gold'],
  },
  'iPhone XS Max': {
    capacity: ['64GB', '256GB', '512GB'],
    color: ['Silver', 'Spcegray', 'Gold'],
  },
  'iPhone SE (2020)': {
    capacity: ['64GB', '128GB', '256GB'],
    color: ['Red', 'Black', 'White'],
  },
  'iPhone 11': {
    capacity: ['64GB', '128GB', '256GB'],
    color: ['Purple', 'Yellow', 'Green', 'Red', 'Black', 'White'],
  },
  'iPhone 11 Pro': {
    capacity: ['64GB', '256GB', '512GB'],
    color: ['mdntgren', 'Silver', 'SpceGray', 'Gold'],
  },
  'iPhone 11 Pro Max': {
    capacity: ['64GB', '256GB', '512GB'],
    color: ['mdntgren', 'Silver', 'SpceGray', 'Gold'],
  },
};

export default async function () {
  const attributes = await Models().Attribute.findAll({
    attributes: ['slug', 'type', 'id', 'name'],
    raw: true,
  });
  const byType = arrayToMultipleMap(attributes, 'type');
  const bySlug = arrayToMap(attributes, 'slug');

  const categories = await Models().Category.findAll({
    raw: true,
    attributes: ['slug', 'name', 'id'],
  });
  const byCat = arrayToMap(categories, 'name', e => e.toLowerCase());
  const categoryImages = await Models().CategoryImage.findAllForCategories(
    categories.map(c => c.id),
  );
  const images = await Models().Image.findAll({
    where: {
      id: categoryImages.map(i => i.img_id),
    },
    raw: true,
  });

  const imagesMap = arrayToMap(images, 'id');

  console.log(bySlug);

  //validate all categories are found
  Object.keys(Valid).forEach(name => {
    if (!byCat.hasOwnProperty(name.toLowerCase())) {
      throw new Error('Invalid category ' + name);
    }
    for (const cap of Valid[name].capacity) {
      if (!bySlug.hasOwnProperty(cap.toLowerCase())) {
        throw new Error('Invalid slug ' + cap);
      }
    }
    for (const cap of Valid[name].color) {
      if (!bySlug.hasOwnProperty(cap.toLowerCase())) {
        throw new Error('Invalid slug ' + cap);
      }
    }
  });

  let n = 1;
  const data = [];
  Object.keys(Valid).forEach(name => {
    const prod = byCat[name.toLowerCase()].slug;
    for (const cap of Valid[name].capacity) {
      for (const color of Valid[name].color) {
        for (const carrier of Object.keys(byType.carrier)) {
          for (const appearance of Object.keys(byType.appearance)) {
            const tags = new Set([
              byCat[name.toLowerCase()].id,
              bySlug[cap.toLowerCase()].id,
              bySlug[color.toLowerCase()].id,
              byType.carrier[carrier].id,
              null,
            ]);
            console.log(findRelevantImages(categoryImages, tags)[0].get().img_id);
            data.push({
              id: `${prod}-${cap}-${color.toLowerCase()}-${byType.carrier[carrier].slug}-${
                byType.appearance[appearance].slug
              }`,
              product: byCat[name.toLowerCase()].name,
              capacity: bySlug[cap.toLowerCase()].name,
              color: bySlug[color.toLowerCase()].name,
              carrier: byType.carrier[carrier].name,
              appearance: byType.appearance[appearance].name,
              image: findRelevantImages(categoryImages, tags)[0].get().img_id
                ? `https://dvmucn6zqagd0.cloudfront.net/static-files/400x400/${
                    imagesMap[findRelevantImages(categoryImages, tags)[0].get().img_id]?.name
                  }`
                : '',
            });
          }
        }
      }
    }
  });

  csvWriter.writeRecords(data).then(() => console.log('The CSV file was written successfully'));
}

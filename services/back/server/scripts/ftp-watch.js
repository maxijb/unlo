/* Imports a CSV file generated by FINALE INVENTORY */

import {StaticFilesFolder} from '@Common/constants/app';
import fs from 'fs';
import http from 'http';
import Logger from '@Common/logger';
import path from 'path';

import {asyncForEach} from '@Common/utils/async';
import {Models} from '../models';
import csv from 'csv-parser';
import chokidar from 'chokidar';
import getConfig from '../get-config';

export default async function () {
  const config = getConfig();
  const ftpRoot = config.get('server').ftpRoot;

  const watcher = chokidar.watch(`${ftpRoot}/seller-*/*.csv`, {
    ignored: /(^|[\/\\])\../, // ignore dotfiles
    persistent: true,
    ignoreInitial: true,
  });

  watcher
    .on('add', path => {
      Logger.log(`FTP File ${path} has been added`);
      processFile(path);
    })
    .on('change', path => {
      Logger.log(`FTP File ${path} has been changed`);
      processFile(path);
    });
}

function processFile(file) {
  const parts = file.split('/');
  const basePath = parts[parts.length - 2];
  const [_, type, _name, seller_id] = basePath.split('-');

  if (type !== 'finale') {
    Logger.error('seller type not supported', type);
    return;
  }

  if (!Number(seller_id)) {
    Logger.error('seller_id not supported', seller_id);
    return;
  }

  const Input = fs.createReadStream(file);

  Input.pipe(csv())
    .on('error', error => {
      Logger.error(error);
      errors.push(error);
    })
    .on('data', async row => {
      const source_id = row['Product ID'];
      const stock = row['Units Available'];
      const price = row['Item price'];

      const product = await Models().Product.findOne({
        where: {
          owner: seller_id,
          source_id,
        },
      });

      if (product) {
        if (typeof stock !== 'undefined' && stock !== null) {
          product.stock = stock;
        }
        if (typeof price !== 'undefined' && price !== null) {
          product.price = price;
        }
        await product.save();
        Logger.info(`Product updated id ${product.get('id')} - SKU ${source_id}`);
      } else {
        Logger.info(`Product ignored for id ${source_id}`);
      }
    });
}

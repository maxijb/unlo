import {Models} from '../models/index';
import {arrayToMap, arrayToMultipleMap} from '@Common/utils/generic-utils';
import {getIncredibleIdFromSlugs} from '../../../front/shared/utils/attributes-utils';

export default async function () {
  const attributes = await Models().Attribute.findAll({
    attributes: ['slug', 'type', 'id', 'name'],
    raw: true,
  });

  const attrs = arrayToMap(attributes, 'id');

  const categories = await Models().Category.findAll({
    raw: true,
    attributes: ['slug', 'name', 'id'],
  });
  const cats = arrayToMap(categories, 'id');

  console.log(cats, attrs);

  const products = await Models().Product.getAttrsById();
  console.log(products.length);
  let n = 0;
  for (const product of products) {
    n++;
    const myAttrs = product.attrs?.split(',').reduce((agg, at) => {
      const thisAttr = attrs[at];
      agg[thisAttr.type] = thisAttr.slug;
      return agg;
    }, {});

    if (!product.category_id || !myAttrs || Object.keys(myAttrs).length !== 4) {
      continue;
    }

    myAttrs.category = cats[product.category_id].slug;
    await Models().Product.update(
      {incredible_id: getIncredibleIdFromSlugs(myAttrs)},
      {where: {id: product.id}},
    );
    console.log('processed item', n);
  }

  /*
  let n = 1;
  const data = [];
  Object.keys(Valid).forEach(name => {
    const prod = byCat[name.toLowerCase()].slug;
    for (const cap of Valid[name].capacity) {
      for (const color of Valid[name].color) {
        for (const carrier of Object.keys(byType.carrier)) {
          for (const appearance of Object.keys(byType.appearance)) {
            data.push({
              id: `${prod}-${cap}-${color.toLowerCase()}-${byType.carrier[carrier].slug}-${
                byType.appearance[appearance].slug
              }`,
              product: byCat[name.toLowerCase()].name,
              capacity: bySlug[cap.toLowerCase()].name,
              color: bySlug[color.toLowerCase()].name,
              carrier: byType.carrier[carrier].name,
              appearance: byType.appearance[appearance].name,
            });
          }
        }
      }
    }
  });
    */
}

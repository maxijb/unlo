import {StaticFilesFolder} from '@Common/constants/app';
import fs from 'fs';
import Logger from '@Common/logger';
import path from 'path';

import mime from 'mime-types';

import sharp from 'sharp';
import {asyncForEach} from '@Common/utils/async';
import {Models} from '../models';
import aws from 'aws-sdk';
import InitConfig from '../init-config';

const s3 = new aws.S3({});
const AllowedExt = new Set(['png', 'jpg', 'webp', 'jpeg', 'pdf']);
const folder = path.join(__dirname, `../../../../../${StaticFilesFolder}`);

async function uploadToS3(file, key) {
  return new Promise((resolve, reject) => {
    // console.log('uploading to', path.join('builds', buildId, key));
    fs.readFile(file, (err, body) => {
      var params = {
        Bucket: 'incredible-public',
        Key: path.join(StaticFilesFolder, key),
        Body: body,
        ACL: 'public-read',
        ContentType: mime.lookup(file) || 'text/plain',
      };

      s3.putObject(params, function (err, data) {
        if (err) {
          Logger.error(err, err.stack);
          reject();
        } else {
          resolve();
        }
      });
    });
  });
}

const Config = [
  {
    options: [
      100,
      100,
      {
        withoutEnlargement: true,
        fit: 'inside',
      },
    ],
    path: '100x100',
  },
  {
    options: [
      150,
      150,
      {
        withoutEnlargement: true,
        fit: 'inside',
      },
    ],
    path: '150x150',
  },
  {
    options: [
      400,
      400,
      {
        withoutEnlargement: true,
        fit: 'inside',
      },
    ],
    path: '400x400',
  },
];

const EncodeOptions = {
  jpeg: {
    options: {
      quality: 50,
    },
    extraFormat: 'webp',
  },
  jpg: {
    options: {
      quality: 50,
    },
    extraFormat: 'webp',
  },
  webp: {
    options: {
      quality: 80,
    },
    extraFormat: 'png',
    folderExt: 'webp',
  },
  png: {
    extraFormat: 'webp',
  },
};

export default function () {
  const files = fs.readdirSync(folder);
  const config = InitConfig();
  asyncForEach(files, file => processFile(file, config));
}

export async function processFile(file, serverConfig) {
  const {shouldUploadToS3} = serverConfig.get('server');

  const location = path.join(folder, file);
  const stats = fs.statSync(location);

  let ext = path.extname(file).replace('.', '');
  if (ext === 'jpg') {
    ext = 'jpeg';
  }

  if (stats.isDirectory() || !AllowedExt.has(ext)) {
    return;
  }
  Logger.log(`Processing ${file}`);

  if (shouldUploadToS3) {
    await uploadToS3(location, file);
  }

  if (!EncodeOptions.hasOwnProperty(ext)) {
    return;
  }

  const sharpFile = sharp(location);

  await asyncForEach(Config, async config => {
    const resizedKey = path.join(`${config.path}${EncodeOptions[ext.folderExt] || ''}`, file);
    const resizedPath = path.join(folder, resizedKey);

    await sharpFile
      .resize(...config.options)
      [ext](EncodeOptions[ext].options || {})
      .toFile(resizedPath);

    if (shouldUploadToS3) {
      await uploadToS3(resizedPath, resizedKey);
    }

    if (EncodeOptions[ext].extraFormat) {
      const newExt = EncodeOptions[ext].extraFormat;
      const newFile = file.split('.').slice(0, -1).concat(newExt).join('.');

      const newKey = path.join(`${config.path}${EncodeOptions[newExt].folderExt || ''}`, newFile);
      const newPath = path.join(folder, newKey);

      await sharpFile
        .resize(...config.options)
        [newExt](EncodeOptions[newExt].options || {})
        .toFile(newPath);

      if (shouldUploadToS3) {
        await uploadToS3(newPath, newKey);
      }
    }
  });

  const model = await Models().Image.create({name: file, visible: 1}, {ignoreDuplicates: true});
  Logger.log(`Done ${file}`);
  return model.get('id');
}

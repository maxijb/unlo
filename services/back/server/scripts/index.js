import Images from './images-script';
import Csv from './csv-script';
import CsvSpecs from './csv-specs-script';
import CsvPrices from './csv-just-prices-script';
import Competitors from './csv-competitors-script';
import Combinations from './combinations';
import FixIds from './generate-incredible-ids';
import FtpWatch from './ftp-watch';
import StaticUrls from './static-urls';

import RevampXml from './revamp-xml';

export default {
  images: Images,
  csv: Csv,
  'csv-prices': CsvPrices,
  'csv-specs': CsvSpecs,
  revamp: RevampXml,
  competitors: Competitors,
  combinations: Combinations,
  'fix-ids': FixIds,
  'ftp-watch': FtpWatch,
  'static-urls': StaticUrls,
};

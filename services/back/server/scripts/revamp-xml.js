import fs from 'fs';
import path from 'path';

export default function RevampXML() {
  let content = fs.readFileSync(path.join(__dirname, './revamp.xml'), 'utf8');
  content = content.replace(/\<deeplink\>.*?\<\/deeplink\>/g, '');
  fs.writeFileSync(path.join(__dirname, './revamp-processed.xml'), content);
  console.log(content);
}

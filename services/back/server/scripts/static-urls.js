import {Models} from '../models/index';
import {arrayToMap, arrayToMultipleMap} from '@Common/utils/generic-utils';
import {findRelevantImages} from '../../../front/shared/utils/attributes-utils';
import {slugify} from '@Common/utils/urls';

export default async function () {
  const categories = await Models().Category.findAll({
    raw: true,
    where: {
      isLeaf: 1,
      visible: 1,
    },
  });
  const carriers = await Models().Attribute.getAllCarriers();
  const cat_map = arrayToMap(categories);

  const categoriesPureItems = [];

  categories.forEach(c => {
    categoriesPureItems.push({
      url: `/refurbished-${slugify(c.name)}`,
      page: 'category',
      data: {id: c.id},
    });

    for (const carrier of carriers) {
      categoriesPureItems.push({
        url: `/refurbished-${slugify(`${carrier.name} ${c.name}`)}`,
        page: 'category',
        data: {id: c.id, attr: carrier.slug},
      });
    }
  });

  for (const cat of categoriesPureItems) {
    const existing = await Models().UrlAlias.findOne({where: {url: cat.url}});
    if (existing) {
      existing.data = JSON.stringify(cat.data);
      existing.page = cat.page;
      await existing.save();
    } else {
      await Models().UrlAlias.create({...cat, data: JSON.stringify(cat.data)});
    }
  }
}

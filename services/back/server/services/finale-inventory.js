import {formatUri} from '@Common/utils/urls';
import QS from 'qs';
import fetch from 'node-fetch';
import path from 'path';
import {RequestError} from '@Common/constants/errors';
import {CsrfTokenHeader, SellerConnectionStatus} from '@Common/constants/app';
import {getBrightpearlRedirectURI} from '@Common/utils/urls';
import {decimalNumber} from '@Common/utils/format-utils';
import {arrayToMap} from '@Common/utils/generic-utils';

export async function getClient(req, accountCode, connection, seller_id) {
  if (accountCode && !connection) {
    connection = await req.Models.SellerConnection.get({
      type: 'finale',
      account_name: accountCode,
    });
  } else if (!accountCode && !connection && (seller_id || req.seller_id)) {
    connection = await req.Models.SellerConnection.get({
      type: 'finale',
      seller_id: seller_id || req.seller_id,
    });
    accountCode = connection?.account_name;
  }

  if (!accountCode || !connection) {
    throw new RequestError('No finale connection available');
  }

  return new FinaleInventoryClient(req, accountCode, connection, seller_id || req.seller_id);
}

class FinaleInventoryClient {
  constructor(req, accountCode, connection, seller_id) {
    this.req = req;
    this.accountCode = accountCode;
    this.connection = connection;
    this.seller_id = seller_id;
  }

  async getAuthToken(query) {
    const data = {
      client_id: this.clientId,
      client_secret: this.secret,
    };

    const r = await fetch(`https://${this.connection.api_domain}/${this.accountCode}/api/auth`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: this.connection.token,
        password: this.connection.refetch_token,
      }),
    });

    this.cookie = r.headers.get('set-cookie');

    const json = await r.json();

    return json;
  }

  async getProducts(query) {
    return this.fetch(`product`);
  }

  async getProduct(query) {
    return this.fetch(`product/${query.id}`);
  }

  async getInventory(query) {
    return this.fetch(`inventoryitem/`);
  }

  async getShippingMethods(query) {
    return this.fetch(`/warehouse-service/shipping-method/`);
  }

  async getTaxCodes(query) {
    return this.fetch(`/accounting-service/tax-code/`);
  }

  async getOrder(query) {
    return this.fetch(`/order-service/order/${query.remote_id}`);
  }

  async cancelItemsInOrder(query) {
    const bpOrder = await this.getOrder({remote_id: query.remote_id});
    const skus = await this.req.Models.Product.findAll({
      where: {
        id: query.products.map(p => p.product_id),
      },
      raw: true,
      attributes: ['source_id', 'id'],
    });
    // we need to map the source_id with the product ID coming from the remote order
    const skusMap = arrayToMap(skus, 'id');

    const toDelete = new Set();
    for (const product of query.products) {
      const bpOrderRows = bpOrder.response?.[0]?.orderRows;

      const foundKey = Object.keys(bpOrderRows).find(key => {
        const row = bpOrderRows[key];
        // must match id and quantity and not having been flagged for deletion before
        return (
          String(row?.productId) === skusMap[product.product_id]?.source_id &&
          Number(row.quantity.magnitude) === product.quantity &&
          !toDelete.has(key)
        );
      });
      if (foundKey) {
        toDelete.add(foundKey);
      }
    }

    // Delete every found row
    for (const key of toDelete) {
      await this.fetch(`order-service/order/${query.remote_id}/row/${key}`, 'DELETE');
    }
    return this.fetch(`order-service/order/${query.remote_id}/note`, 'POST', {
      text: `Items have been cancelled.
Reason: ${query.returnOrder.reason}
Explanation: ${query.returnOrder.explanation}
`,
    });
  }
  async cancelOrder(query) {
    return this.fetch(`/order-service/order/${query.remote_id}/status`, 'PUT', {
      orderStatusId: 5, // cancelled
      orderNote: {
        text: `${query.returnOrder?.reason ? query.returnOrder?.reason + ': ' : ''}${
          query.returnOrder?.explanation || ''
        }`,
      },
    });
  }

  async postOrder(query) {
    const address = await this.req.Models.Address.getPublicFields(query.order.address_id);
    const bpAddress = await this.fetch('/contact-service/postal-address/', 'POST', {
      addressLine1: `${address.street_number} ${address.route} ${address.unit}`,
      addressLine2: `${address.locality}, ${address.administrative_area_level_1}`,
      addressLine3: `${address.country}`,
      postalCode: address.postal_code,
      countryIsoCode: 'USA',
    });

    const contact = await this.fetch('/contact-service/contact/', 'POST', {
      firstName: query.order.firstName,
      lastName: query.order.lastName,
      postAddressIds: {
        DEF: bpAddress.response,
        DEL: bpAddress.response,
        BIL: bpAddress.response,
      },
      emails: {
        PRI: 'notifications@incrediblephones.com',
      },
      telephones: {
        PRI: query.order.phone,
      },
    });

    const rows = [];
    for (const product of query.products) {
      if (product.type === 'product') {
        const prod = await this.req.Models.Product.findOne({
          where: {id: product.product_id},
          attributes: ['source_id'],
          raw: true,
        });
        if (prod.source_id) {
          const net = decimalNumber(product.price * (product.quantity || 1));
          rows.push({
            quantity: product.quantity,
            productId: prod.source_id,
            net,
            taxCode: 'T',
            tax: decimalNumber(net * query.order.tax_rate),
          });
        }
      }
    }

    const body = {
      customer: {
        id: contact.response,
      },
      billing: {
        id: contact.response,
      },
      ref: `Incredible order ${query.order.id}`,
      externalRef: query.order.id,
      installedIntegrationInstanceId: 223, //arbitrary number identifying our server instance
      currency: {
        code: 'USD',
      },
      delivery: {
        address: {
          addressFullName: `${query.order.firstName} ${query.order.lastName}`,
          addressLine1: `${address.street_number} ${address.route} ${address.unit}`,
          addressLine2: `${address.locality}, ${address.administrative_area_level_1}`,
          addressLine3: `${address.country}`,
          postalCode: address.postal_code,
          countryIsoCode: 'USA',
          telephone: query.order.phone,
          email: 'notifications@incrediblephones.com',
        },
        shippingMethodId: Number(query.shipping?.remote_id) || null,
      },
      rows,
    };

    if (this.connection.company) {
      body.channelId = this.connection.company;
    }
    if (this.connection.order_status) {
      body.statusId = this.connection.order_status;
    }

    const sale = await this.fetch('/order-service/sales-order/', 'POST', body);

    await this.req.Models.Cart.update(
      {remote_id: sale.response, remote_source: 'brightpearl'},
      {
        where: {
          order_id: query.order.id,
          seller_id: query.products[0].seller_id,
        },
      },
    );

    return query;
  }

  async getSellerConnection() {
    if (this.connection) {
      return this.connection;
    }

    const connection = await this.req.Models.SellerConnection.findOne({
      where: {
        type: 'finale',
        account_name: this.accountCode,
      },
    });
    return connection;
  }

  async fetch(url, method = 'GET', body = null, retry = false) {
    const connection = await this.getSellerConnection();
    if (!connection) {
      this.req.Logger.error('Trying to call finale without a connection');
      return null;
    }

    if (!this.cookie) {
      await this.getAuthToken();
    }

    if (!this.cookie) {
      this.req.Logger.error('Trying to call finale without a cooke');
    }

    const fullurl = path.join(`${connection.api_domain}/${this.accountCode}/api`, url);

    const headers = {
      'Content-Type': 'application/json',
      cookie: this.cookie,
    };

    const opts = {
      headers,
      method,
    };

    if (body) {
      opts.body = JSON.stringify(body);
    }

    console.log(`https://${fullurl}`, opts);
    const results = await fetch(`https://${fullurl}`, opts);

    // If we need to refresh the token
    if (results.status === 401 && !retry) {
      await this.getAuthToken();
      return this.fetch(url, method, body, true);
    }

    const response = await results.json();
    console.log(JSON.stringify(response, null, 4));
    return response;
  }

  encode(str) {
    return Buffer.from(str).toString('base64');
  }
}

import Paypal from 'paypal-rest-sdk';
import Logger from '@Common/logger';
import request from 'request-promise';
import {formatAddressFirstLine, formatAddressSecondLine} from '@Common/utils/format-utils';

let token = null;

function getBaseUrl(env) {
  return env === 'live' ? 'https://api-m.paypal.com' : 'https://api-m.sandbox.paypal.com';
}

async function getToken(req) {
  if (token && token.expirationDate.getTime() > Date.now()) {
    return token.access_token;
  } else {
    const config = req.Config.get('social').paypal;
    const resp = await request({
      uri: `${getBaseUrl(config.env)}/v1/oauth2/token`,
      headers: {
        Accept: 'application/json',
        'Accept-Language': 'en_US',
        'content-type': 'application/x-www-form-urlencoded',
      },
      auth: {
        user: config.public,
        pass: config.secret,
        // 'sendImmediately': false
      },
      form: {
        grant_type: 'client_credentials',
      },
      method: 'POST',
      json: true,
    });
    const date = new Date();
    date.setTime(date.getTime() + resp.expires_in * 990);
    resp.expirationDate = date;
    token = resp;
    return token.access_token;
  }
}

async function makeRequest(options, req) {
  const config = req.Config.get('social').paypal;
  const access_token = await getToken(req);

  const opts = {
    ...options,
    uri: `${getBaseUrl(config.env)}${options.uri}`,
    headers: {
      ...(options.headers || {}),
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${access_token}`,
    },
    body: options.body,
    json: true,
  };

  Logger.info('paypal-request', opts);
  return request(opts);
}

export function getClient(req) {
  const server = req.Config.get('server');
  return {
    captureOrder: async ({id}) => {
      return makeRequest(
        {
          uri: `/v2/checkout/orders/${id}/capture`,
          method: 'POST',
        },
        req,
      );
    },
    getOrder: async ({id}) => {
      return makeRequest(
        {
          uri: `/v2/checkout/orders/${id}`,
          method: 'GET',
        },
        req,
      );
    },
    createOrder: async ({total, query, tempOrderId, address}) => {
      return makeRequest(
        {
          uri: '/v2/checkout/orders',
          method: 'POST',
          body: {
            intent: 'CAPTURE',
            purchase_units: [
              {
                amount: {
                  currency_code: 'USD',
                  value: total.toFixed(2),
                },
                shipping: {
                  name: {
                    full_name: `${query.firstName} ${query.lastName}`,
                  },
                  type: 'SHIPPING',
                  address: {
                    address_line_1: formatAddressFirstLine(address),
                    address_line_2: formatAddressSecondLine(address),
                    admin_area_2: address?.locality,
                    admin_area_1: address?.administrative_area_level_1,
                    country_code: 'US',
                    postal_code: address?.postal_code,
                  },
                },
              },
            ],
            payer: {
              email_address: query.email,
              name: {
                given_name: query.firstName,
                surname: query.lastName,
              },
            },
            application_context: {
              locale: 'en-US',
              brand_name: 'Incredible',
              landing_page: 'BILLING',
              shipping_preference: 'SET_PROVIDED_ADDRESS',
              user_action: 'PAY_NOW',
              cancel_url: `${server.protocol}://${server.domain}/cart?paypal_reject&tempOrderId=${tempOrderId}`,
              return_url: `${server.protocol}://${server.domain}/cart?paypal_accept&tempOrderId=${tempOrderId}`,
              //payment_method: {
              //  payer_selected: 'PAYPAL_PAY_LATER',
              //},
            },
          },
        },
        req,
      );
    },
  };
}

import request from 'request-promise';
import xml2js from 'xml2js';

let iphoneFeed = null;

export default async function getIphoneFeed() {
  if (!iphoneFeed) {
    const content = await request('https://revampelectronics.com/feed_iphone');

    const parser = new xml2js.Parser();
    const json = await parser.parseStringPromise(content);
    const models = new Set();
    const capacities = new Set();
    const carriers = new Set();
    const tree = json.xml.item.reduce((agg, it) => {
      const {
        model: model_array,
        capacity: capacity_array,
        carrier_name: carrier_array,
        new_price,
        good_price,
        fair_price,
        broken_price,
      } = it;

      const model = model_array?.[0] || null;
      const capacity = capacity_array?.[0] || null;
      const carrier = carrier_array?.[0] || null;

      if (!carrier || !model || !capacity || carrier === '-') {
        return agg;
      }

      if (!agg.hasOwnProperty(model)) {
        agg[model] = {};
        models.add(model);
      }
      if (!agg[model].hasOwnProperty(capacity)) {
        agg[model][capacity] = {};
        capacities.add(capacity);
      }
      if (!agg[model][capacity].hasOwnProperty(carrier)) {
        agg[model][capacity][carrier] = {};
        carriers.add(carrier);
      }

      agg[model][capacity][carrier] = {
        like_new: Number(new_price?.[0]) || null,
        good: Number(good_price?.[0]) || null,
        fair: Number(fair_price?.[0]) || null,
        broken: Number(broken_price?.[0]) || null,
      };

      return agg;
    }, {});

    iphoneFeed = {
      tree,
      models: Array.from(models).sort(),
      capacities: sortByCapacity(Array.from(capacities)),
      carriers: sortByCarrier(Array.from(carriers)),
      conditions: ['broken', 'fair', 'good', 'like_new'],
    };
  }

  return iphoneFeed;
}

export function sortByCapacity(capacities) {
  return capacities.sort((a, b) => parseInt(a) - parseInt(b));
}

export function sortByCarrier(carriers) {
  return carriers.sort((a, b) => CarriersOrder[a] - CarriersOrder[b]);
}

const CarriersOrder = {
  Unlocked: 1,
  'AT&T': 2,
  Sprint: 3,
  'T-Mobile': 4,
  Verizon: 5,
  Other: 6,
};

import {formatUri} from '@Common/utils/urls';
import QS from 'qs';
import fetch from 'node-fetch';
import path from 'path';
import {CommissionRate, CsrfTokenHeader, SellerConnectionStatus} from '@Common/constants/app';
import {getBrightpearlRedirectURI} from '@Common/utils/urls';
import {addDeliveryDate} from '@Common/utils/date-utils';
import {RequestError} from '@Common/constants/errors';
import {SellerCloudReturnReason} from '@Common/constants/seller-constants';
import {getShippingMethodDescriptionSeller} from '@Common/utils/product-utils';
import {getActualOrderTotals} from '@Common/utils/order-utils';
import {decimalNumber} from '@Common/utils/format-utils';
import moment from 'moment';
import {arrayToMap} from '@Common/utils/generic-utils';

export async function getClient(req, seller_id, connection) {
  if (!seller_id) {
    seller_id = req.seller_id;
  }
  if (!seller_id) {
    throw 'No seller_id in Sellercloud connection';
  }

  if (!connection) {
    connection = await req.Models.SellerConnection.get({
      type: 'sellercloud',
      seller_id,
    });
  }

  if (!connection) {
    throw 'No Sellercloud connection available';
  }

  return new SellercloudClient(req, seller_id, connection);
}

class SellercloudClient {
  constructor(req, seller_id, connection) {
    this.seller_id = seller_id;
    this.req = req;
    this.connection = connection;
    this.defaultPagination = 50;
    this.defaultQueryPagination = 10;
  }

  async getAuthToken() {
    const response = await fetch(`https://${this.connection.api_domain}/rest/api/token`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        Username: this.connection.account_name,
        Password: this.connection.token,
      }),
    });

    const resp = await response.json();
    this.token = resp.access_token;
    return this.token;
  }

  async getInventory({pageSize, pageNumber = 1}) {
    return this.fetch(
      `/inventory?pageSize=${pageSize || this.defaultPagination}&pageNumber=${pageNumber}`,
    );
  }

  async getInventoryById({id = []}) {
    return this.fetch(`/inventory?inventoryID=${id.toString()}`);
  }

  async getOrders({ids = []}) {
    const args = ids.map(id => `model.orderIDs=${id}`);
    return this.fetch(`/Orders?${args.join('&')}`);
  }

  async getOrder({remote_id}) {
    return this.fetch(`/Orders/${remote_id}`);
  }

  async postOrder({order, products, shipping}) {
    const address = await this.req.Models.Address.getPublicFields(order.address_id);
    const seller = await this.req.Models.Seller.findOne({where: {id: this.seller_id}, raw: true});

    addDeliveryDate(shipping, seller);

    const payload = {
      CustomerDetails: {
        Email: order.email,
        FirstName: order.firstName,
        LastName: order.lastName,

        IsWholesale: false,
      },
      OrderDetails: {
        CompanyID: this.connection.company,
        Channel: 6,
        OrderSourceOrderID: `incr-${order.id}`,
      },
      ShippingAddress: {
        FirstName: order.firstName,

        LastName: order.lastName,
        Country: address.country,
        City: address.locality,
        State: address.administrative_area_level_1,

        ZipCode: address.postal_code,
        Address: `${address.street_number} ${address.route}`,
        Address2: `${address.unit}`,
        Phone: order.phone,
      },
      BillingAddress: {
        FirstName: order.firstName,

        LastName: order.lastName,
        Country: address.country,
        City: address.locality,
        State: address.administrative_area_level_1,

        ZipCode: address.postal_code,
        Address: `${address.street_number} ${address.route}`,
        Address2: `${address.unit}`,
        Phone: order.phone,
      },
      Products: [],
      ShippingMethodDetails: {
        Carrier: shipping.carrier,
        ShippingMethod: getShippingMethodDescriptionSeller(shipping, this.req.t),
        ShippingFee: shipping.price * (1 + (order.tax_rate || 0)),
        RushOrder: shipping.price != 0,
        PromiseDate: shipping.eta,
      },
    };

    for (const product of products) {
      if (product.type === 'product') {
        const prod = await this.req.Models.Product.findOne({
          where: {id: product.product_id},
          attributes: ['source_id'],
          raw: true,
        });
        if (prod.source_id) {
          payload.Products.push({
            ProductID: prod.source_id,
            ReferenceID: product.product_id,
            SitePrice: product.price,
            Qty: product.quantity,
            LineTaxTotal: decimalNumber(product.price * (product.quantity || 1) * order.tax_rate),
            finalValueFee: decimalNumber(product.price * (product.quantity || 1) * CommissionRate),
          });
        }
      }
    }

    const sale = await this.fetch('/Orders', 'POST', payload);
    if (sale) {
      await this.req.Models.Cart.update(
        {remote_id: sale, remote_source: 'sellercloud'},
        {
          where: {
            order_id: order.id,
            seller_id: this.seller_id,
          },
        },
      );

      const {total} = getActualOrderTotals({...order, cart: products});
      await this.fetch(
        `/Orders/${sale}/ReceiveManualPayment`,
        'PUT',
        {
          Amount: decimalNumber(total),
          PaymentMethod: 4, // cash, according to https://developer.sellercloud.com/dev-article/payment-method/
          Notes: 'Payable by Incredible once shipped',
        },
        true,
      );

      return this.postNoteOrder({
        text: `Only add charger if added to the order as a SKU`,
        category: 0,
        remote_id: sale,
      });
    }

    return null;
  }

  async returnItemsInOrder(query) {
    const scOrder = await this.getOrder({remote_id: query.remote_id});
    const skus = await this.req.Models.Product.findAll({
      where: {
        id: query.products.map(p => p.product_id),
      },
      raw: true,
      attributes: ['source_id', 'id'],
    });
    // we need to map the source_id with the product ID coming from the remote order
    const skusMap = arrayToMap(skus, 'id');

    const toDelete = new Set();
    const toReturn = [];
    for (const product of query.products) {
      const scOrderRows = scOrder?.OrderItems;

      const foundRow = scOrderRows.find(row => {
        // must match id and quantity and not having been flagged for deletion before
        return (
          String(row?.ProductID) === skusMap[product.product_id]?.source_id &&
          Number(row?.Qty) === product.quantity &&
          !toDelete.has(row.ID)
        );
      });
      if (foundRow) {
        toDelete.add(foundRow.ID);
        toReturn.push({
          OrderItemID: foundRow.ID,
          QtyToReturn: product.quantity,
          ReasonID: SellerCloudReturnReason[query.returnOrder.reason],
          Description: query.returnOrder.explanation,
        });
      }
    }

    // Delete every found row
    if (toDelete.size) {
      const resp = await this.fetch(`/Rma`, 'POST', {
        OrderID: query.remote_id,
        OrderItems: toReturn,
      });

      if (resp?.ID) {
        await this.req.Models.Cart.update(
          {
            remote_return_id: resp.ID,
            remote_return_source: 'sellercloud',
          },
          {
            where: {
              id: query.products.map(p => p.id),
            },
          },
        );
      }
      return resp;
    }

    return null;
  }

  async cancelItemsInOrder(query) {
    const scOrder = await this.getOrder({remote_id: query.remote_id});
    const skus = await this.req.Models.Product.findAll({
      where: {
        id: query.products.map(p => p.product_id),
      },
      raw: true,
      attributes: ['source_id', 'id'],
    });
    // we need to map the source_id with the product ID coming from the remote order
    const skusMap = arrayToMap(skus, 'id');

    const toDelete = new Set();
    for (const product of query.products) {
      const scOrderRows = scOrder?.OrderItems;

      const foundRow = scOrderRows.find(row => {
        // must match id and quantity and not having been flagged for deletion before
        return (
          String(row?.ProductID) === skusMap[product.product_id]?.source_id &&
          Number(row?.Qty) === product.quantity &&
          !toDelete.has(row.ID)
        );
      });
      if (foundRow) {
        toDelete.add(foundRow.ID);
      }
    }

    // Delete every found row
    if (toDelete.size) {
      await this.fetch(`/Orders/${query.remote_id}/items`, 'DELETE', Array.from(toDelete));
    }

    return this.postNoteOrder({
      text: `Items have been cancelled.
Reason: ${query.returnOrder.reason}
Explanation: ${query.returnOrder.explanation}
`,
      category: 1,
      remote_id: query.remote_id,
    });
  }

  async cancelOrder(query) {
    await this.fetch(`/Orders/StatusCode`, 'PUT', {
      Status: -1, // cancelled
      Orders: [query.remote_id],
    });
    await this.postNoteOrder({
      text: `Order has been cancelled.
${query.returnOrder?.reason ? query.returnOrder?.reason + ': ' : ''}${
        query.returnOrder?.explanation || ''
      }`,
      remote_id: query.remote_id,
      category: 1,
    });
  }

  async postNoteOrder({text, category = 0, remote_id}) {
    /* Category
    0 = General , 1 = Customer_Instructions , 2 = Customer_Service_Note
    */

    return await this.fetch(
      `/Orders/${remote_id}/Notes`,
      'POST',
      {
        Message: text,
        Category: category,
      },
      true,
    );
  }

  async getSellerConnection() {
    if (this.connection) {
      return this.connection;
    }

    const connection = await this.req.Models.SellerConnection.get({
      where: {
        type: 'sellercloud',
        account_name: this.seller_id,
      },
    });
    return connection;
  }

  async fetch(url, method = 'GET', body = null, ignoreResponse = false) {
    const connection = await this.getSellerConnection();
    if (!connection) {
      this.req.Logger.error('Trying to call sellercloud without a connection');
      return null;
    }

    if (!this.token) {
      this.token = await this.getAuthToken();
    }

    const fullurl = path.join(`${connection.api_domain}/rest/api/`, url);

    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${this.token}`,
    };

    const opts = {
      headers,
      method,
    };

    if (body) {
      opts.body = JSON.stringify(body);
    }

    console.log(`https://${fullurl}`, opts);
    const results = await fetch(`https://${fullurl}`, opts);
    if (results.status === 401) {
      this.token = await this.getAuthToken();
      return this.fetch(url, method, body);
    }
    if (!ignoreResponse) {
      const response = await results.json();
      console.log(JSON.stringify(response, null, 4));
      return response;
    }
  }
}

import sgMail from '@sendgrid/mail';
import {encode} from '@Common/server/security/tokens';
import {UnsubscribeLinkPattern} from '@Common/constants/notifications';
import {asyncForEach} from '@Common/utils/async';

export const sendEmail = async (template, req) => {
  const {
    protocol,
    domain,
    session: {secret},
    sendEmails,
  } = req.Config.get('server');
  let {to, appId, text, html, bcc, ...other} = template || {};

  if (!to) {
    req.Logger.info('email-ignored-empty-address', template.to);
  }
  if (!sendEmails) {
    req.Logger.info('email-ignored-per-config', template.to);
    return;
  }

  const list = Array.isArray(to) ? to : [to];
  // Generate tokens for each email address
  return asyncForEach(list, async (email, i) => {
    // Ignore unsubscribed emails
    // TODO: figure out if we should ignore messages from an app specifically
    const isUnsubscribed = await req.Models.Unsubscribed.findOne({where: {email}});
    if (isUnsubscribed) {
      req.Logger.info('email-ignore-unsubscribed', email);
      return;
    }

    const unsubscribeToken = await encode({app: appId, email}, secret);
    const unsubscribeLink = `${protocol}://${domain}/email-unsubscribe?token=${unsubscribeToken}`;

    const data = {
      ...other,
      to: [email],
      text: text.replace(UnsubscribeLinkPattern, unsubscribeLink),
      html: html.replace(UnsubscribeLinkPattern, unsubscribeLink),
      headers: {
        'List-Unsubscribe': unsubscribeLink,
      },
    };
    if (i === 0 && bcc) {
      data.bcc = bcc;
    }
    req.Logger.info('email-sent', data.to, data.bcc);
    return sgMail.send(data);
  });
};

import {formatUri} from '@Common/utils/urls';
import QS from 'qs';
import fetch from 'node-fetch';
import path from 'path';
import {CsrfTokenHeader, SellerConnectionStatus} from '@Common/constants/app';
import {getBrightpearlRedirectURI} from '@Common/utils/urls';
import {addDeliveryDate} from '@Common/utils/date-utils';
import {RequestError} from '@Common/constants/errors';
import {ShipStationOrderStates} from '@Common/constants/seller-constants';
import {getShippingMethodDescriptionSeller} from '@Common/utils/product-utils';
import {
  decimalNumber,
  formatAddressFirstLine,
  formatAddressSecondLine,
} from '@Common/utils/format-utils';
import moment from 'moment';
import {arrayToMap} from '@Common/utils/generic-utils';
import {UserFilesCdnURL} from '@Common/utils/statif-assets-utils';

export async function getClient(req, seller_id, connection) {
  if (!seller_id) {
    seller_id = req.seller_id;
  }
  if (!seller_id) {
    throw 'No seller_id in ShipStation connection';
  }

  if (!connection) {
    connection = await req.Models.SellerConnection.get({
      type: 'shipstation',
      seller_id,
    });
  }

  if (!connection) {
    throw 'No ShipStation connection available';
  }

  return new ShipStationClient(req, seller_id, connection);
}

class ShipStationClient {
  constructor(req, seller_id, connection) {
    this.seller_id = seller_id;
    this.req = req;
    this.connection = connection;
    this.defaultPagination = 50;
    this.defaultQueryPagination = 10;
    this.orderPrefix = 'Incredible Order ';
    this.timezone = 'America/Los_Angeles';
  }

  async getAuthToken() {
    this.token = Buffer.from(`${this.connection.account_name}:${this.connection.token}`).toString(
      'base64',
    );
    return this.token;
  }

  async getOrders(query = {}) {
    //const args = ids.map(id => `model.orderIDs=${id}`);
    console.log('---', query);
    console.log('---', QS.stringify(query));
    return this.fetch(`/orders?${QS.stringify(query)}`); // ?${args.join('&')}
  }

  async getShipments(query) {
    return this.fetch(`/shipments?${QS.stringify(query)}`);
  }

  getIncredibleOrderId(str) {
    return Number((str || '').replace(this.orderPrefix, ''));
  }

  //async getOrder({remote_id}) {
  //  return this.fetch(`/Orders/${remote_id}`);
  //}

  formatDate(date) {
    return moment(date).tz(this.timezone).format('YYYY-MM-DD hh:mm:ss');
  }

  async postOrder({order, products, shipping, status = ShipStationOrderStates.awaiting_shipment}) {
    const address = await this.req.Models.Address.getPublicFields(order.address_id);
    const seller = await this.req.Models.Seller.findOne({where: {id: this.seller_id}, raw: true});

    addDeliveryDate(shipping, seller);

    const payload = {
      orderNumber: `${this.orderPrefix}${order.id}`,
      orderKey: order.id,
      orderDate: this.formatDate(new Date()),
      shipByDate: this.formatDate(shipping.eta),
      orderStatus: status,
      customerEmail: order.email,
      billTo: {
        name: `${order.firstName} ${order.lastName}`,
        street1: formatAddressFirstLine(address),
        street2: formatAddressSecondLine(address),
        country: 'US', //address.country two letters,
        city: address.locality,
        state: address.administrative_area_level_1,
        postalCode: address.postal_code,
        phone: order.phone,
      },
      shipTo: {
        name: `${order.firstName} ${order.lastName}`,
        street1: formatAddressFirstLine(address),
        street2: formatAddressSecondLine(address),
        country: 'US', //address.country two letters,
        city: address.locality,
        state: address.administrative_area_level_1,
        postalCode: address.postal_code,
        phone: order.phone,
      },
      amountPaid: order.total,
      taxAmount: decimalNumber(order.tax),
      shippingAmount: shipping.price,
      requestedShippingService: getShippingMethodDescriptionSeller(shipping, this.req.t),
      items: [],
    };

    for (const product of products) {
      if (product.type === 'product') {
        const prod = await this.req.Models.Product.findOne({
          where: {id: product.product_id},
          attributes: ['source_id'],
          raw: true,
        });
        if (prod.source_id) {
          payload.items.push({
            lineItemKey: product.id,
            sku: prod.source_id,
            name: product.description_seller,
            imageUrl: product.image ? UserFilesCdnURL(`150x150/${product.image}`) : '',
            unitPrice: product.price,
            quantity: product.quantity,
            taxAmount: decimalNumber(product.price * order.tax_rate),
            productId: product.product_id,
          });
        }
      }
    }

    const sale = await this.fetch('/orders/createorder', 'POST', payload);
    if (sale?.orderId) {
      await this.req.Models.Cart.update(
        {remote_id: sale.orderId, remote_source: 'shipstation'},
        {
          where: {
            order_id: order.id,
            seller_id: this.seller_id,
          },
        },
      );
    }

    return null;
  }

  async getSellerConnection() {
    if (this.connection) {
      return this.connection;
    }

    const connection = await this.req.Models.SellerConnection.get({
      where: {
        type: 'shipstation',
        account_name: this.seller_id,
      },
    });
    return connection;
  }

  async fetch(url, method = 'GET', body = null, ignoreResponse = false) {
    const connection = await this.getSellerConnection();
    if (!connection) {
      this.req.Logger.error('Trying to call ShipStation without a connection');
      return null;
    }

    if (!this.token) {
      this.token = await this.getAuthToken();
    }

    const fullurl = path.join(`${connection.api_domain}`, url);

    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Basic ${this.token}`,
    };

    const opts = {
      headers,
      method,
    };

    if (body) {
      opts.body = JSON.stringify(body);
    }

    console.log(`https://${fullurl}`, opts);
    const results = await fetch(`https://${fullurl}`, opts);

    if (!ignoreResponse) {
      const response = await results.json();
      console.log(JSON.stringify(response, null, 4));
      return response;
    }
  }
}

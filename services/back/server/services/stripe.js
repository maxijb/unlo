import Stripe from 'stripe';

let stripe = null;

export function getStripe(req) {
  if (!stripe) {
    stripe = Stripe(req.Config.get('social').stripe.secret);
  }
  return stripe;
}

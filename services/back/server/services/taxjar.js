import Taxjar from 'taxjar';

let client = null;

export const getClient = req => {
  if (!client) {
    client = new Taxjar({
      apiKey: req.Config.get('social').taxjar.apiKey,
    });
  }
  return client;
};

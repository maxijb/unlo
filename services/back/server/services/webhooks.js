import {formatUri} from '@Common/utils/urls';
import QS from 'qs';
import fetch from 'node-fetch';
import path from 'path';
import {CommissionRate, CsrfTokenHeader, SellerConnectionStatus} from '@Common/constants/app';
import {getBrightpearlRedirectURI} from '@Common/utils/urls';
import {addDeliveryDate} from '@Common/utils/date-utils';
import {RequestError} from '@Common/constants/errors';
import {SellerCloudReturnReason} from '@Common/constants/seller-constants';
import {getShippingMethodDescriptionSeller} from '@Common/utils/product-utils';
import {getActualOrderTotals} from '@Common/utils/order-utils';
import {decimalNumber} from '@Common/utils/format-utils';
import moment from 'moment';
import {arrayToMap} from '@Common/utils/generic-utils';
import {formatOrder} from '@Common/utils/seller-api-utils';

export async function getClient(req, seller_id, webhooks) {
  if (!seller_id) {
    seller_id = req.seller_id;
  }
  if (!seller_id) {
    throw 'No seller_id in Webhooks connection';
  }

  if (!webhooks) {
    const webhooksRaw = await req.Models.SellerWebhook.findAll({
      where: {seller_id},
      attributes: ['type', 'uri'],
      raw: true,
    });
    webhooks = arrayToMap(webhooksRaw, 'type');
  }

  if (!webhooks) {
    throw 'No webhooks available';
  }

  return new WebhookClient(req, seller_id, webhooks);
}

class WebhookClient {
  constructor(req, seller_id, webhooks) {
    this.seller_id = seller_id;
    this.req = req;
    this.webhooks = webhooks;
  }

  async postOrderCreate(order) {
    if (this.webhooks?.on_order_create?.uri) {
      const formatted = await this.getFormattedOrder(order);
      return this.fetch(this.webhooks?.on_order_create?.uri, 'POST', formatted);
    }
    return null;
  }

  async postOrderUpdate(order) {
    if (this.webhooks?.on_order_create?.uri) {
      const formatted = await this.getFormattedOrder(order);
      return this.fetch(this.webhooks?.on_order_update?.uri, 'POST', formatted);
    }
    return null;
  }

  async getFormattedOrder(order) {
    const {orders} = await this.req.Models.Order.list({
      ids: order.order?.id || order.order_id,
      seller_id: this.seller_id,
      isSeller: true,
      isApi: true,
    });
    if (!orders?.[0]) {
      throw new ApiError('ORDER_NOT_FOUND', 'The order was not found');
    }
    return formatOrder(orders[0], this.req, this.seller_id);
  }

  async fetch(url, method = 'GET', body = null, ignoreResponse = false) {
    const headers = {
      'Content-Type': 'application/json',
    };

    const opts = {
      headers,
      method,
    };

    if (body) {
      opts.body = JSON.stringify(body);
    }

    console.log(`https://${url}`, opts);
    return fetch(url, opts);
  }
}

import PublicAPI from './public-api';

const api = {
  health: {controller: 'ok'},
  app: {
    getStaticUrls: {
      controller: 'app.getStaticUrls',
    },
    getCarriers: {
      controller: 'app.getCarriers',
    },
    getTimezonesList: {
      controller: 'app.getTimezonesList',
      middleware: ['authorize'],
    },
    getMenuItems: {
      controller: 'app.getMenuItems',
    },
    findAllVisibleLeaves: {
      controller: 'app.findAllVisibleLeaves',
    },
    getHighlightedProducts: {
      controller: 'app.getHighlightedProducts',
    },
    getTradeInOptions: {
      controller: 'app.getTradeInOptions',
    },

    getTradeInPrediction: {
      controller: 'app.getTradeInPrediction',
    },
    getPublicKeys: {
      controller: 'app.getPublicKeys',
    },
  },
  forms: {
    contact: {
      controller: 'forms.contact',
    },
  },
  // ---------------------------------- REMOVE THESE 3 ------------------
  getId: {
    controller: 'getId',
    method: 'GET',
    path: 'get-generic-id',
  },
  v2: {
    getId: {
      controller: 'v2GetId',
    },
    getUser: {
      controller: 'v2GetUser',
      method: 'POST',
    },
  },
  // ---------------------------------- UNSUBSCRIBE ------------------
  emailNotifications: {
    unsubscribe: {
      controller: 'user.unsubscribeEmail',
    },
    reenable: {
      controller: 'user.reenableEmail',
    },
  },
  email: {
    testPreview: {
      controller: 'email.testPreview',
      middleware: ['isSuperAdmin'],
    },
  },
  // ---------------------------------- LOGIN AND USER ------------------
  user: {
    signup: {
      controller: 'user.signup',
      method: 'POST',
    },

    login: {
      controller: 'user.login',
      method: 'POST',
    },

    getUserDetails: {
      controller: 'user.getUserDetails',
      middleware: ['authorize'],
    },

    getAssets: {
      controller: 'user.getUserAssets',
      middleware: ['authorize'],
    },

    facebookLogin: {
      controller: 'user.facebookLogin',
      method: 'POST',
    },

    googleLogin: {
      controller: 'user.googleLogin',
      method: 'POST',
    },

    logout: {
      controller: 'user.logout',
      middleware: ['authorize'],
    },

    getUserOwnExtendedDetails: {
      controller: 'user.getUserOwnExtendedDetails',
      middleware: ['authorize'],
    },

    getUserOwnMinimumDetails: {
      controller: 'user.getUserOwnMinimumDetails',
      middleware: ['authorize'],
    },

    sendForgotPassword: {
      controller: 'user.sendForgotPassword',
    },

    validateForgotPasswordToken: {
      controller: 'user.validateForgotPasswordToken',
    },

    validateEmailToken: {
      controller: 'user.validateEmailToken',
    },

    resendValidationToken: {
      controller: 'user.resendValidationToken',
    },

    resetPassword: {
      controller: 'user.resetPassword',
      method: 'POST',
    },

    saveSettings: {
      controller: 'user.saveSettings',
      middleware: ['authorize'],
      method: 'POST',
    },

    createApp: {
      controller: 'user.createApp',
      middleware: ['authorize'],
      method: 'POST',
    },

    isSellerOrSuperAdmin: {
      controller: 'user.isSellerOrSuperAdmin',
      middleware: ['authorize'],
    },
    isSuperAdmin: {
      controller: 'user.isSuperAdmin',
      middleware: ['authorize'],
    },
    parseToken: {
      controller: 'user.parseToken',
    },
    trackActivity: {
      controller: 'user.trackActivity',
    },
    saveContact: {
      controller: 'user.saveContact',
      method: 'POST',
    },
  },

  // ---------------------------------- ABS ------------------
  ab: {
    getActiveAB: {
      controller: 'ab.getActiveAB',
    },
  },
  // ---------------------------------- PAYMENTS ------------------
  payments: {
    middleware: ['authorize'],

    performPayment: {
      controller: 'payments.performPayment',
    },
  },

  // ---------------------------------- UPLOAD FILE ------------------
  uploadFile: {
    middleware: ['authorize', 'baseSingleUpload', 'uploadFile'],
    controller: 'ok',
    method: 'POST',
  },
  uploadSellerCsv: {
    middleware: ['authorize', 'csvSingleUpload'],
    controller: 'crud.importSellerCSV',
    method: 'POST',
  },
  uploadCompetitorsCsv: {
    middleware: ['authorize', 'isSuperAdmin', 'csvSingleUpload'],
    controller: 'crud.importCompetitorsCSV',
    method: 'POST',
  },
  // ---------------------------------- REPORT ------------------
  report: {},
  // ---------------------------------- PRODUCT ------------------
  product: {
    getCategoryProducts: {
      controller: 'product.getCategoryProducts',
    },
    getCategory: {
      controller: 'product.getCategory',
    },
    relatedProducts: {
      controller: 'product.relatedProducts',
    },
    getAccessoryProducts: {
      controller: 'product.getAccessoryProducts',
    },
    getMinimumDetails: {
      controller: 'product.getMinimumDetails',
    },
    getMinimumPricePerCondition: {
      controller: 'product.getMinimumPricePerCondition',
    },
    getMinimumPricePerCarrier: {
      controller: 'product.getMinimumPricePerCarrier',
    },
    loadWithAttributes: {
      controller: 'product.loadWithAttributes',
    },
    getVersionWithAttributes: {
      controller: 'product.getVersionWithAttributes',
    },
    getCompetitorPrices: {
      controller: 'product.getCompetitorPrices',
    },
  },

  // ---------------------------------- CART ------------------
  cart: {
    applyPromoCode: {
      controller: 'cart.applyPromoCode',
      method: 'POST',
    },
    addForUser: {
      controller: 'cart.addForUser',
      method: 'POST',
    },
    readActiveForUser: {
      controller: 'cart.readActiveForUser',
    },
    changeQuantity: {
      controller: 'cart.changeQuantity',
      method: 'POST',
    },
    removeItem: {
      controller: 'cart.removeItem',
      method: 'POST',
    },
    getTaxRateByZipCode: {
      controller: 'cart.getTaxRateByZipCode',
      method: 'POST',
    },
    placeOrder: {
      controller: 'cart.placeOrder',
      method: 'POST',
    },
    isOrderMine: {
      controller: 'cart.isOrderMine',
    },
    setShipping: {
      controller: 'cart.setShipping',
      method: 'POST',
    },
    setTradeIn: {
      controller: 'cart.setTradeIn',
      method: 'POST',
    },
    getActiveTradeInForUser: {
      controller: 'cart.getActiveTradeInForUser',
    },
    getPaypalUrl: {
      controller: 'payments.getPaypalUrl',
      method: 'POST',
    },
    capturePaypalOrder: {
      controller: 'payments.capturePaypalOrder',
    },
    getPendingOrderDetails: {
      controller: 'cart.getPendingOrderDetails',
    },
    returnProducts: {
      controller: 'cart.returnProducts',
      middleware: ['authorize'],
    },
  },
  //attribute: {
  //  controller: 'attribute.loadAll',
  //},
  order: {
    listOrdersForUser: {
      controller: 'order.listOrdersForUser',
    },
    getReturnOrder: {
      controller: 'order.getReturnOrder',
      middleware: ['authorize', 'isSellerOrSuperAdmin'],
    },
    // TODO: remove
    orderHooks: {
      controller: 'cart.orderHooks',
      middleware: ['authorize', 'isSellerOrSuperAdmin'],
    },
  },
  // ---------------------------------- CRUD ------------------
  crud: {
    middleware: ['authorize', 'isSellerOrSuperAdmin'],
    sellersList: {
      controller: 'crud.sellersList',
      middleware: ['isSuperAdmin'],
    },
    billablesList: {
      controller: 'crud.billablesList',
    },
    setHooks: {
      controller: 'crud.setHooks',
      method: 'POST',
    },
    categoriesList: {
      controller: 'crud.categoriesList',
    },
    categoryGet: {
      controller: 'crud.categoryGet',
    },
    categoryImagesList: {
      controller: 'crud.categoryImagesList',
    },
    categoryImageCreate: {
      controller: 'crud.categoryImageCreate',
      method: 'POST',
      middleware: ['isSuperAdmin'],
    },
    attributesList: {
      controller: 'crud.attributesList',
    },
    categoryCreate: {
      controller: 'crud.categoryCreate',
      method: 'POST',
      middleware: ['isSuperAdmin'],
    },
    categoryUpdate: {
      controller: 'crud.categoryUpdate',
      method: 'POST',
      middleware: ['isSuperAdmin'],
    },
    categoryDelete: {
      controller: 'crud.categoryDelete',
      method: 'POST',
      middleware: ['isSuperAdmin'],
    },
    imagesList: {
      controller: 'crud.imagesList',
      middleware: ['isSuperAdmin'],
    },

    saveBillable: {
      controller: 'crud.saveBillable',
      method: 'POST',
      middleware: ['isSuperAdmin'],
    },
    categoryImageUpdate: {
      controller: 'crud.categoryImageUpdate',
      method: 'POST',
      middleware: ['isSuperAdmin'],
    },
    categoryImageDelete: {
      controller: 'crud.categoryImageDelete',
      method: 'POST',
      middleware: ['isSuperAdmin'],
    },
    ordersList: {
      controller: 'crud.ordersList',
    },
    addReturnLabel: {
      controller: 'crud.addReturnLabel',
      method: 'POST',
    },
    updateOrderStatus: {
      controller: 'crud.updateOrderStatus',
      method: 'POST',
      middleware: ['isSuperAdmin'],
    },
    saveTracking: {
      controller: 'crud.saveTracking',
      method: 'POST',
    },
    productsList: {
      controller: 'crud.productsList',
    },
    productCreate: {
      controller: 'crud.productCreate',
    },
    productDelete: {
      controller: 'crud.productDelete',
      method: 'POST',
    },
    productUpdate: {
      controller: 'crud.productUpdate',
      method: 'POST',
    },
    attributeProductCreate: {
      controller: 'crud.attributeProductCreate',
      method: 'POST',
      middleware: ['isSuperAdmin'],
    },
    attributeProductUpdate: {
      controller: 'crud.attributeProductUpdate',
      method: 'POST',
      middleware: ['isSuperAdmin'],
    },
    loadSellerNames: {
      controller: 'crud.loadSellerNames',
      middleware: ['isSuperAdmin'],
    },
    attributeProductDelete: {
      controller: 'crud.attributeProductDelete',
      method: 'POST',
    },
    cancelOrderBySeller: {
      controller: 'crud.cancelOrderBySeller',
      method: 'POST',
    },
    returnToSenderSeller: {
      controller: 'crud.returnToSenderSeller',
      method: 'POST',
    },
    getNewSellerToken: {
      controller: 'crud.getNewSellerToken',
      middleware: ['isSuperAdmin'],
    },
    sellerCreateProduct: {
      controller: 'crud.sellerCreateProduct',
      method: 'POST',
    },
    updateSeller: {
      controller: 'crud.updateSeller',
      method: 'POST',
    },
    getSeller: {
      controller: 'crud.getSeller',
    },
  },
  shipstation: {
    middleware: ['authorize', 'isSellerOrSuperAdmin'],
    createConnection: {
      controller: 'shipstation.createConnection',
    },
    getOrders: {
      controller: 'shipstation.getOrders',
    },
    getShipments: {
      controller: 'shipstation.getShipments',
    },
    syncOrders: {
      controller: 'shipstation.syncOrders',
    },
  },
  sellercloud: {
    middleware: ['authorize', 'isSellerOrSuperAdmin'],
    createConnection: {
      controller: 'sellercloud.createConnection',
      method: 'POST',
    },
    getInventory: {
      controller: 'sellercloud.getInventory',
    },
    syncInventory: {
      controller: 'sellercloud.syncInventory',
    },
    syncOrders: {
      controller: 'sellercloud.syncOrders',
    },
  },
  finale: {
    middleware: ['authorize', 'isSellerOrSuperAdmin'],
    createConnection: {
      controller: 'finale.createConnection',
    },
    authToken: {
      controller: 'finale.getAuthToken',
    },
    getProduct: {
      controller: 'finale.getProduct',
    },
    getProducts: {
      controller: 'finale.getProducts',
    },
    getProductStore: {
      controller: 'finale.getProductStore',
    },
    getProductConnection: {
      controller: 'finale.getProductConnection',
    },
    getInventory: {
      controller: 'finale.getInventory',
    },
  },
  brightpearl: {
    middleware: ['authorize', 'isSellerOrSuperAdmin'],
    createConnection: {
      controller: 'brightpearl.createConnection',
      method: 'POST',
    },
    activateConnection: {
      controller: 'brightpearl.activateConnection',
      method: 'POST',
    },
    getProduct: {
      controller: 'brightpearl.getProduct',
    },
    getPriceLists: {
      controller: 'brightpearl.getPriceLists',
    },
    getShippingMethods: {
      controller: 'brightpearl.getShippingMethods',
    },
    getProductPrice: {
      controller: 'brightpearl.getProductPrice',
    },
    getProductSearch: {
      controller: 'brightpearl.getProductSearch',
    },
    getWarehouses: {
      controller: 'brightpearl.getWarehouses',
    },
    getOrder: {
      controller: 'brightpearl.getOrder',
    },
    getProductAvailability: {
      controller: 'brightpearl.getProductAvailability',
    },
    syncInventory: {
      controller: 'brightpearl.syncInventory',
    },
    syncInventoryStock: {
      controller: 'brightpearl.syncInventoryStock',
    },
    syncInventoryPrice: {
      controller: 'brightpearl.syncInventoryPrice',
    },
    setInventoryWebhook: {
      controller: 'brightpearl.setInventoryWebhook',
    },
    listWebhooks: {
      controller: 'brightpearl.listWebhooks',
    },
    updateStock: {
      controller: 'brightpearl.updateStock',
    },
  },
  // ---------------------------------- PUBLIC ------------------
  public: {
    middleware: ['cors'],
    cron: {
      middleware: ['cronjob'],

      syncShipstationOrders: {
        controller: 'shipstation.cronSyncShipstationOrders',
      },
      syncSellercloudInventory: {
        controller: 'sellercloud.cronSyncSellercloudInventory',
      },
      syncBrightpearlPrices: {
        controller: 'brightpearl.cronSyncBrightpearlPrices',
      },
      syncBrightpearlOrders: {
        controller: 'brightpearl.cronSyncBrightpearlOrders',
      },
      notifyAbandonedCarts: {
        controller: 'app.notifyAbandonedCarts',
      },
      mapFacebookCatalog: {
        controller: 'app.mapFacebookCatalog',
      },
    },
    ...PublicAPI,
    getTradeInFeed: {
      controller: 'app.getTradeInFeed',
    },
    getInventoryXml: {
      controller: 'app.getInventoryXml',
    },
    getFacebookInventory: {
      controller: 'app.getFacebookInventory',
    },
    getSellerPaymentsCsv: {
      controller: 'report.getSellerPaymentsCsv',
      middleware: ['authorize', 'isSellerOrSuperAdmin'],
    },
    getSellerOrdersCsv: {
      controller: 'report.getSellerOrdersCsv',
      middleware: ['authorize', 'isSellerOrSuperAdmin'],
    },
    health: {controller: 'ok'},
    brightpearlInstall: {
      controller: 'brightpearl.install',
    },
    brightpearl: {
      inventoryHook: {
        controller: 'brightpearl.executeInventoryHook',
      },
      orderShippedHook: {
        controller: 'brightpearl.executeOrderShippedHook',
      },
    },
  },
};

export default api;

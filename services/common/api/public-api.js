export default {
  // Sellers API
  products: {
    controller: 'public.getProducts',
  },
  orders: {
    controller: 'public.getOrders',
  },
  getOrders: {
    controller: 'public.getOrdersRetailers',
  },
  createOrder: {
    controller: 'public.createOrder',
    method: 'POST',
  },
  updateProducts: {
    controller: 'public.updateProducts',
    method: 'POST',
  },
  updateOrderStatus: {
    controller: 'public.updateOrderStatus',
    method: 'POST',
  },
  log: {
    method: 'POST',
    controller: 'public.log',
  },
};

import {APP_PREFIX} from './app';

export const ABStatus = {
  notStarted: 'not-started',
  running: 'running',
  closed: 'closed',
  fullOn: 'full-on'
};

export const ABCookieName = `${APP_PREFIX}ab`;
export const ABForceHeaderName = 'x-force-ab-cookie';
export const ABRefreshPeriod = 10 * 1000;
export const ABSeparator = '/';
export const ABVariableSeparator = '|';

// TODO: move all secrets to an server-side only file
export const API_PREFIX = 'api';
export const PUBLIC_API_PREFIX = 'api/public';
export const APP_PREFIX = 'unlo-';
export const RPCActionPrefix = '@api/';
export const tokenCookieName = `${APP_PREFIX}token`;
export const appCookieName = `${APP_PREFIX}app`;
export const sessionCookieName = `${APP_PREFIX}sid`;
export const publicSessionCookieName = `${APP_PREFIX}psid`;
export const clientCookieName = `${APP_PREFIX}cid`;
export const publicIdCookieName = `${APP_PREFIX}pid`;
export const ContactCaptureCookieName = `${APP_PREFIX}contact-modal`;
export const sessionInactivityLimit = 60 * 30 * 1000;
export const GoogleMapsApiKey = 'AIzaSyBhzfZtKzUoDOdaL2bGjrBY0rC4wEQFo-k';
export const FACEBOOK_API_BASE_URL = 'https://graph.facebook.com/';
export const GOOGLE_API_BASE_URL = 'https://www.googleapis.com/';
export const StaticFilesFolder = 'static-files';
export const PRODUCT_ITEMS_PER_PAGE = 20;

export const TokenActions = {
  forgotPassword: 'forgotPassword',
};

export const Social = {
  facebook: 'facebook',
  google: 'google',
  twitter: 'twitter',
  linkedin: 'linkedin',
};

export const CsrfTokenURL = '/backend/csrf-token';
export const CsrfTokenHeader = 'x-csrf-token';
export const CsrfTokenRefreshMs = 60 * 29 * 1000;
export const CsrfTokenExpireTime = '30m';

// TODO: use cdn domain once set up
export const GravatarFallback = 'https://wiri.io/static/icons/default-user-icon.png';

export const CONTACT_ITEMS_PER_PAGE = 20;

export const AppColors = {
  dark: '#1e242b',
  subtext: '#65676b',
  negative: '#e05140',
  positive: '#2ccb92',
  black90: '#303030',
  black60: '#4A4A4A',
  white: '#ffffff',
  white10: '#fcfcfc',
  white20: '#eff0f3',
  white30: '#F4F4F4',
  white60: '#ADADAD',
  accent: '#4578db',
  wiriGray: '#CEDBDF',
  placeholder: '#8793a1',
};

export const MobileBreakpoint = 768;
export const TabletBreakpoint = 991;
export const WidgetWidth = 370;
export const DefaultCurrency = '$';
export const EmptyDropdownChoice = 'choose-option';

export const UnlockedCarrierId = 18;
export const UnlockedGSMCarrierId = 100;

export const VerizonCarrierId = 4;
export const ATTCarrierId = 1;
export const TMobileCarrierId = 2;
export const SprintCarrierId = 3;

// Carrier that are shown equally to seller and buyer
export const NoSpecialCarrier = [UnlockedCarrierId, UnlockedGSMCarrierId];

export const FallBackUnlocked = {
  'AT&T': [UnlockedCarrierId, UnlockedGSMCarrierId],
  'T-Mobile': [UnlockedCarrierId, UnlockedGSMCarrierId],
  Sprint: [UnlockedCarrierId],
  Verizon: [UnlockedCarrierId],
  Visible: [UnlockedCarrierId],
  'Cricket Wireless': [UnlockedCarrierId, UnlockedGSMCarrierId],
  Boost: [UnlockedCarrierId],
  'Mint Mobile': [UnlockedCarrierId, UnlockedGSMCarrierId],
  'Straight Talk': [UnlockedCarrierId],
  Metro: [UnlockedCarrierId, UnlockedGSMCarrierId],
  'Google Fi': [UnlockedCarrierId],
  'Consumer Cellular': [UnlockedCarrierId],
  'Xfinity Mobile': [UnlockedCarrierId],
};

export const MultiplyCarrierItems = {
  Unlocked: [
    'AT&T',
    'T-Mobile',
    'Sprint',
    'Verizon',

    'Visible',
    'Cricket Wireless',
    'Boost',
    'Mint Mobile',
    'Straight Talk',
    'Metro',
    'Google Fi',
    'Consumer Cellular',
    'Xfinity Mobile',
  ],

  'Unlocked GSM': ['AT&T', 'T-Mobile', 'Cricket Wireless', 'Mint Mobile', 'Metro'],
};

export const UnlockedSwatches = {
  [UnlockedCarrierId]: [
    'svg/ATT_logo.svg',
    'svg/tmobile.svg',
    'svg/sprint-nextel.svg',
    'svg/verizon.svg',
  ],
  [UnlockedGSMCarrierId]: ['svg/ATT_logo.svg', 'svg/tmobile.svg'],

  // uscelluar
  116: ['svg/us-cellular.svg'],
  // visible
  117: ['svg/visible.png', 'svg/verizon.svg'],
  // cricket
  118: ['svg/cricket.png', 'svg/ATT_logo.svg'],
  // boost
  119: [
    'svg/boost.svg',
    'svg/ATT_logo.svg',
    'svg/tmobile.svg',
    'svg/sprint-nextel.svg',
    'svg/verizon.svg',
  ],
  // mint
  120: ['svg/mint.svg', 'svg/tmobile.svg'],
  // straight talk
  121: [
    'svg/straight-talk.svg',
    'svg/ATT_logo.svg',
    'svg/tmobile.svg',
    'svg/sprint-nextel.svg',
    'svg/verizon.svg',
  ],
  // mint
  122: ['svg/metro.svg', 'svg/tmobile.svg'],
  // googleFi
  123: [
    'svg/google-fi.svg',
    'svg/ATT_logo.svg',
    'svg/tmobile.svg',
    'svg/sprint-nextel.svg',
    'svg/verizon.svg',
  ],
  // consumer cellular
  124: [
    'svg/consumer-cellular.svg',
    'svg/ATT_logo.svg',
    'svg/tmobile.svg',
    'svg/sprint-nextel.svg',
    'svg/verizon.svg',
  ],
  // xfinity
  117: ['svg/xfinity.svg', 'svg/verizon.svg'],
};

export const CartStatus = {
  active: 1,
  deleted: 2,
  inOrder: 3,
  inTransit: 4,
  delivered: 5,
  cancelledByUser: 6,
  cancelledBySeller: 7,
  cancelledByPlatform: 8,
  withProblems: 10,
  returnedToSenderSeller: 11,
  requestedReturn: 20,
  returnLabelSent: 21,
  inTransitReturn: 22,
  returnAwaitingRefund: 23,
  returnedSettled: 24,
  tradeInActive: -1,
  tradeInInOrder: -2,
  tradeInSentConfirmation: -3,
  tradeInSentLabel: -4,
  tradeInReceived: -5,
  tradeInPendingPayment: -6,
  tradeInPayed: -7,
};

export const CartStatusForApi = {
  [CartStatus.active]: 'ACTIVE',
  [CartStatus.deleted]: 'DELETED',
  [CartStatus.inOrder]: 'NEW_ORDER',
  [CartStatus.inTransit]: 'SHIPPED',
  [CartStatus.delivered]: 'DELIVERED',
  [CartStatus.cancelledByUser]: 'CANCELLED_BY_USER',
  [CartStatus.cancelledBySeller]: 'CANCELLED_BY_SELLER',
  [CartStatus.requestedReturn]: 'REQUESTED_RETURN',
  [CartStatus.returnLabelSent]: 'RETURN_LABEL_SENT',
  [CartStatus.inTransitReturn]: 'IN_TRANSIT_RETURNED',
  [CartStatus.returnAwaitingRefund]: 'RETURN_RECEIVED',
  [CartStatus.returnedSettled]: 'RETURN_SETTLED',
  [CartStatus.tradeInActive]: 'TRADE_IN_ACTIVE',
  [CartStatus.tradeInInOrder]: 'TRADE_IN_NEW_ORDER',
  [CartStatus.tradeInSentConfirmation]: 'TRADE_IN_SENT_CONFIRMATION',
  [CartStatus.tradeInSentLabel]: 'TRADE_IN_SENT_LABEL',
  [CartStatus.tradeInReceived]: 'TRADE_IN_RECEIVED',
  [CartStatus.tradeInPendingPayment]: 'TRADE_IN_PENDING_PAYMENT',
  [CartStatus.tradeInPayed]: 'TRADE_IN_PAYED',
  [CartStatus.withProblems]: 'WITH_PROBLEMS',
  [CartStatus.returnedToSenderSeller]: 'RETURNED_TO_SENDER',
};

export const AllowedReturnCartStatus = [
  CartStatus.inOrder,
  CartStatus.inTransit,
  CartStatus.delivered,
  CartStatus.withProblems,
];

export const CartStatusColor = {
  [CartStatus.active]: AppColors.white60,
  [CartStatus.deleted]: AppColors.white60,
  [CartStatus.inOrder]: AppColors.accent,
  [CartStatus.inTransit]: AppColors.positive,
  [CartStatus.delivered]: AppColors.white60,
  [CartStatus.cancelledByUser]: AppColors.negative,
  [CartStatus.cancelledBySeller]: AppColors.negative,
  [CartStatus.requestedReturn]: AppColors.accent,
  [CartStatus.returnLabelSent]: AppColors.negative,
  [CartStatus.inTransitReturn]: AppColors.negative,
  [CartStatus.returnAwaitingRefund]: AppColors.accent,
  [CartStatus.returnedSettled]: AppColors.white60,
  [CartStatus.tradeInActive]: AppColors.white60,
  [CartStatus.tradeInInOrder]: AppColors.white60,
  [CartStatus.tradeInSentConfirmation]: AppColors.accent,
  [CartStatus.tradeInSentLabel]: AppColors.white60,
  [CartStatus.tradeInReceived]: AppColors.accent,
  [CartStatus.tradeInPendingPayment]: AppColors.accent,
  [CartStatus.tradeInPayed]: AppColors.white60,
};

export const OrderVisibleStatus = {
  Active: [
    CartStatus.inOrder,
    CartStatus.inTransit,
    CartStatus.requestedReturn,
    CartStatus.requestedReturn,
    CartStatus.returnLabelSent,
    CartStatus.inTransitReturn,
    CartStatus.returnAwaitingRefund,
  ],
  'Being Returned': [
    CartStatus.requestedReturn,
    CartStatus.requestedReturn,
    CartStatus.returnLabelSent,
    CartStatus.inTransitReturn,
    CartStatus.returnAwaitingRefund,
  ],
  Settled: [
    CartStatus.delivered,
    CartStatus.returnedSettled,
    CartStatus.cancelledByUser,
    CartStatus.cancelledBySeller,
  ],
};

export const OrderStatus = {
  pending: -1,
  payed: 1,
  paymentDenied: 2,
  tradeInStarted: 3,
};

export const DefaultPerPage = 20;
export const DefaultFastShippingRate = 25.99;

export const ShippingMethod = {
  free5days: 'free5days',
  payed2days: 'payed2days',
  mixed: 'mixed',
};

export const DefaultShippingCharges = 0;

export const CartType = {
  tradeIn: 'trade-in',
  shipping: 'shipping',
  product: 'product',
};
export const OrderType = {
  tradeIn: 'trade-in',
  order: 'order',
  return: 'return',
};

export const Modals = {
  promoCode: 'promoCode',
  paypal: 'paypal',
  sellerDescription: 'sellerDescription',
  freeReturns: 'freeReturns',
  battery: 'battery',
  freeShipping: 'freeShipping',
  warranty: 'warranty',
  waterproofing: 'waterproofing',
  unbundling: 'unbundling',
  tested: 'tested',
  lowPrices: 'lowPrices',
  contact: 'contact',
};

export const InviteTokenActions = {
  newSeller: 'new-seller',
};

export const ProductSourceNames = {
  portalManual: 'seller_crud_ui',
  sellerCSVUpload: 'seller_csv_upload',
};

export const DefaultShippingMethod = {price: 0, speed_days: 5, id: -1};
export const DefaultShippingMethods = [
  DefaultShippingMethod,
  {price: DefaultFastShippingRate, speed_days: 2, id: -2},
];

export const AccessoriesCategories = [10, 11];

export const TrackingLinks = {
  FEDEX: 'https://www.fedex.com/fedextrack/?tracknumbers={{number}}',
  USPS: 'https://tools.usps.com/go/TrackConfirmAction?tLabels={{number}}',
  UPS: 'https://www.ups.com/track?tracknum={{number}}',
};

export const BillableTypes = {
  order: 'order',
  paymentToSeller: 'paymentToSeller',
  returnedToSender: 'returnedToSender',
};

export const CommissionRate = 0.1;

export const SellerConnectionStatus = {
  pending: 0,
  active: 1,
  disconnected: 2,
  failed: -1,
};

export const DropshippingSellerID = 1;
export const DropshippingMargin = 1.1;

export const Actors = {
  buyer: 'buyer',
  seller: 'seller',
};

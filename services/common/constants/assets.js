export const Entities = {
  user: 'user',
  team: 'team',
  campaign: 'campaign',
  app: 'app'
};

export const AssetHierarchy = {
  team: 1000,
  app: 100,
  campaign: 99,
  lang: 50
};

export const AssetStatus = {
  invited: 'invited',
  requested: 'requested',
  member: 'member'
};

export const AssetRole = {
  owner: 'owner',
  manager: 'manager',
  analyst: 'analyst',
  operator: 'operator',
  developer: 'developer'
};

export const AssetRoleHierarchy = {
  [AssetRole.owner]: 10,
  [AssetRole.manager]: 9,
  [AssetRole.analyst]: 7,
  [AssetRole.operator]: 3,
  [AssetRole.developer]: 4
};

export const AssetFieldsToBeExtracted = {
  timezone: 'timezone',
  avatar: 'avatar',
  name: 'name',
  website: 'website'
};

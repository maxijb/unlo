export const WaitBetweenMessages = 2000;
export const WaitTypes = {
  input: 'input'
};

export const LogicalOperators = {
  AND: '&&'
};

export const BotOperators = ['![]=', '[]=', '>=', '<=', '!=', '=', '>', '<', '!'];
export const ArrayOperators = {
  '![]=': true,
  '[]=': true
};

export const Competitors = {
  Amazon: {
    linkField: 'Amazon Link',
    source: 'amazon',
    priority: 1,
  },
  Apple: {
    linkField: 'Apple Link',
    source: 'apple',
    priority: 2,
  },
  'Best Buy': {
    linkField: 'Best Buy Link',
    source: 'bestbuy',
    priority: 3,
  },
  'Back Market': {
    linkField: 'Back Market Link',
    source: 'backmarket',
    priority: 7,
  },
  'Wal-Mart': {
    linkField: 'Wal-Mart Link',
    source: 'walmart',
    priority: 4,
  },
  Gazelle: {
    linkField: 'Gazelle Link',
    source: 'gazelle',
    priority: 5,
  },
  UpTrade: {
    source: 'uptrade',
    linkField: 'UpTrade Link',
    priority: 6,
  },
};

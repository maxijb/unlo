export const DayNumbers = [1, 2, 3, 4, 5, 6, 0];
export const HourNumbers = [
  0,
  1,
  2,
  3,
  4,
  5,
  6,
  7,
  8,
  9,
  10,
  11,
  12,
  13,
  14,
  15,
  16,
  17,
  18,
  19,
  20,
  21,
  22,
  23
];

export const DefaultOpenHours = [
  [7, 24],
  [7, 24],
  [7, 24],
  [7, 24],
  [7, 24],
  [7, 24],
  [7, 24]
];

export const Periods = ['month', 'week', 'day'];
export const CalendarColors = ['#f5a623', '#9380ff', '#50e3c2', '#fd0077'];

export const DayStatus = {
  closed: 'closed',
  open: 'open'
};

export const TimeUnitOrder = ['seconds', 'minutes', 'hours', 'days', 'weeks', 'months', 'years'];

export const UnitInMS = [
  1000,
  60 * 1000,
  60 * 60 * 1000,
  24 * 60 * 60 * 1000,
  7 * 24 * 60 * 60 * 1000,
  30 * 24 * 60 * 60 * 1000,
  365 * 24 * 60 * 60 * 1000,
  Infinity
];

export const TimeDirection = {
  forward: 'forward',
  past: 'past'
};

export const DayMs = 24 * 60 * 60 * 1000;

export const PeriodNumberDays = {
  month: 41,
  week: 7,
  day: 1
};

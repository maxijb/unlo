export const ErrorNames = {
  RequestError: 'RequestError',
  ApiError: 'ApiError',
  GenericError: 'GenericError',
  AuthenticationError: 'AuthenticationError',
};

export function ApiError(name = null, msg) {
  const err = Error.call(this, msg);
  err.name = name || ErrorNames.RequestError;
  return err;
}

export function RequestError(msg, data = null, name = null) {
  const err = Error.call(this, msg);
  err.name = name || ErrorNames.RequestError;
  err.data = data;
  return err;
}

export function AuthenticationError(msg, data = null) {
  const err = Error.call(this, msg);
  err.name = ErrorNames.AuthenticationError;
  err.data = data;
  return err;
}

export function formatError(error) {
  if (typeof error === 'string') {
    return {error};
  } else if (error.name && error.message) {
    return {error: error.name, message: error.message, data: error.data || error.stack};
  }

  return {
    error: error.message || error.name || error.toString() || 'Error',
    data: error.data,
  };
}

export const ProductErrorMessage = {
  outOfStock: 'outOfStock',
  missingAddress: 'missingAddress',
  missingAddressPostalCode: 'missingAddressPostalCode',
  missingShippingAddress: 'missingShippingAddress',
  missingShippingAddressPostalCode: 'missingShippingAddressPostalCode',
  missingEmail: 'missingEmail',
  missingFirstName: 'missingFirstName',
  missingLastName: 'missingLastName',
};

export const RequestErrorMessage = {
  unknownError: 'unknownError',
  validationError: 'validationError',
  missingUserId: 'missingUserId',
  userNotFound: 'userNotFound',
  userAlreadyValidated: 'userAlreadyValidated',
  missingStripeToken: 'missingStripeToken',
  missingPaymentAmount: 'missingPaymentAmount',
  paymentError: 'paymentError',
  missingInteractionId: 'missingInteractionId',
  invalidInteractionId: 'invalidInteractionId',
  missingTarget: 'missingTarget',
  targetNotFound: 'targetNotFound',
  productNotFound: 'productNotFound',
  nonMatchingAppId: 'nonMatchingAppId',
};

export const AuthenticationErrorMessage = {
  invalidToken: 'invalidToken',
  userNotFound: 'userNotFound',
  invalidFBToken: 'invalidFBToken',
  invalidFBUserId: 'invalidFBUserId',
  invalidUserId: 'invalidUserId',
  invalidGOOGLEToken: 'invalidGOOGLEToken',
  missingToken: 'missingToken',
  notMatchingOwner: 'notMatchingOwner',
  noAccessToAsset: 'noAccessToAsset',
};

export const PublicAPIErrorMessage = {
  missingTarget: 'missingTarget',
  targetNotFound: 'targetNotFound',
  userAndPidDontMatch: 'userAndPidDontMatch',
  missingInteractionToken: 'missingInteractionToken',
  wrongInteractionToken: 'wrongInteractionToken',
  invalidAssets: 'invalidAssets',
};

export const CartErrorMessage = {
  missingAddress: 'missingAddress',
  missingAddressPostalCode: 'missingAddressPostalCode',
  missingShippingAddress: 'missingShippingAddress',
  missingShippingAddressPostalCode: 'missingShippingAddressPostalCode',
  missingEmail: 'missingEmail',
  missingFirstName: 'missingFirstName',
  missingLastName: 'missingLastName',
  outOfStock: 'outOfStock',
  paypalRejected: 'paypalRejected',
};

export default {
  signup: {
    password: ['isRequired', 'validPassword'],
    email: ['isRequired', 'validEmail'],
  },
  forgotPassword: {
    email: ['isRequired', 'validEmail'],
  },
  resetPassword: {
    password: ['isRequired', 'validPassword'],
  },
  inviteTeammate: {
    role: ['isRequired'],
    email: ['isRequired', 'validEmail'],
  },
  contactPopup: {
    email: ['isRequired', 'validEmail'],
  },
  changeRole: {
    role: ['isRequired'],
    userId: ['isRequired'],
  },
  sendEmail: {
    message: ['isRequired'],
    subject: ['isRequired'],
    sendTo: ['isRequired', 'isMultiValidEmail'],
  },
  createBooking: {
    email: ['isRequired', 'isMultiValidEmail'],
    name: ['isRequired'],
    phone: ['isRequired'],
  },
  welcomeNameFormWithApp: {
    firstName: ['isRequired'],
    lastName: ['isRequired'],
  },
  createApp: {
    appName: ['isRequired'],
  },
  contact: {
    firstName: ['isRequired'],
    lastName: ['isRequired'],
    phone: ['isRequired'],
    email: ['isRequired', 'validEmail'],
    notes: ['isRequired'],
  },
  checkout: {
    email: ['isRequired', 'validEmail'],
    phone: ['isRequired'],
    firstName: ['isRequired'],
    lastName: ['isRequired'],
    paymentType: ['validPaymentType'],
    cardReady: ['validCard'],
    address: ['validGoogleAddress'],
  },
  newProductSellerServer: {
    stock: ['isRequired'],
    price: ['isRequired', 'moreThanZero'],
    capacity: ['isRequired'],
    color: ['isRequired'],
    carrier: ['isRequired'],
    appearance: ['isRequired'],
    category_id: ['isRequired'],
  },
  newProductSeller: {
    stock: ['isRequired'],
    price: ['isRequired', 'moreThanZero'],
    capacity: ['isRequired'],
    color: ['isRequired'],
    carrier: ['isRequired'],
    appearance: ['isRequired'],
  },
  businessSellerUpdate: {
    email: ['isRequired', 'validEmail'],
    name: ['isRequired'],
    phone: ['isRequired'],
    address: ['validGoogleAddress'],
  },
  shippingCommonSellerUpdate: {
    cut_off_time: ['isRequired'],
    cut_off_timezone: ['isRequired'],
  },
  shippingPolicySellerUpdate: {
    carrier: ['isRequired'],
    speed_days: ['isRequired'],
    price: ['isRequired'],
  },
  accessoriesSellerUpdate: {
    category_id: ['isRequired'],
    price: ['isRequired'],
  },
  addReturnLabel: {
    remote_return_label: ['isRequired', 'validUrl'],
  },
};

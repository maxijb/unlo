import path from 'path';
import {APP_PREFIX} from './app';

// Client-side, used in the browser
export const i18nOptions = {
  fallbackLng: 'en',
  load: 'languageOnly', // we only provide en, de -> no region specific locals like en-US, de-DE

  // have a common namespace used around the full app
  ns: ['common'],
  defaultNS: 'common',

  debug: false, // process.env.NODE_ENV !== 'production',
  saveMissing: true,

  interpolation: {
    escapeValue: false, // not needed for react!!
    formatSeparator: ',',
    format: (value, format, lng) => {
      if (format === 'uppercase') {
        return value.toUpperCase();
      }
      return value;
    }
  },
  detection: {
    order: ['cookie', 'querystring', 'localStorage', 'navigator'],
    lookupQuerystring: 'lang',
    lookupCookie: `${APP_PREFIX}lang`
  }
};

export const SupportedLanguages = {
  es: true,
  en: true,
  de: true
};

export const i18nBackendOptions = (config, env) => ({
  ...i18nOptions,
  preload: Object.keys(SupportedLanguages), // preload all languages
  ns: ['common', 'home', 'dashboard'], // need to preload all the namespaces
  defaultNS: 'common',
  backend: {
    loadPath: path.join(__dirname, '/../locales/{{lng}}/{{ns}}.json'),
    addPath: path.join(__dirname, '/../locales/{{lng}}/{{ns}}.missing.json')
  },
  detection: {
    order:
      env === 'front'
        ? ['custom-path', 'querystring', 'cookie', 'header']
        : ['querystring', 'cookie', 'header'],
    lookupCookie: `${APP_PREFIX}lang`,
    lookupQuerystring: 'lang',
    cookieDomain: config.domain,
    caches: ['cookie']
  }
});

export const CustomPathDetector = {
  name: 'custom-path',
  lookup: (req, res, options) => {
    const firstParam = req.originalUrl.split('/')[1];
    if (SupportedLanguages[firstParam]) {
      return firstParam;
    }
  }
};

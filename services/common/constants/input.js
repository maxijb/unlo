export const InputTypes = {
  rate: 'rate',
  survey: 'survey',
  chat: 'chat',
  contact: 'contact'
};

export const DefaultScale = 5;

export const UITypes = {
  rate: 'rate',
  bookingInvitation: 'booking-invitation',
  booking: 'booking',
  chatInvitation: 'chat-invitation',
  chat: 'chat',
  goBack: 'go-back-button',
  contact: 'contact',
  promotion: 'promotion',
  openHours: 'open-hours',
  options: 'options',
  captureInput: 'capture-input',
  welcome: 'welcome',
  menu: 'menu',
  footer: 'footer',
  mapInvitation: 'map-invitation',
  mapDisplay: 'map',
  map: 'map'
};

export const DefaultLang = 'en';
export const PRISTINE_FIELD = '!@!--PRISTINE--!@!';

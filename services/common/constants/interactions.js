export const ConversationStatus = {
  pending: 'pending',
  closed: 'closed',
  all: 'all'
};

export const LoadConversationItems = 20;

export const SegmentTypes = {
  tag: 'tag',
  segment: 'segment',
  label: 'label'
};

export const DefaultConversationSearchOptions = {
  search: '',
  startDate: '',
  endDate: '',
  claimed: '',
  status: ConversationStatus.pending
};

export const ClaimedByValue = {
  claimedByMeOrUnclaimed: 'claimedByMeOrUnclaimed',
  claimedByMe: 'claimedByMe',
  unclaimed: 'unclaimed',
  all: 'all'
};

export const ClaimedByFilterOptions = [
  'claimedByMeOrUnclaimed',
  'claimedByMe',
  'unclaimed',
  {name: 'all', id: ''}
];

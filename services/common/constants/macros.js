import isEmpty from 'is-empty';
import get from 'lodash.get';
import {UITypes} from './input';
import {OrderedHash} from '@Common/utils/ordered-hash';

// TODO: internationalize
export const Macros = {
  rate: {ui: UITypes.rate, display: 'Operator asked your satisfaction level'},
  contact: {ui: UITypes.contact, display: 'Operator asked contact information'},
  //map: {
  //  ui: UITypes.promotion,
  //  display: 'Operator showed location',
  //  config: {
  //    action: 'link',
  //    href: 'https://www.google.com/maps',
  //    template: 'image-right',
  //    title: 'Our location',
  //    image: 'https://developers.google.com/maps/images/lhimages/api/icon_placesapi.svg',
  //    text: 'Look for directions to opur restaurant.',
  //    button: 'Go to map'
  //  }
  //},
  hours: {
    ui: 'open-hours',
    display: 'Operator informed opening hours',
    config: {
      title: 'Our Opening Hours'
      //hours: [
      //  ['10', '23.30'],
      //  [],
      //  ['10', '15', '19', '23'],
      //  ['10', '15', '19', '23'],
      //  ['10', '15', '19', '23'],
      //  ['10', '15', '19', '23'],
      //  ['10', '23.30']
      //]
    }
  },
  options: {
    ui: 'options',
    display: 'Operator provided options',
    config: {
      options: ['Yes', 'No']
    }
  }
};

export const MessageMacros = [
  {
    shortcut: 'hi',
    message: 'Hello there!'
  }
];

export const MacroInitCharacter = '/';
export const MacroInitCharacterForRegExp = '/';

export const MacroMatchPatter = new RegExp(
  `(^|\\s)${MacroInitCharacterForRegExp}([a-z0-9]+)($|\\s)(.*)`,
  'i'
);

export const ScreenUIs = [
  {
    id: 'chat-invitation',
    shortcut: 'chat-invitation',
    ui: 'chat-invitation',
    standAlone: true,
    isMacro: false,
    actionName: 'chat-invitation',
    steps: [{ui: 'chat'}]
  },
  {
    id: 'booking-invitation',
    shortcut: 'booking-invitation',
    ui: 'booking-invitation',
    standAlone: true,
    isMacro: false,
    actionName: 'booking-invitation',
    steps: [{ui: 'booking'}]
  },
  {
    id: 'map',
    shortcut: 'map-invitation',
    ui: 'map-invitation',
    standAlone: true,
    isMacro: false
  }
];

export const ActionMacros = new OrderedHash([
  {
    id: 'map',
    shortcut: 'map',
    ui: 'map',
    condition: options => get(options, 'map.latitude') && get(options, 'map.longitude'),
    configurePage: '/dashboard/settings',
    standAlone: false
  },
  {
    id: 'rate',
    shortcut: 'rate',
    ui: 'rate',
    standAlone: true
  },
  {
    id: 'contact',
    shortcut: 'contact',
    ui: 'contact',
    standAlone: false
  },
  {
    id: 'menu',
    shortcut: 'menu',
    ui: 'menu',
    condition: options =>
      !isEmpty(get(options, 'menuList', null)) && options.menuList.some(m => m.isOn),
    configurePage: '/dashboard/settings/menu',
    standAlone: true
  },
  {
    id: 'booking',
    shortcut: 'booking',
    ui: 'booking',
    condition: options => !isEmpty(get(options, 'bookings.labels')),
    configurePage: '/dashboard/settings/reservations',
    standAlone: false
  },
  {
    id: 'hours',
    shortcut: 'hours',
    ui: 'open-hours',
    condition: options => !isEmpty(get(options, 'openHours')),
    configurePage: '/dashboard/settings/hours',
    standAlone: true
  }
]);

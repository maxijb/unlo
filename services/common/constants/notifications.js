import {SocketEvents} from './socket-status';

export const EventAlertsSettings = [SocketEvents.submitBooking, SocketEvents.message];

export const AlertHandlerType = {
  email: 'email'
};

export const UnsubscribeLinkPattern = '---unsubscribeLink---';

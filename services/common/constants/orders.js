export const ReturnReason = {
  changeMind: 'changeMind',
  issue: 'issue',
  'other/late': 'other/late',
};

export const FieldsUserContact = ['email', 'name', 'consent', 'extendedConsent', 'phone'];
export const FieldsTriggerNewIdentity = {email: true};
export const PublicAttributes = [...FieldsUserContact, ['publicId', 'id'], ['id', 'identityId']];
export const PublicIdPrefix = 'publicId--';

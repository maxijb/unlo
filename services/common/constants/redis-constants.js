export const RedisNamespaces = {
  socketsPid: 'PID',
  socketsMessages: 'MSG',
  userToSocket: 'TID',
  userAssetRole: 'ROLE',
  userNotified: 'UNOT',
  operatorsNotified: 'ONOT'
};

export function getRedisKey(namespace, id) {
  return `${namespace}-${id}`;
}

export function getRedisUserKey(target, pid, namespace = RedisNamespaces.userToSocket) {
  return getRedisKey(namespace, `${pid || ''}|app_${target.appId}`);
}

export function getRedisUserAssetKey(target, pid) {
  return getRedisKey(
    RedisNamespaces.userAssetRole,
    `${pid || ''}|app_${target.appId}${!target.campaignId ? '' : '|campaign_' + target.campaignId}`
  );
}

import {CartStatus} from './app';
import {ReturnReason} from './orders';

export const SellerHttpHeader = 'authorization';

export const ValidStatusTransitions = {
  [CartStatus.inOrder]: [
    CartStatus.inTransit,
    CartStatus.cancelledBySeller,
    CartStatus.withProblems,
  ],
  [CartStatus.inTransit]: [CartStatus.returnedToSenderSeller],
  [CartStatus.requestedReturn]: [
    CartStatus.returnLabelSent,
    CartStatus.returnAwaitingRefund,
    CartStatus.withProblems,
  ],
  [CartStatus.returnLabelSent]: [
    CartStatus.returnAwaitingRefund,
    CartStatus.withProblems,
    CartStatus.returnedSettled,
  ],
  [CartStatus.tradeInSentLabel]: [CartStatus.tradeInReceived, CartStatus.withProblems],
  [CartStatus.withProblems]: [
    CartStatus.inTransit,
    CartStatus.cancelledBySeller,
    CartStatus.returnLabelSent,
    CartStatus.returnAwaitingRefund,
    CartStatus.returnAwaitingRefund,
    CartStatus.tradeInReceived,
  ],
};

export const SellercloudOrderShippingStatus = {
  unknown: 0,
  unshipped: 1,
  partiallyShipped: 2,
  fullyShipped: 3,
  readyForPickup: 4,
};

export const SellercloudOrderStatusCode = {
  cancelled: -1,
  shoppingCart: 1,
  inProcess: 2,
  completed: 3,
  problemOrder: 100,
  onHold: 200,
  quote: 300,
  void: 999,
};

export const SellerCloudReturnReason = {
  [ReturnReason['changeMind']]: 8,
  [ReturnReason['issue']]: 1,
  [ReturnReason['other/late']]: 3,
};

export const ShipStationOrderStates = {
  awaiting_payment: 'awaiting_payment',
  awaiting_shipment: 'awaiting_shipment',
  pending_fulfillment: 'pending_fulfillment',
  shipped: 'shipped',
  on_hold: 'on_hold',
  cancelled: 'cancelled',
};

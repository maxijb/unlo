export const LoadingStates = {
  signup: 'signup',
  facebook: 'facebook',
  google: 'google',
  forgotPassword: 'forgotPassword',
  resetPassword: 'resetPassword',
};

export const GoogleAppId =
  '729969017022-3h5tdchae045u1dfbgfbu0m00qf4gh1h.apps.googleusercontent.com';
export const FacebookAppId = '700385727315414';
export const FacebookApiVersion = 'v3.1';

export const LoginActions = {
  signup: 'signup',
  signin: 'signin',
  logout: 'logout',
};

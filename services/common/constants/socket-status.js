import {getPageLink} from '@Common/utils/urls';

export const SocketStatus = {
  connect: 'new-client',
  disconnect: 'disconnect-client'
};

export const SocketEvents = {
  reconnect: 'reconnect',
  disconnecting: 'disconnecting',
  connection: 'connection',
  message: 'message',
  selectChatAsset: 'selectChatAsset',
  selectChatClient: 'selectChatClient',
  newInteraction: 'newInteraction',
  status: 'status',
  error: 'error',
  updateInteraction: 'updateInteraction',
  flagAsDone: 'flagAsDone',
  typing: 'typing',
  rateInChat: 'rateInChat',
  submitContactData: 'submitContactData',
  addDataToMessage: 'addDataToMessage',
  submitBooking: 'submitBooking',
  viewMessage: 'viewMessage',
  apiMethod: 'apiMethod',
  error: 'error'
};

export const EventWidgetHandler = {
  [SocketEvents.reconnect]: 'handleConnection',
  [SocketEvents.message]: 'handleMessage',
  [SocketEvents.typing]: 'handleTyping',
  [SocketEvents.viewMessage]: 'handleViewedMessage',
  [SocketEvents.addDataToMessage]: 'handleRemoteAddDataToMessage',
  [SocketEvents.submitContactData]: 'handleRemoteSubmitContactData',
  [SocketEvents.error]: 'handleErrors'
};

export const TempConversationId = 'TEMP_CONVERSATION_AWAITING_ID';

export const MessageStatus = {
  sending: 'sending',
  retrying: 'rettrying',
  failed: 'failed',
  received: 'received',
  read: 'read',
  viewed: 'viewed',
  incoming: 'incoming'
};

export const MessageOwners = {
  client: 'client',
  operator: 'operator',
  bot: 'bot'
};

/* When user is in these pages, notification are not triggered */
export const SocketEventsPage = {
  [SocketEvents.submitBooking]: getPageLink('bookings'),
  [SocketEvents.message]: getPageLink('chat')
};

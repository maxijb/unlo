import {APP_PREFIX} from './app';
export const widgetClientCookieName = `${APP_PREFIX}widget-cid`;

export const Namespaces = {
  client: '/widget',
  operator: '/operator'
};

export const WidgetViewportId = 'wiri-viewport-meta';
export const WidgetMobileHtmlClass = 'wiri-mobile-widget-active';

/* Client-side logger */
const logger = {
  log: (...params) => console.log(...params),
  dir: (...params) => console.warn(...params),
  warn: (...params) => console.warn(...params),
  error: (...params) => console.error(...params),
  info: (...params) => console.info(...params)
};

export default logger;

/* Server-side logger
 * Formats the log report and emits the proper log
 *
 * For errors it emits an error to GA
 * TODO: add winston to go to production
 */

export const reqLogger = req => {
  return {
    log: reqLog.bind(null, 'log', req),
    dir: reqLog.bind(null, 'dir', req),
    warn: reqLog.bind(null, 'warn', req),
    error: reqLog.bind(null, 'error', req),
    info: reqLog.bind(null, 'info', req)
  };

  /* Loggin format is:
    u: uow
    l: level
    n: log action name
    d: log action data
  */
  function reqLog(level, req, name, ...params) {
    const data = {u: req.uow, l: level};
    if (typeof name === 'string') {
      data.n = name;
      data.d = params;
    } else if (name && name.name) {
      data.n = name.name;
      data.d = [name, ...params];
    }

    if (level === 'error' && req.GA) {
      req.GA.exception(name.name || name);
    }

    logger[level](data);
  }
};

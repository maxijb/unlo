import {
  sessionCookieName,
  publicSessionCookieName,
  clientCookieName,
  sessionInactivityLimit,
} from '../../constants/app';
import {checkAuth} from '../security/auth';
import {reqLogger} from '../../logger';
import ABTracker from '../../utils/ab-tracker';
import {createUow} from '@Common/utils/requests';

import GA from '../wrapped-ga';

export default {
  preAuthorize(req, res, next) {
    return checkAuth(req)
      .then(auth => {
        req.auth = auth;
        next();
      })
      .catch(e => {
        req.authError = e;
        next();
      });
  },

  /*
   * Ininiatilizes logger server side with
   */
  addLogger(req, res, next) {
    req.Logger = reqLogger(req);
    next();
  },

  /* Common operations for all requests
   *
   * Adds an ID for the:
   * current request (req.uow) server side only, so it's not persisted to a cookie
   * current session (req.sessionId) created (if noavailable) and persisted to cookie in all cases (maxAge 30 min)
   * user's persisting Id (req.clientId) persisted to cookie only once (never expires). It's equal to the user's first session
   *
   * Initializes GA in the server
   *
   * Don't serialize req when sending data to the client
   */
  initRequest(req, res, next) {
    req.toJSON = () => null;

    // init cookies for non-public requests
    if (!req.isPublic) {
      const cookie = req.cookies[sessionCookieName];
      const cookieClient = req.cookies[clientCookieName];

      req.sessionId = cookie || createSessionId(req);
      req.clientId = cookieClient || req.sessionId;
      req.uow = createUow(req.sessionId);
      // socket requests do not have this
      if (res) {
        res.cookie(sessionCookieName, req.sessionId, {maxAge: sessionInactivityLimit});
        if (!cookieClient) {
          res.cookie(clientCookieName, req.sessionId);
        }
      }
    }

    const {GATrackingId} = req.Config.get('server');
    // Google analityic in the server
    req.GA = GA(req, GATrackingId, req.clientId, {
      strictCidFormat: false,
      uid: req.auth ? req.auth.id : null,
    });

    req.ab = new ABTracker(req.activeAB, req.GA);

    // req.GA.event({category: "ABtest", action: "track", label: "test1", value: 1});

    // don't serialize req when sending data to the client
    next();
  },
};

/* Generate a sessionId */
export function createSessionId(req) {
  // SessionId format:
  // $timestamp.u$userId.$randonNumberUpTo5Digits
  return `s${new Date().getTime()}.${Math.ceil(Math.random() * 100000)}-u${
    req.auth ? req.auth.id : 0
  }-`;
}

/* Generate a clientId */
export function createClientId(req) {
  // ClientId format:
  // c$timestamp.$randonNumberUpTo5Digits
  return `c${new Date().getTime()}.${Math.ceil(Math.random() * 100000)}`;
}

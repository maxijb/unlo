import Logger from '@Common/logger';

const isDev = process.env.NODE_ENV !== 'production';

process.on('unhandledRejection', error => {
  Logger.error('unhandledRejection', error.message, error.stack);
  if (!isDev) {
    process.exit(1);
  }
});

process.on('uncaughtException', error => {
  Logger.error('uncaughtException', error.message, error.stack);
  if (!isDev) {
    process.exit(1);
  }
});

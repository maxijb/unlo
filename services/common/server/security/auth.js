import {tokenCookieName, CsrfTokenExpireTime} from '../../constants/app';
import {decode, encode} from './tokens';

/* Checks the JWT available in the request's cookies
 * and validates it.
 * If it's missing of is invalid, it will reject the promise
 */
export function checkAuth(req) {
  const token = req.cookies[tokenCookieName];
  const secret = req.Config.get('server').session.secret;
  return checkAuthToken(token, secret);
}

export function checkAuthToken(token, secret) {
  return new Promise((resolve, reject) => {
    if (!token) {
      const err = {error: 'not_authorized', message: 'missing_token'};
      reject(err);
    } else {
      decode(token, secret)
        .then(auth => resolve(auth))
        .catch(error => {
          const err = {error: error.name, message: error.message};
          reject(err);
        });
    }
  });
}

export async function getCsrfToken(req, res) {
  const token = await createCsrfToken(req);
  res.send({token});
}

export async function createCsrfToken(req, userId) {
  const id = typeof userId !== 'undefined' ? userId : req.auth ? req.auth.id : null;
  const code = Math.random();
  const secret = req.Config.get('server').session.secret;
  const token = await encode({id, code}, secret, {expiresIn: CsrfTokenExpireTime});
  return token;
}

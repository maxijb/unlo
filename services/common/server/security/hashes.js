import CryptoJS from 'crypto-js';

export function getHashWithKey(str, key) {
  const hash = CryptoJS.HmacSHA1(str, key);
  return CryptoJS.enc.Hex.stringify(hash);
}

import jwt from 'jsonwebtoken';

export function encode(payload, secret, options = {expiresIn: '7d'}) {
  if (typeof secret !== 'string' && secret && secret.Config) {
    secret = secret.Config.get('server').session.secret;
  }
  return new Promise((resolve, reject) => {
    jwt.sign(payload, secret, options, (err, token) => {
      if (err) {
        reject(err);
      } else {
        resolve(token);
      }
    });
  });
}

export function decode(token, secret, options = {}) {
  // TODO: find a better place to put this
  if (process.env.NODE_ENV === 'test') {
    options.ignoreExpiration = true;
  }
  if (typeof secret !== 'string' && secret && secret.Config) {
    secret = secret.Config.get('server').session.secret;
  }
  return new Promise((resolve, reject) => {
    jwt.verify(token, secret, options, (err, payload) => {
      if (err) {
        reject(err);
      } else {
        resolve(payload);
      }
    });
  });
}

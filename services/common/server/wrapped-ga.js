import originalGA from 'universal-analytics';

const Wrapped = function (req, ...params) {
  const ga = originalGA(...params);
  return new UniversalGA(ga, req);
};

export default Wrapped;

/* Wrapps universal-analytics to make an API compatible with React-GA */
class UniversalGA {
  constructor(ga, req) {
    this.ga = ga;
    this.req = req;
  }

  event({category, action, label, value, callback}) {
    this.ga.event(
      {
        ec: category,
        ea: action,
        el: label,
        ev: value,
        p: this.req.originalUrl
      },
      callback
    );
  }

  exception({description, fatal, callback}) {
    this.ga.exception(description, fatal, callback);
  }

  pageview(path) {
    this.ga.pageview(path);
  }

  send() {
    this.ga.send();
  }
}

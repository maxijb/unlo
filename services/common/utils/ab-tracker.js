import {getWindowURL} from './urls';

export default class abTracker {
  constructor(active, ga) {
    this.ga = ga;
    this.active = active || {};
    this.tracked = {};
  }

  track(name) {
    const variant = this.active[name] || 1;
    if (!this.tracked[name] && this.active[name] && this.ga) {
      this.tracked[name] = true;
      this.ga.event({
        category: 'ABtest',
        action: `${name}|${variant}`,
        label: name,
        value: variant
      });
    }
    return variant;
  }

  variant(name) {
    return this.active[name] || 1;
  }
}

import Seq from 'sequelize';
// For now  we'll only allow lists of IDs
// 1,2,3,4,5
export const idSetToWhere = idset => {
  if (!idset) {
    return [];
  }

  const parts = idset.split(',');

  return parts;
};

import {idSetToWhere} from './api-utils';
import Seq from 'sequelize';

describe('idSetToWhere', () => {
  it('cases', () => {
    expect(idSetToWhere()).toEqual([]);
    expect(idSetToWhere('1,2')).toEqual([1, 2]);
    expect(idSetToWhere('1,2,3-5,8')).toEqual({
      [Seq.Op.or]: [[1, 2, 8], {[Seq.Op.gte]: 3, [Seq.Op.lte]: 5}],
    });
    expect(idSetToWhere('1,2,3-5,8,10-13')).toEqual({
      [Seq.Op.or]: [
        [1, 2, 8],
        {[Seq.Op.gte]: 3, [Seq.Op.lte]: 5},
        {[Seq.Op.gte]: 10, [Seq.Op.lte]: 13},
      ],
    });
  });
});

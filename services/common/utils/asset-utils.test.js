import {
  sortAssetsByHierarchy,
  uniqueAssets,
  consolidateSettings,
  categorizeAssets
} from './assets-utils';

describe('sortAssetsByHierarchy function', () => {
  it('Sorts by hierarchy, second by id, and finally by empty', () => {
    const input = [
      null,
      {},
      {type: 'campaign', id: 8},
      {type: 'campaign', id: 4},
      {type: 'team'},
      {type: 'app', id: 3}
    ];

    const output = [
      {type: 'team'},
      {type: 'app', id: 3},
      {type: 'campaign', id: 8},
      {type: 'campaign', id: 4},
      {},
      null
    ];

    expect(sortAssetsByHierarchy(input)).toEqual(output);
  });
});

describe('uniqueAssets function', () => {
  const input = [
    {assetType: 'app', assetId: 1},
    {assetType: 'app', assetId: 1},
    {assetType: 'app', assetId: 2},
    {assetType: 'campaign', assetId: 2},
    {assetType: 'campaign', assetId: 2},
    {assetType: 'campaign', assetId: 3},
    {assetType: 'app', assetId: 1}
  ];

  it('filters out repeated assets without adding key', () => {
    const output = [
      {assetType: 'app', assetId: 1},
      {assetType: 'app', assetId: 2},
      {assetType: 'campaign', assetId: 2},
      {assetType: 'campaign', assetId: 3}
    ];
    expect(uniqueAssets(input)).toEqual(output);
  });

  it('filters out repeated assets and adds key', () => {
    const output = [
      {key: 'app--1', assetType: 'app', assetId: 1},
      {key: 'app--2', assetType: 'app', assetId: 2},
      {key: 'campaign--2', assetType: 'campaign', assetId: 2},
      {key: 'campaign--3', assetType: 'campaign', assetId: 3}
    ];
    expect(uniqueAssets(input, true)).toEqual(output);
  });
});

describe('categorizeAssets function', () => {
  const input = [
    {assetType: 'app', assetId: 2},
    {assetType: 'team', assetId: 5},
    {assetType: 'campaign', assetId: 2}
  ];

  it('categorizes teams and the rest separately', () => {
    const output = {
      assets: [
        {assetType: 'app', assetId: 2},
        {assetType: 'campaign', assetId: 2}
      ],
      teams: [{assetType: 'team', assetId: 5}]
    };
    expect(categorizeAssets(input)).toEqual(output);
  });
});

describe('consolidateSettings function', () => {
  it('consolidate settings from assets', () => {
    const input = [
      {options: '{"test": 3, "duo": true}'},
      {options: '{"test": 7, "element": false}'},
      null,
      {},
      {options: ''},
      {options: {g: ['maxi']}}
    ];

    const output = {
      test: 7,
      duo: true,
      element: false,
      g: ['maxi']
    };

    expect(consolidateSettings(input)).toEqual(output);
  });
});

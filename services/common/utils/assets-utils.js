import {AssetHierarchy, Entities} from '../constants/assets';
import {safeJsonParse} from './json';

export function uniqueAssets(assets, shouldAddKey = false) {
  const keys = {};
  return assets.filter(asset => {
    const key = getAssetKey(asset);
    if (keys.hasOwnProperty(key)) {
      return false;
    }
    if (shouldAddKey) {
      asset.key = key;
    }
    keys[key] = true;
    return true;
  });
}

export function getAssetKey(asset) {
  let type = asset.assetType || asset.type;
  if (!type) {
    type = asset.campaignId ? 'campaign' : 'app';
  }
  const id = asset.assetId || asset.id || asset.campaignId || asset.appId;

  const pure = `${type}--${id}`;
  return !asset.lang ? pure : `${pure}--${asset.lang}`;
}

export function deserializeKey(key) {
  const [type, id] = key.split('--');
  return {type, id: Number(id)};
}

export function sortAssetsByHierarchy(assets) {
  return assets.sort((a, b) => {
    if (!a) {
      return 1;
    }

    if (!b) {
      return -1;
    }

    if (!a.type) {
      return 1;
    }

    if (!b.type) {
      return -1;
    }

    const typeComparison = (AssetHierarchy[b.type] || 0) - (AssetHierarchy[a.type] || 0);
    if (typeComparison !== 0) {
      return typeComparison;
    }

    return (b.id || 0) - (a.id || 0);
  });
}

export function consolidateSettings(assets) {
  return assets.reduce((prev, item) => {
    if (!item) {
      return prev;
    }
    const settings = safeJsonParse(item.options);
    return {
      ...prev,
      ...settings
    };
  }, {});
}

/* Gets the target based on appId and campaignId
 * @query appId {integer}
 * @query campaignId {integer}
 */
export function getTargets(query) {
  const targets = [];
  if (query.appId) {
    targets.push({type: 'app', id: query.appId});
  }
  if (query.campaignId) {
    targets.push({type: 'campaign', id: query.campaignId});
  }
  return targets;
}

export const categorizeAssets = allAssets => {
  const assets = [];
  const teams = [];
  allAssets.forEach(asset => {
    if ((asset.assetType || asset.type) === Entities.team) {
      teams.push(asset);
    } else {
      assets.push(asset);
    }
  });
  return {assets, teams};
};

export const formatUserAppRel = (user, app) => ({
  ownerType: Entities.user,
  ownerId: user,
  assetType: Entities.app,
  assetId: app
});

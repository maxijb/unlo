export async function asyncForEach(array, callback) {
  return new Promise(async (resolve, reject) => {
    for (let index = 0; index < array.length; index++) {
      try {
        await callback(array[index], index, array);
      } catch (e) {
        console.error(e);
        reject(e);
      }
    }
    resolve();
  });
}

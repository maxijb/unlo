import get from 'lodash.get';
import Defaults from '@Widget/config/defaults';
import {
  getEmbeddedDateString,
  absDateAndTimeToEmbedded,
  utcToEmbeddedTZ,
  embeddedToAbsUTC,
  addMinutes
} from '@Common/utils/date-utils';

// calculate min and max-guests
export const getGuestsRange = options => {
  const min = get(options, ['bookings', 'minValue'], 1);
  const max = get(options, ['bookings', 'maxValue'], 12);

  const valueRange = [];
  for (let i = min; i <= max; i++) {
    valueRange.push(i);
  }
  return valueRange;
};

export const processBookingDates = (booking, options) => {
  const {timezone, bookings} = options;
  const date = new Date(`${getEmbeddedDateString(booking.startDate)} UTC`);
  const {time} = booking;
  const duration = get(bookings, 'duration', Defaults.defaultBookingDuration);

  const startDate = absDateAndTimeToEmbedded(date, time, timezone);
  const endDate = utcToEmbeddedTZ(addMinutes(startDate, duration), timezone);

  return {
    ...booking,
    startDate,
    endDate,
    absStartDate: embeddedToAbsUTC(startDate),
    absEndDate: embeddedToAbsUTC(endDate)
  };
};

import get from 'lodash.get';
import isEmpty from 'is-empty';
import {BotOperators, ArrayOperators, LogicalOperators} from '../constants/bot';
import {MessageOwners} from '@Common/constants/socket-status';

export const getNextScriptIndex = (script, index) => {
  const newIndex = [...index];

  if (!newIndex.length) {
    return [0];
  }
  // in case we're dealing wiht an unresolved IF
  // render the first element of the selected branch
  const last = newIndex.pop();
  if (isNaN(last)) {
    newIndex.push(last, 0);
  } else {
    newIndex.push(last + 1);
  }

  // if adding an element cannot find anything
  // go back to the previous step recursively
  let shouldContinue = true;
  const num = 0;
  do {
    const nextElement = get(script, newIndex);
    if ((!nextElement || isNaN(newIndex[newIndex.length - 1])) && newIndex.length > 1) {
      newIndex.pop();
      // if the parent is a number increase it
      if (!isNaN(newIndex[newIndex.length - 1])) {
        newIndex[newIndex.length - 1] += 1;
      }
    } else {
      shouldContinue = false;
    }
  } while (shouldContinue);

  return newIndex;
};

export const evaluateBotMessage = (element = {}, newIndex, previousMessage, context) => {
  if (!element) {
    return newIndex;
  }

  if (element.hasOwnProperty('if')) {
    const condition = parseBotIfRule(element.if, previousMessage.data, context);
    const path = condition ? 'affirmative' : 'negative';
    if (element.hasOwnProperty(path)) {
      newIndex.push(path);
    }
  } else if (element.hasOwnProperty('switch')) {
    const activeCase = parseBotSwitchRule(
      element.switch,
      element.cases || {},
      previousMessage.data,
      context
    );
    newIndex.push('cases', activeCase);
  }

  return newIndex;
};

export const parseBotIfRule = (baseRule, data, context) => {
  if (typeof baseRule !== 'string' || !baseRule) {
    return false;
  }

  const rules = baseRule.split(LogicalOperators.AND);

  return rules.every(rule => {
    // find the first operator
    const operator = findRuleOperator(rule, 'bool');

    // get the variable name (base) and the value to compare it to
    // "!" operator is alway this format "!var" (at the start of the rule)
    // all others are "var=89" so the operator is in the middle
    const [base, compare] = operator === '!' ? [rule.substr(1), null] : rule.split(operator);

    // value in data
    const source = base.indexOf('.') === -1 ? data : context;
    const value = get(source, base);

    return evaluateRule(operator, value, compare);
  });
};

export const parseBotSwitchRule = (variable, cases, data = {}, context) => {
  if (typeof variable === 'string') {
    for (const condition of Object.keys(cases || {})) {
      if (condition !== 'default') {
        const operator = findRuleOperator(condition, '=');
        const compare = condition.replace(operator, '');

        const source = variable.indexOf('.') === -1 ? data : context;
        const value = get(source, variable);
        if (evaluateRule(operator, value, compare)) {
          return condition;
        }
      }
    }
  }
  return 'default';
};

export const getLength = obj => {
  if (!obj) {
    return 0;
  } else if (Array.isArray(obj)) {
    return obj.length;
  } else if (typeof obj === 'object') {
    return Object.keys(obj).length;
  } else if (!isNaN(obj)) {
    return Number(obj);
  }
  return obj.length;
};

export const evaluateRule = (operator, value, compare) => {
  // if one or boths are numbers we make sure that we'll compare then as numbers
  if (ArrayOperators.hasOwnProperty(operator)) {
    value = getLength(value);
    compare = getLength(compare);
  } else if (!isNaN(value) && !isNaN(compare)) {
    value = Number(value);
    compare = Number(compare);
  } else {
    value = (value || '').toString().toLowerCase().trim();
    compare = (compare || '').toString().toLowerCase().trim();
  }
  switch (operator) {
    case '![]=':
      return value != compare;
    case '!=':
      return value != compare;
    case '[]=':
      return value == compare;
    case '=':
      return value == compare;
    case '>=':
      return value >= compare;
    case '<=':
      return value <= compare;
    case '<':
      return value < compare;
    case '>':
      return value > compare;
    case '!':
      return !value;
  }
  return Boolean(value);
};

/* Finds the first operator in the rule
 * @param rule {string}
 * @param fallback {string} default operator
 * @return {string}
 */
export const findRuleOperator = (rule, fallback = 'bool') => {
  if (typeof rule !== 'string') {
    return fallback;
  }

  let operator = fallback;
  let minIndex = Infinity;
  BotOperators.forEach(op => {
    const index = rule.indexOf(op);
    if (index != -1 && index < minIndex) {
      operator = op;
      minIndex = index;
    }
  });
  return operator;
};

// TODO: they should use the conditional flag
export const isConditionMessage = message => {
  return (
    message &&
    (message.hasOwnProperty('if') ||
      message.hasOwnProperty('switch') ||
      message.hasOwnProperty('conditional'))
  );
};

export const findStepByName = (script, name) => {
  if (isEmpty(script)) {
    return null;
  }
  if (!name) {
    return null;
  }
  for (let i = 0, len = script.length; i < len; i++) {
    if (script[i].name === name) {
      return [i];
    }
  }
  return null;
};

export const userHasContact = user => {
  return Boolean(get(user, 'email', false));
};

export const resumeConversation = (messages, script) => {
  try {
    let scriptStep = [0];
    // TODO: make this recursive, so loops can be in other places than the root level
    const defaultOptions = script.reduce((prev, item) => {
      return item.name && item.options && item.loop ? {...prev, [item.name]: item.options} : prev;
    }, {});
    const previouslySelected = {};
    const msgs = messages.items();

    // content to evaluate conditions
    const context = {
      previouslySelected,
      defaultOptions,
      messages
    };

    // walk every message
    for (let [i, msg] of msgs.entries()) {
      // if we find a not bot message we just cancel
      if (msg.owner !== MessageOwners.bot) {
        return {scriptStep: false, defaultOptions, previouslySelected};
      }

      // --------------------- if this message is an option loop
      // --------------------- we update the selected options
      if (get(msg, 'message.name') && get(msg, 'message.loop') && get(msg, 'data.selected')) {
        const name = msg.message.name;
        const selected = msg.data.selected;
        // add to previouslySelected
        if (!previouslySelected.hasOwnProperty(name)) {
          previouslySelected[name] = [selected];
        } else {
          previouslySelected[name].push(selected);
        }
        // remove from defaultOptions
        const indexToRemove = (defaultOptions[name] || []).indexOf(selected);
        if (indexToRemove !== -1) {
          defaultOptions[name].splice(indexToRemove, 1);
        }
      }

      let current;
      do {
        // accumulate previouslySelected before finding next
        current = get(script, scriptStep);
        if (!current) {
          throw new Error('invalid script');
        }

        // if this a goto
        if (
          current.hasOwnProperty('goto') &&
          (!current.while || parseBotIfRule(current.while, msgs[i - 1].data, context))
        ) {
          const name = current.goto;
          const gotoIndex = findStepByName(script, name);
          if (gotoIndex !== null) {
            scriptStep = gotoIndex;
            current = get(script, scriptStep) || {};
          }
        }

        // Evaluate conditional messages
        scriptStep = evaluateBotMessage(current, scriptStep, msgs[i - 1], {
          previouslySelected,
          defaultOptions,
          messages
        });

        // get next step
        scriptStep = getNextScriptIndex(script, scriptStep);
        current = get(script, scriptStep);
        if (!current) {
          throw new Error('invalid script');
        }

        // we repeat steps if wait is found
        // or if the last index is 0
        // we always keep the pointer at the next element so we iterate to tuen 0 into 1
      } while (current.hasOwnProperty('wait') || scriptStep[scriptStep.length - 1] === 0);
    }

    return {
      scriptStep,
      defaultOptions,
      previouslySelected
    };
  } catch (e) {
    return {scriptStep: false, defaultOptions: {}, previouslySelected: {}};
  }
};

/*



    */

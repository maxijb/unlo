import {
  getNextScriptIndex,
  parseBotIfRule,
  evaluateBotMessage,
  evaluateRule,
  findRuleOperator,
  parseBotSwitchRule,
  findStepByName,
  getLength,
  resumeConversation
} from './bot-utils';

import {OrderedHash} from '@Common/utils/ordered-hash';

describe('getNextScriptIndex util', () => {
  it('empty index', () => {
    expect(getNextScriptIndex([], [])).toEqual([0]);
  });

  it('single number is increased by one', () => {
    expect(getNextScriptIndex(['hey', 'bye'], [0])).toEqual([1]);
  });

  it('single number is increased by one even if there is not an item at that index', () => {
    expect(getNextScriptIndex(['hey', 'bye'], [2])).toEqual([3]);
  });

  it('if a logic statement is just mentioned go tot first step', () => {
    const script = ['hey', {if: 'selected=gre', affirmative: ['yes', 'sure yes']}, 'bye'];
    const index = [1, 'affirmative'];
    expect(getNextScriptIndex(script, index)).toEqual([1, 'affirmative', 0]);
  });

  it('if a logic statement is started continue it', () => {
    const script = ['hey', {if: 'selected=gre', affirmative: ['yes', 'sure yes']}, 'bye'];
    const index = [1, 'affirmative', 0];
    expect(getNextScriptIndex(script, index)).toEqual([1, 'affirmative', 1]);
  });

  it('if a logic statement finises, go back to parent', () => {
    const script = ['hey', {if: 'selected=gre', affirmative: ['yes', 'sure yes']}, 'bye'];
    const index = [1, 'affirmative', 1];
    expect(getNextScriptIndex(script, index)).toEqual([2]);
  });

  it('nested logic starting statement', () => {
    const script = [
      'hey',
      {
        if: 'selected=gre',
        affirmative: [
          'yes',
          {
            if: 'selected=gre',
            affirmative: ['ohh yeah']
          },
          'sure yes'
        ]
      },
      'bye'
    ];
    const index = [1, 'affirmative', 1, 'affirmative'];
    expect(getNextScriptIndex(script, index)).toEqual([1, 'affirmative', 1, 'affirmative', 0]);
  });

  it('nested logic finishing statement', () => {
    const script = [
      'hey',
      {
        if: 'selected=gre',
        affirmative: [
          'yes',
          {
            if: 'selected=gre',
            affirmative: ['ohh yeah']
          },
          'sure yes'
        ]
      },
      'bye'
    ];
    const index = [1, 'affirmative', 1, 'affirmative', 0];
    expect(getNextScriptIndex(script, index)).toEqual([1, 'affirmative', 2]);
  });

  it('nested logic finishing 2 statements and jumping levels', () => {
    const script = [
      'hey',
      {
        if: 'selected=gre',
        affirmative: [
          'yes',
          {
            if: 'selected=gre',
            affirmative: ['ohh yeah']
          }
        ]
      },
      'bye'
    ];
    const index = [1, 'affirmative', 1, 'affirmative', 0];
    expect(getNextScriptIndex(script, index)).toEqual([2]);
  });

  it('switch logic starting', () => {
    const script = [
      'hey',
      {
        switch: 'selected',
        cases: {
          yes: ['you selected yes']
        }
      },
      'bye'
    ];
    const index = [1, 'cases', 'yes'];
    expect(getNextScriptIndex(script, index)).toEqual([1, 'cases', 'yes', 0]);
  });

  it('switch logic finishing', () => {
    const script = [
      'hey',
      {
        switch: 'selected',
        cases: {
          yes: ['you selected yes']
        }
      },
      'bye'
    ];
    const index = [1, 'cases', 'yes', 0];
    expect(getNextScriptIndex(script, index)).toEqual([2]);
  });
});

describe('parseBotIfRule util', () => {
  it('empty rule object', () => {
    expect(parseBotIfRule(null, null)).toEqual(false);
  });

  it('empty string object', () => {
    expect(parseBotIfRule('', null)).toEqual(false);
  });

  it('equal not found', () => {
    expect(parseBotIfRule('selected=89', null)).toEqual(false);
  });

  it('equal found', () => {
    expect(parseBotIfRule('selected=89', {selected: 89})).toEqual(true);
  });

  it('bool found', () => {
    expect(parseBotIfRule('selected', {selected: 89})).toEqual(true);
  });

  it('bool found but false', () => {
    expect(parseBotIfRule('selected', {selected: false})).toEqual(false);
  });

  it('bool not found', () => {
    expect(parseBotIfRule('selected', {})).toEqual(false);
  });

  it('bool with null data', () => {
    expect(parseBotIfRule('selected', null)).toEqual(false);
  });

  it('greater than', () => {
    expect(parseBotIfRule('selected>89', {selected: 100})).toEqual(true);
  });

  it('greater than not matched', () => {
    expect(parseBotIfRule('selected>89', {selected: 34})).toEqual(false);
  });

  it('lower than', () => {
    expect(parseBotIfRule('selected<89', {selected: 10})).toEqual(true);
  });

  it('lower than not matched', () => {
    expect(parseBotIfRule('selected<89', {selected: 134})).toEqual(false);
  });

  it('greater or equal than (equal)', () => {
    expect(parseBotIfRule('selected>=89', {selected: 89})).toEqual(true);
  });

  it('greater or equal than', () => {
    expect(parseBotIfRule('selected>=89', {selected: 90})).toEqual(true);
  });

  it('greater or equal not matched', () => {
    expect(parseBotIfRule('selected>=89', {selected: 34})).toEqual(false);
  });

  it('lower or equal than (equal)', () => {
    expect(parseBotIfRule('selected<=89', {selected: 89})).toEqual(true);
  });

  it('lower or equal than', () => {
    expect(parseBotIfRule('selected<=89', {selected: 90})).toEqual(false);
  });

  it('lower or equal not matched', () => {
    expect(parseBotIfRule('selected<=89', {selected: 34})).toEqual(true);
  });

  it('not equal matched', () => {
    expect(parseBotIfRule('selected!=89', {selected: 34})).toEqual(true);
  });

  it('not equal not matched', () => {
    expect(parseBotIfRule('selected!=89', {selected: 89})).toEqual(false);
  });

  it('options equal matched', () => {
    expect(parseBotIfRule('options.selected=89', {}, {options: {selected: 89}})).toEqual(true);
  });

  it('options NOT equal matched', () => {
    expect(parseBotIfRule('options.selected!=890', {}, {options: {selected: 89}})).toEqual(true);
  });

  it('options greater than equal not matched', () => {
    expect(parseBotIfRule('options.selected>8', {}, {options: {selected: 1}})).toEqual(false);
  });

  it('options not-bool matched', () => {
    expect(parseBotIfRule('!options.skip', {}, {options: {selected: 1}})).toEqual(true);
    expect(parseBotIfRule('!options.skip', {}, {options: {skip: false}})).toEqual(true);
    expect(parseBotIfRule('!options.skip', {}, {options: {skip: 0}})).toEqual(true);
  });

  it('options not-bool NOT matched', () => {
    expect(parseBotIfRule('!options.skip', {}, {options: {skip: 1}})).toEqual(false);
    expect(parseBotIfRule('!options.skip', {}, {options: {skip: true}})).toEqual(false);
    expect(parseBotIfRule('!options.skip', {}, {options: {skip: 'icj'}})).toEqual(false);
  });
});

describe('evaluateRule utils', () => {
  it('default operator (bool)', () => {
    expect(evaluateRule(null, 3, null)).toEqual(true);
    expect(evaluateRule(null, null, null)).toEqual(false);
  });

  it('bool operator', () => {
    expect(evaluateRule('bool', 3, null)).toEqual(true);
    expect(evaluateRule('bool', null, null)).toEqual(false);
  });

  it('not-bool operator', () => {
    expect(evaluateRule('!', 3, null)).toEqual(false);
    expect(evaluateRule('!', null, null)).toEqual(true);
    expect(evaluateRule('!', false, null)).toEqual(true);
    expect(evaluateRule('!', 0, null)).toEqual(true);
  });

  it('equal operator', () => {
    expect(evaluateRule('=', 3, 3)).toEqual(true);
    expect(evaluateRule('=', 43, 3)).toEqual(false);
  });

  it('equal operator strings, case insensitiver and trim()', () => {
    expect(evaluateRule('=', 'test', 'test')).toEqual(true);
    expect(evaluateRule('=', 'time', '    TimE    ')).toEqual(true);
    expect(evaluateRule('=', 'time', 'TimEA')).toEqual(false);
  });

  it('equal lenght operator', () => {
    expect(evaluateRule('[]=', [], [1])).toEqual(false);
    expect(evaluateRule('[]=', [2], [2])).toEqual(true);
    expect(evaluateRule('[]=', [2, 2], [2, 2])).toEqual(true);
    expect(evaluateRule('[]=', {a: 2, b: 2}, [2, 2])).toEqual(true);
    expect(evaluateRule('[]=', {a: 2, b: 2}, 2)).toEqual(true);
    expect(evaluateRule('[]=', {a: 2, b: 2, c: 3}, 3)).toEqual(true);
  });

  it('equal lenght operator with mixes inptu', () => {
    expect(evaluateRule('[]=', null, [1])).toEqual(false);
    expect(evaluateRule('[]=', 1, [2])).toEqual(true);
    expect(evaluateRule('[]=', [2, 2], '2')).toEqual(true);
  });

  it('not equal lenght operator', () => {
    expect(evaluateRule('![]=', [], [1])).toEqual(true);
    expect(evaluateRule('![]=', [2], [2])).toEqual(false);
    expect(evaluateRule('![]=', [2, 2], [2, 2])).toEqual(false);
    expect(evaluateRule('![]=', {a: 2, b: 2}, [2, 2])).toEqual(false);
    expect(evaluateRule('![]=', {a: 2, b: 2}, 1)).toEqual(true);
  });

  it('not equal lenght operator with mixes inptu', () => {
    expect(evaluateRule('![]=', null, [1])).toEqual(true);
    expect(evaluateRule('![]=', 1, [2])).toEqual(false);
    expect(evaluateRule('![]=', [2, 2], '2')).toEqual(false);
  });

  it('not equal operator', () => {
    expect(evaluateRule('!=', 43, 3)).toEqual(true);
    expect(evaluateRule('!=', 3, 3)).toEqual(false);
  });

  it('equal operator different types', () => {
    expect(evaluateRule('=', 3, '3')).toEqual(true);
  });

  it('greater and lower than with different types (numbers are treated as numbers', () => {
    expect(evaluateRule('>', 103, '23')).toEqual(true);
    expect(evaluateRule('<', 9, '23')).toEqual(true);
    expect(evaluateRule('<=', 9, '23')).toEqual(true);
    expect(evaluateRule('>=', 103, '23')).toEqual(true);
  });

  it('greater than operator', () => {
    expect(evaluateRule('>', 3, 1)).toEqual(true);
    expect(evaluateRule('>', 3, 4)).toEqual(false);
  });

  it('greater or equal operator', () => {
    expect(evaluateRule('>=', 3, 3)).toEqual(true);
    expect(evaluateRule('>=', 5, 3)).toEqual(true);
    expect(evaluateRule('>=', 5, 10)).toEqual(false);
  });

  it('lower than operator', () => {
    expect(evaluateRule('<', 3, 10)).toEqual(true);
    expect(evaluateRule('<', 3, 1)).toEqual(false);
  });

  it('lower or equal operator', () => {
    expect(evaluateRule('<=', 3, 3)).toEqual(true);
    expect(evaluateRule('<=', 1, 3)).toEqual(true);
    expect(evaluateRule('<=', 5, 1)).toEqual(false);
  });
});

describe('evaluateBotMessage util', () => {
  it('with plain string messages nothing changes', () => {
    expect(evaluateBotMessage('hola', [1], null)).toEqual([1]);
  });

  it('with non-condition object message nothing changes', () => {
    expect(evaluateBotMessage({ui: 'contact'}, [1], null)).toEqual([1]);
  });

  it('with IF and matched bool condition affirmative is added if affirmative is there', () => {
    const prev = {data: {selected: 34}};
    expect(evaluateBotMessage({if: 'selected', affirmative: []}, [1], prev)).toEqual([
      1,
      'affirmative'
    ]);
  });

  it('with an AND condition affirmative is selected', () => {
    const prev = {data: {selected: 34}};
    expect(evaluateBotMessage({if: 'selected&&selected>23', affirmative: []}, [1], prev)).toEqual([
      1,
      'affirmative'
    ]);
  });

  it('with an AND condition not matched negative is selected', () => {
    const prev = {data: {selected: 34}};
    expect(
      evaluateBotMessage({if: 'selected&&selected>123', affirmative: [], negative: []}, [1], prev)
    ).toEqual([1, 'negative']);
  });

  it('with an AND condition including context', () => {
    const prev = {data: {selected: 34}};
    const context = {conversation: {ready: true}};
    expect(
      evaluateBotMessage(
        {if: 'selected&&conversation.ready', affirmative: [], negative: []},
        [1],
        prev,
        context
      )
    ).toEqual([1, 'affirmative']);
  });

  it('with an AND condition including context equal', () => {
    const prev = {data: {selected: 34}};
    const context = {conversation: {items: [1, 2]}};
    expect(
      evaluateBotMessage(
        {if: 'selected>2&&conversation.items[]=2', affirmative: [], negative: []},
        [1],
        prev,
        context
      )
    ).toEqual([1, 'affirmative']);
  });

  it('with an AND condition including a negative bool context', () => {
    const prev = {data: {selected: 34}};
    const context = {conversation: {items: [1, 2], skip: false}};
    expect(
      evaluateBotMessage(
        {if: 'selected>2&&!conversation.skip', affirmative: [], negative: []},
        [1],
        prev,
        context
      )
    ).toEqual([1, 'affirmative']);
  });

  it('with an AND condition including a negative bool context', () => {
    const prev = {data: {selected: 34}};
    const context = {conversation: {items: [1, 2]}};
    expect(
      evaluateBotMessage(
        {if: 'selected>2&&conversation.skip', affirmative: [], negative: []},
        [1],
        prev,
        context
      )
    ).toEqual([1, 'negative']);
  });

  it('with IF and matched bool condition affirmative is NOT added if its not defined affirmative', () => {
    const prev = {data: {selected: 34}};
    expect(evaluateBotMessage({if: 'selected'}, [1], prev)).toEqual([1]);
  });

  it('with IF and NOT matched bool condition negative is added if defined', () => {
    const prev = {data: {NOTselected: 34}};
    expect(evaluateBotMessage({if: 'selected', negative: []}, [1], prev)).toEqual([1, 'negative']);
  });

  it('with IF and matched equal condition affirmative is NOT added if its not defined affirmative', () => {
    const prev = {data: {selected: 34}};
    expect(evaluateBotMessage({if: 'selected=34'}, [1], prev)).toEqual([1]);
  });

  it('with IF and matched equal condition affirmative is added when its defined', () => {
    const prev = {data: {selected: 34}};
    expect(evaluateBotMessage({if: 'selected=34', affirmative: []}, [1], prev)).toEqual([
      1,
      'affirmative'
    ]);
  });

  it('with SWITCH and matched equal condition', () => {
    const prev = {data: {selected: 34}};
    const element = {
      switch: 'selected',
      cases: {
        '=34': []
      }
    };
    expect(evaluateBotMessage(element, [1], prev)).toEqual([1, 'cases', '=34']);
  });

  it('with SWITCH and unmatched equal condition', () => {
    const prev = {data: {selected: 'none'}};
    const element = {
      switch: 'selected',
      cases: {
        '=34': []
      }
    };
    expect(evaluateBotMessage(element, [1], prev)).toEqual([1, 'cases', 'default']);
  });

  it('with SWITCH and unmatched greater than condition', () => {
    const prev = {data: {selected: 98}};
    const element = {
      switch: 'selected',
      cases: {
        '>34': [],
        '<34': []
      }
    };
    expect(evaluateBotMessage(element, [1], prev)).toEqual([1, 'cases', '>34']);
  });
});

describe('findRuleOperator util', () => {
  it('no op returns fallback', () => {
    expect(findRuleOperator('', '=')).toEqual('=');
    expect(findRuleOperator('test', '=')).toEqual('=');
    expect(findRuleOperator(null, '=')).toEqual('=');
  });

  it('equal operator', () => {
    expect(findRuleOperator('=test')).toEqual('=');
    expect(findRuleOperator('po=test')).toEqual('=');
  });

  it('not-bool operator', () => {
    expect(findRuleOperator('!test')).toEqual('!');
  });

  it('not equal operator', () => {
    expect(findRuleOperator('!=test')).toEqual('!=');
    expect(findRuleOperator('po!=test')).toEqual('!=');
  });

  it('lower than operator', () => {
    expect(findRuleOperator('<test')).toEqual('<');
    expect(findRuleOperator('po<test')).toEqual('<');
  });

  it('lower than operator', () => {
    expect(findRuleOperator('<test')).toEqual('<');
    expect(findRuleOperator('po<test')).toEqual('<');
  });
});

describe('parseBotSwitchRule util', () => {
  it('null data return default', () => {
    expect(parseBotSwitchRule()).toEqual('default');
    expect(parseBotSwitchRule('')).toEqual('default');
    expect(parseBotSwitchRule('', null)).toEqual('default');
    expect(parseBotSwitchRule('', null, null)).toEqual('default');
  });

  it('not equal rule found', () => {
    expect(parseBotSwitchRule('selected', {'!=yes': [1]}, {selected: 'no'})).toEqual('!=yes');
    expect(parseBotSwitchRule('selected', {'!=yes': [1]}, {selected: 'yes'})).toEqual('default');
  });

  it('equal rule found', () => {
    expect(parseBotSwitchRule('selected', {yes: [1]}, {selected: 'yes'})).toEqual('yes');
    expect(parseBotSwitchRule('selected', {'=yes': [1]}, {selected: 'yes'})).toEqual('=yes');
    expect(parseBotSwitchRule('selected', {'=yes': [1]}, {selected: 'no'})).toEqual('default');
  });

  it('greater than rule found', () => {
    expect(parseBotSwitchRule('selected', {'>34': [1], default: []}, {selected: '100'})).toEqual(
      '>34'
    );
    expect(parseBotSwitchRule('selected', {'>34': [1], default: []}, {selected: 100})).toEqual(
      '>34'
    );
    expect(parseBotSwitchRule('selected', {'>34': [1], default: []}, {selected: 3})).toEqual(
      'default'
    );
  });

  it('lower than rule found', () => {
    expect(parseBotSwitchRule('selected', {'<34': [1]}, {selected: '9'})).toEqual('<34');
    expect(parseBotSwitchRule('selected', {'<34': [1]}, {selected: 9})).toEqual('<34');
    expect(parseBotSwitchRule('selected', {'<34': [1]}, {selected: 300})).toEqual('default');
  });

  it('greater or equal than rule found', () => {
    expect(parseBotSwitchRule('selected', {'>=34': [1]}, {selected: '100'})).toEqual('>=34');
    expect(parseBotSwitchRule('selected', {'>=34': [1]}, {selected: 100})).toEqual('>=34');
    expect(parseBotSwitchRule('selected', {'>=34': [1]}, {selected: 34})).toEqual('>=34');
    expect(parseBotSwitchRule('selected', {'>=34': [1]}, {selected: 3})).toEqual('default');
  });

  it('lower or equal than rule found', () => {
    expect(parseBotSwitchRule('selected', {'<=34': [1]}, {selected: '9'})).toEqual('<=34');
    expect(parseBotSwitchRule('selected', {'<=34': [1]}, {selected: 9})).toEqual('<=34');
    expect(parseBotSwitchRule('selected', {'<=34': [1]}, {selected: 34})).toEqual('<=34');
    expect(parseBotSwitchRule('selected', {'<=34': [1]}, {selected: 300})).toEqual('default');
  });

  it('options lower or equal than rule found', () => {
    expect(
      parseBotSwitchRule('options.selected', {'<=34': [1]}, null, {options: {selected: '9'}})
    ).toEqual('<=34');
    expect(
      parseBotSwitchRule('options.selected', {'<=34': [1]}, null, {options: {selected: 9}})
    ).toEqual('<=34');
    expect(
      parseBotSwitchRule('options.selected', {'<=34': [1]}, null, {options: {selected: 34}})
    ).toEqual('<=34');
    expect(
      parseBotSwitchRule('options.selected', {'<=34': [1]}, null, {options: {selected: 300}})
    ).toEqual('default');
  });

  it('options, equal rule found', () => {
    expect(
      parseBotSwitchRule('options.selected', {yes: [1]}, null, {options: {selected: 'yes'}})
    ).toEqual('yes');
    expect(
      parseBotSwitchRule('options.selected', {'=yes': [1]}, null, {options: {selected: 'yes'}})
    ).toEqual('=yes');
    expect(
      parseBotSwitchRule('options.selected', {'=yes': [1]}, null, {options: {selected: 'no'}})
    ).toEqual('default');
  });
});

describe('findStepByName util', () => {
  const input = [{name: 'test'}, 'test', 'nothing', {name: 'second'}];
  it('several cases', () => {
    expect(findStepByName([])).toBeNull();
    expect(findStepByName()).toBeNull();
    expect(findStepByName([], 'test')).toEqual(null);
    expect(findStepByName(input, 'test')).toEqual([0]);
    expect(findStepByName(input, 'test-not')).toEqual(null);
    expect(findStepByName(input, 'second')).toEqual([3]);
    expect(findStepByName(input, 'nothing')).toEqual(null);
  });
});

describe('getLength util', () => {
  expect(getLength(null)).toEqual(0);
  expect(getLength()).toEqual(0);
  expect(getLength('')).toEqual(0);
  expect(getLength('as')).toEqual(2);
  expect(getLength(['as'])).toEqual(1);
  expect(getLength(['as', 23])).toEqual(2);
  expect(getLength({a: 'as', a2: 23})).toEqual(2);
});

describe('Resuming conversation', () => {
  const script = [
    'How can we help you today?',
    {
      ui: 'options',
      options: [
        'Can I see the menu?',
        'Where are you located?',
        'What time do you open?',
        'I would like to place a reservation'
      ],
      name: 'firstOptions',
      loop: true
    },
    {
      wait: 'input'
    },
    {
      switch: 'selected',
      cases: {
        'Can I see the menu?': [
          'Yes of course. Here you are.',
          {
            ui: 'menu'
          }
        ],
        'Where are you located?': [
          'Here are the details and map of our location.',
          {
            ui: 'map'
          }
        ],
        'What time do you open?': [
          'These are out opening hours.',
          {
            ui: 'open-hours'
          }
        ],
        'I would like to place a reservation': [
          "Great. Just fill in the details below and we'll take care of it.",
          {
            ui: 'booking'
          },
          {
            wait: 'input'
          }
        ]
      }
    },
    {
      if: 'conversation.previouslySelected.firstOptions![]=4&&!conversation.hasClientMessage',
      affirmative: [
        {
          ui: 'options',
          options: ['!@okShowMeMoreOptions'],
          optionsTitle: '',
          kind: 'small'
        },
        {
          wait: 'input'
        },
        {
          conditional: 'true',
          goto: 'firstOptions',
          while: '!conversation.hasClientMessage'
        }
      ]
    },
    {
      if: '!conversation.hasClientMessage',
      affirmative: ['!@anythingElseICanHelp']
    }
  ];

  test('Simple case', () => {
    const messages = new OrderedHash([
      {
        id: 3367,
        tempId: 1572742750918,
        message: 'How can we help you today?',
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:10.918Z'
      }
    ]);
    expect(resumeConversation(messages, script)).toEqual({
      scriptStep: [1],
      defaultOptions: {
        firstOptions: [
          'Can I see the menu?',
          'Where are you located?',
          'What time do you open?',
          'I would like to place a reservation'
        ]
      },
      previouslySelected: {}
    });
  });

  test('Client cancels script', () => {
    const messages = new OrderedHash([
      {
        id: 3367,
        tempId: 1572742750918,
        message: 'How can we help you today?',
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:10.918Z'
      },
      {
        id: 3368,
        tempId: 1572742750918,
        message: 'How can we help you today?',
        owner: 'client',
        status: 'received',
        updatedAt: '2019-11-03T00:59:10.918Z'
      }
    ]);
    expect(resumeConversation(messages, script)).toEqual({
      scriptStep: false,
      defaultOptions: {
        firstOptions: [
          'Can I see the menu?',
          'Where are you located?',
          'What time do you open?',
          'I would like to place a reservation'
        ]
      },
      previouslySelected: {}
    });
  });

  test('Second message', () => {
    const messages = new OrderedHash([
      {
        id: 3367,
        tempId: 1572742750918,
        message: 'How can we help you today?',
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:10.918Z'
      },
      {
        id: 3368,
        tempId: 1572742752924,
        message: {
          ui: 'options',
          options: [
            'Can I see the menu?',
            'Where are you located?',
            'What time do you open?',
            'I would like to place a reservation'
          ],
          name: 'firstOptions',
          loop: true
        },
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:12.930Z',
        data: {
          selected: 'Can I see the menu?'
        }
      }
    ]);
    expect(resumeConversation(messages, script)).toEqual({
      scriptStep: [3],
      defaultOptions: {
        firstOptions: [
          'Where are you located?',
          'What time do you open?',
          'I would like to place a reservation'
        ]
      },
      previouslySelected: {
        firstOptions: ['Can I see the menu?']
      }
    });
  });

  test('First Selected message', () => {
    const messages = new OrderedHash([
      {
        id: 3367,
        tempId: 1572742750918,
        message: 'How can we help you today?',
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:10.918Z'
      },
      {
        id: 3368,
        tempId: 1572742752924,
        message: {
          ui: 'options',
          options: [
            'Can I see the menu?',
            'Where are you located?',
            'What time do you open?',
            'I would like to place a reservation'
          ],
          name: 'firstOptions',
          loop: true
        },
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:12.930Z',
        data: {
          selected: 'Can I see the menu?'
        }
      },
      {
        id: 3369,
        tempId: 1572742757181,
        message: 'Yes of course. Here you are.',
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:17.186Z'
      }
    ]);
    expect(resumeConversation(messages, script)).toEqual({
      scriptStep: [3, 'cases', 'Can I see the menu?', 1],
      defaultOptions: {
        firstOptions: [
          'Where are you located?',
          'What time do you open?',
          'I would like to place a reservation'
        ]
      },
      previouslySelected: {
        firstOptions: ['Can I see the menu?']
      }
    });
  });

  test('First Selected option continues', () => {
    const messages = new OrderedHash([
      {
        id: 3367,
        tempId: 1572742750918,
        message: 'How can we help you today?',
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:10.918Z'
      },
      {
        id: 3368,
        tempId: 1572742752924,
        message: {
          ui: 'options',
          options: [
            'Can I see the menu?',
            'Where are you located?',
            'What time do you open?',
            'I would like to place a reservation'
          ],
          name: 'firstOptions',
          loop: true
        },
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:12.930Z',
        data: {
          selected: 'Can I see the menu?'
        }
      },
      {
        id: 3369,
        tempId: 1572742757181,
        message: 'Yes of course. Here you are.',
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:17.186Z'
      },
      {
        id: 3370,
        tempId: 1572742759202,
        message: {
          ui: 'menu'
        },
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:19.207Z'
      }
    ]);
    expect(resumeConversation(messages, script)).toEqual({
      scriptStep: [4],
      defaultOptions: {
        firstOptions: [
          'Where are you located?',
          'What time do you open?',
          'I would like to place a reservation'
        ]
      },
      previouslySelected: {
        firstOptions: ['Can I see the menu?']
      }
    });
  });

  test('First show more options unclicked', () => {
    const messages = new OrderedHash([
      {
        id: 3367,
        tempId: 1572742750918,
        message: 'How can we help you today?',
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:10.918Z'
      },
      {
        id: 3368,
        tempId: 1572742752924,
        message: {
          ui: 'options',
          options: [
            'Can I see the menu?',
            'Where are you located?',
            'What time do you open?',
            'I would like to place a reservation'
          ],
          name: 'firstOptions',
          loop: true
        },
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:12.930Z',
        data: {
          selected: 'Can I see the menu?'
        }
      },
      {
        id: 3369,
        tempId: 1572742757181,
        message: 'Yes of course. Here you are.',
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:17.186Z'
      },
      {
        id: 3370,
        tempId: 1572742759202,
        message: {
          ui: 'menu'
        },
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:19.207Z'
      },
      {
        id: 3371,
        tempId: 1572742763230,
        message: {
          ui: 'options',
          options: ['!@okShowMeMoreOptions'],
          optionsTitle: '',
          kind: 'small'
        },
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:23.236Z'
      }
    ]);
    expect(resumeConversation(messages, script)).toEqual({
      scriptStep: [4, 'affirmative', 2],
      defaultOptions: {
        firstOptions: [
          'Where are you located?',
          'What time do you open?',
          'I would like to place a reservation'
        ]
      },
      previouslySelected: {
        firstOptions: ['Can I see the menu?']
      }
    });
  });

  test('First show more options clicked', () => {
    const messages = new OrderedHash([
      {
        id: 3367,
        tempId: 1572742750918,
        message: 'How can we help you today?',
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:10.918Z'
      },
      {
        id: 3368,
        tempId: 1572742752924,
        message: {
          ui: 'options',
          options: [
            'Can I see the menu?',
            'Where are you located?',
            'What time do you open?',
            'I would like to place a reservation'
          ],
          name: 'firstOptions',
          loop: true
        },
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:12.930Z',
        data: {
          selected: 'Can I see the menu?'
        }
      },
      {
        id: 3369,
        tempId: 1572742757181,
        message: 'Yes of course. Here you are.',
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:17.186Z'
      },
      {
        id: 3370,
        tempId: 1572742759202,
        message: {
          ui: 'menu'
        },
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:19.207Z'
      },
      {
        id: 3371,
        tempId: 1572742763230,
        message: {
          ui: 'options',
          options: ['!@okShowMeMoreOptions'],
          optionsTitle: '',
          kind: 'small'
        },
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:23.236Z',
        data: {
          selected: '!@okShowMeMoreOptions'
        }
      }
    ]);
    expect(resumeConversation(messages, script)).toEqual({
      scriptStep: [4, 'affirmative', 2],
      defaultOptions: {
        firstOptions: [
          'Where are you located?',
          'What time do you open?',
          'I would like to place a reservation'
        ]
      },
      previouslySelected: {
        firstOptions: ['Can I see the menu?']
      }
    });
  });

  test('Goto should act', () => {
    const messages = new OrderedHash([
      {
        id: 3367,
        tempId: 1572742750918,
        message: 'How can we help you today?',
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:10.918Z'
      },
      {
        id: 3368,
        tempId: 1572742752924,
        message: {
          ui: 'options',
          options: [
            'Can I see the menu?',
            'Where are you located?',
            'What time do you open?',
            'I would like to place a reservation'
          ],
          name: 'firstOptions',
          loop: true
        },
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:12.930Z',
        data: {
          selected: 'Can I see the menu?'
        }
      },
      {
        id: 3369,
        tempId: 1572742757181,
        message: 'Yes of course. Here you are.',
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:17.186Z'
      },
      {
        id: 3370,
        tempId: 1572742759202,
        message: {
          ui: 'menu'
        },
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:19.207Z'
      },
      {
        id: 3371,
        tempId: 1572742763230,
        message: {
          ui: 'options',
          options: ['!@okShowMeMoreOptions'],
          optionsTitle: '',
          kind: 'small'
        },
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:23.236Z',
        data: {
          selected: '!@okShowMeMoreOptions'
        }
      },
      {
        id: 3372,
        tempId: 1572742765087,
        message: {
          ui: 'options',
          options: [
            'Where are you located?',
            'What time do you open?',
            'I would like to place a reservation'
          ],
          name: 'firstOptions',
          loop: true
        },
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:25.087Z'
      }
    ]);
    expect(resumeConversation(messages, script)).toEqual({
      scriptStep: [3],
      defaultOptions: {
        firstOptions: [
          'Where are you located?',
          'What time do you open?',
          'I would like to place a reservation'
        ]
      },
      previouslySelected: {
        firstOptions: ['Can I see the menu?']
      }
    });
  });

  test('Goto should act and then select another', () => {
    const messages = new OrderedHash([
      {
        id: 3367,
        tempId: 1572742750918,
        message: 'How can we help you today?',
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:10.918Z'
      },
      {
        id: 3368,
        tempId: 1572742752924,
        message: {
          ui: 'options',
          options: [
            'Can I see the menu?',
            'Where are you located?',
            'What time do you open?',
            'I would like to place a reservation'
          ],
          name: 'firstOptions',
          loop: true
        },
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:12.930Z',
        data: {
          selected: 'Can I see the menu?'
        }
      },
      {
        id: 3369,
        tempId: 1572742757181,
        message: 'Yes of course. Here you are.',
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:17.186Z'
      },
      {
        id: 3370,
        tempId: 1572742759202,
        message: {
          ui: 'menu'
        },
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:19.207Z'
      },
      {
        id: 3371,
        tempId: 1572742763230,
        message: {
          ui: 'options',
          options: ['!@okShowMeMoreOptions'],
          optionsTitle: '',
          kind: 'small'
        },
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:23.236Z',
        data: {
          selected: '!@okShowMeMoreOptions'
        }
      },
      {
        id: 3372,
        tempId: 1572742765087,
        message: {
          ui: 'options',
          options: [
            'Where are you located?',
            'What time do you open?',
            'I would like to place a reservation'
          ],
          name: 'firstOptions',
          loop: true
        },
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:25.087Z',
        data: {
          selected: 'I would like to place a reservation'
        }
      }
    ]);
    expect(resumeConversation(messages, script)).toEqual({
      scriptStep: [3],
      defaultOptions: {
        firstOptions: ['Where are you located?', 'What time do you open?']
      },
      previouslySelected: {
        firstOptions: ['Can I see the menu?', 'I would like to place a reservation']
      }
    });
  });

  test('Goto should act and then select another', () => {
    const messages = new OrderedHash([
      {
        id: 3367,
        tempId: 1572742750918,
        message: 'How can we help you today?',
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:10.918Z'
      },
      {
        id: 3368,
        tempId: 1572742752924,
        message: {
          ui: 'options',
          options: [
            'Can I see the menu?',
            'Where are you located?',
            'What time do you open?',
            'I would like to place a reservation'
          ],
          name: 'firstOptions',
          loop: true
        },
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:12.930Z',
        data: {
          selected: 'Can I see the menu?'
        }
      },
      {
        id: 3369,
        tempId: 1572742757181,
        message: 'Yes of course. Here you are.',
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:17.186Z'
      },
      {
        id: 3370,
        tempId: 1572742759202,
        message: {
          ui: 'menu'
        },
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:19.207Z'
      },
      {
        id: 3371,
        tempId: 1572742763230,
        message: {
          ui: 'options',
          options: ['!@okShowMeMoreOptions'],
          optionsTitle: '',
          kind: 'small'
        },
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:23.236Z',
        data: {
          selected: '!@okShowMeMoreOptions'
        }
      },
      {
        id: 3372,
        tempId: 1572742765087,
        message: {
          ui: 'options',
          options: [
            'Where are you located?',
            'What time do you open?',
            'I would like to place a reservation'
          ],
          name: 'firstOptions',
          loop: true
        },
        owner: 'bot',
        status: 'received',
        updatedAt: '2019-11-03T00:59:25.087Z',
        data: {
          selected: 'I would like to place a reservation'
        }
      },
      {
        id: 3373,
        message: "Great. Just fill in the details below and we'll take care of it.",
        owner: 'bot'
      }
    ]);
    expect(resumeConversation(messages, script)).toEqual({
      scriptStep: [3, 'cases', 'I would like to place a reservation', 1],
      defaultOptions: {
        firstOptions: ['Where are you located?', 'What time do you open?']
      },
      previouslySelected: {
        firstOptions: ['Can I see the menu?', 'I would like to place a reservation']
      }
    });
  });
});

/*
describe('Resuming conversation', () => {
  const previouslySelected = {
    firstOptions: {
      'Can I see the menu?': true
    }
  };

  const defaultOptions = {
    firstOptions: [
      'Where are you located?',
      'What time do you open?',
      'I would like to place a reservation',
    ]
  };

  const scriptStep = [3];

  const messages = new OrderedHash([
    {
      "3367": {
        "id": 3367,
        "tempId": 1572742750918,
        "message": "How can we help you today?",
        "owner": "bot",
        "status": "received",
        "updatedAt": "2019-11-03T00:59:10.918Z"
      },
      "3368": {
        "id": 3368,
        "tempId": 1572742752924,
        "message": {
          "ui": "options",
          "options": [
            "Can I see the menu?",
            "Where are you located?",
            "What time do you open?",
            "I would like to place a reservation"
          ],
          "name": "firstOptions",
          "loop": true
        },
        "owner": "bot",
        "status": "received",
        "updatedAt": "2019-11-03T00:59:12.930Z",
        "data": {
          "selected": "Can I see the menu?"
        }
      },
      "3369": {
        "id": 3369,
        "tempId": 1572742757181,
        "message": "Yes of course. Here you are.",
        "owner": "bot",
        "status": "received",
        "updatedAt": "2019-11-03T00:59:17.186Z"
      },
      "3370": {
        "id": 3370,
        "tempId": 1572742759202,
        "message": {
          "ui": "menu"
        },
        "owner": "bot",
        "status": "received",
        "updatedAt": "2019-11-03T00:59:19.207Z"
      },
      "3371": {
        "id": 3371,
        "tempId": 1572742763230,
        "message": {
          "ui": "options",
          "options": [
            "!@okShowMeMoreOptions"
          ],
          "optionsTitle": "",
          "kind": "small"
        },
        "owner": "bot",
        "status": "received",
        "updatedAt": "2019-11-03T00:59:23.236Z",
        "data": {
          "selected": "!@okShowMeMoreOptions"
        }
      },
      "3372": {
        "id": 3372,
        "tempId": 1572742765087,
        "message": {
          "ui": "options",
          "options": [
            "Where are you located?",
            "What time do you open?",
            "I would like to place a reservation"
          ],
          "name": "firstOptions",
          "loop": true
        },
        "owner": "bot",
        "status": "received",
        "updatedAt": "2019-11-03T00:59:25.087Z"
      }
    }
  ]);

  const script = [
    "How can we help you today?",
    {
      "ui": "options",
      "options": [
        "Can I see the menu?",
        "Where are you located?",
        "What time do you open?",
        "I would like to place a reservation"
      ],
      "name": "firstOptions",
      "loop": true
    },
    {
      "wait": "input"
    },
    {
      "switch": "selected",
      "cases": {
        "Can I see the menu?": [
          "Yes of course. Here you are.",
          {
            "ui": "menu"
          }
        ],
        "Where are you located?": [
          "Here are the details and map of our location.",
          {
            "ui": "map"
          }
        ],
        "What time do you open?": [
          "These are out opening hours.",
          {
            "ui": "open-hours"
          }
        ],
        "I would like to place a reservation": [
          "Great. Just fill in the details below and we'll take care of it.",
          {
            "ui": "booking"
          },
          {
            "wait": "input"
          }
        ]
      }
    },
    {
      "if": "conversation.previouslySelected.firstOptions![]=4&&!conversation.hasClientMessage",
      "affirmative": [
        {
          "ui": "options",
          "options": [
            "!@okShowMeMoreOptions"
          ],
          "optionsTitle": "",
          "kind": "small"
        },
        {
          "wait": "input"
        },
        {
          "conditional": "true",
          "goto": "firstOptions",
          "while": "!conversation.hasClientMessage"
        }
      ]
    },
    {
      "if": "!conversation.hasClientMessage",
      "affirmative": [
        "!@anythingElseICanHelp"
      ]
    }
  ];

  test('With goto loop', () => {
    expect(resumeConversation(messages, script)).toEqual({scriptStep, defaultOptions, previouslySelected})
  })
});*/

export function rgbaToHex(rgba) {
  if (typeof rgba !== 'string' || rgba.substring(0, 5) !== 'rgba(') {
    return rgba || '';
  }

  const [r, g, b] = extractRgb(rgba).map(n => {
    let res = parseInt(n, 10).toString(16);
    if (res.length < 2) {
      res = '0' + res;
    }
    return res;
  });

  return `#${r}${g}${b}`;
}

export function extractRgb(str) {
  if (typeof str !== 'string' || str.substring(0, 5) !== 'rgba(') {
    return [0, 0, 0];
  }
  return str
    .trim()
    .substr(5, str.length - 6)
    .split(',')
    .slice(0, 3);
}

function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result
    ? [parseInt(result[1], 16), parseInt(result[2], 16), parseInt(result[3], 16)]
    : extractRgb(hex);
}

export function farthestAbsoluteColor(rgba) {
  const [r, g, b] = hexToRgb(rgba);
  if (r + g + b > 382) {
    return '#000000';
  } else {
    return '#ffffff';
  }
}

/* Returns a lighter or darker color
 * @param col (string) '#dddddd' || 'dddddd'
 * @param amt (integer) percentage, positive number lighten, negative number darken
 * @return (string) new color with or without '#', following the pattern received in the input
 */
export function lightenDarkenColor(col, amt) {
  let usePound = false;

  if (col[0] == '#') {
    col = col.slice(1);
    usePound = true;
  }

  const num = parseInt(col, 16);

  let r = (num >> 16) + amt;
  if (r > 255) {
    r = 255;
  } else if (r < 0) {
    r = 0;
  }

  let b = ((num >> 8) & 0x00ff) + amt;
  if (b > 255) {
    b = 255;
  } else if (b < 0) {
    b = 0;
  }

  let g = (num & 0x0000ff) + amt;
  if (g > 255) {
    g = 255;
  } else if (g < 0) {
    g = 0;
  }

  let hex = (g | (b << 8) | (r << 16)).toString(16);
  if (hex.length === 5) {
    hex += 0;
  }
  return (usePound ? '#' : '') + hex;
}

export function isColor(strColor) {
  if (!strColor) {
    return false;
  }
  var s = new Option().style;
  s.color = strColor;
  return s.color == strColor;
}

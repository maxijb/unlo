import {rgbaToHex, farthestAbsoluteColor, lightenDarkenColor} from './color-utils';

describe('rgbaToHex util', () => {
  it('empty input should be false', () => {
    expect(rgbaToHex()).toEqual('');
  });

  it('empty string should be false', () => {
    expect(rgbaToHex('')).toEqual('');
  });

  it('simple string should be returned untouched', () => {
    expect(rgbaToHex('#7591158')).toEqual('#7591158');
  });

  it('simple color', () => {
    expect(rgbaToHex('rgba(75,91,158,1)')).toEqual('#4b5b9e');
  });

  it('black', () => {
    expect(rgbaToHex('rgba(0,0,0,1)')).toEqual('#000000');
  });

  it('black', () => {
    expect(rgbaToHex('rgba(255,255,255,1)')).toEqual('#ffffff');
  });
});

describe('farthestAbsoluteColor util', () => {
  it('empty input should be false', () => {
    expect(farthestAbsoluteColor()).toEqual('#ffffff');
  });

  it('empty string should be false', () => {
    expect(farthestAbsoluteColor('')).toEqual('#ffffff');
  });

  it('simple color', () => {
    expect(farthestAbsoluteColor('rgba(75,91,158,1)')).toEqual('#000000');
  });

  it('black', () => {
    expect(farthestAbsoluteColor('rgba(0,0,0,1)')).toEqual('#ffffff');
  });

  it('white', () => {
    expect(farthestAbsoluteColor('rgba(255,255,255,1)')).toEqual('#000000');
  });

  it('black hex', () => {
    expect(farthestAbsoluteColor('#000000')).toEqual('#ffffff');
  });

  it('white hex', () => {
    expect(farthestAbsoluteColor('#ffffff')).toEqual('#000000');
  });
});

describe('lightenDarkenColor utils', () => {
  it('Darken hex', () => {
    expect(lightenDarkenColor('#212121', -30)).toEqual('#303030');
  });

  it('Brighter hex', () => {
    expect(lightenDarkenColor('#212121', 30)).toEqual('#3f3f3f');
  });
});

import times from 'lodash.times';
import t from 'timestamp-utils';
import get from 'lodash.get';
import isEmpty from 'is-empty';
import {Messages} from '@Widget/config/defaults';
import moment from 'moment-timezone';
import {TimeUnitOrder, UnitInMS, TimeDirection} from '@Common/constants/dates';

const HourSeparator = ':';
const DAYS_TO_DISPLAY_PER_MONTH = 42;

const MONTHS_LENGHT = [31, null, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

const MOVE_BACK_INITIAL_DAY = [1, 0, 6, 5, 4, 3, 2];
export const SLOTS_INTERVAL_SEPARATORS = '       ';

export const isLeapYear = year => (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;

export const initMonth = (timestamp, initialWeekDay = 0, period = 'month') => {
  timestamp = timestamp || Date.now();
  let [year, month, dayNumber] = t.decompose(timestamp);

  const firstMonthDay =
    period === 'month'
      ? getDateWithoutTime(t.addDays(timestamp, -dayNumber + 1))
      : getDateWithoutTime(timestamp);

  const monthLenght = getMonthLength(month - 1, year);
  const lastMonthDay = t.addDays(firstMonthDay, monthLenght - 1);
  const firstMonthDayNumber = t.getWeekDay(firstMonthDay);
  const firstDayToDisplay = t.addDays(
    firstMonthDay,
    -firstMonthDayNumber - MOVE_BACK_INITIAL_DAY[initialWeekDay],
  );

  let month2 = null;
  let year2 = null;
  if (period === 'week') {
    const [fyear, fmonth] = t.decompose(firstDayToDisplay);
    month = fmonth;
    year = fyear;

    const lastDayToDisplay = t.addDays(firstDayToDisplay, 6);
    const [lyear, lmonth] = t.decompose(lastDayToDisplay);
    month2 = lmonth;
    year2 = lyear;
  }

  return {
    firstMonthDay,
    lastMonthDay,
    firstDayToDisplay,
    month,
    year,
    month2,
    year2,
  };
};

export const getMonthLength = (month, year) => {
  return MONTHS_LENGHT[month] || (isLeapYear(year) ? 29 : 28);
};

export const parseRange = (startDate, endDate) => ({
  startDate: endDate ? Math.min(startDate, endDate) : startDate,
  endDate: endDate && endDate !== startDate ? Math.max(startDate, endDate) : null,
});

export const getDays = (firstDay, period) => {
  let days = DAYS_TO_DISPLAY_PER_MONTH;
  if (period === 'week') {
    days = 7;
  }
  return times(days, i => t.addDays(firstDay, i));
};

export const getDateWithoutTime = timestamp => {
  const [, , , hours, minutes, seconds, milliseconds] = t.decompose(timestamp || Date.now());
  return t.add(timestamp, {
    hours: -hours,
    minutes: -minutes,
    seconds: -seconds,
    milliseconds: -milliseconds,
  });
};

export const dateIsBetween = (date, start, end) => date > start && date < end;

export const dateIsOut = (date, start, end) => date < start || date > end;

export const formatTime = value => `0${value}`.slice(-2);

export const formatDate = value => new Intl.DateTimeFormat('en-US').format(new Date(value));

export const strDate = (date, time) => {
  const d = new Date(date);
  return `${d.getFullYear()}-${addZero(d.getMonth() + 1)}-${d.getDate()}${
    Boolean(time) ? ' ' + time : ''
  }`;
};

export const calculateTimeSlots = (timestamp = new Date().getTime(), options = {}) => {
  const weekDay = new Date(timestamp).getUTCDay();

  // read options
  let hours = get(options, ['openHours', weekDay]);
  if (isEmpty(hours)) {
    hours = [0, 24];
  }

  if (!isEmpty(options.specialOpenHours)) {
    const date = getAbsDateOnly(timestamp);
    if (options.specialOpenHours[date]) {
      hours = options.specialOpenHours[date];
    }
  }

  // more options
  const {duration: defaultDurationMinutes = 30, defaultTimeInterval = 15} = get(
    options,
    'bookings',
    {},
  );

  // selected base date
  const [year, month, dayNumber] = t.decompose(timestamp || Date.now());

  // we'll return this
  const slots = [];

  // for each interval
  for (let i = 0, len = Math.floor(hours.length / 2); i < len; i++) {
    if (i > 0) {
      slots.push(SLOTS_INTERVAL_SEPARATORS);
    }
    const startHour = hours[i * 2].toString().split(HourSeparator);
    const endHour = hours[i * 2 + 1].toString().split(HourSeparator);
    let startDate = new Date(year, month, dayNumber, ...startHour).getTime();
    const endDate = new Date(year, month, dayNumber, ...endHour).getTime();

    // repeat until out of range
    while (true) {
      if (t.addMinutes(startDate, defaultDurationMinutes) <= endDate) {
        slots.push(startDate);
        startDate = t.addMinutes(startDate, defaultTimeInterval);
      } else {
        break;
      }
    }
  }

  const processedSlots = slots.map(s => {
    if (s === SLOTS_INTERVAL_SEPARATORS) {
      return s;
    }
    const date = new Date(s);
    const mins = date.getMinutes();
    return `${date.getHours()}:${addZero(mins)}`;
  });

  if (processedSlots[processedSlots.length - 1] === '0:00') {
    processedSlots[processedSlots.length - 1] = '24:00';
  }

  return processedSlots;
};

export function addZero(mins = 0) {
  const shouldAdd = typeof min === 'string' ? mins.length < 2 : mins < 10;
  return `${shouldAdd ? '0' : ''}${Number(mins)}`;
}

export function formatTimeAsString(time) {
  if (!time) {
    return '0:00';
  }

  const [hour, mins = 0] = time.toString().split(HourSeparator);
  return `${hour}${HourSeparator}${addZero(mins)}`;
}

export function formatAllHoursAsString(hours) {
  if (isEmpty(hours)) {
    return [];
  }
  return hours.map(day => {
    if (isEmpty(day)) {
      return [];
    }
    return day.map(time => formatTimeAsString(time));
  });
}

export const defaultAbsPrintTime = date => {
  const d = new Date(date);
  return `${d.getUTCHours()}:${addZero(d.getUTCMinutes())}`;
};

export const defaultAbsPrintDate = (d, messages = Messages) => {
  const date = new Date(d);
  const month = date.getUTCMonth();
  const day = date.getUTCDate();
  const year = date.getUTCFullYear();

  return `${messages.shortMonths[month]} ${day}${
    year === new Date().getUTCFullYear() ? '' : ` ${year}`
  }`;
};

export const getAbsDateOnly = date => {
  date = new Date(date);
  const month = date.getUTCMonth() + 1;
  const day = date.getUTCDate();
  const year = date.getUTCFullYear();
  return `${year}-${addZero(month)}-${addZero(day)}`;
};

export const defaultPrintTime = date => {
  const d = embeddedToAbsUTC(date);
  return defaultAbsPrintTime(d);
};

export const defaultPrintDate = (d, messages = Messages) => {
  const date = embeddedToAbsUTC(d);
  return defaultAbsPrintDate(date, messages);
};

export const defaultPrintDateWithDayName = (d, messages = Messages) => {
  const date = embeddedToAbsUTC(d);
  const day = date.getUTCDay();
  return `${messages.shortDays[day]} ${defaultAbsPrintDate(date, messages)}`;
};

export const formatGMTOffset = offsetMins => {
  if (!offsetMins) {
    return 'Z';
  }

  const offset = offsetMins / 60;

  const sign = offset > 0 ? '+' : '-';
  const diff = Math.abs(offset);
  const mod = diff % 1;

  return `${sign}${addZero(Math.floor(diff))}:${mod ? addZero(mod * 60) : '00'}`;
};

export const formatMysqlTimezoneOffset = offsetMins => {
  if (!offsetMins) {
    return '+00:00';
  }

  const offset = offsetMins / 60;

  const sign = offset > 0 ? '+' : '-';
  const diff = Math.abs(offset);
  const mod = diff % 1;

  return `${sign}${addZero(Math.floor(diff))}:${mod ? addZero(mod * 60) : '00'}`;
};

export const embeddedToAbsUTC = str => {
  const offset = extractTimezoneMinOffsetFromString(str) * 60000;
  return new Date(new Date(str).getTime() + offset);
};

export const extractTimezoneMinOffsetFromString = str => {
  if (!str || typeof str !== 'string') {
    return 0;
  }

  const match = str.match(/[\sT]\d{1,2}:\d{1,2}:\d{1,2}\s?(Z|GMT)?([+-])?([0-9:]*)?/i);
  if (!match || match.length < 4) {
    return 0;
  }
  const [timestr, zone, sign, rawdelta] = match;
  if (zone === 'Z' || !rawdelta) {
    return 0;
  }

  const delta = rawdelta.replace(':', '');
  let mins = 0;
  let hours = 0;
  if (delta.length > 2) {
    mins = Number(delta.substring(delta.length - 2));
    hours = Number(delta.substr(0, delta.length - 2));
  } else {
    hours = Number(delta);
  }
  return sign === '-' ? hours * -60 - mins : hours * 60 + mins;
};

export const absUTCtoEmbeddedTZ = (d, timezone, gmt) => {
  const tz = gmt || formatGMTOffset(getTimezoneOffset(timezone, d));
  return `${getUTCDateTimeAsString(d)}${tz}`;
};

export const utcToEmbeddedTZ = (date, timezone) => {
  const timezoneOffset = getTimezoneOffset(timezone, date);
  const gmt = formatGMTOffset(timezoneOffset);
  const d = addMinutes(date, timezoneOffset);
  return absUTCtoEmbeddedTZ(d, null, gmt);
};

/* Converts a date object (utc non abs) into the expected ISO google format:
 * 20100909T201212Z
 * @param date {Date}
 * @param timezone {object} {name, offset, untils, etc}
 */
export const dateToGoogleFormat = (date, timezone) => {
  const timezoneOffset = getTimezoneOffset(timezone, date);
  const d = addMinutes(date, timezoneOffset);
  return `${d.getUTCFullYear()}${addZero(d.getUTCMonth() + 1)}${addZero(d.getUTCDate())}T${addZero(
    d.getUTCHours(),
  )}${addZero(d.getUTCMinutes())}${addZero(d.getUTCSeconds())}`;
};

export const addMinutes = (date, offset = 0) => {
  const d = new Date(date);
  return new Date(d.getTime() + offset * 60000);
};

export const absDateAndTimeToEmbedded = (date, time, timezone) => {
  const d = new Date(date);
  const [hs, mins] = time.split(':');
  const tz = formatGMTOffset(getTimezoneOffset(timezone, d));
  return `${d.getUTCFullYear()}-${addZero(d.getUTCMonth() + 1)}-${addZero(
    d.getUTCDate(),
  )}T${addZero(hs)}:${addZero(mins)}:${addZero(d.getUTCSeconds())}${tz}`;
};

export const getUTCDateTimeAsString = (d, readable = false) => {
  return `${d.getUTCFullYear()}-${addZero(d.getUTCMonth() + 1)}-${addZero(d.getUTCDate())}${
    readable ? ' ' : 'T'
  }${addZero(d.getUTCHours())}:${addZero(d.getUTCMinutes())}:${addZero(d.getUTCSeconds())}${
    readable ? ' UTC' : ''
  }`;
};

export const getUTCDateAsString = d => {
  return `${d.getUTCFullYear()}-${addZero(d.getUTCMonth() + 1)}-${addZero(d.getUTCDate())}`;
};

export const beginningOfThisMonth = date => {
  const d = date || new Date();
  return `${d.getUTCFullYear()}-${addZero(d.getUTCMonth() + 1)}-01`;
};

export const endOfThisMonth = date => {
  const d = date || new Date();
  const monthLenght = getMonthLength(d.getUTCMonth(), d.getUTCFullYear());
  return `${d.getUTCFullYear()}-${addZero(d.getUTCMonth() + 1)}-${monthLenght}`;
};

/* Add time to a string date
 * @param date {string}
 */
export const begginingOfDay = date => {
  if (typeof date !== 'string') {
    date = getAbsDateOnly(date);
  }
  return `${date} 00:00:00`;
};
/* Add time to a string date
 * @param date {string}
 */
export const endOfDay = date => {
  if (typeof date !== 'string') {
    date = getAbsDateOnly(date);
  }
  return `${date} 23:59:59`;
};

export const addTimezoneToStringDate = (date, offset) => {
  return `${date}${formatGMTOffset(offset)}`;
};

export const getUTCToday = () => {
  const d = new Date();
  return new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate())).getTime();
};

export const guessTimezone = () => {
  return typeof Intl !== 'undefined' ? Intl.DateTimeFormat().resolvedOptions().timeZone : 'UTC';
};

export const getTimezoneOffset = (timezone, date) => {
  if (!timezone || isEmpty(timezone.offsets)) {
    return 0;
  }
  const {offsets, untils} = timezone;
  if (offsets.length < 2) {
    return -offsets[0];
  }

  const ts = new Date(date).getTime();
  let index = 0;
  while (index < untils.length) {
    if (ts <= untils[index] || untils[index] === null) {
      return -offsets[index];
    }
    index++;
  }

  return -offsets[index];
};

export const sortByStartDate = (a, b) => {
  return new Date(a.startDate).getTime() - new Date(b.startDate).getTime();
};

export const getEmbeddedDateString = str => {
  if (str.match(/(\d)*?[-\/](\d)*?T/)) {
    return str.split('T')[0];
  }
  return str.split(' ')[0];
};

export const hasRegularOpenHours = openHours => {
  return !isEmpty(openHours) && !openHours.every(day => isEmpty(day));
};

export const getYesterday = (timezone, type = 'date') => {
  const date = new Date(new Date().getTime() - 24 * 60 * 60 * 1000);
  if (type === 'date') {
    return date;
  }

  return getAbsDateOnly(date);
};

export const isDayClosed = (abstimestamp, options) => {
  const {specialOpenHours, openHours} = options;
  if (isEmpty(openHours)) {
    return false;
  }

  const date = new Date(abstimestamp);
  const strDate = getAbsDateOnly(date);
  const weekDay = date.getUTCDay();

  const hours = get(specialOpenHours, strDate) || get(openHours, [weekDay], null);
  const isClosed = isEmpty(hours) || hours[0] === null;

  return isClosed;
};

/* Checks the open hours of a day (including special hours)
 * param abstimestamp [integer] mandatory
 * options [{openHours, specialOpenHours}] mandatory
 * strDate {string} date optional
 */
export const getDayHours = (abstimestamp, options, strDate) => {
  let {specialOpenHours, openHours} = options;
  if (isEmpty(openHours)) {
    openHours = [];
  }

  const date = new Date(abstimestamp);
  if (!strDate) {
    strDate = getAbsDateOnly(date);
  }
  const weekDay = date.getUTCDay();

  const hours = get(specialOpenHours, strDate) || get(openHours, [weekDay], null);
  return hours;
};

export const timeFrom = (time, time2 = new Date()) => {
  const d = new Date(time).getTime();
  const d2 = new Date(time2).getTime();
  const diff = Math.abs(d - d2);

  let unit = 'seconds';
  let amount = 1;
  for (let i = 0, len = UnitInMS.length; i < len; i++) {
    if (diff < UnitInMS[i + 1]) {
      unit = TimeUnitOrder[i];
      amount = Math.round(diff / UnitInMS[i]);
      break;
    }
  }

  return {
    unit,
    amount,
    direction: d > d2 ? TimeDirection.forward : TimeDirection.past,
  };
};

/* Compares two dates using local month/day/year comparisons
 * @param d {string|number|date}
 * @param d2 {striong|number|date} optional -> defaults to now
 * @return {boolean} true if different
 */
export const compareLocalDates = (d, d2) => {
  const date = new Date(d);
  const other = d2 ? new Date(d2) : new Date();
  return (
    date.getMonth() !== other.getMonth() ||
    date.getDate() !== other.getDate() ||
    date.getFullYear() !== other.getFullYear()
  );
};

/* Calculates based on opening hours whether the business is open or not
 * @param hours {array} open hours format
 * timezone {object} our own timezone format
 */
export const calculateOpenNow = (hours, timezone, date) => {
  if (isEmpty(hours)) {
    return false;
  }
  // find the current time in the restaurant timezone
  date = date || new Date();
  const timezoneOffset = getTimezoneOffset(timezone, date);
  const d = addMinutes(date, timezoneOffset);
  // current hour and minutes
  const hour = d.getUTCHours();
  const minutes = d.getUTCMinutes();
  // const openHours todaya
  const dayHours = hours[d.getUTCDay()] || [];

  for (let i = 0; i < dayHours.length; i = i + 2) {
    const open = processTime(dayHours[i]);
    const close = processTime(dayHours[i + 1]);

    if (
      (hour > open[0] || (hour === open[0] && minutes >= (open[1] || 0))) &&
      (hour < close[0] || (hour === close[0] && minutes <= (close[1] || 0)))
    ) {
      return true;
    }
  }

  return false;

  function processTime(time) {
    return String(time)
      .split(':')
      .map(v => Number(v) || 0);
  }
};

export function addWorkingDays(startDate, days) {
  if (isNaN(days)) {
    console.log('Value provided for "days" was not a number');
    return;
  }
  if (!(startDate instanceof Date)) {
    console.log('Value provided for "startDate" was not a Date object');
    return;
  }
  // Get the day of the week as a number (0 = Sunday, 1 = Monday, .... 6 = Saturday)
  var dow = startDate.getDay();
  var daysToAdd = parseInt(days);
  // If the current day is Sunday add one day
  if (dow == 0) daysToAdd++;
  // If the start date plus the additional days falls on or after the closest Saturday calculate weekends
  if (dow + daysToAdd >= 6) {
    //Subtract days in current working week from work days
    var remainingWorkDays = daysToAdd - (5 - dow);
    //Add current working week's weekend
    daysToAdd += 2;
    if (remainingWorkDays > 5) {
      //Add two days for each working week by calculating how many weeks are included
      daysToAdd += 2 * Math.floor(remainingWorkDays / 5);
      //Exclude final weekend if remainingWorkDays resolves to an exact number of weeks
      if (remainingWorkDays % 5 == 0) daysToAdd -= 2;
    }
  }
  startDate.setDate(startDate.getDate() + daysToAdd);
  return startDate;
}

export function stringDate(date, t) {
  return `${t(`common:shortDays.${date.getUTCDay()}`)} ${t(
    `common:shortMonths.${date.getUTCMonth()}`,
  )} ${date.getUTCDate()}`;
}

export function addDeliveryDate(shippingMethod, seller, when = moment()) {
  const {cut_off} = calculateCutoff(seller, when, when);

  shippingMethod.cut_off = cut_off.utc();
  shippingMethod.eta = addWorkingDaysMoment(moment(cut_off), shippingMethod.speed_days).format(
    'YYYY-MM-DD',
  );

  return shippingMethod;
}

export function calculateCutoff(seller, date = moment(), now = moment.now()) {
  const cut_off_time = !seller || !seller.cut_off_time ? 10 : seller.cut_off_time;
  const cut_off_timezone =
    !seller || !seller.cut_off_timezone ? 'America/Los_Angeles' : seller.cut_off_timezone;

  const cut_off = date.tz(cut_off_timezone).hour(cut_off_time).minute(0).second(0);

  const todayCounts = cut_off > now;
  if (!todayCounts) {
    cut_off.add(1, 'day');
  }
  if (cut_off.day() === 0) {
    cut_off.add(1, 'day');
  }
  if (cut_off.day() === 6) {
    cut_off.add(2, 'day');
  }

  return {cut_off, todayCounts};
}

export function addWorkingDaysMoment(now, days) {
  while (days > 0) {
    now.add(1, 'day');
    if (now.day() !== 6 && now.day() !== 0) {
      days--;
    }
  }
  return now;
}

export function localDate(date = Date.now()) {
  return new Date(date).toLocaleDateString('en-US', {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  });
}

export function localTime(date = Date.now()) {
  return new Date(date).toLocaleTimeString();
}

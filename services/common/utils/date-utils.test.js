import {
  calculateTimeSlots,
  SLOTS_INTERVAL_SEPARATORS,
  addZero,
  formatTimeAsString,
  formatAllHoursAsString,
  formatGMTOffset,
  embeddedToAbsUTC,
  extractTimezoneMinOffsetFromString,
  utcToEmbeddedTZ,
  absUTCtoEmbeddedTZ,
  getTimezoneOffset,
  hasRegularOpenHours,
  timeFrom,
  calculateOpenNow,
  getEmbeddedDateString,
  calculateCutoff,
} from './date-utils';
import moment from 'moment-timezone';

describe('addZero utils', () => {
  it('Empty', () => {
    expect(addZero()).toEqual('00');
  });

  it('With zero', () => {
    expect(addZero(0)).toEqual('00');
  });

  it('With string zero', () => {
    expect(addZero('0')).toEqual('00');
  });

  it('With double zero', () => {
    expect(addZero('00')).toEqual('00');
  });

  it('With 10 string', () => {
    expect(addZero('10')).toEqual('10');
  });

  it('With 8', () => {
    expect(addZero(8)).toEqual('08');
  });

  it('With string 8', () => {
    expect(addZero('08')).toEqual('08');
  });
});

describe('Generating default time slots', () => {
  it('Test without options does 0-24 evry default mins', () => {
    const slots = calculateTimeSlots();
    expect(slots).toEqual(allDaySlots);
  });
});

describe('Generating default time slots', () => {
  it('Test without options does 0-24 evry default mins', () => {
    const slots = calculateTimeSlots(undefined, {bookings: {duration: 0}});
    expect(slots).toEqual([...allDaySlots, '23:45', '24:00']);
  });
});

describe('Generating time slots with options', () => {
  const openHours = [[8, 10], ['9:30', '11:30'], [], ['9:30', '11:30', '12', '13']];

  it('Test with simple range and default duration', () => {
    //'2019-04-07 00:00:00 GMT'
    const slots = calculateTimeSlots(1554595200000, {openHours});
    expect(slots).toEqual(['8:00', '8:15', '8:30', '8:45', '9:00', '9:15', '9:30']);
  });

  it('Test with simple range and extended duration', () => {
    const slots = calculateTimeSlots(1554625201358, {
      openHours,
      bookings: {duration: 45},
    });
    expect(slots).toEqual(['8:00', '8:15', '8:30', '8:45', '9:00', '9:15']);
  });

  it('Test with simple range and extended duration every 10 mins', () => {
    const slots = calculateTimeSlots(1554625201358, {
      openHours,
      bookings: {duration: 45, defaultTimeInterval: 10},
    });
    expect(slots).toEqual(['8:00', '8:10', '8:20', '8:30', '8:40', '8:50', '9:00', '9:10']);
  });

  it('With empty time range for the day', () => {
    //'2019-04-09 00:00:00 GMT'
    expect(calculateTimeSlots(1554768000000, {openHours})).toEqual(allDaySlots);
  });

  it('With string time ranges for the day', () => {
    // '2019-04-08 00:00:00 GMT'
    expect(calculateTimeSlots(1554681600000, {openHours})).toEqual([
      '9:30',
      '9:45',
      '10:00',
      '10:15',
      '10:30',
      '10:45',
      '11:00',
    ]);
  });

  it('With two time ranges for the day', () => {
    // '2019-04-10 00:00:00 GMT'
    expect(calculateTimeSlots(1554854400000, {openHours})).toEqual([
      '9:30',
      '9:45',
      '10:00',
      '10:15',
      '10:30',
      '10:45',
      '11:00',
      SLOTS_INTERVAL_SEPARATORS,
      '12:00',
      '12:15',
      '12:30',
    ]);
  });
});

const allDaySlots = [
  '0:00',
  '0:15',
  '0:30',
  '0:45',
  '1:00',
  '1:15',
  '1:30',
  '1:45',
  '2:00',
  '2:15',
  '2:30',
  '2:45',
  '3:00',
  '3:15',
  '3:30',
  '3:45',
  '4:00',
  '4:15',
  '4:30',
  '4:45',
  '5:00',
  '5:15',
  '5:30',
  '5:45',
  '6:00',
  '6:15',
  '6:30',
  '6:45',
  '7:00',
  '7:15',
  '7:30',
  '7:45',
  '8:00',
  '8:15',
  '8:30',
  '8:45',
  '9:00',
  '9:15',
  '9:30',
  '9:45',
  '10:00',
  '10:15',
  '10:30',
  '10:45',
  '11:00',
  '11:15',
  '11:30',
  '11:45',
  '12:00',
  '12:15',
  '12:30',
  '12:45',
  '13:00',
  '13:15',
  '13:30',
  '13:45',
  '14:00',
  '14:15',
  '14:30',
  '14:45',
  '15:00',
  '15:15',
  '15:30',
  '15:45',
  '16:00',
  '16:15',
  '16:30',
  '16:45',
  '17:00',
  '17:15',
  '17:30',
  '17:45',
  '18:00',
  '18:15',
  '18:30',
  '18:45',
  '19:00',
  '19:15',
  '19:30',
  '19:45',
  '20:00',
  '20:15',
  '20:30',
  '20:45',
  '21:00',
  '21:15',
  '21:30',
  '21:45',
  '22:00',
  '22:15',
  '22:30',
  '22:45',
  '23:00',
  '23:15',
  '23:30',
];

describe('formatGMTOffset util', () => {
  it('empty', () => {
    expect(formatGMTOffset()).toEqual('Z');
  });
  it('zero', () => {
    expect(formatGMTOffset(0)).toEqual('Z');
  });
  it('one hour', () => {
    expect(formatGMTOffset(60)).toEqual('+01:00');
  });
  it('two and a half hour', () => {
    expect(formatGMTOffset(150)).toEqual('+02:30');
  });
  it('minus two and a half hour', () => {
    expect(formatGMTOffset(-150)).toEqual('-02:30');
  });
  it('minus one hour', () => {
    expect(formatGMTOffset(-60)).toEqual('-01:00');
  });
  it('minus 8 mins', () => {
    expect(formatGMTOffset(-8)).toEqual('-00:08');
  });
});

describe('embeddedToAbsUTC util', () => {
  it('localized date with timezone embedded', () => {
    const date = embeddedToAbsUTC('2019-03-03T10:00:00+02:00');
    expect(date.getUTCDate()).toEqual(3);
    expect(date.getUTCMonth()).toEqual(2);
    expect(date.getUTCFullYear()).toEqual(2019);
    expect(date.getUTCHours()).toEqual(10);
  });
});

describe('extractTimezoneMinOffsetFromString', () => {
  it('extracts timezone', () => {
    expect(extractTimezoneMinOffsetFromString('2019-03-03T10:00:00+02:00')).toEqual(120);
    expect(extractTimezoneMinOffsetFromString('2019-03-03 10:00:00 GMT+2')).toEqual(120);
  });
  it('extracts timezone with zero', () => {
    expect(extractTimezoneMinOffsetFromString('2019-03-03T10:00:00+02:00')).toEqual(120);
    expect(extractTimezoneMinOffsetFromString('2019-03-03 10:00:00 GMT+02')).toEqual(120);
  });
  it('extracts timezone with minutes', () => {
    expect(extractTimezoneMinOffsetFromString('2019-03-03 10:00:00 GMT+230')).toEqual(150);
    expect(extractTimezoneMinOffsetFromString('2019-03-03T10:00:00+02:30')).toEqual(150);
  });
  it('extracts timezone with minutes and zero', () => {
    expect(extractTimezoneMinOffsetFromString('2019-03-03 10:00:00 GMT+0230')).toEqual(150);
    expect(extractTimezoneMinOffsetFromString('2019-03-03T10:00:00+02:30')).toEqual(150);
  });
  it('extracts timezone with GMT', () => {
    expect(extractTimezoneMinOffsetFromString('2019-03-03 10:00:00 GMT')).toEqual(0);
    expect(extractTimezoneMinOffsetFromString('2019-03-03T10:00:00Z')).toEqual(0);
  });
  it('extracts timezone with no timezone', () => {
    expect(extractTimezoneMinOffsetFromString('2019-03-03T10:00:00')).toEqual(0);
    expect(extractTimezoneMinOffsetFromString('2019-03-03 10:00:00')).toEqual(0);
  });
});

describe('utcToEmbeddedTZ util', () => {
  it('extracts timezone with no timezone', () => {
    expect(utcToEmbeddedTZ(new Date('2019-03-03 10:00:00 GMT+2'), {offsets: [-120]})).toEqual(
      '2019-03-03T10:00:00+02:00',
    );
  });

  it('extracts timezone with no timezone', () => {
    expect(utcToEmbeddedTZ(new Date('2019-03-03 10:00:00 GMT-2'), {offsets: [120]})).toEqual(
      '2019-03-03T10:00:00-02:00',
    );
  });
});

describe('absUTCtoEmbeddedTZ util', () => {
  it('extracts timezone with no timezone', () => {
    expect(absUTCtoEmbeddedTZ(new Date('2019-03-03 10:00:00 GMT'), {offsets: [-120]})).toEqual(
      '2019-03-03T10:00:00+02:00',
    );
  });

  it('extracts timezone with no timezone', () => {
    expect(absUTCtoEmbeddedTZ(new Date('2019-03-03 10:00:00 GMT'), {offsets: [120]})).toEqual(
      '2019-03-03T10:00:00-02:00',
    );
  });
});

describe('getTimezoneOffset util', () => {
  it('returns 0 if nothing found', () => {
    expect(getTimezoneOffset()).toEqual(0);
  });

  const timezone = {
    offsets: [-60, -120, -60, -120, -60, -120, -60],
    untils: [
      1396141200000,
      1414285200000,
      1427590800000,
      1445734800000,
      1459040400000,
      1477789200000,
      null,
    ],
  };

  it('returns first if lower', () => {
    expect(getTimezoneOffset(timezone, 0)).toEqual(60);
  });

  it('returns second if higher', () => {
    expect(getTimezoneOffset(timezone, 1396141200005)).toEqual(120);
  });

  it('returns last if higher than recorded', () => {
    expect(getTimezoneOffset(timezone)).toEqual(60);
  });
});

describe('hasRegularOpenHours util', () => {
  if (
    ('empty input',
    () => {
      expect(hasRegularOpenHours()).toEqual('false');
    })
  );

  if (
    ('empty null',
    () => {
      expect(hasRegularOpenHours(null)).toEqual('false');
    })
  );

  if (
    ('empty arrayt',
    () => {
      expect(hasRegularOpenHours([])).toEqual('false');
    })
  );

  if (
    ('empty set of arrays',
    () => {
      expect(hasRegularOpenHours([[], []])).toEqual('false');
    })
  );

  if (
    ('arrays with data',
    () => {
      expect(hasRegularOpenHours([[3, 4], []])).toEqual('true');
    })
  );

  if (
    ('arrays with 0 data',
    () => {
      expect(hasRegularOpenHours([[0, 0], []])).toEqual('true');
    })
  );
});

describe('timeFrom util', () => {
  it('in seconds', () => {
    expect(timeFrom(0, 1000)).toEqual({unit: 'seconds', amount: 1, direction: 'past'});
    expect(timeFrom(0, 3000)).toEqual({unit: 'seconds', amount: 3, direction: 'past'});
    expect(timeFrom(0, 3005)).toEqual({unit: 'seconds', amount: 3, direction: 'past'});
    expect(timeFrom(0, 3705)).toEqual({unit: 'seconds', amount: 4, direction: 'past'});
  });

  it('in minutes', () => {
    expect(timeFrom(0, 1000 * 65)).toEqual({unit: 'minutes', amount: 1, direction: 'past'});
    expect(timeFrom(1000 * 125, 0)).toEqual({unit: 'minutes', amount: 2, direction: 'forward'});
  });

  it('in days', () => {
    expect(timeFrom(0, 24 * 60 * 60 * 1000)).toEqual({unit: 'days', amount: 1, direction: 'past'});
    expect(timeFrom(0, 24 * 60 * 60 * 1000 + 1)).toEqual({
      unit: 'days',
      amount: 1,
      direction: 'past',
    });
    expect(timeFrom(0, 3 * 24 * 60 * 60 * 1000)).toEqual({
      unit: 'days',
      amount: 3,
      direction: 'past',
    });
  });

  it('in weeks', () => {
    expect(timeFrom(0, 20 * 24 * 60 * 60 * 1000)).toEqual({
      unit: 'weeks',
      amount: 3,
      direction: 'past',
    });
    expect(timeFrom(10 * 24 * 60 * 60 * 1000, 0)).toEqual({
      unit: 'weeks',
      amount: 1,
      direction: 'forward',
    });
  });

  it('in months', () => {
    expect(timeFrom(0, 45 * 24 * 60 * 60 * 1000)).toEqual({
      unit: 'months',
      amount: 2,
      direction: 'past',
    });
    expect(timeFrom(98 * 24 * 60 * 60 * 1000, 0)).toEqual({
      unit: 'months',
      amount: 3,
      direction: 'forward',
    });
  });

  it('in years', () => {
    expect(timeFrom(0, 370 * 24 * 60 * 60 * 1000)).toEqual({
      unit: 'years',
      amount: 1,
      direction: 'past',
    });
    expect(timeFrom(740 * 24 * 60 * 60 * 1000, 0)).toEqual({
      unit: 'years',
      amount: 2,
      direction: 'forward',
    });
  });
});

describe('calculateOpenNow tests', () => {
  const timezone = {offsets: [60]};
  const openHours = [[], [10, 20], ['10', '20:30'], ['10:30', '12.30', '15:30', '23']];

  it('no input', () => {
    expect(calculateOpenNow()).toEqual(false);
  });

  it('no openHours', () => {
    expect(calculateOpenNow(null, timezone)).toEqual(false);
  });

  it('selecting day with no openHours', () => {
    const d = new Date('2019-07-07 10:00:00Z'); //sunday
    expect(calculateOpenNow(openHours, timezone, d)).toEqual(false);
  });

  it('selecting day that fails becuase of timezone offsets', () => {
    const d = new Date('2019-07-08 10:00:00Z'); //monday
    expect(calculateOpenNow(openHours, timezone, d)).toEqual(false);
  });

  it('selecting day and time within range', () => {
    const d = new Date('2019-07-08 11:00:00Z'); //monday
    expect(calculateOpenNow(openHours, timezone, d)).toEqual(true);
  });

  it('selecting day and time within range with strings', () => {
    const d = new Date('2019-07-09 11:00:00Z'); //tuesday
    expect(calculateOpenNow(openHours, timezone, d)).toEqual(true);
  });

  it('selecting day and time out of range with strings', () => {
    const d = new Date('2019-07-09 10:00:00Z'); //tuesday
    expect(calculateOpenNow(openHours, timezone, d)).toEqual(false);
  });

  it('selecting day and time within range with split service', () => {
    const d = new Date('2019-07-10 11:40:00Z'); //wednesdauy
    expect(calculateOpenNow(openHours, timezone, d)).toEqual(true);
  });

  it('selecting day and time out of range between halfs of split service', () => {
    const d = new Date('2019-07-10 15:00:00Z'); //wednesday
    expect(calculateOpenNow(openHours, timezone, d)).toEqual(false);
  });

  it('selecting day and time within range with split service in second half', () => {
    const d = new Date('2019-07-10 17:40:00Z'); //wednesdauy
    expect(calculateOpenNow(openHours, timezone, d)).toEqual(true);
  });
});

describe('getEmbeddedDateString util', () => {
  it('with space', () => {
    expect(getEmbeddedDateString('2020-02-20 12:23:23')).toEqual('2020-02-20');
  });

  it('with T', () => {
    expect(getEmbeddedDateString('2020-02-20T12:23:23')).toEqual('2020-02-20');
  });
});

describe('calculateCutoff util', () => {
  it('same day', () => {
    const now = moment.tz('2021-03-24 21:00:00', 'America/Los_Angeles');
    const {cut_off, todayCounts} = calculateCutoff(
      {
        cut_off_time: 22,
        cut_off_timezone: 'America/Los_Angeles',
      },
      moment.tz('2021-03-24 23:00:00', 'America/Los_Angeles'),
      now,
    );
    expect(todayCounts).toBeTruthy();
    expect(cut_off > now).toBeTruthy();
  });

  it('next day', () => {
    const now = moment.tz('2021-03-21 21:00:00', 'America/Los_Angeles');
    const {cut_off, todayCounts} = calculateCutoff(
      {
        cut_off_time: 19,
        cut_off_timezone: 'America/Los_Angeles',
      },
      moment.tz('2021-03-21 23:00:00', 'America/Los_Angeles'),
      now,
    );
    expect(todayCounts).toBeFalsy();
    expect(cut_off > now).toBeTruthy();
  });

  it('on saturday', () => {
    const now = moment.tz('2021-03-20 21:00:00', 'America/Los_Angeles');
    const {cut_off, todayCounts} = calculateCutoff(
      {
        cut_off_time: 19,
        cut_off_timezone: 'America/Los_Angeles',
      },
      moment.tz('2021-03-20 23:00:00', 'America/Los_Angeles'),
      now,
    );
    expect(todayCounts).toBeFalsy();
    expect(cut_off > now).toBeTruthy();
    expect(cut_off.date()).toEqual(22);
  });

  it('on sunday', () => {
    const now = moment.tz('2021-03-21 21:00:00', 'America/Los_Angeles');
    const {cut_off, todayCounts} = calculateCutoff(
      {
        cut_off_time: 22,
        cut_off_timezone: 'America/Los_Angeles',
      },
      moment.tz('2021-03-20 23:00:00', 'America/Los_Angeles'),
      now,
    );
    expect(todayCounts).toBeFalsy();
    expect(cut_off > now).toBeTruthy();
    expect(cut_off.date()).toEqual(22);
  });
});

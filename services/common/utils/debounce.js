const debounceCache = {};

export const debounceByKey = (cb, waitingTime, ...keys) => {
  // serialize all params
  const hash = keys.join(' ');

  // if no timeout is waiting
  if (!debounceCache.hasOwnProperty(hash)) {
    // settimeout
    debounceCache[hash] = setTimeout(() => {
      delete debounceCache[hash];
    }, waitingTime);
    // ... and execute
    cb();
  }

  // otherwise just ignore
};

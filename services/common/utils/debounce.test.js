import {debounceByKey} from './debounce';

describe('debouncing fn', () => {
  const cb = jest.fn();

  it('first call', () => {
    debounceByKey(cb, 10, 'test');
    expect(cb.mock.calls.length).toBe(1);
  });

  it('second call ignored', () => {
    debounceByKey(cb, 10, 'test');
    expect(cb.mock.calls.length).toBe(1);
  });

  it('changing key is executed', () => {
    debounceByKey(cb, 10, 'test2');
    expect(cb.mock.calls.length).toBe(2);
  });

  it('second call with another key is ignore', () => {
    debounceByKey(cb, 10, 'test2');
    expect(cb.mock.calls.length).toBe(2);
  });

  it("after timeout it's executed again", () => {
    setTimeout(() => {
      debounceByKey(cb, 10, 'test2');
      expect(cb.mock.calls.length).toBe(3);
    }, 10);
  });
});

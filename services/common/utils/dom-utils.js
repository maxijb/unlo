import isEmpty from 'is-empty';

export const setStyleVars = (elem, vars) => {
  if (!isEmpty(vars) && elem && elem.style) {
    Object.keys(vars).forEach(name => {
      elem.style.setProperty(`--${name}`, vars[name]);
    });
  }
};

export const getVisibilityAPIVars = () => {
  let hidden;
  let visibilityChange;
  if (typeof document.hidden !== 'undefined') {
    // Opera 12.10 and Firefox 18 and later support
    hidden = 'hidden';
    visibilityChange = 'visibilitychange';
  } else if (typeof document.msHidden !== 'undefined') {
    hidden = 'msHidden';
    visibilityChange = 'msvisibilitychange';
  } else if (typeof document.webkitHidden !== 'undefined') {
    hidden = 'webkitHidden';
    visibilityChange = 'webkitvisibilitychange';
  }
  return {hidden, visibilityChange};
};

import {DefaultCurrency} from '@Common/constants/app';
import {StateCodes, StateCodesSet} from '@Common/constants/state-codes';

export function formatOperatorName(operator) {
  if (!operator) {
    return '';
  }

  const op = operator.firstName;
  return !operator.lastName ? op : `${op} ${(operator.lastName || '').substr(0, 1)}.`;
}

export function formatFullAddressTxt(business) {
  if (!business) {
    return '';
  }

  return `${business.address}, ${business.city}
${business.state} ${business.zip} - ${business.country}`;
}

export function formatFullAddressHtml(business) {
  if (!business) {
    return '';
  }

  return [
    `${business.address}, ${business.city}`,
    `${business.state} ${business.zip} - ${business.country}`,
  ]
    .map(t => psmall(t, 'margin-bottom: 0;'))
    .join('');
}

export function p(txt, style) {
  return `
    <p style="word-break: break-word; font-family: 'Montserrat', sans-serif; font-size: 14px; font-weight: normal; margin: 0 0 15px; ${
      style || ''
    }">
    ${txt}
    </p>
  `;
}

export function pbig(txt, style) {
  return `
    <p style="word-break: break-word; font-family: 'Montserrat', sans-serif; font-size: 18px; font-weight: 500; margin: 0 0 18px; ${
      style || ''
    }">
    ${txt}
    </p>
  `;
}
export function psmall(txt, style) {
  return `
    <p style="word-break: break-word; font-family: 'Montserrat', sans-serif; font-size: 12px; font-weight: normal; margin: 0 0 5px; ${
      style || ''
    }">
    ${txt}
    </p>
  `;
}

export function pframe(txt, style) {
  return `
  <p style="background-color: #eeeeee; border: 1px solid #cccccc; border-radius: 4px; padding: 15px; word-break: break-word; font-family: 'Montserrat', sans-serif; font-size: 14px; font-weight: normal; margin: 0 0 15px; ${
    style || ''
  }">
  ${txt}
  </p>
  `;
}

export function textPrice(value, currency = DefaultCurrency) {
  const text = formatPrice(value);
  return `${Number(value) < 0 ? '-' : ''}${currency}${text}`;
}

const formatterDecimals = new Intl.NumberFormat('en-US', {
  //style: 'currency',
  //currency: 'USD',
  minimumFractionDigits: 2,
  maximumFractionDigits: 2,
});

function formatPrice(value) {
  const num = Number(value);
  if (Number.isNaN(num)) {
    return value;
  }
  return formatterDecimals.format(Math.abs(num));
}

export function formatAddress(address) {
  const unit = !address.unit ? '' : Number(address.unit) ? `#${address.unit}` : address.unit;
  return `${address.street_number} ${address.route} ${unit} - ${address.locality} - ${address.administrative_area_level_1} ${address.postal_code} - ${address.country}`;
}

export function formatAddressFirstLine(address) {
  if (!address) {
    return '';
  }
  return `${address.street_number} ${address.route}`;
}

export function formatAddressSecondLine(address) {
  if (!address) {
    return '';
  }
  return !address.unit ? '' : Number(address.unit) ? `#${address.unit}` : address.unit;
}

export function pricePlain(price) {
  if (!price && price !== 0) {
    return '';
  }
  return Number(Number(price).toFixed(2));
}

export const formatState = state => {
  if (!state) {
    return '';
  }
  const st = String(state).split('.').join('').toLowerCase();
  if (StateCodes.hasOwnProperty(st)) {
    return StateCodes[st];
  }
  const sign = state.substr(0, 2).toUpperCase();
  if (StateCodesSet.has(sign)) {
    return sign;
  }
  return state;
};

export const decimalNumber = num => {
  if (!Number(num)) {
    return num;
  }
  return Math.round(num * 100) / 100;
};

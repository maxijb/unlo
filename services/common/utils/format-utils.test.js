import {formatState} from './format-utils';

describe('format state', () => {
  it('cases', () => {
    expect(formatState('GA - jn')).toEqual('GA');
    expect(formatState('GA')).toEqual('GA');
    expect(formatState('ga')).toEqual('GA');
    expect(formatState('georgia')).toEqual('GA');
    expect(formatState('gaga')).toEqual('GA');
  });
});

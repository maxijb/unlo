import React from 'react';
export const noop = () => {};
export const identityFn = a => a;

export const waitMS = ms => {
  return new Promise(resolve => setTimeout(resolve, ms));
};

export const arrayToMap = (array, field = 'id', transform = e => e, transformValue = e => e) => {
  return array.reduce((prev, item) => {
    prev[transform(item[field])] = transformValue(item);
    return prev;
  }, {});
};

export const arrayToMultipleMap = (
  array,
  field = 'id',
  transform = e => e,
  transformValue = t => t,
) => {
  return array.reduce((prev, item) => {
    if (!prev.hasOwnProperty(transform(item[field]))) {
      prev[transform(item[field])] = [];
    }
    prev[transform(item[field])].push(transformValue(item));
    return prev;
  }, {});
};

export const mapToArray = map => {
  return Object.keys(map).map(key => map[key]);
};

export const multiLineTrans = text => {
  return text.split('\n').map((p, i) => (
    <p key={i} style={{marginBottom: '16px'}}>
      {p}
    </p>
  ));
};

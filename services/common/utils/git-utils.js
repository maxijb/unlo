const cp = require('child_process');

module.exports = function () {
  return cp.execSync('git rev-parse HEAD', {encoding: 'utf8'});
};

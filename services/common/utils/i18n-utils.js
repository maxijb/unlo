export function internationalizeOptionsArray(options, t, prefix = '') {
  return options.map(option => ({
    display: t(`${prefix}${option.display || option}`),
    value: option.value || option
  }));
}

export const ipv4ToNumericRange = ip => {
  if (!ip) {
    return 0;
  }

  const parts = ip.split('.');

  if (parts.length !== 4) {
    return 0;
  }

  return Number(parts[3]) + parts[2] * 256 + parts[1] * 256 * 256 + parts[0] * 256 * 256 * 256;
};

export function getIP(req) {
  let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  if (ip.substr(0, 7) == '::ffff:') {
    ip = ip.substr(7);
  }
  return ip;
}

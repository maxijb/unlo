export function safeJsonParse(str) {
  let json = {};
  if (!str) {
    return json;
  }

  if (typeof str !== 'string') {
    return str;
  }

  try {
    json = JSON.parse(str);
  } catch (e) {
    // fail silently
  } finally {
    return json;
  }
}

import {safeJsonParse} from './json';

describe('safeJsonParse function', () => {
  it('empty fallsback to object ok', () => {
    expect(safeJsonParse(null)).toEqual({});
  });

  it('invalid fallsback to object ok', () => {
    expect(safeJsonParse('iji')).toEqual({});
  });

  it('valid is pased', () => {
    expect(safeJsonParse('{"iji": 3}')).toEqual({iji: 3});
  });

  it('if not string returns input', () => {
    expect(safeJsonParse({test: 2})).toEqual({test: 2});
    expect(safeJsonParse(67)).toEqual(67);
  });
});

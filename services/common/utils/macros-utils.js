import {Macros, ActionMacros, MacroInitCharacter, MacroMatchPatter} from '../constants/macros';

export const parseMacro = (message, customMacros) => {
  return isMacro(message, customMacros) || message || '';
};

/* Checks a str message, and determines if it conatins a macro
 * if found, will return the macro, otherwise false
 * @param message {string}
 * @param customMacros {object} dictionary of app (or config) level custom macros that overrides the global macros
 */
export const isMacro = (message, customMacros) => {
  if (!message || typeof message !== 'string') {
    return false;
  }

  const match = message.match(MacroMatchPatter);
  if (!match || match.length < 5) {
    return false;
  }
  const prev = message.substr(0, match.index);
  const macro = match[2];
  const params = processParams(match[4]);

  // App macros
  if ((customMacros || {}).hasOwnProperty(macro)) {
    const resp = customMacros[macro];
    return typeof resp === 'string'
      ? `${prev ? prev + ' ' : ''}${resp}${params ? ' ' + params.toString() : ''}`
      : {...customMacros[macro], prev, params};
  }

  // Global macros
  const action = ActionMacros.get(macro);
  if (action) {
    return {ui: action.ui, config: action.config, prev, params};
  }

  return false;
};

/* Processes parameters added after the macro name
 * Uses "" to find parameters including more than one word
 * @param params {string}
 */
export const processParams = params => {
  if (!params || typeof params !== 'string') {
    return null;
  }

  const str = params.trim();
  const result = [];
  let buffer = '';
  let openQuotes = false;

  for (let i = 0, l = params.length; i < l; i++) {
    const letter = params[i];
    if (letter === ' ' && !openQuotes) {
      result.push(buffer);
      buffer = '';
    } else if (letter == '"') {
      if (!openQuotes) {
        result.push(buffer);
        buffer = '';
      }
      openQuotes = !openQuotes;
    } else {
      buffer += letter;
    }
  }

  result.push(buffer);
  const finalResult = result.filter(it => Boolean(it));

  return finalResult.length ? finalResult : null;
};

export const processMessage = item => {
  if (!item.message) {
    item.message = '';
  }

  if (item.message[0] === '{') {
    try {
      item.message = JSON.parse(item.message);
    } catch (e) {
      // ignore
    }
  }
  if (item.data) {
    try {
      item.data = JSON.parse(item.data);
    } catch (e) {
      // ignore
    }
  }
  return item;
};

export const processInteractionMessage = item => {
  if (item.message && item.message[0] === '{') {
    try {
      item.message = JSON.parse(item.message);
    } catch (e) {
      // ignore
    }
  }
  return item;
};

import {isMacro, processParams, parseMacro} from './macros-utils';

describe('parseMacro util', () => {
  it('empty input should be false', () => {
    expect(parseMacro()).toEqual('');
  });

  it('empty string should be false', () => {
    expect(parseMacro('')).toEqual('');
  });

  it('simple string should be false', () => {
    expect(parseMacro('maxi')).toEqual('maxi');
  });

  it('starting macro to be extracted', () => {
    expect(parseMacro('/rate')).toMatchObject({ui: 'rate', prev: '', params: null});
  });

  it('invalid macro', () => {
    expect(parseMacro('maxi/rate')).toEqual('maxi/rate');
  });

  it('macro in the middle to be extracted', () => {
    expect(parseMacro('maxi /rate')).toMatchObject({ui: 'rate', prev: 'maxi', params: null});
  });

  it('macro in the middle with params', () => {
    expect(parseMacro('maxi /rate esa foto')).toMatchObject({
      ui: 'rate',
      prev: 'maxi',
      params: ['esa', 'foto']
    });
  });

  it('macro in the middle with params', () => {
    expect(parseMacro('maxi /rate "esa foto"')).toMatchObject({
      ui: 'rate',
      prev: 'maxi',
      params: ['esa foto']
    });
  });
});

describe('isMacro util', () => {
  it('empty input should be false', () => {
    expect(isMacro()).toBeFalsy();
  });

  it('empty string should be false', () => {
    expect(isMacro('')).toBeFalsy();
  });

  it('simple string should be false', () => {
    expect(isMacro('maxi')).toBeFalsy();
  });

  it('starting macro to be extracted', () => {
    expect(isMacro('/rate')).toMatchObject({ui: 'rate', prev: '', params: null});
  });

  it('invalid macro', () => {
    expect(isMacro('maxi/rate')).toBeFalsy();
  });

  it('macro in the middle to be extracted', () => {
    expect(isMacro('maxi /rate')).toMatchObject({ui: 'rate', prev: 'maxi', params: null});
  });

  it('macro in the middle with params', () => {
    expect(isMacro('maxi /rate esa foto')).toMatchObject({
      ui: 'rate',
      prev: 'maxi',
      params: ['esa', 'foto']
    });
  });

  it('macro in the middle with params', () => {
    expect(isMacro('maxi /rate "esa foto"')).toMatchObject({
      ui: 'rate',
      prev: 'maxi',
      params: ['esa foto']
    });
  });
});

describe('processParams util', () => {
  it('empty should be null', () => {
    expect(processParams()).toEqual(null);
  });

  it('empty string be null', () => {
    expect(processParams('')).toEqual(null);
  });

  it('simple string be handled', () => {
    expect(processParams('maxi')).toEqual(['maxi']);
  });

  it('simple string be handled', () => {
    expect(processParams('maxi test')).toEqual(['maxi', 'test']);
  });

  it('quoted string should be handled as 1', () => {
    expect(processParams('"maxi test"')).toEqual(['maxi test']);
  });

  it('quoted string among others should be handled as 1', () => {
    expect(processParams('today "maxi test" time')).toEqual(['today', 'maxi test', 'time']);
  });

  it('unterminated quoted string is allowed and handled as 1', () => {
    expect(processParams('jnj "maxi test')).toEqual(['jnj', 'maxi test']);
  });
});

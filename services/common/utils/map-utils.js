export const generateAddress = config => {
  config = config || {};
  let address = `${config.address || ''}${config.address && config.city ? ', ' : ''}${
    config.city || ''
  }\r\n`;
  address += config.zip ? `(${config.zip}) ` : config.state ? ', ' : '';
  address += `${config.state || ''}${config.state && config.country ? ' - ' : ''}${
    config.country || ''
  }`;

  return address;
};

const fields = ['zip', 'city', 'state', 'country'];
export const generateGeolocationAddress = config => {
  const add =
    config.address || config.address2 ? [`${config.address || ''} ${config.address2 || ''}`] : [];
  fields.forEach(field => {
    if (config[field]) {
      add.push(config[field]);
    }
  });
  return add.map(a => a.toString().trim()).join(', ');
};

export const getGoogleMapsLink = (latitude, longitude) => {
  return `https://www.google.com/maps/search/?api=1&query=${latitude},${longitude}`;
};

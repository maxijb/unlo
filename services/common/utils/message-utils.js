import get from 'lodash.get';
import {MessageOwners} from '@Common/constants/socket-status';
import {UserFilesCdnURL} from '@Common/utils/statif-assets-utils';
import {GravatarFallback} from '@Common/constants/app';

export const isMineAsClient = owner => {
  return owner === MessageOwners.client;
};

export const isMineAsOperator = owner => {
  return owner !== MessageOwners.client;
};

export const getAvatar = ({
  owner,
  operators = {},
  options = {},
  user = {},
  includeUserGravatar = false
}) => {
  if (owner === MessageOwners.bot || !owner) {
    return UserFilesCdnURL(options.botImage || options.avatar || GravatarFallback);
  } else if (owner === MessageOwners.client) {
    const email = user && user.email;
    return !includeUserGravatar || !email ? GravatarFallback : {type: 'gravatar', email};
  }

  return UserFilesCdnURL(get(operators, [owner, 'avatar']) || GravatarFallback);
};

export const getOperatorName = ({options = {}, messages = {}, operators = {}, owner}) => {
  if (owner === MessageOwners.bot || !owner) {
    return get(options, 'botName', messages.bot);
  } else if (owner === MessageOwners.client) {
    return messages.you;
  }
  return get(operators, [owner, 'firstName'], messages.operator);
};

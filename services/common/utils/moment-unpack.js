import data from '../constants/moment-data-packed';
import {unpack} from './moment';

function unpackAll() {
  return data.zones.reduce((prev, zone) => {
    const [name] = zone.split('|');
    const unpacked = unpack(zone);
    prev[name] = unpacked;
    return prev;
  }, {});
}

export default {
  unpackAll
};

import {CartStatus, Actors} from '@Common/constants/app';
import {decimalNumber} from './format-utils';

export function getActualPrice(c) {
  if (!c) {
    return null;
  }
  if (typeof c.get === 'function') {
    return c.get('price');
  }
  return c.price;
}

export function getDisplayPrice(c) {
  if (!c) {
    return null;
  }
  if (typeof c.get === 'function') {
    return c.get('display_price') || c.get('price');
  }
  return c.display_price || c.price;
}

export const isReturnCart = product => {
  return [
    CartStatus.returnedToSenderSeller,
    CartStatus.requestedReturn,
    CartStatus.returnLabelSent,
    CartStatus.inTransitReturn,
    CartStatus.returnAwaitingRefund,
    CartStatus.returnedSettled,
  ].includes(product.status);
};

export const canBeCancelled = product =>
  product.type === 'product' && [CartStatus.inOrder].includes(product.status);
export const canBeReturned = product =>
  product.type === 'product' &&
  [CartStatus.inTransit, CartStatus.delivered, CartStatus.withProblems].includes(product.status);

/* PromoCodes are ignored in seller's view */
export const getActualOrderTotals = order => {
  let ship = 0;
  const statusesInt = new Set();
  let shippingDesc = null;
  const track = new Set();
  const trackCarriers = [];
  const shipBySeller = new Map();
  let totProducts = 0;
  const returnLabels = new Set();

  const tot = order.cart.reduce((agg, it) => {
    const price = getActualPrice(it);
    if (it.type === 'shipping') {
      ship += price;
      shippingDesc = it.special_item;
      shipBySeller.set(it.seller_id, it);
    } else if (it.type === 'product') {
      totProducts += price * (it.quantity || 1);
      statusesInt.add(it.status);
      if (it.tracking && !track.has(it.tracking)) {
        track.add(it.tracking);
        trackCarriers.push(it.tracking_carrier);
      }
      if (it.remote_return_label) {
        returnLabels.add(it.remote_return_label);
      }
    } else if (it.type === 'code') {
      return agg;
    }
    return agg + price * (it.quantity || 1);
  }, 0);
  const taxes = tot * (order.tax_rate || 0);

  return {
    subtotal: totProducts,
    tax: taxes,
    total: tot + taxes,
    shippingTotal: ship,
    statuses: Array.from(statusesInt),
    tracking: Array.from(track),
    trackingCarriers: trackCarriers,
    returnLabels: Array.from(returnLabels),
    shippingDescription: shippingDesc,
    shippingBySeller: shipBySeller,
    promoCode: null,
  };
};

/* Calculates the total for billable events.
   @param filterFn allows to filter specific conditions (for example c => c.status === CartStatus.inTransit)
*/
export const getTotalsForBillable = (carts, filterFn = () => true) => {
  const cart_ids = [];
  const cart_products = {};
  const cart_total = carts.reduce((agg, c) => {
    if (c.type === 'code') {
      return agg;
    }
    if (!agg.hasOwnProperty(c.seller_id)) {
      cart_products[c.seller_id] = 0;
      agg[c.seller_id] = 0;
    }

    cart_ids.push(c.id);
    if (c.type === 'product' && filterFn(c)) {
      cart_products[c.seller_id] += c.price * (c.quantity || 1);
    }
    agg[c.seller_id] += c.price * (c.quantity || 1);
    return agg;
  }, {});

  return {cart_ids, cart_products, cart_total};
};

/* PromoCodes are calculated in buyer's view */
export const getDisplayOrderTotals = order => {
  let ship = 0;
  const statusesInt = new Set();
  let shippingDesc = null;
  const track = new Set();
  const shipBySeller = new Map();
  let totProducts = 0;
  let promoCode = null;

  const tot = order.cart.reduce((agg, it) => {
    const price = getDisplayPrice(it);
    if (it.type === 'shipping') {
      ship += price;
      shippingDesc = it.special_item;
      shipBySeller.set(it.seller_id, it);
    } else if (it.type === 'product') {
      totProducts += price * (it.quantity || 1);
      statusesInt.add(it.status);
      if (it.tracking) {
        track.add(it.tracking);
      }
    } else if (it.type === 'code') {
      totProducts += price * (it.quantity || 1);
      promoCode = it;
    }
    return agg + price * (it.quantity || 1);
  }, 0);
  const taxes = tot * (order.tax_rate || 0);

  return {
    subtotal: totProducts,
    tax: taxes,
    total: tot + taxes,
    shippingTotal: ship,
    statuses: Array.from(statusesInt),
    tracking: Array.from(track),
    shippingDescription: shippingDesc,
    shippingBySeller: shipBySeller,
  };
};

export const countReturnsAndCancellations = products => {
  let nrq = 0;
  let cbc = 0;
  (products || []).forEach(p => {
    if (p.type !== 'product') {
      return;
    }
    if (canBeReturned(p)) {
      nrq++;
    }
    if (canBeCancelled(p)) {
      cbc++;
    }
  });
  return {
    canBeReturnedQuantity: nrq,
    canBeCancelledQuantity: cbc,
  };
};

export function getRowValue(row, actor = Actors.seller) {
  return actor === Actors.seller ? getActualRowValue(row) : getDisplayRowValue(row);
}

export function getActualRowValue(row) {
  if (!row) {
    return 0;
  }
  return decimalNumber((getActualPrice(row) || 0) * (row.quantity || 1));
}

export function getDisplayRowValue(row) {
  if (!row) {
    return 0;
  }
  return decimalNumber((getDisplayPrice(row) || 0) * (row.quantity || 1));
}

export function normalizeTrackingCarrier(carrier) {
  const str = (carrier || '').toLowerCase();
  if (str.indexOf('fedex') !== -1) {
    return 'FEDEX';
  }
  if (str.indexOf('usps') !== -1) {
    return 'USPS';
  }
  if (str.indexOf('ups') !== -1) {
    return 'UPS';
  }

  return '';
}

export function getPriceFn(actor = Actors.buyer) {
  return actor === Actors.buyer ? getDisplayPrice : getActualPrice;
}

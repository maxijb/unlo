import get from 'lodash.get';
/* This class creates an immutable ordered hash
 * every time the hash is modified it returns a new object
 * so it must be called this way:
 *
 * const newHash = oldHash.insert({item});
 *
 * To get the elements in order, we can do:
 * newHash.items()
 *
 */

export class OrderedHash {
  constructor(data = [], fn = obj => obj.id, setFn = (obj, id) => ({...obj, id})) {
    if (data instanceof OrderedHash) {
      // this shouldn't be user, will create a new object but keys and values will be the same reference
      // as the previous object
      Object.assign(this, data);
    } else if (data.hasOwnProperty('values') && data.hasOwnProperty('keys')) {
      // if we're passing data to create a new copy of a previous object (immutable)
      this.keys = data.keys;
      this.values = data.values;
      this.fn = fn;
      this.setFn = setFn;
    } else {
      // creating from an array of data
      this.keys = [];
      this.values = {};
      this.fn = fn;
      this.setFn = setFn;

      data.forEach(it => this.insert(it, {values: this.values, keys: this.keys}, false));
    }
  }

  /* Creates a copy of the internal data to keep an immutable approach */
  copyData() {
    return {
      values: {...this.values},
      keys: [...this.keys]
    };
  }

  /* Inserts an element at the end of the queue
   * @can take arrays
   */
  insert(item, copiedData, shouldReturn = true) {
    const items = Array.isArray(item) ? item : [item];

    const {keys, values} = copiedData || this.copyData();

    items.forEach(it => {
      if (it) {
        const key = this.fn(it);
        keys.push(key);
        values[key] = it;
      }
    });

    // we don't return on initializing
    // otherwise we create a new object
    if (shouldReturn) {
      return new OrderedHash({keys, values});
    }
  }

  /* Inserts non-existent elements and replaces (doesn't extend but fully replaces)
   * objects with repeated keys
   * @can take arrays
   */
  upsert(item, copiedData, shouldReturn = true) {
    const items = Array.isArray(item) ? item : [item];

    const {keys, values} = copiedData || this.copyData();

    items.forEach(it => {
      const key = this.fn(it);
      if (!values.hasOwnProperty(key)) {
        keys.push(key);
      }
      values[key] = it;
    });

    // we don't return on initializing
    // otherwise we create a new object
    if (shouldReturn) {
      return new OrderedHash({keys, values});
    }
  }

  insertAndSort(item, sortFn) {
    const copiedData = this.copyData();
    this.insert(item, copiedData, false);
    return this.sort(sortFn, copiedData);
  }

  extendAndSort(id, data, sortFn) {
    const copiedData = this.copyData();
    this.extend(id, data, copiedData, false);
    return this.sort(sortFn, copiedData);
  }

  sort(sortFn, copiedData) {
    const {keys, values} = copiedData || this.copyData();

    return new OrderedHash({
      values,
      keys: !sortFn
        ? keys
        : keys.sort((a, b) => {
            const elementA = values[a];
            const elementB = values[b];
            return sortFn(elementA, elementB);
          })
    });
  }

  /* Inserts an element at the end of the queue
   * @can take arrays
   */
  insertBeforeLast(item, copiedData, shouldReturn = true) {
    const items = Array.isArray(item) ? item : [item];

    const {keys, values} = copiedData || this.copyData();

    items.forEach(it => {
      const key = this.fn(it);
      const previousKey = keys.pop();
      keys.push(key, previousKey);
      values[key] = it;
    });

    // we don't return on initializing
    // otherwise we create a new object
    if (shouldReturn) {
      return new OrderedHash({keys, values});
    }
  }

  /* Renames a key
   * @can take arrays
   */
  rename(oldId, id, newState) {
    const oldIds = Array.isArray(oldId) ? oldId : [oldId];
    const ids = Array.isArray(id) ? id : [id];
    const newStates = Array.isArray(newState) ? newState : [newState];

    if (ids.length !== oldIds.length) {
      console.warn('Keys rename ignored: numbers of keys have different length');
      return this;
    }

    const {values, keys} = this.copyData();

    let hasChanged = false;
    oldIds.forEach((old, i) => {
      const newId = ids[i];

      // if not found or same id ignore
      if (old === newId || !values.hasOwnProperty(old)) {
        return;
      }
      hasChanged = true;

      for (let i = keys.length - 1; i >= 0; i--) {
        if (keys[i] === old) {
          keys[i] = newId;
          break;
        }
      }

      values[newId] = this.setFn(this.values[old], newId);
      delete values[old];

      // this is equivalen to extend without
      if (newStates[i]) {
        this.extend(newId, newStates[i], {values, keys}, false);
      }
    });

    return hasChanged ? new OrderedHash({values, keys}) : this;
  }

  remove(id) {
    const {values, keys} = this.copyData();

    const index = keys.findIndex(it => it == id);
    if (index === -1) {
      return this;
    }

    delete values[id];
    keys.splice(index, 1);

    return new OrderedHash({values, keys});
  }

  removeByFunctionMatch(fn) {
    const {values, keys} = this.copyData();

    const indices = [];
    keys.forEach((key, i) => {
      if (fn(values[key])) {
        indices.push(i);
      }
    });

    if (!indices.length) {
      return this;
    }
    for (let i = indices.length - 1; i >= 0; i--) {
      const key = keys.splice(indices[i], 1)[0];
      delete values[key];
    }

    return new OrderedHash({values, keys});
  }

  moveToTop(id, data) {
    const {keys, values} = this.copyData();

    const index = keys.findIndex(key => key === id);
    if (index === -1 && !data) {
      return this;
    } else if (index !== -1) {
      const removed = keys.splice(index, 1)[0];
      keys.unshift(removed);
    }

    if (data) {
      return this.extend(id, data, {values, keys});
    }

    return new OrderedHash({values, keys});
  }

  moveToBottom(id, data) {
    const {keys, values} = this.copyData();
    const index = keys.findIndex(key => key === id);
    if (index === -1 && !data) {
      return this;
    } else if (index !== -1) {
      const removed = keys.splice(index, 1)[0];
      keys.push(removed);
    }

    if (data) {
      return this.extend(id, data, {values, keys});
    }

    return new OrderedHash({values, keys});
  }

  /* Extends an element from the queue
   * @can take arrays
   */
  extend(id, data, copiedData, shouldReturn = true) {
    const ids = Array.isArray(id) ? id : [id];
    const datas = Array.isArray(data) ? data : [data];

    if (ids.length !== datas.length) {
      console.warn('Keys extend ignored: numbers of keys have different length');
      return this;
    }

    const {values, keys} = copiedData || this.copyData();

    ids.forEach((newId, i) => {
      if (!values.hasOwnProperty(newId)) {
        return this.insert({id: newId, ...datas[i]}, {values, keys}, false);
      }

      values[newId] = {
        ...values[newId],
        ...datas[i]
      };
    });

    if (shouldReturn) {
      return new OrderedHash({keys, values});
    }
  }

  /* Extends an element from the queue
   * @can take arrays
   */
  modify(id, data, copiedData, shouldReturn = true) {
    const ids = Array.isArray(id) ? id : [id];
    const datas = Array.isArray(data) ? data : [data];

    if (ids.length !== datas.length) {
      console.warn('Keys extend ignored: numbers of keys have different length');
      return this;
    }

    const {values, keys} = copiedData || this.copyData();

    let changed = false;
    ids.forEach((newId, i) => {
      if (!values.hasOwnProperty(newId)) {
        return;
      }

      changed = true;
      values[newId] = {
        ...values[newId],
        ...datas[i]
      };
    });

    if (shouldReturn) {
      if (!changed) {
        return this;
      }

      return new OrderedHash({keys, values});
    }
  }

  /* Extends the data field
   * @can take arrays
   */
  extendDataField(id, newData) {
    const ids = Array.isArray(id) ? id : [id];
    const newDatas = Array.isArray(newData) ? newData : [newData];

    let hasChanged = false;
    ids.forEach((newId, i) => {
      const item = this.get(newId);

      if (!item) {
        return;
      }

      hasChanged = true;

      const data = {
        ...item.data,
        ...newDatas[i]
      };
      newDatas[i] = {data};
    });

    return hasChanged ? this.extend(ids, newDatas) : this;
  }

  findFromBottom(fn) {
    const {keys, values} = this;
    for (let i = keys.length - 1; i >= 0; i--) {
      const key = keys[i];
      const obj = values[key];
      if (key && obj && fn(obj, key)) {
        return key;
      }
    }

    return null;
  }

  /* Gets the ordered list of items */
  items() {
    // generates the cache if values have not changed or doesn't exist
    if (!this.cached) {
      this.cached = this.keys.map(key => this.values[key]);
    }
    return this.cached;
  }

  /* gets the item for the id */
  get(id) {
    return this.values[id];
  }

  has(id) {
    return this.values.hasOwnProperty(id);
  }

  getField(id, field) {
    const item = this.get(id);
    return get(item, field, null);
  }

  getLast() {
    return this.get(this.keys[this.keys.length - 1]);
  }

  getFirst() {
    return this.get(this.keys[0]);
  }

  isEmpty() {
    return this.keys.length === 0;
  }

  length() {
    return this.keys.length;
  }
}

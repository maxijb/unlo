import {OrderedHash} from './ordered-hash';

describe('Initializing OrderedHash', () => {
  it('creates data structures', () => {
    const item = new OrderedHash();
    expect(Array.isArray(item.keys)).toBeTruthy();
    expect(typeof item.values === 'object').toBeTruthy();
  });

  const data = [
    {id: 1, content: 'test'},
    {id: 2, content: 'time'}
  ];
  const obj = new OrderedHash(data);
  it('adds data', () => {
    expect(obj.keys).toEqual([1, 2]);
    expect(obj.values).toEqual({
      1: {id: 1, content: 'test'},
      2: {id: 2, content: 'time'}
    });
  });

  it('insert item', () => {
    const newObj = obj.insert({id: -1, content: 'negative'});
    expect(newObj === obj).toBeFalsy();
    expect(newObj.keys).toEqual([1, 2, -1]);
    expect(newObj.values).toEqual({
      1: {id: 1, content: 'test'},
      2: {id: 2, content: 'time'},
      '-1': {id: -1, content: 'negative'}
    });

    // previous object is not modified
    expect(obj.keys).toEqual([1, 2]);
    expect(obj.values).toEqual({
      1: {id: 1, content: 'test'},
      2: {id: 2, content: 'time'}
    });
  });

  it('batch insert multiple items', () => {
    const newObj = obj.insert([
      {id: -1, content: 'negative'},
      {id: 3, content: 'negative'},
      {id: 5, content: 'negative'}
    ]);
    expect(newObj === obj).toBeFalsy();
    expect(newObj.keys).toEqual([1, 2, -1, 3, 5]);
    expect(newObj.values).toEqual({
      1: {id: 1, content: 'test'},
      2: {id: 2, content: 'time'},
      '-1': {id: -1, content: 'negative'},
      3: {id: 3, content: 'negative'},
      5: {id: 5, content: 'negative'}
    });

    // previous object is not modified
    expect(obj.keys).toEqual([1, 2]);
    expect(obj.values).toEqual({
      1: {id: 1, content: 'test'},
      2: {id: 2, content: 'time'}
    });
  });

  it('insertBeforeLast item', () => {
    const newObj = obj.insertBeforeLast({id: -1, content: 'negative'});
    expect(newObj === obj).toBeFalsy();
    expect(newObj.keys).toEqual([1, -1, 2]);
    expect(newObj.values).toEqual({
      1: {id: 1, content: 'test'},
      2: {id: 2, content: 'time'},
      '-1': {id: -1, content: 'negative'}
    });

    // previous object is not modified
    expect(obj.keys).toEqual([1, 2]);
    expect(obj.values).toEqual({
      1: {id: 1, content: 'test'},
      2: {id: 2, content: 'time'}
    });
  });

  it('batch insert multiple items', () => {
    const newObj = obj.insertBeforeLast([
      {id: -1, content: 'negative'},
      {id: 3, content: 'negative'},
      {id: 5, content: 'negative'}
    ]);
    expect(newObj === obj).toBeFalsy();
    expect(newObj.keys).toEqual([1, -1, 3, 5, 2]);
    expect(newObj.values).toEqual({
      1: {id: 1, content: 'test'},
      2: {id: 2, content: 'time'},
      '-1': {id: -1, content: 'negative'},
      3: {id: 3, content: 'negative'},
      5: {id: 5, content: 'negative'}
    });

    // previous object is not modified
    expect(obj.keys).toEqual([1, 2]);
    expect(obj.values).toEqual({
      1: {id: 1, content: 'test'},
      2: {id: 2, content: 'time'}
    });
  });

  it('rename not existing item', () => {
    const result = obj.rename(23, 4);
    expect(result === obj).toBeTruthy;
    expect(result.keys).toEqual([1, 2]);
    expect(result.values).toEqual({
      1: {id: 1, content: 'test'},
      2: {id: 2, content: 'time'}
    });
  });

  it('rename item with the same is a no op, and returns the same object', () => {
    const result = obj.rename(2, 2);
    expect(result === obj).toBeTruthy();
  });

  it('rename item', () => {
    const result = obj.rename(2, 4);
    expect(result !== obj).toBeTruthy();
    expect(result.keys).toEqual([1, 4]);
    expect(result.values).toEqual({
      1: {id: 1, content: 'test'},
      4: {id: 4, content: 'time'}
    });

    expect(obj.keys).toEqual([1, 2]);
    expect(obj.values).toEqual({
      1: {id: 1, content: 'test'},
      2: {id: 2, content: 'time'}
    });
  });

  it('batch rename items', () => {
    const input = new OrderedHash([
      {id: 1, content: 'test'},
      {id: 2, content: 'time'},
      {id: 3, content: 'third'}
    ]);
    const result = input.rename([2, 1], [4, 5]);
    expect(result !== input).toBeTruthy();
    expect(result.keys).toEqual([5, 4, 3]);
    expect(result.values).toEqual({
      5: {id: 5, content: 'test'},
      4: {id: 4, content: 'time'},
      3: {id: 3, content: 'third'}
    });

    expect(input.keys).toEqual([1, 2, 3]);
    expect(input.values).toEqual({
      1: {id: 1, content: 'test'},
      2: {id: 2, content: 'time'},
      3: {id: 3, content: 'third'}
    });
  });

  it('extends item', () => {
    const result = obj.extend(2, {data: 3});
    expect(result !== obj).toBeTruthy();
    expect(result.keys).toEqual([1, 2]);
    expect(result.values).toEqual({
      1: {id: 1, content: 'test'},
      2: {id: 2, content: 'time', data: 3}
    });
  });

  it('batch extends items', () => {
    const result = obj.extend([2, 1], [{data: 3}, {data: 'new'}]);
    expect(result !== obj).toBeTruthy();
    expect(result.keys).toEqual([1, 2]);
    expect(result.values).toEqual({
      1: {id: 1, content: 'test', data: 'new'},
      2: {id: 2, content: 'time', data: 3}
    });
  });

  it('batch extends insterts non-existing items', () => {
    const result = obj.extend([2, 1, 67], [{data: 3}, {data: 'new'}, {isNew: true}]);
    expect(result !== obj).toBeTruthy();
    expect(result.keys).toEqual([1, 2, 67]);
    expect(result.values).toEqual({
      1: {id: 1, content: 'test', data: 'new'},
      2: {id: 2, content: 'time', data: 3},
      67: {id: 67, isNew: true}
    });
  });

  it('extends null item insers new object', () => {
    const result = obj.extend(4243, {data: 3});
    expect(result === obj).toBeFalsy();
    expect(result.keys).toEqual([1, 2, 4243]);
    expect(result.values).toEqual({
      1: {id: 1, content: 'test'},
      2: {id: 2, content: 'time'},
      4243: {id: 4243, data: 3}
    });

    expect(obj.values).toEqual({
      1: {id: 1, content: 'test'},
      2: {id: 2, content: 'time'}
    });
  });

  it('modify item', () => {
    const result = obj.modify(2, {data: 3});
    expect(result !== obj).toBeTruthy();
    expect(result.keys).toEqual([1, 2]);
    expect(result.values).toEqual({
      1: {id: 1, content: 'test'},
      2: {id: 2, content: 'time', data: 3}
    });
  });

  it('batch modify items', () => {
    const result = obj.modify([2, 1], [{data: 3}, {data: 'new'}]);
    expect(result !== obj).toBeTruthy();
    expect(result.keys).toEqual([1, 2]);
    expect(result.values).toEqual({
      1: {id: 1, content: 'test', data: 'new'},
      2: {id: 2, content: 'time', data: 3}
    });
  });

  it('batch modify ignores non-existing items', () => {
    const result = obj.modify([2, 1, 67], [{data: 3}, {data: 'new'}, {isNew: true}]);
    expect(result !== obj).toBeTruthy();
    expect(result.keys).toEqual([1, 2]);
    expect(result.values).toEqual({
      1: {id: 1, content: 'test', data: 'new'},
      2: {id: 2, content: 'time', data: 3}
    });
  });

  it('modify null item coesnt create new object', () => {
    const result = obj.modify(4243, {data: 3});
    expect(result === obj).toBeTruthy();
    expect(result.keys).toEqual([1, 2]);
    expect(result.values).toEqual({
      1: {id: 1, content: 'test'},
      2: {id: 2, content: 'time'}
    });

    expect(obj.values).toEqual({
      1: {id: 1, content: 'test'},
      2: {id: 2, content: 'time'}
    });
  });

  it('extendsAndSorts doesnt sort if no function is provided', () => {
    const result = obj.extendAndSort(2, {data: 3, content: 'a'});
    expect(result !== obj).toBeTruthy();
    expect(result.keys).toEqual([1, 2]);
    expect(result.values).toEqual({
      1: {id: 1, content: 'test'},
      2: {id: 2, content: 'a', data: 3}
    });
  });

  it('extendsAndSorts sorts when function is provided', () => {
    const result = obj.extendAndSort(2, {data: 3, content: 'a'}, (a, b) =>
      a.content > b.content ? 1 : -1
    );
    expect(result !== obj).toBeTruthy();
    expect(result.keys).toEqual([2, 1]);
    expect(result.values).toEqual({
      1: {id: 1, content: 'test'},
      2: {id: 2, content: 'a', data: 3}
    });
  });

  it('returns the items array of objects in order', () => {
    const a = obj.items();
    const b = obj.items();
    const c = obj.items();
    expect(a).toEqual([
      {id: 1, content: 'test'},
      {id: 2, content: 'time'}
    ]);
    expect(a === b && b === c).toBeTruthy();

    const newObj = obj.insert({id: 5, content: 5});
    const d = newObj.items();
    const e = newObj.items();
    expect(d === c).toBeFalsy();
    expect(d === e).toBeTruthy();
    expect(d).toEqual([
      {id: 1, content: 'test'},
      {id: 2, content: 'time'},
      {id: 5, content: 5}
    ]);
  });
});

describe('rename and extend in one op', () => {
  const obj = new OrderedHash([
    {id: 1, content: 'a'},
    {id: 2, content: 'b'}
  ]);
  const result = obj.rename(2, 3, {status: 'valid'});
  expect(result.items()).toEqual([
    {id: 1, content: 'a'},
    {id: 3, content: 'b', status: 'valid'}
  ]);
});

describe('upsert', () => {
  const obj = new OrderedHash([
    {id: 1, content: 'a'},
    {id: 2, content: 'b'}
  ]);

  it('replaces existing objects', () => {
    const result = obj.upsert({id: 1, status: 'valid'});
    expect(result.items()).toEqual([
      {id: 1, status: 'valid'},
      {id: 2, content: 'b'}
    ]);
  });

  it('adds non-existent objects', () => {
    const result = obj.upsert({id: 3, status: 'valid'});
    expect(result.items()).toEqual([
      {id: 1, content: 'a'},
      {id: 2, content: 'b'},
      {id: 3, status: 'valid'}
    ]);
  });

  it('it works with arrays', () => {
    const result = obj.upsert([
      {id: 3, status: 'valid'},
      {id: 1, content: 'c'}
    ]);
    expect(result.items()).toEqual([
      {id: 1, content: 'c'},
      {id: 2, content: 'b'},
      {id: 3, status: 'valid'}
    ]);
  });
});

describe('insertAndSort in one op', () => {
  const obj = new OrderedHash([
    {id: 1, content: 1},
    {id: 2, content: 4}
  ]);
  const result = obj.insertAndSort({id: 3, content: 2}, (a, b) => (a.content > b.content ? 1 : -1));
  expect(result.items()).toEqual([
    {id: 1, content: 1},
    {id: 3, content: 2},
    {id: 2, content: 4}
  ]);
});

describe('insertAndSort with strings', () => {
  const obj = new OrderedHash([
    {id: 1, content: 'a'},
    {id: 2, content: 'd'}
  ]);
  const result = obj.insertAndSort({id: 3, content: 'b'}, (a, b) =>
    a.content > b.content ? 1 : -1
  );
  expect(result.items()).toEqual([
    {id: 1, content: 'a'},
    {id: 3, content: 'b'},
    {id: 2, content: 'd'}
  ]);
});

describe('insertAndSort adding multiple', () => {
  const obj = new OrderedHash([
    {id: 1, content: 'a'},
    {id: 2, content: 'd'}
  ]);
  const result = obj.insertAndSort(
    [
      {id: 3, content: 'b'},
      {id: 5, content: 'j'}
    ],
    (a, b) => (a.content > b.content ? 1 : -1)
  );
  expect(result.items()).toEqual([
    {id: 1, content: 'a'},
    {id: 3, content: 'b'},
    {id: 2, content: 'd'},
    {id: 5, content: 'j'}
  ]);
});

describe('insertAndSort with empty initial array', () => {
  const obj = new OrderedHash();
  const result = obj.insertAndSort({id: 3, content: 'b'}, (a, b) =>
    a.content > b.content ? 1 : -1
  );
  expect(result.items()).toEqual([{id: 3, content: 'b'}]);
});

describe('moveToTop', () => {
  const obj = new OrderedHash([
    {id: 1, content: 'a'},
    {id: 2, content: 'b'},
    {id: 3, content: 'c'}
  ]);

  it('if not found returns same object', () => {
    const result = obj.moveToTop(45);
    expect(result === obj).toBeTruthy();
  });

  it('when found moves to top', () => {
    const result = obj.moveToTop(2);
    expect(result === obj).toBeFalsy();
    expect(result.items()).toEqual([
      {id: 2, content: 'b'},
      {id: 1, content: 'a'},
      {id: 3, content: 'c'}
    ]);
  });

  it('when found and data is supplied, moves to top and extends object', () => {
    const result = obj.moveToTop(3, {valid: true});
    expect(result === obj).toBeFalsy();
    expect(result.items()).toEqual([
      {id: 3, content: 'c', valid: true},
      {id: 1, content: 'a'},
      {id: 2, content: 'b'}
    ]);
    expect(obj.items()).toEqual([
      {id: 1, content: 'a'},
      {id: 2, content: 'b'},
      {id: 3, content: 'c'}
    ]);
  });
});

describe('moveToBottom', () => {
  const obj = new OrderedHash([
    {id: 1, content: 'a'},
    {id: 2, content: 'b'},
    {id: 3, content: 'c'}
  ]);

  it('if not found returns same object', () => {
    const result = obj.moveToBottom(45);
    expect(result === obj).toBeTruthy();
  });

  it('when found moves to bottom', () => {
    const result = obj.moveToBottom(2);
    expect(result === obj).toBeFalsy();
    expect(result.items()).toEqual([
      {id: 1, content: 'a'},
      {id: 3, content: 'c'},
      {id: 2, content: 'b'}
    ]);
  });

  it('when found and data is supplied, moves to bottom and extends object', () => {
    const result = obj.moveToBottom(3, {valid: true});
    expect(result === obj).toBeFalsy();
    expect(result.items()).toEqual([
      {id: 1, content: 'a'},
      {id: 2, content: 'b'},
      {id: 3, content: 'c', valid: true}
    ]);
  });
});

describe('extendDataField', () => {
  const obj = new OrderedHash([
    {id: 1, content: 'a'},
    {id: 2, content: 'b'},
    {id: 3, content: 'c', data: {val: 12, yes: true}}
  ]);

  it('if id not found returns same object', () => {
    const result = obj.extendDataField(45);
    expect(result === obj).toBeTruthy();
  });

  it('if item has no data its added', () => {
    const result = obj.extendDataField(2, {test: 34});
    expect(result === obj).toBeFalsy();
    expect(result.get(2)).toEqual({id: 2, content: 'b', data: {test: 34}});
    expect(obj.get(2)).toEqual({id: 2, content: 'b'});
  });

  it('when data key already existed in the object, it is merged', () => {
    const result = obj.extendDataField(3, {yes: false});
    expect(result === obj).toBeFalsy();
    expect(result.get(3)).toEqual({id: 3, content: 'c', data: {val: 12, yes: false}});
  });

  it('batch editions and merging', () => {
    const result = obj.extendDataField([3, 1], [{yes: false}, {no: true}]);
    expect(result === obj).toBeFalsy();
    expect(result.get(3)).toEqual({id: 3, content: 'c', data: {val: 12, yes: false}});
    expect(result.get(1)).toEqual({id: 1, content: 'a', data: {no: true}});
  });
});

describe('getField', () => {
  const obj = new OrderedHash([
    {id: 1, content: 'a'},
    {id: 2, content: 'b'},
    {id: 3, content: 'c', data: {val: 12, yes: true}}
  ]);

  it('if id not found returns null', () => {
    const result = obj.getField(45, 'test');
    expect(result).toEqual(null);
  });

  it('if found returns field', () => {
    const result = obj.getField(1, 'content');
    expect(result).toEqual('a');
  });

  it('if found array returns field', () => {
    const result = obj.getField(1, ['content']);
    expect(result).toEqual('a');
  });

  it('if found longer array returns field', () => {
    const result = obj.getField(3, ['data', 'val']);
    expect(result).toEqual(12);
  });

  it('if found complex query returns field', () => {
    const result = obj.getField(3, 'data.val');
    expect(result).toEqual(12);
  });
});

describe('removeByFunctionMatch', () => {
  const obj = new OrderedHash([
    {id: 1, content: 'a'},
    {id: 3, content: 'b'},
    {id: 2, content: 'c'}
  ]);

  it('finding non-existent object returns same object', () => {
    const result = obj.removeByFunctionMatch(x => x.id === 1000);
    expect(result === obj).toBeTruthy();
  });

  it('when an item is found a new object without that key will be returned', () => {
    const result = obj.removeByFunctionMatch(x => x.id === 2);
    expect(result === obj).toBeFalsy();
    expect(result.items()).toEqual([
      {id: 1, content: 'a'},
      {id: 3, content: 'b'}
    ]);
    expect(obj.items()).toEqual([
      {id: 1, content: 'a'},
      {id: 3, content: 'b'},
      {id: 2, content: 'c'}
    ]);
  });
});

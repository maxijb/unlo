export const getLongProductName = ({name, color, capacity}) => {
  return `${name} ${capacity || ''} ${color ? ` - ${color}` : ''}`;
};

export const getShippingMethodDescriptionSeller = (method, t) => {
  if (method?.name) {
    return method?.name;
  }
  return getShippingMethodDescription(method, t);
};
export const getShippingMethodDescription = (method, t) => {
  return t('common:XdayShipping', {
    ...method,
    carrier: t(`common:${method.carrier}`),
    context: `${!method.carrier ? 'nocarrier' : ''}${!Number(method.price) ? 'free' : ''}`,
  });
};

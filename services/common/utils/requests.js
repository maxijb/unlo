import {ErrorNames, formatError} from '@Common/constants/errors';

/* Get payload from one request, regardless of its method
 * TODO: sanitize data to avoid XSS and stuff
 */
export function getRequestQuery(req) {
  return {
    ...(req.params || {}),
    ...(req.query || {}),
    ...(req.body || {})
  };
}

/* Uows are unique ids for each requests
 * Format: // SessionId + randon number
 *  $timestamp.u$userId.$randonNumberUpTo5Digits.$randonNumberUpTo5Digits
 */
export function createUow(sessionId, isPublic) {
  return `${isPublic ? 'p' : ''}${sessionId}r${new Date().getTime()}.${Math.ceil(
    Math.random() * 100000
  )}`;
}

export function safeMiddleware(handler) {
  return async function (req, res, next) {
    try {
      await handler(req, res, next);
    } catch (e) {
      handleError(e, req, res);
    }
  };
}

/* Handle (log and return) errors in API requests */
export function handleError(error, req, res, statusCode) {
  // sometimes errors are already handled by the middleware itself
  if (res.headersSent) {
    return;
  }

  if (statusCode) {
    res.status(statusCode).send(formatError(error));
  } else if (error.name === ErrorNames.RequestError) {
    res.status(400).send(formatError(error));
  } else {
    req.Logger.error(error);
    const output = {error: error.name, message: error.message};
    if (process.env.NODE_ENV !== 'production') {
      output.stack = error.stack;
    }
    res.status(500).send(output);
  }
}

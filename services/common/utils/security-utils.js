export function obfuscateEmail(email) {
  if (!email) {
    return '';
  }

  let [name, domain = ''] = email.split('@');
  name = `${name[0]}***`;

  if (!domain) {
    return name;
  }

  const parts = domain.split('.');
  const main = `${name}@${domain[0]}***`;
  return parts.length > 1 ? `${main}.${parts[parts.length - 1]}` : main;
}

export function obfuscateEmailForDisplay(email) {
  if (!email) {
    return '';
  }
  let [name, domain = ''] = email.split('@');
  return `${name.substr(0, 5)}...`;
}

export function obfuscateName(name) {
  if (!name) {
    return '';
  }

  return name
    .split(' ')
    .map(word => `${word[0]}***`)
    .join(' ');
}

export function obfuscatePhone(phone) {
  if (!phone) {
    return '';
  }
  if (typeof phone !== 'string') {
    phone = phone.toString();
  }
  return `${phone[0]}***${phone.substr(-3)}`;
}

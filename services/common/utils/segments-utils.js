export function renderSegments({customer, segments, t}) {
  return (customer.segments || [])
    .map(id => {
      const segment = segments.get(id);
      return (segment && segment.name) || t('unknown');
    })
    .join(', ');
}

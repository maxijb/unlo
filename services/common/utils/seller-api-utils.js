import {CartStatus, CartStatusForApi} from '@Common/constants/app';
import {arrayToMap} from '@Common/utils/generic-utils';
import {decimalNumber} from '@Common/utils/format-utils';

import {getActualOrderTotals} from '@Common/utils/order-utils';

export const getNumericStatusFromSellerEnum = val => {
  for (const [key, value] of Object.entries(CartStatusForApi)) {
    if (value === val) {
      return Number(key);
    }
  }
  return null;
};

export const parseStatusValues = val => {
  return !val ? null : val.split(',').map(getNumericStatusFromSellerEnum);
};

export function getRetailerPrice(price) {
  return decimalNumber(price * 0.9 * 1.015);
}

export async function formatOrderForRetailer(o, req, seller_id) {
  const pre = await formatOrder(o, req, seller_id);
  pre.cart = pre.cart.map(c => {
    delete c.source_id;
    delete c.sku;
    delete c.tax;
    c.price = getRetailerPrice(c.price);
    c.unit_price = getRetailerPrice(c.unit_price);
    return c;
  });
  delete pre.subtotal;
  delete pre.total;
  delete pre.tax;
  delete pre.tax_rate;
  return pre;
}

export async function formatOrder(o, req, seller_id) {
  const shippingIndex = o.cart.findIndex(c => c.type === 'shipping');

  let {shippingMethod} = await req.Models.Order.getShippingMethodForSellerFromCart(
    o.cart,
    seller_id || req.seller_id,
  );

  const allCarts = [...o.cart];
  let shipping = null;
  if (shippingIndex !== -1) {
    shipping = o.cart.splice(shippingIndex, 1)[0];
  } else {
    shipping = o.cart[0];
  }

  if (o.cart.length) {
    const prods = await req.Models.Product.findAll({
      where: {id: o.cart.map(c => c.product_id)},
      raw: true,
    });
    const products = arrayToMap(prods);

    o.cart = o.cart.map(c => {
      c.incredible_id = products[c.product_id]?.incredible_id;
      c.sku = products[c.product_id]?.source_id;
      return c;
    });
  }

  o.shipping = shippingMethod;

  const {subtotal, total, tax} = getActualOrderTotals({
    ...o,
    cart: allCarts,
  });

  o.subtotal = decimalNumber(subtotal);
  o.total = decimalNumber(total);
  o.tax = decimalNumber(tax);

  delete o.address_id;
  delete o.parent_id;
  delete o.shipping_address_id;
  delete o.type;
  delete o.status;
  delete o.user_id;

  o.cart = o.cart.filter(c => c.type === 'product');
  o.cart.forEach(c => {
    delete c.order_id;
    delete c.seller_id;
    delete c.type;
    delete c.id;
    delete c.special_item;
    c.status = CartStatusForApi[c.status];
    c.unit_price = c.price;
    c.price = decimalNumber(c.price * c.quantity);
    c.tax = decimalNumber(c.unit_price * c.quantity * o.tax_rate);
  });

  return o;
}

import {StaticFilesFolder} from '@Common/constants/app';

const Config = {};

export const initCDN = config => {
  Config.cdnDomain = `${config.protocol}://${config.cdnDomain}`;
  Config.userFilesCdnDomain = `${config.protocol}://${config.cdnDomain}`;
  if (typeof process !== 'undefined') {
    process.cdnDomain = Config.cdnDomain;
    process.userFilesCdnDomain = Config.userFilesCdnDomain;
  }
};

export const cdnURL = url => {
  const userFilesCdnDomain =
    typeof process !== 'undefined' && process.cdnDomain ? process.cdnDomain : Config.cdnDomain;
  const condition = typeof url !== 'string' || url.match(/^https?:\/\//);
  return condition ? url : `${Config.cdnDomain || ''}${url}`;
};

export const UserFilesCdnURL = url => {
  const userFilesCdnDomain =
    typeof process !== 'undefined' && process.userFilesCdnDomain
      ? process.userFilesCdnDomain
      : Config.userFilesCdnDomain;
  return typeof url !== 'string' || url.match(/^https?:\/\//)
    ? url
    : `${userFilesCdnDomain || ''}${
        url.startsWith(`/${StaticFilesFolder}/`) ? '' : `/${StaticFilesFolder}/`
      }${url}`;
};

const HeroImage = [
  'signup-bg.jpg',
  'other-bg.jpg',
  'other3-bg.jpg',
  'other2-bg.jpg',
  'login-bg.jpg',
];

export const getHeroImg = () => {
  const img = HeroImage[Math.floor(Math.random() * HeroImage.length)];
  return cdnURL(`/static/photos/${img}`);
};

/* Replace resized image folder to big
 * Used for avatars in email templates
 */
export const ReplaceBigImage = str => {
  return str.replace('/resized/', '/big/');
};

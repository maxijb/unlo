import {capitalizeFirst} from './str-utils';

describe('capitalizeFirst util', () => {
  it('all cases', () => {
    expect(capitalizeFirst()).toEqual('');
    expect(capitalizeFirst('')).toEqual('');
    expect(capitalizeFirst('test')).toEqual('Test');
    expect(capitalizeFirst('test test')).toEqual('Test test');
  });
});

import TimezonesData from '@Common/constants/timezones-data';

export const DefaultTimezone = TimezonesData.zones['Etc/UTC'];

export function getUnpackedTimezone(timezone) {
  if (!timezone) {
    return DefaultTimezone.unpacked;
  }

  if (TimezonesData.zones.hasOwnProperty(timezone)) {
    return TimezonesData.zones[timezone].unpacked;
  }

  if (TimezonesData.links.hasOwnProperty(timezone)) {
    const tz = TimezonesData.links[timezone];
    return {...TimezonesData.zones[tz].unpacked, name: timezone};
  }

  return DefaultTimezone.unpacked;
}

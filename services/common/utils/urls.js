import get from 'lodash.get';
import decamelize from 'decamelize';
import {API_PREFIX, TrackingLinks} from '../constants/app';
import {dateToGoogleFormat} from '@Common/utils/date-utils';
import {formatFullAddressTxt} from '@Common/utils/format-utils';
import QS from 'qs';

// TODO: support dynamic urls in API
export function formatUri(uri, prefix = API_PREFIX) {
  const path = decamelize(uri, '-').replace(/\./g, '/');
  return `/${prefix}/${path}`;
}

export function getWindowURL() {
  if (typeof window === 'undefined') {
    return '';
  }

  return window.location.pathname + window.location.search;
}

export function slugify(str) {
  return str
    .toLowerCase()
    .replace(/-/g, '')
    .replace(/ +(?= )/g, '')
    .replace(/\s/g, '-');
}

// ----------------------------- CREATE FRONT WEB APP URLs ------------------

const PageURLResolvers = {
  index: () => '/',
  index2: () => '/index2',
  cart: query => {
    return query.promoCode || query.cid ? `/cart?${QS.stringify(query)}` : '/cart';
  },
  product: ({product, t}) => {
    return `/product/${product.category_id || product.id}/${slugify(
      t('common:productSlug', {product: product.name}),
    )}`;
  },
  category: ({product, t}) => {
    return `/category/${product.category_id || product.id}/${slugify(
      t('common:productSlug', {product: product.name}),
    )}`;
  },

  variant: ({product, description = null, attributes = {}, t}) => {
    const name = description
      ? description
      : [
          t(product.name),
          t(attributes.capacity),
          t(attributes.color),
          t(attributes.appearance),
          t(attributes.carrier),
        ]
          .filter(n => !!n)
          .join(' ');

    return `/product/${product.category_id}-${product.id}/${slugify(
      t('common:productSlug', {product: name}),
    )}`;
  },
  dashboard: (params = {}) => {
    const {action} = params;
    const base = '/dashboard';
    return action ? `${base}?action=${action}` : base;
  },
  welcome: (params = {}) => {
    const {action} = params;
    const base = '/dashboard/welcome';
    return action ? `${base}?action=${action}` : base;
  },
  wizard: ({step, app}) => {
    const base = '/dashboard/wizard';
    const qs = [];
    if (step) {
      qs.push(`step=${step}`);
    }
    if (app) {
      qs.push(`app=${app}`);
    }
    return qs.length ? `${base}?${qs.join('&')}` : base;
  },
  bookings: () => '/dashboard/calendar',
  chat: ({publicId, status, search}) => {
    const base = '/dashboard/chat';
    const qs = [];
    if (publicId) {
      qs.push(`publicId=${publicId}`);
      if (!status) {
        qs.push('status=all');
      }
    }
    if (status) {
      qs.push(`status=${status}`);
    }
    if (search) {
      qs.push(`search=${search}`);
    }
    return qs.length ? `${base}?${qs.join('&')}` : base;
  },
  help: () => '/dashboard/help',
  terms: () => '/terms-of-use',
  signin: () => '/signin',
  settings: () => '/dashboard/settings',
  account: () => '/dashboard/account',
  teammates: () => '/dashboard/account/teammates',
  invite: token => `/dashboard/invite?token=${token}`,
};

export function absoluteGetPageLink(page, params = {}, config) {
  return `${config.protocol}://${config.domain}${getPageLink(page, params)}`;
}

export function getPageLink(page, params = {}) {
  const baseUrl = PageURLResolvers[page](params);

  if (params.qs) {
    const separator = baseUrl.indexOf('?') === -1 ? '?' : '&';
    const queryString = QS.stringify(params.qs);
    return `${baseUrl}${separator}${queryString}`;
  }
  return baseUrl;
}

export function parseFragment(hash) {
  hash = !hash ? '' : hash.substr(1);

  return hash.split('&').reduce(function (result, item) {
    var parts = item.split('=');
    result[parts[0]] = parts[1];
    return result;
  }, {});
}

// ---------------------------------- CALENDAR LINKS ---------------------------

export function getGoogleCalendarLink(utcStartDate, utcEndDate, options) {
  return getGoogleCalendarLinkFromStr({
    strDate: dateToGoogleFormat(utcStartDate, options.timezone),
    strDate2: dateToGoogleFormat(utcEndDate, options.timezone),
    address: formatFullAddressTxt(options.business),
    timezoneName: options.timezone.name,
    options,
  });
}

export function getGoogleCalendarLinkFromStr({strDate, strDate2, timezoneName, address, options}) {
  return `https://calendar.google.com/calendar/r/eventedit?text=${encodeURIComponent(
    `Reservation at ${get(options, 'business.name', options.name)}!`,
  )}&dates=${strDate}/${strDate2}&ctz=${timezoneName}&location=${encodeURIComponent(
    address || formatFullAddressTxt(options.business),
  )}`;
}

export function getBrightpearlRedirectURI() {
  return 'https://incrediblephones.com/portal/settings?inc-action=brightpearl-redirect';
}

export function getBrightpearlOathURL({account_name, connection_id}) {
  return `https://oauth.brightpearl.com/authorize/${account_name}?response_type=code&client_id=incredible-app&redirect_uri=${getBrightpearlRedirectURI()}&state=${connection_id}`;
}

export function getPostCarrierTrackingUrl(carrier, number) {
  if (carrier && number && TrackingLinks.hasOwnProperty(carrier)) {
    return TrackingLinks[carrier].replace('{{number}}', number) || '';
  }
  return '';
}

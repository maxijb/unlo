import {formatUri} from './urls';

describe('formatUri converter', () => {
  it('should decamelize, and replace dots', () => {
    expect(formatUri('myT.yourT')).toEqual('/api/my-t/your-t');
  });
});

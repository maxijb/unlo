export function ValidateForm(input, validations) {
  const errors = {};
  Object.keys(validations).forEach(field => {
    let error = null;
    validations[field].forEach(fn => {
      error = error || Validators[fn](field, input);
    });

    if (error) {
      errors[field] = error;
    }
  });

  return errors;
}

/* ----------------------------- Validators ------------------------ */

const Regexs = {
  validPassword: /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*/,
  validEmail: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  validUrl: /^(https?|ftp):\/\/[-A-Za-z0-9+&@#\/%?=~_|!:,.;]+[-A-Za-z0-9+&@#\/%=~_|]/,
};

export const Validators = {
  isRequired: (field, input) => {
    return input[field] || input[field] === 0 || input[field] === '0' ? null : 'fieldRequired';
  },

  validGoogleAddress: (field, input) => {
    return input[field] ? null : 'googleAddressInvalid';
  },

  moreThanZero: (field, input) => {
    return Number(input[field]) > 0 ? null : 'cannotBeZero';
  },

  validPaymentType: (field, input) => {
    return input[field] ? null : 'validPaymentType';
  },

  validCard: (field, input) => {
    return input[field] ? null : 'validCard';
  },

  validPassword: (field, input) => {
    const password = input[field];
    if (!password || password.length < 8) {
      return 'passwordMin8Chars';
    }

    return Regexs.validPassword.test(password) ? null : 'invalidPasswrod';
  },

  validEmail: (field, input) => {
    return Regexs.validEmail.test(input[field]) ? null : 'invalidEmail';
  },

  validUrl: (field, input) => {
    return Regexs.validUrl.test(input[field]) ? null : 'invalidUrl';
  },

  isMultiValidEmail: (field, input) => {
    const addresses = input[field]
      .split(',')
      .map(address => (address || '').trim())
      .filter(address => Boolean(address));

    return addresses.length &&
      addresses.every(address => !Validators.validEmail('address', {address}))
      ? null
      : 'invalidEmail';
  },
};

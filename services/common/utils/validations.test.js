import {ValidateForm} from './validations';

describe('ValidateForm function', () => {
  it('empty validations is always ok', () => {
    expect(ValidateForm({}, {})).toEqual({});
  });

  it('required is triggered', () => {
    expect(ValidateForm({}, {field: ['isRequired']})).toEqual({field: 'fieldRequired'});
  });
});

const path = require('path');
const Common = path.resolve(__dirname, '../common');
const Widget = path.resolve(__dirname, './widget');
const Components = path.resolve(__dirname, './shared/components');

require('@babel/register')({
  presets: [
    [
      '@babel/preset-env',
      {
        targets: {
          node: '8.11.1'
        }
      }
    ],
    '@babel/preset-react'
  ],
  plugins: [
    '@babel/plugin-syntax-object-rest-spread',
    '@babel/plugin-proposal-optional-chaining',
    '@babel/plugin-proposal-class-properties',
    [
      'module-resolver',
      {
        extensions: ['.js'],
        resolvePath(sourcePath, currentFile, opts) {
          // Alias for @Common
          if (sourcePath.indexOf('@Common/') === 0) {
            return sourcePath.replace('@Common', Common);
          }

          if (sourcePath.indexOf('@Components/') === 0) {
            return sourcePath.replace('@Components', Components);
          }

          if (sourcePath.indexOf('@Widget/') === 0) {
            return sourcePath.replace('@Widget', Widget);
          }
          // All other paths will be returned unchanged
          return undefined;
        }
      }
    ]
  ],
  only: ['.', path.resolve(__dirname, '../common')],
  ignore: ['node_modules']
});
require('./server/init');

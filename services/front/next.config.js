const path = require('path');
const withSass = require('@zeit/next-sass');
const NextBuildId = require('next-build-id');
// const ExtractTextPlugin = require('extract-text-webpack-plugin');
// const loaderUtils = require('loader-utils');
const cp = require('child_process');

const isDev = process.env.NODE_ENV !== 'production';

// const extract = new ExtractTextPlugin({
//  filename: isDev ? 'static/[name].css' : 'static/[name]-[contenthash].css'
// });

// Plugin to filter warning due to css order (doesn't harm styles because our namespace)
class FilterPlugin {
  constructor(options) {
    this.options = options;
  }

  apply(compiler) {
    compiler.hooks.afterEmit.tap('FilterPlugin', compilation => {
      compilation.warnings = compilation.warnings.filter(
        warning => !this.options.filter.test(warning.message),
      );
    });
  }
}

// Build used to point to static assets
const buildId = cp.execSync('git rev-parse HEAD', {encoding: 'utf8'});

module.exports = withSass({
  assetPrefix: !isDev ? `https://dvmucn6zqagd0.cloudfront.net/builds/${buildId}` : '',
  // Set the build id to the lates git commit
  generateBuildId: () => {
    return new NextBuildId({dir: __dirname}).then(fromGit => {
      return isDev ? `${fromGit.id}--${new Date().getTime()}` : `${fromGit.id}`;
    });
  },

  // Loads css files
  cssModules: true,
  cssLoaderOptions: {
    importLoaders: 1,
    localIdentName: '[local]',
    getLocalIdent: (context, localIdentName, localName, options) => {
      // console.log(context.resource);
      // Semantic modules use original name to match the framework component
      if (
        context.resource.indexOf('compiled-css') !== -1 ||
        context.resource.indexOf('node_modules') !== -1
      ) {
        return localName;
      }

      // All other modules used hashed class names
      const request = path.relative(context.context, context.resourcePath).replace(/\.s?css$/i, '');
      return `${request}--${localName}`;
    },
  },
  // extractCSSPlugin: extract,
  webpack(config, {dev, isServer}) {
    // Using babel on common files (depends on local .babelrc)
    config.module.rules.forEach(rule => {
      if (rule.use && rule.use.loader === 'next-babel-loader') {
        rule.include.push(path.resolve(__dirname, '../common'));
      }
    });

    // Redirecting to local modules for common files
    config.resolve.modules.push(path.resolve(__dirname, './node_modules'));

    config.node = {
      fs: 'empty',
      net: 'empty',
      tls: 'empty',
    };

    // Don't include controllers in client-side bundles
    config.resolve.alias['@Common'] = path.resolve(__dirname, '../common');
    config.resolve.alias['@Widget'] = path.resolve(__dirname, './widget');
    config.resolve.alias['@Components'] = path.resolve(__dirname, './shared/components');
    // This is used only for sass imports
    config.resolve.alias.CommonStyles = path.resolve(__dirname, '../common/styles');

    config.module.rules.push(
      // {
      //  test: /\.(ttf|eot|svg|woff|woff2)$/,
      //  use: {
      //    loader: 'url-loader',
      //    options: {
      //      name: '../fonts/[name][hash].[ext]'
      //    }
      //  }
      // },
      {
        test: /\.(ttf|eot|svg|woff|woff2)$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[name][hash].[ext]',
            outputPath: 'static/css/static/fonts/',
            publicPath: '../../fonts/',
            emitFile: true,
          },
        },
      },
      {
        test: /\.(png|jpg)$/,
        loader: 'url-loader',
      },
    );

    // Extract styles and create css files
    // config.plugins.push(extract);
    const warningsFilter = new FilterPlugin({
      filter: /chunk styles \[mini-css-extract-plugin]\nConflicting order between:/,
    });
    config.plugins.push(warningsFilter);

    return config;
  },
  // this is supposed to fix the issues with non working links after a few minutes of inactivty
  onDemandEntries: {
    // period (in ms) where the server will keep pages in the buffer
    maxInactiveAge: 25 * 1000,
    // number of pages that should be kept simultaneously without being disposed
    pagesBufferLength: 5,
  },
});

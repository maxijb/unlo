import App, {Container} from 'next/app';
import React from 'react';
import {get} from 'lodash';
import GA from 'react-ga';
import Router from 'next/router';
import {FacebookAppId} from '@Common/constants/social';
import * as Sentry from '@sentry/react';
import {Integrations} from '@sentry/tracing';

import {I18nextProvider} from 'react-i18next';
import initialI18nInstance from '../shared/i18n/i18n';
import Api from '../shared/actions/api-actions';
import memoizeOne from 'memoize-one';
import Logger from '@Common/logger';
import ABTracker from '@Common/utils/ab-tracker';
import {DefaultSeo} from 'next-seo';

import withReduxStore from '../shared/store/with-redux-store';
import {Provider, connect} from 'react-redux';

import appActions from '../shared/actions/app-actions';
import apiActions from '../shared/actions/api-actions';
import AppProvider from '../shared/components/app-context';
import {MobileBreakpoint, TabletBreakpoint} from '@Common/constants/app';

import './_compiled-css--semantic.scss';

// We memoize the function to generate the page container
// based on whether it's the first render
// and if the component is the same
const memoGetContainer = memoizeOne(getContainer);

class MyApp extends App {
  static async getInitialProps({Component, ctx}) {
    let pageProps = {};
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    let deviceType;
    if (ctx.req) {
      const UAParser = require('ua-parser-js');
      deviceType = new UAParser(ctx.req.get('User-Agent')).getDevice().type;

      if (ctx.query?.s) {
        ctx.dispatch(
          Api.user.trackActivity(
            {type: 'landing', subtype: ctx.query.s, cid: ctx.req.clientId},
            ctx.req,
          ),
        );
      }
    }

    return {
      pageProps,
      req: ctx.req,
      isMobile: deviceType === 'mobile' || deviceType === 'wearable',
      isTablet: deviceType === 'tablet',
    };
  }

  constructor(props) {
    super(props);
    this.state = {};
    this.onResize = this.onResize.bind(this);
  }

  componentDidMount() {
    Router.router.events.on('routeChangeComplete', () => {
      window.scrollTo(0, 0);
    });
    window.addEventListener('resize', this.onResize);
    this.onResize();

    const sentry = this.props.reduxStore.getState().app.config?.sentry;
    if (sentry) {
      Sentry.init({
        dsn: sentry,
        integrations: [new Integrations.BrowserTracing()],

        // We recommend adjusting this value in production, or using tracesSampler
        // for finer control
        tracesSampleRate: 1.0,
      });
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize);
  }

  onResize() {
    const isMobile = window.innerWidth < MobileBreakpoint;
    const isTablet = window.innerWidth >= MobileBreakpoint && window.innerWidth <= TabletBreakpoint;

    const compareMobile =
      typeof this.state.isMobile !== 'undefined' ? this.state.isMobile : this.props.isMobile;
    const compareTablet =
      typeof this.state.isTablet !== 'undefined' ? this.state.isTablet : this.props.isTablet;

    //if (isMobile !== compareMobile || isTablet !== compareTablet) {
    this.setState({
      isMobile,
      isTablet,
    });
    //}
  }

  render() {
    const {Component, pageProps, reduxStore, router, req} = this.props;
    const {i18n, initialI18nStore, initialLanguage} = pageProps || {};

    const isMobile =
      typeof this.state.isMobile !== 'undefined' ? this.state.isMobile : this.props.isMobile;
    const isTablet =
      typeof this.state.isTablet !== 'undefined' ? this.state.isTablet : this.props.isTablet;

    const ReduxContainer = memoGetContainer(Component);
    const ga = req ? req.GA : GA;
    const ab = req ? req.ab : new ABTracker(reduxStore.getState().app.activeAB, ga);
    const config = reduxStore.getState().app.config;

    return (
      <Container>
        <DefaultSeo
          title="Incredible Phones | Factory Refurbished Iphones | 1-Yr Warranty | Free Returns"
          description="Buy a pre-tested iPhone that work like new for up to 70% less. Plus ✅ 12 month Warranty ✅ Free returns ✅ Buy now, pay later."
          canonical={`${config.protocol}://${config.fullDomain}${router.asPath.split('?')[0]}`}
          openGraph={{
            type: 'website',
            locale: 'en_US',
            url: `${config.protocol}://${config.fullDomain}${router.asPath.split('?')[0]}`,
            title: 'Incredible Phones | Factory Refurbished Iphones | 1-Yr Warranty | Free Returns',
            description:
              'Buy a pre-teste iPhone that work like new for up to 70% less. Plus ✅ 12 month Warranty ✅ Free returns ✅ Buy now, pay later.',
            site_name: 'Incredible Phones',
            images: [
              {
                url: `${config.protocol}://${config.fullDomain}/static/og-image.png`,
                width: 1314,
                height: 1114,
                alt: 'Pre-tested iphones that work like new for up to 70% less',
              },
            ],
          }}
          twitter={{
            handle: '@incrediblephones',
            site: '@incrediblephones',
            cardType: 'summary_large_image',
          }}
          facebook={{
            appId: FacebookAppId,
          }}
        />
        <AppProvider
          Logger={req ? req.Logger : Logger}
          GA={ga}
          ab={ab}
          store={reduxStore}
          router={router}
          isMobile={isMobile}
          isTablet={isTablet}
        >
          <I18nextProvider
            i18n={i18n || initialI18nInstance}
            initialI18nStore={initialI18nStore}
            initialLanguage={initialLanguage}
          >
            <Provider store={reduxStore}>
              <ReduxContainer
                {...pageProps}
                isMobile={isMobile}
                isTablet={isTablet}
                router={router}
              />
            </Provider>
          </I18nextProvider>
        </AppProvider>
      </Container>
    );
  }
}

export default withReduxStore(MyApp);

/* -------------------------- Redux container ----------------- */

const mapDispatchToProps = dispatch => {
  return {
    actions: {...startActions(appActions)},
    api: {...startActions(apiActions)},
  };

  function startActions(base) {
    return Object.keys(base).reduce((prev, key) => {
      if (typeof base[key] === 'object') {
        prev[key] = startActions(base[key], dispatch);
      } else if (typeof base[key] === 'function') {
        const actionCreator = base[key];
        prev[key] = (...params) => dispatch(actionCreator(...params));
      }
      return prev;
    }, {});
  }
};

const mapStateToProps = (state, ownProps) => {
  return {...state, ...ownProps};
};

function getContainer(Component) {
  return connect(mapStateToProps, mapDispatchToProps)(Component);
}

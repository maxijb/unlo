import React from 'react';
import {cdnURL} from '@Common/utils/statif-assets-utils';
import Document, {Head, Main, NextScript} from 'next/document';

export default class MyDocument extends Document {
  // Initial props only run in the server for MyDocument
  // static async getInitialProps({req, dispatch}) {
  //  return {};
  // }

  render() {
    const {
      buildManifest: {css},
    } = this.props;

    return (
      <html>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <link rel="preconnect" href="https://fonts.gstatic.com" />
          <link
            href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&family=Vollkorn:wght@800&display=swap"
            rel="stylesheet"
          />

          <link rel="icon" href={cdnURL('/static/icons/favicon_inc.png')} />
          <meta
            name="google-site-verification"
            content="X5Ti75XAbMuITIlyevpY3YmGKMcXS2O3FijQ7knH0IE"
          />
          <link rel="apple-touch-icon" href={cdnURL('/static/icons/favicon_inc.png')} />
          <meta name="msapplication-TileImage" content={cdnURL('/static/icons/favicon_inc.png')} />
          <meta name="facebook-domain-verification" content="92sjhay1uhasxgck01hjluex82vjf3" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}

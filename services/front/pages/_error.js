import React from 'react';
import Header from '@Components/header/header';
import Footer from '@Components/footer/footer';
import classnames from 'classnames';
import {Grid} from 'semantic-ui-react';
import {withI18next} from '../shared/i18n/with-i18n.js';
import WithPublicContext from '@Components/hocs/with-public-context';
import {withAppConsumer} from '../shared/components/app-context';

import styles from '@Common/styles/styles.scss';

function Error(props) {
  const {statusCode} = props;
  return (
    <>
      <Header {...props} />
      <Grid
        columns={16}
        className={classnames(styles.MainGrid, styles.MarginDoubleTop, styles.MarginDoubleBottom)}
      >
        <Grid.Row>
          <Grid.Column width={16} textAlign="center" style={{minHeight: '500px'}}>
            <h1
              className={classnames(styles.H1, styles.MarginDoubleBottom)}
              style={{marginTop: '120px !important'}}
            >
              Error {statusCode}
            </h1>
            <h3>Oops... that's not very incredible of us :(</h3>
            <p
              className={classnames(
                styles.MarginDoubleTop,
                styles.MarginDoubleBottom,
                styles.Medium,
              )}
            >
              {statusCode == 404
                ? 'The page you where looking for was not found.'
                : 'There seems to be an error.'}
            </p>
            <a href="/">Go back to home page</a>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Footer {...props} />
    </>
  );
}

Error.getInitialProps = ({res, err}) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return {statusCode};
};

export default withAppConsumer(WithPublicContext(withI18next(['home', 'common'])(Error)));

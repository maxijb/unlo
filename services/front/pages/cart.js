import React, {useState, useEffect, useCallback, useMemo} from 'react';
import Link from 'next/link';

import {getPageLink} from '@Common/utils/urls';
import Logger from '@Common/logger';
import {Grid, Button} from 'semantic-ui-react';
import Api from '../shared/actions/api-actions';
import Actions from '../shared/actions/app-actions';
import {withI18next} from '../shared/i18n/with-i18n.js';
import Header from '../shared/components/header/header';
import ImageBox from '@Components/product/image-box';
import PurchaseBox from '@Components/product/purchase-box';
import {RequestErrorMessage, CartErrorMessage} from '@Common/constants/errors';
import classnames from 'classnames';
import WithPublicContext from '@Components/hocs/with-public-context';
import {withAppConsumer} from '../shared/components/app-context';
import {
  calculateDisplaySubtotal,
  calculateDisplaySubtotalAndShipping,
} from '../shared/utils/cart-utils';
import {UnlockedCarrierId, EmptyDropdownChoice} from '@Common/constants/app';
import HighlightedProducts from '@Components/product/highlighted-products';
import CartForm from '@Components/cart/cart-form';
import Router from 'next/router';
import OpenCartModal from '@Components/modals/open-cart-modal';
import {getCartDisplayProductId} from '../shared/utils/cart-utils';

import {loadStripe} from '@stripe/stripe-js';
import {Elements} from '@stripe/react-stripe-js';

import styles from '@Common/styles/styles.scss';
import css from './cart.scss';

function Cart(props) {
  const {
    cart,
    cart: {
      items,
      reference,
      address,
      differentShippingAddress,
      shippingAddress,
      paymentType,
      paymentCard,
      paymentCardError,
      taxRate,
      isOrdering,
      shippingMethod,
    },
    isMobile,
    isTablet,
    api,
    actions,
    user,
    t,
    app: {publicKeys},
  } = props;

  const cartIsEmpty = !items?.length;
  const stripePromise = useMemo(() => loadStripe(publicKeys.stripe), []);

  const {subtotal, shippingCharges, promoCode, subtotalWithoutPromoCode} = useMemo(() => {
    return calculateDisplaySubtotalAndShipping(cart.items, cart.reference);
  }, [cart.items, cart.reference]);

  const tax = useMemo(() => {
    return cart.taxRate === null ? 0 : subtotal * cart.taxRate;
  }, [subtotal, cart.taxRate]);

  const [isOpenCartModal, setIsOpenCartModal] = useState(false);

  const total = subtotal + tax;

  const content = !items?.length ? (
    <Grid.Row className={classnames()}>
      <Grid.Column computer={16} mobile={16} tablet={16}>
        <div className={css.emptyCart}>{t('yourCartIsEmpty')}</div>
      </Grid.Column>
    </Grid.Row>
  ) : (
    <Elements stripe={stripePromise}>
      <CartForm
        {...props}
        total={total}
        subtotal={subtotal}
        subtotalWithoutPromoCode={subtotalWithoutPromoCode}
        tax={tax}
        promoCode={promoCode}
        shippingCharges={shippingCharges}
        setIsOpenCartModal={setIsOpenCartModal}
      />
    </Elements>
  );

  return (
    <>
      <Header
        {...props}
        isCart={true}
        total={total}
        openCartModal={() => setIsOpenCartModal(true)}
      />
      <Grid
        columns={16}
        className={classnames({
          fullSize: isMobile,
          [css.cartBackground]: isMobile,
        })}
      >
        {content}
        {isMobile && <div className={css.bottomSpace} />}
      </Grid>
      {isOpenCartModal && (
        <OpenCartModal
          onClose={() => setIsOpenCartModal(false)}
          cart={cart}
          subtotal={subtotal}
          subtotalWithoutPromoCode={subtotalWithoutPromoCode}
          shippingCharges={shippingCharges}
          total={total}
          tax={tax}
          api={api}
          promoCode={promoCode}
          items={items}
          reference={reference}
        />
      )}
    </>
  );
}

Cart.getInitialProps = async ({req, reduxStore, dispatch, query, res, ...other}) => {
  // Paypal listeners
  if (query.hasOwnProperty('paypal_reject')) {
    if (req) {
      req.GA.event({
        category: 'cart',
        action: 'payment-failure',
        label: 'paypal',
        nonInteraction: true,
      });
    }
    await dispatch(Actions.setOrderError(CartErrorMessage.paypalRejected, req));
    await dispatch(Api.cart.getPendingOrderDetails({id: query.tempOrderId}, req));
  } else if (query.hasOwnProperty('paypal_accept')) {
    const order = await dispatch(Api.cart.capturePaypalOrder(query, req));
    if (order?.resp?.ok && order?.resp?.order_id) {
      if (res) {
        req.GA.event({
          category: 'cart',
          action: 'payment-success',
          label: 'paypal',
          value: Math.round(order?.resp?.total || 0),
          nonInteraction: true,
        });
        res.redirect(`/order-confirmation/${order?.resp?.order_id}?reset=true`);
      } else {
        Router.push(
          `/order-confirmation?id=${order?.resp?.order_id}&reset=true`,
          `/order-confirmation/${order?.resp?.order_id}`,
        );
      }
      return;
    } else {
      await dispatch(Actions.setOrderError(order.resp.errors?.[0] || order.resp.error));
      await dispatch(Api.cart.getPendingOrderDetails({id: query.tempOrderId}, req));
    }
  }

  // Load current order
  await Promise.all([
    dispatch(Api.cart.readActiveForUser({}, req)),
    dispatch(Api.user.getUserOwnExtendedDetails({}, req)),
    dispatch(Api.cart.getActiveTradeInForUser({}, req)),
  ]);

  const state = reduxStore.getState();
  // Load tax rate
  const zip = state.user.primaryAddress?.postal_code;
  if (zip) {
    const subtotal = calculateDisplaySubtotal(state.cart.items, state.cart.reference);

    const tax = await dispatch(
      Api.cart.getTaxRateByZipCode(
        {
          address: state.user.primaryAddress,
          subtotal,
        },
        req,
      ),
    );
  }
};

export default withAppConsumer(WithPublicContext(withI18next(['home', 'common'])(Cart)));

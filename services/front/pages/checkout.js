import React from 'react';
import Link from 'next/link';

import {getPageLink} from '@Common/utils/urls';
import Logger from '@Common/logger';

import Api from '../shared/actions/api-actions';
import Actions from '../shared/actions/app-actions';
import {withI18next} from '../shared/i18n/with-i18n.js';
import Header from '../shared/components/header/header';
import CheckoutForm from '../shared/components/payments/checkout-form';

import css from './index.scss';

class Checkout extends React.Component {
  /* On getInitialProps, we get the action creators from app-actions. and api-actions.js
   * If multiple actions need to be called we can paralelize with Promise.all
   * We need to pass req as final param in all API actions, so controllers have access to the DB.
   */
  static async getInitialProps({req, reduxStore, dispatch}) {
    const user = reduxStore.getState().user;
    if (!user.email) {
      const id = req ? req.auth.id : reduxStore.getState().user.id;
      await dispatch(Api.user.getUserDetails({id}, req));
    }
  }
  /* On all the other methods, the action creators are received as props, passed by the redux container */
  /* 't' is always available to get traductions */
  render() {
    const {app, api, user} = this.props;

    return (
      <div className={css.about}>
        <Header {...this.props} />
        <CheckoutForm
          performPayment={api.payments.performPayment}
          cancelPayment={2}
          amount={3499}
          lang={app.lang}
          user={user}
        />
      </div>
    );
  }
}

export default WithPublicContext(withI18next(['home', 'common'])(Checkout));

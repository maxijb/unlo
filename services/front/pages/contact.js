import React, {useState} from 'react';
import Link from 'next/link';

import HighlightedProducts from '@Components/product/highlighted-products';
import {Grid, Button} from 'semantic-ui-react';
import Logger from '@Common/logger';
import Api from '../shared/actions/api-actions';
import Actions from '../shared/actions/app-actions';
import {withI18next} from '../shared/i18n/with-i18n.js';

import Highlights from '@Components/display/highlights';
import Header from '../shared/components/header/header';
import WithPublicContext from '@Components/hocs/with-public-context';
import {withAppConsumer} from '../shared/components/app-context';
import {getPageLink} from '@Common/utils/urls';
import classnames from 'classnames';

import Footer from '@Components/footer/footer';
import ContactForm from '@Components/forms/contact-form';

import styles from '@Common/styles/styles.scss';
import css from './static-pages.scss';

function IndexView(props) {
  const {t, app, actions, currentOrder, user, query, api} = props;
  const [isSignin, setIsSignin] = useState(false);
  const [isOrderDetails, setIsOrderDetails] = useState(false);
  return (
    <div>
      <Header {...props} />

      <Grid columns={16} className={classnames(styles.MainGrid)}>
        <Grid.Row>
          <Grid.Column width={16} className={css.content}>
            <ContactForm api={api} />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={16} className={css.content} textAlign="center">
            <h3>We’re also available via phone, email and good old fashioned mail. </h3>

            <p>2820 Howard Cmns #309 </p>
            <p>Howard, WI 54313</p>
            <p>Call or text (888) 675-2905</p>
            <p>support@incrediblephones.com</p>

            <h5>Hours</h5>
            <div>Monday - Saturday</div>
            <div>10:00AM to 5:00PM PST</div>
            <div>(24-48 hr response time)</div>
            <p>Closed Sunday</p>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Footer {...props} />
    </div>
  );
}

IndexView.getInitialProps = async ({req, reduxStore, dispatch, query, res}) => {};

export default withAppConsumer(WithPublicContext(withI18next(['home', 'common'])(IndexView)));

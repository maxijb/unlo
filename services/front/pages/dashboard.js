import React from 'react';
import Link from 'next/link';
import {Grid} from 'semantic-ui-react';
import classnames from 'classnames';

import Logger from '@Common/logger';
import {getPageLink} from '@Common/utils/urls';

import Api from '../shared/actions/api-actions';
import Actions from '../shared/actions/app-actions';
import {withI18next} from '../shared/i18n/with-i18n.js';

import DashboardLayout from '../shared/components/layout/dashboard-layout';
import WithActiveConfig from '@Components/hocs/with-active-config';
import WithDashboardContext from '../shared/components/hocs/with-dashboard-context';
import TrendChart from '@Components/charts/trend-chart';
import RankingSessionsByURL from '@Components/dashboards/ranking-sessions-by-url';
import ConversationByOperator from '@Components/dashboards/conversations-by-operator';
import DashboardBubble from '@Components/dashboards/dashboard-bubble';

import ValidatedUserWarning from '@Components/information/validated-user-warning';

import css from './dashboard.scss';

class Dashboard extends React.Component {
  static async getInitialProps({req, reduxStore, dispatch, query}) {}

  /* On all the other methods, the action creators are received as props, passed by the redux container */
  /* 't' is always available to get traductions */
  render() {
    const {
      t,

      user,
      api,
    } = this.props;

    return (
      <DashboardLayout {...this.props} mainTitle={t('dashboard')}>
        <Grid columns={16} padded>
          <Grid.Column
            computer={10}
            className={css.mainColumn}
            mobile={16}
            tablet={10}
          ></Grid.Column>
          <Grid.Column
            width={6}
            className={classnames(css.columnSeparator, css.mainColumn)}
            only={'computer tablet'}
          ></Grid.Column>
        </Grid>
      </DashboardLayout>
    );
  }
}

export default WithDashboardContext(
  WithActiveConfig(withI18next(['dashboard', 'common'])(Dashboard), true),
);

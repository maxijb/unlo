import React from 'react';
import Link from 'next/link';
import get from 'lodash.get';
import {Grid, TextArea, Button, Form} from 'semantic-ui-react';
import extend from 'deep-extend';

import Api from '../../../shared/actions/api-actions';
import {guessTimezone} from '@Common/utils/date-utils';
import {withI18next} from '../../../shared/i18n/with-i18n.js';
import DashboardLayout from '@Components/layout/dashboard-layout';
import WithDashboardContext from '@Components/hocs/with-dashboard-context';
import WithActiveConfig from '@Components/hocs/with-active-config';
import Input from '@Components/inputs/wrapped-input';
import AvatarInput from '@Components/inputs/avatar-input';
import SaveButton from '@Components/inputs/save-button';
import Dropdown from '@Components/inputs/wrapped-dropdown';

import css from '../settings/settings.scss';

const Currencies = [
  {value: 'EUR', text: 'Euro'},
  {value: 'USD', text: 'US Dollar'}
];

class BasicInfo extends React.Component {
  constructor(props) {
    super(props);
    const {
      user: {firstName, lastName, email, phone, avatar}
    } = props;
    this.state = {firstName, lastName, email, phone, avatar};
  }

  onSetField = (value, field) => {
    this.setState({[field]: value});
  };

  onFileUploaded = (err, data) => {
    if (data && data.filename) {
      this.setState({avatar: data.filename});
    }
  };

  onSave = name => {
    const {
      api: {
        user: {saveSettings}
      }
    } = this.props;
    const {firstName, lastName, email, phone, avatar} = this.state;
    const input = {firstName, lastName, email, phone, avatar};

    this.setState({saving: name || true, savingError: null, savingSuccessful: null});
    saveSettings(input).then(({resp}) => {
      if (resp.error) {
        this.setState({saving: false, savingError: name || resp});
      } else {
        this.setState({saving: false, savingSuccessful: name || true});
        this.props.actions.updateUserInfo(input);
      }
    });
  };

  render() {
    const {t} = this.props;
    const {
      saving,
      savingSuccessful,
      savingError,

      firstName,
      lastName,
      email,
      phone,
      avatar
    } = this.state;

    return (
      <DashboardLayout {...this.props} mainTitle={t('header.accountSettings')}>
        <Grid columns={16} padded={false} className={css.mainGrid}>
          <Grid.Row>
            <Grid.Column width={16}>
              <h3>{t('accountSettings.userAccount')}</h3>
            </Grid.Column>
          </Grid.Row>

          <Grid.Row className="margin">
            <Grid.Column width={8}>
              <Input
                value={firstName}
                fluid
                label={t('accountSettings.firstName')}
                onChange={this.onSetField}
                name="firstName"
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row className="margin">
            <Grid.Column width={8}>
              <Input
                value={lastName}
                fluid
                label={t('accountSettings.lastName')}
                onChange={this.onSetField}
                name="lastName"
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row className="margin">
            <Grid.Column width={8}>
              <Input
                value={email}
                fluid
                label={t('common:email')}
                onChange={this.onSetField}
                disabled={Boolean(email)}
                name="email"
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row className="margin">
            <Grid.Column width={8}>
              <Input
                value={phone}
                fluid
                label={t('common:phone')}
                onChange={this.onSetField}
                name="phone"
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row className="margin">
            <Grid.Column width={8}>
              <AvatarInput
                image={avatar}
                onFileUploaded={this.onFileUploaded}
                type="avatar"
                label={t('myAvatar')}
              />
            </Grid.Column>
          </Grid.Row>

          <Grid.Row className="margin">
            <Grid.Column width={16}>
              <SaveButton
                saving={saving}
                onClick={this.onSave}
                savingSuccessful={savingSuccessful}
                savingError={savingError}
                text={t('save')}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </DashboardLayout>
    );
  }
}

export default WithDashboardContext(
  WithActiveConfig(withI18next(['dashboard', 'common'])(BasicInfo))
);

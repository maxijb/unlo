import React from 'react';
import Link from 'next/link';
import get from 'lodash.get';
import isEmpty from 'is-empty';
import classnames from 'classnames';
import {Grid, TextArea, Button, Form} from 'semantic-ui-react';
import extend from 'deep-extend';

import Api from '../../../shared/actions/api-actions';
import {guessTimezone} from '@Common/utils/date-utils';
import {AssetStatus, AssetRole, AssetRoleHierarchy} from '@Common/constants/assets';
import FormValidations from '@Common/constants/form-validations';
import {ValidateForm} from '@Common/utils/validations';

import {withI18next} from '../../../shared/i18n/with-i18n.js';
import DashboardLayout from '@Components/layout/dashboard-layout';
import WithDashboardContext from '@Components/hocs/with-dashboard-context';
import WithActiveConfig from '@Components/hocs/with-active-config';
import ConfirmationModal from '@Components/modals/confirmation-modal';
import Input from '@Components/inputs/wrapped-input';
import Avatar from '@Components/information/avatar';
import SaveButton from '@Components/inputs/save-button';
import Dropdown from '@Components/inputs/wrapped-dropdown';
import ErrorRenderer from '@Components/inputs/error-renderer';

import mcss from '../settings/settings.scss';
import css from './teammates.scss';

const Currencies = [
  {value: 'EUR', text: 'Euro'},
  {value: 'USD', text: 'US Dollar'}
];

class Teammates extends React.Component {
  static allowedRoles = [AssetRole.owner, AssetRole.manager];

  constructor(props) {
    super(props);
    this.state = {
      inviteEmail: '',
      inviteRole: '',
      updating: false,
      saving: false,
      deleting: false,
      savingError: {},
      savingSuccessful: false
    };
  }

  onSetField = (value, name) => {
    this.setState({[name]: value});
  };

  onSetRole = (userId, role) => {
    const {
      target: {appId},
      api: {
        assets: {changeRole}
      },
      configuration: {members}
    } = this.props;

    const oldRole = members.getField(userId, 'role');

    this.setState({updating: userId});
    changeRole({appId, userId, role, oldRole}).then(({resp}) => {
      this.setState({updating: false});
    });
  };

  onRevokeAccess = userId => {
    this.setState({revokingUser: userId});
  };

  onAbortRevokeAccess = () => {
    this.setState({revokingUser: null});
  };

  onDoRevokeAccess = userId => {
    const {
      target: {appId},
      api: {
        assets: {revokeAccess}
      }
    } = this.props;
    const {revokingUser} = this.state;

    this.setState({deleting: userId, revokingUser: null});
    revokeAccess({appId, userId: revokingUser}).then(({resp}) => {
      this.setState({deleting: false});
    });
  };

  onInvite = name => {
    const {
      target: {appId},
      api: {
        assets: {inviteTeammate}
      }
    } = this.props;

    const {inviteEmail, inviteRole} = this.state;
    const input = {appId, email: inviteEmail, role: inviteRole};

    const foundErrors = ValidateForm(input, FormValidations.inviteTeammate);
    this.setState({
      saving: isEmpty(foundErrors),
      savingError: foundErrors,
      savingSuccessful: null
    });

    if (isEmpty(foundErrors)) {
      inviteTeammate(input).then(({resp}) => {
        const errors = !resp.error ? {} : resp.errorData || {form: {...resp}};
        this.setState({
          saving: false,
          savingError: errors,
          savingSuccessful: resp.error ? false : 'inviteTeammateSuccesful'
        });

        if (!resp.error) {
          this.setState({
            inviteEmail: '',
            inviteRole: ''
          });
        }
      });
    }
  };

  renderMember(member) {
    const {t, user} = this.props;
    const {deleting, updating} = this.state;

    const canModify = AssetRoleHierarchy[user.appRole] >= AssetRoleHierarchy[member.role];
    const isDisabled = !canModify || updating === member.id || deleting === member.id;
    return (
      <div
        className={classnames(css.memberLine, {
          [css.deleting]: deleting === member.id
        })}
      >
        <div className={classnames(css.memberCell, css.avatarCell)}>
          <Avatar src={member.avatar} />
        </div>
        <div className={classnames(css.memberCell, css.nameCell)}>
          <div>
            {member.firstName} {member.lastName}
          </div>
          <div>{member.email}</div>
        </div>
        <div className={classnames(css.memberCell, css.statusCell)}>
          {member.status !== AssetStatus.member && t(member.status)}
        </div>
        <div className={classnames(css.memberCell, css.roleCell)}>
          <Dropdown
            fluid
            disabled={isDisabled}
            value={member.role}
            t={t}
            onChange={this.onSetRole.bind(this, member.id)}
            options={Object.keys(AssetRole).filter(
              role =>
                AssetRoleHierarchy[user.appRole] >= AssetRoleHierarchy[role] || role === member.role
            )}
          />
        </div>

        <div className={classnames(css.memberCell, css.revokeCell)}>
          {member.id !== user.id && (
            <Button
              fluid
              basic
              onClick={this.onRevokeAccess.bind(this, member.id)}
              disabled={isDisabled}
            >
              {t('revokeAccess')}
            </Button>
          )}
        </div>
      </div>
    );
  }

  renderMembers() {
    const {
      configuration: {members}
    } = this.props;
    const users = members.items();

    const content = members.items().map(member => this.renderMember(member));

    return <div className={css.membersTable}>{content}</div>;
  }

  render() {
    const {t, user} = this.props;
    const {
      saving,
      savingSuccessful,
      savingError,
      revokingUser,

      inviteEmail,
      inviteRole
    } = this.state;

    return (
      <DashboardLayout {...this.props} mainTitle={t('header.accountSettings')}>
        <Grid columns={16} padded={false} className={mcss.mainGrid}>
          <Grid.Row>
            <Grid.Column width={16}>
              <h3>{t('submenu.teammates')}</h3>
              <div>{t('teammateSettings.subtitle')}</div>
            </Grid.Column>
          </Grid.Row>

          <Grid.Row>
            <Grid.Column width={16}>{this.renderMembers()}</Grid.Column>
          </Grid.Row>

          <Grid.Row>
            <Grid.Column width={16}>
              <div className={css.horizontalSeparator} />
              <div className={classnames(css.memberLine, css.memberForm)}>
                <div className={classnames(css.memberCell, css.inviteEmailCell)}>
                  <Input
                    value={inviteEmail}
                    name="inviteEmail"
                    error={savingError.email}
                    onChange={this.onSetField}
                    fluid
                    placeholder={t('teammateSettings.enterTeamatesEmail')}
                  />
                </div>
                <div className={classnames(css.memberCell, css.roleCell)}>
                  <Dropdown
                    fluid
                    name="inviteRole"
                    error={savingError.role}
                    onChange={this.onSetField}
                    value={inviteRole}
                    placeholder={t('teammateSettings.selectRole')}
                    t={t}
                    options={Object.keys(AssetRole).filter(
                      role => AssetRoleHierarchy[user.appRole] >= AssetRoleHierarchy[role]
                    )}
                  />
                </div>
                <div className={classnames(css.memberCell, css.revokeCell)}>
                  <Button fluid primary loading={saving} onClick={this.onInvite}>
                    {t('teammateSettings.inviteTeammate')}
                  </Button>
                </div>
              </div>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row className="noMargin">
            <Grid.Column width={16}>
              <ErrorRenderer
                error={savingError.form || savingSuccessful || null}
                positive={savingSuccessful}
                classes={css.successMessage}
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={16}>
              {Object.keys(AssetRole).map(role => {
                return (
                  <div className={css.roleLine}>
                    <div className={css.roleName}>{t(role)}</div>
                    <div className={css.rolesDescription}>{t(`rolesDescription.${role}`)}</div>
                  </div>
                );
              })}
            </Grid.Column>
          </Grid.Row>
        </Grid>
        {revokingUser && (
          <ConfirmationModal
            onConfirm={this.onDoRevokeAccess}
            onClose={this.onAbortRevokeAccess}
            message={t('sureRevokeAccess')}
            title={t('revokeAccess')}
            no={t('noKeep')}
            yes={t('yesRevokeIt')}
          />
        )}
      </DashboardLayout>
    );
  }
}

export default WithDashboardContext(
  WithActiveConfig(withI18next(['dashboard', 'common'])(Teammates))
);

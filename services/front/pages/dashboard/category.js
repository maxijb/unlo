import React from 'react';
import Link from 'next/link';
import {Grid} from 'semantic-ui-react';
import classnames from 'classnames';

import Logger from '@Common/logger';
import {getPageLink} from '@Common/utils/urls';

import Api from '../../shared/actions/api-actions';
import Actions from '../../shared/actions/app-actions';
import {withI18next} from '../../shared/i18n/with-i18n.js';

import DashboardLayout from '../../shared/components/layout/dashboard-layout';
import WithActiveConfig from '@Components/hocs/with-active-config';
import WithDashboardContext from '../../shared/components/hocs/with-dashboard-context';

import AddButton from '@Components/inputs/add-button';
import CrudAttrImage from '@Components/crud/attr-image';
import CrudChild from '@Components/crud/child';
import css from '../dashboard.scss';

class Dashboard extends React.Component {
  static async getInitialProps({req, reduxStore, dispatch, query}) {
    let id = query.id || null;
    if (id === 'null') {
      id = null;
    }
    const current = await dispatch(Api.crud.categoryGet({id}, req));

    await Promise.all([
      dispatch(Api.crud.categoryGet({id: current?.resp?.parent}, req)),
      dispatch(Api.crud.categoriesList({parent: id}, req)),
      dispatch(Api.crud.categoryImagesList({id}, req)),
      dispatch(Api.crud.attributesList({}, req)),
    ]);
    return {query};
  }

  /* On all the other methods, the action creators are received as props, passed by the redux container */
  /* 't' is always available to get traductions */
  render() {
    const {t, crud, user, api, actions, query} = this.props;
    const {images, imageMap, imagesTotalCount} = crud;
    const parent = crud.categories[crud.categories?.[query.id]?.parent];
    return (
      <DashboardLayout {...this.props} mainTitle={t('dashboard')}>
        <Grid columns={16} padded>
          <Grid.Column computer={16} className={css.mainColumn} mobile={16} tablet={16}>
            <>
              {parent && (
                <div>
                  <Link
                    href={`/dashboard/category?id=${parent.id}`}
                    as={`/dashboard/category/${parent.id}`}
                  >
                    <a>
                      {'<< '} {parent.name}
                    </a>
                  </Link>
                </div>
              )}

              <h1>{crud.categories?.[query.id]?.name || 'Root'}</h1>
              <h3>Children</h3>
              {crud.children.map(c => (
                <CrudChild
                  key={c}
                  item={crud.categories[c]}
                  type="category"
                  onChange={async changes => {
                    return api.crud.categoryUpdate({...changes, id: c});
                  }}
                  onDelete={() => api.crud.categoryDelete({id: c})}
                />
              ))}
              <AddButton
                text="Add child category"
                onClick={() => {
                  api.crud.categoryCreate({parent: query.id});
                }}
              />
              <h3>Images</h3>
              {crud.itemImages.map(it => (
                <CrudAttrImage
                  key={it.id}
                  item={it}
                  image={crud.imageMap[it.img_id]}
                  attributes={crud.attributes}
                  attributeTypes={crud.attributeTypes}
                  api={api}
                  actions={actions}
                  imageMap={imageMap}
                  images={images}
                  imagesTotalCount={imagesTotalCount}
                />
              ))}
              <AddButton
                text="Add category image"
                onClick={() => api.crud.categoryImageCreate({category_id: query.id})}
              />
            </>
          </Grid.Column>
        </Grid>
      </DashboardLayout>
    );
  }
}

export default WithDashboardContext(
  WithActiveConfig(withI18next(['dashboard', 'common'])(Dashboard), true),
);

import React from 'react';
import classnames from 'classnames';
import Link from 'next/link';
import {get} from 'lodash';
import isEmpty from 'is-empty';
import {Form, Table, Grid, Button, Popup, Modal} from 'semantic-ui-react';

import {CONTACT_ITEMS_PER_PAGE} from '@Common/constants/app';

import {default as Defaults, Messages as DefaultMessages} from '@Widget/config/defaults';
import Api from '../../shared/actions/api-actions';
import {withI18next} from '../../shared/i18n/with-i18n.js';
import DashboardLayout from '@Components/layout/dashboard-layout';
import WithDashboardContext from '@Components/hocs/with-dashboard-context';
import WithActiveConfig from '@Components/hocs/with-active-config';
import MyModal from '@Components/modals/generic-modal';
import CustomersTable from '@Components/forms/customers-table/customers-table';
import ActiveCustomer from '@Components/forms/customers-table/active-customer';
import SearchInput from '@Components/inputs/search-input';
import WrappedDropdown from '@Components/inputs/wrapped-dropdown';
import Checkbox from '@Components/inputs/wrapped-checkbox';
import Pagination from '@Components/inputs/pagination';
import {extractIdName} from '../../shared/selectors/dropdown-selector';
import {AssetRole} from '@Common/constants/assets';
import {SelectTip} from '@Widget/components/display/svg-icons';
import ConfirmationModal from '@Components/modals/confirmation-modal';

import dcss from '../dashboard.scss';
import css from './contacts.scss';

class ContactsView extends React.Component {
  static allowedRoles = [AssetRole.owner, AssetRole.manager, AssetRole.operator, AssetRole.analyst];
  constructor(props) {
    super(props);
    this.state = {
      activeCustomer: null,
      page: 0,
      view: 'list'
    };
  }

  componentDidMount() {
    const {
      target: {appId},
      api
    } = this.props;

    api.customers.list({appId, initial: true});
  }

  onClickCustomer = customer => {
    this.setState({activeCustomer: customer.id});
    if (this.props.isMobile) {
      this.setState({view: 'detail'});
    }

    if (!customer.isComplete) {
      const {
        target: {appId},
        api
      } = this.props;
      api.customers.loadProfile({appId, identityId: customer.id, publicId: customer.publicId});
    }
  };

  mobileGoBack = () => {
    this.setState({view: 'list'});
  };

  onSelectSegment = segment => {
    this.props.actions.selectSegment(segment);
    this.performSearch({segment});
  };

  onSearch = search => {
    this.performSearch({search});
  };

  onToggleEmailModal = newState => {
    const condition = typeof newState === 'boolean' ? newState : this.state.view === 'detail';
    const view = condition ? 'email' : 'detail';
    this.setState({view});
  };

  moveToPage = page => {
    if (page > this.state.page) {
      this.performSearch(null, page);
    }
    this.setState({page});
  };

  refreshSearch = () => {
    const {
      customers: {selectedSegment}
    } = this.props;

    this.setState({loadingRefresh: true});
    this.performSearch({segment: selectedSegment}, this.state.page).then(() => {
      this.setState({loadingRefresh: false});
    });
  };

  performSearch = (filters, page = 0) => {
    const {
      target: {appId},
      customers: {selectedSegment},
      api
    } = this.props;

    return api.customers.list({
      appId,
      initial: page === 0 ? true : undefined,
      offset: page * CONTACT_ITEMS_PER_PAGE,
      segment: selectedSegment,
      search: this.refs.search.getValue(),
      // filter may override selectedSegment or search
      ...filters
    });
  };

  saveNotes = ({identityId, notes}) => {
    const {
      target: {appId},
      api
    } = this.props;
    api.customers.saveNotes({appId, identityId, notes});
  };

  onBatchAddSegment = segment => {
    const {
      customers: {selected, list},
      target: {appId},
      api
    } = this.props;

    const customerIds = Object.keys(selected);
    const publicIds = customerIds
      .map(id => {
        const user = list.get(id) || {};
        return user.publicId;
      })
      .filter(pid => Boolean(pid));
    api.customers.batchAddSegment({
      customerIds,
      publicIds,
      appId,
      segmentId: segment.id
    });
  };

  onBatchDeleteContacts = () => {
    this.setState({deletingUsers: true});
  };

  onAbortBatchDeleteCustomer = () => {
    this.setState({deletingUsers: false});
  };

  exportCSV = () => {
    const {
      customers: {list}
    } = this.props;
    if (list.isEmpty()) {
      return;
    }

    const contacts = list.items().reduce(
      (prev, it) => {
        it.email.forEach(e => {
          prev.push(`${it.name},${e},${it.phone}`);
        });
        return prev;
      },
      [`Name,Email,Phone`]
    );

    const csvContent = 'data:text/csv;charset=utf-8,' + contacts.join('\n');
    const encodedUri = encodeURI(csvContent);
    var link = document.createElement('a');
    link.setAttribute('href', encodedUri);
    link.setAttribute('download', 'contacts.csv');
    document.body.appendChild(link); // Required for FF
    link.click();
  };

  doDeleteBatchCustomer = () => {
    const {
      customers: {selected},
      target: {appId},
      api
    } = this.props;

    const ids = Object.keys(selected || {});

    if (ids.length) {
      api.customers.deleteIdentity({
        identityId: ids,
        appId
      });
    }
    this.onAbortBatchDeleteCustomer();
  };

  sendBatchEmail = () => {
    const {
      customers: {selected, list}
    } = this.props;
    if (!this.refs.activeCustomer) {
      return;
    }

    let emails = [];
    Object.keys(selected).forEach(id => {
      const contacts = list.get(id);
      if (contacts && !isEmpty(contacts.email)) {
        emails.push(...contacts.email);
      }
    });

    this.refs.activeCustomer.wrappedInstance.sendBatchEmail(emails);
  };

  getBatchActions() {
    const {
      customers: {segments},
      t
    } = this.props;

    return [
      <div className={classnames('item', css.extendableMenu)} key={'addSegment'}>
        <SelectTip className={css.tip} />
        {t('addSegment')}
        <div className={css.extended}>
          {segments.items().map(segment => (
            <div
              key={segment.id}
              className={classnames(css.extendedItem, 'item')}
              onClick={this.onBatchAddSegment.bind(this, segment)}
            >
              {segment.name}
            </div>
          ))}
        </div>
      </div>,
      <div className="item" key="emailSelected" onClick={this.sendBatchEmail}>
        {t('emailSelected')}
      </div>,
      <div className="item" key="deleteSelected" onClick={this.onBatchDeleteContacts}>
        {t('deleteSelected')}
      </div>,
      <div className="item" key="exportCSV" onClick={this.exportCSV}>
        {t('exportContactsCSV')}
      </div>
    ];
  }

  render() {
    const {
      api,
      actions: {toggleSelectCustomer},
      target: {appId},
      app: {selectedApp, config: serverConfig},
      user: {assets},
      config,
      user,
      customers,
      customers: {list, segments, selected, selectedSegment, totalCount},
      t,
      isMobile,
      isTablet
    } = this.props;
    const {page, view, loadingRefresh} = this.state;

    const {activeCustomer, deletingUsers} = this.state;
    const activeCustomerData = customers.list.get(activeCustomer);

    return (
      <DashboardLayout
        {...this.props}
        mainTitle={
          view === 'detail'
            ? activeCustomerData.name
            : view === 'email'
            ? t('sendEmail')
            : t('contacts')
        }
        goBack={
          view === 'detail'
            ? this.mobileGoBack
            : view === 'email' && this.refs.activeCustomer
            ? this.refs.activeCustomer.wrappedInstance.onAbortSendEmail
            : null
        }
        refreshAction={this.refreshSearch}
        loadingRefresh={loadingRefresh}
      >
        <Grid columns={16} padded>
          {(!isMobile || view === 'list') && (
            <Grid.Column computer={11} tablet={10} mobile={16} className={dcss.mainColumn}>
              <Grid.Row className={css.contactsMenuRow}>
                <Grid columns={16}>
                  <Grid.Column computer={8} className={css.contactsActions} only={'computer'}>
                    <WrappedDropdown
                      size="mini"
                      inline
                      options={this.getBatchActions()}
                      placeholder={t('batchActions')}
                      className="overflowVisible"
                    />
                    <span className={css.selectedNumber}>
                      {t('xContactsSelected', {count: Object.keys(selected).length})}
                    </span>
                  </Grid.Column>
                  <Grid.Column computer={8} mobile={16} className={css.contactsFilters}>
                    {!isMobile && (
                      <WrappedDropdown
                        size="mini"
                        inline
                        value={selectedSegment}
                        onChange={this.onSelectSegment}
                        emptyOption={t('allSegments')}
                        options={segments.items ? segments.items() : []}
                        formatFunction={extractIdName}
                        placeholder={t('filterBySegment')}
                      />
                    )}
                    <SearchInput
                      size="mini"
                      inline={!isMobile}
                      fluid={isMobile}
                      onSearch={this.onSearch}
                      ref="search"
                      placeholder={t('searchCustomers')}
                      addon={'button'}
                      t={t}
                    />
                  </Grid.Column>
                </Grid>
              </Grid.Row>
              <Grid.Row className="margin">
                <CustomersTable
                  page={page}
                  perPage={CONTACT_ITEMS_PER_PAGE}
                  customers={customers}
                  onClickCustomer={this.onClickCustomer}
                  toggleSelectCustomer={toggleSelectCustomer}
                  isMobile={isMobile}
                  isTablet={isTablet}
                />
              </Grid.Row>
              <Grid.Row>
                {
                  <Pagination
                    page={page}
                    totalCount={totalCount}
                    moveToPage={this.moveToPage}
                    perPage={CONTACT_ITEMS_PER_PAGE}
                  />
                }
              </Grid.Row>
            </Grid.Column>
          )}
          {(!isMobile || view === 'detail' || view === 'email') && (
            <Grid.Column computer={5} tablet={6} mobile={16} className={dcss.columnSeparator}>
              <ActiveCustomer
                serverConfig={serverConfig}
                user={user}
                onToggleEmailModal={this.onToggleEmailModal}
                app={assets.get(selectedApp)}
                appConfig={config}
                appId={appId}
                sendEmail={api.email.send}
                customer={activeCustomerData}
                segments={customers.segments}
                removeSegment={api.customers.removeSegment}
                addSegment={api.customers.addSegment}
                saveNotes={api.customers.saveNotes}
                deleteIdentity={api.customers.deleteIdentity}
                isMobile={isMobile}
                ref="activeCustomer"
              />
            </Grid.Column>
          )}
        </Grid>
        {deletingUsers && (
          <ConfirmationModal
            onConfirm={this.doDeleteBatchCustomer}
            onClose={this.onAbortBatchDeleteCustomer}
            message={t('sureDeleteCustomer')}
            title={t('deleteContact')}
            yes={t('yesDelete')}
            no={t('noKeep')}
          />
        )}
      </DashboardLayout>
    );
  }
}

export default WithDashboardContext(
  WithActiveConfig(withI18next(['dashboard', 'common'])(ContactsView), true)
);

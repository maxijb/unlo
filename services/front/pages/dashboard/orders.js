import React, {useMemo, useState, useEffect, useRef} from 'react';
import Link from 'next/link';
import {Grid, Table, Button} from 'semantic-ui-react';
import classnames from 'classnames';
import {debounce} from 'lodash';
import Router from 'next/router';
import Logger from '@Common/logger';
import {DefaultPerPage, OrderVisibleStatus} from '@Common/constants/app';
import {getPageLink} from '@Common/utils/urls';

import Api from '../../shared/actions/api-actions';
import Actions from '../../shared/actions/app-actions';
import {withI18next} from '../../shared/i18n/with-i18n.js';

import DashboardLayout from '../../shared/components/layout/dashboard-layout';
import WithActiveConfig from '@Components/hocs/with-active-config';
import WithDashboardContext from '../../shared/components/hocs/with-dashboard-context';

import Dropdown from '@Components/inputs/wrapped-dropdown';
import Input from '@Components/inputs/wrapped-input';
import AddButton from '@Components/inputs/add-button';
import CrudAttrImage from '@Components/crud/attr-image';
import CrudChild from '@Components/crud/child';
import CrudOrder from '@Components/crud/crud-order';
import Pagination from '@Components/inputs/pagination';

import styles from '@Common/styles/styles.scss';
import css from '../dashboard.scss';
import crudCss from '@Components/crud/crud.scss';

function DashboardOrders(props) {
  const {t, crud, user, api, actions, query} = props;
  const {images, imageMap, productsTotalCount} = crud;

  const [page, setPage] = useState(0);
  const [search, setSearch] = useState('');
  const [loading, setLoading] = useState(false);
  const [visibleStatus, setVisibleStatus] = useState('Active');

  const loadData = useMemo(() => {
    return debounce(payload => {
      setLoading(true);
      return api.crud.ordersList(payload).then(_, setLoading(false));
    }, 500);
  }, []);

  const didMountRef = useRef(0);

  const load = () => {
    if (didMountRef.current > 2) {
      const payload = {
        visibleStatus,
        offset: page * DefaultPerPage,
        search,
      };
      window.history.pushState(null, window.title, window.location.pathname);
      loadData(payload);
    }
  };

  useEffect(() => {
    didMountRef.current++;
    setPage(0);
    load();
  }, [search, visibleStatus]);

  useEffect(() => {
    didMountRef.current++;
    load();
  }, [page]);

  return (
    <DashboardLayout {...props} mainTitle={t('dashboard')}>
      <Grid columns={16} padded>
        <Grid.Column computer={16} className={css.mainColumn} mobile={16} tablet={16}>
          <>
            <div className={styles.FlexSpaceBetween}>
              <h1 className={classnames(crudCss.line)} style={{alignItems: 'flex-end'}}>
                <span className={styles.MarginDoubleRight}>Orders</span>

                <Button size="mini" href={`/api/public/get-seller-orders-csv`}>
                  Download Report
                </Button>
              </h1>
              <div className={classnames(styles.NoWrap, styles.FlexNoShrink, styles.FlexCenter)}>
                <span className={classnames(styles.MarginHalfRight, styles.Small, styles.NoWrap)}>
                  SEARCH
                </span>
                <span className={classnames(styles.MarginRight, styles.Small, styles.NoWrap)}>
                  <Input
                    size="small"
                    value={search}
                    placeholder="Name, email or phone"
                    onChange={val => setSearch(val)}
                  />
                </span>
                <span className={classnames(styles.MarginHalfRight, styles.Small, styles.NoWrap)}>
                  FILTER BY STATUS
                </span>

                <Dropdown
                  placeholder="Select Status"
                  size="mini"
                  value={visibleStatus}
                  options={[
                    {value: '', text: 'All'},
                    ...Object.keys(OrderVisibleStatus).map(c => ({
                      value: c,
                      text: c,
                    })),
                  ]}
                  onChange={val => {
                    setVisibleStatus(val);
                  }}
                />
              </div>
            </div>

            <Table striped className={crudCss.table}>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell textAlign="center">Order ID</Table.HeaderCell>
                  <Table.HeaderCell>Customer</Table.HeaderCell>
                  <Table.HeaderCell>Date</Table.HeaderCell>
                  <Table.HeaderCell>Amount</Table.HeaderCell>
                  <Table.HeaderCell>Status</Table.HeaderCell>
                  <Table.HeaderCell>Tracking No.</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {crud.orders.list.map(order => {
                  return (
                    <CrudOrder
                      api={api}
                      order={order}
                      key={order.id}
                      user={user}
                      crud={crud}
                      forceExpanded={order.id == query.id}
                    />
                  );
                })}
              </Table.Body>
            </Table>
            <Pagination
              totalCount={crud.orders.totalCount}
              page={page}
              perPage={DefaultPerPage}
              moveToPage={num => {
                setPage(num);
              }}
            />
          </>
        </Grid.Column>
      </Grid>
    </DashboardLayout>
  );
}

DashboardOrders.getInitialProps = async ({req, reduxStore, dispatch, query}) => {
  await Promise.all([
    dispatch(Api.crud.ordersList(query.id ? {ids: [query.id]} : {visibleStatus: 'Active'}, req)),
    dispatch(Api.crud.attributesList({}, req)),
  ]);
  return {query};
};

export default WithDashboardContext(
  WithActiveConfig(withI18next(['dashboard', 'common'])(DashboardOrders), true),
);

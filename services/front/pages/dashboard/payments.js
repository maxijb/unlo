import React, {useMemo, useState, useEffect} from 'react';
import Link from 'next/link';
import {Grid, Table, Button, Modal} from 'semantic-ui-react';
import classnames from 'classnames';
import Router from 'next/router';
import Logger from '@Common/logger';
import {DefaultPerPage, OrderVisibleStatus, BillableTypes} from '@Common/constants/app';
import {getPageLink} from '@Common/utils/urls';

import Api from '../../shared/actions/api-actions';
import Actions from '../../shared/actions/app-actions';
import {withI18next} from '../../shared/i18n/with-i18n.js';

import DashboardLayout from '../../shared/components/layout/dashboard-layout';
import WithActiveConfig from '@Components/hocs/with-active-config';
import WithDashboardContext from '../../shared/components/hocs/with-dashboard-context';

import ErrorRenderer from '@Components/inputs/error-renderer';
import Dropdown from '@Components/inputs/wrapped-dropdown';
import Checkbox from '@Components/inputs/wrapped-checkbox';
import Input from '@Components/inputs/wrapped-input';
import AddButton from '@Components/inputs/add-button';
import GenericModal from '@Components/modals/generic-modal';
import CrudAttrImage from '@Components/crud/attr-image';
import CrudChild from '@Components/crud/child';
import CrudBillable from '@Components/crud/crud-billable';
import Pagination from '@Components/inputs/pagination';
import {DateInput} from 'semantic-ui-calendar-react';

import styles from '@Common/styles/styles.scss';
import css from '../dashboard.scss';
import crudCss from '@Components/crud/crud.scss';

function DashboardPayments(props) {
  const {t, crud, user, api, actions, query} = props;

  const [page, setPage] = useState(0);
  const [seller, setSeller] = useState(query.seller || null);
  const [error, setError] = useState(null);
  const [minDate, setMinDate] = useState('');
  const [maxDate, setMaxDate] = useState('');

  const [newPayment, setNewPayment] = useState(null);

  const setPayment = (value, name) => {
    setNewPayment({
      ...newPayment,
      [name]: value,
    });
  };

  const handleDateChange = (event, {name, value}) => {
    const method = name === 'minDate' ? setMinDate : setMaxDate;
    method(value);
  };

  const sellerOptions = useMemo(() => {
    return Object.keys(crud.sellers).map(key => ({
      key,
      value: key,
      text: `${key} - ${crud.sellers[key].name}`,
    }));
  }, crud.sellers);

  useEffect(() => {
    api.crud.billablesList({seller, offset: page * DefaultPerPage});
  }, [seller, page]);

  const [showReportModal, setShowReportModal] = useState(false);
  const [includeSummary, setIncludeSummary] = useState(false);
  const [includeStateSummary, setIncludeStateSummary] = useState(false);

  return (
    <DashboardLayout {...props} mainTitle={t('dashboard')}>
      <Grid columns={16} padded>
        <Grid.Column computer={16} className={css.mainColumn} mobile={16} tablet={16}>
          <>
            <div className={styles.FlexSpaceBetween}>
              <h1 className={classnames(crudCss.line)} style={{alignItems: 'flex-end'}}>
                <span className={styles.MarginDoubleRight}>Payments</span>
                {user.superAdmin && (
                  <AddButton
                    text="Add event"
                    onClick={() => {
                      setNewPayment({});
                    }}
                  />
                )}
                <Button size="mini" onClick={() => setShowReportModal(true)}>
                  Download Report
                </Button>
              </h1>
              {user.superAdmin && (
                <div className={classnames(styles.NoWrap, styles.FlexNoShrink, styles.FlexCenter)}>
                  <span
                    className={classnames(
                      styles.MarginHalfRight,
                      styles.Small,
                      styles.NoWrap,
                      styles.Capitalize,
                    )}
                  >
                    {t('common:seller')}
                  </span>

                  <Dropdown
                    placeholder="Select Seller"
                    size="mini"
                    value={seller || ''}
                    options={sellerOptions}
                    onChange={val => {
                      setSeller(val);
                      setPage(0);
                    }}
                  />
                </div>
              )}
            </div>

            {newPayment && (
              <Grid columns={16} className={styles.sectionWithBackground}>
                <Grid.Row>
                  <Grid.Column width={2}>
                    <Dropdown
                      options={Object.keys(BillableTypes)}
                      fluid
                      onChange={setPayment}
                      value={newPayment.type || ''}
                      name="type"
                      placeholder="Type"
                    />
                  </Grid.Column>
                  <Grid.Column width={6}>
                    <Input
                      fluid
                      name="description"
                      placeholder="Description (optional)"
                      onChange={setPayment}
                      value={newPayment.description || ''}
                    />
                  </Grid.Column>
                  <Grid.Column width={3}>
                    <Input
                      fluid
                      name="credit"
                      placeholder="Credit"
                      onChange={setPayment}
                      value={newPayment.credit || ''}
                    />
                  </Grid.Column>
                  <Grid.Column width={3}>
                    <Input
                      fluid
                      name="debit"
                      placeholder="Debit"
                      onChange={setPayment}
                      value={newPayment.debit || ''}
                    />
                  </Grid.Column>
                  <Grid.Column width={2}>
                    <Button
                      primary
                      onClick={() => {
                        api.crud
                          .saveBillable({
                            seller_id: seller,
                            ...newPayment,
                          })
                          .then(response => {
                            if (response.resp.error) {
                              setError(response.resp.message || response.resp.error);
                            } else {
                              setNewPayment(null);
                            }
                          });
                      }}
                    >
                      Save
                    </Button>
                    <ErrorRenderer error={error} />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            )}

            <Table striped className={crudCss.table}>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>ID</Table.HeaderCell>
                  <Table.HeaderCell>Date</Table.HeaderCell>
                  <Table.HeaderCell>Description</Table.HeaderCell>
                  <Table.HeaderCell textAlign="right">Credit</Table.HeaderCell>
                  <Table.HeaderCell textAlign="right">Debit</Table.HeaderCell>
                  <Table.HeaderCell textAlign="right">Balance</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {crud.billables.list.map(billable => {
                  return <CrudBillable billable={billable} />;
                })}
              </Table.Body>
            </Table>
            <Pagination
              totalCount={crud.billables?.totalCount || 0}
              page={page}
              perPage={DefaultPerPage}
              moveToPage={num => {
                setPage(num);
              }}
            />
          </>
        </Grid.Column>
      </Grid>
      {showReportModal && (
        <GenericModal onClose={() => setShowReportModal(false)} open={showReportModal}>
          <Modal.Header>
            <div className={classnames(styles.FlexCenter, styles.FlexSpaceBetween)}>
              <span>Export payments report</span>
              <div className={styles.FlexCenter}>
                <DateInput
                  name="minDate"
                  placeholder="Start date"
                  iconPosition="right"
                  value={minDate}
                  onChange={handleDateChange}
                  dateFormat="YYYY-MM-DD"
                  className={classnames(styles.NoMargin, styles.MarginDoubleRight)}
                />

                <DateInput
                  name="maxDate"
                  placeholder="End date"
                  iconPosition="right"
                  value={maxDate}
                  dateFormat="YYYY-MM-DD"
                  onChange={handleDateChange}
                  className={classnames(styles.NoMargin, styles.MarginDoubleRight)}
                />
              </div>
            </div>
          </Modal.Header>
          <Modal.Content>
            <div className={styles.MarginBottom}>
              <Checkbox
                id="summary"
                onClick={() => setIncludeSummary(!includeSummary)}
                checked={includeSummary}
                label="Include summary"
              />
            </div>
            <div className={styles.MarginBottom}>
              <Checkbox
                id="stateTax"
                disabled={!includeSummary}
                checked={includeSummary && includeStateSummary}
                onClick={() => setIncludeStateSummary(!includeStateSummary)}
                label="Include Sales Tax by State"
              />
            </div>
            <div className={styles.Centered}>
              <Button
                primary
                href={`/api/public/get-seller-payments-csv?seller_id=${seller}&includeSummary=${includeSummary}&includeStateSummary=${
                  includeSummary && includeStateSummary
                }&minDate=${minDate}&maxDate=${maxDate}`}
                onClick={() => setShowReportModal(false)}
              >
                Generate report
              </Button>
            </div>
          </Modal.Content>
        </GenericModal>
      )}
    </DashboardLayout>
  );
}

DashboardPayments.getInitialProps = async ({req, reduxStore, dispatch, query}) => {
  await Promise.all([dispatch(Api.crud.sellersList({}, req))]);
  return {query};
};

export default WithDashboardContext(
  WithActiveConfig(withI18next(['dashboard', 'common'])(DashboardPayments), true),
);

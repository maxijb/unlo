import React, {useMemo, useState, useEffect} from 'react';
import Link from 'next/link';
import {Grid, Table} from 'semantic-ui-react';
import classnames from 'classnames';
import Router from 'next/router';
import Logger from '@Common/logger';
import {DefaultPerPage} from '@Common/constants/app';
import {getPageLink} from '@Common/utils/urls';

import {debounce} from 'lodash';
import Api from '../../shared/actions/api-actions';
import Actions from '../../shared/actions/app-actions';
import {withI18next} from '../../shared/i18n/with-i18n.js';

import DashboardLayout from '../../shared/components/layout/dashboard-layout';
import WithActiveConfig from '@Components/hocs/with-active-config';
import WithDashboardContext from '../../shared/components/hocs/with-dashboard-context';

import CrudAttrFilter from '@Components/crud/attr-filter';
import CSVInput from '@Components/inputs/csv-input';
import CSVCompetitorsInput from '@Components/inputs/csv-competitors-input';
import Dropdown from '@Components/inputs/wrapped-dropdown';
import Input from '@Components/inputs/wrapped-input';
import AddButton from '@Components/inputs/add-button';
import CrudAttrImage from '@Components/crud/attr-image';
import CrudChild from '@Components/crud/child';
import CrudProduct from '@Components/crud/crud-product';
import CreateCrudProduct from '@Components/crud/create-crud-product';
import CrudSellerProduct from '@Components/crud/crud-seller-product';
import ImportErrors from '@Components/crud/import-errors';
import Pagination from '@Components/inputs/pagination';

import styles from '@Common/styles/styles.scss';
import css from '../dashboard.scss';
import crudCss from '@Components/crud/crud.scss';

function Dashboard(props) {
  const {t, crud, user, api, actions, query} = props;
  const {images, imageMap, productsTotalCount} = crud;
  const category = crud.categories?.[query.id];
  const parent = crud.categories[category?.parent];
  const [page, setPage] = useState(0);
  const [loading, setLoading] = useState(false);

  const [newProduct, setNewProduct] = useState(false);
  const [errors, setErrors] = useState(false);
  const [filters, setFilters] = useState({});
  const [skuFilter, setSkuFilter] = useState('');
  const [buyBoxFilter, setBuyBoxFilter] = useState('');
  const [stockFilter, setStockFilter] = useState('');
  const [totalErrors, setTotalErrors] = useState(null);
  const [importErrors, setImportErrors] = useState(null);
  const [sellers, setSellers] = useState({});

  useEffect(() => {
    if (user.superAdmin) {
      api.crud.loadSellerNames().then(response => {
        setSellers(response.resp.sellers);
      });
    }
  }, []);

  const loadData = useMemo(() => {
    return debounce(search => {
      setLoading(true);
      return api.crud.productsList(search).then(_, setLoading(false));
    }, 500);
  }, []);

  const load = () => {
    const search = query.id ? {category_id: query.id} : {};
    search.offset = page * DefaultPerPage;
    search.attributes = Object.values(filters).filter(a => !!a);
    if (skuFilter) {
      search.source_id = skuFilter;
    }
    search.buyBoxFilter = buyBoxFilter;
    search.stockFilter = stockFilter;

    loadData(search);
  };

  useEffect(() => {
    setPage(0);
    load();
  }, [filters, query.id, skuFilter, buyBoxFilter, stockFilter]);
  useEffect(load, [page]);
  useEffect(load, []);

  const breadcrumb = useMemo(() => {
    let current = category;
    const items = [];

    while (current) {
      items.push({id: current.id, name: current.name});
      current = crud.categories?.[current.parent];
    }
    items.push({id: null, name: 'Root'});
    return items.reverse();
  }, [category, crud.categories]);

  const urlPrefix = user.superAdmin ? 'dashboard' : 'portal';
  return (
    <DashboardLayout {...props} mainTitle={t('dashboard')}>
      <Grid columns={16} padded>
        <Grid.Column computer={16} className={css.mainColumn} mobile={16} tablet={16}>
          <>
            <div className={styles.MarginBottom}>
              {breadcrumb.map((it, i) => {
                return (
                  <>
                    <Link
                      href={`/dashboard/products?id=${it?.id || null}`}
                      as={`/${urlPrefix}/products/${it?.id || ''}`}
                    >
                      <a style={{textDecoration: 'none'}}>{it?.name || 'Root'}</a>
                    </Link>
                    {i < breadcrumb.length - 1 && (
                      <span className={classnames(styles.MarginHalfLeft, styles.MarginHalfRight)}>
                        {' > '}
                      </span>
                    )}
                  </>
                );
              })}
            </div>

            <div className={styles.FlexSpaceBetween}>
              <h1 className={crudCss.line}>
                <span className={styles.MarginRight}>
                  {crud.categories?.[query.id]?.name || 'Root'}
                </span>
                {(crud.children?.length || 0) > 0 && (
                  <Dropdown
                    placeholder="Child category"
                    value={''}
                    options={[
                      {value: '', text: 'Select category'},
                      ...crud.children.map(c => ({
                        value: c,
                        text: crud.categories[c]?.name,
                      })),
                    ]}
                    onChange={val => {
                      if (val) {
                        Router.push(
                          `/dashboard/products?id=${val}`,
                          `/${urlPrefix}/products/${val}`,
                        );
                      }
                    }}
                  />
                )}
              </h1>
              <div className={styles.FlexCenter}>
                <CSVInput
                  onFileUploaded={(err, response) => {
                    console.log(response);
                    if (!err) {
                      load();
                      setTotalErrors(response.totalErrors);
                      setImportErrors(response.errors);
                    } else {
                      setTotalErrors(null);
                      setImportErrors(null);
                    }
                  }}
                />
                {user.superAdmin && (
                  <CSVCompetitorsInput
                    onFileUploaded={(err, response) => {
                      console.log(response);
                      if (!err) {
                        load();
                        setTotalErrors(response.totalErrors);
                        setImportErrors(response.errors);
                      } else {
                        setTotalErrors(null);
                        setImportErrors(null);
                      }
                    }}
                  />
                )}
              </div>
            </div>

            <ImportErrors errors={importErrors} totalErrors={totalErrors} />

            <div
              className={classnames(
                styles.FlexSpaceBetween,

                styles.MarginDoubleTop,
                styles.MarginBottom,
              )}
            >
              <h3 style={{marginBottom: 0}}>
                <span class={styles.MarginRight}>Product Filters</span>
                {category?.isLeaf && (
                  <AddButton
                    text="Add product"
                    onClick={() => {
                      if (user.superAdmin) {
                        api.crud.productCreate({category_id: query.id});
                      } else {
                        setNewProduct({});
                      }
                    }}
                  />
                )}
              </h3>
            </div>
            <div
              className={classnames(
                styles.FlexCenter,
                styles.FlexSpaceBetween,
                styles.FlexWrap,
                styles.MarginDoubleBottom,
                crudCss.filtersHeader,
              )}
            >
              <div
                className={classnames(
                  crudCss.attrInputLong,
                  styles.MarginLeft,
                  styles.MarginBottom,
                )}
              >
                <Input
                  fluid
                  placeholder="Source ID / SKU"
                  onChange={setSkuFilter}
                  value={skuFilter}
                />
              </div>
              <div
                className={classnames(
                  crudCss.attrInputLong,
                  styles.MarginLeft,
                  styles.MarginBottom,
                )}
              >
                <Dropdown
                  value={stockFilter}
                  options={[
                    {value: '', text: 'All'},
                    {value: 'withStock', text: 'In Stock'},
                    {value: 'withoutStock', text: 'Out of Stock'},
                  ]}
                  onChange={setStockFilter}
                  fluid
                  placeholder="Availability"
                />
              </div>
              <div
                className={classnames(
                  crudCss.attrInputLong,
                  styles.MarginLeft,
                  styles.MarginBottom,
                )}
              >
                <Dropdown
                  value={buyBoxFilter}
                  options={[
                    {value: '', text: 'All'},
                    {value: 'in-buybox', text: 'In Buy Box'},
                    {value: 'out-buybox', text: 'No Buy Box'},
                  ]}
                  onChange={setBuyBoxFilter}
                  fluid
                  placeholder="Buy Box"
                />
              </div>
              {['capacity', 'color', 'carrier', 'appearance'].map(attr => (
                <CrudAttrFilter
                  fixedType={attr}
                  attributes={crud.attributes}
                  attributeTypes={crud.attributeTypes}
                  onSelect={attr_id => {
                    const newFilters = {
                      ...filters,
                      [attr]: attr_id,
                    };

                    setFilters(newFilters);
                  }}
                />
              ))}
            </div>

            {newProduct && (
              <CreateCrudProduct
                category={category}
                onClose={() => setNewProduct(false)}
                api={api}
                user={user}
                attributes={crud.attributes}
                attributeTypes={crud.attributeTypes}
              />
            )}

            {user.superAdmin ? (
              crud.products.map(p => (
                <CrudProduct
                  seller={sellers[crud.productMap[p]?.owner]}
                  product={crud.productMap[p]}
                  key={p}
                  attributes={crud.attributes}
                  attributeTypes={crud.attributeTypes}
                  api={api}
                  user={user}
                />
              ))
            ) : (
              <Table striped className={crudCss.table}>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell></Table.HeaderCell>
                    <Table.HeaderCell>Product</Table.HeaderCell>
                    <Table.HeaderCell>Seller ProductID / SKU</Table.HeaderCell>
                    <Table.HeaderCell>Stock</Table.HeaderCell>
                    <Table.HeaderCell>Price</Table.HeaderCell>
                    <Table.HeaderCell>Buy Box</Table.HeaderCell>
                    <Table.HeaderCell>Actions</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  {crud.products.map(p => {
                    return (
                      <CrudSellerProduct
                        product={crud.productMap[p]}
                        key={p}
                        attributes={crud.attributes}
                        attributeTypes={crud.attributeTypes}
                        api={api}
                      />
                    );
                  })}
                </Table.Body>
              </Table>
            )}

            <Pagination
              totalCount={productsTotalCount}
              page={page}
              perPage={DefaultPerPage}
              moveToPage={num => {
                setPage(num);
              }}
            />
          </>
        </Grid.Column>
      </Grid>
    </DashboardLayout>
  );
}

Dashboard.getInitialProps = async ({req, reduxStore, dispatch, query}) => {
  if (!query.id) {
    query.id = 3;
  }
  if (query.id === 'null') {
    query.id = null;
  }
  const id = query.id;

  const current = await dispatch(Api.crud.categoryGet({id}, req));
  await Promise.all([
    query.id ? dispatch(Api.crud.categoryGet({id: current?.resp?.parent}, req)) : null,
    dispatch(Api.crud.categoriesList({parent: id}, req)),
    dispatch(Api.crud.categoryImagesList({id}, req)),
    dispatch(Api.crud.attributesList({}, req)),
  ]);
  return {query};
};

export default WithDashboardContext(
  WithActiveConfig(withI18next(['dashboard', 'common'])(Dashboard), true),
);

import React, {useMemo, useState, useEffect} from 'react';
import Link from 'next/link';
import {Grid} from 'semantic-ui-react';
import classnames from 'classnames';
import Router from 'next/router';
import Logger from '@Common/logger';
import {DefaultPerPage} from '@Common/constants/app';
import {getPageLink} from '@Common/utils/urls';

import Api from '../../shared/actions/api-actions';
import Actions from '../../shared/actions/app-actions';
import {withI18next} from '../../shared/i18n/with-i18n.js';

import DashboardLayout from '../../shared/components/layout/dashboard-layout';
import WithActiveConfig from '@Components/hocs/with-active-config';
import WithDashboardContext from '../../shared/components/hocs/with-dashboard-context';

import Dropdown from '@Components/inputs/wrapped-dropdown';
import AddButton from '@Components/inputs/add-button';
import CrudAttrImage from '@Components/crud/attr-image';
import CrudChild from '@Components/crud/child';
import CrudProduct from '@Components/crud/crud-product';
import Pagination from '@Components/inputs/pagination';

import styles from '@Common/styles/styles.scss';
import css from '../dashboard.scss';
import crudCss from '@Components/crud/crud.scss';

function DashboardTokens(props) {
  const {
    t,
    crud,
    user,
    api,
    actions,
    query,
    token,
    app: {config},
  } = props;

  const [seller, setSeller] = useState(null);
  const [sellerToken, setSellerToken] = useState(null);

  const sellerOptions = useMemo(() => {
    return Object.keys(crud.sellers).map(key => ({
      key,
      value: key,
      text: `${key} - ${crud.sellers[key].name}`,
    }));
  }, crud.sellers);

  const url = `${config.protocol}://${config.fullDomain}/signup?inviteToken=${token}`;
  const sellerUrl = `${config.protocol}://${config.fullDomain}/signup?inviteToken=${sellerToken}`;

  useEffect(() => {
    setSellerToken(null);
    if (seller) {
      api.crud.getNewSellerToken({seller_id: seller}).then(response => {
        if (response.resp.token) {
          setSellerToken(response.resp.token);
        }
      });
    }
  }, [seller]);

  return (
    <DashboardLayout {...props} mainTitle={t('dashboard')}>
      <Grid columns={16} padded>
        <Grid.Column computer={16} className={css.mainColumn} mobile={16} tablet={16}>
          <>
            <h1 className={crudCss.line}>
              <span className={styles.MarginRight}>Tokens</span>
            </h1>

            <h3>
              <span class={styles.MarginRight}>
                Share this URL with new seller for their onboarding
              </span>
            </h3>
            <h4>
              <a href={url} target="_blank">
                {url}
              </a>
            </h4>
            <h3>
              <span class={styles.MarginRight}>Or share a token for an already created seller</span>
            </h3>
            <div className={classnames(styles.NoWrap, styles.FlexNoShrink, styles.FlexCenter)}>
              <span
                className={classnames(
                  styles.MarginHalfRight,
                  styles.Small,
                  styles.NoWrap,
                  styles.Capitalize,
                )}
              >
                {t('common:seller')}
              </span>

              <Dropdown
                placeholder="Select Seller"
                size="mini"
                value={seller || ''}
                options={sellerOptions}
                onChange={val => {
                  setSeller(val);
                }}
              />
            </div>
            {sellerToken && (
              <h4>
                <a href={sellerUrl} target="_blank">
                  {sellerUrl}
                </a>
              </h4>
            )}
          </>
        </Grid.Column>
      </Grid>
    </DashboardLayout>
  );
}

DashboardTokens.getInitialProps = async ({req, reduxStore, dispatch, query}) => {
  if (!query.id || query.id === 'null') {
    query.id = null;
  }
  const id = query.id;

  await Promise.all([dispatch(Api.crud.sellersList({}, req))]);
  const tokenResp = await dispatch(Api.crud.getNewSellerToken({}, req));
  return {token: tokenResp.resp.token};
};

export default WithDashboardContext(
  WithActiveConfig(withI18next(['dashboard', 'common'])(DashboardTokens), true),
);

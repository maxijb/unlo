import React from 'react';
import classnames from 'classnames';
import Footer from '@Components/static/footer';
import Navigation from '@Components/static/navigation';
import {Grid, Button, Loader} from 'semantic-ui-react';
import Api from '../shared/actions/api-actions';
import {withI18next} from '../shared/i18n/with-i18n.js';

import css from '@Common/styles/styles.scss';

class UnsubscribeView extends React.Component {
  static async getInitialProps({req, query, dispatch}) {
    if (!req || !req.query.token) {
      return {error: AuthenticationErrorMessage.missingToken};
    }

    const info = await dispatch(Api.emailNotifications.unsubscribe({token: req.query.token}, req));
    return info.resp;
  }

  constructor(props) {
    super(props);
    this.state = {
      reenable: false,
    };
  }

  reenable = () => {
    const {api, token} = this.props;
    this.setState({reenable: 'loading'});
    this.props.api.emailNotifications.reenable({token}).then(() => {
      this.setState({reenable: true});
    });
  };

  renderReenable() {
    const {t} = this.props;
    const {reenable} = this.state;

    if (reenable === 'loading') {
      return (
        <div className={classnames(css.MarginTop, css.PaddingTop)}>
          <Loader inverted active={true} size="large" />
        </div>
      );
    }

    return (
      <React.Fragment>
        <div className={classnames(css.HeroBold, css.MarginTop, css.PaddingTop)}>
          {t('unsubscribe.reenableTitle')}
        </div>
        <div className={classnames(css.MarginTop, css.Large)}>
          {t('unsubscribe.reenableLegend')}
        </div>
      </React.Fragment>
    );
  }

  renderError() {
    const {t} = this.props;
    return (
      <React.Fragment>
        <div className={classnames(css.HeroBold, css.MarginTop, css.PaddingTop)}>
          {t('unsubscribe.error')}
        </div>
        <div className={classnames(css.MarginTop, css.Large)}>
          {t('unsubscribe.refreshOrTryAgain')}
        </div>
      </React.Fragment>
    );
  }

  renderSuccess() {
    const {t} = this.props;
    return (
      <React.Fragment>
        <div className={classnames(css.HeroBold, css.MarginTop, css.PaddingTop)}>
          {t('unsubscribe.title')}
        </div>
        <div className={classnames(css.MarginTop, css.Large)}>{t('unsubscribe.legend')}</div>
        <div className={classnames(css.MarginTop, css.Medium)}>{t('unsubscribe.disclaimer')}</div>
        <div className={classnames(css.Medium)}>{t('unsubscribe.disclaimer2')}</div>
        <div className={classnames(css.MarginTop)}>
          <Button primary onClick={this.reenable}>
            {t('unsubscribe.button')}
          </Button>
        </div>
      </React.Fragment>
    );
  }

  render() {
    const {t, error} = this.props;
    const {reenable} = this.state;

    return (
      <React.Fragment>
        <Navigation />
        <Grid columns={16}>
          <Grid.Column width={1} textAlign="center" />
          <Grid.Column width={14} textAlign="center">
            {error && this.renderError()}
            {!error && !reenable && this.renderSuccess()}
            {!error && reenable && this.renderReenable()}
          </Grid.Column>
          <Grid.Column width={1} textAlign="center" />
        </Grid>
        <Footer />
      </React.Fragment>
    );
  }
}

export default withI18next(['home'])(UnsubscribeView);

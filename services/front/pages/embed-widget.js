import React from 'react';
import Link from 'next/link';

import Logger from '@Common/logger';
import {getPageLink} from '@Common/utils/urls';

import Actions from '../shared/actions/app-actions';
import Api from '../shared/actions/api-actions';
import {withI18next} from '../shared/i18n/with-i18n.js';
// import {WidgetWrapper} from '../../widget/dist/lib';
import Header from '../shared/components/header/header';

// const que = require('../../widget/src/components/root/root');
// import WidgetRootNative from '../../widget/src/components/root/root';
import WidgetRoot from '@Widget/components/root/root';
import WidgetApi from '@Widget/connections/widget-api';
import SafeSocket from '@Widget/connections/safe-socket';

import css from './index2.scss';

class Index2View extends React.Component {
  // componentDidMount() {
  //  window.widget = new WidgetWrapper({
  //    selector: '#widget-frame',
  //    target: {
  //      appId: 1,
  //      campaignId: 2,
  //      lang: 'en'
  //    },
  //    options: {
  //      // screens: [['rate']]
  //    }
  //  });
  // }

  render() {
    return (
      <div>
        <Header {...this.props} />

        <div id="widget-frame" />

        <Link prefetch href={getPageLink('index')}>
          <a>go to index</a>
        </Link>

        <WidgetRoot
          Api={WidgetApi}
          SafeSocket={SafeSocket}
          options={{open: true}}
          target={{
            appId: 1,
            campaignId: 2,
            lang: 'en'
          }}
        />
      </div>
    );
  }
}

export default withI18next(['home', 'common'])(Index2View);

import React, {useState} from 'react';
import Link from 'next/link';

import HighlightedProducts from '@Components/product/highlighted-products';
import {Grid, Button} from 'semantic-ui-react';
import Logger from '@Common/logger';

import {withI18next} from '../shared/i18n/with-i18n.js';

import Highlights from '@Components/display/highlights';
import Header from '../shared/components/header/header';
import ABSelector from '../shared/components/ab/ab-selector';
import ABVariant from '../shared/components/ab/ab-variant';
import WithPublicContext from '@Components/hocs/with-public-context';
import {withAppConsumer} from '../shared/components/app-context';
import {getPageLink} from '@Common/utils/urls';
import classnames from 'classnames';

import {Passed, EarnIcon} from '../widget/components/display/svg-icons';
import SignupDialog from '@Components/login/signup-confirmation';
import SigninForm from '@Components/login/signin-confirmation';
import OrderStatus from '@Components/cart/order-status';
import Order from '@Components/order/order';
import Footer from '@Components/footer/footer';

import styles from '@Common/styles/styles.scss';
import css from './static-pages.scss';

function IndexView(props) {
  const {t, app, actions, currentOrder, user} = props;

  return (
    <div>
      <Header {...props} />

      <Grid columns={16} className={classnames(styles.MainGrid)}>
        <Grid.Row>
          <Grid.Column width={16} className={css.content}>
            <h1>You may be wondering.</h1>

            <div className={css.questions}>
              <div className={css.item}>
                <div className={css.question}>What condition are phones in?</div>
                <div className={css.answer}>
                  Our products are 100% good-as-new functionally speaking. That said, we assign
                  three product grades for their physical appearance:
                  <p>
                    <b>Excellent:</b> No scratches, exterior looks new. For smartphones and tablets:
                    the screen is intact and without scratches.
                  </p>
                  <p>
                    <b> Good:</b> Light scratches on the exterior, invisible from more than 8 inches
                    away. For smartphones and tablets: the screen is intact and without scratches.
                  </p>
                  <p>
                    <b>Fair:</b> There may be visible scratches/dents on the external shell. For
                    smartphones and tablets: the screen may have small scratches, invisible when it
                    is on.
                  </p>
                  <p>
                    All grades have the same functionality - the only difference is their looks. A
                    lower grade just means that the iPhone will have a little more character - signs
                    of use and/or impact on its backs and sides.
                  </p>
                  <p>
                    Still on the fence? Just remember, if it turns out that you aren't satisfied
                    with the product you receive, you have 30 days to return your device and request
                    a full refund.
                  </p>
                </div>
              </div>
              <div className={css.item}>
                <div className={css.question}>What is your money back guarantee?</div>
                <div className={css.answer}>
                  <p>
                    Consider the first 30 days your trial period with your new iPhone. If you change
                    your mind or are unsatisfied for any reason, you can return your iPhone for a
                    full refund - no questions asked. After that, you continue to have a full year
                    warranty protection against defects.
                  </p>
                </div>
              </div>
              <div className={css.item}>
                <div className={css.question}>What does the one year warranty cover?</div>
                <div className={css.answer}>
                  <p>
                    The 1-year warranty covers any and all product defects (hardware and software).
                    Contact us to share all issues you encounter, and we'll coordinate a repair or
                    replacement free of charge. You're only eligible for a refund if the seller can
                    NOT repair or replace the defective device for you.The limited warranty does NOT
                    cover cracked screens and damages to the device caused by the customer.
                  </p>
                </div>
              </div>

              <div className={css.item}>
                <div className={css.question}>How do payments & trade-ins work?</div>
                <div className={css.answer}>
                  <p>
                    Once you select your phone and proceed to check-out, you'll see an option to
                    send a trade-in phone and can check eligibility for pay-over-time purchase
                    options. You receive cash for your trade-in once the trade-in is received and
                    the condition is confirmed.
                  </p>
                </div>
              </div>
              <div className={css.item}>
                <div className={css.question}>Do all phones work on my network?</div>
                <div className={css.answer}>
                  <p>
                    Hate commitment? Incredible carries phones that work on all four major US
                    networks: Verizon, AT&T, T-Mobile and Sprint. Select an iPhone that works with
                    your specific network, or choose a phone that is fully unlocked and works on any
                    network - no strings attached!
                  </p>
                </div>
              </div>
            </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Footer {...props} />
    </div>
  );
}

IndexView.getInitialProps = async ({req, reduxStore, dispatch, query, res}) => {};

export default withAppConsumer(WithPublicContext(withI18next(['home', 'common'])(IndexView)));

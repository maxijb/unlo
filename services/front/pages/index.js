import React, {useState} from 'react';
import Link from 'next/link';
import Router from 'next/router';
import {Trans} from 'react-i18next';

import classnames from 'classnames';
import useContactBanner from '@Components/product/use-contact-banner';
import HighlightedProducts from '@Components/product/highlighted-products';
import ProductDropdown from '@Components/product/product-dropdown';
import {Grid} from 'semantic-ui-react';
import Logger from '@Common/logger';
import Api from '../shared/actions/api-actions';
import Actions from '../shared/actions/app-actions';
import {withI18next} from '../shared/i18n/with-i18n.js';
import Header from '../shared/components/header/header';
import ABSelector from '../shared/components/ab/ab-selector';
import ABVariant from '../shared/components/ab/ab-variant';
import Dropdown from '@Components/inputs/wrapped-dropdown';
import WithPublicContext from '@Components/hocs/with-public-context';
import {withAppConsumer} from '../shared/components/app-context';
import {getPageLink} from '@Common/utils/urls';
import {MobileBreakpoint, Modals} from '@Common/constants/app';
import {Shield, Ribbon, Truck, LowPrice} from '../widget/components/display/svg-icons';
import ExtendedFooter from '@Components/footer/extended-footer';
import TopProduct from '@Components/product/top-product';
import Highlights from '@Components/display/highlights';
import Image from '@Components/images/image';
import {LocalBusinessJsonLd} from 'next-seo';

import styles from '@Common/styles/styles.scss';
import css from './index.scss';

function IndexView(props) {
  const {
    t,
    app: {
      highlightedProducts,
      menuItems,
      menuItemsMap,
      visibleLeaves,
      config: {protocol, fullDomain},
    },
    actions,
    product,
    app,
    user,
    context,
    isMobile,
    isTablet,
    context: {GA},
  } = props;

  const highlights = [
    {
      name: '12MonthWarrantyIncluded',
      icon: (
        <div>
          <Shield />
        </div>
      ),
      learnMore: true,
      onClick: () => actions.setCurrentModal(Modals.warranty),
    },
    {
      name: 'FreeShippingFreeReturns',
      icon: (
        <div>
          <Truck />
        </div>
      ),
      learnMore: true,
      onClick: () => actions.setCurrentModal(Modals.freeShipping),
    },
    {
      name: 'TestedByExpertsWorksLikeNew',
      icon: (
        <div>
          <Ribbon />
        </div>
      ),
      learnMore: true,
      onClick: () => actions.setCurrentModal(Modals.tested),
    },
    {
      name: 'lowPricesGuaranteed',
      icon: (
        <div>
          <LowPrice />
        </div>
      ),
      learnMore: true,
      onClick: () => actions.setCurrentModal(Modals.lowPrices),
    },
  ];

  useContactBanner(app, user, actions, context);

  return (
    <>
      <LocalBusinessJsonLd
        type="Store"
        id={`${protocol}://${fullDomain}`}
        name="Incredible Phones"
        description="Buy a pre-tested iphones that work like new for up to 70% less. Plus ✅ 12 month Warranty ✅ Free returns ✅ Buy now, pay later."
        url={`${protocol}://${fullDomain}`}
        telephone="+18002424185"
        address={{
          streetAddress: '2820 Howard Cmns #309',
          addressLocality: 'Howard',
          addressRegion: 'WI',
          postalCode: '54313',
          addressCountry: 'US',
        }}
        images={[`${protocol}://${fullDomain}/static/og-image.png`]}
      />
      <Header {...props} isIndex={true} />
      <Grid columns={16} className={styles.FullGrid}>
        <Grid.Row className="fullSize">
          {isMobile && (
            <div className={css.heroVideoMobileFrame}>
              <video className={css.heroVideoMobile} loop playsInline autoPlay muted preload="auto">
                <source type="video/mp4" src="/static/hero-video-mobile.mp4" />
              </video>
            </div>
          )}
          {!isMobile && (
            <div className={css.heroVideoFrame}>
              <div className={css.heroVideoMask}>
                <video className={css.heroVideo} loop playsInline autoPlay muted preload="auto">
                  <source type="video/mp4" src="/static/hero-video-4k.mp4" />
                </video>
              </div>
              <div className={css.heroContent}>
                <div>
                  <div className={css.heroTitle}>{t('homeHero')}</div>
                  <div
                    className={classnames(
                      css.heroSubtext,
                      styles.ColorSubtext,
                      styles.MarginDoubleTop,
                    )}
                  >
                    {t('buyRefurbishedIphone')}
                  </div>
                  <div className={classnames(css.heroDropdownParent, styles.MarginDoubleTop)}>
                    <div className={css.heroDropdown}>
                      <ProductDropdown visibleLeaves={visibleLeaves} />
                    </div>
                    <div className={css.trustpilot}>
                      <a
                        href="https://www.trustpilot.com/review/incrediblephones.com"
                        target="blank"
                        rel="noopener noreferer"
                      >
                        <div>
                          <img src="/static/incredible/trustpilot.png" width="75" alt="truspilot" />
                        </div>
                        <div className={css.trustpilotRating}>
                          <img
                            src="/static/incredible/trustpilot-rating.png"
                            width="100"
                            alt="trustpilot rating"
                          />
                        </div>
                      </a>
                      <div>
                        4.8 out of 5 on{' '}
                        <a
                          href="https://www.trustpilot.com/review/incrediblephones.com"
                          target="blank"
                          rel="noopener noreferer"
                        >
                          {' '}
                          TrustPilot
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
        </Grid.Row>
      </Grid>
      <Grid columns={16} className={classnames(styles.MainGrid)}>
        {isMobile && (
          <Grid.Row>
            <Grid.Column width={16}>
              <div
                className={classnames(
                  styles.centered,
                  styles.Huge,
                  styles.MarginLeft,
                  styles.MarginRight,
                  styles.MarginDoubleTop,
                  styles.MarginDoubleBottom,
                )}
              >
                {t('homeHero')}
              </div>
              <div className={styles.MarginDoubleBottom}>
                <ProductDropdown visibleLeaves={visibleLeaves} />
              </div>
            </Grid.Column>
          </Grid.Row>
        )}
        <Grid.Row>
          <Grid.Column width={16}>
            <div
              className={classnames({
                [styles.MarginDoubleTop]: !isMobile,
              })}
            >
              <HighlightedProducts
                products={highlightedProducts}
                title={t('mostPopularPhones')}
                page="index"
              />
            </div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row
          className={classnames({
            [styles.MarginDoubleTop]: !isMobile,
            narrow: !isMobile,
          })}
        >
          {!isMobile && (
            <Grid.Column mobile={16} computer={8} tablet={8}>
              <TopProduct product={product} actions={actions} />
            </Grid.Column>
          )}
          <Grid.Column mobile={16} computer={8} tablet={8} className={css.highlightsHolder}>
            <Highlights highlights={highlights} />
          </Grid.Column>
        </Grid.Row>
        {isMobile && (
          <Grid.Row>
            <Grid.Column width={16}>
              <ProductDropdown visibleLeaves={visibleLeaves} />
            </Grid.Column>
          </Grid.Row>
        )}
      </Grid>
      <ExtendedFooter {...props} />
    </>
  );
}

IndexView.getInitialProps = async ({req, reduxStore, dispatch, query, res}) => {
  const highlightedProducts = await dispatch(Api.app.getHighlightedProducts({}, req));

  if (highlightedProducts?.resp?.highlightedCategory) {
    const product = await dispatch(
      Api.product.getMinimumDetails({id: highlightedProducts.resp.highlightedCategory}, req),
    );

    await dispatch(
      Api.product.getCompetitorPrices(
        {
          category_id: product.resp.product?.id,
          attr_id: product.resp.defaultVersion?.tags
            ?.filter(t => t.type === 'capacity')
            ?.map(t => t.id),
        },
        req,
      ),
    );
  }
};

export default withAppConsumer(WithPublicContext(withI18next(['home', 'common'])(IndexView)));

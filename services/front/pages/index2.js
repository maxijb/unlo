import React from 'react';
import Link from 'next/link';

import Logger from '@Common/logger';
import {getPageLink} from '@Common/utils/urls';

import Actions from '../shared/actions/app-actions';
import Api from '../shared/actions/api-actions';
import {withI18next} from '../shared/i18n/with-i18n.js';
import Header from '../shared/components/header/header';
import css from './index2.scss';

class Index2View extends React.Component {
  static async getInitialProps({req, dispatch}) {
    await Promise.all([dispatch(Api.v2.getId({id: 48}, req)), dispatch(Actions.incrementCount())]);
  }
  render() {
    return (
      <div>
        <Header {...this.props} />
        <span className={css.big}>Hello World ({this.props.test})</span>
        <div className={css.base}>
          <a
            className={css.big}
            onClick={() => {
              this.props.actions.incrementCount().then(() => {
                console.log('counter', this.props.app.count);
              });
            }}
          >
            INCREMENT
          </a>
          <a
            onClick={() => {
              this.props.actions.v2.decrementCount().then(() => {
                console.log(this.props.app.count);
              });
            }}
          >
            {' '}
            / DECREMENT
          </a>
        </div>
        <pre>{JSON.stringify(this.props.app, undefined, 4)}</pre>
        <Link prefetch href={getPageLink('index')}>
          <a>go to index</a>
        </Link>
        <Link prefetch href={getPageLink('dashboard')}>
          <a>DASHBOARD</a>
        </Link>
      </div>
    );
  }
}

export default withI18next(['home', 'common'])(Index2View);

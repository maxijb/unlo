import React from 'react';
import Link from 'next/link';

import Logger from '@Common/logger';
import {getPageLink} from '@Common/utils/urls';

import Actions from '../shared/actions/app-actions';
import Api from '../shared/actions/api-actions';
import {withI18next} from '../shared/i18n/with-i18n.js';
import Header from '../shared/components/header/header';

import css from './index2.scss';

class Index2View extends React.Component {
  componentDidMount() {
    const {
      app: {
        config: {
          cdn: {cdnDomain}
        }
      }
    } = this.props;
    var sct = document.createElement('script');
    sct.src = `//${cdnDomain}/static/widget/launcher.js`;
    sct.type = 'text/javascript';
    sct.onload = function () {
      window.wiriWidget = new Wiri({
        target: {appId: 1},
        options: {
          triggerVerticalOffset: 200,
          triggerHorizontalOffset: 200,
          mobileTriggerVerticalOffset: () => 50,
          mobileTriggerHorizontalOffset: () => 50
        }
      });
    };
    document.body.appendChild(sct);
  }

  render() {
    return (
      <div>
        <Header {...this.props} />

        <div id="widget-frame" />

        <Link prefetch href={getPageLink('index')}>
          <a>go to index</a>
        </Link>
      </div>
    );
  }
}

export default withI18next(['home', 'common'])(Index2View);

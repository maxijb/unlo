import React, {useState, useEffect} from 'react';
import Link from 'next/link';

import ChatLink from '@Components/display/chat-link';
import HighlightedProducts from '@Components/product/highlighted-products';
import {Grid, Button} from 'semantic-ui-react';
import Logger from '@Common/logger';
import Api from '../shared/actions/api-actions';
import Actions from '../shared/actions/app-actions';
import {withI18next} from '../shared/i18n/with-i18n.js';

import {getUTCDateAsString, addWorkingDays} from '@Common/utils/date-utils';
import Highlights from '@Components/display/highlights';
import Header from '../shared/components/header/header';
import ABSelector from '../shared/components/ab/ab-selector';
import ABVariant from '../shared/components/ab/ab-variant';
import WithPublicContext from '@Components/hocs/with-public-context';
import {withAppConsumer} from '../shared/components/app-context';
import {getPageLink} from '@Common/utils/urls';
import classnames from 'classnames';
import OrderDetails from '@Components/order/order-details';
import {Passed, EarnIcon} from '../widget/components/display/svg-icons';
import SignupDialog from '@Components/login/signup-confirmation';
import SigninForm from '@Components/login/signin-confirmation';
import OrderStatus from '@Components/cart/order-status';
import Order from '@Components/order/order';
import ExtendedFooter from '@Components/footer/extended-footer';
import CartTradein from '@Components/cart/cart-tradein';
import Separator from '@Components/display/separator';
import CrossSelling from '@Components/display/cross-selling';
import styles from '@Common/styles/styles.scss';
import css from './order-confirmation.scss';

function OrderConfirmation(props) {
  const {t, app, actions, currentOrder, user, query, api, isMobile, cart} = props;
  const [isSignin, setIsSignin] = useState(false);
  const [isOrderDetails, setIsOrderDetails] = useState(false);

  useEffect(() => {
    if (query.reset) {
      actions.resetCart();
    }

    if (app.config.isProd && !user.superAdmin) {
      gtag('event', 'conversion', {
        send_to: 'AW-316844746/bw0ZCKzIk-kCEMrVipcB',
        transaction_id: currentOrder?.order?.id,
        value: currentOrder?.order?.total,
        currency: 'USD',
      });
    }
  }, []);

  const orderDetails = (
    <>
      <OrderDetails
        address={currentOrder.address}
        order={currentOrder.order}
        products={currentOrder.products}
      />
      {(isOrderDetails || !isMobile) && currentOrder.tradeInOrder && (
        <Order
          order={currentOrder.tradeInOrder}
          products={currentOrder.tradeInProducts}
          showStatus={isMobile}
        />
      )}
    </>
  );

  return (
    <>
      <div>
        <Header {...props} />
        <Grid columns={16} className={styles.FullGrid}>
          <Grid.Row className="fullSize">
            <div className={css.confirmationHeader}>
              <div className={css.confirmationLegend}>
                <img src="/static/incredible/passed.png" alted="passed" />
                {t('niceWorkDeal')}
              </div>
            </div>
          </Grid.Row>
        </Grid>
        <Grid columns={16} className={classnames(styles.MainGrid, styles.MarginDoubleTop)}>
          <Grid.Row className="narrow">
            <Grid.Column computer={10} mobile={16} tablet={8}>
              <OrderStatus order={currentOrder.products} />
              {!user.id && (
                <>
                  <div
                    className={classnames(
                      styles.Large,
                      styles.MarginDoubleTop,
                      styles.MarginHalfBottom,
                    )}
                  >
                    {t('Create an account')}
                  </div>
                  <div>{t('Track delivery status  and manage this order online.')}</div>
                  <div>
                    {!isSignin ? (
                      <SignupDialog
                        email={currentOrder.order.email}
                        firstName={currentOrder.order.firstName}
                        lastName={currentOrder.order.lastName}
                        submitForm={api.user.signup}
                        closeModal={() => {}}
                        isOpen={true}
                        facebookLogin={api.user.facebookLogin}
                        googleLogin={api.user.googleLogin}
                        openSigninModal={() => setIsSignin(true)}
                        onSuccessLogin={() => {}}
                        mainCTA={t('saveOrderToMyAccount')}
                      />
                    ) : (
                      <SigninForm
                        submitForm={api.user.login}
                        facebookLogin={api.user.facebookLogin}
                        googleLogin={api.user.googleLogin}
                        openSignupModal={() => setIsSignin(false)}
                        sendForgotPassword={api.user.sendForgotPassword}
                        mainCTA={t('saveOrderToMyAccount')}
                      />
                    )}
                  </div>
                </>
              )}
              {user.id && (
                <div
                  className={classnames(styles.MarginTop, styles.MarginBottom, styles.MediumBold)}
                >
                  <span>You can manage your orders at your </span>
                  <Link href="/user/orders">
                    <a>Order Management Center.</a>
                  </Link>
                </div>
              )}
              {isMobile && (
                <Highlights
                  highlights={[
                    {
                      name: t('viewOrderDetails'),
                      icon: <img src="/static/incredible/invoice.png" alt="invoice" />,
                      onClick: () => setIsOrderDetails(true),
                      selected: isOrderDetails,
                    },
                    {
                      name: t('earnCashOldPhone'),
                      icon: <EarnIcon />,
                      onClick: () => {
                        setIsOrderDetails(false);
                        actions.setIsOpenTradeIn(true);
                      },
                    },
                  ]}
                />
              )}

              {!isMobile && <Separator />}
              <CrossSelling />

              {!isMobile && (
                <>
                  <CartTradein
                    cart={cart}
                    actions={actions}
                    standAlone={true}
                    location="confirmation"
                  />
                </>
              )}
              <div className={styles.MarginDoubleTop}>
                <ChatLink />
              </div>
            </Grid.Column>
            {(!isMobile || isOrderDetails) && (
              <Grid.Column computer={6} tablet={8} mobile={16}>
                <div className={classnames({[styles.withBorder]: !isMobile})}>{orderDetails}</div>
              </Grid.Column>
            )}
          </Grid.Row>
        </Grid>
        <ExtendedFooter {...props} questionsOnTop={true} />
      </div>
    </>
  );
}

OrderConfirmation.getInitialProps = async ({req, reduxStore, dispatch, query, res}) => {
  const currentOrder = await dispatch(Api.cart.isOrderMine({id: query.id}, req));

  if (!currentOrder?.resp?.order) {
    res.redirect('/404');
  }
  return {currentOrder: currentOrder.resp, query};
};

export default withAppConsumer(
  WithPublicContext(withI18next(['home', 'common'])(OrderConfirmation)),
);

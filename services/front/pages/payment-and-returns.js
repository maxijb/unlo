import React, {useState} from 'react';
import Link from 'next/link';

import HighlightedProducts from '@Components/product/highlighted-products';
import {Grid, Button} from 'semantic-ui-react';
import Logger from '@Common/logger';
import Api from '../shared/actions/api-actions';
import Actions from '../shared/actions/app-actions';
import {withI18next} from '../shared/i18n/with-i18n.js';

import Highlights from '@Components/display/highlights';
import Header from '../shared/components/header/header';
import ABSelector from '../shared/components/ab/ab-selector';
import ABVariant from '../shared/components/ab/ab-variant';
import WithPublicContext from '@Components/hocs/with-public-context';
import {withAppConsumer} from '../shared/components/app-context';
import {getPageLink} from '@Common/utils/urls';
import classnames from 'classnames';
import OrderDetails from '@Components/order/order-details';
import {Passed, EarnIcon} from '../widget/components/display/svg-icons';
import SignupDialog from '@Components/login/signup-confirmation';
import SigninForm from '@Components/login/signin-confirmation';
import OrderStatus from '@Components/cart/order-status';
import Order from '@Components/order/order';
import Footer from '@Components/footer/footer';
import Separator from '@Components/display/separator';

import styles from '@Common/styles/styles.scss';
import css from './static-pages.scss';

function IndexView(props) {
  const {t, app, actions, currentOrder, user, query, api} = props;
  const [isSignin, setIsSignin] = useState(false);
  const [isOrderDetails, setIsOrderDetails] = useState(false);
  return (
    <div>
      <Header {...props} />

      <Grid columns={16} className={classnames(styles.MainGrid)}>
        <Grid.Row>
          <Grid.Column width={16} className={css.content}>
            <h1>What payment methods are accepted on Incredible?</h1>

            <p>
              Incredible accepts Paypal, Visa, MasterCard, Discover, and American Express on all
              orders. We also accept Affirm and QuadPay for eligible customers who would like to pay
              in installments.
            </p>
            <p>
              <strong>
                Incredible does not accept payments via bank transfer, check, or cash.
              </strong>
            </p>
            <p>
              Please note: In all cases, your order will only become active at the moment Incredible
              receives your payment. At this time, an order confirmation email will be sent to you
              which contains your order number, which should be kept.
            </p>
            <Separator />
            <h2 class="h5 spacerBottom">
              <strong>(Last Revised: June 30, 2020)</strong>
            </h2>

            <h4>EFFECTIVE DATE: January 1, 2020</h4>
            <h1>
              <strong>RETURN POLICY</strong>
            </h1>
            <p>
              Customers can return any product for a full refund within 30 days of delivery.
              Delivery date is based on the date that the product is marked as “delivered” by the
              carrier. Return postage is paid for by Incredible. Returns are shipped to: Incredible,
              2820 Howard Cmns #309, Howard, WI 54313.
            </p>
            <p>
              Customers must use the pre-paid return shipping label that is provided by Incredible.
              Customers cannot return a product after the 30 day return window.&nbsp;
            </p>
            <p>For questions, contact us any time:</p>
            <p>
              2820 Howard Cmns #309 Howard, WI 54313 (888) 675-2905 support@incrediblephones.com
            </p>

            <h1>
              <strong>REFUND POLICY</strong>
            </h1>
            <p>
              Refunds are issued for the full charged amount and applied to the customer’s original
              payment method within 3 business days of receiving a returned product. Once the refund
              is applied, customers should wait 3-5 business days for the refunded amount to show on
              their card account. Customers may contact the card-issuing bank to learn their bank’s
              specific refund timelines. More information on refund billing timelines can be found{' '}
              <a href="https://support.stripe.com/questions/customer-refund-processing-time">
                here
              </a>
              .&nbsp;{' '}
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                Refunds are not issued in the form of store credit.{' '}
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                A partial refund may be issued if the product is returned in a condition that is
                different than the originally-delivered condition. Our support team will contact
                customers prior to making their partial-refund assessment.&nbsp;
              </span>
            </p>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Footer {...props} />
    </div>
  );
}

IndexView.getInitialProps = async ({req, reduxStore, dispatch, query, res}) => {};

export default withAppConsumer(WithPublicContext(withI18next(['home', 'common'])(IndexView)));

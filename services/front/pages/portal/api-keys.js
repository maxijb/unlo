import React, {useMemo, useState} from 'react';
import Link from 'next/link';
import {Grid, Button} from 'semantic-ui-react';
import classnames from 'classnames';
import Router from 'next/router';
import Logger from '@Common/logger';
import {DefaultPerPage, SellerConnectionStatus} from '@Common/constants/app';
import {getPageLink, getBrightpearlOathURL} from '@Common/utils/urls';
import isEmpty from 'is-empty';

import Api from '../../shared/actions/api-actions';
import Actions from '../../shared/actions/app-actions';
import {withI18next} from '../../shared/i18n/with-i18n.js';
import FormValidations from '@Common/constants/form-validations';
import {ValidateForm} from '@Common/utils/validations';

import DashboardLayout from '../../shared/components/layout/dashboard-layout';
import WithActiveConfig from '@Components/hocs/with-active-config';
import WithDashboardContext from '../../shared/components/hocs/with-dashboard-context';

import ErrorRenderer from '@Components/inputs/error-renderer';
import Textarea from '@Components/inputs/wrapped-textarea';
import Dropdown from '@Components/inputs/wrapped-dropdown';
import Input from '@Components/inputs/wrapped-input';
import AddressInput from '@Components/inputs/address-input';
import AddButton from '@Components/inputs/add-button';

import CrudSection from '@Components/crud/crud-section';

import styles from '@Common/styles/styles.scss';
import css from '../dashboard.scss';
import crudCss from '@Components/crud/crud.scss';

function Dashboard(props) {
  const {t, crud, user, api, actions, query, seller, errors: serverErrors = null} = props;

  const {webhooks, api_token} = seller;

  const [hooks, setHooks] = useState(
    webhooks.reduce((agg, h) => {
      agg[h.type] = h.uri;
      return agg;
    }, {}),
  );

  console.log(webhooks, hooks);
  const [errors, setErrors] = useState(serverErrors);
  const onChange = (value, name) => {
    hooks[name] = value;
    setHooks({...hooks});
  };

  const onUpdate = async () => {
    setLoading('hooks');
    await api.crud.setHooks({hooks});
    setLoading('false');
  };
  const [loading, setLoading] = useState(false);

  return (
    <DashboardLayout {...props} mainTitle={t('dashboard')}>
      <Grid columns={16} padded>
        <Grid.Column computer={16} className={css.mainColumn} mobile={16} tablet={16}>
          <>
            <div className={styles.FlexSpaceBetween}>
              <h1>API Access</h1>
            </div>

            <CrudSection
              title="Token"
              subtitle="Use this token to sign all request to Incredible's API."
            >
              <Grid columns={16}>
                <Grid.Row>
                  <Grid.Column width={4}>
                    <h5>Keep it safe</h5>
                    <div>
                      When an order is created or updated, we'll call these endpoints on your server
                    </div>
                  </Grid.Column>
                  <Grid.Column width={12}>
                    <Grid columns={16}>
                      <Grid.Row>
                        <Grid.Column width={8}>
                          <Input
                            name="name"
                            label="API Token"
                            onChange={onChange}
                            value={api_token}
                            disabled={true}
                            fluid
                          />
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </CrudSection>

            <CrudSection
              title="Webhooks"
              subtitle="Set which URLs on your servers will be called upon certain events."
            >
              <Grid columns={16}>
                <Grid.Row>
                  <Grid.Column width={4}>
                    <h5>Order Events</h5>
                    <div>
                      When an order is created or updated, we'll call these endpoints on your
                      server. All endpoint must expect a POST request. Please enter the absolute URL
                      including http(s)://
                    </div>
                  </Grid.Column>
                  <Grid.Column width={12}>
                    <Grid columns={16}>
                      <Grid.Row>
                        <Grid.Column width={8}>
                          <Input
                            name="on_order_create"
                            label="On Order create"
                            placeholder={'URL to call whenever a new order is created'}
                            onChange={onChange}
                            value={hooks.on_order_create}
                            fluid
                          />
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={8}>
                          <Input
                            name="on_order_update"
                            label={'On order update'}
                            placeholder={'URL to call whenever a new order is updated'}
                            onChange={onChange}
                            value={hooks.on_order_update}
                            fluid
                          />
                        </Grid.Column>
                      </Grid.Row>

                      <Grid.Row>
                        <Grid.Column width={8} textAlign="right">
                          <Button
                            primary
                            onClick={() => onUpdate('hooks')}
                            loading={loading === 'hooks'}
                          >
                            Save
                          </Button>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </CrudSection>
          </>
        </Grid.Column>
      </Grid>
    </DashboardLayout>
  );
}

Dashboard.getInitialProps = async ({req, reduxStore, dispatch, query}) => {
  const seller = await dispatch(Api.crud.getSeller({}, req));
  return {seller: seller.resp};
};

export default WithDashboardContext(
  WithActiveConfig(withI18next(['dashboard', 'common'])(Dashboard), true),
);

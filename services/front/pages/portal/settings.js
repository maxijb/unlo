import React, {useMemo, useState} from 'react';
import Link from 'next/link';
import {Grid, Button} from 'semantic-ui-react';
import classnames from 'classnames';
import Router from 'next/router';
import Logger from '@Common/logger';
import {DefaultPerPage, SellerConnectionStatus} from '@Common/constants/app';
import {getPageLink, getBrightpearlOathURL} from '@Common/utils/urls';
import isEmpty from 'is-empty';

import {cdnURL} from '@Common/utils/statif-assets-utils';
import Api from '../../shared/actions/api-actions';
import Actions from '../../shared/actions/app-actions';
import {withI18next} from '../../shared/i18n/with-i18n.js';
import FormValidations from '@Common/constants/form-validations';
import {ValidateForm} from '@Common/utils/validations';

import DashboardLayout from '../../shared/components/layout/dashboard-layout';
import WithActiveConfig from '@Components/hocs/with-active-config';
import WithDashboardContext from '../../shared/components/hocs/with-dashboard-context';

import ErrorRenderer from '@Components/inputs/error-renderer';
import Textarea from '@Components/inputs/wrapped-textarea';
import Dropdown from '@Components/inputs/wrapped-dropdown';
import Input from '@Components/inputs/wrapped-input';
import AddressInput from '@Components/inputs/address-input';
import AddButton from '@Components/inputs/add-button';

import CrudSection from '@Components/crud/crud-section';

import styles from '@Common/styles/styles.scss';
import css from '../dashboard.scss';
import crudCss from '@Components/crud/crud.scss';

const Fields = {
  business: ['description', 'address', 'address_id', 'email', 'name', 'phone'],
  payout: ['bank_name', 'bank_address', 'routing_number', 'account_number'],
  shippingCommon: ['cut_off_time', 'cut_off_timezone'],
  shippingPolicy: [
    'carrier',
    'speed_days',
    'name',
    'price',
    'id',
    'remote_id',
    'remote_name',
    'remote_source',
  ],
  accessories: ['category_id', 'price', 'id', 'source_id'],
};

function Dashboard(props) {
  const {
    t,
    crud,
    user,
    api,
    actions,
    query,
    seller,
    errors: serverErrors = null,
    bpShippingMethods,
  } = props;

  const {connections, ...other} = seller;

  const [data, setData] = useState({
    ...other,
    textAddress: seller?.address?.textName || '',
    brightpearl_name: connections?.brightpearl?.account_name || '',
    sellercloud_name: connections?.sellercloud?.account_name || '',
    sellercloud_api_domain: connections?.sellercloud?.api_domain || '',
    sellercloud_password: connections?.sellercloud?.token || '',
    shipstation_key: connections?.shipstation?.account_name || '',
    shipstation_secret: connections?.shipstation?.token || '',
    finale_server: connections?.finale?.account_name || '',
    finale_user: connections?.finale?.token || '',
    finale_password: connections?.finale?.refetch_token || '',
    finale_ftp_user: connections?.ftpfinale?.account_name || '',
    finale_ftp_password: connections?.ftpfinale?.token || '',
  });
  const [errors, setErrors] = useState(serverErrors);
  const onChange = (value, name) => {
    data[name] = value;
    setData({...data});
  };
  const [loading, setLoading] = useState(false);
  const onChangeArray = (value, name, index, field) => {
    data[name][index][field] = value;
    onChange([...data[name]], name);
  };

  const [selectedSeller, setSelectedSeller] = useState(query.seller_id || null);
  const sellerOptions = useMemo(() => {
    return Object.keys(crud.sellers).map(key => ({
      key,
      value: key,
      text: `${key} - ${crud.sellers[key].name}`,
    }));
  }, crud.sellers);

  // if we are forcing an ID, then we disable saving
  const saveDisabled = !!query.seller_id;
  console.log(saveDisabled);
  const onUpdate = async (
    form,
    source = data,
    errorPrefix = '',
    inputWrapper = null,
    editKey = null,
    editIndex = null,
  ) => {
    if (saveDisabled) {
      return;
    }
    setLoading(errorPrefix || form);
    let input = {};
    Fields[form].forEach(k => {
      input[k] = source[k];
    });

    const errs = FormValidations.hasOwnProperty(`${form}SellerUpdate`)
      ? ValidateForm(input, FormValidations[`${form}SellerUpdate`])
      : null;
    errorPrefix ? setErrors({[errorPrefix]: errs}) : setErrors(errs);
    if (isEmpty(errs)) {
      if (inputWrapper) {
        input = {[inputWrapper]: input};
      }
      const response = await api.crud.updateSeller({input});
      if (response.resp.ok && editKey && editIndex !== null) {
        const {ok, ...newData} = response.resp;
        data[editKey][editIndex] = {
          ...data[editKey][editIndex],
          ...newData,
        };
        onChange([...data[editKey]], editKey);
      }
    }
    setLoading(false);
  };

  return (
    <DashboardLayout {...props} mainTitle={t('dashboard')}>
      <Grid columns={16} padded>
        <Grid.Column computer={16} className={css.mainColumn} mobile={16} tablet={16}>
          <>
            <div className={styles.FlexSpaceBetween}>
              <h1>Settings</h1>
              {user.superAdmin && (
                <div className={classnames(styles.NoWrap, styles.FlexNoShrink, styles.FlexCenter)}>
                  <span
                    className={classnames(
                      styles.MarginHalfRight,
                      styles.Small,
                      styles.NoWrap,
                      styles.Capitalize,
                    )}
                  >
                    {t('common:seller')}
                  </span>

                  <Dropdown
                    placeholder="Select Seller"
                    size="mini"
                    value={selectedSeller || ''}
                    options={sellerOptions}
                    onChange={val => {
                      window.location = `?seller_id=${val}`;
                      setSelectedSeller(val);
                    }}
                  />
                </div>
              )}
            </div>

            <CrudSection title="Business" subtitle="Details about your company">
              <Grid columns={16}>
                <Grid.Row>
                  <Grid.Column width={4}>
                    <h5>Contact Information</h5>
                  </Grid.Column>
                  <Grid.Column width={12}>
                    <Grid columns={16}>
                      <Grid.Row>
                        <Grid.Column width={8}>
                          <Input
                            name="name"
                            placeholder="Company name"
                            onChange={onChange}
                            value={data.name}
                            error={errors?.name}
                            fluid
                          />
                        </Grid.Column>
                        <Grid.Column width={8}>
                          <Input
                            name="phone"
                            placeholder="Phone number"
                            onChange={onChange}
                            value={data.phone}
                            error={errors?.phone}
                            fluid
                          />
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={16}>
                          <AddressInput
                            name="address"
                            placeholder="Address"
                            initialValue={data.textAddress}
                            onChange={address => {
                              onChange(address, 'address');
                            }}
                            error={errors?.address}
                            fluid
                          />
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={8}>
                          <Input
                            name="unit"
                            placeholder="Apt/Unit"
                            onChange={unit => onChange({...(data.address || {}), unit}, 'address')}
                            value={data.address?.unit || ''}
                            fluid
                          />
                        </Grid.Column>
                        <Grid.Column width={8}>
                          <Input
                            name="email"
                            placeholder="Email to receive updates"
                            onChange={onChange}
                            value={data.email}
                            error={errors?.email}
                            fluid
                          />
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={16}>
                          <Textarea
                            name="description"
                            placeholder="Business bio (to be shown to customers)"
                            value={data.description}
                            onChange={onChange}
                            fluid
                          />
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={16} textAlign="right">
                          <Button
                            disabled={saveDisabled}
                            primary
                            onClick={() => onUpdate('business')}
                            loading={loading === 'business'}
                          >
                            Save
                          </Button>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </CrudSection>

            <CrudSection
              title="Shipping"
              subtitle="Create and modify your shipping policy settings in real time."
            >
              <Grid columns={16}>
                <Grid.Row>
                  <Grid.Column width={4}>
                    <h5>Cut-off time</h5>
                    <div>
                      What time must orders be placed in order to ship same-day? If you do not ship
                      same day, leave blank.
                    </div>
                  </Grid.Column>
                  <Grid.Column width={12}>
                    <Grid columns={16}>
                      <Grid.Row>
                        <Grid.Column width={12}>
                          <div className={styles.FlexCenter}>
                            <Dropdown
                              name="cut_off_time"
                              options={[
                                {value: 0, text: '0:00'},
                                {value: 1, text: '1:00'},
                                {value: 2, text: '2:00'},
                                {value: 3, text: '3:00'},
                                {value: 4, text: '4:00'},
                                {value: 5, text: '5:00'},
                                {value: 6, text: '6:00'},
                                {value: 7, text: '7:00'},
                                {value: 8, text: '8:00'},
                                {value: 9, text: '9:00'},
                                {value: 10, text: '10:00'},
                                {value: 11, text: '11:00'},
                                {value: 12, text: '12:00'},
                                {value: 13, text: '13:00'},
                                {value: 14, text: '14:00'},
                                {value: 15, text: '15:00'},
                                {value: 16, text: '16:00'},
                                {value: 17, text: '17:00'},
                                {value: 18, text: '18:00'},
                                {value: 19, text: '19:00'},
                                {value: 20, text: '20:00'},
                                {value: 21, text: '21:00'},
                                {value: 22, text: '22:00'},
                                {value: 23, text: '23:00'},
                              ]}
                              placeholder="Cut off time"
                              onChange={onChange}
                              value={data.cut_off_time}
                              error={errors?.cut_off_time}
                              fluid
                            />
                            <Dropdown
                              name="cut_off_timezone"
                              options={[
                                'America/Adak',
                                'America/Anchorage',
                                'America/Boise',
                                'America/Chicago',
                                'America/Denver',
                                'America/Detroit',
                                'America/Indiana/Indianapolis',
                                'America/Indiana/Knox',
                                'America/Indiana/Marengo',
                                'America/Indiana/Petersburg',
                                'America/Indiana/Tell_City',
                                'America/Indiana/Vevay',
                                'America/Indiana/Vincennes',
                                'America/Indiana/Winamac',
                                'America/Juneau',
                                'America/Kentucky/Louisville',
                                'America/Kentucky/Monticello',
                                'America/Los_Angeles',
                                'America/Menominee',
                                'America/Metlakatla',
                                'America/New_York',
                                'America/Nome',
                                'America/North_Dakota/Beulah',
                                'America/North_Dakota/Center',
                                'America/North_Dakota/New_Salem',
                                'America/Phoenix',
                                'America/Sitka',
                                'America/Yakutat',
                                'Pacific/Honolulu',
                              ]}
                              placeholder="Timezone"
                              error={errors?.cut_off_timezone}
                              onChange={onChange}
                              value={data.cut_off_timezone}
                              className={styles.MarginLeft}
                              fluid
                            />
                          </div>
                        </Grid.Column>
                        <Grid.Column width={4}>
                          <Button
                            disabled={saveDisabled}
                            loading={loading === 'shippingCommon'}
                            primary
                            onClick={() => onUpdate('shippingCommon')}
                          >
                            Save
                          </Button>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row
                  className={styles.MarginTop}
                  style={{borderTop: '1px solid #ccc', paddingTop: 32}}
                >
                  <Grid.Column width={4}>
                    <h5>Shipping Policies</h5>
                    <div>
                      What time must orders be placed in order to ship same-day? If you do not ship
                      same day, leave blank.
                    </div>
                    <AddButton
                      text="Add Shipping Method"
                      onClick={() => {
                        const policies = [
                          ...(data.shippingPolicies || []),
                          {carrier: 'USPS', speed_days: 5, name: '', price: 0},
                        ];
                        onChange(policies, 'shippingPolicies');
                      }}
                      className={styles.MarginTop}
                    />
                  </Grid.Column>
                  <Grid.Column width={12}>
                    <Grid columns={2}>
                      <Grid.Row>
                        {(data.shippingPolicies || []).map((s, i) => {
                          if (!s) {
                            return null;
                          }

                          return (
                            <Grid.Column key={s.id || i}>
                              <div
                                className={classnames(css.line, crudCss.card, styles.MarginBottom)}
                              >
                                <Dropdown
                                  value={s.carrier}
                                  options={[
                                    {value: 'USPS', text: 'USPS'},
                                    {value: 'UPS', text: 'UPS'},
                                    {value: 'FEDEX', text: 'FedEx'},
                                  ]}
                                  placeholder="Carrier"
                                  name="carrier"
                                  fluid
                                  onChange={(value, name) => {
                                    onChangeArray(value, 'shippingPolicies', i, name);
                                  }}
                                  className={styles.MarginBottom}
                                />
                                <Dropdown
                                  value={s.speed_days}
                                  options={[
                                    {value: 0, text: 'Same day'},
                                    {value: 1, text: 'Next working day'},
                                    {value: 2, text: 'Up to 2 business days'},
                                    {value: 3, text: 'Up to 3 business days'},
                                    {value: 4, text: 'Up to 4 business days'},
                                    {value: 5, text: 'Up to 5 business days'},
                                    {value: 6, text: 'Up to 6 business days'},
                                    {value: 7, text: 'Up to 7 business days'},
                                    {value: 8, text: 'Up to 8 business days'},
                                    {value: 9, text: 'Up to 9 business days'},
                                    {value: 10, text: 'Up to 10 business days'},
                                  ]}
                                  placeholder="Shipping days"
                                  name="speed_days"
                                  fluid
                                  error={errors?.[`shipping[${i}]`]?.carrier}
                                  onChange={(value, name) => {
                                    onChangeArray(value, 'shippingPolicies', i, name);
                                  }}
                                  className={styles.MarginBottom}
                                />

                                <Input
                                  value={s.name}
                                  placeholder="Enter title"
                                  name="name"
                                  fluid
                                  error={errors?.[`shipping[${i}]`]?.name}
                                  onChange={(value, name) => {
                                    onChangeArray(value, 'shippingPolicies', i, name);
                                  }}
                                  className={styles.MarginBottom}
                                />

                                <Input
                                  value={s.price}
                                  placeholder="Enter Price"
                                  fluid
                                  name="price"
                                  error={errors?.[`shipping[${i}]`]?.price}
                                  onChange={(value, name) => {
                                    onChangeArray(value, 'shippingPolicies', i, name);
                                  }}
                                  className={styles.MarginBottom}
                                />

                                {bpShippingMethods?.length > 0 && (
                                  <Dropdown
                                    options={[
                                      {value: '', text: "Don't sync to any Brightpearl method"},
                                      ...bpShippingMethods.map(m => ({
                                        value: String(m.id),
                                        text: m.name,
                                      })),
                                    ]}
                                    placeholder="Select a Brightpearl method"
                                    fluid
                                    name="remote_id"
                                    value={s.remote_id || ''}
                                    onChange={(value, name) => {
                                      const foundMethod = bpShippingMethods.find(
                                        bpm => bpm.id === value,
                                      );
                                      data.shippingPolicies[i].remote_id = value;
                                      data.shippingPolicies[i].remote_name =
                                        foundMethod?.name || '';
                                      data.shippingPolicies[i].remote_source = 'brightpearl';

                                      onChange([...data['shippingPolicies']], 'shippingPolicies');
                                    }}
                                    className={styles.MarginBottom}
                                  />
                                )}

                                <Button
                                  disabled={saveDisabled}
                                  className={classnames(styles.MarginRight, styles.MarginBottom)}
                                  loading={loading === `deleteShippingMethod${i}`}
                                  onClick={async () => {
                                    setLoading(`deleteShippingMethod${i}`);
                                    if (s.id) {
                                      await api.crud.updateSeller({
                                        input: {
                                          deleteShippingMethod: s.id,
                                        },
                                      });
                                    }
                                    data.shippingPolicies.splice(i, 1);
                                    onChange([...data.shippingPolicies], 'shippingPolicies');
                                    setLoading(false);
                                  }}
                                >
                                  Delete
                                </Button>
                                <Button
                                  disabled={saveDisabled}
                                  primary
                                  loading={loading === `shipping[${i}]`}
                                  onClick={() => {
                                    onUpdate(
                                      'shippingPolicy',
                                      data.shippingPolicies[i],
                                      `shipping[${i}]`,
                                      'shippingMethod',
                                      'shippingPolicies',
                                      i,
                                    );
                                  }}
                                >
                                  Save
                                </Button>
                              </div>
                            </Grid.Column>
                          );
                        })}
                      </Grid.Row>
                    </Grid>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </CrudSection>

            <CrudSection title="Pricing" subtitle="Set prices for accessory items">
              <Grid columns={16}>
                <Grid.Row>
                  <Grid.Column width={4}>
                    <h5>Unbundling</h5>
                    <div>
                      Phones are sold without charger and cable by default, so please specify prices
                      for the following accessories.
                    </div>
                    <AddButton
                      text="Add Accessory"
                      onClick={() => {
                        const accessories = [
                          ...(data.accessories || []),
                          {category_id: null, price: null, id: null},
                        ];
                        onChange(accessories, 'accessories');
                      }}
                      className={styles.MarginTop}
                    />
                  </Grid.Column>
                  <Grid.Column width={12}>
                    <Grid columns={2}>
                      <Grid.Row>
                        {(data.accessories || []).map((s, i) => {
                          if (!s) {
                            return null;
                          }
                          return (
                            <Grid.Column key={s.id || i}>
                              <div
                                className={classnames(css.line, crudCss.card, styles.MarginBottom)}
                              >
                                <Dropdown
                                  value={s.category_id}
                                  options={data.accessoryCategories.map(acc => ({
                                    text: acc.name,
                                    value: acc.id,
                                  }))}
                                  placeholder="Accessory type"
                                  name="category_id"
                                  fluid
                                  onChange={(value, name) => {
                                    onChangeArray(value, 'accessories', i, name);
                                  }}
                                  className={styles.MarginBottom}
                                />

                                <Input
                                  value={s.price}
                                  placeholder="Enter Price"
                                  fluid
                                  name="price"
                                  error={errors?.[`accessories[${i}]`]?.price}
                                  onChange={(value, name) => {
                                    onChangeArray(value, 'accessories', i, name);
                                  }}
                                  className={styles.MarginBottom}
                                />

                                <Input
                                  value={s.source_id}
                                  placeholder="Enter SKU"
                                  fluid
                                  name="source_id"
                                  error={errors?.[`accessories[${i}]`]?.source_id}
                                  onChange={(value, name) => {
                                    onChangeArray(value, 'accessories', i, name);
                                  }}
                                  className={styles.MarginBottom}
                                />

                                <Button
                                  disabled={saveDisabled}
                                  className={classnames(styles.MarginRight, styles.MarginBottom)}
                                  loading={loading === `deleteAccessories${i}`}
                                  onClick={async () => {
                                    setLoading(`deleteAccessories${i}`);
                                    if (s.id) {
                                      await api.crud.updateSeller({
                                        input: {
                                          deleteAccessories: s.id,
                                        },
                                      });
                                    }
                                    data.accessories.splice(i, 1);
                                    onChange([...data.accessories], 'accessories');
                                    setLoading(false);
                                  }}
                                >
                                  Delete
                                </Button>
                                <Button
                                  disabled={saveDisabled}
                                  primary
                                  loading={loading === `accessories[${i}]`}
                                  onClick={() => {
                                    onUpdate(
                                      'accessories',
                                      data.accessories[i],
                                      `accessories[${i}]`,
                                      'accessories',
                                      'accessories',
                                      i,
                                    );
                                  }}
                                >
                                  Save
                                </Button>
                              </div>
                            </Grid.Column>
                          );
                        })}
                      </Grid.Row>
                    </Grid>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </CrudSection>
            <CrudSection title="Payout information">
              <Grid columns={16}>
                <Grid.Row>
                  <Grid.Column width={4}>
                    <h5>Bank wire information</h5>
                    <div>
                      We issue a payment to your bank on the first and 3rd Monday of every month.
                    </div>
                  </Grid.Column>
                  <Grid.Column width={12}>
                    <Grid columns={16}>
                      <Grid.Row>
                        <Grid.Column width={8}>
                          <Input
                            name="bank_name"
                            placeholder="Bank name"
                            onChange={onChange}
                            value={data.bank_name}
                            fluid
                          />
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={8}>
                          <Input
                            name="bank_address"
                            placeholder="Bank address"
                            onChange={onChange}
                            value={data.bank_address}
                            fluid
                          />
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={8}>
                          <Input
                            name="routing_number"
                            placeholder="Routing Number"
                            onChange={onChange}
                            value={data.routing_number}
                            fluid
                          />
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={8}>
                          <Input
                            name="account_number"
                            placeholder="Account Number"
                            onChange={onChange}
                            value={data.account_number}
                            fluid
                          />
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={8} textAlign="right">
                          <Button
                            disabled={saveDisabled}
                            primary
                            onClick={() => onUpdate('payout')}
                            loading={loading === 'payout'}
                          >
                            Save
                          </Button>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </CrudSection>
            <CrudSection
              title={
                <img
                  alt="brightpearl"
                  src={cdnURL('/static/partners/brightpearl.svg')}
                  style={{
                    height: 30,
                    marginBottom: -13,
                  }}
                />
              }
            >
              <Grid columns={16}>
                <Grid.Row>
                  <Grid.Column width={4}>
                    <h5>Connect your account</h5>
                    <div>
                      Enter your Brightpearl account name and hit "Connect". We'll make sure to sync
                      your inventory and orders with that platform.
                    </div>
                  </Grid.Column>
                  <Grid.Column width={12}>
                    <Grid columns={16}>
                      <Grid.Row>
                        <Grid.Column width={8}>
                          <Input
                            name="brightpearl_name"
                            placeholder="Brightpearl account name"
                            onChange={onChange}
                            value={data.brightpearl_name}
                            fluid
                          />
                          <ErrorRenderer error={errors?.brightpearl} />
                        </Grid.Column>
                        <Grid.Column width={8}>
                          <div className={styles.FlexCenter}>
                            {connections?.brightpearl?.status !== 1 ? (
                              <Button
                                disabled={saveDisabled}
                                primary
                                loading={loading === 'brightpearl'}
                                onClick={() => {
                                  setLoading('brightpearl');
                                  api.brightpearl
                                    .createConnection({
                                      account_name: data.brightpearl_name,
                                    })
                                    .then(response => {
                                      if (response?.resp?.id) {
                                        window.location.href = getBrightpearlOathURL({
                                          account_name: data.brightpearl_name,
                                          connection_id: response?.resp?.id,
                                        });
                                      } else {
                                        setErrors({brightpearl: 'Connection error'});
                                      }
                                      setLoading(false);
                                    });
                                }}
                              >
                                Connect
                              </Button>
                            ) : (
                              <Button
                                disabled={saveDisabled}
                                primary
                                onClick={() => {
                                  api.brightpearl.syncInventory().then(response => {
                                    if (response.resp.ok) {
                                      alert(
                                        'Your inventory is being updated. This operation might take a few minutes.',
                                      );
                                    }
                                  });
                                }}
                              >
                                Sync Inventory
                              </Button>
                            )}
                            {connections?.brightpearl && (
                              <div className={styles.MarginLeft}>
                                Status:{' '}
                                <b>
                                  {connections?.brightpearl?.status ===
                                  SellerConnectionStatus.active
                                    ? 'Active'
                                    : 'Disabled'}
                                </b>
                              </div>
                            )}
                          </div>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </CrudSection>

            <CrudSection
              title={
                <img
                  src={cdnURL('/static/partners/sellercloud.svg')}
                  style={{
                    height: 30,
                    marginBottom: -5,
                  }}
                />
              }
            >
              <Grid columns={16}>
                <Grid.Row>
                  <Grid.Column width={4}>
                    <h5>Connect your account</h5>
                    <div>Enter the SellerCloud username and pasword to connect to the API.</div>
                  </Grid.Column>
                  <Grid.Column width={12}>
                    <Grid columns={16}>
                      <Grid.Row>
                        <Grid.Column width={8}>
                          <Input
                            name="sellercloud_name"
                            placeholder="SellerCloud account name"
                            onChange={onChange}
                            value={data.sellercloud_name}
                            fluid
                            className={styles.MarginBottom}
                          />
                          <Input
                            name="sellercloud_password"
                            placeholder="SellerCloud password"
                            className={styles.MarginBottom}
                            onChange={onChange}
                            value={data.sellercloud_password}
                            fluid
                            type="password"
                          />
                          <Input
                            name="sellercloud_api_domain"
                            placeholder="SellerCloud API domain"
                            onChange={onChange}
                            value={data.sellercloud_api_domain}
                            fluid
                          />
                          <ErrorRenderer error={errors?.sellercloud} />
                        </Grid.Column>
                        <Grid.Column width={8}>
                          <div>
                            <Button
                              disabled={saveDisabled}
                              primary
                              className={styles.MarginBottom}
                              loading={loading === 'sellercloud'}
                              onClick={() => {
                                setLoading('sellercloud');
                                setErrors({sellercloud: null});
                                api.sellercloud
                                  .createConnection({
                                    account_name: data.sellercloud_name,
                                    token: data.sellercloud_password,
                                    api_domain: data.sellercloud_api_domain,
                                  })
                                  .then(response => {
                                    if (response?.resp?.ok) {
                                      alert('Connection successful!');
                                    } else {
                                      setErrors({sellercloud: 'Connection error'});
                                    }
                                    setLoading(false);
                                  });
                              }}
                            >
                              Save and Test Connection
                            </Button>
                          </div>
                          {connections?.sellercloud?.status === SellerConnectionStatus.active && (
                            <>
                              <div>
                                <Button
                                  disabled={saveDisabled}
                                  className={styles.MarginBottom}
                                  primary
                                  onClick={() => {
                                    api.sellercloud.syncInventory().then(response => {
                                      if (response?.resp?.ok) {
                                        alert('Connection successful!');
                                      } else {
                                        setErrors({sellercloud: 'Connection error'});
                                      }
                                    });
                                  }}
                                >
                                  Sync inventory
                                </Button>
                              </div>
                              <div>
                                <Button
                                  disabled={saveDisabled}
                                  className={styles.MarginBottom}
                                  primary
                                  onClick={() => {
                                    api.sellercloud.syncOrders().then(response => {
                                      if (response?.resp?.ok) {
                                        alert('Connection successful!');
                                      } else {
                                        setErrors({sellercloud: 'Connection error'});
                                      }
                                    });
                                  }}
                                >
                                  Sync Orders
                                </Button>
                              </div>
                            </>
                          )}
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </CrudSection>

            <CrudSection
              title={
                <img
                  src={cdnURL('/static/partners/shipstation.svg')}
                  style={{
                    height: 85,
                    marginBottom: -30,
                    marginTop: -25,
                  }}
                />
              }
            >
              <Grid columns={16}>
                <Grid.Row>
                  <Grid.Column width={4}>
                    <h5>Connect your account</h5>
                    <div>
                      Enter the Shipstation API key and secret to be able to push Orders to
                      ShipStation.
                    </div>
                  </Grid.Column>
                  <Grid.Column width={12}>
                    <Grid columns={16}>
                      <Grid.Row>
                        <Grid.Column width={8}>
                          <Input
                            name="shipstation_key"
                            placeholder="ShipStation API Key"
                            onChange={onChange}
                            value={data.shipstation_key}
                            fluid
                            className={styles.MarginBottom}
                          />
                          <Input
                            name="shipstation_secret"
                            placeholder="ShipStation API secret"
                            className={styles.MarginBottom}
                            onChange={onChange}
                            value={data.shipstation_secret}
                            fluid
                          />
                          <ErrorRenderer error={errors?.shipstation} />
                        </Grid.Column>
                        <Grid.Column width={8}>
                          <div>
                            <Button
                              disabled={saveDisabled}
                              primary
                              className={styles.MarginBottom}
                              loading={loading === 'shipstation'}
                              onClick={() => {
                                setLoading('shipstation');
                                setErrors({shipstation: null});
                                api.shipstation
                                  .createConnection({
                                    account_name: data.shipstation_key,
                                    token: data.shipstation_secret,
                                    api_domain: 'ssapi.shipstation.com',
                                    refetch_token: data.name,
                                  })
                                  .then(response => {
                                    if (response?.resp?.ok) {
                                      alert('Connection successful!');
                                    } else {
                                      setErrors({shipstation: 'Connection error'});
                                    }
                                    setLoading(false);
                                  });
                              }}
                            >
                              Save
                            </Button>
                          </div>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </CrudSection>

            <CrudSection
              title={
                <img
                  src={cdnURL('/static/partners/finale.png')}
                  style={{
                    height: 40,
                    marginBottom: -5,
                  }}
                />
              }
            >
              <Grid columns={16}>
                <Grid.Row>
                  <Grid.Column width={4}>
                    <h5>API Access (optional)</h5>
                    <div>
                      Please enter the user/password Incredible should use to connect to your Finale
                      account API.
                    </div>
                  </Grid.Column>
                  <Grid.Column width={12}>
                    <Grid columns={16}>
                      <Grid.Row>
                        <Grid.Column width={8}>
                          <Input
                            name="finale_server"
                            placeholder="Finale Seller Account Name"
                            onChange={onChange}
                            value={data.finale_server}
                            fluid
                            className={styles.MarginBottom}
                          />
                          <Input
                            name="finale_user"
                            placeholder="Finale User"
                            onChange={onChange}
                            value={data.finale_user}
                            fluid
                            className={styles.MarginBottom}
                          />
                          <Input
                            name="finale_password"
                            placeholder="Finale Password"
                            className={styles.MarginBottom}
                            onChange={onChange}
                            value={data.finale_password}
                            fluid
                            type="password"
                          />
                          <ErrorRenderer error={errors?.finaleAPI} />
                        </Grid.Column>
                        <Grid.Column width={8}>
                          <div>
                            <Button
                              disabled={saveDisabled}
                              primary
                              className={styles.MarginBottom}
                              loading={loading === 'finaleAPI'}
                              onClick={() => {
                                setLoading('finaleAPI');
                                setErrors({finaleAPI: null});
                                api.finale
                                  .createConnection({
                                    account_name: data.finale_server,
                                    token: data.finale_user,
                                    api_domain: 'app.finaleinventory.com',
                                    refetch_token: data.finale_password,
                                  })
                                  .then(response => {
                                    if (response?.resp?.ok) {
                                      alert('Connection successful!');
                                    } else {
                                      setErrors({finaleAPI: 'Connection error'});
                                    }
                                    setLoading(false);
                                  });
                              }}
                            >
                              Save
                            </Button>
                          </div>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row
                  className={styles.MarginTop}
                  style={{borderTop: '1px solid #ccc', paddingTop: 32}}
                >
                  <Grid.Column width={4}>
                    <h5>FTP access</h5>
                    <div>
                      Use these credentials to import your Finale inventory and catalog via FTP.
                    </div>
                  </Grid.Column>
                  <Grid.Column width={12}>
                    <Grid columns={16}>
                      <Grid.Row>
                        <Grid.Column width={8}>
                          <Input
                            name="finale_ftp_server"
                            onChange={onChange}
                            value={'ftp.incrediblephones.com'}
                            fluid
                            disabled={true}
                            className={styles.MarginBottom}
                          />
                          <Input
                            name="finale_ftp_user"
                            placeholder="Request to your Incredible representative"
                            onChange={onChange}
                            value={data.finale_ftp_user}
                            fluid
                            disabled={true}
                            className={styles.MarginBottom}
                          />
                          <Input
                            name="finale_ftp_password"
                            placeholder="Request to your Incredible representative"
                            className={styles.MarginBottom}
                            disabled={true}
                            value={data.finale_ftp_password}
                            fluid
                          />
                          <ErrorRenderer error={errors?.finaleFTP} />
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </CrudSection>
          </>
        </Grid.Column>
      </Grid>
    </DashboardLayout>
  );
}

Dashboard.getInitialProps = async ({req, reduxStore, dispatch, query}) => {
  let errors = null;
  if (query['inc-action'] === 'brightpearl-redirect') {
    const response = await dispatch(Api.brightpearl.activateConnection(query, req));

    if (response.resp?.error) {
      errors = {brightpearl: response.resp?.message || response.resp?.error};
    }
  }
  const seller = await dispatch(Api.crud.getSeller({seller_id: query.seller_id}, req));
  let bpShippingMethods = null;

  if (seller?.resp?.connections?.brightpearl?.status === SellerConnectionStatus.active) {
    const methods = await dispatch(Api.brightpearl.getShippingMethods({}, req));
    bpShippingMethods = methods.resp || null;
  }

  await Promise.all([dispatch(Api.crud.sellersList({}, req))]);
  return {seller: seller.resp, errors, bpShippingMethods, query};
};

export default WithDashboardContext(
  WithActiveConfig(withI18next(['dashboard', 'common'])(Dashboard), true),
);

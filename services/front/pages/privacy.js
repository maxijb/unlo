import React, {useState} from 'react';
import Link from 'next/link';

import HighlightedProducts from '@Components/product/highlighted-products';
import {Grid, Button} from 'semantic-ui-react';
import Logger from '@Common/logger';
import Api from '../shared/actions/api-actions';
import Actions from '../shared/actions/app-actions';
import {withI18next} from '../shared/i18n/with-i18n.js';

import Highlights from '@Components/display/highlights';
import Header from '../shared/components/header/header';
import ABSelector from '../shared/components/ab/ab-selector';
import ABVariant from '../shared/components/ab/ab-variant';
import WithPublicContext from '@Components/hocs/with-public-context';
import {withAppConsumer} from '../shared/components/app-context';
import {getPageLink} from '@Common/utils/urls';
import classnames from 'classnames';

import {Passed, EarnIcon} from '../widget/components/display/svg-icons';
import SignupDialog from '@Components/login/signup-confirmation';
import SigninForm from '@Components/login/signin-confirmation';
import OrderStatus from '@Components/cart/order-status';
import Order from '@Components/order/order';
import Footer from '@Components/footer/footer';

import styles from '@Common/styles/styles.scss';
import css from './static-pages.scss';

function IndexView(props) {
  const {t, app, actions, currentOrder, user, query, api} = props;

  return (
    <div>
      <Header {...props} />

      <Grid columns={16} className={classnames(styles.MainGrid)}>
        <Grid.Row>
          <Grid.Column width={16} className={css.content}>
            <h1>Privacy Policy</h1>

            <h4>EFFECTIVE DATE: January 1, 2020</h4>
            <h3>
              <strong>INTRODUCTION</strong>
            </h3>
            <p>
              Future Signal, Inc (“Incredible,” “us” or “we”) is committed to protecting the privacy
              of our customers and end users. This Privacy and Cookie Policy (“Policy”) explains how
              we collect, share and use personal information collected through our corporate
              websites (including{' '}
              <a class="vglnk" href="http://incrediblephones.com" rel="nofollow">
                <span>www</span>
                <span>.</span>
                <span>incrediblephones</span>
                <span>.</span>
                <span>com</span>
              </a>
              , and marketplaces within the Incredible Phones Network) (the “Sites”) and any related
              Incredible Phones application, marketplace service or tool (collectively, the
              “Services”), as well as in connection with our events, sales and marketing activities.
              “You” or “your” means the individual using the Services and the entity which such
              individual represents.
            </p>
            <p>
              If you have any questions or concerns about our use of your personal information, then
              please contact us using the contact details provided at the bottom of this Policy.
            </p>
            <h3>
              <strong>ABOUT US</strong>
            </h3>
            <p>
              Incredible Phones is headquartered in the United States. We provide online consumer
              marketplace solutions and related services that enable our customers to more easily
              buy and sell refurbished inventory. Our customers include retailers, e-commerce
              companies and manufacturers looking to sell their inventory (“Sellers”) and consumers
              looking purchase that inventory (“Buyers”).
            </p>
            <h3>
              <strong>INFORMATION THAT WE COLLECT</strong>
            </h3>
            <p>
              The personal information that we may collect about you broadly falls into the
              following categories:
            </p>
            <p>
              Information you provide us. We may collect personal information you provide to us
              voluntarily: for example, when you apply to create an account to access the Services,
              buy products sold via the Sites, subscribe to marketing communications from us, submit
              enquires to us and/or otherwise contact us. We may also collect personal information
              from you offline, such as when you attend one of our events, during phone calls with
              sales representatives, or when you contact customer support. The personal information
              that you are asked to provide, and the reasons why you are ask to provide it, will be
              made clear to you at the point we ask you to provide your personal information.
            </p>
            <p>The information we collect may include:</p>
            <ul>
              <li>
                Contact and identification information, such as your first and last name, the name
                of the entity which you represent (if applicable), your email address, phone number,
                business address, business resale certificate number, and other customer
                information, including without limitation, the password chosen by you for access to
                the Services.
              </li>
              <li>
                Financial information, such as credit card or bank account numbers and address of
                the bank from which you will be wiring funds for purchases.
              </li>
              <li>
                Buying or selling information you provide during a transaction, or other
                transaction-based content that you generate or that is connected to your account as
                a result of a transaction you are involved in.
              </li>
            </ul>
            <p>
              We do not, however, knowingly collect personal information from children under the age
              of thirteen.
            </p>
            <p>
              <strong>Information we collect automatically.</strong>&nbsp;When you visit or use our
              Services, we automatically collect certain information from your device. In some
              countries, including countries in the European Economic Area, this information may be
              considered personal information under applicable data protection laws.
            </p>
            <p>
              Specifically, the information we collect automatically may include information such as
              your IP addresses, browser and device characteristics, operating system, language
              preferences, referring URLs, broad geographic location (e.g. country or city-level
              location) and other technical information. We may also collect information about how
              your device has interacted with our Sites, including pages accessed and links clicked.
            </p>
            <p>
              This information is primarily used to main the security and operation of our Services.
              Collecting this information also enables us to better understand the users (like you)
              who come to our Sites, where they come from, and what content on our Sites is of
              interest to them. We use this information for our internal analytics and reporting
              purposes and to improve the quality and relevance of our Services to users.
            </p>
            <p>
              Some of this information may be collected using cookies and similar tracking
              technologies.
            </p>
            <p>
              <strong>Information we obtain from third party sources.</strong> From time to time, we
              may receive personal information about you from third party sources (including lists
              from tradeshow events that you’ve registered for), but only where we have checked that
              these third parties either have your consent or are otherwise legally permitted or
              required to disclose your personal information to us.
            </p>
            <p>
              The types of information we collect from third parties include your first and last
              name, the name of the entity which you represent (if applicable), your email address,
              phone number, business address, business resale certificate number, and other customer
              information, and we use the information we receive from these third parties to access
              the Services, buy products sold via the Sites, subscribe to marketing communications
              from us, submit enquires to us and/or otherwise contact us.
            </p>
            <h3>
              <strong>HOW WE USE YOUR INFORMATION</strong>
            </h3>
            <p>
              We use personal information collected via our Sites for a variety of business purposes
              described below. If you are from the European Economic Area, we have also described
              the legal basis for collecting and using the personal information we collect from you.
            </p>
            <p>
              We normally collect personal information from you where the processing is in our
              legitimate business interests (“Business Purposes”), in order to enter into or perform
              a contract with you (“Contractual”), where we have your consent to so (“Consent”),
              and/or for compliance with our legal obligations (“Legal Reasons”).
            </p>
            <p>We may use the personal information we collect in the following ways:</p>
            <ul>
              <li>
                To provide access to and use of our Services (for our Business Purposes, Contractual
                purposes and/or with your Consent).
              </li>
              <li>
                To enable us to notify you to confirm your purchases (for Contractual purposes
                and/or with your Consent).
              </li>
              <li>
                To respond to your requests for support or to provide you with information you have
                requested about Incredible Phones or any of our Services (for our Business Purposes,
                Contractual purposes and/or with your Consent).
              </li>
              <li>
                To provide our Sellers, service providers and/or partners with information about
                your purchases to entities who require such information to ensure successful
                fulfillment of any purchases you make through the Services (for Contractual purposes
                and/or with your Consent).
              </li>
              <li>
                To send you information about your relationship or transactions with us and other
                service related information, including confirmations, invoices, technical notices,
                updates, security alerts and support and administrative messages (for our Business
                Purposes, Contractual purposes and/or with your Consent).
              </li>
              <li>
                To communicate with you and to personalize our communications with you (for our
                Business Purposes and/or with your Consent). For example, we may contact you by
                phone or email or other means (where this in accordance with your marketing
                preferences) to inform you about auctions, products, services, special offers or
                other promotional information related to our Services that we believe will be of
                interest to you. For more information about managing your marketing preferences,
                please see the “Your Data Protection Rights“
              </li>
              <li>
                To prevent, detect, mitigate, and investigate fraud, security breaches, potentially
                prohibited or illegal activities and enforce our agreements and policies (for our
                Business Purposes and/or Legal Reasons).
              </li>
              <li>
                For other Business Purposes such as data analysis, identifying usage trends,
                determining the effectiveness of our promotional campaigns and to enhance, customize
                and improve our Services.
              </li>
            </ul>
            <p>
              If you have questions about or need further information concerning the legal basis on
              which we collect and use your personal information, please contact us using the
              contact details provided under the “How to contact us”.
            </p>
            <h3>
              <strong>HOW WE RETAIN YOUR INFORMATION</strong>
            </h3>
            <p>
              We retain your personal information for as long as it is necessary and relevant for
              the purposes outlined in the section above. When we have no ongoing legitimate
              business need to process your personal information, we will either delete or anonymize
              it or, if this is not possible (for example, because your personal information has
              been stored in backup archives), then we will securely store your personal information
              and isolate it from further processing until deletion is possible.
            </p>
            <p>
              In addition, we may retain personal information from closed accounts for Legal
              Reasons, e.g. to comply with national laws, prevent fraud, collect any fees owed,
              resolve disputes, troubleshoot problems, assist with any investigation, enforce our
              customer agreements and take other actions permitted or required by applicable
              national laws.
            </p>
            <h3>
              <strong>HOW WE SHARE YOUR INFORMATION</strong>
            </h3>
            <p>
              We may disclose your personal information to the following categories of recipient’s
              parties for the following purposes:
            </p>
            <p>
              <strong>With Sellers:</strong> If you are purchasing products through the Services,
              Incredible Phones may share such information with the Sellers from whom you buy
              products via the Services, as necessary to ensure successful fulfillment of any
              purchases you make via the Services.
            </p>
            <p>
              <strong>Service Providers:</strong> Incredible Phones may use other companies to
              perform services on its behalf and which require access to your information to do that
              work. Examples include, hosting data, analyzing data, providing marketing assistance,
              facilitating some aspects of the Services, sending e-mail, fulfilling purchases and
              providing payment processing services. These other companies may be supplied with or
              have access to your personal information solely for purposes that are described in
              this Policy or notified to you when we collect your personal information.
            </p>
            <p>
              <strong>Incredible Phones Group Companies:</strong>&nbsp;We may share your information
              with our parent companies, subsidiaries and/or affiliates for use and processing
              purposes described in this Policy.
            </p>
            <p>
              <strong>Special Circumstances:</strong> There may be instances when Incredible may
              disclose personal information without providing you with a choice in order to protect
              the legal rights of its Sellers, Incredible, or any of their affiliates, employees,
              agents and contractors; to protect the safety and security of visitors to our Site; to
              protect against fraud or for risk management purposes; or to comply with the law,
              governmental request, judicial proceeding or other legal process (including in
              response to public authorities to meet national security or law enforcement
              requirements).
            </p>
            <p>
              <strong>Business Transfers:</strong> If Incredible Phones or any Seller sells all or
              part of its business or makes a sale or transfer of assets or is otherwise involved in
              a merger or business transfer, or in the unlikely event of a bankruptcy, we may
              transfer your personal information to a potential buyer (and its agents and advisors)
              in connection with that transaction.
            </p>
            <h3 id="yourdata">
              <strong>YOUR DATA PROTECTION RIGHTS</strong>
            </h3>
            <p>You have the following data protection rights:</p>
            <ul>
              <li>
                If you wish to&nbsp;<strong>access, correct, update</strong>&nbsp;or&nbsp;
                <strong>request deletion</strong>&nbsp;of your personal information, you can do so
                at any time by contacting us using the contact details provided under the “How to
                contact us” heading below.
              </li>
              <li>
                In addition, if you are a resident of the European Union, you can&nbsp;
                <strong>object to processing</strong>&nbsp;of your personal information, ask us
                to&nbsp;<strong>restrict processing</strong>&nbsp;of your personal information
                or&nbsp;<strong>request portability</strong>&nbsp;of your personal information.
                Again, you can exercise these rights by contacting us using the contact details
                provided under the “How to contact us” heading below.
              </li>
              <li>
                You have the right to&nbsp;<strong>opt-out of marketing communications</strong>
                &nbsp;we send you at any time. You can exercise this right by clicking on the
                “unsubscribe” or “opt-out” link in the marketing e-mails we send you. To opt-out of
                other forms of marketing (such as postal marketing or telemarketing), then please
                contact us using the contact details provided under the “How to contact us” heading
                below.
              </li>
              <li>
                Similarly, if we have collected and process your personal information with your
                consent, then you can&nbsp;<strong>withdraw your consent</strong>&nbsp;at any time.
                Withdrawing your consent will not affect the lawfulness of any processing we
                conducted prior to your withdrawal, nor will it affect processing of your personal
                information conducted in reliance on lawful processing grounds other than consent.
              </li>
              <li>
                You have the&nbsp;<strong>right to complain to a data protection authority</strong>
                &nbsp;about our collection and use of your personal information. For more
                information, please contact your local data protection authority. (Contact details
                for data protection authorities in the European Economic Area, Switzerland and
                certain non-European countries (including the US and Canada) are available&nbsp;
                <a href="http://ec.europa.eu/justice/data-protection/article-29/structure/data-protection-authorities/index_en.htm">
                  here
                </a>
                .)
              </li>
            </ul>
            <p>
              We will consider and act upon any request in accordance with applicable data
              protection laws.
            </p>
            <h3>
              <strong>SECURITY</strong>
            </h3>
            <p>
              We have implemented appropriate administrative, technical, personnel and physical
              measures designed to protect the security of any personal information in our
              possession against loss, theft and unauthorized use, disclosure or modification.
              However, please also remember that we cannot guarantee that the internet itself is
              100% secure. Although we will do our best to protect your personal information,
              transmission of personal information to and from our site is at your own risk. You
              should only access the services within a secure environment.
            </p>
            <h3>
              <strong>INTERNATIONAL DATA TRANSFERS</strong>
            </h3>
            <p>
              Your personal information may be transferred to, and processed in, countries other
              than the country in which you are resident. These countries may have data protection
              laws that are different to the laws of your country (and, in some cases, may not be as
              protective).
            </p>
            <p>
              Specifically, our Site servers are located in the United States, and our group
              companies and third party service providers and partners operate around the world.
              This means that when we collect your personal information we may process it in any of
              these countries.
            </p>
            <p>
              However, we have taken appropriate safeguards to require that your personal
              information will remain protected in accordance with this Policy. Such measures
              include implementing the European Commission’s Standard Contractual Clauses for
              transfers of personal information between our group companies and between us and our
              third-party providers, which require all such recipients to protect personal
              information that they process from the EEA in accordance with European data protection
              laws.
            </p>
            <h3 id="cookies">
              <strong>COOKIES AND SIMILAR TECHNOLOGIES</strong>
            </h3>
            <p>
              We use cookies (small text files containing a string of alphanumeric characters that
              we put on your computer) and other similar tracking technologies (like pixels) to
              collect information about the pages you view, the links you click, and other actions
              you take through our Services, in our Sites and within our advertising or e-mail
              content. We use the term “cookies” or “similar technologies” interchangeably in our
              policies to refer to all technologies that we may use to store data in your browser or
              device or that collect information or help us to identify in the manner described in
              this Policy.
            </p>
            <p>
              When you visit our Sites or use our Services, we may send one or more cookies to your
              browser. They enable us to store information about your device that is then used for
              matching certain other device related information that we collect via the browser.
              This helps us to, amongst other thing, remember your user preferences and maximize and
              analyze the performance of our Services. We also use cookies to provide, enhance and
              personalize certain aspects of our Services. Cookies are also used for targeting
              purposes as described below.
            </p>
            <p>Our use of these technologies fall into the following categories:</p>
            <p>
              <strong>Operationally necessary.</strong> These are cookies that are required for the
              operation of our Services. For example, these cookies are required to identify
              irregular site behavior, prevent fraudulent activity and improve security. They also
              allow users of our Services to make use of our functions such as shopping carts, saved
              search, or similar functions. Without these cookies, services that you have asked for
              cannot be provided.
            </p>
            <p>
              <strong>Functionality related.</strong>&nbsp;These cookies allow us to offer you
              enhanced functionality when accessing or using our Services. This may include to
              remembering choices you make, such as: remembering your username, preferences and
              settings; remembering if reacted to something on or through the Services, so you’re
              not asked to do it again; remembering if you’ve used any of our Services before;
              restricting the number of time you are shown a particular advertisement; remembering
              your location; and enabling social media components like Facebook or Twitter. As
              described below, you may disable any of these functional cookies; but if you do so,
              then various functions of our Services may be unavailable to you or may not work the
              way you want them to.
            </p>
            <p>
              <strong>Performance related.</strong>&nbsp;These cookies assess the performance of our
              Services, including as party of our analytic practices to help us understand how
              visitors use and interact with our Services, for instance which pages on our Sites
              they go to most often. These cookies also enable us to personalize content and
              remember your preferences (e.g., your choice of language, country, or region). These
              cookies help us improve the way our Sites work and provide a better, personalized user
              experience. Some of our performance related cookies are managed for us by third
              parties. However, we don’t allow the third party to use the cookies for any purpose
              other than those listed above.
            </p>
            <p>
              <strong>Advertising or targeted related.</strong>&nbsp;These cookies record your visit
              to our Services, the pages you have visited on our Sites, and the links you have
              clicked. They gather information about your browsing habits and remember that you have
              visited a Site. We (and third-party advertising platforms or networks) may use this
              information to make our Services, content, and advertisements displayed on our Sites
              more relevant to your interests (this is sometimes called “behavioral” or “targeted”
              advertising). These types of cookies are also used to limit the number of times you
              see an advertisement as well as to help measure the effectiveness of advertising
              campaigns.
            </p>
            <p>
              To find out more about interest-based ads and your choices, visit these sites: Digital
              Advertising Alliance, the Network Advertising Initiative, and the Interactive
              Advertising Bureau (IAB) Europe and these links:&nbsp;
              <a href="http://www.allaboutcookies.org/">http://www.allaboutcookies.org</a>
              &nbsp;or&nbsp;
              <a href="http://www.youronlinechoices.com/">http://www.youronlinechoices.com</a>.
            </p>
            <h3>
              <strong>Google Analytics</strong>
            </h3>
            <p>
              We use Google Analytics, a web analysis-tool of Google Inc., 1600 Amphitheater Parkway
              Mountain View, California, 94043, USA (“Google”). Google Analytics uses “cookies” to
              track visitor interactions, which are text files being saved to your computer to help
              us analyze visits to our Site and how our Services are used. For example, by using
              cookies, Google can tell us which pages our users view, which are most popular, what
              time of day our Sites are visited, whether visitors have been to our Sites before,
              what website referred the visitor to our Sites, and other similar information.
            </p>
            <p>
              For more information about Google analytic cookies, please see Google’s help pages and
              privacy policy:
            </p>
            <h3>
              <strong>How to delete and block cookies</strong>
            </h3>
            <p>
              You can disable and/or delete cookies by activating the setting on your browser that
              allows you to refuse the setting of all or some cookies. However, if you use your
              browser settings to block all cookies (including operationally necessary cookies), you
              may not be able to access all or parts of our Sites or Services. Unless you have
              adjusted your browser setting so that it will refuse cookies, our system will issue
              cookies as soon as you visit our Services.
            </p>
            <p>
              These settings are usually found in the “options” or “preferences” menu of your
              internet browser. Use the “Help” option in your Internet browser for more details. If
              you do not want your usage of our Services to be analyzed by Google, you can disable
              Google Analytics by using an add-on in your Internet browser. You can download and
              install the add-on at:&nbsp;
              <a href="https://tools.google.com/dlpage/gaoptout?hl=fr">
                https://tools.google.com/dlpage/gaoptout?hl=fr
              </a>
              .
            </p>
            <p>
              To find out more about cookies, visit:&nbsp;
              <a href="http://www.allaboutcookies.org/">http://www.allaboutcookies.org</a>
            </p>
            <h3>
              <strong>CHANGES TO POLICY</strong>
            </h3>
            <p>
              We may update this Policy from time to time, and you can see when it was last updated
              at the top of this policy. Please try to check on this page from time to time so that
              you can keep up to date with any changes.
            </p>
            <p>
              If we make any material changes, we will make every effort to post a prominent notice
              on the Site and/or notify you via email or notification on the Services.
            </p>
            <h3 id="contact">
              <strong>HOW TO CONTACT US</strong>
            </h3>
            <p>
              If you have any questions, comments or concerns about our Policy, you may contact us
              at
              <span class="skimlinks-unlinked">
                <a class="vglnk" href="mailto:support@incrediblephones.com" rel="nofollow">
                  <span>support@</span>
                  <span>incrediblephones</span>
                  <span>.</span>
                  <span>com</span>
                </a>
              </span>{' '}
              or by postal mail to “Incredible” at 2820 Howard Cmns #309, Howard, WI 54313.
            </p>
            <p>
              If you are a resident in the European Economic Area, the “data controller” of your
              personal information is Future Signal, Inc.
            </p>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Footer {...props} />
    </div>
  );
}

IndexView.getInitialProps = async ({req, reduxStore, dispatch, query, res}) => {};

export default withAppConsumer(WithPublicContext(withI18next(['home', 'common'])(IndexView)));

import React, {useState, useEffect, useCallback, useMemo, useRef} from 'react';
import Link from 'next/link';
import Router from 'next/router';
import {Trans} from 'react-i18next';

import useContactBanner from '@Components/product/use-contact-banner';
import {getUTCDateAsString, addWorkingDays} from '@Common/utils/date-utils';
import {UserFilesCdnURL} from '@Common/utils/statif-assets-utils';
import Head from 'next/head';
import MobilePriceDelivery from '@Components/display/mobile-price-delivery';
import CarrierSelector from '@Components/product/carrier-selector';
import TrustPilotReviews from '@Components/product/trustpilot-reviews';
import {Modals, NoSpecialCarrier} from '@Common/constants/app';
import ExtendedFooter from '@Components/footer/extended-footer';
import {getPageLink} from '@Common/utils/urls';
import Logger from '@Common/logger';
import {Grid, Button, Icon} from 'semantic-ui-react';
import Api from '../shared/actions/api-actions';
import Actions from '../shared/actions/app-actions';
import {withI18next} from '../shared/i18n/with-i18n.js';
import Header from '../shared/components/header/header';
import ImageBox from '@Components/product/image-box';
import PurchaseBox from '@Components/product/purchase-box';
import CompetitorPrices from '@Components/product/competitor-prices';

import StickyBuy from '@Components/product/sticky-buy';
import {RequestErrorMessage} from '@Common/constants/errors';
import classnames from 'classnames';
import WithPublicContext from '@Components/hocs/with-public-context';
import {withAppConsumer} from '../shared/components/app-context';
import {UnlockedCarrierId, EmptyDropdownChoice} from '@Common/constants/app';
import HighlightedProducts from '@Components/product/highlighted-products';
import Bubble from '@Components/display/bubble';
import {
  consolidateAttributes,
  consolidateAttributesAsName,
  getProductNameWithAttributes,
  findRelevantImages,
} from '../shared/utils/attributes-utils';
import {NextSeo, ProductJsonLd} from 'next-seo';
import TagLink from '@Components/display/tag-link';
import ChatLink from '@Components/display/chat-link';
import Stars from '@Components/display/stars';
import TechnicalSpecs from '@Components/product/technical-specs';
import TechnicalInspection from '@Components/product/technical-inspection';
import {Shield, Ribbon, Truck, Paypal} from '../widget/components/display/svg-icons';
import CartTradein from '@Components/cart/cart-tradein';
import BuyFixed from '@Components/product/buy-fixed';

import TrustPilot from '@Components/product/trustpilot';
import TradeInSection from '@Components/trade-in/trade-in-section';

import styles from '@Common/styles/styles.scss';
import css from './product.scss';

function Product(props) {
  const {
    app,
    app: {config},
    api,
    user,
    product,
    actions,
    t,
    cart,
    isMobile,
    isTablet,
    query,
    context: {GA, router},
    context,
    applySelections,
  } = props;

  const [hideMobileBanner, setHideMobileBanner] = useState(false);
  const [lastClicked, setLastClicked] = useState(null);
  const [selections, setSelectionsState] = useState(applySelections || {});
  const [openSpec, setOpenSpec] = useState('inspection');
  const [isOpenCarriersModal, setIsOpenCarriersModal] = useState(false);

  useEffect(() => {
    setLastClicked(null);
  }, [product.product?.id]);

  useContactBanner(app, user, actions, context);

  useEffect(() => {
    if (product.isLoading) {
      return;
    }
    const current = product?.defaultVersion?.tags;
    // abort if there
    let newSelections = selections;

    if (current?.length) {
      newSelections = consolidateAttributes(current);
      setSelectionsState({...newSelections, carrier: selections.carrier || newSelections.carrier});
    }

    if (lastClicked !== 'appearance') {
      const {appearance: _do_not_send, ...other} = newSelections;
      api.product.getMinimumPricePerCondition({
        id: product.product.id,
        selections: other,
      });
    }
  }, [product?.defaultVersion?.tags, lastClicked]);

  const loadVariant = newVariant => {
    GA.event({
      category: 'inputs',
      action: 'load-cheapest-variant',
      label: 'competitors',
      value: product?.product?.id || 0,
    });
    api.product.getVersionWithAttributes({variant_id: newVariant});
    setLastClicked(null);
  };

  // Get the list of all tags IDs
  const tags = useMemo(() => {
    return product.defaultVersion?.tags?.reduce((agg, it) => {
      if (!selections[it.type] || it.type === 'carrier') {
        agg.add(it.id);
      }
      return agg;
    }, new Set([null, selections.color, selections.capacity, selections.appearance, selections.carrier]));
  }, [product.defaultVersion?.tags, selections]);

  const longName = useMemo(() => {
    const data = consolidateAttributesAsName(product.defaultVersion?.tags, t);
    if (process.browser && product.defaultVersion?.variant?.id) {
      window.history.replaceState(
        null,
        null,
        getPageLink('variant', {
          product: {
            name: product.product.name,
            category_id: product.product.id,
            id: product.defaultVersion?.variant?.id,
          },
          attributes: data,
          t,
        }),
      );
    }
    return getProductNameWithAttributes(product.product.name, data, t);
  }, [product.product.name, product.defaultVersion?.tags]);

  // Load minimum price per condition
  useEffect(() => {
    api.product.relatedProducts({id: product.product.id});
  }, [product.product.id]);

  // Callback to update settings
  const setSelections = useCallback(
    (attr, value) => {
      const newSelections = {
        ...selections,
        [attr]: value,
      };

      if (attr === 'capacity') {
        api.product.getCompetitorPrices({category_id: product.product.id, attr_id: value});
      }

      GA.event({category: 'inputs', action: 'select-filter', label: attr, value: value || 0});
      setLastClicked(attr);
      setSelectionsState(newSelections);
      api.product.loadWithAttributes({
        selections: newSelections,
        lastClicked: attr,
        id: product.product.id,
      });
    },
    [selections],
  );

  const [isBuying, setIsBuying] = useState(false);
  const [isExtended, setIsExtended] = useState(false);

  const onBuy = async (redirect = true) => {
    setIsBuying(true);
    const variant = product.defaultVersion.variant;
    const currentAttributes = product.defaultVersion.tags;
    const response = await api.cart.addForUser({
      quantity: 1,
      variant_id: variant.id,
      attributes: currentAttributes,
      selections: NoSpecialCarrier.includes(selections.carrier)
        ? null
        : {carrier: selections.carrier},
    });
    api.product.getAccessoryProducts({
      id: product.product.id,
      seller_id: response?.resp?.seller_id || product.defaultVersion?.variant?.owner,
    });
    GA.event({
      category: 'cart',
      action: 'add-to-cart',
      label: 'main-product',
      value: product?.product?.id || 0,
    });
    if (response.resp.ok) {
      actions.cartAddReference({
        category_id: product.product.id,
        category: product.product.name,
        attributes: currentAttributes,
        ...variant,
      });
    }
    setIsBuying(false);
    if (redirect) {
      Router.push('/cart');
    }
  };

  const competitorPrices = (
    <CompetitorPrices
      name={product.product.name}
      product={product}
      actions={actions}
      api={api}
      loadVariant={loadVariant}
      capacity={selections.capacity}
      tags={tags}
      showImage={isMobile || isTablet}
    />
  );

  const refurbExplanation = (
    <>
      <div className={classnames(styles.section, styles.Tiny)}>
        {product?.seller?.id && product?.seller?.id != 1 && product?.seller?.name && (
          <div className={classnames(styles.MarginBottom)}>
            <Trans i18nKey="whoSeller" values={{name: product?.seller?.name}}>
              Text
              <a
                className={styles.ColorAccent}
                onClick={() => actions.setCurrentModal(Modals.sellerDescription)}
                className={styles.TinyBold}
              >
                link
              </a>
            </Trans>
          </div>
        )}
        <div className={styles.MarginBottom}>
          <span>{t('refurbishmentProcessExplanation')} </span>
          <a onClick={() => actions.setCurrentModal(Modals.waterproofing)}>{t('learnMore')}</a>
        </div>
        <div className={styles.MarginBottom}>
          <span>{t('toReduceWasteExplanation')} </span>
          <a onClick={() => actions.setCurrentModal(Modals.unbundling)}>{t('learnMore')}</a>
        </div>
      </div>
    </>
  );

  const specsHtml = (
    <>
      <Grid.Row>
        <Grid.Column mobile={8} tablet={8} computer={8}>
          <TagLink
            onClick={() => setOpenSpec('spec')}
            text={t('techSpecs')}
            chevron={null}
            selected={openSpec === 'spec'}
          />
        </Grid.Column>
        <Grid.Column mobile={8} tablet={8} computer={8}>
          <TagLink
            onClick={() => setOpenSpec('inspection')}
            text={t('inspectionInfo')}
            chevron={null}
            selected={openSpec === 'inspection'}
          />
        </Grid.Column>
      </Grid.Row>
      {openSpec !== null && (
        <Grid.Row>
          <Grid.Column computer={16} tablet={16} mobile={16}>
            {openSpec === 'spec' && <TechnicalSpecs product={product} />}
            {openSpec === 'inspection' && <TechnicalInspection product={product} />}
          </Grid.Column>
        </Grid.Row>
      )}
    </>
  );

  const imagesToRender = useMemo(() => {
    return findRelevantImages(product.allImages, tags);
  }, [product.allImages, tags]);

  return (
    <>
      <NextSeo
        title={longName}
        description={`Buy a pre-tested ${
          query.variantId ? longName : product.product?.name
        } that work like new for up to 70% less. Plus ✅ 12 month Warranty ✅ Free returns ✅ Buy now, pay later.`}
        canonical={`${config.protocol}://${config.fullDomain}${router.asPath}`}
        openGraph={{
          type: 'product',
          locale: 'en_US',
          url: `${config.protocol}://${config.fullDomain}${router.asPath}`,
          title: `${
            query.variantId ? longName : product.product?.name
          } | Factory Refurbished Iphones | 1-Yr Warranty | Free Returns`,
          description: `Buy a pre-tested ${
            query.variantId ? longName : product.product?.name
          } that work like new for up to 70% less. Plus ✅ 12 month Warranty ✅ Free returns ✅ Buy now, pay later.`,

          images: imagesToRender.map(img => ({
            url: UserFilesCdnURL(`400x400/${product.imageMap[img.img_id]}`),
            alt: longName,
          })),
        }}
      />
      <Head>
        <meta name="product:price:amount" content={product.cheapestOwnPrice} />
        <meta name="product:price:currency" content="USD" />
      </Head>
      <ProductJsonLd
        productName={query.variantId ? longName : product.product?.name}
        images={imagesToRender.map(img =>
          UserFilesCdnURL(`400x400/${product.imageMap[img.img_id]}`),
        )}
        description={`Buy a pre-tested ${
          query.variantId ? longName : product.product?.name
        } that work like new for up to 70% less. Plus ✅ 12 month Warranty ✅ Free returns ✅ Buy now, pay later.`}
        brand="Apple"
        aggregateRating={{
          ratingValue: '4.8',
          reviewCount: '182',
        }}
        offers={[
          {
            price: product.cheapestOwnPrice,
            priceCurrency: 'USD',
            itemCondition: 'https://schema.org/RefurbishedCondition',
            availability: product.defaultVersion?.variant?.stock
              ? 'https://schema.org/InStock'
              : 'https://schema.org/OutOfStock',
            seller: {
              name: 'Incredible Phones',
            },
            priceValidUntil: getUTCDateAsString(addWorkingDays(new Date(), 5)),
          },
        ]}
      />
      <Header {...props} />

      {isMobile && (
        <>
          <Grid columns={16} className={classnames(styles.MainGrid)}>
            <>
              <Grid.Row>
                <Grid.Column width={16}>
                  <Stars />
                  <div
                    className={classnames(
                      styles.Huge,
                      styles.centered,
                      styles.fluid,
                      styles.MarginTop,
                    )}
                  >
                    {longName}
                  </div>
                  <CarrierSelector
                    selections={selections}
                    allCarriers={app.carriers}
                    carriers={product.defaultVersion.tags?.filter(it => it.type === 'carrier')}
                    onlySecondLine={true}
                    api={api}
                    category_id={product.id}
                    isOpen={isOpenCarriersModal}
                    setIsOpen={setIsOpenCarriersModal}
                  />
                  {/*
                  <div
                    className={classnames(
                      styles.FlexSpaceBetween,
                      styles.MarginDoubleTop,
                      styles.MarginBottom,
                    )}
                  >
                    <Bubble
                      text={t('12moWarranty')}
                      onClick={() => actions.setCurrentModal(Modals.warranty)}
                    />
                    <Bubble
                      text={t('freeShipping')}
                      onClick={() => actions.setCurrentModal(Modals.freeShipping)}
                    />
                    <Bubble
                      text={t('30dayReturns')}
                      onClick={() => actions.setCurrentModal(Modals.freeReturns)}
                    />
                  </div>
                  */}
                </Grid.Column>
              </Grid.Row>
            </>
          </Grid>
          {competitorPrices}
        </>
      )}
      <Grid columns={16} className={classnames(styles.MainGrid)}>
        <>
          {!isMobile && (
            <Grid.Row>
              <Grid.Column width={16}>
                <div
                  className={classnames(
                    styles.FlexSpaceAround,
                    styles.sectionWithBackground,
                    css.topUSPs,
                  )}
                >
                  <div
                    className={styles.Clickable}
                    onClick={() => actions.setCurrentModal(Modals.warranty)}
                  >
                    <Ribbon />
                    {t('12MonthWarrantyIncluded')}
                  </div>
                  <div
                    className={styles.Clickable}
                    onClick={() => actions.setCurrentModal(Modals.freeReturns)}
                  >
                    <Shield />
                    {t('30dayReturnRefund')}
                  </div>
                  <div
                    className={styles.Clickable}
                    onClick={() => actions.setCurrentModal(Modals.freeShipping)}
                  >
                    <Truck />
                    Free shipping
                  </div>
                  <div
                    className={styles.Clickable}
                    onClick={() => actions.setCurrentModal(Modals.paypal)}
                  >
                    <Paypal size={20} />
                    {t('payPalBuyerProtection')}
                  </div>
                </div>
              </Grid.Column>
            </Grid.Row>
          )}
          <Grid.Row>
            {!isMobile && (
              <Grid.Column computer={6} mobile={16} tablet={8}>
                <div className={styles.MarginBottom}>{competitorPrices}</div>
                <ImageBox imageMap={product.imageMap} allImages={product.allImages} tags={tags} />
                {refurbExplanation}
                <Grid columns={16}>{specsHtml}</Grid>
                <CartTradein cart={cart} actions={actions} standAlone={true} location="product" />
              </Grid.Column>
            )}
            {isMobile && (
              <Grid.Column computer={6} mobile={16} tablet={9}>
                <div className={classnames(styles.MarginDoubleBottom, styles.MarginTop)}>
                  <MobilePriceDelivery
                    variant={product?.defaultVersion?.variant}
                    shippingMethodsBySeller={cart.shippingMethodsBySeller}
                  />
                </div>
                <PurchaseBox
                  isOpenCarriersModal={isOpenCarriersModal}
                  setIsOpenCarriersModal={setIsOpenCarriersModal}
                  onBuy={onBuy}
                  isBuying={isBuying}
                  actions={actions}
                  api={api}
                  loadingPricePerCondition={product.loadingPricePerCondition}
                  accessoryProducts={product.accessoryProducts}
                  allCarriers={app.carriers}
                  product={product.product}
                  tags={tags}
                  cart={cart}
                  selections={selections}
                  variant={product.defaultVersion.variant}
                  currentAttributes={product.defaultVersion.tags}
                  allAttributes={product.allAttributes}
                  minimumPricePerCondition={product.minimumPricePerCondition}
                  setSelections={setSelections}
                  setIsOpenTradeIn={actions.setIsOpenTradeIn}
                />

                <div className={classnames(styles.section)}>
                  {product.peopleWatching > 0 && (
                    <div className={css.topScarcity}>
                      <b>{t('prePeopleWatching')} </b>
                      <span>{t('peopleWatching', {number: product.peopleWatching})} </span>
                    </div>
                  )}
                  <Button
                    fluid
                    primary
                    onClick={() => {
                      onBuy(false);
                      setIsExtended(true);
                    }}
                    loading={isBuying}
                  >
                    <span>{t('buyNow')}</span>
                  </Button>
                </div>

                <BuyFixed
                  api={api}
                  actions={actions}
                  cart={cart}
                  variant={product.defaultVersion.variant}
                  onBuy={onBuy}
                  isBuying={isBuying}
                  accessoryProducts={product.accessoryProducts}
                  loadingAccessoryProducts={product.loadingAccessoryProducts}
                  isExtended={isExtended}
                  setIsExtended={setIsExtended}
                />

                {refurbExplanation}
                <ChatLink />
                <TrustPilotReviews />
                <TradeInSection cart={cart} setIsOpenTradeIn={actions.setIsOpenTradeIn} />
              </Grid.Column>
            )}
            {!isMobile && (
              <Grid.Column computer={10} tablet={8} style={{position: 'relative'}}>
                <StickyBuy
                  {...props}
                  isOpenCarriersModal={isOpenCarriersModal}
                  setIsOpenCarriersModal={setIsOpenCarriersModal}
                  longName={longName}
                  api={api}
                  onBuy={onBuy}
                  isBuying={isBuying}
                  product={product}
                  cart={cart}
                  tags={tags}
                  selections={selections}
                  setSelections={setSelections}
                  limitId={'highlighted_products'}
                />
              </Grid.Column>
            )}
          </Grid.Row>

          <Grid.Row id="highlighted_products">
            {Boolean(product.relatedProducts?.list?.length) && (
              <Grid.Column mobile={16} tablet={16} computer={16}>
                <HighlightedProducts
                  className={classnames(styles.MarginDoubleTop, styles.MarginDoubleBottom)}
                  products={product.relatedProducts}
                  title={t('otherPhonesYouMightLike')}
                  isProductPage={true}
                  page="product"
                />
              </Grid.Column>
            )}
          </Grid.Row>

          {isMobile && specsHtml}
        </>
      </Grid>
      <ExtendedFooter {...props} bottomSpace={isMobile} isProduct={true} />
    </>
  );
}

Product.getInitialProps = async ({req, reduxStore, dispatch, query, res}) => {
  if (query.variant_id) {
    const variant_id = query.variant_id.split('-')[0];
    const product = await dispatch(Api.product.getCategory({variant_id}, req));
    query.id = `${product.resp.category_id}-${variant_id}`;
  }

  const [product] = await Promise.all([
    dispatch(Api.product.getMinimumDetails({id: query.id, attr: query.attr}, req)),
    dispatch(Api.app.getCarriers({}, req)),
  ]);

  if (product?.resp?.error && product?.resp?.message === RequestErrorMessage.productNotFound) {
    if (res) {
      res.redirect('/404');
    } else {
      Router.push('/404');
    }
  }

  await dispatch(
    Api.product.getCompetitorPrices(
      {
        category_id: query.id.split('-')[0],
        attr_id: product.resp.defaultVersion?.tags
          ?.filter(t => t.type === 'capacity')
          ?.map(t => t.id),
      },
      req,
    ),
  );

  return {
    query: {
      ...query,
      variantId: query.id.split('-')[1],
    },
    applySelections: product?.resp?.applySelections,
  };
};

export default withAppConsumer(WithPublicContext(withI18next(['home', 'common'])(Product)));

import React from 'react';
import Link from 'next/link';
import {Grid} from 'semantic-ui-react';
import classnames from 'classnames';
import Router from 'next/router';
import isEmpty from 'is-empty';

import Logger from '@Common/logger';
import Api from '../shared/actions/api-actions';
import Actions from '../shared/actions/app-actions';
import {withI18next} from '../shared/i18n/with-i18n.js';
import {Button, Image, Modal, Form, Transition, Icon, Divider} from 'semantic-ui-react';

import Input from '../shared/components/inputs/wrapped-input';
import Header from '../shared/components/header/header';
import ABSelector from '../shared/components/ab/ab-selector';
import ABVariant from '../shared/components/ab/ab-variant';
import {LoadingStates} from '../../common/constants/social';

import {ValidateForm} from '@Common/utils/validations';
import FormValidations from '@Common/constants/form-validations';
import {Logo} from '@Widget/components/display/svg-icons';
import {AppColors} from '@Common/constants/app';
import {withAppConsumer, AppConsumer} from '../shared/components/app-context';
import {getPageLink} from '@Common/utils/urls';
import SigninForm from '@Components/login/signin-inline';
import {getLoginProps} from '@Components/hocs/get-login-props';
import {getHeroImg} from '@Common/utils/statif-assets-utils';
import ErrorRenderer from '../shared/components/inputs/error-renderer';

import fonts from '@Common/styles/styles.scss';
import css from './signup.scss';
import formcss from '@Components/login/signup-dialog.scss';

class ResetPasswordView extends React.Component {
  static async getInitialProps({req, dispatch}) {
    const heroImg = getHeroImg();

    if (req) {
      const data = await dispatch(
        Api.user.validateForgotPasswordToken({token: req.query.token}, req)
      );
      return {...(data.resp || data), heroImg};
    }
    return {error: new AuthenticationError(AuthenticationErrorMessage.missingToken), heroImg};
  }

  constructor(props) {
    super(props);
    this.state = {
      password: '',
      errors: {},
      loading: null
    };
  }

  onSuccessfulSubmit() {
    this.setState({passwordWasReset: true});
  }

  onGoBackLogin = () => {
    Router.push(getPageLink('signin'));
  };

  onResetPasswordSubmit() {
    const {errors, loading, password} = this.state;
    const {id, token} = this.props;
    const input = {password};
    const foundErrors = ValidateForm(input, FormValidations.resetPassword);
    this.setState({
      errors: foundErrors,
      loading: !isEmpty(foundErrors) ? null : LoadingStates.resetPassword
    });

    if (isEmpty(foundErrors)) {
      this.props.api.user.resetPassword({...input, id, token}).then(({resp}) => {
        const errors = !resp.error ? {} : resp.errorData || {form: {...resp}};
        this.setState({loading: null, errors});
        if (!resp.error) {
          this.onSuccessfulSubmit();
        }
      });
    }
  }

  onUpdateForm(field, eventOrValue) {
    const value = eventOrValue.target ? eventOrValue.target.value : eventOrValue;
    this.setState({[field]: value});
  }

  renderError() {
    const {error, t} = this.props;
    return (
      <div className={formcss.inlineSignupForm}>
        <Logo color={AppColors.accent} className={css.logo} />
        <div className={classnames(fonts.H1, css.title)}>
          {t('common:login.resetPasswordErrorTitle')}
        </div>
        <Form.Field>
          <div className={fonts.Medium}>{t('common:login.resetPasswordErrorSubTitle')}</div>
        </Form.Field>
        <ErrorRenderer error={error} />
        <Form.Field>
          <div className={fonts.SmallBold}>
            <a onClick={this.onGoBackLogin}>{t('common:login.goBackToLogin')}</a>
          </div>
        </Form.Field>
      </div>
    );
  }

  renderSuccess() {
    const {t} = this.props;

    return (
      <Form className={formcss.inlineSignupForm}>
        <Logo color={AppColors.accent} className={css.logo} />
        <div className={classnames(fonts.H1, css.title)}>
          {t('common:login.resetPasswordSuccess')}
        </div>
        <Form.Field>
          <div className={fonts.SmallBold}>
            <a onClick={this.onGoBackLogin}>{t('common:login.goBackToLogin')}</a>
          </div>
        </Form.Field>
      </Form>
    );
  }

  renderForm() {
    const {t} = this.props;
    const {password, errors, loading, passwordWasReset} = this.state;

    if (passwordWasReset) {
      return this.renderSuccess();
    }

    return (
      <Form onSubmit={this.onResetPasswordSubmit.bind(this)} className={formcss.inlineSignupForm}>
        <Logo color={AppColors.accent} className={css.logo} />
        <div className={classnames(fonts.H1, css.title)}>
          {t('common:login.resetPasswordTitle')}
        </div>
        <div className={fonts.Medium}>{t('common:login.resetPasswordSubTitle')}</div>
        <Form.Field>
          <Input
            label={t('common:login.newPassword')}
            size="large"
            type="password"
            value={password}
            error={errors.password}
            errorBottom={true}
            disabled={Boolean(loading)}
            onChange={this.onUpdateForm.bind(this, 'password')}
          />
        </Form.Field>
        <Form.Field>
          <Button
            size="large"
            fluid
            primary
            type="submit"
            disabled={Boolean(loading)}
            loading={loading === LoadingStates.resetPassword}
          >
            {t('common:login.submit')}
          </Button>
          <ErrorRenderer error={errors.form} />
        </Form.Field>
        <Form.Field>
          <div className={fonts.SmallBold}>
            <a onClick={this.onGoBackLogin}>{t('common:login.goBackToLogin')}</a>
          </div>
        </Form.Field>
      </Form>
    );
  }

  /* On all the other methods, the action creators are received as props, passed by the redux container */
  /* 't' is always available to get traductions */
  render() {
    const {t, app, actions, inviteDetails, user, api, heroImg, error} = this.props;

    return (
      <Grid colums={16} className="noMargin">
        <Grid.Column
          width={8}
          className={classnames(css.sellingColumn, css.column)}
          style={{backgroundImage: `url(${heroImg})`}}
        >
          <div className={css.heroContainer}>
            <h1 className={css.hero}>{t('home:signinLoginMainMessage')}</h1>
            <h3 className={css.subhero}>{t('home:signinLoginSubMessage')}</h3>
          </div>
        </Grid.Column>
        <Grid.Column width={8} className={css.column}>
          {!error ? this.renderForm() : this.renderError()}
        </Grid.Column>
      </Grid>
    );
  }
}

export default withAppConsumer(withI18next(['home', 'common'])(ResetPasswordView));

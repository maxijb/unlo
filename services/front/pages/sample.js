import React from 'react';
import Link from 'next/link';

import Logger from '@Common/logger';
import Api from '../shared/actions/api-actions';
import Actions from '../shared/actions/app-actions';
import {withI18next} from '../shared/i18n/with-i18n.js';

import Header from '../shared/components/header/header';
import ABSelector from '../shared/components/ab/ab-selector';
import ABVariant from '../shared/components/ab/ab-variant';

import css from './index.scss';
import {withAppConsumer, AppConsumer} from '../shared/components/app-context';
import {getPageLink} from '@Common/utils/urls';

class IndexView extends React.Component {
  /* On getInitialProps, we get the action creators from app-actions. and api-actions.js
   * If multiple actions need to be called we can paralelize with Promise.all
   * We need to pass req as final param in all API actions, so controllers have access to the DB.
   */
  static async getInitialProps({req, store, dispatch, ...other}) {
    const logger = req ? req.Logger : Logger;
    await Promise.all([dispatch(Api.getId({id: 23}, req)), dispatch(Actions.incrementCount())]);
  }

  componentDidMount() {
    this.props.context.Logger.info('claro');
  }

  /* On all the other methods, the action creators are received as props, passed by the redux container */
  /* 't' is always available to get traductions */
  render() {
    const {t, app, actions, user} = this.props;

    return (
      <div className={css.about}>
        <Header {...this.props} />
        <h1>{t('welcome')}</h1>
        {this.props.context.ab.track('test_run') !== 1 ? <h1>{t('welcome222')}</h1> : 'es el 1!!!'}
        <h1>{t('common:extendedComponent', {name: 'Maxi'})}</h1>
        <div>
          <a className={css.big} onClick={() => actions.incrementCount()}>
            INCREMENT
          </a>
          <a onClick={() => actions.v2.decrementCount()}> / DECREMENT</a>
        </div>
        <AppConsumer>{({Logger}) => Logger.info('maxi', 23, 45)}</AppConsumer>
        <ABSelector name="test_run">
          <ABVariant variant={3}>IT'S 3</ABVariant>
          <ABVariant variant={2}>IT'S 2</ABVariant>
          <ABVariant variant={1}>IT'S 1</ABVariant>
        </ABSelector>

        <ABSelector name="test_run" variant={2}>
          <span>Only renders if 2</span>
        </ABSelector>
        <pre>{JSON.stringify(app, undefined, 4)}</pre>
        <pre>{JSON.stringify(user, undefined, 4)}</pre>
        <Link prefetch href={getPageLink('index2')}>
          <a>go to index8</a>
        </Link>
        <video
          loop
          playsInline
          autoPlay
          muted
          preload="auto"
          style={{
            height: '500px',
            width: '500px',
            border: '1px solid black',
            background: 'blue',
          }}
        >
          <source type="video/mp4" src="/static/hero-video.mp4" />
        </video>
      </div>
    );
  }
}

export default withAppConsumer(withI18next(['home', 'common'])(IndexView));

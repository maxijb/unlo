import React, {useState} from 'react';
import Link from 'next/link';

import HighlightedProducts from '@Components/product/highlighted-products';
import {Grid, Button} from 'semantic-ui-react';
import Logger from '@Common/logger';
import Api from '../shared/actions/api-actions';
import Actions from '../shared/actions/app-actions';
import {withI18next} from '../shared/i18n/with-i18n.js';

import Highlights from '@Components/display/highlights';
import Header from '../shared/components/header/header';
import ABSelector from '../shared/components/ab/ab-selector';
import ABVariant from '../shared/components/ab/ab-variant';
import WithPublicContext from '@Components/hocs/with-public-context';
import {withAppConsumer} from '../shared/components/app-context';
import {getPageLink} from '@Common/utils/urls';
import classnames from 'classnames';

import {Passed, EarnIcon} from '../widget/components/display/svg-icons';
import SignupDialog from '@Components/login/signup-confirmation';
import SigninForm from '@Components/login/signin-confirmation';
import OrderStatus from '@Components/cart/order-status';
import Order from '@Components/order/order';
import Footer from '@Components/footer/footer';
import Separator from '@Components/display/separator';

import styles from '@Common/styles/styles.scss';
import css from './static-pages.scss';

function ShippingView(props) {
  const {t, app, actions, currentOrder, user, query, api} = props;

  return (
    <div>
      <Header {...props} />

      <Grid columns={16} className={classnames(styles.MainGrid)}>
        <Grid.Row>
          <Grid.Column width={16} className={css.content}>
            <h1>Shipping Policy</h1>

            <h4>EFFECTIVE DATE: January 1, 2020</h4>
            <h3>
              <strong>SHIPPING POLICY</strong>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                All US customers receive a free shipping option.{' '}
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                Items ordered before 2:00PM CST are shipped same-day. To receive free shipping,
                simply select the free shipping option that is listed at check-out. Free shipping
                has a four-day delivery speed. All US customers are eligible for free shipping,
                including customers in Alaska and Hawaii.{' '}
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                Customers also have the option to select an expedited shipping option, for a fee, at
                check-out. Incredible Phones reserves the right to select shipping carriers. Most
                orders are shipped via FedEx or USPS.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                <strong>Incredible Phones does not ship to hotel addresses or PO Boxes.</strong>{' '}
              </span>
            </p>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Footer {...props} />
    </div>
  );
}

ShippingView.getInitialProps = async ({req, reduxStore, dispatch, query, res}) => {};

export default withAppConsumer(WithPublicContext(withI18next(['home', 'common'])(ShippingView)));

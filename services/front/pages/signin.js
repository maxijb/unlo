import React from 'react';
import Link from 'next/link';
import Router from 'next/router';
import {Grid} from 'semantic-ui-react';
import classnames from 'classnames';
import get from 'lodash.get';

import Logger from '@Common/logger';
import Api from '../shared/actions/api-actions';
import Actions from '../shared/actions/app-actions';
import {withI18next} from '../shared/i18n/with-i18n.js';

import Header from '../shared/components/header/header';
import ABSelector from '../shared/components/ab/ab-selector';
import ABVariant from '../shared/components/ab/ab-variant';

import {withAppConsumer, AppConsumer} from '../shared/components/app-context';
import {getPageLink} from '@Common/utils/urls';
import {InviteTokenActions} from '@Common/constants/app';
import SigninForm from '@Components/login/signin-inline';
import {getLoginProps} from '@Components/hocs/get-login-props';
import {getHeroImg} from '@Common/utils/statif-assets-utils';

import css from './signup.scss';

class SigninView extends React.Component {
  static async getInitialProps(ctx) {
    const loginProps = await getLoginProps(ctx);
    return {...ctx.query, ...loginProps, heroImg: getHeroImg()};
  }

  openSignupModal = () => {
    const {next} = this.props;
    window.location = '/signup' + window.location.search;
  };
  /* On all the other methods, the action creators are received as props, passed by the redux container */
  /* 't' is always available to get traductions */
  render() {
    const {t, app, actions, inviteDetails, user, api, heroImg, next, inviteToken} = this.props;

    return (
      <Grid colums={16} className="noMargin">
        <Grid.Column
          width={8}
          only={'computer tablet'}
          className={classnames(css.sellingColumn, css.column)}
          style={{backgroundImage: `url(${heroImg})`}}
        >
          {inviteDetails?.action === InviteTokenActions.newSeller ? (
            <div className={css.heroContainer}>
              <h1 className={css.hero}>{t('loginToSeller')}</h1>
              <h3 className={css.subhero}>{t('inviteLoginSubMessage')}</h3>
            </div>
          ) : (
            <div className={css.heroContainer}>
              <h1 className={css.hero}>{t('signinLoginMainMessage')}</h1>
              <h3 className={css.subhero}>{t('signinLoginSubMessage')}</h3>
            </div>
          )}
        </Grid.Column>
        <Grid.Column tablet={8} computer={8} mobile={16} className={css.column}>
          <SigninForm
            next={next}
            inviteToken={inviteToken}
            inviteDetails={inviteDetails}
            submitForm={api.user.login}
            facebookLogin={api.user.facebookLogin}
            googleLogin={api.user.googleLogin}
            openSignupModal={this.openSignupModal}
            sendForgotPassword={api.user.sendForgotPassword}
            onSuccessLogin={() => Router.push('/user')}
          />
        </Grid.Column>
      </Grid>
    );
  }
}

export default withAppConsumer(withI18next(['home', 'common'])(SigninView));

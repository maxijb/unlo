import React from 'react';
import classnames from 'classnames';
import Link from 'next/link';
import Router from 'next/router';
import {Grid} from 'semantic-ui-react';
import {get} from 'lodash';

import Logger from '@Common/logger';
import Api from '../shared/actions/api-actions';
import Actions from '../shared/actions/app-actions';
import {withI18next} from '../shared/i18n/with-i18n.js';

import Header from '../shared/components/header/header';
import ABSelector from '../shared/components/ab/ab-selector';
import ABVariant from '../shared/components/ab/ab-variant';

import {InviteTokenActions} from '@Common/constants/app';
import {withAppConsumer, AppConsumer} from '../shared/components/app-context';
import {getPageLink} from '@Common/utils/urls';
import SignupForm from '@Components/login/signup-inline';
import {getLoginProps} from '@Components/hocs/get-login-props';
import {getHeroImg} from '@Common/utils/statif-assets-utils';

import css from './signup.scss';

class SignupView extends React.Component {
  static async getInitialProps(ctx) {
    const loginProps = await getLoginProps(ctx);
    return {...ctx.query, ...loginProps, heroImg: getHeroImg()};
  }

  openSigninModal = () => {
    const {next} = this.props;
    window.location = '/signin' + window.location.search;
  };
  /* On all the other methods, the action creators are received as props, passed by the redux container */
  /* 't' is always available to get traductions */
  render() {
    const {t, app, inviteDetails, actions, user, api, next, heroImg, inviteToken} = this.props;

    return (
      <Grid colums={16} className="noMargin">
        <Grid.Column
          width={8}
          className={classnames(css.sellingColumn, css.column)}
          style={{backgroundImage: `url(${heroImg})`}}
          only={'tablet computer'}
        >
          {inviteDetails?.action === InviteTokenActions.newSeller ? (
            <div className={css.heroContainer}>
              <h1 className={css.hero}>{t('createSellerAccount')}</h1>
              <h3 className={css.subhero}>{t('inviteLoginSubMessage')}</h3>
            </div>
          ) : (
            <div className={css.heroContainer}>
              <h1 className={css.hero}>{t('signupLoginMainMessage')}</h1>
              <h3 className={css.subhero}>{t('signupLoginSubMessage')}</h3>
            </div>
          )}
        </Grid.Column>
        <Grid.Column tablet={8} computer={8} mobile={16} className={css.column}>
          <SignupForm
            inviteDetails={inviteDetails}
            inviteToken={inviteToken}
            submitForm={api.user.signup}
            facebookLogin={api.user.facebookLogin}
            googleLogin={api.user.googleLogin}
            openSigninModal={this.openSigninModal}
            next={next}
            onSuccessLogin={() => Router.push('/user')}
          />
        </Grid.Column>
      </Grid>
    );
  }
}

export default withAppConsumer(withI18next(['home', 'common'])(SignupView));

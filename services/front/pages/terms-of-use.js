import React, {useState} from 'react';
import Link from 'next/link';

import HighlightedProducts from '@Components/product/highlighted-products';
import {Grid, Button} from 'semantic-ui-react';
import Logger from '@Common/logger';
import Api from '../shared/actions/api-actions';
import Actions from '../shared/actions/app-actions';
import {withI18next} from '../shared/i18n/with-i18n.js';

import Highlights from '@Components/display/highlights';
import Header from '../shared/components/header/header';
import ABSelector from '../shared/components/ab/ab-selector';
import ABVariant from '../shared/components/ab/ab-variant';
import WithPublicContext from '@Components/hocs/with-public-context';
import {withAppConsumer} from '../shared/components/app-context';
import {getPageLink} from '@Common/utils/urls';
import classnames from 'classnames';

import {Passed, EarnIcon} from '../widget/components/display/svg-icons';
import SignupDialog from '@Components/login/signup-confirmation';
import SigninForm from '@Components/login/signin-confirmation';
import OrderStatus from '@Components/cart/order-status';
import Order from '@Components/order/order';
import Footer from '@Components/footer/footer';

import styles from '@Common/styles/styles.scss';
import css from './static-pages.scss';

function IndexView(props) {
  const {t, app, actions, currentOrder, user, query, api} = props;
  const [isSignin, setIsSignin] = useState(false);

  return (
    <div>
      <Header {...props} />

      <Grid columns={16} className={classnames(styles.MainGrid)}>
        <Grid.Row>
          <Grid.Column width={16} className={css.content}>
            <h1>Terms of service</h1>
            <h3>
              <strong>INTRODUCTION</strong>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                Welcome to the website of Future Signal, Inc., a Delaware corporation, dba
                “Incredible,” with its principal place of business at Incredible (hereinafter
                “Incredible”). The website is operated by Incredible Phones at{' '}
                <span class="skimlinks-unlinked">
                  <a class="vglnk" href="http://incrediblephones.com" rel="nofollow">
                    <span>http</span>
                    <span>://</span>
                    <span>www</span>
                    <span>.</span>
                    <span>incrediblephones</span>
                    <span>.</span>
                    <span>com</span>
                  </a>
                </span>{' '}
                (the “Website”).&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                PLEASE READ THIS TERMS OF SERVICE AGREEMENT (THE “TERMS OF SERVICE” OR “AGREEMENT”)
                CAREFULLY. BY ACCESSING OR USING THIS WEBSITE IN ANY WAY, INCLUDING USING THE
                SERVICES AND RESOURCES AVAILABLE OR ENABLED VIA THE WEBSITE (EACH, A “SERVICE” AND
                COLLECTIVELY, THE “SERVICES”) BY Incredible Phones OR USERS OF THE WEBSITE
                (“USERS”), CLICKING ON THE “I ACCEPT” BUTTON, COMPLETING THE REGISTRATION PROCESS,
                AND/OR BROWSING THE WEBSITE, YOU REPRESENT THAT&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                (1) YOU HAVE READ, UNDERSTAND, AND AGREE TO BE BOUND BY THIS TERMS OF SERVICE; (2)
                YOU ARE OF LEGAL AGE TO FORM A BINDING CONTRACT WITH Incredible; AND (3) YOU HAVE
                THE AUTHORITY TO ENTER INTO THIS TERMS OF SERVICE PERSONALLY OR ON BEHALF OF THE
                COMPANY YOU HAVE NAMED AS THE SELLER, AND TO BIND THAT COMPANY TO THE TERMS OF
                SERVICE. THE TERM “YOU” REFERS TO THE INDIVIDUAL OR LEGAL ENTITY, AS APPLICABLE,
                IDENTIFIED AS THE USER WHEN YOU REGISTERED ON THE WEBSITE.{' '}
              </span>
              <b>
                IF YOU DO NOT AGREE TO BE BOUND BY THE TERMS OF SERVICE, YOU MAY NOT ACCESS OR USE
                THIS WEBSITE OR THE SERVICES.
              </b>
              <span nostyle="font-weight: 400;">&nbsp;</span>
            </p>
            <p>
              <b>
                THE TERMS OF SERVICE INCLUDES (1) YOUR AGREEMENT THAT Incredible Phones HAS NO
                LIABILITY REGARDING THE SERVICES (SECTION 2); (2) YOUR AGREEMENT THAT THE SERVICES
                ARE PROVIDED “AS IS” AND WITHOUT WARRANTY (SECTION 8); (3) YOUR CONSENT TO RELEASE
                Incredible Phones FROM LIABILITY BASED ON CLAIMS BETWEEN USERS (SECTION 3.3) AND
                GENERALLY (SECTION 16.5); AND (4) YOUR AGREEMENT TO INDEMNIFY Incredible Phones FOR
                YOUR USE OR INABILITY TO USE THE SERVICES (SECTION 5.7).&nbsp;
              </b>
            </p>
            <p>
              <b>
                THE TERMS OF SERVICE INCLUDES A CLASS ACTION WAIVER AND A WAIVER OF JURY TRIALS, AND
                REQUIRES BINDING ARBITRATION ON AN INDIVIDUAL BASIS TO RESOLVE DISPUTES (SECTION
                16.9).&nbsp;
              </b>
            </p>
            <p>
              <b>
                THE TERMS OF SERVICE LIMITS THE REMEDIES THAT MAY BE AVAILABLE TO YOU IN THE EVENT
                OF A DISPUTE.&nbsp;
              </b>
            </p>
            <h3>
              <strong nostyle="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen-Sans, Ubuntu, Cantarell, 'Helvetica Neue', sans-serif;">
                CHANGES TO TERMS
              </strong>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                PLEASE NOTE THAT THE AGREEMENT IS SUBJECT TO CHANGE BY Incredible Phones IN ITS SOLE
                DISCRETION AT ANY TIME.
              </span>{' '}
              <span nostyle="font-weight: 400;">
                When changes are made, Incredible Phones will make a new copy of the Terms of
                Service available at the Website, and any new Supplemental Terms will be made
                available from within, or through, the affected Service on the Website. We will also
                update the “Last Updated” date at the top of the Terms of Service. If we make any
                material changes, and you have registered with us to create an Account (as defined
                in Section 4.1 below), we will also send an email to you at the last email address
                you provided to us pursuant to the Agreement. Any changes to the Agreement will be
                effective immediately for new Users of the Website, the Services, and the Software
                (as these terms are defined below) (collectively, the “Incredible Phones
                Properties”) and will be effective thirty (30) days after posting notice of such
                changes on the Website for Users, provided that any material changes shall be
                effective for Users who have an Account with us upon the earlier of thirty (30) days
                after posting notice of such changes on the Website or thirty (30) days after
                dispatch of an email notice of such changes to Members (defined in Section 1.5).
                Incredible Phones may require you to provide consent to the updated Agreement in a
                specified manner before use of the Incredible Phones Properties is permitted. If you
                do not agree to any change(s) after receiving notice of such change(s), you shall
                stop using the Incredible Properties. Otherwise, your continued use of the Website,
                the Services and/or the Software constitutes your acceptance of such change(s).{' '}
              </span>
              <b>PLEASE REGULARLY CHECK THE WEBSITE TO VIEW THE THEN-CURRENT AGREEMENT.&nbsp;</b>
            </p>
            <h3>
              <strong nostyle="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen-Sans, Ubuntu, Cantarell, 'Helvetica Neue', sans-serif;">
                ABOUT US
              </strong>
            </h3>
            <p>
              Incredible Phones is headquartered in the United States. We provide online consumer
              marketplace solutions and related services that enable our customers to more easily
              buy and sell refurbished inventory. Our customers include retailers, e-commerce
              companies and manufacturers looking to sell their inventory (“Sellers”) and consumers
              looking purchase that inventory (“Buyers”).
            </p>
            <h3>
              <b nostyle="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen-Sans, Ubuntu, Cantarell, 'Helvetica Neue', sans-serif;">
                1. DEFINITIONS&nbsp;
              </b>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                1.1 ‘Buyer’: means a Member who has indicated his/her acceptance of an offer from a
                Seller on one of the Websites.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                1.2 ‘Seller’: means a Member who opens a virtual shop on the Website and puts
                Products up for sale through the Website.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                1.3 ‘Content’: means any information, data, text, software, music, sound,
                photographs, graphics, video, messages, tags, and/or other materials accessible
                through the Incredible Phones Properties.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                1.4 ‘User Content’: means any content provided by a Member on our Website, including
                content in the following non exhaustive list: any Product, data, information, text,
                listed object, description, comment, Seller or Product review, name, pseudonym,
                store name, photograph, picture, sound, video, logo or any other element provided by
                the Member on the Website, including during the Member’s registration, in an ad or
                in an email, on a forum or on a Seller’s shop.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                1.5 ‘Member’: means any person or company that registers on our Website.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                1.6 ‘User’: means any person or company using the Website.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                1.7 ‘Virtual Wallet’ designates: (a) for the Seller, the money reserve available on
                the account of a Seller who has made at least one sale on one of the Websites
                through the Incredible Phones secure bank card payment system, and has been credited
                with the sale amount. The Virtual Wallet can be used by the Seller to make
                storefront purchases or to buy products on the Website; (b) for the Buyer, when
                available, the money reserve available on the account of a Buyer who has made at
                least one purchase on one of the Websites through the Incredible Phones secure bank
                card payment system, and has been partially or completely refunded for his/her
                purchases by the Seller following a refund request. The Virtual Wallet can be used
                by the Buyer to buy Products on the Website.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                1.8 ‘Product’ means an electronic product which has been put up for sale on the
                Website by the Seller. The Product must be compliant with this Agreement and in
                particular, it must not be a Forbidden Product.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                1.9 ‘Forbidden Product’ means a product whose sale on the Website is forbidden, as
                more fully explained in Section 5.13 of this Agreement.&nbsp;
              </span>
            </p>
            <h3>
              <b>2. SERVICES</b>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                2.1 Incredible Phones provides a marketplace that allows Members to offer, sell and
                buy Products. As a marketplace, we do not own, possess or sell the Products listed
                on the Website, so the actual contract for sale is directly between the Seller and
                the Buyer. Incredible Phones does not act as a consignee.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                2.2 While Incredible Phones may provide pricing and guidance in our Services, such
                information is solely informational. We are not a party to any transaction between
                Members. We do not have control over the quality, timing, legality, failure to
                provide, or any aspect whatsoever of any ratings provided by Members, Products sold
                by Sellers, or of the integrity, responsibility, or any actions of any Users.
                Incredible Phones makes no representations about the suitability, reliability,
                timeliness or accuracy in public, private or offline interactions. Although
                Incredible Phones may provide background checks, we cannot confirm that each Seller
                is who it claims to be. Incredible Phones does not assume any responsibility for the
                accuracy or reliability of this information or any information provided through the
                Services.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                When interacting with other Users you should exercise caution and common sense to
                protect your personal safety and property, just as you would when interacting with
                other persons whom you don’t know. NEITHER Incredible Phones NOR ITS AFFILIATES OR
                LICENSORS IS RESPONSIBLE FOR THE CONDUCT, WHETHER ONLINE OR OFFLINE, OF ANY USER OF
                THE SERVICES. Incredible Phones AND ITS AFFILIATES AND LICENSORS WILL NOT BE LIABLE
                FOR ANY CLAIM, INJURY OR DAMAGE ARISING IN CONNECTION WITH YOUR USE OF THE
                Incredible Phones PROPERTIES.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                2.3 The Incredible Phones Properties and the information and content available on
                the Incredible Phones Properties are protected by copyright laws throughout the
                world. Subject to the Agreement, Incredible Phones grants you a limited license to
                reproduce portions of the Incredible Phones Properties for the sole purpose of using
                the Services to buy Products in accordance with the terms of this Agreement. Unless
                otherwise specified by Incredible Phones in a separate license, your right to use
                any Incredible Phones Properties is subject to this Agreement.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                2.4 Use of the Incredible Phones Internet Platform and any other software and
                associated documentation that is made available via the Website or the Services
                (“Software”) is governed by the terms of the license agreement that accompanies or
                is included with the Software, or by the license agreement expressly stated on the
                Website page(s) accompanying the Software. These license terms may be posted with
                the Software downloads or at the Website page where the Software can be accessed.
                Use of the Software is made available solely for purposes of providing the Services.
                You shall not use, download or install any Software that is accompanied by or
                includes a license agreement unless you agree to the terms of such license
                agreement. At no time will Incredible Phones provide you with any tangible copy of
                our Software. Incredible Phones shall deliver access to the Software via electronic
                transfer or download and shall not use or deliver any tangible media in connection
                with the (a) delivery, installation, updating or problem resolution of any Software
                (including any new releases); or (b) delivery, correction or updating of
                documentation. For the purposes of this section tangible media shall include, but
                not be limited to, any tape disk, compact disk, card, flash drive, or any other
                comparable physical medium. Unless the accompanying license agreement expressly
                allows otherwise, any copying or redistribution of the Software is prohibited,
                including any copying or redistribution of the Software to any other server or
                location, or redistribution or use on a service bureau basis. If there is any
                conflict between the Terms and the license agreement, the license agreement shall
                take precedence in relation to that Software (except as provided in the following
                sentence). If the Software is a pre-release version, then, notwithstanding anything
                to the contrary included within an accompanying license agreement, you are not
                permitted to use or otherwise rely on the Software for any commercial or production
                purposes. If no license agreement accompanies use of the Software, use of the
                Software will be governed by this Agreement. Subject to your compliance with this
                Agreement, Incredible Phones grants you a non-assignable, non-transferable,
                non-sub-licensable, revocable non-exclusive license to use the Software for the sole
                purpose of enabling you to use the Services in the manner permitted by this
                Agreement. Some Software may be offered under an open source license that we will
                make available to you. There may be provisions in the open source license that
                expressly override some of these terms.&nbsp;
              </span>
            </p>
            <h3>
              <b>3. Incredible Phones ROLE AND RESPONSIBILITIES&nbsp;</b>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                3.1 Incredible Phones offers Members community tools allowing them to hold a
                dialogue with other Members and exchange information about their common
                interests.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                3.2 Incredible Phones on one hand and each Member on the other are independent
                parties, with each acting on its own behalf and on its own account. Unless
                stipulated otherwise in this Agreement, the Agreement does not create any
                relationship of subordination, mandate, silent partnership, joint venture, or any
                employer/employee or franchisor/franchisee relationship between Incredible Phones
                and each Member.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                3.3 Incredible Phones expressly disclaims any liability that may arise between Users
                of its Services. The Services are only a venue for connecting Members. Because
                Incredible Phones is not involved in the actual contract between Members or in the
                completion of the sale, in the event that you have a dispute with one or more other
                Members, you release Incredible Phones (and our officers, directors, agents,
                investors, subsidiaries, and employees) from any and all claims, demands, or damages
                (actual or consequential) of every kind and nature, known and unknown, suspected and
                unsuspected, disclosed and undisclosed, arising out of or in any way connected with
                such dispute.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                3.4 Without prejudice to the foregoing, Incredible Phones reserves the right to
                participate in the promotion of the Products put online and commercialized by the
                Sellers, particularly through the setting up of partnerships with other websites
                and/or media publications and/or the sending of advertising emails complying with
                the applicable legal requirements. Such participation does not affect the defined
                role limited to intermediation between the Sellers and the Buyers, as Incredible
                Phones is only promoting the Incredible Website and the Products sold there, but
                does not intervene in the relationship between Buyer and Seller concerning one or
                more specific Products, except to the limited extent set forth in this
                Agreement.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                3.5 Incredible Phones does not control the information provided by the Members and
                made public on the Website. Information from the other Members may be offensive,
                prejudicial, inaccurate or deceptive. The identity and quality of a Member may be
                different from those displayed on the Website. Incredible Phones recommends that
                each User should exercise caution and apply common sense when using the Website and
                the Services.&nbsp;
              </span>
            </p>
            <h3>
              <b>4. REGISTRATION&nbsp;</b>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                4.1 To access the Services, Buyer must before access register and open an account on
                the Website. Registration is only authorized for legal entities and for natural
                persons over the age of 18 who have the capacity to accomplish legal acts.
                Registration of a legal entity can only be carried out by a person duly authorized
                to represent the entity, which must be named.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                4.2 Any person who registers as a Buyer on our Website agrees to provide accurate,
                full and up-to-date information, and any other data (personal data in particular)
                that may be necessary for finalizing and maintaining registration on the Website, in
                particular a valid email address and all the additional information required to
                become a Buyer. Buyer also agrees to update his/her account and make any
                modifications to his/her information without delay. Buyer will be solely responsible
                for the possible consequences of providing untruthful, invalid or erroneous
                information to Incredible Phones or to another Member.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                4.3 To register, Buyer must choose a username (which, subject to Incredible’s
                approval, may be a pseudo) and a password. Buyer agrees not to register a username
                that could infringe a third party’s rights. In particular, Buyer will not use a
                username which infringes the copyrights, brands, company name or store name of a
                third party.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                4.4 Registration of a Buyer on the Website allows him/her to create a unique account
                through which he/she can access the Services available through the Website.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                4.5 The username and the password of the Buyer are strictly personal and the Buyer
                agrees to keep them confidential. Only the registered Buyer is authorized to use the
                Services with his/her username and password and Buyer agrees that he/she will not
                allow access with his/her username and password by any other person. The use of the
                Buyer’s username, associated with his/her password, results in a presumption that
                the Buyer concerned is accessing and using the Services. In the event that Buyer
                becomes aware of access being made to his/her account by a third party, Buyer agrees
                to inform Incredible Phones without delay by an email sent to the address: support@
                <span class="skimlinks-unlinked">
                  <a class="vglnk" href="http://incrediblephones.com" rel="nofollow">
                    <span>incrediblephones</span>
                    <span>.</span>
                    <span>com</span>
                  </a>
                </span>
                &nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                4.6 Buyer agrees to create and use only one account. Any derogation from this rule
                will require an explicit request from Buyer and a specific and express authorization
                by Incredible Phones The action of creating or using new accounts under one’s own
                identity or the identity of a third party without having requested and obtained
                authorization from Incredible Phones may lead to immediate suspension of Buyer’s
                accounts and of all the associated services.&nbsp;
              </span>
            </p>
            <h3>
              <b>5. USER CONTENT; ACCEPTABLE USE</b>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                5.1 Buyer agrees to use the Website and provide User Content on the Website in a way
                that complies with this Agreement and current legal and/or regulatory
                provisions.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                5.2 Buyer is solely responsible for his/her User Content. Incredible Phones only
                plays a passive technical intermediation role to put the User Content online and
                does not control the Content before it is put online. Buyer agrees not to provide
                any Content and more generally not to take any action or perform any activity: You
                acknowledge that Incredible Phones has no obligation to pre-screen Content
                (including, but not limited to, your User Content and other User Content), although
                Incredible Phones reserves the right in its sole discretion to pre-screen, refuse or
                remove any Content. By entering into this Agreement, you hereby provide your
                irrevocable consent to such monitoring. You acknowledge and agree that you have no
                expectation of privacy concerning the transmission of your User Content, including
                without limitation chat, text or voice communications. In the event that Incredible
                Phones pre-screens, refuses or removes any Content, you acknowledge that Incredible
                Phones will do so for its benefit, not yours. Without limiting the foregoing,
                Incredible Phones shall have the right to remove any Content that violates the
                Agreement or is otherwise objectionable.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                5.3 While using or accessing the Incredible Phones Properties, you agree that you
                will not, under any circumstances provide any User Content, and more generally, you
                will not take any action or perform any activity on the Incredible Properties:&nbsp;
              </span>
            </p>
            <ul>
              <li>
                <span nostyle="font-weight: 400;">
                  that infringes any copyrights, patents, brands, designs and models, manufacturing
                  secrets, or infringes any right of disclosure or the privacy of third
                  parties;&nbsp;
                </span>
              </li>
              <li>
                <span nostyle="font-weight: 400;">
                  that is defamatory, offensive, derogatory. false, misleading or libelous; that is
                  discriminatory or encourages violence or racial, religious or ethnic hatred;&nbsp;
                </span>
              </li>
              <li>
                <span nostyle="font-weight: 400;">that is obscene or pedophilic;&nbsp;</span>
              </li>
              <li>
                <span nostyle="font-weight: 400;">
                  that could be deemed to constitute misappropriation, swindle, or breach of trust,
                  or to constitute any other criminal offense;&nbsp;
                </span>
              </li>
              <li>
                <span nostyle="font-weight: 400;">
                  that is intended to obtain the transfer of money without in return delivering a
                  Product that is compliant with the present Terms of Service and is of the same
                  value as the requested sum of money or fail to pay for Products purchased by you,
                  except to the extent set forth herein;&nbsp;
                </span>
              </li>
              <li>
                <span nostyle="font-weight: 400;">
                  that could damage any IT system or surreptitiously intercept any data or personal
                  information;&nbsp;
                </span>
              </li>
              <li>
                <span nostyle="font-weight: 400;">
                  that engages the responsibility of Incredible Phones or that has as a consequence
                  loss of the benefit of all or part of the services of Incredible‘s service
                  providers, and particularly its Internet services providers, payment service
                  providers and storage service providers;&nbsp;
                </span>
              </li>
              <li>
                <span nostyle="font-weight: 400;">
                  that could damage the reputation of Incredible Phones or constitute acts of unfair
                  competition with regard to Incredible, or to any other Member or third
                  party;&nbsp;
                </span>
              </li>
              <li>
                <span nostyle="font-weight: 400;">
                  that breaches any applicable law or regulation and/or any contractual clause that
                  is binding on the Member;{' '}
                </span>
              </li>
              <li>
                <span nostyle="font-weight: 400;">
                  that may undermine our feedback or ratings systems;&nbsp;
                </span>
              </li>
              <li>
                <span nostyle="font-weight: 400;">
                  upload, post, email, transmit or otherwise make available any unsolicited or
                  unauthorized advertising, promotional materials, “junk mail,” “spam,” “chain
                  letters,” “pyramid schemes,” or any other form of solicitation; or&nbsp;
                </span>
              </li>
              <li>
                <span nostyle="font-weight: 400;">
                  use the Incredible Phones Properties to collect, harvest, transmit, distribute,
                  post or submit any information concerning any other person or entity, including
                  without limitation, photographs of others without their permission, personal
                  contact information or credit, debit, calling card or account numbers.&nbsp;
                </span>
              </li>
            </ul>
            <p>
              <span nostyle="font-weight: 400;">
                5.4 By communicating your User Content through the Website, you grant to Incredible
                Phones (but without resulting in any obligation for Incredible) a license that is
                non-exclusive, non-transferrable, sublicensable, and free of charge, worldwide, and
                for the duration of your registration, to use, reproduce, represent, publish, make
                available, communicate, modify, adapt, and display on the Website and on all other
                media (especially on all physical or digital media, in all press releases, press or
                financial release or dossiers, presentation material, promotional and/or advertising
                material), by all means, all or part of your User Content.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                5.5 By communicating your User Content through the Website, you also grant to any
                other User a license that is non-exclusive, personal, non-transferrable,
                nonsublicensable, worldwide, and for the duration of your registration, to reproduce
                and represent your User Content for private non-commercial purposes.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                5.6 You agree to indemnify and hold Incredible, its parents, subsidiaries,
                affiliates, officers, employees, agents, partners and licensors (collectively, the
                “Incredible Phones Parties”) harmless from any losses, costs, liabilities and
                expenses (including reasonable attorneys’ fees) relating to or arising out of: (a)
                your User Content; (b) your use of, or inability to use, the Incredible Properties;
                (c) your violation of this Agreement; (d) your violation of any rights of another
                party, including any Users; or (e) your violation of any applicable laws, rules or
                regulations. Incredible Phones reserves the right, at its own cost, to assume the
                exclusive defense and control of any matter otherwise subject to indemnification by
                you, in which event you will fully cooperate with Incredible Phones in asserting any
                available defenses. You agree that the provisions in this section will survive any
                termination of your account, this Agreement or your access to the Incredible Phones
                Properties.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                5.7 If you become aware that Forbidden Products (as defined in the Section 5.12 of
                this Agreement) are put up for sale on the Website, you agree to inform Incredible
                Phones immediately by email at the following email address: support@
                <span class="skimlinks-unlinked">
                  <a class="vglnk" href="http://incrediblephones.com" rel="nofollow">
                    <span>incrediblephones</span>
                    <span>.</span>
                    <span>com</span>
                  </a>
                </span>
                .&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                5.8 The Services may not be used to solicit for any other business, website or
                services. You may not solicit, advertise for, or contact in any form Users for
                employment, contracting or any purpose not related to the Services facilitated
                through the Incredible Phones Properties. You may not use the Services to collect
                usernames and/or email addresses of Users by electronic or other means without the
                express prior written consent of Incredible.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                5.9 Incredible Phones may, but is not obligated to, monitor or review the Incredible
                Phones Properties and Content at any time. Without limiting the foregoing,
                Incredible Phones shall have the right, in its sole discretion, to remove any of
                your User Content for any reason (or no reason), including if such Content violates
                the Agreement or any applicable law. Although Incredible Phones does not generally
                monitor User activity occurring in connection with the Incredible Phones Properties
                or Content, if Incredible Phones becomes aware of any possible violation by you of
                any provision of this Agreement, Incredible Phones reserves the right to investigate
                such violations, and Incredible Phones may, at its sole discretion, immediately
                terminate your license to use the Incredible Phones Properties, or change, alter or
                remove your User Content, in whole or in part, without prior written notice to
                you.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                5.10 You are solely responsible for your interactions with other Users of the
                Incredible Phones Properties and any other parties with whom you interact through
                the Incredible Phones Properties, provided, however, that Incredible reserves the
                right, but has no obligation, to intercede in such disputes.
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                5.11 The Incredible Phones Properties may contain User Content provided by other
                Users. Incredible Phones is not responsible for and does not control User Content.
                Incredible Phones has no obligation to review or monitor, and does not approve,
                endorse or make any representations or warranties with respect to User Content. You
                use all User Content and interact with other Users at your own risk.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                5.12 Each Member agrees not to list and offer for sale on the Website any Forbidden
                Products, and particularly Products that: contravene applicable laws and
                regulations, are contrary to accepted standards of good behavior, or are not
                authorized by virtue of contractual provisions, and particularly fraudulent Products
                (especially Products that constitute stolen or illegally imported goods), that
                infringe the rights of a third party, such as counterfeit Products as defined in the
                provisions of the Intellectual Property Code, Products sold in breach of selective
                or exclusive distribution networks, or Products that are stolen and/or non-compliant
                with the laws in force relating to health and safety that are applicable to certain
                products (for example toys, electronic equipment, cosmetics and personal hygiene
                products, childcare articles, textile articles, and articles made of leather or
                imitation leather)-live animals; medicines, drugs or articles that may incite the
                use of narcotics and/or; products which may constitute in themselves and/or through
                their presentation an infringement in the instance of the defamatory,
                discriminatory, pornographic, hateful, vulgar, obscene, harmful or otherwise
                objectionable nature of the product or its presentation.&nbsp;
              </span>
            </p>
            <h3>
              <b>6. BUYER OBLIGATIONS&nbsp;</b>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                6.1 Buyer can only create one account on the Website. Creating an account on a
                Website allows him/her to buy Products on the Website.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                6.2 Buyer agrees to comply with all applicable laws and regulations relating to the
                purchase of Products.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                6.3 After having chosen his/her Product(s), the Buyer must check the order details,
                the price, fill in if necessary, depending on the payment means chosen, the
                information allowing the payment of the Product and correct any possible errors
                before confirming the order to express acceptance.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                6.4 From the moment the Buyer places a Product order through the Services on the
                Website, Buyer accepts that his/her surname and first name, as well as the delivery
                address, and phone number, if provided, may be communicated to the Seller.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                6.5 Orders placed by Buyer are independent from one another. If one or more orders
                have not been accepted by the Seller(s) in question, Buyer’s other orders are not
                invalidated and are binding on Buyer. Consequently, the fact that one or several
                Products ordered from one or several Sellers are unavailable does not constitute
                grounds for cancelling all the ordered Products, as Buyer is bound by the order of
                the other available Product(s).&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                6.6 As soon as he/she receives the ordered Products, Buyer will confirm delivery. In
                the absence of product delivery, Buyer will register a complaint. If, at the end of
                21 days beginning from the date of shipping of the order, no confirmation or
                complaint has been received, the transaction is deemed to be completed. The
                transaction is also deemed to be completed from the moment Buyer confirms it.&nbsp;
              </span>
            </p>
            <h3>
              <b>7. SALE PROCESS&nbsp;</b>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                7.1 To buy a Product on the Website, you must either already be registered as a
                Member, or register as a Member at the time of purchase.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                7.2 The payment means available on the Website are the following: payment through
                the payment system set up by Incredible Phones and its payment partners, in
                particular Stripe and Paypal. The sale is concluded between the Buyer and the Seller
                immediately on finalization by the Buyer of his/her order on the Website, provided,
                in the case of payment by bank card through the secure Credit card payment system
                set up by Stripe, that the Seller confirms within one business day the possibility
                of fulfilling the order. In this case, if the time span has elapsed without
                confirmation, the order may be cancelled by Incredible Phones and the Buyer refunded
                its purchase price.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                7.3 As soon as Buyer has finalized his/her order on the Website, an order
                confirmation will be sent to Buyer and the Seller. Buyer will then see, depending on
                his/her banking institution, the total amount of his/her order debited within a
                maximum of seven (7) days. Notwithstanding the foregoing, Buyer agrees that the time
                periods mentioned above may be delayed due to security checks performed by
                Incredible Phones payment partners.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                7.4 The Products available on the Website can be shipped to the 48 contiguous states
                of the United States, plus Alaska and Hawaii. Buyer’s contact details will be
                available to the Seller once the payment has been made. As soon as the Seller is
                ready to ship the order, he/she must inform Incredible Phones and Buyer accordingly,
                indicating to the latter the shipping date or the estimated time for delivery. If
                the order is shipped as a recorded delivery parcel, the Seller also agrees to
                indicate the tracking number on his/her account. The ordered Products are shipped
                and delivered at the expense and risk of the Seller.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                7.5 When shipping the order, the Seller must confirm correct shipping by providing
                the parcel tracking number corresponding to the order to the Buyer.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                7.6 The order can be tracked at any time on the Website by Buyer, through his/her
                account. Once the order has been delivered, Buyer can rate the transaction through
                the review system available on the Website.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                7.7 The security of the transactions is ensured by Stripe (payment institution) or
                if applicable by Paypal, which means purchases can be made safely through secure
                connections (https). The Buyer’s bank details are encrypted by means of the SSL
                protocol. Incredible Phones does not therefore store the Buyer’s bank details at any
                time. In case of change or additional payment partners, Incredible Phones will
                obtain a commitment from such payment partners to comply with the above security
                standards.&nbsp;
              </span>
            </p>
            <h3>
              <b>8. COMPLAINTS, RIGHT TO WITHDRAW AND DISCLAIMER OF WARRANTY COMPLAINTS&nbsp;</b>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                8.1 Complaints by Buyer should be sent directly to the Seller through his/her
                account on the Website. The Buyer and the Seller will each be able to follow the
                procedure from their respective accounts and they will have access to an alert
                system in their order tracking. The transaction is deemed closed once Buyer confirms
                reception of the order through his/her account. Failing that, the transaction is
                deemed closed 21 days after the Seller has shipped the order, unless Buyer has sent
                a complaint.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                8.2 The Buyer and the Seller will make every effort to achieve a satisfactory
                resolution of the complaint.
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                8.3 In case of a complaint linked to a non-compliant Product or to the exercise of
                the right of withdrawal, Buyer agrees to return the Product to the Seller. In
                particular, Buyer agrees to pack the Product appropriately and to take all the usual
                precautions for its transport as well as remove all personal data and
                passcodes.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                8.4 A Buyer having paid for a Product by bank card via the Website’s secure payment
                system will be able to receive a refund via the same means of payment, or if not
                possible, on his/her Virtual Wallet, provided that the credit of the Seller’s Wallet
                is sufficient. In case of a refund on the Virtual Wallet, the Buyer will be able to
                use his/her Virtual Wallet to make other purchases on the Website or may at any time
                request that the amount on his/her Virtual Wallet be transferred to his/her bank
                account. In this case, he/she must make a bank transfer request and communicate to
                Incredible Phones his/her bank details.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                8.5 YOU EXPRESSLY UNDERSTAND AND AGREE THAT TO THE EXTENT PERMITTED BY APPLICABLE
                LAW, YOUR USE OF THE Incredible Phones PROPERTIES IS AT YOUR SOLE RISK, AND THE
                INCREDIBLE PHONES PROPERTIES ARE PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS,
                WITH ALL FAULTS. COMPANY PARTIES EXPRESSLY DISCLAIM ALL WARRANTIES, REPRESENTATIONS,
                AND CONDITIONS OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED
                TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A
                PARTICULAR PURPOSE AND NON-INFRINGEMENT.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                8.6 THE Incredible Phones PARTIES MAKE NO WARRANTY, REPRESENTATION OR CONDITION
                THAT: (1) THE Incredible Phones PROPERTIES WILL MEET YOUR REQUIREMENTS; (2) YOUR USE
                OF THE INCREDIBLE PHONES PROPERTIES WILL BE UNINTERRUPTED, TIMELY, SECURE OR
                ERROR-FREE; (3) THE RESULTS THAT MAY BE OBTAINED FROM THE USE OF THE Incredible
                Phones PROPERTIES WILL BE ACCURATE OR RELIABLE; OR (4) ANY ERRORS IN THE Incredible
                Phones PROPERTIES WILL BE CORRECTED.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                8.7 ANY CONTENT DOWNLOADED FROM OR OTHERWISE ACCESSED THROUGH THE Incredible
                PROPERTIES IS ACCESSED AT YOUR OWN RISK, AND YOU SHALL BE SOLELY RESPONSIBLE FOR ANY
                DAMAGE TO YOUR PROPERTY OR PERSON, INCLUDING, BUT NOT LIMITED TO, YOUR COMPUTER
                SYSTEM AND ANY DEVICE YOU USE TO ACCESS THE Incredible Phones PROPERTIES, OR ANY
                OTHER LOSS THAT RESULTS FROM ACCESSING SUCH CONTENT.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                8.8 THE SERVICES MAY BE SUBJECT TO DELAYS, CANCELLATIONS AND OTHER DISRUPTIONS.
                Incredible Phones MAKES NO WARRANTY, REPRESENTATION OR CONDITION WITH RESPECT TO
                SERVICES, INCLUDING BUT NOT LIMITED TO, THE QUALITY, EFFECTIVENESS, REPUTATION AND
                OTHER CHARACTERISTICS OF SERVICES.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                8.9 NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED FROM Incredible
                Phones OR THROUGH THE Incredible PROPERTIES WILL CREATE ANY WARRANTY NOT EXPRESSLY
                MADE HEREIN.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                8.10 FROM TIME TO TIME, Incredible Phones MAY OFFER NEW “BETA” FEATURES OR TOOLS
                WITH WHICH ITS USERS MAY EXPERIMENT. SUCH FEATURES OR TOOLS ARE OFFERED SOLELY FOR
                EXPERIMENTAL PURPOSES AND WITHOUT ANY WARRANTY OF ANY KIND, AND MAY BE MODIFIED OR
                DISCONTINUED AT Incredible’S SOLE DISCRETION. THE PROVISIONS OF THIS SECTION APPLY
                WITH FULL FORCE TO SUCH FEATURES OR TOOLS.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                8.11 YOU ACKNOWLEDGE AND AGREE THAT THE Incredible Phones PARTIES ARE NOT LIABLE,
                AND YOU AGREE NOT TO SEEK TO HOLD THE Incredible Phones PARTIES LIABLE, FOR THE
                CONDUCT OF THIRD PARTIES, INCLUDING OPERATORS OF EXTERNAL SITES, AND THAT THE RISK
                OF INJURY FROM SUCH THIRD PARTIES RESTS ENTIRELY WITH YOU.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                8.12 Incredible Phones makes no warranty that the Products provided by Sellers will
                meet your requirements or be available on an uninterrupted, secure, or error-free
                basis. Incredible Phones makes no warranty regarding the quality of any such goods,
                or the accuracy, timeliness, truthfulness, completeness or reliability of any User
                Content obtained through the Incredible Phones Properties. We are not involved in
                the actual transaction between Buyers and Sellers. While we may help facilitate the
                resolution of disputes through various programs, we have no control over and do not
                guarantee the quality, safety or legality of items advertised, the truth or accuracy
                of Users’ content or listings, the ability of Sellers to sell items, the ability of
                Buyers to pay for item, or that Buyer or Seller will actually complete a transaction
                or return all items.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                8.13 Incredible Phones does not transfer legal ownership of items from the Seller to
                the Buyer.
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                N.Y. U.C.C. §2-401(2) and Uniform Commercial Code § 2-401(2) apply to the transfer
                of ownership between the Buyer and the Seller, unless the Buyer and the Seller agree
                otherwise. Further, we cannot guarantee continuous or secure access to the
                Incredible Phones Properties and operation of the Incredible Properties may be
                interfered with by numerous factors outside of our control. Accordingly, to the
                extent legally permitted, we exclude all implied warranties, terms and
                conditions.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                8.14 YOU ARE SOLELY RESPONSIBLE FOR ALL OF YOUR COMMUNICATIONS AND INTERACTIONS WITH
                OTHER USERS OF THE Incredible Phones PROPERTIES. YOU UNDERSTAND THAT INCREDIBLE
                PHONES DOES NOT MAKE ANY ATTEMPT TO VERIFY THE STATEMENTS OF USERS OF THE Incredible
                PROPERTIES. INCREDIBLE PHONES MAKES NO REPRESENTATIONS OR WARRANTIES AS TO THE
                CONDUCT OF USERS OF THE Incredible Phones PROPERTIES. YOU AGREE TO TAKE REASONABLE
                PRECAUTIONS IN ALL COMMUNICATIONS AND INTERACTIONS WITH OTHER USERS OF THE
                Incredible PROPERTIES.&nbsp;
              </span>
            </p>
            <h3>
              <b>9. THIRD PARTY SERVICES</b>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                The Incredible Phones Properties may contain links to third-party websites
                (“Third-Party Websites”). When you click on a link to a Third-Party Website, we will
                not warn you that you have left the Incredible Phones Properties and are subject to
                the terms and conditions (including privacy policies) of another website or
                destination. Such Third-Party Websites are not under the control of Incredible.
                Incredible Phones is not responsible for any Third-Party Websites.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                Incredible Phones provides these Third-Party Websites only as a convenience and does
                not review, approve, monitor, endorse, warrant, or make any representations with
                respect to Third-Party Websites, or their products or services.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                You use all links in Third-Party Websites at your own risk. When you leave our
                Website, our Agreement and policies no longer govern. You should review applicable
                terms and policies, including privacy and data gathering practices, of any
                Third-Party Websites, and should make whatever investigation you feel necessary or
                appropriate before proceeding with any transaction with any third party.&nbsp;
              </span>
            </p>
            <h3>
              <b>10. OWNERSHIP&nbsp;</b>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                10.1 Except with respect to User Content, including your User Content, you agree
                that Incredible Phones and its suppliers own all rights, title and interest in the
                Incredible Phones Properties. You will not remove, alter or obscure any copyright,
                trademark service mark or other proprietary rights notices incorporated in or
                accompanying the Incredible Phones Properties.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                10.2 Any person publishing a Website and wishing to create a direct hypertext link
                to any of the Websites must request in writing authorization from Incredible. The
                Incredible Phones authorization will, under no circumstances, be granted
                definitively. The link must be deleted when this is requested by Incredible&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                10.3 Hypertext links to the Website which use techniques such as framing or
                insertion by hypertext links (in-line linking) are strictly forbidden.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                10.4 Except with respect to your User Content, you agree that you have no right or
                title in or to any Content that appears on the Incredible Phones Properties.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                10.5 Incredible Phones does not claim ownership of your User Content. However, when
                you as a User post or publish User Content on or in the Incredible Phones
                Properties, you represent that you own and/or have a royalty-free, perpetual,
                irrevocable, worldwide nonexclusive right (including any moral rights) and license
                to use, license, reproduce, modify, adapt, publish, translate, create derivative
                works from, distribute, derive revenue or other remuneration from, and communicate
                to the public, perform and display your User Content (in whole or in part) worldwide
                and/or to incorporate it in other works in any form, media or technology now known
                or later developed for the full term of any worldwide intellectual property right
                that may exist in your User Content.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                10.6 You agree that the submission of any ideas, suggestions, documents, and/or
                proposals to Incredible Phones through its suggestion, feedback, wiki, forum or
                similar pages (“Feedback”) is at your own risk and that Incredible Phones has no
                obligations (including without limitation obligations of confidentiality) with
                respect to such Feedback. You represent and warrant that you have all rights
                necessary to submit the Feedback. You hereby grant to Incredible Phones a fully
                paid, royalty-free, perpetual, irrevocable, worldwide, non-exclusive, and fully
                sublicensable right and license to use, reproduce, perform, display, distribute,
                adapt, modify, re-format, create derivative works of, and otherwise commercially or
                non-commercially exploit in any manner, any and all Feedback, and to sublicense the
                foregoing rights, in connection with the operation and maintenance of the Incredible
                Phones Properties.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                10.7 The Services host User generated Content that is related to reviews of certain
                Users. Such reviews are opinions and are not the opinion of Incredible, have not
                been verified by Incredible, and each User should undertake his or her own research
                to be satisfied concerning any specific User. You agree that Incredible Phones is
                not liable for any User generated Content.
              </span>
            </p>
            <h3>
              <b>11. COMPLIANCE WITH THE LAW&nbsp;</b>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                11.1 Each Member agrees to comply with the laws, regulations and standards of all
                kinds that are in force relating to the use of the Services offered through the
                Website, the putting up for sale, the purchase, the soliciting of offers to sell and
                the sale of Products.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                11.2 In the case of manifest fraud (fraudulent copy, receiving stolen goods, illegal
                importation, etc.) or on demand, Incredible Phones will forward all the necessary
                information, including nominative information, to the relevant services in charge of
                the repression of such fraud and infractions.&nbsp;
              </span>
            </p>
            <h3>
              <b>12. DURATION AND TERMINATION OF THE AGREEMENT&nbsp;</b>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                12.1 This Agreement will continue for an indefinite period of time and can be
                terminated at any time by you or by Incredible, without particular reason, by
                notification which can be given by email or through the Services. Any termination
                will take effect at the end of the month following the one during which the
                notification was sent.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                12.2 Without excluding other legal remedies, Incredible Phones reserves the right to
                issue a warning, to temporarily or permanently suspend Buyer’s registration, to end
                Buyer’s registration immediately and/or to stop providing its Services to Buyer in
                the following cases:&nbsp;
              </span>
            </p>
            <ul>
              <li>
                <span nostyle="font-weight: 400;">
                  in case of breach of the present Agreement, including the documents which are
                  incorporated by reference;&nbsp;
                </span>
              </li>
              <li>
                <span nostyle="font-weight: 400;">
                  if Incredible Phones is unable to verify or authenticate the information provided,
                  and Buyer does not respond to verification and/or authentication requests;&nbsp;
                </span>
              </li>
              <li>
                <span nostyle="font-weight: 400;">
                  if the actions of Buyer could engage the responsibility of Incredible, that of
                  Buyer, or that of any other Member.&nbsp;
                </span>
              </li>
            </ul>
            <p>
              <span nostyle="font-weight: 400;">
                12.3 In case of termination, Buyer’s account will be deactivated. Buyer will have to
                request the transfer to his/her account of the remaining balance which might be
                present in his/her Virtual Wallet before the effective date of termination. In such
                case, Buyer agrees to provide to Incredible Phones the documents required by the
                payment service provider for anti-money laundering purposes.&nbsp;
              </span>
            </p>
            <h3>
              <b>13. ACCOUNT REMOVAL&nbsp;</b>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                13.1 If Buyer wishes to remove his/her account, he/she must send his/her request to
                the address:{' '}
              </span>
              <a href="mailto:support@incrediblephones.com">
                <span nostyle="font-weight: 400;">support@incrediblephones.com</span>
              </a>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                13.2 If the Virtual Wallet of a Buyer who wishes to remove his/her account has a
                positive balance, the Buyer must request the transfer of this amount to his/her bank
                account when he/she makes the account removal request.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                13.3 Incredible Phones will remove the account of Buyer at Buyer’s request, subject
                to the conditions that all orders have been honored; that claims, exercises of the
                right of withdrawal; and refunds have been processed; and that there are no other
                ongoing operations and/or warranties.&nbsp;
              </span>
            </p>
            <h3>
              <b>14. LIMITATION OF LIABILITY&nbsp;</b>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                14.1 Disclaimer of Certain Damages. YOU UNDERSTAND AND AGREE THAT IN NO EVENT SHALL
                Incredible Phones PARTIES BE LIABLE FOR ANY INDIRECT, INCIDENTAL, SPECIAL,
                EXEMPLARY, OR CONSEQUENTIAL DAMAGES ARISING OUT OF OR IN CONNECTION WITH THE
                Incredible PROPERTIES, OR ANY DAMAGES RESULTING FROM LOSS OF USE, DATA, OR PROFITS,
                WHETHER OR NOT Incredible Phones HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
                DAMAGES, OR FOR ANY DAMAGES FOR PERSONAL OR BODILY INJURY OR EMOTIONAL DISTRESS
                ARISING OUT OF OR IN CONNECTION WITH THE TERMS, OR FROM ANY COMMUNICATIONS,
                INTERACTIONS OR MEETINGS WITH OTHER USERS OF THE Incredible Phones PROPERTIES, ON
                ANY THEORY OF LIABILITY, RESULTING FROM: (1) THE USE OR INABILITY TO USE THE
                Incredible Phones PROPERTIES; (2) THE COST OF PROCUREMENT OF SUBSTITUTE GOODS OR
                SERVICES RESULTING FROM ANY GOODS, DATA, INFORMATION OR SERVICES PURCHASED OR
                OBTAINED OR MESSAGES RECEIVED FOR TRANSACTIONS ENTERED INTO THROUGH THE Incredible
                Phones PROPERTIES; (3) UNAUTHORIZED ACCESS TO OR ALTERATION OF YOUR TRANSMISSIONS OR
                DATA; (4) STATEMENTS OR CONDUCT OF ANY THIRD PARTY ON THE Incredible Phones
                PROPERTIES; OR (5) ANY OTHER MATTER RELATED TO THE Incredible Phones PROPERTIES,
                WHETHER BASED ON WARRANTY, COPYRIGHT, CONTRACT, TORT (INCLUDING NEGLIGENCE), PRODUCT
                LIABILITY OR ANY OTHER LEGAL THEORY.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                14.2 Cap on Liability. UNDER NO CIRCUMSTANCES WILL Incredible’S LIABILITY HEREUNDER
                EXCEED THE HIGHEST OF THE FOLLOWING AMOUNTS: (i) IN THE CASE OF A CLAIM BY A SELLER,
                THE TOTAL AMOUNT OF SALES FEES PAID BY THAT SELLER TO Incredible Phones IN THE 12
                MONTH PERIOD PRECEDING THE DATE OF THE CLAIM; AND (ii) $175.00.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                14.3 Exclusion of Damages. CERTAIN JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR
                LIMITATION OF CERTAIN DAMAGES. IF THESE LAWS APPLY TO YOU, SOME OR ALL OF THE ABOVE
                EXCLUSIONS OR LIMITATIONS MAY NOT APPLY TO YOU, AND YOU MIGHT HAVE ADDITIONAL
                RIGHTS.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                14.4 User Content. Incredible Phones PARTIES ASSUME NO RESPONSIBILITY FOR THE
                TIMELINESS, DELETION, MIS-DELIVERY OR FAILURE TO STORE ANY CONTENT (INCLUDING, BUT
                NOT LIMITED TO, YOUR CONTENT AND USER CONTENT), USER COMMUNICATIONS OR
                PERSONALIZATION SETTINGS.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                14.5 Basis of the Bargain. THE LIMITATIONS OF DAMAGES SET FORTH ABOVE ARE
                FUNDAMENTAL ELEMENTS OF THE BASIS OF THE BARGAIN BETWEEN COMPANY AND YOU.&nbsp;
              </span>
            </p>
            <h3>
              <b>15. PROCEDURE FOR MAKING CLAIMS OF INFRINGEMENT&nbsp;</b>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                It is Incredible’s policy to terminate membership privileges of any User who
                repeatedly infringes copyright upon prompt notification to Incredible Phones by the
                copyright owner or the copyright owner’s legal agent. Without limiting the
                foregoing, if you believe that your work has been copied and posted on the
                Incredible Phones Properties in a way that constitutes copyright infringement,
                please provide our Copyright Agent with the following information: (1) an electronic
                or physical signature of the person authorized to act on behalf of the owner of the
                copyright interest; (2) a description of the copyrighted work that you claim has
                been infringed; (3) a description of the location on the Incredible Phones
                Properties of the material that you claim is infringing; (4) your address, telephone
                number and e-mail address; (5) a written statement by you that you have a good faith
                belief that the disputed use is not authorized by the copyright owner, its agent or
                the law; and (6) a statement by you, made under penalty of perjury, that the above
                information in your notice is accurate and that you are the copyright owner or
                authorized to act on the copyright owner’s behalf. Contact information for
                Incredible’s Copyright Agent for notice of claims of copyright infringement is as
                follows: Copyright Department, Incredible, 2820 Howard CMNS, #309, Howard, WI
                54313.&nbsp;
              </span>
            </p>
            <h3>
              <b>16. GENERAL PROVISIONS&nbsp;</b>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                16.1 The relationship between Incredible Phones and its Members is solely that of
                independent contractors. Nothing in this Agreement creates any agency, joint
                venture, partnership or other form of joint enterprise, employment or fiduciary
                relationship between Incredible Phones and Buyer. Neither Incredible on the one
                hand, and each Member on the other hand, has any express or implied right or
                authority to assume or create any obligations on behalf of or in the name of the
                other party, or to bind the other party to any contract, agreement or undertaking
                with any third party.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                16.2 Incredible Phones reserves the right at any time and at its sole discretion to
                suspend or interrupt access to the Website and/or operation of the Website, fully or
                partly, particularly for maintenance interventions, operational necessities,
                internal choices or in case of emergency. Incredible Phones also reserves the right
                at any time and at its sole discretion to remove or modify any Content, including
                but not limited to, for technical, commercial or practical reasons. It is agreed
                that such interventions shall not give rise to any compensation or damages claims
                for the benefit of a Member or a User of the Website.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                16.3 Any notification addressed to Incredible Phones but intended for another Member
                will normally be sent by email to the address which was provided by the Member to
                Incredible Phones when the Member registered. The notifications are deemed to have
                been received by the Member 24 hours after the sending of the email, unless the
                sender is notified of the invalidity of the email address. The notifications may
                also be sent to the Member by registered letter with proof of delivery at the
                address provided on registration. The time span of 24 hours mentioned above is then
                extended to 3 days after the sending of the letter.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                16.4 Electronic Communications. The communications between you and Incredible use
                electronic means, whether you visit the Incredible Phones Properties or send
                Incredible Phones e-mails, or whether Incredible posts notices on the Incredible
                Phones Properties or communicates with you via e-mail. For contractual purposes, you
                (1) consent to receive communications from Incredible Phones in an electronic form;
                and (2) agree that all terms and conditions, agreements, notices, disclosures, and
                other communications that Incredible Phones provides to you electronically satisfy
                any legal requirement that such communications would satisfy if it were to be in
                writing. The foregoing does not affect your statutory rights. Including but not
                limited to your rights under the Electronic Signatures in Global and National
                Commerce Act, 15 U.S.C. §7001 et seq.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                16.5 Release. You hereby release Incredible Phones Parties and their successors from
                claims, demands, any and all losses, damages, rights, and actions of any kind,
                including personal injuries, death, and property damage, that is either directly or
                indirectly related to or arises from your use of the Incredible Phones Properties,
                including but not limited to, any interactions with or conduct of other Users or
                third-party websites of any kind arising in connection with or as a result of the
                Terms or your use of the Incredible Phones Properties. If you are a California
                resident, you hereby waive California Civil Code Section 1542, which states, “A
                general release does not extend to claims which the creditor does not know or
                suspect to exist in his favor at the time of executing the release, which if known
                by him must have materially affected his settlement with the debtor.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                16.6 Assignment. The Agreement, and your rights and obligations hereunder, may not
                be assigned, subcontracted, delegated or otherwise transferred by you without
                Incredible’s prior written consent, and any attempted assignment, subcontract,
                delegation, or transfer in violation of the foregoing will be null and void.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                16.7 Force Majeure. Incredible Phones shall not be liable for any delay or failure
                to perform resulting from causes outside its reasonable control, including, but not
                limited to, acts of God, war, terrorism, riots, embargos, acts of civil or military
                authorities, fire, floods, accidents, strikes or shortages of transportation
                facilities, fuel, energy, labor or materials.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                16.8 Questions, Complaints, Claims. If you have any questions, complaints or claims
                with respect to the Incredible Phones Properties, please contact us at: support@
                <span class="skimlinks-unlinked">
                  <a class="vglnk" href="http://incrediblephones.com" rel="nofollow">
                    <span>incrediblephones</span>
                    <span>.</span>
                    <span>com</span>
                  </a>
                </span>
                . We will do our best to address your concerns. If you feel that your concerns have
                been addressed incompletely, we invite you to let us know for further
                investigation.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">16.9 Dispute Resolution.&nbsp;</span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                Applicability of Arbitration Agreement. All claims and disputes (excluding claims
                for injunctive or other equitable relief as set forth below) in connection with the
                Agreement or the use of any product or service provided by Incredible Phones that
                cannot be resolved informally or in small claims court shall be resolved by binding
                arbitration on an individual basis under the terms of this Arbitration Agreement.
                Unless otherwise agreed, all arbitration proceedings will be held in English. This
                Arbitration Agreement applies to you and Incredible, and to any subsidiaries,
                affiliates, agents, employees, predecessors in interest, successors, and assigns, as
                well as all authorized or unauthorized users or beneficiaries of services or goods
                provided under the Agreement.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                Notice Requirement and Informal Dispute Resolution. Before either party may seek
                arbitration, the party must first send to the other party a written Notice of
                Dispute (“Notice”) describing the nature and basis of the claim or dispute, and the
                requested relief. A Notice to Incredible Phones should be sent to: Legal Department,
                Incredible, 2820 Howard CMNS, #309, Howard, WI 54313. After the Notice is received,
                you and Incredible Phones may attempt to resolve the claim or dispute informally. If
                you and Incredible Phones do not resolve the claim or dispute within thirty (30)
                days after the Notice is received, either party may begin an arbitration proceeding.
                The amount of any settlement offer made by any party may not be disclosed to the
                arbitrator until after the arbitrator has determined the amount of the award, if
                any, to which either party is entitled.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                Arbitration Rules. Arbitration shall be initiated through the American Arbitration
                Association (“AAA”), an established alternative dispute resolution provider (“ADR
                Provider”) that offers arbitration as set forth in this section. If AAA is not
                available to arbitrate, the parties shall agree to select an alternative ADR
                Provider. The rules of the ADR Provider shall govern all aspects of the arbitration,
                including but not limited to the method of initiating and/or demanding arbitration,
                except to the extent such rules are in conflict with the Agreement. The AAA Consumer
                Arbitration Rules governing the arbitration are available online at{' '}
                <span class="skimlinks-unlinked">
                  <a class="vglnk" href="http://www.adr.org" rel="nofollow">
                    <span>www</span>
                    <span>.</span>
                    <span>adr</span>
                    <span>.</span>
                    <span>org</span>
                  </a>
                </span>{' '}
                or by calling the AAA at 1-800-778-7879. The arbitration shall be conducted by a
                single, neutral arbitrator. Any claims or disputes where the total amount of the
                award sought is less than Ten Thousand U.S. Dollars (US $10,000.00) may be resolved
                through binding non-appearance-based arbitration, at the option of the party seeking
                relief. For claims or disputes where the total amount of the award sought is Ten
                Thousand U.S. Dollars (US $10,000.00) or more, the right to a hearing will be
                determined by the Arbitration Rules. Any hearing will be held in a location within
                100 miles of your residence, unless you reside outside of the United States, and
                unless the parties agree otherwise. If you reside outside of the U.S., the
                arbitrator shall give the parties reasonable notice of the date, time and place of
                any oral hearings. Any judgment on the award rendered by the arbitrator may be
                entered in any court of competent jurisdiction. If the arbitrator grants you an
                award that is greater than the last settlement offer that Incredible Phones made to
                you prior to the initiation of arbitration, Incredible Phones will pay you the
                greater of the award or $50,000 (USD). Each party shall bear its own costs
                (including attorney’s fees) and disbursements arising out of the arbitration and
                shall pay an equal share of the fees and costs of the ADR Provider.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                Additional Rules for Non-Appearance Based Arbitration. If non-appearance based
                arbitration is elected, the arbitration shall be conducted by telephone, online
                and/or based solely on written submissions; the specific manner shall be chosen by
                the party initiating the arbitration. The arbitration shall not involve any personal
                appearance by the parties or witnesses unless otherwise agreed by the parties.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                Time Limits. If you or Incredible Phones pursue arbitration, the arbitration action
                must be initiated and/or demanded within the statute of limitations (i.e., the legal
                deadline for filing a claim) and within any deadline imposed under the AAA Rules for
                the pertinent claim.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                Authority of Arbitrator. If arbitration is initiated, the arbitrator will decide the
                rights and liabilities, if any, of you and Incredible, and the dispute will not be
                consolidated with any other matters or joined with any other cases or parties. The
                arbitrator shall have the authority to grant motions dispositive of all or part of
                any claim. The arbitrator shall have the authority to award monetary damages, and to
                grant any non-monetary remedy or relief available to an individual under applicable
                law, the AAA Rules, and the Agreement. The arbitrator shall issue a written award
                and statement of decision describing the essential findings and conclusions on which
                the award is based, including the calculation of any damages awarded. The arbitrator
                has the same authority to award relief on an individual basis that a judge in a
                court of law would have. The award of the arbitrator is final and binding upon you
                and Incredible.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                Waiver of Jury Trial. THE PARTIES HEREBY WAIVE THEIR CONSTITUTIONAL AND STATUTORY
                RIGHTS TO GO TO COURT AND HAVE A TRIAL IN FRONT OF A JUDGE OR A JURY, instead
                electing that all claims and disputes shall be resolved by arbitration under this
                Arbitration Agreement. Arbitration procedures are typically more limited, more
                efficient and less costly than rules applicable in a court and are subject to very
                limited review by a court. In the event any litigation should arise between you and
                Incredible Phones in any state or federal court in a suit to vacate or enforce an
                arbitration award or otherwise, YOU AND Incredible Phones WAIVE ALL RIGHTS TO A JURY
                TRIAL, instead electing that the dispute be resolved by a judge.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                Waiver of Class or Consolidated Actions. ALL CLAIMS AND DISPUTES WITHIN THE SCOPE OF
                THIS ARBITRATION AGREEMENT MUST BE ARBITRATED OR LITIGATED ON AN INDIVIDUAL BASIS
                AND NOT ON A CLASS BASIS, AND CLAIMS OF MORE THAN ONE CUSTOMER OR USER CANNOT BE
                ARBITRATED OR LITIGATED JOINTLY OR CONSOLIDATED WITH THOSE OF ANY OTHER CUSTOMER OR
                USER.
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                Confidentiality. All aspects of the arbitration proceeding, including but not
                limited to the award of the arbitrator and compliance therewith, shall be strictly
                confidential. The parties agree to maintain confidentiality unless otherwise
                required by law. This paragraph shall not prevent a party from submitting to a court
                of law any information necessary to enforce this Agreement, to enforce an
                arbitration award, or to seek injunctive or equitable relief.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                Severability. If any part or parts of this Arbitration Agreement are found under the
                law to be invalid or unenforceable by a court of competent jurisdiction, then such
                specific part or parts shall be of no force and effect and shall be severed and the
                remainder of the Agreement shall continue in full force and effect.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                Right to Waive. Any or all of the rights and limitations set forth in this
                Arbitration Agreement may be waived by the party against whom the claim is asserted.
                Such waiver shall not waive or affect any other portion of this Arbitration
                Agreement.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                Survival of Agreement. This Arbitration Agreement will survive the termination of
                your relationship with Incredible.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                Small Claims Court. Notwithstanding the foregoing, either you or Incredible may
                bring an individual action in small claims court.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                Emergency Equitable Relief . Notwithstanding the foregoing, either party may seek
                emergency equitable relief before a state or federal court in order to maintain the
                status quo pending arbitration. A request for interim measures shall not be deemed a
                waiver of any other rights or obligations under this Arbitration Agreement.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                Claims Not Subject to Arbitration. Notwithstanding the foregoing, claims of
                defamation, violation of the Computer Fraud and Abuse Act, and infringement or
                misappropriation of the other party’s patent, copyright, trademark or trade secrets
                shall not be subject to this Arbitration Agreement. Courts. In any circumstances
                where the foregoing Arbitration Agreement permits the parties to litigate in court,
                the parties hereby agree to submit to the personal jurisdiction of the courts
                located within Alameda County, CA, for such purpose.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                16.10 Governing Law. These Terms of Service will be construed in accordance with and
                governed exclusively by the laws of the State of California applicable to agreements
                made among California residents and to be performed wholly within such jurisdiction,
                regardless of such parties’ actual domiciles.
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                16.11 Notice. Where Incredible Phones requires that you provide an e-mail address,
                you are responsible for providing Incredible Phones with your most current e-mail
                address. In the event that the last e-mail address you provided to Incredible is not
                valid, or for any reason is not capable of delivering to you any notices required/
                permitted by the Agreement, Incredible’s dispatch of the e-mail containing such
                notice will nonetheless constitute effective notice. You may give notice to
                Incredible Phones at the following address:{' '}
                <a class="vglnk" href="mailto:support@incrediblephones.com" rel="nofollow">
                  support@incrediblephones.com
                </a>
                . Such notice shall be deemed given when received by Incredible Phones by letter
                delivered by nationally recognized overnight delivery service or first class postage
                prepaid mail at the above address.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                16.12 Waiver. Any waiver or failure to enforce any provision of the Agreement on one
                occasion will not be deemed a waiver of any other provision or of such provision on
                any other occasion.
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                16.13 Severability. If any provision of the Agreement is, for any reason, held to be
                invalid or unenforceable, the other provisions of the Agreement will remain
                enforceable, and the invalid or unenforceable provision will be deemed modified so
                that it is valid and enforceable to the maximum extent permitted by law.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                16.14 Export Control. You may not use, export, import, or transfer the Incredible
                Phones Properties except as authorized by U.S. law, the laws of the jurisdiction in
                which you obtained the Incredible Phones Properties, and any other applicable laws.
                In particular, but without limitation, the Incredible Properties may not be exported
                or re-exported (a) into any United States embargoed countries, or (b) to anyone on
                the U.S. Treasury Department’s list of Specially Designated Nationals or the U.S.
                Department of Commerce’s Denied Person’s List or Entity List. By using the
                Incredible Phones Properties, you represent and warrant that (i) you are not located
                in a country that is subject to a U.S. Government embargo, or that has been
                designated by the U.S. Government as a “terrorist supporting” country and (ii) you
                are not listed on any U.S. Government list of prohibited or restricted parties. You
                also will not use the Incredible Properties for any purpose prohibited by U.S. law,
                including the development, design, manufacture or production of missiles, nuclear,
                chemical or biological weapons. You acknowledge and agree that products, services or
                technology provided by Incredible Phones are subject to the export control laws and
                regulations of the United States. You shall comply with these laws and regulations
                and shall not, without prior U.S. government authorization, export, re-export, or
                transfer Incredible Phones products, services or technology, either directly or
                indirectly, to any country in violation of such laws and regulations.
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                16.15 Entire Agreement. The Agreement is the final, complete and exclusive agreement
                of the parties with respect to the subject matter hereof and supersedes and merges
                all prior discussions between the parties with respect to such subject matter.
              </span>
            </p>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Footer {...props} />
    </div>
  );
}

IndexView.getInitialProps = async ({req, reduxStore, dispatch, query, res}) => {};

export default withAppConsumer(WithPublicContext(withI18next(['home', 'common'])(IndexView)));

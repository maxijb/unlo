import React, {useState} from 'react';
import Link from 'next/link';
import Router from 'next/router';

import HighlightedProducts from '@Components/product/highlighted-products';
import {Grid, Button} from 'semantic-ui-react';
import Logger from '@Common/logger';

import {withI18next} from '../../shared/i18n/with-i18n.js';

import Highlights from '@Components/display/highlights';
import Header from '../../shared/components/header/header';

import TagLink from '@Components/display/tag-link';
import WithPublicContext from '@Components/hocs/with-public-context';
import {withAppConsumer} from '../../shared/components/app-context';
import {getPageLink} from '@Common/utils/urls';
import classnames from 'classnames';

import {Passed, EarnIcon} from '../../widget/components/display/svg-icons';
import Footer from '@Components/footer/footer';

import styles from '@Common/styles/styles.scss';

function IndexView(props) {
  const {t, app, actions, currentOrder, user, query, api} = props;
  const [isSignin, setIsSignin] = useState(false);
  const [isOrderDetails, setIsOrderDetails] = useState(false);
  return (
    <div>
      <Header {...props} />

      <Grid columns={16} className={classnames(styles.MainGrid)}>
        <Grid.Row>
          <Grid.Column width={16}>
            <div className={classnames(styles.Huge, styles.MarginTop, styles.MarginDoubleBottom)}>
              {t('heyThereName', {name: user.displayName})}
            </div>
          </Grid.Column>
        </Grid.Row>

        <Grid.Row>
          <Grid.Column width={16}>
            <Highlights
              highlights={[
                {
                  name: t('viewMyOrders'),
                  icon: <img src="/static/incredible/invoice.png" alt="invoice" />,
                  onClick: () => Router.push('/user/orders'),
                  selected: isOrderDetails,
                },
                {
                  name: t('earnCashOldPhone'),
                  icon: <EarnIcon />,
                  onClick: () => {
                    setIsOrderDetails(false);
                    actions.setIsOpenTradeIn(true);
                  },
                },
              ]}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={16}>
            <TagLink href="/contact" text={t('QuestionsContactusanytime')} />
            <TagLink
              onClick={() => api.user.logout().then(() => Router.push('/'))}
              text={t('logout')}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Footer {...props} />
    </div>
  );
}

IndexView.getInitialProps = async ({req, reduxStore, dispatch, query, res}) => {};

export default withAppConsumer(WithPublicContext(withI18next(['home', 'common'])(IndexView)));

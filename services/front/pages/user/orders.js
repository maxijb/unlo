import React, {useEffect, useState} from 'react';
import Link from 'next/link';

import HighlightedProducts from '@Components/product/highlighted-products';
import {Grid, Button} from 'semantic-ui-react';
import Logger from '@Common/logger';
import Api from '../../shared/actions/api-actions';
import Actions from '../../shared/actions/app-actions';
import {withI18next} from '../../shared/i18n/with-i18n.js';

import Header from '@Components/header/header';
import ABSelector from '@Components/ab/ab-selector';
import ABVariant from '@Components/ab/ab-variant';
import Order from '@Components/order/order';
import WithPublicContext from '@Components/hocs/with-public-context';
import {withAppConsumer} from '@Components/app-context';
import {getPageLink} from '@Common/utils/urls';
import {Actors} from '@Common/constants/app';
import classnames from 'classnames';
import Footer from '@Components/footer/footer';
import ReturnModal from '@Components/modals/return-modal';

import styles from '@Common/styles/styles.scss';
import css from '@Common/styles/styles.scss';

function IndexView(props) {
  const {t, api, order} = props;
  useEffect(() => {
    api.order.listOrdersForUser();
  }, []);
  const [returning, setReturning] = useState(null);

  return (
    <div className={css.about}>
      <Header {...props} />
      <Grid columns={16} className={classnames(css.MainGrid)}>
        <Grid.Row>
          <Grid.Column width={16}>
            <div className={classnames(styles.Huge, styles.MarginTop, styles.MarginDoubleBottom)}>
              {t('myOrders')}
            </div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={16}>
            {order.orders.map(item => (
              <Order
                key={item.id}
                onStartReturn={ord => setReturning(ord)}
                order={item}
                address={order.addresses?.[item.address_id]}
                actor={Actors.buyer}
              />
            ))}
          </Grid.Column>
        </Grid.Row>
      </Grid>
      {returning && <ReturnModal order={returning} api={api} onClose={() => setReturning(null)} />}
      <Footer {...props} />
    </div>
  );
}

IndexView.getInitialProps = async ({req, reduxStore, dispatch, query, res}) => {};

export default withAppConsumer(WithPublicContext(withI18next(['home', 'common'])(IndexView)));

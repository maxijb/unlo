import React from 'react';
import isEmpty from 'is-empty';
import Link from 'next/link';
import classnames from 'classnames';
import Router from 'next/router';

import {cdnURL} from '@Common/utils/statif-assets-utils';
import Logger from '@Common/logger';
import {AuthenticationError, AuthenticationErrorMessage} from '@Common/constants/errors';
import {getPageLink} from '@Common/utils/urls';
import {ValidateForm} from '@Common/utils/validations';
import FormValidations from '@Common/constants/form-validations';
import {LoadingStates} from '@Common/constants/social';

import Actions from '../shared/actions/app-actions';
import Api from '../shared/actions/api-actions';
import {
  Button,
  Header,
  Image,
  Modal,
  Form,
  Transition,
  Icon,
  Divider,
  Grid,
} from 'semantic-ui-react';
import Input from '../shared/components/inputs/wrapped-input';
import ErrorRenderer from '../shared/components/inputs/error-renderer';
import {withI18next} from '../shared/i18n/with-i18n.js';
import DashboardLayout from '@Components/layout/dashboard-layout';
import WithDashboardContext from '@Components/hocs/with-dashboard-context';
import {default as PageHeader} from '../shared/components/header/header';

import css from '@Common/styles/styles.scss';

class ValidateEmail extends React.Component {
  static async getInitialProps({req, dispatch}) {
    if (req?.query?.token) {
      const data = await dispatch(Api.user.validateEmailToken({token: req.query.token}, req));
      return {...(data.resp || data), token: req.query.token};
    }
    return {
      error: new AuthenticationError(AuthenticationErrorMessage.missingToken),
      token: '',
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      loading: null,
      sent: false,
      errors: {},
    };
  }

  onResendValidationToken() {
    this.setState({loading: true});
    this.props.api.user.resendValidationToken({previousToken: this.props.token}).then(({resp}) => {
      const errors = !resp.error ? {} : resp.errorData || {form: {...resp}};
      this.setState({loading: null, errors, sent: true});
    });
  }

  openSigninModal() {
    Router.push(getPageLink('signin'));
  }

  renderError() {
    const {
      // error in validating original toke
      error,
      user,
      t,
    } = this.props;

    // errors in re-sending token via api, {errors: {form: {error}}}
    const {errors, sent} = this.state;

    return (
      <div>
        <h2 className={classnames(css.HeroBold)}>{t('common:error')}</h2>
        <ErrorRenderer error={error} classes={css.Large} />
        {user.id ? (
          <div>
            {!sent && (
              <div className={css.MarginTop}>
                <div>{t('login.offerToResendValidateToken')}</div>
                <div className={css.MarginTop}>
                  <Button primary onClick={this.onResendValidationToken.bind(this)}>
                    {t('login.acceptOfferResendValidationToke')}
                  </Button>
                </div>
                <ErrorRenderer error={errors.form} />
              </div>
            )}
            {sent && isEmpty(errors) && <p>{t('login.validateEmailResent')}</p>}
            {sent && !isEmpty(errors) && (
              <div>
                <ErrorRenderer error={errors.form} />
                <div className={css.MarginTop}>
                  <Link prefetch href={getPageLink('dashboard')}>
                    <a>{t('login.linkDashboard')}</a>
                  </Link>
                </div>
              </div>
            )}
          </div>
        ) : (
          <div className={css.MarginTop}>
            <span className={css.MarginRight}>{t('login.loginToGetNewToken')}</span>
            <Button primary onClick={this.openSigninModal}>
              {t('login.signin')}
            </Button>
          </div>
        )}
      </div>
    );
  }

  renderSuccess() {
    const {t, user} = this.props;
    const {password, errors, loading, passwordWasReset} = this.state;

    if (passwordWasReset) {
      return <div>You've resetted your password successfuly</div>;
    }

    return (
      <React.Fragment>
        <Grid.Row>
          <h2 className={classnames(css.HeroBold)}>{t('common:awesome')}</h2>
        </Grid.Row>
        <Grid.Row>
          <div className={classnames(css.MarginTop, css.Large)}>
            {t('login.validateEmailSubTitle')}
          </div>

          <div className={css.MarginTop}>
            {user.id && (
              <Link prefetch href={getPageLink('dashboard')}>
                <a>{t('login.linkDashboard')}</a>
              </Link>
            )}
            {!user.id && (
              <div>
                <span className={css.MarginRight}>{t('login.loginToAccessYourDashboard')}</span>

                <Button primary onClick={this.openSigninModal}>
                  {t('login.signin')}
                </Button>
              </div>
            )}
          </div>

          <div>
            <img
              src={cdnURL('/static/empty-placeholder.png')}
              style={{maxWidth: '100%'}}
              alt="placeholder"
            />
          </div>
        </Grid.Row>
      </React.Fragment>
    );
  }

  render() {
    const {error, t} = this.props;

    return (
      <DashboardLayout
        {...this.props}
        mainTitle={error ? t('errors.genericError') : t('dashboard:wizard.welcomeToWiri')}
      >
        <Grid columns={16} padded textAlign="center">
          <Grid.Column width={16} textAlign="center" style={{maxWidth: '500px'}}>
            {!error ? this.renderSuccess() : this.renderError()}
          </Grid.Column>
        </Grid>
      </DashboardLayout>
    );
  }
}

//export default withI18next(['common'])(ValidateEmail);
export default WithDashboardContext(withI18next(['common', 'dashboard'])(ValidateEmail));

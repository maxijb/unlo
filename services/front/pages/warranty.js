import React, {useState} from 'react';
import Link from 'next/link';

import HighlightedProducts from '@Components/product/highlighted-products';
import {Grid, Button} from 'semantic-ui-react';
import Logger from '@Common/logger';
import Api from '../shared/actions/api-actions';
import Actions from '../shared/actions/app-actions';
import {withI18next} from '../shared/i18n/with-i18n.js';

import Highlights from '@Components/display/highlights';
import Header from '../shared/components/header/header';
import ABSelector from '../shared/components/ab/ab-selector';
import ABVariant from '../shared/components/ab/ab-variant';
import WithPublicContext from '@Components/hocs/with-public-context';
import {withAppConsumer} from '../shared/components/app-context';
import {getPageLink} from '@Common/utils/urls';
import classnames from 'classnames';

import {Passed, EarnIcon} from '../widget/components/display/svg-icons';
import SignupDialog from '@Components/login/signup-confirmation';
import SigninForm from '@Components/login/signin-confirmation';
import OrderStatus from '@Components/cart/order-status';
import Order from '@Components/order/order';
import Footer from '@Components/footer/footer';

import styles from '@Common/styles/styles.scss';
import css from './static-pages.scss';

function IndexView(props) {
  const {t, app, actions, currentOrder, user, query, api} = props;

  return (
    <div>
      <Header {...props} />

      <Grid columns={16} className={classnames(styles.MainGrid)}>
        <Grid.Row>
          <Grid.Column width={16} className={css.content}>
            <h1>Limited Warranty Agreement</h1>

            <h4>EFFECTIVE DATE: January 1, 2020</h4>
            <h3>
              <strong>INTRODUCTION</strong>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                Future Signal, Inc. (dba Incredible) requires all persons and entities that sell
                products on the Incredible Phones website (“the Site”) to offer individuals who buy
                any goods, which sellers (the “Seller”) have listed on the Site for sale (each, a
                “Product”) the following limited warranty (this “Limited Warranty”). This Limited
                Warranty is not provided by Incredible, but is provided by the Seller. The
                capitalized terms in this Limited Warranty have the meaning assigned to them below,
                as well as the meaning assigned to them in the Buyer Terms of Use, accessible on the
                Site.&nbsp;
              </span>
            </p>
            <h3>
              <b nostyle="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen-Sans, Ubuntu, Cantarell, 'Helvetica Neue', sans-serif;">
                1. COVERED BY LIMITED WARRANTY&nbsp;
              </b>
            </h3>
            <p>
              The Seller warrants only to the original purchaser of the Product on Incredible (“you”
              or the “Buyer”) that the Product shall be free from defects in materials and
              workmanship under normal use for the Warranty Period defined below, as documented by a
              valid proof of purchase — e.g. receipt or invoice. This Limited Warranty excludes
              consequential damages, limits the duration of implied warranties, and provides for
              liquidated damages.
            </p>
            <h3>
              <b>2. NOT COVERED BY LIMITED WARRANTY</b>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                2.1 &nbsp;This Limited Warranty does not cover Products purchased outside of the 48
                contiguous United States. This Limited Warranty does not apply to, and the term
                “Product” shall not include, (a) any consumable Parts of the Product (e.g.
                batteries), or (b) Software, even if packaged or sold with the Product or embedded
                in the Product (.e.g. firmware and/or system software) (“
                <strong>Seller Software</strong>”). Please refer to the applicable licensing
                agreement, if any, that covers such Seller Software for details of your rights with
                respect to its use.&nbsp;
              </span>
            </p>
            <h4>
              <span nostyle="font-weight: 400;">
                <strong>2.2 This Limited Warranty does not apply to any:</strong>{' '}
              </span>
            </h4>
            <p>
              <span nostyle="font-weight: 400;">
                (a) damage to the Product caused by use with non-Seller products;{' '}
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                (b) damage to the Product caused by accident, abuse, misuse, spillage of food or
                liquid or other external causes, including but not limited to fire or an act of God,
                such as a flood;{' '}
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                (c) damage to the Product caused by abnormal use of the Product, outside the
                permitted or intended uses described by Seller;{' '}
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                (d) damage to the Product caused by failure to follow instructions relating to the
                Product’s use, or use of improper voltage or power supply;{' '}
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                (e) damage to the Product caused by service performed by anyone who is not a
                representative of Seller;{' '}
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                (f) cosmetic damage to the Product, including but not limited to scratches or dents;
                or{' '}
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                (g) defects or Parts requiring replacement due to ordinary wear and tear, corrosion,
                rust or stains, scratches, dents on the casing or paintwork of the Product.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                When interacting with other Users you should exercise caution and common sense to
                protect your personal safety and property, just as you would when interacting with
                other persons whom you don’t know. NEITHER Incredible Phones NOR ITS AFFILIATES OR
                LICENSORS IS RESPONSIBLE FOR THE CONDUCT, WHETHER ONLINE OR OFFLINE, OF ANY USER OF
                THE SERVICES. Incredible Phones AND ITS AFFILIATES AND LICENSORS WILL NOT BE LIABLE
                FOR ANY CLAIM, INJURY OR DAMAGE ARISING IN CONNECTION WITH YOUR USE OF THE
                Incredible Phones PROPERTIES.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                2.3 Recovery and reinstallation of Software programs and user data are not covered
                under this Limited Warranty. It is your responsibility to backup any data, Software,
                or other materials you may have stored or preserved on the Product. It is likely
                that such data, Software, or other materials will be lost or reformatted during the
                performance of any warranty service, and Seller will not be responsible for any such
                damage or loss.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                2.4 No Seller employee is authorized to make any modification, extension, or
                addition to this Limited Warranty. If any term of this Limited Warranty is held to
                be illegal or unenforceable, the legality or enforceability of the remaining terms
                shall not be affected or impaired.&nbsp;
              </span>
            </p>
            <h3>
              <b>3. LIMITED WARRANTY COVERAGE DURATION&nbsp;</b>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                This Limited Warranty lasts for one (1) year, starting on the date that you purchase
                the Product (the “Warranty Period”).&nbsp;
              </span>
              <span nostyle="font-weight: 400;">&nbsp;</span>
            </p>
            <h3>
              <b>4. HOW TO OBTAIN WARRANTY SERVICE&nbsp;</b>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                4.1 To obtain warranty service from the Seller under this Limited Warranty, you must
                contact Incredible Phones at support@
                <span class="skimlinks-unlinked">
                  <a class="vglnk" href="http://incrediblephones.com" rel="nofollow">
                    <span>incrediblephones</span>
                    <span>.</span>
                    <span>com</span>
                  </a>
                </span>{' '}
                or +1 (888) 675-2905 and a representative will assist you in contacting the
                Seller.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                4.2 The Seller will ask you questions to determine your eligibility under this
                Limited Warranty. Based on the initial inquiry, the Seller will provide, where
                applicable, a Return Materials Authorization (“RMA”) and instructions for returning
                the defective Product and a pre-paid return label. You understand that such measures
                do not imply that the Limited Warranty is applicable. Please note that you are
                required to return the defective Product in either its original packaging or
                packaging providing an equal degree of protection, together with proof of purchase,
                to the address specified by the Seller. By sending the Product, you hereby
                acknowledge that ownership of the Product is transferred to Seller upon Seller’s
                receipt of the defective Product. If the claim is justified based on this Limited
                Warranty, the Seller will pay the cost of shipping the replacement or repaired
                Product to you. Any Product returned to Seller without a valid warranty claim or
                without an RMA may be rejected, returned at your cost (subject to prepayment) capped
                to 5% of the price of the Product, or kept for sixty (60) days for your pick-up and
                then disposed of in Seller’s sole discretion with no further liability or obligation
                to you.{' '}
              </span>
            </p>
            <h3>
              <b>5. SELLER OBLIGATIONS</b>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                5.1 If a defect in the Product arises and a valid claim is received by Seller within
                the Warranty Period, Seller will, at its option and to the extent permitted by law,
                either (1) repair the Product at no charge, using new or refurbished replacement
                parts, or (2) exchange the Product with a new or refurbished product that is the
                same or similar to the Product you purchased. Seller provides no assurance,
                representation or warranty that any replacement product will be identical to, or
                will offer the same functionalities as, the Product you purchase from Seller on the
                Site. Technological advances and Product availability may result in your receiving a
                replacement with a lower selling price than the original Product. In all cases,
                comparability of the replacement product with the original Product will be
                determined by the Seller in its reasonable discretion. If Seller determines it is
                not reasonable to repair or replace the defective Product, Seller may refund to you
                the purchase price you paid for the Product.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                5.2 In the event of a Product defect, to the extent permitted by law, these are your
                sole and exclusive remedies. This Limited Warranty is valid only for purchases made
                by you within the United States. Replacement or repaired products will only be
                shipped by Seller to addresses within the United States, and refunds will only be
                credited to your original method of payment. Any replacement product will be
                warranted for the remainder of the original Warranty Period.&nbsp;
              </span>
            </p>
            <h3>
              <b>6. EXCLUSIONS AND LIMITATIONS</b>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                6.1 IMPLIED WARRANTIES AND CONDITIONS. EXCEPT TO THE EXTENT PROHIBITED BY APPLICABLE
                LAW, ALL IMPLIED WARRANTIES AND CONDITIONS (INCLUDING WARRANTIES AND CONDITIONS OF
                MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE) SHALL BE LIMITED TO THE
                DURATION OF THIS LIMITED WARRANTY.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                6.2 These limitations shall be enforceable to the extent permitted by law. Some
                states do not allow the exclusion or limitation of incidental or consequential
                damages or the limitation of implied warranties, so the limitations or exclusions
                listed above may not apply to you.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                6.3 LIMITATION OF DAMAGES. EXCEPT TO THE EXTENT PROHIBITED BY APPLICABLE LAW, THE
                SELLER SHALL NOT BE LIABLE FOR ANY INCIDENTAL, INDIRECT, SPECIAL, OR CONSEQUENTIAL
                DAMAGES, OR ANY LOSS OF PROFITS, REVENUE OR DATA, RESULTING FROM ANY BREACH OF AN
                EXPRESS OR IMPLIED WARRANTY OR CONDITION, OR UNDER ANY OTHER LEGAL THEORY, EVEN IF
                THE SELLER HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES AND REGARDLESS OF THE
                FORM OF ACTION WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE) OR ANY OTHER LEGAL
                OR EQUITABLE THEORY. Some jurisdictions do not allow the exclusion or limitation of
                special, indirect, incidental or consequential damages, so the above limitation or
                exclusion may not apply to you.&nbsp;
              </span>
            </p>
            <p>
              <span nostyle="font-weight: 400;">
                6.4 This Limited Warranty is personal to the original Buyer and does not run with
                the Product. You may not assign, transfer, or convey this Limited Warranty.&nbsp;
              </span>
            </p>
            <h3>
              <b>7. SPECIFIC LEGAL RIGHTS</b>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                This Limited Warranty gives you specific legal rights, and you may also have other
                rights that vary from state to state
              </span>
              <span nostyle="font-weight: 400;">.&nbsp;</span>
            </p>
            <h3>
              <b>8. GOVERNING LAW</b>
            </h3>
            <p>
              <span nostyle="font-weight: 400;">
                This Limited Warranty shall be governed by the laws of the State of California, USA,
                without giving effect to any conflict of laws principles that may require the
                application of the law of another jurisdiction. This warranty gives you specific
                legal rights, and you may also have other rights, which vary from state to
                state.&nbsp;
              </span>
            </p>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Footer {...props} />
    </div>
  );
}

IndexView.getInitialProps = async ({req, reduxStore, dispatch, query, res}) => {};

export default withAppConsumer(WithPublicContext(withI18next(['home', 'common'])(IndexView)));

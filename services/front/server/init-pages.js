import dir from 'node-dir';
import path from 'path';
import {getRequestQuery} from '@Common/utils/requests';
import Logger from '@Common/logger';
import getConfig from './get-config';
import api from '../shared/universal-api';

import dinamicPages from './pages';

export default async function (server, app) {
  // Get native pages
  const basePath = path.resolve(__dirname, '../pages');
  const nativePages = dir
    .files(basePath, {
      sync: true,
      recursive: true,
    })
    .map(f => f.replace(basePath, ''))
    .filter(f => f[1] !== '_' && f.substr(-3) === '.js')
    .reduce((prev, f) => {
      const path = f.substr(0, f.length - 3);
      prev[path] = {page: path};
      return prev;
    }, {});

  // combine with dynamic pages
  const allPages = {
    ...nativePages,
    ...dinamicPages,
  };

  // Assign internationalized routes to all middlewares
  Object.keys(allPages).forEach(path => {
    const {method = 'GET', middleware = [], page = null} = allPages[path];

    if (middleware.length) {
      const i18nPath = `/:lang([a-zA-z]{2})?${path}`;
      console.log('Adding middleware to', method, i18nPath);
      server[method.toLowerCase()](i18nPath, middleware);
    }
  });

  // Assign internationalized routes to all pages
  Object.keys(allPages).forEach(path => {
    const {method = 'GET', middleware = [], page = null} = allPages[path];

    if (page) {
      const i18nPath = `/:lang([a-zA-z]{2})?${path}`;
      const resolvedPage = page[0] === '/' ? page : `/${page}`;

      const handler = (req, res) => {
        return app.render(req, res, resolvedPage, getRequestQuery(req)).then(() => {
          // // disabled server-side GA tracking for /pages on march 2021
          //if (req.GA) {
          //  req.GA.send();
          //}
        });
      };

      Logger.info('Published page', i18nPath, resolvedPage);
      server[method.toLowerCase()](i18nPath, handler);
    }
  });

  const response = await api.app.getStaticUrls(
    {},
    {
      Config: getConfig(),
    },
  );

  response.resp.forEach(cat => {
    const resolvedPage = cat.page[0] === '/' ? cat.page : `/${cat.page}`;
    const handler = (req, res) => {
      return app.render(req, res, resolvedPage, JSON.parse(cat.data)).then(() => {
        // // disabled server-side GA tracking for /pages on march 2021
        //if (req.GA) {
        //  req.GA.send();
        //}
      });
    };

    Logger.info('Published page', cat.url, resolvedPage);
    server.get(cat.url, handler);
  });
}

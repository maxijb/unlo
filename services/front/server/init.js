import express from 'express';
import next from 'next';
import {default as initPages, initI18NPages} from './init-pages';
import cookieParser from 'cookie-parser';

import fs from 'fs';
import fetchConfig from 'zero-config';
import Middleware from './middleware';
import CommonMiddleware from '@Common/server/middleware/common-middleware';

import robotsTxt from './seo/robots';
import {initCDN} from '@Common/utils/statif-assets-utils';
import widgetIframeController from './widget-iframe';
import path from 'path';
const i18nextMiddleware = require('i18next-express-middleware');
const Backend = require('i18next-node-fs-backend');
import i18n from '../shared/i18n/i18n';
import {
  i18nBackendOptions,
  SupportedLanguages,
  CustomPathDetector,
} from '@Common/constants/i18n-config';
import {StaticFilesFolder} from '@Common/constants/app';

require('@Common/server/middleware/error-handlers');

import {default as Logger} from '@Common/logger';

const dev = process.env.NODE_ENV !== 'production';
const app = next({dev, dir: path.resolve(__dirname, '../')});
const handle = app.getRequestHandler();

// ------------------------------- CONFIG -------------------------------
const server = express();
server.config = fetchConfig(path.join(__dirname, '../../common'), {
  dcValue: 'dc',
  env: {NODE_ENV: process.env.NODE_ENV || 'development'},
  loose: false,
});
const serverConfig = server.config.get('server');

initCDN(server.config.get('server'));
const lngDetector = new i18nextMiddleware.LanguageDetector();
lngDetector.addDetector(CustomPathDetector);

i18n
  .use(Backend)
  .use(lngDetector)
  .init(i18nBackendOptions(serverConfig, 'front'), () => {
    app
      .prepare()
      .then(async () => {
        server.get('/health', (req, res) => res.status(200).send('ok'));
        // ------------------------------- i18n -------------------------------
        // serve locales for client
        server.use('/locales', express.static(path.join(__dirname, '../../common/locales')));

        // serve static folder for uploaded files
        server.use(
          '/static-files',
          express.static(path.join(__dirname, `../../../../${StaticFilesFolder}`)),
        );

        // missing keys
        server.post('/locales/add/:lng/:ns', i18nextMiddleware.missingKeyHandler(i18n));
        server.use((req, res, next) => {
          req.toJSON = () => null;
          req.Config = server.config;
          next();
        });

        // ------------------------------- PARSERS AND AUTH -------------------------------
        // post body
        server.use(/\/((?!_next).)*/, [
          cookieParser(),
          CommonMiddleware.addLogger,
          i18nextMiddleware.handle(i18n),
          Middleware.getActiveAB,
          CommonMiddleware.preAuthorize,
          CommonMiddleware.initRequest,
        ]);

        // ------------------------------- PAGES -------------------------------
        server.get('/robots.txt', robotsTxt);
        try {
          await initPages(server, app);
        } catch (e) {
          console.error(e);
        }

        // all next native routes except pages (that are handled by initPages)
        server.get('*', (req, res) => {
          return handle(req, res);
        });

        // start the server
        const Port = serverConfig.ports.frontend;
        server.listen(Port, err => {
          if (err) {
            throw err;
          }
          Logger.log(`> Ready on http://localhost:${Port}`);
        });
      })
      .catch(e => {
        console.error(e);
      });
  });

import api from '../../shared/universal-api';
import {ABCookieName} from '@Common/constants/ab-tests';

export default {
  getActiveAB(req, res, next) {
    api.ab
      .getActiveAB({}, req)
      .then(response => {
        if (response.resp.error) {
          return fail(response.resp.error);
        }

        req.activeAB = response.resp.activeAB || {};
        req.cookieAB = response.resp.cookieAB || '';
        res.cookie(ABCookieName, req.cookieAB);
        next();
      })
      .catch(e => {
        fail(e);
      });

    function fail(e) {
      req.Logger.error(e);
      req.activeAB = {};
      return next();
    }
  }
};

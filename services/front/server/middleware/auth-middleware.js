/* This file is only included server-side
 * It's the main entry point for all middlewares defined for the API and pages
 */
import {AuthenticationError} from '../../../common/constants/errors';
import {CsrfTokenHeader, appCookieName} from '../../../common/constants/app';
import api from '../../shared/universal-api';
import {getPageLink} from '@Common/utils/urls';
import path from 'path';

export default {
  async superAdmin(req, res, next) {
    if (!req.auth?.id) {
      return fail();
    }

    const response = await api.user.isSuperAdmin({}, req);
    if (!response?.resp?.isSuperAdmin) {
      fail();
    } else {
      next();
    }

    function fail(e) {
      req.Logger.error(new AuthenticationError(`Failed superAdmin login on ${req.originalUrl}`));
      res.redirect(`/signin?next=${encodeURIComponent(req.originalUrl)}`);
    }
  },

  async sellerOrSuperAdmin(req, res, next) {
    if (!req.auth?.id) {
      return fail();
    }

    const response = await api.user.isSellerOrSuperAdmin({}, req);
    if (!response?.resp?.isSellerOrSuperAdmin) {
      fail();
    } else {
      next();
    }

    function fail(e) {
      req.Logger.error(new AuthenticationError('Failed Seller login'));
      res.redirect(`/signin?next=${encodeURIComponent(req.originalUrl)}`);
    }
  },

  authorizePage(req, res, next) {
    if (!req.auth || !req.auth.id) {
      fail();
    } else {
      next();
    }

    function fail(e) {
      req.Logger.error(req.authError);
      res.redirect(`/signin?next=${encodeURIComponent(req.originalUrl)}`);
    }
  },

  async selectApp(req, res, next) {
    console.log(req.originalUrl);
    // if a custom target is set in the query string
    if (req.query.app || req.query.campaign) {
      // check if target is valid
      let response;
      try {
        response = await api.assets.hasUserAccessToAsset(
          {
            appId: req.query.app,
            campaignId: req.query.campaign,
          },
          req,
        );
      } catch (e) {
        req.Logger.error(e);
        res.redirect(req.path);
      }

      if (response.request.status !== 200) {
        // if invalid remove custom target
        res.redirect(req.path);
      } else {
        // if valid set this app as default
        res.cookie(appCookieName, req.query.app);
      }
      next();
      return;
    }

    if (req.cookies[appCookieName]) {
      const appId = Number(req.cookies[appCookieName]);
      const response2 = await api.assets.hasUserAccessToAsset({appId}, req);
      if (response2.request.status === 200) {
        req.query.app = appId;
      }
    }
    next();
  },

  redirectToPortalOrders(req, res, next) {
    res.redirect('/portal/orders');
  },
};

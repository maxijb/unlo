/* This file is only included server-side
 * It's the main entry point for all middlewares defined for the API and pages
 */
import AuthMiddleware from './auth-middleware';
import ABMiddleware from './ab-middleware';

export default {
  ...AuthMiddleware,
  ...ABMiddleware
};

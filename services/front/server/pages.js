import Middleware from '../server/middleware';

export default {
  '/dashboard*': {
    middleware: [Middleware.authorizePage, Middleware.superAdmin],
  },
  '/portal*': {
    middleware: [Middleware.authorizePage, Middleware.sellerOrSuperAdmin],
  },
  '/portal': {
    middleware: [Middleware.redirectToPortalOrders],
  },
  '/dashboard/settings': {
    page: '/portal/settings',
  },
  '/portal/orders': {
    page: '/dashboard/orders',
  },
  '/portal/products': {
    page: '/dashboard/products',
  },
  '/portal/payments': {
    page: '/dashboard/payments',
  },
  '/portal/products/:id': {
    page: '/dashboard/products',
  },
  '/dashboard/category/:id': {
    page: '/dashboard/category',
  },
  '/dashboard/products/:id': {
    page: '/dashboard/products',
  },
  '/sample/:id': {
    page: 'sample',
  },
  '/checkout': {
    middleware: [Middleware.authorizePage],
    page: 'checkout',
  },
  '/product/:id/:slug?': {
    page: 'product',
  },
  '/category/:id/:slug?': {
    page: 'category',
  },
  '/variant/:variant_id/:slug?': {
    page: 'product',
  },
  '/order-confirmation/:id': {
    page: 'order-confirmation',
  },
  '/user/*': {
    middleware: [Middleware.authorizePage],
  },
  '/user': {
    middleware: [Middleware.authorizePage],
    page: '/user/home',
  },
};

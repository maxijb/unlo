export default function robots(req, res) {
  res.send(`
    User-agent: *
    Disallow: /dashboard/

    Sitemap: https://incrediblephones.com/sitemap.xml
  `);
}

const fs = require('fs');
const path = require('path');

let content;

export default function (req, res) {
  // In prod we keep the content in memore after we read for the 1st time
  // Id dev, we always reload to get a fresh copy
  if (process.env.NODE_ENV !== 'production' || !content) {
    content = fs.readFileSync(path.join(__dirname, '../static/widget/widget.js'), 'utf8');
  }
  res.send(`
		<!DOCTYPE html>
		<html lang="en">
		<head>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1.0 maximum-scale=1.0, user-scalable=0"">
		  <meta http-equiv="x-ua-compatible" content="ie=edge">
		  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500&display=swap&subset=latin-ext" rel="stylesheet">
		  <title>Wiri Widget</title>
		  <base target="_blank"/>
		<body>
		  <script>
		  	${content}
		  </script>
		</body>
		</html>
	`);
}

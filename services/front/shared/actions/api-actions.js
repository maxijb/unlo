import Logger from '@Common/logger';
import {formatError} from '@Common/constants/errors';

import Api from '../universal-api';
import {getRPCId, createActionNames} from '../utils/rpc-utils';
import extendedApiActions from './extended-api-actions';

const ids = {};
const apiActions = startApiActions(Api);

export const rpcIds = ids;
export const rpcNames = createActionNames(ids);

/* Wraps each method defined in the API and produces a RPC action for everyone.
 * Every action returns a promise that can be chained by the caller.
 * Once resolved, the promise will return either the repsonse, or an error object
 * this is a recursive function
 * @param base {object} api and its branches/leaves
 * @param prefix {stirng} previous branches name as a string branch.subbranch
 */
function startApiActions(base, prefix = '') {
  const uapi = Object.keys(base).reduce((prev, key) => {
    const method = base[key];
    if (typeof method === 'object') {
      prev[key] = startApiActions(base[key], `${prefix}${key}.`);
    } else if (typeof base[key] === 'function') {
      const id = `${prefix}${key}`;
      const rpcId = getRPCId(prefix, key);

      ids[id] = rpcId;

      prev[key] = (params, req) => dispatch => {
        dispatch({type: rpcNames[ids[id]].start, payload: params});
        return method(params, req)
          .then(response => {
            // check is it's a successful response
            const type =
              response.resp.error || (response.request && response.request.status >= 400)
                ? 'failure'
                : 'success';
            // dispatch the option
            dispatch({
              type: rpcNames[ids[id]][type],
              payload: {...params, ...response}
            });
            return response;
          })
          .catch(error => {
            const logger = req ? req.Logger : Logger;
            logger.error(error);
            // if errors are found it's a failure action
            dispatch({
              type: rpcNames[ids[id]].failure,
              payload: {...params, resp: {...formatError(error)}}
            });
            return {resp: {error: formatError(error)}};
          });
      };
    }
    return prev;
  }, {});

  return uapi;
}

// ----------------------------- These are custom actions -------------------------------

export default {
  ...apiActions,
  extended: {
    ...extendedApiActions(apiActions)
  }
};

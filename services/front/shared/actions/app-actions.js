import {createAction} from 'redux-actions';
import mirror from 'mirror-array-to-object';

const types = mirror([
  'TICK',
  'incrementCount',
  'decrementCount',
  'RESET',
  'setUserId',
  'setWindowActive',
  // Signup
  'openSignupModal',
  'closeSignupModal',
  'updateSignupForm',
  // Signin
  'updateSigninForm',
  'closeSigninModal',
  'openSigninModal',
  // Chat
  'setChatSearch',
  'selectChatAsset',
  'selectChatClient',
  'selectChatAssetLoaded',
  'selectChatClientLoaded',
  'sendChatMessage',
  'receiveNewInteraction',
  'receiveNewMessage',
  'receiveNewStatus',
  'receiveMessageData',
  'receiveUserContactData',
  'flagChatAsDone',
  'addInteractionTag',
  'removeInteractionTag',
  // Customers
  'toggleSelectCustomer',
  'selectSegment',
  // Current Admin User
  'selectActiveApp',
  'updateUserInfo',
  // Notifications
  'addNotification',
  'removeNotification',
  // Cart
  'cartAddReference',
  'setCartAddress',
  'setCartUnitAddress',
  'setCartShippingAddress',
  'setCartShippingUnitAddress',
  'setIsDifferentShippingAddress',
  'setCartAddressInput',
  'setIsOpenTradeIn',
  // User
  'setUserEmail',
  'setUserFirstName',
  'setUserLastName',
  'setUserPhone',
  // Cart,
  'setCartPaymentCard',
  'setCartPaymentType',
  'resetCart',
  'setOrderError',
  'setCartErrors',
  // Admin
  'storeImageUploaded',
  // Modal
  'setCurrentModal',
  'showContactModal',
  'setPromoCodeModal',
]);

const defaultActions = Object.keys(types).reduce((prev, key) => {
  prev[key] = (...params) => dispatch => {
    const action = dispatch(createAction(types[key])(...params));
    return Promise.resolve(action);
  };
  return prev;
}, {});

export default {
  ...defaultActions,
  /* Just include none common actions here
   * anything that doesn't follow the general pattern
   */
  v2: {
    decrementCount: () => dispatch =>
      Promise.resolve(dispatch(createAction(types.decrementCount)({teta: 3}))),
  },
  resetCount: () => dispatch => {
    return dispatch({type: types.RESET});
  },
  socketAction: (controller, message) => dispatch => {
    return dispatch({
      type: `socket--${controller}`,
      payload: message,
    });
  },
};

export const actionTypes = types;

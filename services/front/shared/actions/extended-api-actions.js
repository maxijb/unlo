import GA from 'react-ga';
import Router from 'next/router';

import {getPageLink} from '@Common/utils/urls';
export default function (apiActions) {
  return {
    /* Logs an user out, tracks, and redirects */
    fullLogout: () => dispatch => {
      apiActions.user
        .logout()(dispatch)
        .then(() => {
          GA.set({userId: null});
          window.location = getPageLink('signin');
        });
    },

    test: () => (dispatch, getState) => {
      dispatch({type: 'test'});
    }
  };
}

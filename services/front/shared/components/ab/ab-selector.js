import React from 'react';
import {AppConsumer} from '../app-context';
import ABVariant from './ab-variant';

class ABSelector extends React.Component {
  /* On all the other methods, the action creators are received as props, passed by the redux container */
  /* 't' is always available to get traductions */
  render() {
    const {children, name, variant, condition} = this.props;

    if (typeof condition !== 'undefined' && !condition) {
      return null;
    }

    return (
      <AppConsumer>
        {({ab}) => {
          const activeVariant = ab.track(name);
          if (!variant || Number(variant) === Number(activeVariant)) {
            return React.Children.map(this.props.children, child => {
              return typeof child.type === 'function'
                ? React.cloneElement(child, {activeVariant})
                : child;
            });
          }
        }}
      </AppConsumer>
    );
  }
}

export default ABSelector;

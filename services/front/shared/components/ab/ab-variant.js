import React from 'react';

class ABVariant extends React.Component {
  /* On all the other methods, the action creators are received as props, passed by the redux container */
  /* 't' is always available to get traductions */
  render() {
    const {variant, activeVariant} = this.props;
    if (Number(variant) !== Number(activeVariant)) {
      return null;
    }

    return <React.Fragment>{this.props.children}</React.Fragment>;
  }
}

export default ABVariant;

import React, {Component} from 'react';
import Router from 'next/router';
import Head from 'next/head';
import Cohere from 'cohere-js';
import Script from 'react-load-script';

import {initCDN} from '@Common/utils/statif-assets-utils';

// first we will make a new context
const AppContext = React.createContext();

// Then create a provider Component
class AppProvider extends Component {
  //componentDidMount() {
  //  //Router.events.on('routeChangeStart', () => console.log('start', Date.now()));
  //
  //}
  componentWillMount() {
    if (process.browser) {
      const {GA, store, router} = this.props;
      const state = store.getState();
      const user = state.user || {};
      const {cdn, GATrackingId} = state.app.config;

      GA.initialize(GATrackingId, {
        titleCase: false,
        gaOptions: {
          cookieDomain: 'none',
          clientId: user.clientId,
        },
      });

      if (user.id) {
        GA.set({dimension1: user.id});
      }

      if (process.browser) {
        initCDN(cdn);
        Router.events.on('routeChangeComplete', this.trackGAPageview.bind(this));
        if (router.query.source) {
          GA.set({dimension2: router.query.source});
        }
      }
    }
  }

  componentDidMount() {
    this.trackGAPageview();
    const {
      user: {clientId: cid, superAdmin},
      app: {
        config: {useCohere},
      },
    } = this.props.store.getState();
    if (useCohere && !superAdmin) {
      Cohere.init('hfBpedlGt_EOq7bipXYSjNT0');
      Cohere.identify(cid);
    }

    window.intercomSettings = {
      app_id: 'd5letjeo',
      hide_default_launcher: true,
    };

    var w = window;
    var ic = w.Intercom;
    if (typeof ic === 'function') {
      ic('reattach_activator');
      ic('update', w.intercomSettings);
    } else {
      var d = document;
      var i = function () {
        i.c(arguments);
      };
      i.q = [];
      i.c = function (args) {
        i.q.push(args);
      };
      w.Intercom = i;

      var s = d.createElement('script');
      s.type = 'text/javascript';
      s.async = true;
      s.src = 'https://widget.intercom.io/widget/d5letjeo';
      var x = d.getElementsByTagName('script')[0];
      x.parentNode.insertBefore(s, x);
    }
  }

  trackGAPageview() {
    this.props.GA.pageview(window.location.pathname + window.location.search);
    //console.log('route done', Date.now());
  }

  render() {
    const {Logger, GA, ab, isMobile, isTablet, store, router} = this.props;

    const config = store.getState().app?.config || {};
    const user = store.getState().user || {};
    return (
      <AppContext.Provider
        value={{
          Logger,
          GA,
          ab,
          isMobile,
          isTablet,
          router,
        }}
      >
        {config.isProd && !user.superAdmin && (
          <Head>
            <script async src="https://www.googletagmanager.com/gtag/js?id=AW-316844746" />
            <script
              dangerouslySetInnerHTML={{
                __html: `
                window.dataLayer = window.dataLayer || [];
                function gtag(){
                  dataLayer.push(arguments);
                }
                gtag('js', new Date());
                gtag('config', 'AW-316844746');
            `,
              }}
            />
          </Head>
        )}
        {this.props.children}

        {config.isProd && !user.superAdmin && (
          <script
            dangerouslySetInnerHTML={{
              __html: `
                 (function(h,o,t,j,a,r){
    h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
    h._hjSettings={hjid:2515941,hjsv:6};
    a=o.getElementsByTagName('head')[0];
    r=o.createElement('script');r.async=1;
    r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
    a.appendChild(r);
  })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
            `,
            }}
          />
        )}
      </AppContext.Provider>
    );
  }
}

// then make a consumer which will surface it
const AppConsumer = AppContext.Consumer;

export default AppProvider;
export {AppConsumer};

export function withAppConsumer(Component) {
  const hoc = function ThemedComponent(props) {
    return <AppConsumer>{context => <Component {...props} context={context} />}</AppConsumer>;
  };
  // passes getInitialProps to the HOC if it's a page
  if (Component.getInitialProps) {
    hoc.getInitialProps = Component.getInitialProps;
  }
  return hoc;
}

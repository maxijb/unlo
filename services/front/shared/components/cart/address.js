import React from 'react';
import {formatAddress} from '@Common/utils/format-utils';

function Address(address) {
  return <span>{formatAddress(address)}</span>;
}

export default Address;

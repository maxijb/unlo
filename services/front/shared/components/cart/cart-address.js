import React, {useState, useEffect} from 'react';
import {translate} from 'react-i18next';
import {Grid} from 'semantic-ui-react';
import AddressInput from '@Components/inputs/address-input';
import WrappedInput from '@Components/inputs/wrapped-input';
import WrappedCheckbox from '@Components/inputs/wrapped-checkbox';
import Address from './address';
import classnames from 'classnames';

import css from './cart-list.scss';
function CartAddress({cart, actions, api, t, user, subtotal}) {
  const {differentShippingAddress, address, shippingAddress, cartErrors} = cart;
  const {primaryAddress} = user;
  //const [isEditing, setIsEditing] = useState(false);

  const getTaxRate = () => {
    api.cart.getTaxRateByZipCode({
      address,
      subtotal,
      shipping: 0,
    });
  };

  useEffect(() => {
    if (
      address?.postal_code?.length === 5 &&
      address?.showTextName &&
      !cart.loadingTaxRate &&
      address?.administrative_area_level_1 &&
      (cart.taxRate === null ||
        address?.postal_code !== cart.taxRateAddress?.postal_code ||
        address?.administrative_area_level_1 !==
          cart.taxRateAddress?.administrative_area_level_1) &&
      address?.administrative_area_level_1?.length >= 2
    ) {
      getTaxRate();
    }
  }, [address?.postal_code, address?.showTextName, address?.administrative_area_level_1]);

  const header = (
    <Grid.Row>
      <Grid.Column width={16}>
        {t(differentShippingAddress ? 'billingAddress' : 'billingAndShippingAddress')}
      </Grid.Column>
    </Grid.Row>
  );

  //const textAddress =
  //  !address?.showTextName && primaryAddress?.id
  //    ? primaryAddress.showTextName
  //    : address?.showTextName || '';
  //const textUnit = !address && primaryAddress?.id ? primaryAddress.unit : address?.unit || '';

  /*
  if (primaryAddress?.id) {
    return (
      <Grid columns={16}>
        {header}
        <div>
          <div className={css.fact}>
            <Address textName={primaryAddress.textName} unit={primaryAddress.unit} />
          </div>
          <div>
            <a onClick={() => setIsEditing(true)}>{t('edit')}</a>
          </div>
        </div>
      </Grid>
    );
  }
  */

  return (
    <Grid columns={16} className="fullSize">
      {/*header*/}
      <Grid.Row>
        <Grid.Column width={16}>
          <AddressInput
            className={classnames(css.cartInput, css.leftColumn)}
            index={0}
            fluid
            error={cartErrors.address}
            errorClassName={css.errorInput}
            placeholder={t('shippingAddress')}
            initialValue={address?.showTextName || ''}
            onChange={address => {
              actions.setCartAddress(address);
            }}
            onChangeText={value => actions.setCartAddressInput({value, field: 'showTextName'})}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column width={16}>
          <WrappedInput
            className={css.cartInput}
            placeholder={t('aptPlaceholder')}
            fluid
            value={address?.unit || ''}
            onChange={value => {
              actions.setCartUnitAddress(value);
            }}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column width={9}>
          <WrappedInput
            className={classnames(css.cartInput, css.leftColumn)}
            placeholder={t('city')}
            fluid
            error={cart.errorTaxRate}
            errorClassName={css.errorInput}
            value={address?.locality || ''}
            onChange={value => {
              actions.setCartAddressInput({value, field: 'locality'});
            }}
          />
        </Grid.Column>
        <Grid.Column width={3}>
          <WrappedInput
            className={classnames(css.cartInput, css.leftColumn)}
            placeholder={t('state')}
            fluid
            value={address?.administrative_area_level_1 || ''}
            onChange={value => {
              actions.setCartAddressInput({value, field: 'administrative_area_level_1'});
            }}
          />
        </Grid.Column>
        <Grid.Column width={4}>
          <WrappedInput
            className={css.cartInput}
            placeholder={t('zip')}
            fluid
            value={address?.postal_code || ''}
            onChange={value => {
              actions.setCartAddressInput({value, field: 'postal_code'});
            }}
          />
        </Grid.Column>
      </Grid.Row>
      {/*
      <Grid.Row>
        <Grid.Column width={16}>
          <WrappedCheckbox
            checked={differentShippingAddress}
            label={t('useDifferentShippingAddress')}
            onClick={() => actions.setIsDifferentShippingAddress(!differentShippingAddress)}
          />
        </Grid.Column>
      </Grid.Row>
        */}
      {differentShippingAddress && (
        <>
          <Grid.Row>
            <Grid.Column width={16}>{t('shippingAddress')}</Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={12}>
              <AddressInput
                initialValue={shippingAddress?.showTextName || ''}
                index={1}
                fluid
                onChange={address => {
                  actions.setCartShippingAddress(address);
                }}
              />
            </Grid.Column>
            <Grid.Column width={4}>
              <WrappedInput
                placeholder={t('aptPlaceholder')}
                fluid
                value={shippingAddress?.unit || ''}
                onBlur={evt => {
                  actions.setCartShippingUnitAddress(evt.target.value);
                }}
              />
            </Grid.Column>
          </Grid.Row>
        </>
      )}
    </Grid>
  );
}

export default translate(['home'])(CartAddress);

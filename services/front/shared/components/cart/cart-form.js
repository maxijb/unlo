import React, {useState, useMemo} from 'react';
import Router from 'next/router';
import isEmpty from 'is-empty';
import Link from 'next/link';
import {Trans} from 'react-i18next';
import {translate} from 'react-i18next';
import {Grid, Button} from 'semantic-ui-react';
import classnames from 'classnames';
import {withAppConsumer} from '../app-context';
import FormValidations from '@Common/constants/form-validations';
import {ValidateForm} from '@Common/utils/validations';
import Bubble from '@Components/display/bubble';
import {AppColors} from '@Common/constants/app';
import {RequestErrorMessage} from '@Common/constants/errors';
import CartList from '@Components/cart/cart-list';
import CartAddress from '@Components/cart/cart-address';
import CartPerson from '@Components/cart/cart-person';
import CartShipping from '@Components/cart/cart-shipping';
import CartPayment from '@Components/cart/cart-payment';
import CartTotals from '@Components/cart/cart-totals';
import CartTradein from '@Components/cart/cart-tradein';
import BoxFixed from '@Components/product/box-fixed';
import OrderErrorModal from '@Components/modals/order-error-modal';
import Price from '@Components/product/price';
import ErrorRenderer from '@Components/inputs/error-renderer';
import {LockClosed, BlueOK} from '../../../widget/components/display/svg-icons';
import {CardElement, useStripe, useElements} from '@stripe/react-stripe-js';
import ChatLink from '@Components/display/chat-link';

import styles from '@Common/styles/styles.scss';
import css from './cart-list.scss';

function CartForm(props) {
  const {
    cart,
    cart: {
      items,
      reference,
      address,
      differentShippingAddress,
      shippingAddress,
      paymentType,
      paymentCard,
      paymentCardError,
      taxRate,
      isOrdering,
      shippingMethod,
      orderErrors,
      cartErrors,
    },
    shippingCharges,
    isMobile,
    isTablet,
    setIsOpenCartModal,
    api,
    actions,
    user,
    t,
    total,
    promoCode,
    subtotal,
    subtotalWithoutPromoCode,
    tax,
    context: {GA},
  } = props;

  const stripe = useStripe();
  const elements = useElements();
  const [isSubmitting, setIsSubmitting] = useState(false);

  const validate = () => {
    return ValidateForm(
      {
        email: user.email,
        phone: user.phone,
        firstName: user.firstName,
        lastName: user.lastName,
        paymentType,
        cardReady: (paymentType === 'card' && paymentCard.cardNumber) || paymentType === 'paypal',
        address: (user.primaryAddress?.id && cart.address === null) || cart.address?.postal_code,
      },
      FormValidations.checkout,
    );
  };

  const canPlaceOrder = useMemo(() => {
    return (
      ((user.primaryAddress?.id && cart.address === null) || cart.address?.postal_code) &&
      taxRate !== null &&
      (!cart.differentShippingAddress || cart.shippingAddress) &&
      user.email &&
      user.firstName &&
      user.lastName &&
      user.phone &&
      (paymentType === 'affirm' ||
        paymentType === 'paypal' ||
        (paymentType === 'card' &&
          stripe &&
          elements &&
          //paymentCard.zip &&
          //paymentCard.cvc &&
          paymentCard.cardNumber))
      //&&
      //paymentCard.expiry
    );
  }, [cart, user]);

  const placeOrder = async () => {
    setIsSubmitting(true);
    const foundErrors = validate();
    if (!isEmpty(foundErrors)) {
      setIsSubmitting(false);
      actions.setCartErrors(foundErrors);
      return;
    }
    GA.event({
      category: 'cart',
      action: 'place-order',
      label: paymentType,
      value: Math.round(total),
      nonInteraction: true,
    });

    let stripePaymentId = null;

    // ---------------------------------- CARD PAYMENT -----------------------
    if (paymentType === 'card') {
      const tokenData = {
        currency: 'usd',
        address_country: 'US',
        name: `${user.firstName} ${user.lastName}`,
        // stripe full element captures this
        //address_zip: paymentCard.zip,
      };

      // TODO: if using a pre-recorded address use that as source
      if (address) {
        if (address.locality || address.sublocality_level_1) {
          tokenData.address_city = address.locality || address.sublocality_level_1;
        }
        tokenData.address_state = address.administrative_area_level_1 || null;
        tokenData.address_line1 = `${address.street_number} ${address.route}`;
        tokenData.address_line2 = `#${address.unit || ''}`;
      }
      const {error, token} = await stripe.createToken(elements.getElement(CardElement), tokenData);

      if (error) {
        GA.event({
          category: 'cart',
          action: 'payment-failure',
          label: 'stripe-token-failure',
          value: Math.round(total),
          nonInteraction: true,
        });
        setIsSubmitting(false);
        actions.setOrderError(error.message);
        return;
      }
      stripePaymentId = token.id;

      const order = await api.cart.placeOrder({
        paymentType,
        stripePaymentId,
        stripeCardId: token.card.id,
        address,
        storedAddressId: address !== null ? null : user.primaryAddress?.id,
        differentShippingAddress,
        shippingAddress,
        paymentType,
        paymentCard,
        phone: user.phone,
        email: user.email,
        firstName: user.firstName,
        lastName: user.lastName,
      });

      setIsSubmitting(false);

      if (order?.resp?.ok && order?.resp?.order_id) {
        GA.event({
          category: 'cart',
          action: 'payment-success',
          label: paymentType,
          value: Math.round(total),
          nonInteraction: true,
        });
        Router.push(
          `/order-confirmation?id=${order?.resp?.order_id}&reset=true`,
          `/order-confirmation/${order?.resp?.order_id}`,
        );
      } else {
        GA.event({
          category: 'cart',
          action: 'payment-failure',
          label: paymentType,
          value: Math.round(total),
          nonInteraction: true,
        });
        actions.setOrderError(order.resp.errors?.[0] || order.resp.error);
      }
    }

    if (paymentType === 'paypal') {
      const {resp} = await api.cart.getPaypalUrl({
        paymentType,
        address,
        storedAddressId: address !== null ? null : user.primaryAddress?.id,
        differentShippingAddress,
        shippingAddress,
        paymentType,
        paymentCard,
        phone: user.phone,
        email: user.email,
        firstName: user.firstName,
        lastName: user.lastName,
      });

      if (resp.ok && resp.approveUrl) {
        GA.event({
          category: 'cart',
          action: 'payment-redirect',
          label: paymentType,
          value: Math.round(total),
          nonInteraction: true,
        });
        window.location = resp.approveUrl;
      } else {
        GA.event({
          category: 'cart',
          action: 'payment-failure',
          label: paymentType,
          value: Math.round(total),
          nonInteraction: true,
        });
        actions.setOrderError(resp.errors?.[0] || resp.error || RequestErrorMessage.paymentError);
      }
    }
    setIsSubmitting(false);
  };

  const disabledOrderCallback = () => {
    GA.event({
      category: 'cart',
      action: 'click-disabled-place-order',
      label: paymentType,
    });

    const foundErrors = validate();
    actions.setCartErrors(foundErrors);
  };

  const placeOrderButton = (
    <Button
      primary
      fluid
      size="big"
      onClick={() => {
        if (isOrdering || isSubmitting) {
          return;
        }
        if (canPlaceOrder) {
          placeOrder();
        } else {
          disabledOrderCallback();
        }
      }}
      loading={isOrdering || isSubmitting}
      className={classnames(styles.NoWrap, {[css.disabled]: !canPlaceOrder})}
    >
      <div className={classnames(styles.FlexAllCenter)}>
        <LockClosed color="white" />
        <span className={styles.MarginHalfLeft}>
          {t(`proceedWithOrder-${paymentType || 'card'}`)}
        </span>
      </div>
    </Button>
  );

  return (
    <>
      <Grid.Row className="narrow">
        <Grid.Column computer={10} mobile={16} tablet={8}>
          {!isMobile && (
            <div
              className={classnames(
                styles.MarginDoubleBottom,
                styles.MarginDoubleTop,
                styles.FlexSpaceBetween,
              )}
            >
              <div>
                <LockClosed color={AppColors.placeholder} />
                <span className={classnames(styles.Large, styles.MarginHalfLeft)}>
                  {t('placeOrder')}
                </span>
              </div>
              <div className={styles.FlexCenter}>
                <Bubble
                  text={
                    <div className={styles.FlexCenter}>
                      <BlueOK color={AppColors.positive} className={styles.MarginHalfRight} />
                      {t('12MonthWarranty')}
                    </div>
                  }
                />
                <Bubble
                  text={
                    <div className={styles.FlexCenter}>
                      <BlueOK color={AppColors.positive} className={styles.MarginHalfRight} />
                      {t('freeShippingAndReturn')}
                    </div>
                  }
                />
              </div>
            </div>
          )}
          <div className={css.formContainer}>
            {isMobile && (
              <CartShipping
                user={user}
                cart={cart}
                api={api}
                actions={actions}
                openCartModal={() => {
                  GA.event({
                    category: 'cart',
                    action: 'open-cart-modal',
                  });
                  setIsOpenCartModal(true);
                }}
              />
            )}
            <CartPerson user={user} cart={cart} api={api} actions={actions} />
            <CartAddress user={user} cart={cart} api={api} actions={actions} subtotal={subtotal} />
            <CartPayment user={user} cart={cart} api={api} actions={actions} />
          </div>
          {isMobile && (
            <div
              className={classnames(
                styles.Tiny,
                styles.ColorSubtext,
                styles.MarginTop,
                styles.centered,
                styles.MarginHorizontal,
                styles.PaddingHorizontal,
              )}
            >
              {t('clickingTermsAndPrivacy')}
            </div>
          )}
          {!isMobile && (
            <>
              <div
                className={classnames(css.sellerItem, styles.FlexCenter, styles.MarginDoubleTop)}
              >
                <div>{placeOrderButton}</div>
                <div className={styles.MarginLeft}>
                  <div
                    className={classnames(styles.MediumMedium, styles.FlexGrow, styles.FlexCenter)}
                  >
                    <div>{t('estimatedTotal')}</div>
                    <div className={styles.MarginLeft}>
                      <Price value={total} />
                    </div>
                  </div>
                  <div className={classnames(styles.Tiny, styles.ColorWhite80)}>
                    {t('paymentConditionsSecond')}
                  </div>
                </div>
              </div>
              <ChatLink />
            </>
          )}
        </Grid.Column>
        {!isMobile && (
          <Grid.Column computer={6} mobile={16} tablet={8}>
            <div className={classnames(css.sellerItem, styles.MarginDoubleTop)}>
              <div
                className={classnames(
                  styles.MediumMedium,
                  styles.FlexSpaceBetween,
                  styles.MarginBottom,
                )}
              >
                <div>{t('estimatedTotal')}</div>
                <div>
                  <Price value={total} />
                </div>
              </div>
              <div className={classnames(styles.MarginDoubleBottom)}>{placeOrderButton}</div>
              <CartList nested={true} items={items} reference={reference} api={api} cart={cart} />
              <CartTotals
                cart={cart}
                subtotal={subtotal}
                subtotalWithoutPromoCode={subtotalWithoutPromoCode}
                shippingChargesBrokeDown={true}
                total={total}
                promoCode={promoCode}
                tax={tax}
                nested={true}
                api={api}
                placeOrderButton={null}
              />
            </div>
            <div className={classnames(styles.Tiny, styles.ColorWhite80, styles.Centered)}>
              <div>{t('paymentConditionsFirst')}</div>
              <div>{t('paymentConditionsSecond')}</div>
            </div>
          </Grid.Column>
        )}
        {isMobile && (
          <BoxFixed>
            {placeOrderButton}
            <div className={styles.Centered}>
              <ErrorRenderer error={orderErrors[0]} />
              <div
                className={classnames(styles.ButtonRegular, styles.MarginTop)}
                onClick={() => window.Intercom && window.Intercom('showNewMessage')}
              >
                {t('needHelp')}
                <a>{t('chatWithUs')}</a>
              </div>
            </div>
          </BoxFixed>
        )}
        {!!orderErrors?.[0] && (
          <OrderErrorModal
            error={orderErrors[0]}
            onClose={() => actions.setOrderError(null)}
            isOpen={true}
          />
        )}
      </Grid.Row>
    </>
  );
}

export default withAppConsumer(translate(['home', 'common'])(CartForm));

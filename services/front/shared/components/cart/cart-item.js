import {getPageLink} from '@Common/utils/urls';

import React, {useMemo} from 'react';
import {translate} from 'react-i18next';
import Image from '@Components/images/image';
import {consolidateAttributesAsName} from '../../utils/attributes-utils';
import Price from '../product/price';
import IntegerInput from '@Components/inputs/integer-input';
import ErrorRenderer from '@Components/inputs/error-renderer';
import Link from 'next/link';
import classnames from 'classnames';
import {CartErrorMessage} from '@Common/constants/errors';

import styles from '@Common/styles/styles.scss';
import css from './cart-item.scss';

function CartItem({item, data = {}, t, api, shippingMethod, nested}) {
  const attributes = consolidateAttributesAsName(data.attributes, t);
  const onChangeQuantity = async quantity => {
    await api.cart.changeQuantity({cart_id: item.cart_id, quantity});
  };
  const onDelete = async () => {
    await api.cart.removeItem({cart_id: item.cart_id});
  };

  const asURL = getPageLink('variant', {product: data, attributes, t});
  const link = `/product/?id=${data.category_id}-${data.id}`;

  return (
    <div className={classnames(css.item, {[css.nested]: nested})}>
      <div className={css.details}>
        <Link href={link} as={asURL}>
          <Image file={data.image} size={75} withFrame={true} className={styles.MarginRight} />
        </Link>
        <div className={classnames(styles.FlexGrow)}>
          <div>
            <Link href={link} as={asURL}>
              <a className={css.name}>
                {data.name} {attributes.capacity || ''}
                {data.color ? ` - ${data.color}` : ''}
              </a>
            </Link>
          </div>
          {attributes.carrier && (
            <div className={css.attribute}>
              {attributes.carrier !== 'Unlocked'
                ? t('worksOnCarrier', {
                    carrier: t(attributes.carrier),
                  })
                : t('worksOnUnlocked')}
            </div>
          )}
          {attributes.appearance && (
            <div className={css.attribute}>
              {t('appearance')}: {t(attributes.appearance)}
            </div>
          )}
          <div className={classnames(styles.FlexSpaceBetween, styles.FlexTop)}>
            <div>
              <Price
                value={data.price}
                className={classnames(styles.ButtonMedium, styles.MarginHalfTop)}
              />
              {item.error === CartErrorMessage.outOfStock && (
                <ErrorRenderer
                  classes={css.stockError}
                  error={t('errorOutOfStock', {
                    max_stock: item.max_stock,
                    context: item.max_stock ? 'withMaxStock' : '',
                  })}
                />
              )}
            </div>
            <div className={styles.Centered}>
              <IntegerInput
                value={item.quantity}
                onSubmit={onChangeQuantity}
                isLoading={item.isLoading}
              />
              <div>
                <a onClick={onDelete} className={styles.Tiny}>
                  {t('remove')}
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default translate(['home'])(CartItem);

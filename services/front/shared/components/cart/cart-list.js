import React, {useMemo} from 'react';
import {withAppConsumer} from '../app-context';
import {translate} from 'react-i18next';
import CartItem from './cart-item';
import Radio from '@Components/inputs/wrapped-radio';
import {
  DefaultFastShippingRate,
  ShippingMethod,
  DefaultShippingMethods,
} from '@Common/constants/app';
import classnames from 'classnames';
import Price from '@Components/product/price';
import {stringDate} from '@Common/utils/date-utils';
import {getCartDisplayProductId} from '../../utils/cart-utils';

import styles from '@Common/styles/styles.scss';
import css from './cart-list.scss';

function CartList({items, reference, api, shippingMethod, cart, t, nested, context: {GA}}) {
  let promoCode = null;
  const sellers = useMemo(() => {
    const map = (cart?.items || []).reduce((agg, it) => {
      if (it.type === 'code') {
        return agg;
      }

      let seller = it.seller_id;

      const shippingMethods = cart.shippingMethodsBySeller[seller] || DefaultShippingMethods;

      if (!agg.has(seller)) {
        agg.set(seller, {
          id: seller,
          shippingMethods,
          shippingMethod: shippingMethods[0],
          items: [],
        });
      }

      const data = agg.get(seller);
      if (it.type === 'shipping') {
        data.shippingMethod = shippingMethods.find(m => m.id === it.variant_id);
      } else if (it.type === 'product') {
        data.items.push(it);
      }
      return agg;
    }, new Map());
    return Array.from(map.values());
  });

  return (
    <div className={css.list}>
      {sellers.map(seller => {
        return (
          <div className={classnames(css.sellerItem, {[css.nested]: nested})}>
            {seller.items.map(it => {
              return (
                <CartItem
                  key={it.cart_id}
                  item={it}
                  data={reference[getCartDisplayProductId(it)]}
                  api={api}
                  shippingMethod={shippingMethod}
                  nested={true}
                />
              );
            })}
            {seller.shippingMethods.map(method => (
              <div className={classnames(styles.FlexSpaceBetween, styles.MarginBottom)}>
                <Radio
                  name="shipping"
                  checked={true}
                  items={[
                    {
                      display: (
                        <div className={styles.Clickable}>
                          <div>{stringDate(new Date(method.eta), t)}</div>
                          <div className={classnames(styles.Tiny, styles.ColorSubtext)}>
                            {t('common:XdayShipping', {
                              ...method,
                              carrier: t(`common:${method.carrier}`),
                              context: !Number(method.price) ? 'free' : '',
                            })}
                          </div>
                        </div>
                      ),
                      value: method.id,
                    },
                  ]}
                  value={seller.shippingMethod?.id}
                  onClick={val => {
                    GA.event({
                      category: 'cart',
                      action: 'shipping-method',
                      label: method.speed_days,
                    });
                    api.cart.setShipping({
                      shipping_id: method.id,
                      seller_id: seller.id,
                    });
                  }}
                />
                <Price value={method.price} />
              </div>
            ))}
          </div>
        );
      })}
    </div>
  );
}

export default withAppConsumer(translate(['home', 'common'])(CartList));

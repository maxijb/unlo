import React, {useState, useMemo} from 'react';
import {translate} from 'react-i18next';
import {withAppConsumer} from '../app-context';
import {Grid, Radio, FormField, Transition} from 'semantic-ui-react';
import AddressInput from '@Components/inputs/address-input';
import {AppColors} from '@Common/constants/app';
import WrappedInput from '@Components/inputs/wrapped-input';
import WrappedCheckbox from '@Components/inputs/wrapped-checkbox';
import CreditCardInput from 'react-credit-card-input';
import TagsSelector from '@Components/product/tag-selector';
import {Lock, LockClosed, BasicCard, Paypal} from '../../../widget/components/display/svg-icons';
import classnames from 'classnames';
import ErrorRenderer from '../inputs/error-renderer';

/// STRIPE
import {
  CardElement,
  CardNumberElement,
  CardExpiryElement,
  CardCvcElement,
  Elements,
  useStripe,
  useElements,
} from '@stripe/react-stripe-js';

import styles from '@Common/styles/styles.scss';
import css from './cart-list.scss';

const options = {
  showIcon: true,
  style: {
    base: {
      fontSize: '16px',
      fontFamily: '"Open Sans", "Helvetica Neue", arial, sans-serif',
      height: '64px',
      fontWeight: 500,
      paddingLeft: '32px',
      paddingRight: '32px',
      '::placeholder': {
        color: '#8793a1',
        fontWeight: 400,
      },
    },
  },
};

function CartPayment({cart, user, actions, api, t, context: {GA, isMobile}}) {
  const [isEdit, setIsEdit] = useState(false);
  const {paymentCard, paymentType, cartErrors} = cart;

  const selectedTags = useMemo(() => new Set([paymentType]), [paymentType]);
  const tags = useMemo(() => {
    return [
      {
        id: 'card',
        content: (
          <div className={css.paymentTypeIconHolder}>
            <BasicCard /> <span>Card</span>
          </div>
        ),
      },
      {
        id: 'paypal',
        content: <Paypal size={18} />,
      },
      //{
      //  id: 'affirm',
      //  content: <img className={css.paymentTypeIcon} src="/static/incredible/affirm.svg" />,
      //},
    ];
  }, []);

  const setCartPaymentCard = (field, value) => {
    actions.setCartPaymentCard({field, value});
  };

  const setCartPaymentType = value => {
    GA.event({
      category: 'cart',
      action: 'cart-payment-select',
      label: value,
    });

    actions.setCartPaymentType({value});
  };

  const setCartPaymentStripeError = (field, data) => {
    actions.setCartPaymentCard({
      field,
      value: !data.error && data.complete,
    });
  };

  return (
    <Grid columns={16} className={classnames('fullSize', styles.WhiteBackground)}>
      <>
        <Grid.Row>
          <Grid.Column width={16}>
            <div
              className={classnames(css.cartInputSection, {[css.open]: !!paymentType || isMobile})}
            >
              <label
                className={classnames(
                  styles.Small,
                  styles.MarginHalfBottom,
                  styles.Block,
                  styles.ColorPlaceholder,
                )}
              >
                {t('paymentMethod')}
              </label>
              <TagsSelector selectedTags={selectedTags} tags={tags} onChange={setCartPaymentType} />
              <ErrorRenderer
                error={cartErrors.paymentType}
                classes={[css.errorInput, css.paymentError]}
              />
            </div>
          </Grid.Column>
        </Grid.Row>

        <Grid.Row style={{display: paymentType !== 'card' ? 'none' : 'block'}}>
          <Grid.Column width={16}>
            <div className={css.stripeInput}>
              <CardElement
                options={options}
                onFocus={() => {
                  GA.event({
                    category: 'cart',
                    action: 'cart-card-number',
                    label: 'stripe',
                  });
                }}
                onChange={data => {
                  setCartPaymentStripeError('cardNumber', data);
                }}
              />
              <ErrorRenderer
                error={cartErrors.cardReady}
                classes={[css.errorInput, css.cardError]}
              />
            </div>
          </Grid.Column>
        </Grid.Row>

        {paymentType === 'card' && (
          <div
            className={classnames(css.cartLegend, {
              [css.cartInput]: isMobile,
            })}
          >
            <LockClosed color={AppColors.placeholder} className={styles.MarginHalfRight} />
            <b>{t('sslEncrypted')} </b>
            {t('sslEncryptedExplanation')}
          </div>
        )}
        {paymentType === 'affirm' && (
          <div
            className={classnames(css.cartLegend, {
              [css.cartInput]: isMobile,
            })}
          >
            <b>{t('payWithAffirmExplanation')} </b>
            {t('payWithAffirmDescription')}
          </div>
        )}
        {paymentType === 'paypal' && (
          <div
            className={classnames(css.cartLegend, {
              [css.cartInput]: isMobile,
            })}
          >
            <b>{t('payPaypal')} </b>
            {t('redirectToPaypal')}
          </div>
        )}
      </>
    </Grid>
  );
}

export default withAppConsumer(translate(['home'])(CartPayment));

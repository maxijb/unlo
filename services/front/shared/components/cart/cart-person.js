import React, {useState} from 'react';
import {withAppConsumer} from '../app-context';
import {translate} from 'react-i18next';
import {Grid} from 'semantic-ui-react';
import AddressInput from '@Components/inputs/address-input';
import WrappedInput from '@Components/inputs/wrapped-input';
import WrappedCheckbox from '@Components/inputs/wrapped-checkbox';
import classnames from 'classnames';

import css from './cart-list.scss';
function CartPerson({cart, user, actions, api, t, context: {GA}}) {
  const [isEdit, setIsEdit] = useState(false);
  const {email, firstName, lastName, cartErrors} = cart;

  /*
  if (user.id && !isEdit) {
    return (
      <Grid columns={16}>
        <Grid.Row>
          <Grid.Column width={16}>
            <div className={css.fact}>
              {user.firstName} {user.firstName} ({user.email})
            </div>
            <a onClick={() => setIsEdit(true)}>{t('edit')}</a>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
  */

  const saveContact = () => {
    if (user?.email && !user?.id) {
      api.user.saveContact({
        email: user?.email,
        firstName: user?.firstName,
        lastName: user?.lastName,
        phone: user?.phone,
      });
    }
  };

  return (
    <Grid columns={16} className="fullSize">
      <>
        <Grid.Row>
          <Grid.Column width={16}>
            <WrappedInput
              className={css.cartInput}
              placeholder={t('email')}
              fluid
              error={cartErrors.email}
              errorClassName={css.errorInput}
              value={user?.email || ''}
              onFocus={() => {
                GA.event({
                  category: 'cart',
                  action: 'cart-inputs-focus',
                  label: 'email',
                });
              }}
              onChange={value => {
                actions.setUserEmail((value || '').trim());
              }}
              onBlur={saveContact}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={8}>
            <WrappedInput
              placeholder={t('firstName')}
              className={classnames(css.cartInput, css.leftColumn)}
              fluid
              error={cartErrors.firstName}
              errorClassName={css.errorInput}
              value={user?.firstName || ''}
              onFocus={() => {
                GA.event({
                  category: 'cart',
                  action: 'cart-inputs-focus',
                  label: 'first-name',
                });
              }}
              onChange={value => {
                actions.setUserFirstName(value);
              }}
              onBlur={saveContact}
            />
          </Grid.Column>
          <Grid.Column width={8}>
            <WrappedInput
              placeholder={t('lastName')}
              className={css.cartInput}
              fluid
              error={cartErrors.lastName}
              errorClassName={css.errorInput}
              value={user?.lastName || ''}
              onFocus={() => {
                GA.event({
                  category: 'cart',
                  action: 'cart-inputs-focus',
                  label: 'last-name',
                });
              }}
              onChange={value => {
                actions.setUserLastName(value);
              }}
              onBlur={saveContact}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={16}>
            <WrappedInput
              className={css.cartInput}
              placeholder={t('phone')}
              fluid
              error={cartErrors.phone}
              errorClassName={css.errorInput}
              value={user?.phone || ''}
              onFocus={() => {
                GA.event({
                  category: 'cart',
                  action: 'cart-inputs-focus',
                  label: 'phone',
                });
              }}
              onChange={value => {
                actions.setUserPhone(value);
              }}
              onBlur={saveContact}
            />
          </Grid.Column>
        </Grid.Row>
      </>
    </Grid>
  );
}

export default withAppConsumer(translate(['home'])(CartPerson));

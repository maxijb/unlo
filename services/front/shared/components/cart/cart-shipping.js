import React, {useState, useMemo} from 'react';
import {translate} from 'react-i18next';
import {Grid} from 'semantic-ui-react';
import classnames from 'classnames';
import {DefaultShippingMethod, ShippingMethod} from '@Common/constants/app';

import css from './cart-list.scss';
function CartShipping({cart, user, actions, api, t, openCartModal}) {
  const shippingMethod = useMemo(() => {
    const sellers = new Set(cart.items.map(it => it.seller_id));

    const methods = new Set();

    for (const seller_id of sellers) {
      const cartItem = cart.items.find(it => it.type === 'shipping' && it.seller_id === seller_id);
      const method =
        cart.shippingMethodsBySeller[seller_id]?.find(
          s => s.id === cartItem?.variant_id && s.id !== null,
        ) ||
        cart.shippingMethodsBySeller[seller_id]?.[0] ||
        DefaultShippingMethod;

      methods.add(
        t('common:XdayShipping', {
          ...method,
          carrier: t(`common:${method.carrier}`),
          context: !Number(method.price) ? 'free' : '',
        }),
      );
    }

    if (!methods.size) {
      return t('common:XdayShipping', {
        ...DefaultShippingMethod,
        carrier: t(`common:${DefaultShippingMethod.carrier}`),
        context: !Number(DefaultShippingMethod.price) ? 'free' : '',
      });
    } else if (methods.size === 1) {
      return Array.from(methods)[0];
    }

    return t('common:shippingMethod.mixed');
  }, [cart.items]);

  return (
    <Grid columns={16}>
      <>
        <Grid.Row>
          <Grid.Column width={16}>
            <div className={classnames(css.shippingBanner, css.cartInput)} onClick={openCartModal}>
              <a>{shippingMethod}</a>
              <span className={css.modifyButton}>{t('modify')}</span>
            </div>
          </Grid.Column>
        </Grid.Row>
      </>
    </Grid>
  );
}

export default translate(['home', 'common'])(CartShipping);

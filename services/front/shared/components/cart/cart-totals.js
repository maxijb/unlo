import React, {useMemo} from 'react';
import {translate} from 'react-i18next';
import Price from '../product/price';
import classnames from 'classnames';
import {DefaultCurrency} from '@Common/constants/app';

import styles from '@Common/styles/styles.scss';
import css from './cart-totals.scss';

function CartTotals({
  cart,
  api,
  t,
  placeOrderButton,
  subtotal,
  subtotalWithoutPromoCode,
  tax,
  total,
  nested,
  promoCode,
  shippingCharges,
  shippingChargesBrokeDown = true,
  includeTotal = true,
}) {
  return (
    <div className={classnames(css.frame, {[css.nested]: nested})}>
      <div className={css.field}>
        <span>{t('subtotal')}</span>
        <Price value={subtotalWithoutPromoCode} currency={DefaultCurrency} />
      </div>
      {!shippingChargesBrokeDown && (
        <div className={css.field}>
          <span>{t('shippingCost')}</span>
          <Price value={shippingCharges} currency={DefaultCurrency} isFree={true} />
        </div>
      )}
      {promoCode && (
        <div className={css.field}>
          <div>
            <div>
              {t('common:promoCode')}:{' '}
              <b className={styles.Emphasis}>
                {promoCode.special_item?.name || t('common:discount')}
              </b>
            </div>
            <div className={classnames(styles.Small, styles.ColorSubtext)}>
              {promoCode.description}
            </div>
          </div>
          <Price value={promoCode.price} className={styles.Emphasis} />
        </div>
      )}
      <div className={css.field}>
        <span>{t('salesTax')}</span>
        {cart.taxRate === null ? (
          <span className={css.minimal}>{t('enterAddressToSeeTax')}</span>
        ) : (
          <Price value={tax} currency={DefaultCurrency} />
        )}
      </div>
      {includeTotal && (
        <div className={classnames(css.field, styles.MediumMedium)}>
          <span>{t('estimatedTotal')}</span>
          <Price value={tax + subtotal} currency={DefaultCurrency} />
        </div>
      )}
      {placeOrderButton}
    </div>
  );
}

export default translate(['home', 'common'])(CartTotals);

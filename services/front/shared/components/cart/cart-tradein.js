import React, {useState} from 'react';
import {withAppConsumer} from '../app-context';
import {translate} from 'react-i18next';
import {Grid, Button} from 'semantic-ui-react';
import AddressInput from '@Components/inputs/address-input';
import WrappedInput from '@Components/inputs/wrapped-input';
import WrappedCheckbox from '@Components/inputs/wrapped-checkbox';
import classnames from 'classnames';
import {RightChevron, TradeIn} from '../../../widget/components/display/svg-icons';
import {textPrice} from '../product/price';

import styles from '@Common/styles/styles.scss';
import css from './cart-tradein.scss';

function CartTradein({cart, actions, t, standAlone = false, className, location, context: {GA}}) {
  const content = cart.tradeInItems?.length ? (
    <div className={styles.FlexGrow}>
      <div className={styles.MediumBold}>
        {t('earnOnTradeIn', {amount: textPrice({value: cart.tradeInItems[0].price})})}
      </div>
      <div className={classnames(styles.Tiny, styles.ColorSubtext, styles.MarginHalfTop)}>
        {t('tradeInWaste')}
      </div>
    </div>
  ) : (
    <div className={styles.FlexGrow}>
      <div className={styles.MediumBold}>{t('earnWithTradeIn')}</div>
      <div className={classnames(styles.Tiny, styles.ColorSubtext, styles.MarginHalfTop)}>
        {t('receiveRefundTodayOrder')}
      </div>
    </div>
  );

  return (
    <Grid columns={16}>
      <>
        <Grid.Row>
          <Grid.Column width={16}>
            <div
              className={classnames(css.tradeinBlock, className, {[css.standAlone]: standAlone})}
              onClick={() => {
                GA.event({
                  category: 'trade-in',
                  action: 'open',
                  label: location || 'cart',
                });
                actions.setIsOpenTradeIn(true);
              }}
            >
              <div className={styles.FlexSpaceBetween}>
                {standAlone && (
                  <div className={classnames(styles.FlexNoShrink, styles.MarginRight)}>
                    <TradeIn />
                  </div>
                )}
                {content}
                {!standAlone && (
                  <div className={styles.circularButton}>
                    <RightChevron color="white" size={30} />
                  </div>
                )}
              </div>
              {standAlone && (
                <div className={styles.MarginTop}>
                  <Button fluid primary>
                    {t('checkPrices')}
                  </Button>
                </div>
              )}
            </div>
          </Grid.Column>
        </Grid.Row>
      </>
    </Grid>
  );
}

export default withAppConsumer(translate(['home'])(CartTradein));

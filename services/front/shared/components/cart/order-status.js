import React, {useMemo} from 'react';
import {CartStatus} from '@Common/constants/app';
import {translate} from 'react-i18next';
import classnames from 'classnames';
import {isReturnCart} from '@Common/utils/order-utils';

import styles from '@Common/styles/styles.scss';
import css from './order-status.scss';

function OrderStatus({order, t, className}) {
  let item = order;
  if (Array.isArray(order)) {
    item = order[0];
  }
  if (!item) {
    return null;
  }

  /*
  .bar_3,
.bar_-1,
.bar_-2,
.bar_-3,
.bar_-4 {
  width: 33%;
  animation: w33 1s ease forwards;
}
.bar_-6,
.bar_-5,
.bar_4 {
  width: 66%;
  animation: w66 1s ease forwards;
}
.bar_5,
.bar_-7 {
  width: 100%;

*/

  const step = useMemo(() => {
    switch (item.status) {
      case CartStatus.requestedReturn:
        return 0;
      case CartStatus.inOrder:

      case CartStatus.returnLabelSent:
      case CartStatus.inTransitReturn:
        return 1;
      case CartStatus.inTransit:
      case CartStatus.returnAwaitingRefund:
        return 2;
      case CartStatus.delivered:
      case CartStatus.returnedSettled:
        return 3;
      default:
        return 0;
    }
  }, [item.status]);

  let content = null;
  if (item.status === CartStatus.cancelledByUser || item.status === CartStatus.cancelledBySeller) {
    content = (
      <div className={classnames(styles.W100, styles.Centered)}>This order has been cancelled</div>
    );
  } else if (!isReturnCart(item)) {
    content = (
      <>
        <div>
          {t('paid')}
          {/*<div className={css.date}>{new Intl.DateTimeFormat('en-US').format(Date.now())}</div>*/}
        </div>
        <div className={classnames({[css.not]: step < 1})}>
          {t('processing')}
          {/*<div className={css.date}>{new Intl.DateTimeFormat('en-US').format(Date.now())}</div>*/}
        </div>
        <div className={classnames({[css.not]: step < 2})}>{t('shipped')}</div>
        <div className={classnames({[css.not]: step < 3})}>{t('delivered')}</div>
      </>
    );
  } else {
    content = (
      <>
        <div>
          {t('requestedReturn')}
          {/*<div className={css.date}>{new Intl.DateTimeFormat('en-US').format(Date.now())}</div>*/}
        </div>

        <div className={classnames({[css.not]: step < 1})}>{t('labelSent')}</div>
        <div className={classnames({[css.not]: step < 2})}>{t('received')}</div>
        <div className={classnames({[css.not]: step < 3})}>{t('refunded')}</div>
      </>
    );
  }

  return (
    <div className={className}>
      <div className={css.barHolder}>
        <div className={classnames(css.bar, css[`bar_${step}`])} />
      </div>
      <div className={css.labels}>{content}</div>
    </div>
  );
}

export default translate(['home'])(OrderStatus);

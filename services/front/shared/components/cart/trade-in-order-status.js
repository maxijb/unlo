import React, {useMemo} from 'react';
import {CartStatus} from '@Common/constants/app';
import {translate} from 'react-i18next';
import classnames from 'classnames';

import styles from '@Common/styles/styles.scss';
import css from './order-status.scss';

function TradeInOrderStatus({order, t, className}) {
  let item = order;
  if (Array.isArray(order)) {
    item = order[0];
  }
  if (!item) {
    return null;
  }

  const step = useMemo(() => {
    switch (item.status) {
      case CartStatus.tradeInInOrder:
      case CartStatus.tradeInSentConfirmation:
      case CartStatus.tradeInSentLabel:
        return 1;
      case CartStatus.tradeInReceived:
      case CartStatus.tradeInPendingPayment:
        return 2;
      case CartStatus.tradeInPayed:
        return 3;
      default:
        return 0;
    }
  }, [item.status]);

  return (
    <div className={className}>
      <div className={css.barHolder}>
        <div className={classnames(css.bar, css[`bar_${step}`])} />
      </div>
      <div className={css.labels}>
        <div>
          {t('requested')}
          {/*<div className={css.date}>{new Intl.DateTimeFormat('en-US').format(Date.now())}</div>*/}
        </div>
        <div>
          {t('inTransit')}
          {/*<div className={css.date}>{new Intl.DateTimeFormat('en-US').format(Date.now())}</div>*/}
        </div>
        <div className={css.not}>{t('received')}</div>
        <div className={css.not}>{t('reimbursed')}</div>
      </div>
    </div>
  );
}

export default translate(['home'])(TradeInOrderStatus);

import React, {PureComponent} from 'react';
import classnames from 'classnames';
import {translate} from 'react-i18next';
import {
  AreaSeries,
  XYPlot,
  MarkSeries,
  HorizontalGridLines,
  LineSeries,
  LineMarkSeries,
  Hint,
  YAxis,
  XAxis
} from 'react-vis';
import {Colors} from '@Common/constants/charts';
import {AppColors} from '@Common/constants/app';
import {getChartSelector} from '../../selectors/charts-selectors';
import css from './trend-chart.scss';

const defaultMarginWithAxis = {left: 30, top: 10, bottom: 20, right: 50};
const defaultMargin = 10;

const yAxisStyle = {
  line: {
    stroke: AppColors.wiriGray,
    strokeWidth: '1px'
  },
  text: {
    fontSize: 10,
    fontWeight: 'normal',
    fill: AppColors.white60,
    textAlign: 'left'
  }
};

const horizontalLinesStyle = {
  stroke: AppColors.wiriGray
};

const xAxisStyle = {
  line: {
    stroke: '#cccccc',
    strokeWidth: '1px'
  },
  text: {
    fontSize: 10,
    fontWeight: 'normal',
    fill: AppColors.white60,
    transform: 'translateY(11px)'
  }
};

class TrendChart extends PureComponent {
  static defaultProps = {
    showAxis: true,
    showArea: true,
    lookback: 'daily',
    height: 200,
    width: 500
  };

  constructor(props) {
    super(props);
    this.state = {selectedMark: null};
  }

  componentDidMount() {
    this.setWidth();
    setTimeout(this.setWidth, 100);
    window.addEventListener('resize', this.setWidth);
  }

  componentDidUpdate(oldProps) {
    if (oldProps.data !== this.props.data) {
      this.setWidth();
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setWidth);
  }

  selectNullMark = () => {
    this.setState({selectedMark: null});
  };

  selectMark = mark => {
    this.setState({selectedMark: mark || null});
  };

  setWidth = () => {
    if (this.refs.container) {
      this.setState({width: this.refs.container.clientWidth});
    }
  };

  renderChart() {
    const {
      data,
      data: {items = [], start, end, total},
      showAxis,
      showArea,
      lookback,
      width,
      height,
      isTablet,
      isMobile
    } = this.props;

    const {selectedMark, width: stateWidth} = this.state;
    const {max, chartItems} = getChartSelector(data);
    const yDomain = [0, max + 1 || 10];
    const xDomain = [new Date(start).getTime(), new Date(end).getTime()];

    return (
      <XYPlot
        onMouseLeave={this.selectNullMark}
        height={height}
        width={stateWidth || width}
        yDomain={yDomain}
        xDomain={xDomain}
        margin={showAxis ? defaultMarginWithAxis : defaultMargin}
        style={{cursor: 'pointer'}}
      >
        {showAxis && (
          <YAxis
            tickSize={0}
            tickTotal={Math.min(max + 1, 5)}
            tickFormat={label => (!isNaN(label) ? Math.round(label) : label)}
            width={30}
            position="end"
            style={yAxisStyle}
          />
        )}
        <HorizontalGridLines style={horizontalLinesStyle} />
        {showAxis && (
          <XAxis
            tickSize={0}
            tickTotal={isMobile || isTablet ? 8 : 15}
            tickFormat={label => {
              const date = new Date(label);
              return `${date.getMonth() + 1}/${date.getDate()}`;
            }}
            position="end"
            style={xAxisStyle}
          />
        )}
        {showArea && <AreaSeries data={chartItems} color={Colors.area} opacity={0.5} />}
        <LineSeries data={chartItems} color={Colors.main} stroke={Colors.main} />
        <MarkSeries
          data={chartItems}
          onNearestXY={this.selectMark}
          color={Colors.main}
          stroke={Colors.main}
          size={4}
        />
        {selectedMark && (
          <MarkSeries
            style={{pointerEvents: 'none'}}
            data={[selectedMark]}
            color={AppColors.accent}
            stroke={AppColors.accent}
            size={4}
          />
        )}
        {selectedMark && (
          <Hint
            value={selectedMark}
            align={{horizontal: 'auto', vertical: 'top'}}
            className={css.tooltip}
          >
            {new Date(selectedMark.x).toDateString()}
          </Hint>
        )}
      </XYPlot>
    );
  }

  renderPreviousPeriodComparison() {
    const {data = {}, t} = this.props;
    const {total, previousPeriod} = data;

    if (!previousPeriod) {
      return null;
    }

    const sign = !total ? '' : previousPeriod >= total ? '+' : '-';
    const num = !total ? previousPeriod : (Math.abs(previousPeriod - total) * 100) / previousPeriod;

    return (
      <div className={css.previousPeriod}>
        <span
          className={classnames(css.numbers, {
            [css.negative]: sign === '-'
          })}
        >
          {sign}
          {Math.round(num)}
          {total ? '%' : ''}
        </span>
        <span> {t('sincePrevious30Days')}</span>
      </div>
    );
  }

  render() {
    const {title, data, height} = this.props;

    return (
      <div ref="container" className={css.chartFrame}>
        <div className={css.chartTitle}>
          <div>
            {title}: {data.total}
          </div>
          {this.renderPreviousPeriodComparison()}
        </div>
        <div style={{minHeight: height}} className={css.chartHolder}>
          {this.state.width && this.renderChart()}
        </div>
      </div>
    );
  }
}

export default translate(['dashboard'])(TrendChart);

/*
              {target !== null &&
                <HorizontalGridLines
                  tickTotal={1}
                  tickValues={[target]}
                  style={{stroke: 'black', strokeDasharray: '3, 3'}}/>}
              {showArea &&
                <AreaSeries
                  data={data}
                  color={passed ? Superfine.green : Superfine.orange}
                  opacity={0.75}
                />
              }
              {
                lookback === LookbackStatus[0] &&
                <MarkSeries
                  data={data}
                  onNearestXY={this.selectMark.bind(this)}
                  color="white"
                  stroke={Superfine['uber-blue']}
                  size={4}/>
              }
              {
                !isEmpty(periods) && lookback !== LookbackStatus[0] &&
                <MarkSeries
                  data={periods}
                  onValueMouseOver={this.selectMark.bind(this)}
                  onValueMouseOut={this.selectMark.bind(this, null)}
                  color="orange"
                  stroke={Superfine['uber-blue']}
                  size={8}/>
              }
              
              {this.renderTooltips()}

*/

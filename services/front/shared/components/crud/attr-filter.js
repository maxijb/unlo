import React, {useMemo, useState} from 'react';
import {translate} from 'react-i18next';
import classnames from 'classnames';
import Link from 'next/link';
import Dropdown from '@Components/inputs/wrapped-dropdown';
import DeleteButton from './delete-button';

import styles from '@Common/styles/styles.scss';
import css from './crud.scss';

function CrudAttr({
  id,
  attributes,
  attributeTypes,
  onSelect,
  typeInputClass,
  onDelete,
  fixedType,
  errors = {},
  t,
}) {
  const attr = attributes[id];
  const [type, setType] = useState(fixedType || attr?.type || null);
  const [selected, setSelected] = useState(attr?.id || '');
  const types = useMemo(() => {
    return attributeTypes.map(a => ({
      value: a,
      text: a,
    }));
  }, [attributeTypes]);

  const options = useMemo(() => {
    if (!type) {
      return [];
    }
    const res = [
      {
        value: '',
        text: t(`dashboard:allFilters.${fixedType}`),
      },
    ];
    Object.keys(attributes).forEach(a => {
      const it = attributes[a];
      if (it.type === type) {
        res.push({
          value: it.id,
          text: it.name,
        });
      }
    });
    return res;
  }, [attributeTypes, type]);

  return (
    <div className={classnames(css.attrInputLong, styles.MarginLeft, styles.MarginBottom)}>
      <Dropdown
        fluid
        error={errors?.[fixedType] || null}
        options={options}
        value={selected}
        placeholder={t(fixedType)}
        onChange={attr_id => {
          onSelect(attr_id);
          setSelected(attr_id);
        }}
      />
    </div>
  );
}

export default translate(['dashboard', 'common'])(CrudAttr);
/*
<Link href={`/dashboard/${type}?id=${item.id}`} as={`/dashboard/${type}/${item.id}`}>
        <a>{item.name}</a>
      </Link>
      */

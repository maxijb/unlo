import React, {useState} from 'react';
import Link from 'next/link';
import Image from '../images/image';
import Attr from './attr';
import AddImageButton from '@Components/inputs/add-image-button';
import Input from '@Components/inputs/wrapped-input';
import {Icon} from 'semantic-ui-react';
import ConfirmationModal from '@Components/modals/confirmation-modal';

import css from './crud.scss';
function CrudAttrImage({
  item,
  image,
  attributes,
  attributeTypes,
  api,
  actions,
  images,
  imageMap,
  onSelect,
  imagesTotalCount,
}) {
  const [order, setOrder] = useState(item.order || '');
  const [isDeleting, setIsDeleting] = useState(false);
  const onDelete = () => {
    api.crud.categoryImageDelete({id: item.id});
  };
  return (
    <div class={css.line}>
      {image?.name ? (
        <div
          className={css.imageToDelete}
          onClick={() => api.crud.categoryImageUpdate({id: item.id, img_id: null})}
        >
          <Image size={50} file={image?.name} />
        </div>
      ) : (
        <AddImageButton
          api={api}
          actions={actions}
          images={images}
          imageMap={imageMap}
          imagesTotalCount={imagesTotalCount}
          onSelect={img_id => api.crud.categoryImageUpdate({id: item.id, img_id})}
        />
      )}
      <div className={css.attrInput}>
        <Input
          value={order}
          fluid
          placeholder="Order"
          onChange={setOrder}
          onBlur={() => api.crud.categoryImageUpdate({id: item.id, order})}
        />
      </div>
      <Attr
        id={item.attr1}
        attributes={attributes}
        attributeTypes={attributeTypes}
        onSelect={attr_id => api.crud.categoryImageUpdate({id: item.id, attr1: attr_id})}
      />
      <Attr
        id={item.attr2}
        attributes={attributes}
        attributeTypes={attributeTypes}
        onSelect={attr_id => api.crud.categoryImageUpdate({id: item.id, attr2: attr_id})}
      />
      <Attr
        id={item.attr3}
        attributes={attributes}
        attributeTypes={attributeTypes}
        onSelect={attr_id => api.crud.categoryImageUpdate({id: item.id, attr3: attr_id})}
      />
      <Icon
        className={css.clickable}
        color="red"
        name="remove circle"
        onClick={() => setIsDeleting(true)}
      />
      {isDeleting && (
        <ConfirmationModal
          onConfirm={onDelete}
          onClose={() => setIsDeleting(false)}
          message={'Are you sure you want to delete this item?'}
          title={'Delete item'}
        />
      )}
    </div>
  );
}

export default CrudAttrImage;

/*
<Link href={`/dashboard/${type}?id=${item.id}`} as={`/dashboard/${type}/${item.id}`}>
        <a>{item.name}</a>
      </Link>*/

import React, {useMemo, useState} from 'react';
import Link from 'next/link';
import Dropdown from '@Components/inputs/wrapped-dropdown';
import DeleteButton from './delete-button';

import css from './crud.scss';

function CrudAttr({
  id,
  attributes,
  attributeTypes,
  onSelect,
  typeInputClass,
  onDelete,
  fixedType,
  errors = {},
}) {
  const attr = attributes[id];
  const [type, setType] = useState(fixedType || attr?.type || null);
  const [selected, setSelected] = useState(attr?.id || null);
  const types = useMemo(() => {
    return attributeTypes.map(a => ({
      value: a,
      text: a,
    }));
  }, [attributeTypes]);

  const options = useMemo(() => {
    if (!type) {
      return [];
    }
    const res = [];
    Object.keys(attributes).forEach(a => {
      const it = attributes[a];
      if (it.type === type) {
        res.push({
          value: it.id,
          text: it.name,
        });
      }
    });
    return res;
  }, [attributeTypes, type]);

  return (
    <div className={css.line}>
      <div className={typeInputClass || css.attrInput}>
        {fixedType || (
          <Dropdown
            fluid
            options={types}
            value={type}
            placeholder={'Type'}
            onChange={val => setType(val)}
          />
        )}
      </div>
      <div className={css.attrInputLong}>
        <Dropdown
          fluid
          error={errors?.[fixedType] || null}
          options={options}
          value={selected}
          placeholder={'Attribute'}
          onChange={attr_id => {
            onSelect(attr_id);
            setSelected(attr_id);
          }}
        />
      </div>
      {!fixedType && onDelete && <DeleteButton onDelete={onDelete} />}
    </div>
  );
}

export default CrudAttr;

/*
<Link href={`/dashboard/${type}?id=${item.id}`} as={`/dashboard/${type}/${item.id}`}>
        <a>{item.name}</a>
      </Link>
      */

import React, {useState} from 'react';
import Link from 'next/link';
import Input from '@Components/inputs/wrapped-input';
import {Icon} from 'semantic-ui-react';
import ConfirmationModal from '@Components/modals/confirmation-modal';

import css from './crud.scss';

function Child({item, type, onChange, onDelete, showActions = true}) {
  const [isEditing, setIsEditing] = useState(false);
  const [isDeleting, setIsDeleting] = useState(false);
  const [name, setName] = useState(item?.name || '');

  const onClose = () => setIsDeleting(false);
  return (
    <div className={css.line}>
      {!item?.name || isEditing ? (
        <Input
          value={name}
          placeholder="Name"
          onChange={n => setName(n)}
          onBlur={async val => {
            await onChange({name: val});
            setIsEditing(false);
          }}
          submitOnEnter={true}
          onSubmit={async val => {
            await onChange({name: val});
            setIsEditing(false);
          }}
        />
      ) : (
        <>
          <Link href={`/dashboard/${type}?id=${item?.id}`} as={`/dashboard/${type}/${item?.id}`}>
            <a>{item?.name}</a>
          </Link>
          {showActions && (
            <>
              <Icon
                className={css.clickable}
                color="blue"
                name="edit"
                onClick={() => setIsEditing(true)}
              />
              <Icon
                className={css.clickable}
                color="red"
                name="remove circle"
                onClick={() => setIsDeleting(true)}
              />
            </>
          )}
        </>
      )}
      {isDeleting && (
        <ConfirmationModal
          onConfirm={onDelete}
          onClose={onClose}
          message={'Are you sure you want to delete this item?'}
          title={'Delete item'}
        />
      )}
    </div>
  );
}

export default Child;

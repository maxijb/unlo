import React, {useMemo, useState} from 'react';
import Link from 'next/link';
import {Button} from 'semantic-ui-react';
import isEmpty from 'is-empty';
import Dropdown from '@Components/inputs/wrapped-dropdown';
import Input from '@Components/inputs/wrapped-input';
import Image from '@Components/images/image';
import classnames from 'classnames';
import CrudAttr from './attr';
import ErrorRenderer from '@Components/inputs/error-renderer';
import AddButton from '@Components/inputs/add-button';
import FormValidations from '@Common/constants/form-validations';
import {ValidateForm} from '@Common/utils/validations';

import styles from '@Common/styles/styles.scss';
import css from './crud.scss';

function CreateCrudProduct({
  product = {},
  attributes,
  attributeTypes,
  api,
  user,
  onClose,
  category,
}) {
  const [source_id, setSourceId] = useState('');
  const [stock, setStock] = useState(0);
  const [attrs, setAttrs] = useState({});
  const [price, setPrice] = useState(0);
  const [errors, setErrors] = useState(null);

  const onSave = async () => {
    const input = {price, stock, source_id, ...attrs, category_id: category.id};
    const errs = ValidateForm(input, FormValidations.newProductSeller);
    setErrors(errs);
    if (!isEmpty(errs)) {
      return;
    }
    const response = await api.crud.sellerCreateProduct({input});
    if (response.resp.error) {
      const serverErrors = !response.resp.error ? {} : {form: {...response.resp}};
      setErrors(serverErrors);
    } else {
      onClose();
    }
  };

  return (
    <>
      <div className={classnames(css.line, css.top, styles.sectionWithBackground)}>
        <Image file={product.image} size={100} />
        <div>
          <div className={classnames(css.line, styles.MediumBold)}>{category.name}</div>
          <div className={css.line}>
            <label>Stock</label>
            <Input
              value={stock}
              placeholder="Stock"
              name="stock"
              onChange={setStock}
              error={errors?.stock}
            />
          </div>
          <div className={css.line}>
            <label>Price</label>
            <Input
              value={price}
              placeholder="Price"
              name="price"
              onChange={setPrice}
              error={errors?.price}
            />
          </div>
          <div className={css.line}>
            <label>Seller ID</label>
            <Input
              value={source_id}
              placeholder="Seller's product own Id"
              name="source_id"
              onChange={setSourceId}
            />
          </div>
        </div>
        <div>
          {['capacity', 'color', 'carrier', 'appearance'].map(attr => (
            <CrudAttr
              errors={errors}
              id={attr}
              key={attr}
              fixedType={attr}
              attributes={attributes}
              attributeTypes={attributeTypes}
              typeInputClass={classnames(css.attrInputLong)}
              onSelect={attr_id => {
                setAttrs({
                  ...attrs,
                  [attr]: attr_id,
                });
              }}
            />
          ))}
        </div>
        <div style={{flexGrow: 1, textAlign: 'right'}}>
          <Button onClick={onSave}>Save</Button>
          <ErrorRenderer error={errors?.form} />
        </div>
      </div>
    </>
  );
}

export default CreateCrudProduct;

import React, {useMemo, useState} from 'react';
import Link from 'next/link';
import {Trans} from 'react-i18next';
import {translate} from 'react-i18next';
import {Button, Popup, Table, Icon} from 'semantic-ui-react';
import Dropdown from '@Components/inputs/wrapped-dropdown';
import Input from '@Components/inputs/wrapped-input';
import Image from '@Components/images/image';
import Price from '@Components/product/price';

import classnames from 'classnames';
import {CartStatusColor, BillableTypes} from '@Common/constants/app';
import CrudAttr from './attr';
import AddButton from '@Components/inputs/add-button';
import {Truck} from '../../../widget/components/display/svg-icons';
import {getUTCDateTimeAsString, localDate} from '@Common/utils/date-utils';

import styles from '@Common/styles/styles.scss';
import css from './crud.scss';

function CrudBillable({billable, t}) {
  let description = billable.description;
  if (!description) {
    description = billable.order_id ? (
      <Trans i18nKey={`billableEvents.${billable.type}`} values={billable}>
        text <a href={`/dashboard/orders?id=${billable.order_id}`}>jnd</a> sjnj
      </Trans>
    ) : (
      t(`billableEvents.${billable.type}`, {...billable, context: 'plain'})
    );
    //description = t(`billableEvents.${billable.type}`, billable);
  }

  return (
    <Table.Row verticalAlign="left" style={{height: 60}}>
      <Table.Cell width={1}>{billable.id}</Table.Cell>
      <Table.Cell textAlign="left" width={3}>
        {localDate(billable.createdAt)}
      </Table.Cell>
      <Table.Cell
        textAlign="left"
        width={6}
        className={classnames({
          [styles.ColorAccent]: billable.type === BillableTypes.paymentToSeller,
          [styles.Button]: billable.type === BillableTypes.paymentToSeller,
        })}
      >
        {description}
      </Table.Cell>
      <Table.Cell textAlign="right" width={2}>
        {(billable.gross || billable.commission) && (
          <Popup
            trigger={<Icon name="plus circle" color={'blue'} />}
            content={
              <div>
                <div>
                  Gross: <Price value={billable.gross} />
                </div>
                <div>
                  Fees: <Price value={billable.commission} />
                </div>
              </div>
            }
          />
        )}
        {Boolean(billable.credit) && <Price value={billable.credit} />}
      </Table.Cell>
      <Table.Cell textAlign="right" width={2}>
        {billable.debit && <Price value={-billable.debit} />}
      </Table.Cell>
      <Table.Cell textAlign="right" width={2}>
        <b>{<Price value={billable.balance} />}</b>
      </Table.Cell>
    </Table.Row>
  );
}

export default translate(['dashboard', 'common'])(CrudBillable);

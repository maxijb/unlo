import React, {useMemo, useState, useEffect} from 'react';
import Link from 'next/link';
import isEmpty from 'is-empty';
import {translate} from 'react-i18next';
import {Button, Popup, Table, Confirm, Select, Icon} from 'semantic-ui-react';

import Dropdown from '@Components/inputs/wrapped-dropdown';
import FileOrLinkInput from '@Components/inputs/file-or-link-input';
import Input from '@Components/inputs/wrapped-input';
import Image from '@Components/images/image';
import Price from '@Components/product/price';
import OrderAddress from '@Components/order/order-address';
import OrderTotals from '@Components/order/order-totals';

import OrderProduct from '@Components/order/order-product';
import classnames from 'classnames';
import {CartStatusColor, CartStatus, Actors, DropshippingSellerID} from '@Common/constants/app';
import CrudAttr from './attr';
import AddButton from '@Components/inputs/add-button';
import {Truck} from '../../../widget/components/display/svg-icons';
import {getUTCDateAsString, localDate, localTime} from '@Common/utils/date-utils';
import {getActualOrderTotals, getDisplayOrderTotals, isReturnCart} from '@Common/utils/order-utils';
import {getPostCarrierTrackingUrl} from '@Common/utils/urls';
import {ValidateForm} from '@Common/utils/validations';
import FormValidations from '@Common/constants/form-validations';

import styles from '@Common/styles/styles.scss';
import css from './crud.scss';

function CrudOrder({order, user, api, t, crud, forceExpanded}) {
  const {
    total,
    shippingTotal,
    statuses,
    tracking,
    trackingCarriers,
    returnLabels,
    tax,
    subtotal,
    shippingDescription,
    shippingBySeller,
    displayTotals,
  } = useMemo(() => {
    const actual = getActualOrderTotals(order);

    let displayTotals = null;
    if (user.superAdmin) {
      displayTotals = getDisplayOrderTotals(order);
    }
    return {
      ...actual,
      displayTotals,
    };
  }, [order]);

  const expandedOrder = useMemo(() => {
    return {
      ...order,
      total,
      subtotal,
      tax,
      shipping: !shippingTotal
        ? null
        : {
            price: shippingTotal,
            special_item: shippingDescription,
          },
    };
  }, [order, total, subtotal, tax, shippingTotal, shippingDescription]);

  const [returnOrders, setReturnOrders] = useState(false);

  const canReturnToSender = useMemo(() => {
    return order.cart.some(p => p.type === 'product' && p.status === CartStatus.inTransit);
  }, [order.cart]);

  const canCancel = useMemo(() => {
    return order.cart.some(p => p.type === 'product' && p.status === CartStatus.inOrder);
  }, [order.cart]);

  const includeSeller = useMemo(() => {
    return order.cart.some(p => p.seller_id !== DropshippingSellerID);
  }, [order.cart]);

  const isReturn = useMemo(() => {
    return order.cart.some(isReturnCart);
  }, [order.cart]);

  const statusOptions = useMemo(() => {
    return Object.values(CartStatus).map(s => {
      return {
        key: s,
        value: s,
        text: (
          <div key={s} style={{color: CartStatusColor[s]}} className={css.statusLabel}>
            <span>{t(`common:cartStatus.${s}`)}</span>
            <div
              className={css.statusLabelBackground}
              style={{backgroundColor: CartStatusColor[s]}}
            />
          </div>
        ),
      };
    });
  }, []);

  const [editReturn, setEditReturn] = useState(false);
  const [tempReturn, setTempReturn] = useState('');
  const [returnError, setReturnError] = useState(null);

  const [editTracking, setEditTracking] = useState(false);
  const [tempTracking, setTempTracking] = useState('');
  const [tempTrackingCarrier, setTempTrackingCarrier] = useState('USPS');
  const [expanded, setExpanded] = useState(false);
  const [isCancelling, setIsCancelling] = useState(false);

  const toggleExpanded = async () => {
    setExpanded(!expanded);
    if (returnOrders === false) {
      const response = await api.order.getReturnOrder({id: order.id});
      setReturnOrders(response?.resp?.orders || null);
    }
  };

  useEffect(() => {
    if (forceExpanded) {
      toggleExpanded();
    }
  }, []);

  const returnOrdersPerCart = {};

  // bring this back when there's time
  //useMemo(() => {
  //  return (returnOrders || []).reduce((agg, it) => {
  //    const {cart_id, ...data} = it;
  //    cart_id.forEach(c => {
  //      agg[c] = data;
  //    });
  //    return agg;
  //  }, {});
  //}, [returnOrders]);

  return (
    <>
      <Table.Row verticalAlign="center" style={{height: 60}}>
        <Table.Cell>
          <a onClick={toggleExpanded}>
            <b>{order.id}</b>
          </a>
        </Table.Cell>
        <Table.Cell textAlign="left">
          {order.firstName} {order.lastName}
        </Table.Cell>
        <Table.Cell textAlign="left">
          <div>{localDate(order.createdAt)}</div>
          <div className={classnames(styles.ColorSubtext, styles.Small)}>
            {localTime(order.createdAt)}
          </div>
        </Table.Cell>
        <Table.Cell textAlign="left">
          <Price value={total} className={styles.Button} />
          {user.superAdmin && displayTotals?.total && displayTotals.total !== total && (
            <span className={styles.ColorSubtext}>
              {' '}
              (<Price value={displayTotals.total} />)
            </span>
          )}
          {shippingTotal > 0 && (
            <Popup
              content={
                <div>
                  Includes <Price value={shippingTotal} /> in shipping costs
                </div>
              }
              trigger={
                <div
                  style={{
                    display: 'inline-block',
                    transform: 'translateY(5px)',
                    cursor: 'pointer',
                  }}
                >
                  <Truck color="blue" size={18} className={styles.MarginHalfLeft} />
                </div>
              }
            />
          )}
          {user.superAdmin && includeSeller && (
            <Popup
              content={<div>Fullfilled by seller</div>}
              trigger={
                <div
                  style={{
                    display: 'inline-block',
                    cursor: 'pointer',
                  }}
                >
                  <Icon name="bullhorn" color="blue" size={18} className={styles.MarginHalfLeft} />
                </div>
              }
            />
          )}
        </Table.Cell>
        <Table.Cell textAlign="left">
          <>
            {(statuses.length > 1 || !user.superAdmin) &&
              statuses.map(s => (
                <div key={s} style={{color: CartStatusColor[s]}} className={css.statusLabel}>
                  <span>{t(`common:cartStatus.${s}`)}</span>
                  <div
                    className={css.statusLabelBackground}
                    style={{backgroundColor: CartStatusColor[s]}}
                  />
                </div>
              ))}

            {statuses.length === 1 && user.superAdmin && (
              <Dropdown
                value={statuses[0]}
                options={statusOptions}
                onChange={val => {
                  api.crud.updateOrderStatus({status: val, order_id: order.id});
                }}
              />
            )}
          </>
        </Table.Cell>
        <Table.Cell textAlign="left" width={5}>
          {tracking.length > 0 &&
            !editTracking &&
            tracking.map((tid, i) => (
              <div key={tid} className={classnames(styles.FlexCenter)}>
                <span
                  className={classnames(
                    styles.TinyBold,
                    styles.FlexNoShrink,
                    styles.MarginHalfRight,
                  )}
                >
                  SHIPPING{' '}
                </span>
                <a
                  href={getPostCarrierTrackingUrl(trackingCarriers[i], tid)}
                  target="_blank"
                  rel="noopener noreferer"
                  className={css.orderShippingLabel}
                >
                  {tid}
                </a>
                <a
                  className={classnames(styles.Accent, styles.MarginHalfLeft, styles.TinyBold)}
                  onClick={() => setEditTracking(true)}
                >
                  EDIT
                </a>
              </div>
            ))}
          {(!tracking.length || editTracking) && (
            <Input
              size="mini"
              value={tempTracking}
              fluid
              placeholder={'Add tracking number'}
              action={
                <>
                  <Select
                    compact
                    options={[
                      {key: 'usps', text: 'USPS', value: 'USPS'},
                      {key: 'ups', text: 'UPS', value: 'UPS'},
                      {key: 'fedex', text: 'FEDEX', value: 'FEDEX'},
                    ]}
                    value={tempTrackingCarrier}
                    onChange={(_event, val) => setTempTrackingCarrier(val.value)}
                    size="mini"
                  />
                  <Button
                    onClick={() => {
                      api.crud.saveTracking({
                        tracking: tempTracking,
                        tracking_carrier: tempTrackingCarrier,
                        order_id: order.id,
                      });
                      setEditTracking(false);
                    }}
                  >
                    Save
                  </Button>
                </>
              }
              onChange={setTempTracking}
            />
          )}
          {returnLabels?.length > 0 &&
            !editReturn &&
            returnLabels.map(l => (
              <div key={l} className={classnames(styles.MarginHalfTop, styles.FlexCenter)}>
                <span
                  className={classnames(
                    styles.TinyBold,
                    styles.FlexNoShrink,
                    styles.MarginHalfRight,
                  )}
                >
                  RETURN LABEL{' '}
                </span>
                <a
                  href={l}
                  target="_blank"
                  rel="noopener noreferer"
                  className={css.orderShippingLabel}
                >
                  {l}
                </a>
                <a
                  className={classnames(styles.Accent, styles.MarginHalfLeft, styles.TinyBold)}
                  onClick={() => setEditReturn(true)}
                >
                  EDIT
                </a>
              </div>
            ))}
          {isReturn && (!returnLabels?.length || editReturn) && (
            <div className={styles.MarginHalfTop}>
              <FileOrLinkInput
                size="mini"
                placeholder="Enter return label URL or upload"
                fileInputClassname={css.fileInputButton}
                onChange={setTempReturn}
                accept={'application/pdf,image/*'}
                error={returnError}
                value={tempReturn}
                action={
                  <Button
                    onClick={() => {
                      const errors = ValidateForm(
                        {remote_return_label: tempReturn},
                        FormValidations.addReturnLabel,
                      );
                      setReturnError(errors?.remote_return_label);
                      if (!isEmpty(errors)) {
                        return;
                      }
                      api.crud.addReturnLabel({
                        remote_return_label: tempReturn,
                        order_id: order.id,
                      });
                      setEditReturn(false);
                    }}
                  >
                    Save
                  </Button>
                }
              />
            </div>
          )}
        </Table.Cell>
      </Table.Row>
      {expanded && (
        <>
          <Table.Row className={classnames(css.expanded, 'expanded')}>
            <Table.Cell></Table.Cell>
            <Table.Cell colSpan={4}>
              {(order.cart || []).map(p => {
                if (p.type === 'product') {
                  return (
                    <OrderProduct
                      superAdmin={user.superAdmin}
                      actor={Actors.seller}
                      product={p}
                      inline={true}
                      key={p.id}
                      shipping={shippingBySeller.get(p.seller_id)}
                      seller={crud?.sellers?.[p.seller_id]}
                      showAppearance={true}
                      inAdmin={true}
                      returnOrder={returnOrdersPerCart[p.id]}
                      crud={crud}
                    />
                  );
                }
              })}
              <div className={styles.MarginTop}>
                <OrderTotals
                  order={expandedOrder}
                  products={order.cart}
                  superAdmin={user.superAdmin}
                  displayTotals={displayTotals}
                />
              </div>
            </Table.Cell>
            <Table.Cell width={5} className={styles.Relative}>
              <div className={css.flexVerticalSpaceBetween}>
                <div>
                  <OrderAddress order={order} address={order.address} vertical />
                  <div className={classnames(styles.MarginBottom, styles.MarginTop)}>
                    {user.superAdmin && (
                      <div>
                        <b>Email:</b> {order.email}
                      </div>
                    )}
                    <div>
                      <b>Phone:</b> {order.phone}
                    </div>
                    {crud.orders.sources?.[order.cid] && (
                      <div>
                        <b>Source:</b> {crud.orders.sources[order.cid]}
                      </div>
                    )}
                  </div>
                </div>

                <div className={styles.FlexSpaceBetween}>
                  {canReturnToSender && (
                    <Button
                      primary
                      className={classnames(styles.FlexGrow, styles.FlexAllCenter)}
                      onClick={() => api.crud.returnToSenderSeller({order_id: order.id})}
                    >
                      Flag as Returned To Sender
                    </Button>
                  )}

                  {canCancel && (
                    <Button
                      primary
                      className={classnames(styles.FlexGrow, styles.FlexAllCenter)}
                      onClick={() => setIsCancelling(true)}
                    >
                      Cancel Order
                    </Button>
                  )}
                  <Button className={styles.MarginLeft} onClick={toggleExpanded}>
                    ^
                  </Button>
                </div>
              </div>
            </Table.Cell>
          </Table.Row>
          <Table.Row className={classnames(css.expanded, 'expanded')}></Table.Row>
        </>
      )}
      <Confirm
        open={isCancelling}
        header="Cancel Order"
        content={
          'This is a permanent action to notify the customer that their order cannout be fulfilled. Are you sure?'
        }
        onCancel={() => setIsCancelling(false)}
        onConfirm={() => {
          api.crud.cancelOrderBySeller({order_id: order.id});
          setIsCancelling(false);
        }}
        cancelButton="No, keep the order active"
        confirmButton="Yes, cancel the order"
      />
    </>
  );
}

export default translate(['dashboard', 'common'])(CrudOrder);

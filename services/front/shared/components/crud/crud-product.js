import React, {useMemo, useState} from 'react';
import Link from 'next/link';
import {Button} from 'semantic-ui-react';
import Dropdown from '@Components/inputs/wrapped-dropdown';
import Input from '@Components/inputs/wrapped-input';
import Image from '@Components/images/image';
import classnames from 'classnames';
import CrudAttr from './attr';
import AddButton from '@Components/inputs/add-button';

import styles from '@Common/styles/styles.scss';
import css from './crud.scss';

function CrudProduct({product, attributes, attributeTypes, api, user, seller}) {
  const [stock, setStock] = useState(product.stock);
  const [originalPrice, setOriginalPrice] = useState(product.original_price);
  const [price, setPrice] = useState(product.price);

  const updateProduct = (value, field) => {
    api.crud.productUpdate({value, field, id: product.id});
  };

  return (
    <>
      <div className={classnames(css.line, css.top, css.withBorder)}>
        <Image file={product.image} size={100} />
        <div>{product.name}</div>
        <div>
          <div className={css.line}>
            <label>Stock</label>
            <Input
              value={stock}
              placeholder="Stock"
              name="stock"
              onBlur={updateProduct}
              onChange={setStock}
            />
          </div>
          <div className={css.line}>
            <label>Price</label>
            <Input
              value={price}
              placeholder="Price"
              name="price"
              onBlur={updateProduct}
              onChange={setPrice}
            />
          </div>
          <div className={css.line}>
            <label>Original Price</label>
            <Input
              value={originalPrice}
              onChange={setOriginalPrice}
              placeholder="Original Price"
              name="original_price"
              onBlur={updateProduct}
            />
          </div>
          <div className={classnames(styles.ColorSubtext)}>
            Seller:{' '}
            <b>
              {seller?.name || ''} ({product.owner})
            </b>
          </div>
          <div className={classnames(styles.ColorSubtext)}>
            ID: <b>{product.id}</b>
          </div>
        </div>
        <div>
          {product.attributes.map(attr => (
            <CrudAttr
              id={attr.attr_id}
              key={attr.attr_id}
              attributes={attributes}
              attributeTypes={attributeTypes}
              typeInputClass={classnames(css.attrInputLong)}
              onSelect={attr_id => {
                api.crud.attributeProductUpdate({attr_id, id: attr.id});
              }}
              onDelete={() => {
                api.crud.attributeProductDelete({product_id: attr.product_id, id: attr.id});
              }}
            />
          ))}
          <AddButton
            text="Add attribute"
            onClick={() => {
              api.crud.attributeProductCreate({product_id: product.id});
            }}
          />
        </div>
        <div style={{flexGrow: 1, textAlign: 'right'}}>
          <Button color="red" onClick={() => api.crud.productDelete({id: product.id})}>
            Delete Product
          </Button>
        </div>
      </div>
    </>
  );
}

export default CrudProduct;

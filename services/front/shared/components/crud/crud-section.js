import React, {useMemo, useState} from 'react';
import classnames from 'classnames';
import Link from 'next/link';
import Dropdown from '@Components/inputs/wrapped-dropdown';
import DeleteButton from './delete-button';

import styles from '@Common/styles/styles.scss';
import css from './crud.scss';

function CrudSection({children, title, subtitle}) {
  return (
    <div className={css.section}>
      <div className={css.sectionHeader}>
        <div className={styles.Large}>{title}</div>
        <div className={classnames(styles.ColorSubtext)}>{subtitle}</div>
      </div>
      <div className={css.sectionBody}>{children}</div>
    </div>
  );
}

export default CrudSection;

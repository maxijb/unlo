import React, {useMemo, useState, useEffect} from 'react';
import Link from 'next/link';
import {translate} from 'react-i18next';
import {Button, Table, Icon, Popup} from 'semantic-ui-react';
import {getPageLink} from '@Common/utils/urls';
import Dropdown from '@Components/inputs/wrapped-dropdown';
import Input from '@Components/inputs/wrapped-input';
import IntegerInput from '@Components/inputs/integer-input';
import Price, {textPrice} from '@Components/product/price';
import Image from '@Components/images/image';
import classnames from 'classnames';
import CrudAttr from './attr';
import AddButton from '@Components/inputs/add-button';
import {
  consolidateAttributesAsName,
  getProductNameWithAttributes,
} from '../../utils/attributes-utils';

import styles from '@Common/styles/styles.scss';
import css from './crud.scss';

function CrudSellerProduct({product, attributes, attributeTypes, api, user, t}) {
  const [source_id, setSourceId] = useState(product.source_id);
  const [stock, setStock] = useState(product.stock);
  const [price, setPrice] = useState(product.price);
  const [updating, setUpdating] = useState(null);

  const updateProduct = async (value, field) => {
    setUpdating(field);
    await api.crud.productUpdate({value, field, id: product.id});
    setUpdating(null);
  };

  const data = consolidateAttributesAsName(product.attributes, t);
  const url = getPageLink('variant', {product, t});

  useEffect(() => {
    setStock(product.stock);
  }, [product.stock]);

  useEffect(() => {
    setSourceId(product.source_id);
  }, [product.source_id]);

  useEffect(() => {
    setPrice(product.price);
  }, [product.price]);

  return (
    <Table.Row>
      <Table.Cell>
        <Image file={product.image} size={50} />
      </Table.Cell>
      <Table.Cell>
        <a href={url} target="_blank">
          <div>
            <b>
              {product.name} {t(data.capacity)}
            </b>
          </div>
          <div>
            {t(data.color)} - {t(data.carrier)} - {t(data.appearance)}
          </div>
        </a>
        <div className={classnames(styles.Tiny, styles.ColorSubtext)}>
          <b>ID:</b> {product.id}
        </div>
      </Table.Cell>
      <Table.Cell>
        <Input
          value={source_id}
          placeholder="Source ID/SKU"
          name="source_id"
          onBlur={updateProduct}
          onSubmit={updateProduct}
          submitOnEnter={true}
          onChange={setSourceId}
          className={css.miniInput}
          fluid
          disabled={updating === 'source_id'}
          style={{minWidth: 300}}
        />
      </Table.Cell>
      <Table.Cell>
        <Input
          value={stock}
          placeholder="Stock"
          name="stock"
          onBlur={updateProduct}
          onSubmit={updateProduct}
          submitOnEnter={true}
          onChange={setStock}
          className={css.miniInput}
          disabled={updating === 'stock'}
        />
      </Table.Cell>
      <Table.Cell>
        <Input
          value={price}
          placeholder="Price"
          name="price"
          onBlur={updateProduct}
          onSubmit={updateProduct}
          submitOnEnter={true}
          onChange={setPrice}
          className={css.miniInput}
          disabled={updating === 'price'}
        />
      </Table.Cell>
      <Table.Cell>
        {product.min_price && (
          <div>
            {product.min_price === product.price ? (
              <Popup content={t('inBuyBox')} trigger={<Icon name="check circle" color="green" />} />
            ) : (
              <Popup
                content={t('outOfBuyBox', {price: textPrice({value: product.min_price})})}
                trigger={<Icon name="remove circle" color="red" />}
              />
            )}
            <Price
              value={product.min_price}
              className={classnames(styles.ColorSubtext, styles.MarginHalfLeft)}
            />
          </div>
        )}
      </Table.Cell>
      <Table.Cell>
        <Popup
          content={t('Delete')}
          trigger={
            <Icon
              size="large"
              name="trash"
              color="red"
              onClick={() => api.crud.productDelete({id: product.id})}
            />
          }
        />
      </Table.Cell>
    </Table.Row>
  );
}

export default translate(['dashboard', 'common'])(CrudSellerProduct);

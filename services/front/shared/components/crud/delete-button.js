import React, {useState} from 'react';
import {Icon} from 'semantic-ui-react';
import ConfirmationModal from '@Components/modals/confirmation-modal';

import css from './crud.scss';

export default function DeleteButton({onDelete}) {
  const [isDeleting, setIsDeleting] = useState(false);
  return (
    <>
      <Icon
        className={css.clickable}
        color="red"
        name="remove circle"
        onClick={() => setIsDeleting(true)}
      />
      {isDeleting && (
        <ConfirmationModal
          onConfirm={onDelete}
          onClose={() => setIsDeleting(false)}
          message={'Are you sure you want to delete this item?'}
          title={'Delete item'}
        />
      )}
    </>
  );
}

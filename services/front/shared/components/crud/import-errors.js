import React, {useMemo, useState} from 'react';
import Link from 'next/link';
import {Trans} from 'react-i18next';
import {translate} from 'react-i18next';
import {Button, Popup, Table, Icon} from 'semantic-ui-react';

import Input from '@Components/inputs/wrapped-input';
import Image from '@Components/images/image';
import Price from '@Components/product/price';

import classnames from 'classnames';
import {CartStatusColor, BillableTypes} from '@Common/constants/app';
import CrudAttr from './attr';
import AddButton from '@Components/inputs/add-button';
import {Truck} from '../../../widget/components/display/svg-icons';
import {getUTCDateTimeAsString, localDate} from '@Common/utils/date-utils';

import styles from '@Common/styles/styles.scss';
import css from './crud.scss';

function ImportErrors({errors, totalErrors}) {
  const [isOpen, setIsOpen] = useState(false);

  if (errors === null || !totalErrors || !errors?.length) {
    return null;
  }

  return (
    <div>
      <div className={css.errorsMessage}>
        Import successful but with {totalErrors} errors
        <Icon
          name="plus circle"
          color={'blue'}
          onClick={() => setIsOpen(!isOpen)}
          className={styles.MarginHalfLeft}
        />
      </div>
      {isOpen && errors?.length > 0 && (
        <div className={css.errorsBox}>
          {errors.map(e => (
            <div>{e}</div>
          ))}
          {errors.length < totalErrors && (
            <div>... and {totalErrors - errors.length} more errors.</div>
          )}
        </div>
      )}
    </div>
  );
}

export default translate(['dashboard', 'common'])(ImportErrors);

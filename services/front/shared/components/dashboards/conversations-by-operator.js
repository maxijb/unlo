import React, {PureComponent} from 'react';
import classnames from 'classnames';
import {translate} from 'react-i18next';
import isEmpty from 'is-empty';
import Bubble from './dashboard-bubble';

import Avatar from '@Components/information/avatar';
import css from './dashboard-bubble.scss';

class ConversationByOperator extends PureComponent {
  static defaultProps = {
    data: {}
  };

  renderOwner(owner, member) {
    const {t} = this.props;

    if (!member) {
      return <div>{t('userN', {user: owner})}</div>;
    }

    return (
      <div className={css.verticalAlignCenter}>
        <Avatar src={member.avatar} className={css.avatar} />
        {member.firstName} {member.lastName}
      </div>
    );
  }

  render() {
    const {data, members, t} = this.props;

    return (
      <Bubble title={t('conversationsPerOperator')}>
        <table className={css.table}>
          <tbody>
            {(data.items || []).map(item => {
              const member = members && members.get(item.owner);
              return (
                <tr key={item.owner} className={classnames({[css.withAvatar]: Boolean(member)})}>
                  <td>{this.renderOwner(item.owner, member)}</td>
                  <td className={css.right}>{item.count}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        {isEmpty(data.items) && (
          <div className={css.emptyList}>{t('nothingToShowInThisPeriod')}</div>
        )}
      </Bubble>
    );
  }
}

export default translate(['dashboard'])(ConversationByOperator);

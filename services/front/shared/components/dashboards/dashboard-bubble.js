import React, {PureComponent} from 'react';

import css from './dashboard-bubble.scss';

class DashboardBubble extends PureComponent {
  render() {
    const {title, children} = this.props;

    return (
      <div className={css.bubble}>
        {title && <div className={css.title}>{title}</div>}
        {children}
      </div>
    );
  }
}

export default DashboardBubble;

import React, {PureComponent} from 'react';
import {translate} from 'react-i18next';
import isEmpty from 'is-empty';
import Bubble from './dashboard-bubble';

import css from './dashboard-bubble.scss';

class RankingSessionsByURL extends PureComponent {
  static defaultProps = {
    data: {}
  };

  render() {
    const {data, t} = this.props;

    return (
      <Bubble title={t('conversationsByURLTitle')}>
        <table className={css.table}>
          <tbody>
            {(data.items || []).map(item => {
              return (
                <tr key={item.url}>
                  <td>{item.url}</td>
                  <td className={css.right}>{item.count}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        {isEmpty(data.items) && (
          <div className={css.emptyList}>{t('nothingToShowInThisPeriod')}</div>
        )}
      </Bubble>
    );
  }
}

export default translate(['dashboard'])(RankingSessionsByURL);

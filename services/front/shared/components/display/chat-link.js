import React from 'react';
import {translate} from 'react-i18next';
import {withAppConsumer} from '../app-context';

import TagLink from './tag-link';
import classnames from 'classnames';

import styles from '@Common/styles/styles.scss';
import css from './chat-link.scss';

function ChatLink({t, context: {isMobile}, onlyContent}) {
  const content = (
    <div>
      <div className={css.dot} />
      <img src="/static/incredible/founders.png" className={css.image} alt="chat with an expert" />
      <span className={css.legend}>
        {!isMobile && t('needHelp')} <a>{t('chatWithUs')}</a>
      </span>
    </div>
  );

  return onlyContent ? (
    <div onClick={() => window.Intercom && Intercom('showNewMessage')}>{content}</div>
  ) : (
    <TagLink onClick={() => window.Intercom && Intercom('showNewMessage')} text={content} />
  );
}

export default withAppConsumer(translate(['home'])(ChatLink));

import React from 'react';
import classnames from 'classnames';
import {Grid} from 'semantic-ui-react';
import {translate} from 'react-i18next';

import {withAppConsumer} from '../app-context';
import styles from '@Common/styles/styles.scss';
import css from './cross-selling.scss';

function CrossSelling({t, context: {GA}}) {
  return (
    <Grid>
      <Grid.Row>
        <Grid.Column width={16}>
          <div className={classnames(styles.MarginBottom, styles.MediumBold)}>
            {t('customerAlsoBought')}
          </div>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column width={8}>
          <div
            onClick={() => {
              GA.event({category: 'confirmation', action: 'cross-sell', label: 'akko', value: 1});
              window.open('https://getakko.com/phone-insurance?ref=getunlockd');
            }}
            className={styles.Clickable}
          >
            <div className={css.header}>
              <img src="/static/akko/logo.png" alt="akko insurance" />
              <div>
                <div className={css.company}>AKKO</div>
                <div>
                  <a>Visit store</a>
                </div>
              </div>
            </div>
            <div className={css.product}>
              <img src="/static/akko/product.png" alt="akko insurance" />
              <div>iPhone insurance plan</div>
              <div>
                <b>from $5 a month</b>
              </div>
            </div>
          </div>
        </Grid.Column>
        <Grid.Column width={8}>
          <div
            onClick={() => {
              GA.event({
                category: 'confirmation',
                action: 'cross-sell',
                label: 'casemate',
                value: 1,
              });
              window.open('https://casemate.kxyi.net/qn46PO');
            }}
            className={styles.Clickable}
          >
            <div className={css.header}>
              <img src="/static/casemate/logo.png" alt="Casemate" />
              <div>
                <div className={css.company}>Case Mate</div>
                <div>
                  <a>Visit store</a>
                </div>
              </div>
            </div>
            <div className={css.product}>
              <img src="/static/casemate/product.jpeg" alt="Casemate" />
              <div>
                20% all cases and protectors with code
                <b> "CMSAVE20"</b>
              </div>
            </div>
          </div>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
}

export default withAppConsumer(translate(['home'])(CrossSelling));

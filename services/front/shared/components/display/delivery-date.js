import React from 'react';
import {translate} from 'react-i18next';
import classnames from 'classnames';
import Countdown from 'react-countdown';

import {withAppConsumer} from '@Components/app-context';
import {addWorkingDays, stringDate} from '@Common/utils/date-utils';

import styles from '@Common/styles/styles.scss';

function DeliveryDate({shippingMethodsBySeller, seller_id, t, context: {isMobile}}) {
  const shippingMethods = shippingMethodsBySeller?.[seller_id];

  if (!shippingMethods || !shippingMethods?.[0]?.eta) {
    return null;
  }

  const free = new Date(shippingMethods[0].eta);
  const cut_off = new Date(shippingMethods[0].cut_off);
  const fastest =
    shippingMethods?.length > 1 ? new Date(shippingMethods[shippingMethods.length - 1].eta) : null;

  return (
    <div>
      <div
        className={classnames(styles.NoWrap, {
          [styles.MediumMedium]: !isMobile,
        })}
        style={{marginBottom: 2}}
      >
        <span className={styles.ColorAccent}>{t('freeDelivery')}:</span>{' '}
        <b>{stringDate(free, t)}</b>
      </div>
      {cut_off - Date.now() < 24 * 60 * 60 * 1000 && (
        <div
          className={classnames(styles.ColorSubtext, {
            [styles.Small]: isMobile,
          })}
          style={{marginBottom: 2}}
        >
          {t('orderWithin')}{' '}
          <Countdown
            date={cut_off}
            renderer={({hours, minutes, completed}) => {
              return `${hours}hr ${minutes}min`;
            }}
          />
        </div>
      )}
      {fastest && (
        <div className={classnames(styles.Tiny, styles.ColorSubtext)}>
          {t('fastestDelivery')}: {stringDate(fastest, t)}
        </div>
      )}
    </div>
  );
}

export default withAppConsumer(translate(['home', 'common'])(DeliveryDate));

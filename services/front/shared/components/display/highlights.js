import React from 'react';
import classnames from 'classnames';
import {translate} from 'react-i18next';

import styles from '@Common/styles/styles.scss';
import css from './highlights.scss';

const noop = () => {};

function Highlights({className = '', highlights, t}) {
  return (
    <div className={css.highlights}>
      {highlights.map(H => (
        <div
          key={H.name}
          className={classnames(css.highlight, {[css.selected]: H.selected})}
          onClick={H.onClick || noop}
        >
          <div>{H.icon}</div>
          {t(H.name)}
          {H.learnMore && (
            <div className={styles.MarginHalfTop}>
              <a>{t('learnMore')}</a>
            </div>
          )}
        </div>
      ))}
    </div>
  );
}

export default translate(['home'])(Highlights);

import React from 'react';
import {translate} from 'react-i18next';
import classnames from 'classnames';
import DeliveryDate from './delivery-date';
import Price from '../product/price';

import {withAppConsumer} from '@Components/app-context';

import styles from '@Common/styles/styles.scss';
import css from '../product/sticky-buy.scss';

function MobilePriceDelivery({t, variant, shippingMethodsBySeller}) {
  return (
    <div
      className={classnames(styles.FlexSpaceBetween, css.block)}
      style={{marginLeft: 0, marginRight: 0}}
    >
      <div>
        {variant?.price && variant?.stock ? (
          <Price value={variant.price} className={styles.Huge} />
        ) : (
          t('outOfStock')
        )}
        {variant?.original_price && (
          <div className={css.originalPrice}>
            {t('origPrice')}: <Price value={variant.original_price} className={css.struk} />
          </div>
        )}
      </div>
      <div className={styles.Right}>
        <DeliveryDate
          seller_id={variant?.owner}
          shippingMethodsBySeller={shippingMethodsBySeller}
        />
      </div>
    </div>
  );
}

export default withAppConsumer(translate(['home', 'common'])(MobilePriceDelivery));

import React from 'react';
import classnames from 'classnames';

import css from './separator.scss';

export default function Label({className = '', style = {}}) {
  return <div className={classnames(css.separator, className)} style={style} />;
}

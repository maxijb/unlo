import React from 'react';
import {translate} from 'react-i18next';
import {withAppConsumer} from '../app-context';

import {Star} from '../../../widget/components/display/svg-icons';
import classnames from 'classnames';

import styles from '@Common/styles/styles.scss';
import css from './chat-link.scss';

function Stars({t, size, includeLabel = true, className, context: {isMobile}}) {
  return (
    <div
      className={classnames(styles.FlexCenter, className, {
        [styles.FlexAllCenter]: isMobile || !includeLabel,
      })}
    >
      <Star size={size} />
      <Star size={size} />
      <Star size={size} />
      <Star size={size} />
      <Star size={size} />
      {includeLabel && (
        <span
          className={classnames(styles.ColorSubtext, styles.Small, styles.MarginHalfLeft)}
          style={{
            transform: 'translateY(1px)',
          }}
        >
          200+ reviews
        </span>
      )}
    </div>
  );
}

export default withAppConsumer(translate(['home'])(Stars));

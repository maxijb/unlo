import React from 'react';
import Link from 'next/link';
import {RightChevron} from '../../../widget/components/display/svg-icons';
import classnames from 'classnames';

import styles from '@Common/styles/styles.scss';
import css from './tag-link.scss';

const noop = () => {};
function TagLink({
  text,
  href,
  onClick = noop,
  selected = false,
  chevron = <RightChevron />,
  className,
}) {
  const content = (
    <div
      className={classnames(styles.MarginBottom, css.tagLink, className, {
        [css.selected]: selected,
        [styles.Medium]: !!chevron,
        [css.withChevron]: !!chevron,
      })}
      onClick={onClick}
    >
      {text}
      {chevron}
    </div>
  );

  return href ? <Link href={href}>{content}</Link> : content;
}

export default TagLink;

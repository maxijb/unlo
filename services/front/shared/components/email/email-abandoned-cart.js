import React from 'react';
import EmailLayout from './email-layout';
import {EmailContext} from './email-context';
import {multiLineTrans} from '@Common/utils/generic-utils';
import {getPageLink, absoluteGetPageLink} from '@Common/utils/urls';
import {getDisplayRowValue, getActualRowValue} from '@Common/utils/order-utils';
import {Actors} from '@Common/constants/app';
import {formatDate} from '@Common/utils/date-utils';
import {textPrice, formatAddress} from '@Common/utils/format-utils';
import Address from '../cart/address';
import Image from '../images/image';
import EmailTradeIn, {TextEmailTradeIn} from './email-tradein';
import {cdnURL} from '@Common/utils/statif-assets-utils';
import {Trans} from 'react-i18next';

import {I18nextProvider} from 'react-i18next';
import initialI18nInstance from '../../i18n/i18n';

import styles from './email-styles.js';

export default function EmailAbandonedCart({order: {contact, products, promoCode}, t, config}) {
  const rowValueFn = getDisplayRowValue;

  return (
    <I18nextProvider i18n={initialI18nInstance}>
      <EmailContext.Provider
        value={{
          t,
          config,
        }}
      >
        <EmailLayout>
          <>
            <div style={{...styles.mainTitle, marginBottom: 0}}>
              {t('dashboard:30DayReturns1YearWarranty')}
            </div>
            <img src={cdnURL('/static/incredible/deal-for-you.jpg')} />
            <p style={{...styles.large, ...styles.accent, ...styles.center}}>
              <Trans i18nKey="dashboard:areYouSureLeaveBehind">
                Text
                <b>link</b>
              </Trans>
            </p>
            <p style={styles.center}>
              <Trans i18nKey="dashboard:temporarilyReservedRedeem">
                Text
                <a
                  href={absoluteGetPageLink('cart', {promoCode, cid: contact.cid}, config)}
                  target="blank"
                  rel="noopener"
                >
                  link
                </a>
              </Trans>
            </p>
            <div
              style={{
                marginTop: 24,
                borderTop: '1px solid #ccc',
                marginBottom: 24,
              }}
            />
            <table
              width="100%"
              style={{
                borderCollapse: 'collapse',
              }}
            >
              <tbody>
                {products.map(product => {
                  if (product.type !== 'product') {
                    return null;
                  }
                  return (
                    <tr>
                      <td style={styles.cellNoBorder}>
                        <Image file={product.image} size={150} style={{verticalAlign: 'middle'}} />
                      </td>
                      <td style={styles.cellNoBorder}>
                        <div style={{marginBottom: 16, fontSize: 14}}>
                          <a
                            href={absoluteGetPageLink(
                              'variant',
                              {
                                product: product.product,
                                description: product.description,
                                qs: {promoCode, cid: contact.cid},
                                t,
                              },
                              config,
                            )}
                            target="blank"
                            rel="noopener noreferer"
                          >
                            {product.quantity > 1 && <b>{product.quantity}x </b>}{' '}
                            {product.description}
                          </a>
                        </div>
                        <div>{t('dashboard:free1yrWarranty')}</div>
                        <div>{t('home:freeShippingAndReturn')}</div>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>

            <div style={{...styles.center, marginBottom: '32px', marginTop: '32px'}}>
              <a
                style={styles.button}
                href={absoluteGetPageLink('cart', {qs: {promoCode, cid: contact.cid}}, config)}
                target="blank"
                rel="noopener"
              >
                {t('dashboard:claimYour5Off')}
              </a>
            </div>

            <div style={styles.center}>
              <Trans i18nKey="dashboard:questionsSupportTeam">
                Text
                <a href="mailto:support@incrediblephones.com">link</a>
              </Trans>
            </div>
          </>
        </EmailLayout>
      </EmailContext.Provider>
    </I18nextProvider>
  );
}

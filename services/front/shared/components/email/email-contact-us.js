import React from 'react';
import EmailLayout from './email-layout';
import {EmailContext} from './email-context';

import styles from './email-styles.js';

export default function EmailOrder({query, t, config}) {
  return (
    <EmailContext.Provider
      value={{
        t,
        config,
      }}
    >
      <EmailLayout>
        <>
          <div style={styles.mainTitle}>Contact Us Submission</div>

          <table>
            <tbody>
              {Object.keys(query).map(key => (
                <tr key={key}>
                  <td>{key}</td>
                  <td>{query[key]}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </>
      </EmailLayout>
    </EmailContext.Provider>
  );
}

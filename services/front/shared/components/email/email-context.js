import React, {Component} from 'react';

// first we will make a new context
const EmailContext = React.createContext({t: a => a});

// then make a consumer which will surface it
const EmailConsumer = EmailContext.Consumer;

//export default AppProvider;
export {EmailConsumer, EmailContext};

export function withEmailConsumer(Component) {
  const hoc = function ThemedComponent(props) {
    return (
      <EmailConsumer>
        {context => {
          return <Component {...props} context={context} />;
        }}
      </EmailConsumer>
    );
  };
  return hoc;
}

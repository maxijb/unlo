import React from 'react';
import {withEmailConsumer} from './email-context';
import {cdnURL} from '@Common/utils/statif-assets-utils';

import styles from './email-styles';

function EmailHeader({context}) {
  return (
    <div style={styles.marginBottom}>
      <img src={cdnURL('/static/incredible/logo-email.png')} width="300" />
    </div>
  );
}

export default withEmailConsumer(EmailHeader);

import React from 'react';
import {withEmailConsumer} from './email-context';
import {cdnURL} from '@Common/utils/statif-assets-utils';
import AppColors from '@Common/constants/app';
import EmailHeader from './email-header';
import {UnsubscribeLinkPattern} from '@Common/constants/notifications';

import styles from './email-styles';

function EmailLayout({context: {t}, children}) {
  return (
    <table border="0" cellPadding="0" cellSpacing="0" height="100%" width="100%">
      <tbody>
        <tr>
          <td align="center" valign="top">
            <table
              border="0"
              cellPadding="0"
              cellSpacing="0"
              width="600"
              style={{background: 'white', padding: '16px 0 0 0'}}
            >
              <tbody>
                <tr>
                  <td align="center" valign="top">
                    <EmailHeader />
                  </td>
                </tr>
                <tr>
                  <td style={{padding: '0 16px'}}>{children}</td>
                </tr>
                <tr>
                  <td align="center" valign="top">
                    <div style={{background: '#eee', padding: '16px 0', marginTop: 32}}>
                      <div>
                        <b>IncrediblePhones.com</b>
                      </div>
                      <div style={styles.minimal}>
                        <div>(888) 675-2905 | support@incrediblephones.com</div>
                        <div>2820 Howard Cmns #309 Howard, WI 54313</div>
                      </div>
                      <p style={styles.legal}>
                        {t('dashboard:dontLikeTheseEmails?')}{' '}
                        <a href={UnsubscribeLinkPattern}>{t('dashboard:unsubscribeHere')}</a>
                      </p>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
  );
}

export default withEmailConsumer(EmailLayout);

import React from 'react';
import EmailLayout from './email-layout';
import {EmailContext} from './email-context';
import {multiLineTrans} from '@Common/utils/generic-utils';
import {getDisplayRowValue, getActualRowValue} from '@Common/utils/order-utils';
import {Actors} from '@Common/constants/app';
import {formatDate} from '@Common/utils/date-utils';
import {textPrice, formatAddress} from '@Common/utils/format-utils';
import Address from '../cart/address';
import Image from '../images/image';
import EmailTradeIn, {TextEmailTradeIn} from './email-tradein';

import styles from './email-styles.js';

export default function EmailOrder({
  order: {order, products, address, tradeInOrder, tradeInProducts, promoCode},
  t,
  config,
  actor = 'buyer',
}) {
  const payment = order.payment_description.match(/(.*)\s(\d*)$/);
  const rowValueFn = actor === 'buyer' ? getDisplayRowValue : getActualRowValue;
  const colspan = actor === Actors.seller ? 3 : 2;
  return (
    <EmailContext.Provider
      value={{
        t,
        config,
      }}
    >
      <EmailLayout>
        <>
          <div style={styles.mainTitle}>
            {t(
              actor === 'seller'
                ? 'dashboard:email.newOrderSeller'
                : 'dashboard:email.orderReceived',
            )}
          </div>
          {actor === 'seller' ? (
            <>
              <div>
                {t('dashboard:email.orderReceivedLongSeller1')}
                <a href={`${config.protocol}://${config.domain}/portal/orders`}>
                  {t('dashboard:email.orderReceivedLongSeller1Link')}
                </a>
              </div>
              <div>{multiLineTrans(t('dashboard:email.orderReceivedLongSeller2'))}</div>
            </>
          ) : (
            multiLineTrans(t('dashboard:email.orderReceivedLong', {name: order.firstName}))
          )}
          <p style={{...styles.large, ...styles.accent}}>
            <a href={`${config.protocol}://${config.domain}/portal/orders`}>
              {t('home:orderNumberX', {number: order.id})}
            </a>{' '}
            ({formatDate(order.createdAt)})
          </p>
          <table width="100%" style={{borderCollapse: 'collapse'}}>
            <tbody>
              <tr>
                {actor === Actors.seller && (
                  <td style={styles.cell}>
                    <b>{t('SKU')}</b>
                  </td>
                )}
                <td style={styles.cell}>
                  <b>{t('product')}</b>
                </td>
                <td style={styles.cell} align="center">
                  <b>{t('quantity')}</b>
                </td>
                <td style={styles.cell} align="center">
                  <b>{t('price')}</b>
                </td>
              </tr>
              {products.map(product => {
                if (product.type !== 'product') {
                  return null;
                }
                return (
                  <tr>
                    {actor === Actors.seller && <td style={styles.cell}>{product.source_id}</td>}

                    <td style={styles.cell}>
                      <Image file={product.image} size={50} style={{verticalAlign: 'middle'}} />
                      {product.description}
                    </td>
                    <td style={styles.cell} align="center">
                      {product.quantity}
                    </td>
                    <td style={styles.cell} align="right">
                      {textPrice(rowValueFn(product))}
                    </td>
                  </tr>
                );
              })}
              <tr>
                <td style={styles.cell} colSpan={colspan}>
                  <div style={styles.emphasis}>{t('home:subtotal')}</div>
                </td>
                <td style={styles.cell} align="right">
                  {textPrice(order.subtotal)}
                </td>
              </tr>
              {products.map(product => {
                if (product.type !== 'shipping') {
                  return null;
                }
                return (
                  <tr>
                    {actor === Actors.seller && <td style={styles.cell}>{product.source_id}</td>}
                    <td style={styles.cell} colSpan={colspan}>
                      <div style={styles.emphasis}>{product.description}</div>
                    </td>
                    <td style={styles.cell} align="right">
                      {textPrice(rowValueFn(product))}
                    </td>
                  </tr>
                );
              })}
              {actor === Actors.buyer && promoCode && (
                <tr>
                  <td style={styles.cell} colSpan={colspan}>
                    <div>
                      {t('common:promoCode')}:{' '}
                      <b style={styles.emphasis}>
                        {promoCode.special_item?.name || t('common:discount')}
                      </b>
                    </div>
                    <div style={styles.minimal}>{promoCode.description}</div>
                  </td>
                  <td style={styles.cell} align="right">
                    <span style={styles.emphasis}>{textPrice(promoCode.price)}</span>
                  </td>
                </tr>
              )}
              <tr>
                <td style={styles.cell} colSpan={colspan}>
                  <div style={styles.emphasis}>{t('home:salesTax')}</div>
                </td>
                <td style={styles.cell} align="right">
                  {textPrice(order.tax)}
                </td>
              </tr>
              <tr>
                <td style={styles.cell} colSpan={colspan}>
                  <div style={styles.emphasis}>{t('home:paymentMethod')}</div>
                </td>
                <td style={styles.cell} align="right">
                  {payment === null
                    ? t(`paymentMethods.${order.payment_description}`)
                    : t('common:paymentMethods.cardEnding', {
                        brand: payment[1],
                        number: payment[2],
                      })}
                </td>
              </tr>
              <tr>
                <td style={styles.cell} colSpan={colspan}>
                  <div style={styles.emphasis}>{t('home:total')}</div>
                </td>
                <td style={styles.cell} align="right">
                  {textPrice(order.total)}
                </td>
              </tr>
            </tbody>
          </table>

          <div style={{...styles.large, marginBottom: '8px', marginTop: '16px'}}>
            {t('home:shipsTo')}
          </div>

          <div style={styles.section}>
            <div>
              {order.firstName} {order.lastName}
            </div>
            <div>
              <Address {...(address || {})} />
            </div>
          </div>

          {tradeInOrder && (
            <EmailTradeIn tradeInOrder={tradeInOrder} tradeInProducts={tradeInProducts} />
          )}
          {actor !== 'seller' && (
            <script
              type="application/json+trustpilot"
              dangerouslySetInnerHTML={{
                __html: `
                {
                  "recipientName": "${order.firstName} ${order.lastName}",
                  "recipientEmail": "${order.email}",
                  "referenceId": "${order.id}",
                }
            `,
              }}
            />
          )}
        </>
      </EmailLayout>
    </EmailContext.Provider>
  );
}

export function TextEmailOrder({
  order: {order, products, address, tradeInOrder, tradeInProducts, promoCode},
  t,
  config,
  actor = 'buyer',
}) {
  const rowValueFn = actor === 'buyer' ? getDisplayRowValue : getActualRowValue;
  const lines = [
    actor === 'seller' ? 'dashboard:email.newOrderSeller' : 'dashboard:email.orderReceived',

    '',
    actor === 'seller'
      ? `${t('dashboard:email.orderReceivedLongSeller1')} ${t(
          'dashboard:email.orderReceivedLongSeller1Link',
        )}`
      : t('dashboard:email.orderReceivedLong', {name: order.firstName}),
    '',
    `${t('home:orderNumberX', {number: order.id})} (${formatDate(order.createdAt)})`,
    '',
    `${t('quantity')}\t ${t('product')}\t ${t('price')}`,
    '',
  ];

  products.forEach(product => {
    if (product.type !== 'product') {
      return;
    }
    lines.push(`${product.quantity} \t${product.description} \t${textPrice(rowValueFn(product))}`);
    lines.push('');
  });

  lines.push(...[`${t('home:subtotal')}: ${textPrice(order.subtotal)}`, '']);

  products.forEach(product => {
    if (product.type !== 'shipping') {
      return;
    }

    lines.push(`${product.description}: ${textPrice(rowValueFn(product))}`);
    lines.push('');
  });

  if (promoCode) {
    lines.push(
      `${t('common:promoCode')}: ${
        promoCode.special_item?.name || t('common:discount')
      } ${textPrice(promoCode.price)} `,
      promoCode.description,
      '',
    );
  }

  lines.push(
    ...[
      `${t('home:salesTax')}: ${textPrice(order.tax)}`,
      '',
      `${t('home:paymentMethod')}: ${t(`paymentMethods.${order.payment_description}`)}`,
      '',
      `${t('home:total')}: ${textPrice(order.total)}`,
      '',
      `${t('home:shipsTo')}:`,
      '',
      `${order.firstName} ${order.lastName}`,
      formatAddress(address),
      '',
      '',
    ],
  );

  if (tradeInOrder) {
    lines.push('----------------------------');
    lines.push('');
    lines.push(...TextEmailTradeIn({tradeInOrder, tradeInProducts, t}));
  }

  lines.push(
    ...[
      'IncrediblePhones.com',
      'Call or text (888) 675-2905',
      'support@incrediblephones.com',
      '2820 Howard Cmns #309 Howard, WI 54313',
      '',
    ],
  );

  return lines.join('\n');
}

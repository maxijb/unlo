import React from 'react';
import EmailLayout from './email-layout';
import {EmailContext} from './email-context';
import {multiLineTrans} from '@Common/utils/generic-utils';
import {getPageLink, absoluteGetPageLink} from '@Common/utils/urls';
import {getDisplayRowValue, getActualRowValue} from '@Common/utils/order-utils';
import {Actors} from '@Common/constants/app';
import {formatDate} from '@Common/utils/date-utils';
import {textPrice, formatAddress} from '@Common/utils/format-utils';
import Address from '../cart/address';
import Image from '../images/image';
import EmailTradeIn, {TextEmailTradeIn} from './email-tradein';
import {cdnURL} from '@Common/utils/statif-assets-utils';
import {Trans} from 'react-i18next';

import {I18nextProvider} from 'react-i18next';
import initialI18nInstance from '../../i18n/i18n';

import styles from './email-styles.js';

export default function EmailAbandonedCart({order: {order, products}, t, config}) {
  return (
    <I18nextProvider i18n={initialI18nInstance}>
      <EmailContext.Provider
        value={{
          t,
          config,
        }}
      >
        <EmailLayout>
          <>
            <div style={{...styles.mainTitle, marginBottom: 0}}>
              {t('dashboard:email.returnLabelTitle')}
            </div>

            <p>{multiLineTrans(t('dashboard:email.returnLabelLong', {name: order.firstName}))}</p>

            <p>
              Erase the iPhone by going to Settings > General > Reset > Erase All Content and
              Settings (instructions{' '}
              <a
                href="https://support.apple.com/en-us/HT201351"
                target="_blank"
                rel="noopener noreferer"
              >
                here
              </a>
              ). Make sure you no longer see the device listed at{' '}
              <a href="https://icloud.com/#find" target="_blank" rel="noopener noreferer">
                https://icloud.com/#find.
              </a>
            </p>

            <p>{multiLineTrans(t('dashboard:email.returnLabelLong3'))}</p>
            <div
              style={{
                marginTop: 24,
                borderTop: '1px solid #ccc',
                marginBottom: 24,
              }}
            />
            <table
              width="100%"
              style={{
                borderCollapse: 'collapse',
              }}
            >
              <tbody>
                {products.map(product => {
                  if (product.type !== 'product') {
                    return null;
                  }
                  return (
                    <tr>
                      <td style={styles.cellNoBorder}>
                        <Image file={product.image} size={150} style={{verticalAlign: 'middle'}} />
                      </td>
                      <td style={styles.cellNoBorder}>
                        <div style={{marginBottom: 16, fontSize: 14}}>
                          {product.quantity > 1 && <b>{product.quantity}x </b>}{' '}
                          {product.description}
                        </div>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>

            <div style={{...styles.center, marginBottom: '32px', marginTop: '32px'}}>
              <a
                style={styles.button}
                href={products[0].remote_return_label}
                target="blank"
                rel="noopener"
              >
                {t('dashboard:email.returnLabelCTA')}
              </a>
            </div>

            <div style={styles.center}>
              <Trans i18nKey="dashboard:questionsSupportTeam">
                Text
                <a href="mailto:support@incrediblephones.com">link</a>
              </Trans>
            </div>
          </>
        </EmailLayout>
      </EmailContext.Provider>
    </I18nextProvider>
  );
}

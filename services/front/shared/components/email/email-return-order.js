import React from 'react';
import EmailLayout from './email-layout';
import {EmailContext} from './email-context';
import {multiLineTrans} from '@Common/utils/generic-utils';
import {formatDate} from '@Common/utils/date-utils';
import {textPrice, formatAddress} from '@Common/utils/format-utils';
import Address from '../cart/address';
import Image from '../images/image';
import EmailTradeIn, {TextEmailTradeIn} from './email-tradein';
import {getRowValue} from '@Common/utils/order-utils';
import {Actors} from '@Common/constants/app';
import styles from './email-styles.js';

function getContextType(returnType, status) {
  return status && [6, 7, 8].includes(status) ? `${returnType}-${status}` : returnType;
}

export default function EmailReturnOrder({
  order: {order, parent, products, returnType},
  t,
  config,
  actor = Actors.seller,
}) {
  const context = getContextType(returnType, products?.[0]?.status);
  return (
    <EmailContext.Provider
      value={{
        t,
        config,
      }}
    >
      <EmailLayout>
        <>
          <div style={styles.mainTitle}>{t('dashboard:email.returnReceived', {context})}</div>

          {multiLineTrans(
            t('dashboard:email.returnReceivedLong', {name: parent.firstName, context}),
          )}
          <p style={{...styles.large, ...styles.accent}}>
            <a href={`${config.protocol}://${config.domain}/user/orders`}>
              {t('home:orderNumberX', {number: parent.id})}
            </a>{' '}
            -- Order update started on {formatDate(order.createdAt)}
          </p>
          <table width="100%" style={{borderCollapse: 'collapse'}}>
            <tbody>
              <tr>
                {actor === Actors.seller && (
                  <td style={styles.cell}>
                    <b>{t('SKU')}</b>
                  </td>
                )}
                <td style={styles.cell}>
                  <b>{t('product')}</b>
                </td>
                <td style={styles.cell} align="center">
                  <b>{t('quantity')}</b>
                </td>
                <td style={styles.cell} align="center">
                  <b>{t('price')}</b>
                </td>
              </tr>
              {products.map(product => {
                if (product.type !== 'product') {
                  return null;
                }
                return (
                  <tr>
                    {actor === Actors.seller && <td style={styles.cell}>{product.source_id}</td>}
                    <td style={styles.cell}>
                      <Image file={product.image} size={50} style={{verticalAlign: 'middle'}} />
                      <span style={styles.marginRight}>{product.description}</span>
                      <span style={styles.colorTag}>
                        {t(`common:cartStatus.${product.status}`)}
                      </span>
                    </td>
                    <td style={styles.cell} align="center">
                      {product.quantity}
                    </td>
                    <td style={styles.cell} align="right">
                      {textPrice(getRowValue(product, actor))}
                    </td>
                  </tr>
                );
              })}

              <tr>
                <td style={styles.cell} colSpan={actor === Actors.seller ? 3 : 2}>
                  <div style={styles.emphasis}>{t('home:paymentMethod')}</div>
                </td>
                <td style={styles.cell} align="right">
                  {t(`paymentMethods.${parent.payment_type}`)}
                </td>
              </tr>
            </tbody>
          </table>
        </>
      </EmailLayout>
    </EmailContext.Provider>
  );
}

export function TextEmailReturnOrder({
  query: {order, products, parent, returnType},
  t,
  config,
  actor = Actors.seller,
}) {
  const context = getContextType(returnType, products?.[0]?.status);
  const lines = [
    t('dashboard:email.returnReceived', {context}),
    '',
    t('dashboard:email.returnReceivedLong', {name: parent.firstName, context}),
    '',
    `${t('home:orderNumberX', {number: parent.id})} (${formatDate(order.createdAt)})`,
    '',
    `${t('quantity')}\t ${t('product')}\t ${t('price')}`,
    '',
  ];

  products.forEach(product => {
    if (product.type !== 'product') {
      return;
    }
    lines.push(
      `${product.quantity} \t${product.description} \t${t(
        `common:cartStatus.${product.status}`,
      )} \t${textPrice(getRowValue(product, actor))}`,
    );
    lines.push('');
  });

  lines.push(...[`${t('home:paymentMethod')}: ${t(`paymentMethods.${parent.payment_type}`)}`, '']);

  lines.push(
    ...[
      'IncrediblePhones.com',
      'Call or text (888) 675-2905',
      'support@incrediblephones.com',
      '2820 Howard Cmns #309 Howard, WI 54313',
      '',
    ],
  );

  return lines.join('\n');
}

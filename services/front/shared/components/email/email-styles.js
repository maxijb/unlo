import {AppColors} from '@Common/constants/app';

const Styles = {
  mainTitle: {
    background: AppColors.dark,
    color: 'white',
    fontSize: '36px',
    textAlign: 'center',
    marginBottom: '16px',
  },
  accent: {
    color: AppColors.accent,
  },
  colorTag: {
    backgroundColor: AppColors.accent,
    color: 'white',
    padding: '2px 5px',
    borderRadius: '3px',
  },

  large: {
    fontSize: '24px',
    fontWeight: 600,
  },
  minimal: {
    fontSize: '11px',
    color: '#666',
  },
  legal: {
    fontSize: '10px',
    color: '#999',
  },
  emphasis: {
    fontWeight: 600,
    color: AppColors.subtext,
  },
  marginTop: {
    marginTop: '16px',
  },
  marginBottom: {
    marginBottom: '16px',
  },
  marginRight: {
    marginRight: '16px',
  },
  section: {
    background: '#ddd',
    padding: '16px',
    borderRadius: '8px',
  },
  cell: {
    border: '1px solid #ccc',
    padding: '10px',
  },
  cellNoBorder: {
    padding: '10px',
  },
  center: {
    textAlign: 'center',
  },
  button: {
    display: 'inline-block',
    fontSize: 20,
    background: AppColors.accent,
    color: 'white',
    margin: '0 auto',
    padding: '5px 20px',
    borderRadius: 3,
    cursor: 'pointer',
    fontWeight: 500,
    textDecoration: 'none',
  },
};

export default Styles;

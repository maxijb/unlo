import React from 'react';
import EmailLayout from './email-layout';
import {EmailContext} from './email-context';
import {multiLineTrans} from '@Common/utils/generic-utils';
import {TrackingLinks} from '@Common/constants/app';
import {formatDate} from '@Common/utils/date-utils';
import {textPrice, formatAddress} from '@Common/utils/format-utils';
import Address from '../cart/address';
import Image from '../images/image';
import EmailTradeIn, {TextEmailTradeIn} from './email-tradein';

import styles from './email-styles.js';

export default function EmailTracking({
  order: {order, products, address, tradeInOrder, tradeInProducts},
  t,
  config,
  actor = 'buyer',
}) {
  const {tracking_carrier, tracking} = products?.[0] || {};

  return (
    <EmailContext.Provider
      value={{
        t,
        config,
      }}
    >
      <EmailLayout>
        <>
          <div style={styles.mainTitle}>{t('dashboard:email.orderShipped')}</div>
          <p>{t('dashboard:email.HiName', {name: order.firstName})}</p>
          <p>{t('dashboard:email.GreatNewsDelivery')}</p>

          <div style={{marginBottom: 16}}>
            {t('dashboard:email.deliveredBy', {carrier: t(`common:${tracking_carrier}`)})}
          </div>
          <div style={styles.section}>
            <div>
              {order.firstName} {order.lastName}
            </div>
            <div>
              <Address {...(address || {})} />
            </div>
          </div>
          <p>
            {t('dashboard:email.trackUpdates')}
            <a href={TrackingLinks[tracking_carrier]?.replace('{{number}}', tracking)}>
              <b>this link</b>
            </a>
          </p>
          {multiLineTrans(t('dashboard:email.trackingThanks'))}
        </>
      </EmailLayout>
    </EmailContext.Provider>
  );
}

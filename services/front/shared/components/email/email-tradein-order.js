import React from 'react';
import EmailLayout from './email-layout';
import {EmailContext} from './email-context';
import {multiLineTrans} from '@Common/utils/generic-utils';
import {formatDate} from '@Common/utils/date-utils';
import {textPrice, formatAddress} from '@Common/utils/format-utils';
import Address from '../cart/address';
import Image from '../images/image';
import EmailTradeIn, {TextEmailTradeIn} from './email-tradein';

import styles from './email-styles.js';

export default function EmailTradeInOrder({tradeInOrder, tradeInProducts, t, config}) {
  return (
    <EmailContext.Provider
      value={{
        t,
        config,
      }}
    >
      <EmailLayout>
        <>
          <div style={styles.mainTitle}>{t('dashboard:email.tradeinReceived')}</div>

          {multiLineTrans(t('dashboard:email.tradeinReceivedLong', {name: tradeInOrder.firstName}))}
          <p style={{...styles.large, ...styles.accent}}>
            <a href={`${config.protocol}://${config.domain}/user/orders`}>
              {t('home:orderNumberX', {number: tradeInOrder.id})}
            </a>{' '}
            ({formatDate(tradeInOrder.createdAt)})
          </p>

          <EmailTradeIn tradeInOrder={tradeInOrder} tradeInProducts={tradeInProducts} />
        </>
      </EmailLayout>
    </EmailContext.Provider>
  );
}

export function TextEmailTradeInOrder({tradeInOrder, tradeInProducts, t, config}) {
  const lines = [
    t('dashboard:email.tradeinReceived'),
    '',
    t('dashboard:email.tradeinReceivedLong', {name: tradeInOrder.firstName}),
    '',
    `${t('home:orderNumberX', {number: tradeInOrder.id})} (${formatDate(tradeInOrder.createdAt)})`,
    '',
    ...TextEmailTradeIn({tradeInOrder, tradeInProducts, t}),
  ];

  lines.push(
    ...[
      'IncrediblePhones.com',
      'Call or text (888) 675-2905',
      'support@incrediblephones.com',
      '2820 Howard Cmns #309 Howard, WI 54313',
      '',
    ],
  );

  return lines.join('\n');
}

import React from 'react';
import {withEmailConsumer} from './email-context';
import {textPrice} from '@Common/utils/format-utils';
import {safeJsonParse} from '@Common/utils/json';

import styles from './email-styles';

function EmailTradeIn({context: {t}, tradeInProducts, tradeInOrder}) {
  return (
    <div style={{...styles.section, ...{marginTop: '24px'}}}>
      <div style={styles.large}>
        {t('dashboard:email.youCanEarn', {value: textPrice(tradeInOrder.total)})}
      </div>
      <ul style={styles.emphasis}>
        {(tradeInProducts || []).map(item => {
          const attrs = safeJsonParse(item.special_item);
          return Object.keys(attrs).map(attr => (
            <li>
              {t(`home:${attr}`)}: {attrs[attr]}
            </li>
          ));
        })}
      </ul>
      <div>
        <p>
          <b>{t('dashboard:email.instructions')}</b>
        </p>
        <p>{t('dashboard:email.instructionsLong')}</p>
      </div>
    </div>
  );
}

export default withEmailConsumer(EmailTradeIn);

export function TextEmailTradeIn({tradeInProducts, tradeInOrder, t}) {
  const lines = [];
  lines.push(t('dashboard:email.youCanEarn', {value: textPrice(tradeInOrder.total)}));

  const trade = (tradeInProducts || []).map(item => {
    const attrs = safeJsonParse(item.special_item);
    return Object.keys(attrs).map(attr => {
      return `${t(`home:${attr}`)}: ${attrs[attr]}`;
    });
  });

  lines.push(
    ...['', `${t('dashboard:email.instructions')}:`, t('dashboard:email.instructionsLong'), '', ''],
  );
  return lines;
}

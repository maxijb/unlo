import React from 'react';
import Link from 'next/link';
import Footer from './footer';
import {Grid} from 'semantic-ui-react';
import {getPageLink} from '@Common/utils/urls';
import classnames from 'classnames';
import {RightChevron} from '../../../widget/components/display/svg-icons';
import TagLink from '../display/tag-link';

import styles from '@Common/styles/styles.scss';
import css from './footer.scss';

function ExtendedFooter(props) {
  const {t, app, actions, isProduct, api, questionsOnTop = false, isMobile, isTablet} = props;

  const menuHalfItems = Math.floor(app.visibleLeaves.length / 2);
  return (
    <>
      <Grid columns={16} className={classnames(styles.MainGrid)}>
        <Grid.Row className={classnames({narrow: !isMobile && !isProduct})}>
          <Grid.Column width={16}>
            {questionsOnTop && <TagLink href="/contact" text={t('QuestionsContactusanytime')} />}
            <div
              className={classnames({
                [styles.sectionWithBackground]: isMobile,
                [styles.banner]: !isMobile,
                [styles.FlexSpaceBetween]: !isMobile,
              })}
            >
              <div className={styles.MarginLeft}>
                <div className={styles.sectionTitle}>{t('betterForThePlanet')}</div>
                <div className={styles.ButtonRegular}>{t('betterForThePlanetLong')}</div>
              </div>
              <div
                className={classnames(css.betterPlanetImg, {
                  [styles.MarginBottom]: isMobile,
                  [styles.MarginTop]: isMobile,
                  [styles.MarginLeft]: !isMobile,
                  [styles.FlexCenter]: !isMobile,
                })}
              >
                <img src="/static/icons/waste-blk-new.svg" alt="Fight e-waste" />

                <img src="/static/icons/factory-blk-new.svg" alt="Fewer new phones" />
              </div>
            </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Grid columns={16} className={classnames(styles.MainGridWithBackground)} id="start_footer">
        <Grid.Row>
          <Grid.Column width={16}>
            <div className={styles.section}>
              <div className={styles.sectionTitle}>{t('allIphoneModels')}</div>

              <div
                className={classnames(
                  styles.MarginTop,
                  styles.MarginBottom,
                  styles.FlexSpaceBetween,
                  styles.FlexTop,
                )}
              >
                <div className={css.staticMenuModels}>
                  {app.visibleLeaves.map(it => (
                    <MenuItem key={it.name} item={it} t={t} />
                  ))}
                </div>
              </div>
            </div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={16}>
            <div
              className={classnames(styles.MarginBottom, {
                [styles.TinyBold]: isMobile,
                [styles.Button]: !isMobile,
              })}
            >
              {t('Whatisarefurbishedphone')}
            </div>
            <div
              className={classnames(styles.MarginDoubleBottom, {
                [styles.Tiny]: isMobile,
              })}
            >
              {t('Whatisarefurbishedphone_long')}
            </div>
            <div
              className={classnames(styles.MarginBottom, {
                [styles.TinyBold]: isMobile,
                [styles.Button]: !isMobile,
              })}
            >
              {t('WhatisIncredible')}
            </div>
            <div
              className={classnames(styles.MarginDoubleBottom, {
                [styles.Tiny]: isMobile,
              })}
            >
              {t('WhatisIncredible_long')}
            </div>
            {!questionsOnTop && (
              <TagLink
                href="/contact"
                text={t('QuestionsContactusanytime')}
                className={css.questionsLink}
              />
            )}
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Footer {...props} />
    </>
  );
}

function MenuItem({item, t}) {
  return (
    <Link href={`/product?id=${item.id}`} as={getPageLink('product', {product: item, t})}>
      <a>
        {item.name} <RightChevron />
      </a>
    </Link>
  );
}

export default ExtendedFooter;

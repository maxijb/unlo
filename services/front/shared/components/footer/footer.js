import React from 'react';
import Link from 'next/link';
import {Trans} from 'react-i18next';
import {CreditCards, LogoInverted} from '../../../widget/components/display/svg-icons';
import {Grid} from 'semantic-ui-react';
import classnames from 'classnames';

import styles from '@Common/styles/styles.scss';
import css from './footer.scss';

export default function Footer(props) {
  const {t, api, actions, bottomSpace, isMobile} = props;

  const address = (
    <div>
      <div>2820 Howard Cmns #309</div>
      <div className={styles.MarginBottom}>Howard, WI 54313</div>
      <div>Call or text (888) 675-2905</div>
      <div className={styles.MarginBottom}>support@incrediblephones.com</div>
    </div>
  );

  return (
    <Grid columns={16} className={classnames(css.footer, styles.MainGrid)}>
      <Grid.Row>
        <Grid.Column width={16}>
          <div className={classnames(styles.MarginDoubleTop, styles.MarginDoubleBottom)}>
            <Link href="/">
              <LogoInverted />
            </Link>
          </div>
          <div
            className={classnames(
              styles.MarginDoubleTop,
              styles.MarginDoubleBottom,
              styles.FlexSpaceBetween,
            )}
          >
            <div className={css.staticMenu}>
              <Link href="/shipping">
                <a>{t('shipping')}</a>
              </Link>
              <Link href="/privacy">
                <a>{t('privacy')}</a>
              </Link>
              <Link href="/terms-of-use">
                <a>{t('termsOfUse')}</a>
              </Link>
              <Link href="/warranty">
                <a>{t('warranty')}</a>
              </Link>
            </div>
            <div className={css.staticMenu}>
              <Link href="/contact">
                <a>{t('contact')}</a>
              </Link>
              <Link href="/faq">
                <a>{t('faq')}</a>
              </Link>
              <Link href="/payment-and-returns">
                <a>{t('paymentAndReturns')}</a>
              </Link>
            </div>
            {!isMobile && (
              <div className={classnames(css.staticMenu, css.address)}>
                <div
                  className={classnames(
                    styles.Tiny,
                    styles.ColorWhite,
                    styles.MarginDoubleTop,
                    styles.MarginDoubleBottom,
                    styles.FlexSpaceBetween,
                    styles.FlexCenter,
                  )}
                >
                  <a
                    href="https://www.trustpilot.com/review/incrediblephones.com"
                    target="_blank"
                    rel="noopener noreferer"
                  >
                    <img
                      src="/static/incredible/trustpilot-inverted.png"
                      width="114"
                      alt="Trustpilot"
                    />
                  </a>
                </div>
                {address}
              </div>
            )}
          </div>
          {isMobile && (
            <div className={classnames(css.address, styles.MarginDoubleBottom)}>{address}</div>
          )}
          <div>
            <CreditCards />
            <div className={classnames(styles.Tiny, styles.ColorWhite, styles.MarginHalfTop)}>
              {t('Payments 100% secured by Stripe')}
            </div>
          </div>

          <div
            className={classnames(
              styles.Tiny,
              styles.ColorWhite,
              styles.MarginDoubleTop,
              styles.MarginDoubleBottom,
            )}
          >
            <Trans i18nKey="footerLongText">
              Text
              <a href="/terms-of-use">link</a>
              <a href="/privacy">link</a>
              <a href="/payment-and-returns">link</a>
            </Trans>
          </div>
          {isMobile && (
            <div
              className={classnames(
                styles.Tiny,
                styles.ColorWhite,
                styles.MarginDoubleTop,
                styles.MarginDoubleBottom,
                styles.FlexSpaceBetween,
                styles.FlexCenter,
              )}
            >
              <a
                href="https://www.trustpilot.com/review/incrediblephones.com"
                target="_blank"
                rel="noopener noreferer"
              >
                <img
                  src="/static/incredible/trustpilot-inverted.png"
                  width="114"
                  alt="Trustpilot"
                />
              </a>
            </div>
          )}
        </Grid.Column>
      </Grid.Row>

      {bottomSpace && <div className={css.bottomSpace} />}
    </Grid>
  );
}

import React from 'react';
import {Table, Grid, Button, Icon} from 'semantic-ui-react';
import isEmpty from 'is-empty';
import {get} from 'lodash';

import {ActionMacros} from '@Common/constants/macros';

import WrappedCheckbox from '@Components/inputs/wrapped-checkbox';
import WrappedInput from '@Components/inputs/wrapped-input';
import WrappedDropdown from '@Components/inputs/wrapped-dropdown';

import css from './bot-question-line.scss';

class BotQuestionLine extends React.Component {
  static defaultProps = {
    macro: {id: '', shortcut: ''}
  };

  shouldComponentUpdate(nextProps) {
    return nextProps.item !== this.props.item;
  }

  onQuestionChange = event => {
    const value = event.target ? event.target.value : event;
    const {index, onChange} = this.props;
    onChange(index, 'question', value);
  };

  onAnswerChange = event => {
    const value = event.target ? event.target.value : event;
    const {index, onChange} = this.props;
    onChange(index, 'answer', value);
  };

  onUIChange = (event, data) => {
    const value = event.target ? event.target.value : event;
    const {index, onChange} = this.props;
    onChange(index, 'ui', value);
  };

  onToggle = () => {
    const {index, onToggle} = this.props;
    onToggle(index);
  };

  onDelete = () => {
    const {index, onDelete} = this.props;
    onDelete(index);
  };

  getActionItems = () => {
    const {t} = this.props;

    const actions = ActionMacros.items().map(macro => {
      return {value: macro.ui, text: t(`macroDescription.${macro.id}`), key: macro.id};
    });

    return [
      {
        value: '',
        text: t('noAction'),
        key: 'noaction'
      }
    ].concat(actions);
  };

  render() {
    const {t, index, item, options} = this.props;

    return (
      <Table.Row key={index}>
        <Table.Cell>
          <WrappedInput value={item.question} onChange={this.onQuestionChange} fluid />
        </Table.Cell>
        <Table.Cell>
          <WrappedInput value={item.answer} onChange={this.onAnswerChange} fluid />
        </Table.Cell>
        <Table.Cell className={css.actionCell}>
          <WrappedDropdown
            placeholder={t('noAction')}
            options={this.getActionItems()}
            value={item.ui || ''}
            selection
            onChange={this.onUIChange}
          />
        </Table.Cell>
        <Table.Cell className={css.buttonCell}>
          <WrappedCheckbox checked={item.active !== false} toggle onClick={this.onToggle} />
        </Table.Cell>
        <Table.Cell className={css.buttonCell}>
          <a onClick={this.onDelete}>
            <Icon name="trash alternate" color="blue" size="large" />
          </a>
        </Table.Cell>
      </Table.Row>
    );
  }
}

export default BotQuestionLine;

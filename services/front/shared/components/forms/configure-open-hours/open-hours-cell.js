import React from 'react';
import {Table, Grid, Button} from 'semantic-ui-react';
import isEmpty from 'is-empty';
import {get} from 'lodash';

import WrappedDropdown from '@Components/inputs/wrapped-dropdown';
import WrappedCheckbox from '@Components/inputs/wrapped-checkbox';

import css from './open-hours-line.scss';

class OpenHoursCell extends React.Component {
  shouldComponentUpdate(nextProps) {
    return nextProps.value !== this.props.value;
  }

  onChangeTime = (...args) => {
    const {index, num} = this.props;
    this.props.onChangeTime(index, num, ...args);
  };

  render() {
    const {t, label, slots, value, num} = this.props;
    return (
      <Table.Cell key={num} className={css.hourCell}>
        <span className={css.cellLabel}>{t(label)}</span>
        <WrappedDropdown
          options={slots}
          value={value}
          onChange={this.onChangeTime}
          selection
          className={css.hourInput}
          search
        />
      </Table.Cell>
    );
  }
}

export default OpenHoursCell;

import React from 'react';
import {Table, Grid, Button, Icon} from 'semantic-ui-react';
import isEmpty from 'is-empty';
import {get} from 'lodash';

import WrappedCheckbox from '@Components/inputs/wrapped-checkbox';
import OpenHoursInput from './open-hours-cell';

import css from './open-hours-line.scss';

class OpenHoursLine extends React.Component {
  static defaultProps = {
    today: []
  };

  shouldComponentUpdate(nextProps) {
    return nextProps.today !== this.props.today;
  }

  onSplit = () => {
    const {index, onSplit} = this.props;
    onSplit(index);
  };

  onUnsplit = () => {
    const {index, onUnsplit} = this.props;
    onUnsplit(index);
  };

  onEnable = () => {
    const {index, onEnable} = this.props;
    onEnable(index);
  };

  render() {
    const {t, index, today, dayName, slots, onChangeTime, isWizard} = this.props;
    return (
      <Table.Row key={index} className={isWizard ? css.isWizard : ''}>
        <Table.Cell className={css.dayNameCell}>
          <span className={css.dayName}>{dayName}</span>
        </Table.Cell>
        <Table.Cell className={css.toggleCell}>
          <WrappedCheckbox toggle checked={!isEmpty(today)} onClick={this.onEnable} />
        </Table.Cell>
        <OpenHoursInput
          index={index}
          key={0}
          num={0}
          value={today[0]}
          t={t}
          label={'hoursSettings.from'}
          slots={slots}
          onChangeTime={onChangeTime}
        />
        <OpenHoursInput
          index={index}
          key={1}
          num={1}
          value={today[1]}
          t={t}
          label={'hoursSettings.to'}
          slots={slots}
          onChangeTime={onChangeTime}
        />
        {today.length > 2 ? (
          [
            <OpenHoursInput
              index={index}
              key={2}
              num={2}
              value={today[2]}
              t={t}
              label={'hoursSettings.and'}
              slots={slots}
              onChangeTime={onChangeTime}
            />,
            <OpenHoursInput
              index={index}
              key={3}
              num={3}
              value={today[3]}
              t={t}
              label={'hoursSettings.to'}
              slots={slots}
              onChangeTime={onChangeTime}
            />
          ]
        ) : (
          <Table.Cell colSpan="2" key="split">
            <Button basic onClick={this.onSplit}>
              {t('hoursSettings.splitService')}
            </Button>
          </Table.Cell>
        )}
        <Table.Cell>
          {today.length > 2 &&
            (isWizard ? (
              <Icon
                color="blue"
                onClick={this.onUnsplit}
                name="exchange"
                size="large"
                className={css.clickable}
              />
            ) : (
              <Button basic onClick={this.onUnsplit}>
                {t('hoursSettings.unsplitService')}
              </Button>
            ))}
        </Table.Cell>
      </Table.Row>
    );
  }
}

export default OpenHoursLine;

import React, {useState} from 'react';
import {Grid, Button} from 'semantic-ui-react';
import {translate} from 'react-i18next';
import {withAppConsumer} from '@Components/app-context';
import Input from '@Components/inputs/wrapped-input';
import Textarea from '@Components/inputs/wrapped-textarea';
import classnames from 'classnames';
import FormValidations from '@Common/constants/form-validations';
import {ValidateForm} from '@Common/utils/validations';
import isEmpty from 'is-empty';
import ErrorRenderer from '@Components/inputs/error-renderer';

import styles from '@Common/styles/styles.scss';
import css from './contact-form.scss';

function ContactForm({t, api, basic, nested = false, context: {isMobile}}) {
  const [errors, setErrors] = useState({});
  const [data, setData] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [isSent, setIsSent] = useState(false);
  const setInput = (value, name) => {
    setData({...data, [name]: value});
    setErrors({...errors, [name]: null});
    setIsSent(false);
  };
  const onSubmit = () => {
    const err = ValidateForm(data, FormValidations.contact);
    setErrors(err);

    if (isEmpty(err)) {
      setIsLoading(true);
      api.forms.contact(data).then(({resp}) => {
        const serverErrors = !resp.error ? {} : resp.data || {form: {...resp}};
        setIsLoading(false);
        if (!isEmpty(serverErrors)) {
          return setErrors(serverErrors);
        }
        setData({});
        setIsSent(true);
      });
    }
  };

  return (
    <Grid columns={16} className={classnames({[styles.MainGrid]: !nested && !isMobile})}>
      <>
        <Grid.Row className={css.container}>
          <Grid.Column width={16}>
            <div
              className={classnames(styles.Centered, {[css.form]: !basic, [styles.form]: !basic})}
            >
              {!basic && (
                <>
                  <div className={classnames(styles.Huge, styles.MarginBottom)}>
                    {t('getInTouch')}
                  </div>
                  <div className={classnames(styles.Medium, styles.MarginDoubleBottom)}>
                    {t('plaseFillTheFormAndWellGetBack')}
                  </div>
                </>
              )}

              <Grid columns={16}>
                <Grid.Row>
                  <Grid.Column
                    computer={8}
                    tablet={8}
                    mobile={16}
                    className={classnames({[styles.MarginBottom]: isMobile})}
                  >
                    <Input
                      placeholder={t('firstName')}
                      error={errors?.firstName}
                      name="firstName"
                      value={data.firstName || ''}
                      fluid
                      onChange={setInput}
                      className={classnames({[styles.MarginBottom]: !isMobile})}
                    />
                  </Grid.Column>
                  <Grid.Column computer={8} tablet={8} mobile={16}>
                    <Input
                      placeholder={t('lastName')}
                      error={errors?.lastName}
                      name="lastName"
                      value={data.lastName || ''}
                      fluid
                      onChange={setInput}
                      className={classnames({[styles.MarginBottom]: !isMobile})}
                    />
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column
                    computer={8}
                    tablet={8}
                    mobile={16}
                    className={classnames({[styles.MarginBottom]: isMobile})}
                  >
                    <Input
                      placeholder={t('email')}
                      error={errors?.email}
                      name="email"
                      value={data.email || ''}
                      fluid
                      onChange={setInput}
                      className={classnames({[styles.MarginBottom]: !isMobile})}
                    />
                  </Grid.Column>
                  <Grid.Column computer={8} tablet={8} mobile={16}>
                    <Input
                      placeholder={t('phone')}
                      error={errors?.phone}
                      name="phone"
                      value={data.phone || ''}
                      fluid
                      onChange={setInput}
                      className={classnames({[styles.MarginBottom]: !isMobile})}
                    />
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={16}>
                    <Textarea
                      placeholder={t('comments')}
                      error={errors?.notes}
                      name="notes"
                      value={data.notes || ''}
                      fluid
                      rows={7}
                      onChange={setInput}
                    />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
              <Grid.Row>
                <Grid.Column columns={16} centered className={styles.MarginDoubleTop}>
                  {basic ? (
                    <Button primary fluid onClick={onSubmit} loading={isLoading}>
                      {t('submit')}
                    </Button>
                  ) : (
                    <Button color="black" onClick={onSubmit} loading={isLoading}>
                      {t('submit')}
                    </Button>
                  )}
                  <ErrorRenderer error={errors?.form} />
                  {isSent && <ErrorRenderer error={t('successfulSubmission')} positive />}
                </Grid.Column>
              </Grid.Row>
            </div>
          </Grid.Column>
        </Grid.Row>
      </>
    </Grid>
  );
}

export default withAppConsumer(translate(['home'])(ContactForm));

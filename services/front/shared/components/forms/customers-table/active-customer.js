import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {translate} from 'react-i18next';
import classnames from 'classnames';
import {get} from 'lodash';
import isEmpty from 'is-empty';
import {Form, Table, Icon, Grid, Button, Popup, Modal} from 'semantic-ui-react';
import Gravatar from 'react-gravatar';
import {getPageLink} from '@Common/utils/urls';

import Router from 'next/router';
import {defaultPrintDate, defaultPrintTime} from '@Common/utils/date-utils';
import {GravatarFallback} from '@Common/constants/app';

import BookingStatus from './customer-bookings-status';
import Checkbox from '@Components/inputs/wrapped-checkbox';
import DateFrom from '@Components/inputs/date-from';
import BrowserDetails from '@Components/inputs/browser-details';
import ActionHeader from '@Components/inputs/action-header';
import Textarea from '@Components/inputs/wrapped-textarea';
import ConfirmationModal from '@Components/modals/confirmation-modal';
import SendEmailModal from '@Components/modals/send-email-modal';
import SegmentsEditor from './segments-editor';
import EmptyPlaceholder from '@Components/information/empty-placeholder';

import css from './active-customer.scss';

class ActiveCustomer extends PureComponent {
  static propTypes = {
    appId: PropTypes.number,
    customer: PropTypes.object,
    segments: PropTypes.object,
    removeSegment: PropTypes.func,
    addSegment: PropTypes.func,
    saveNotes: PropTypes.func,
    deleteIdentity: PropTypes.func
  };

  static defaultProps = {
    extended: true,
    allowDelete: true
  };

  constructor(props) {
    super(props);
    this.state = {
      editingNotes: false,
      deletingUser: false,
      sendingEmail: false
    };
  }

  onEditNotes = () => {
    const {editingNotes} = this.state;
    this.setState({editingNotes: !editingNotes});
    if (editingNotes) {
      const {saveNotes, customer, appId} = this.props;
      try {
        const value = this.refs.notes.refs.component.ref.value;
        saveNotes({identityId: customer.id, notes: value, appId});
      } catch (e) {
        // ignore if no ref is set
      }
    }
  };

  onAbortDeleteCustomer = () => {
    this.setState({deletingUser: false});
  };

  onDeleteCustomer = () => {
    this.setState({deletingUser: true});
  };

  doDeleteCustomer = () => {
    const {customer, deleteIdentity, appId} = this.props;
    this.onAbortDeleteCustomer();
    deleteIdentity({identityId: customer.id, appId});
  };

  onSendEmail = () => {
    this.setState({sendingEmail: true});
    this.props.onToggleEmailModal(true);
  };

  onAbortSendEmail = () => {
    this.setState({sendingEmail: false, batchEmails: false});
    this.props.onToggleEmailModal(false);
  };

  removeSegment = ({id, name}) => {
    const {customer, removeSegment, appId} = this.props;
    removeSegment({identityId: customer.id, publicId: customer.publicId, segmentId: id, appId});
  };

  addSegment = id => {
    const {customer, addSegment, appId} = this.props;
    addSegment({identityId: customer.id, publicId: customer.publicId, segmentId: id, appId});
  };

  openConversations = () => {
    const {
      customer: {publicId, name, email = [], phone}
    } = this.props;
    Router.push(
      getPageLink('chat', {
        publicId,
        search: name || email[0] || phone || ''
      })
    );
  };

  createSegment = name => {
    const {customer, addSegment, appId} = this.props;
    return addSegment({
      identityId: customer.id,
      publicId: customer.publicId,
      segmentName: name,
      appId
    });
  };

  sendBatchEmail = emails => {
    this.setState({sendingEmail: true, batchEmails: emails});
  };

  renderEmpty() {
    const {t} = this.props;
    return (
      <div className={classnames(css.emptyColumn)}>
        <EmptyPlaceholder title={null} subtitle={t('emptyUserPlaceholder')} />
      </div>
    );
  }

  renderLocation(location) {
    const {t} = this.props;

    const content = isEmpty(location)
      ? t('unknown')
      : `${location.city_name}, ${location.region_name}, ${location.country_name}`;

    return (
      <div className={css.fact}>
        <Icon name="map marker alternate" className={css.itemIcon} />
        {content}
      </div>
    );
  }

  renderActivityItem(title, value) {
    return (
      <div className={css.activityItem} key={title}>
        <div key="item" className={css.activityTitle}>
          {title}
        </div>
        <div key="value" className={css.activityValue}>
          {value}
        </div>
      </div>
    );
  }

  renderExtended() {
    const {customer, allowDelete, isChatPage, t} = this.props;
    const {editingNotes} = this.state;
    return (
      <React.Fragment>
        <ActionHeader
          title={t('notes')}
          icon={editingNotes ? 'check circle' : 'edit'}
          action={editingNotes ? t('save') : t('edit')}
          onClick={this.onEditNotes}
        />
        {editingNotes ? (
          <Textarea initialValue={customer.notes || ''} ref="notes" />
        ) : (
          <div className={classnames({[css.emptyNotes]: !customer.notes})}>
            {customer.notes || t('noNotesYet')}
          </div>
        )}
        <ActionHeader title={''} />
        <div className={css.buttonsHolder}>
          <Button primary fluid onClick={this.onSendEmail}>
            {t('sendEmail')}
          </Button>
          {!isChatPage && (
            <Button primary fluid onClick={this.openConversations}>
              {t('openConversations')}
            </Button>
          )}
        </div>
        <ActionHeader title={t('activity')} />
        {this.renderActivityItem(
          t('firstVisited'),
          <span>
            <DateFrom t={t} date={customer.firstVisit} /> - {defaultPrintDate(customer.firstVisit)}{' '}
            - {defaultPrintTime(customer.firstVisit)}
          </span>
        )}
        {this.renderActivityItem(
          t('lastVisited'),
          <span>
            <DateFrom t={t} date={customer.lastVisit} /> - {defaultPrintDate(customer.lastVisit)} -{' '}
            {defaultPrintTime(customer.lastVisit)}
          </span>
        )}
        {customer.lastSession && [
          this.renderActivityItem(t('lastInteractionURL'), customer.lastSession.url),
          this.renderActivityItem(
            t('webBrowserAndDevice'),
            <BrowserDetails session={customer.lastSession} />
          )
        ]}
        {allowDelete !== false && (
          <Form.Field>
            <Button basic fluid onClick={this.onDeleteCustomer}>
              {t('deleteContact')}
            </Button>
          </Form.Field>
        )}
      </React.Fragment>
    );
  }

  render() {
    const {
      extended,
      customer,
      segments,
      user,
      t,
      app,
      sendEmail,
      serverConfig,
      appConfig,
      isMobile
    } = this.props;
    const {deletingUser, sendingEmail, batchEmails} = this.state;

    if (!customer) {
      return this.renderEmpty();
    }

    return (
      <React.Fragment>
        {sendingEmail && (
          <SendEmailModal
            appConfig={appConfig}
            serverConfig={serverConfig}
            sendEmail={sendEmail}
            user={user}
            isMobile={isMobile}
            customer={customer}
            onClose={this.onAbortSendEmail}
            batchEmails={batchEmails}
            app={app}
          />
        )}
        {(!isMobile || !sendingEmail) && (
          <div className={css.activeCustomer}>
            <div className={css.nameTitle}>{customer.name}</div>
            <div className={css.activeCustomerHeader}>
              <Gravatar
                className={css.activeGravatar}
                email={customer.email[0]}
                default={GravatarFallback}
              />
              <div className={css.fact}>
                {customer.email.map(email => (
                  <div key={email}>
                    <Icon name="mail" className={css.itemIcon} />
                    {email}
                  </div>
                ))}
              </div>
              <div className={css.fact}>
                <Icon name="phone" className={css.itemIcon} />
                {customer.phone}
              </div>
              {this.renderLocation(customer.location)}
            </div>
            <div>
              <BookingStatus bookings={customer.bookingsStatus} />
            </div>
            <SegmentsEditor
              segments={segments}
              segmentIds={customer.segments}
              removeSegment={this.removeSegment}
              addSegment={this.addSegment}
              createSegment={this.createSegment}
            />
            {extended && this.renderExtended()}
          </div>
        )}
        {deletingUser && (
          <ConfirmationModal
            onConfirm={this.doDeleteCustomer}
            onClose={this.onAbortDeleteCustomer}
            message={t('sureDeleteCustomer')}
            title={t('deleteContact')}
            yes={t('yesDelete')}
            no={t('noKeep')}
          />
        )}
      </React.Fragment>
    );
  }
}

export default translate(['dashboard'], {withRef: true})(ActiveCustomer);

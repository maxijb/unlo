import React, {PureComponent} from 'react';
import {translate} from 'react-i18next';
import classnames from 'classnames';
import isEmpty from 'is-empty';

import css from './bookings-status.scss';

class BookingsStatus extends PureComponent {
  render() {
    const {bookings, t} = this.props;

    if (!bookings || !bookings.total) {
      return <div />;
    }

    return (
      <div className={css.bookingsStatus}>
        <div>
          <div className={classnames(css.capsule)}>{bookings.total}</div>
          <span>{t('reservations')}</span>
        </div>
        <div>
          <div className={classnames(css.capsule, css.capsuleCancelled)}>
            {bookings.CANCELLED || 0}
          </div>
          <span>{t('cancellations')}</span>
        </div>
        <div>
          <div className={classnames(css.capsule, css.capsuleNoShow)}>{bookings.NOSHOW || 0}</div>
          <span>{t('noShow')}</span>
        </div>
      </div>
    );
  }
}

export default translate(['dashboard'])(BookingsStatus);

import React, {PureComponent} from 'react';
import {translate} from 'react-i18next';
import classnames from 'classnames';
import {get} from 'lodash';
import isEmpty from 'is-empty';
import {Form, Table, Grid, Button, Popup, Modal} from 'semantic-ui-react';
import Gravatar from 'react-gravatar';

import {GravatarFallback} from '@Common/constants/app';
import {renderSegments} from '@Common/utils/segments-utils';
import Checkbox from '@Components/inputs/wrapped-checkbox';
import DateFrom from '@Components/inputs/date-from';

import css from './customers.scss';

class CustomerRow extends PureComponent {
  onClickCustomer = () => {
    this.props.onClickCustomer(this.props.customer);
  };

  toggleSelectCustomer = () => this.props.toggleSelectCustomer({customer: this.props.customer});

  renderEmail() {
    const {
      customer: {email}
    } = this.props;
    if (isEmpty(email)) {
      return '';
    }

    return (
      <React.Fragment>
        <span>{email[0]}</span>
        {email.length > 1 && <span className={css.plusEmailTrigger}>+{email.length - 1}</span>}
      </React.Fragment>
    );
  }

  render() {
    const {customer, segments, selected, t, isMobile, isTablet} = this.props;
    return (
      <Table.Row onClick={this.onClickCustomer} className={css.row}>
        {!isMobile && (
          <Table.Cell width={1} textAlign="center">
            <Checkbox
              onClick={this.toggleSelectCustomer}
              checked={selected}
              noMargin
              className={css.checkbox}
            />
          </Table.Cell>
        )}
        <Table.Cell className={css.nameCell}>
          {!isEmpty(customer.email) && (
            <Gravatar
              className={css.gravatar}
              email={customer.email[0]}
              default={GravatarFallback}
            />
          )}
          <div className={css.name}>{customer.name}</div>
          <div className={css.email}>{this.renderEmail()}</div>
        </Table.Cell>
        {!isMobile && !isTablet && (
          <Table.Cell className={css.phoneCell}>{customer.phone}</Table.Cell>
        )}
        <Table.Cell className={css.segmentsCell}>
          <span>{renderSegments({customer, segments, t})}</span>
        </Table.Cell>
        {!isMobile && !isTablet && (
          <React.Fragment>
            <Table.Cell className={css.phone}>
              <DateFrom date={customer.firstVisit} t={t} />
            </Table.Cell>
            <Table.Cell className={css.phone}>
              <DateFrom date={customer.lastVisit} t={t} />
            </Table.Cell>
          </React.Fragment>
        )}
      </Table.Row>
    );
  }
}

export default translate(['dashboard', 'common'])(CustomerRow);

import React, {PureComponent} from 'react';
import {translate} from 'react-i18next';
import classnames from 'classnames';
import {get} from 'lodash';
import isEmpty from 'is-empty';
import {Form, Table, Grid, Button, Popup, Modal} from 'semantic-ui-react';
import Checkbox from '@Components/inputs/wrapped-checkbox';
import CustomerRow from './customer-row';
import EmptyPlaceholder from '@Components/information/empty-placeholder';

import css from './customers.scss';

class CustomersTable extends PureComponent {
  render() {
    const {
      customers: {list, segments, selected},
      onClickCustomer,
      toggleSelectCustomer,
      page = 0,
      perPage,
      t,
      isMobile,
      isTablet
    } = this.props;

    return (
      <React.Fragment>
        <Table className={css.table}>
          <Table.Header>
            <Table.Row className={css.row}>
              {!isMobile && (
                <Table.HeaderCell width={1} textAlign="center">
                  <Checkbox className={css.checkbox} noMargin />
                </Table.HeaderCell>
              )}
              <Table.HeaderCell className={css.nameCell}>{t('common:name')}</Table.HeaderCell>
              {!isTablet && !isMobile && <Table.HeaderCell>{t('common:phone')}</Table.HeaderCell>}
              <Table.HeaderCell>{t('segments')}</Table.HeaderCell>
              {!isMobile && !isTablet && (
                <React.Fragment>
                  <Table.HeaderCell>{t('firstVisited')}</Table.HeaderCell>
                  <Table.HeaderCell>{t('lastVisited')}</Table.HeaderCell>
                </React.Fragment>
              )}
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {list
              .items()
              .slice(page * perPage, (page + 1) * perPage)
              .map(item => {
                return (
                  <CustomerRow
                    key={item.id}
                    customer={item}
                    selected={selected.hasOwnProperty(item.id)}
                    segments={segments}
                    onClickCustomer={onClickCustomer}
                    toggleSelectCustomer={toggleSelectCustomer}
                    isMobile={isMobile}
                    isTablet={isTablet}
                  />
                );
              })}
          </Table.Body>
        </Table>
        {list.isEmpty() && <EmptyPlaceholder className={css.emptyPlaceholder} />}
      </React.Fragment>
    );
  }
}

export default translate(['dashboard', 'common'])(CustomersTable);

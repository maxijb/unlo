import React, {PureComponent} from 'react';
import {translate} from 'react-i18next';
import classnames from 'classnames';
import isEmpty from 'is-empty';
import {Icon, Menu, Popup, Loader} from 'semantic-ui-react';

import {cdnURL} from '@Common/utils/statif-assets-utils';
import {noop} from '@Common/utils/generic-utils';
import ActionHeader from '@Components/inputs/action-header';
import CancellableCapsule from '@Components/inputs/cancellable-capsule';
import Input from '@Components/inputs/wrapped-input';

import acss from '@Components/inputs/action-header.scss';
import css from './segments-editor.scss';

class SegmentsEditor extends React.Component {
  static defaultProps = {
    addSegment: noop,
    removeSegment: noop,
    showHeader: true
  };

  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      newSegment: '',
      creating: false
    };
  }

  opOpen = () => {
    this.setState({isOpen: true});
  };

  onStartCreating = () => {
    this.setState({creating: true});
  };

  onClose = event => {
    event && event.stopPropagation();
    this.setState({isOpen: false, creating: false});
  };

  addSegment = (id, event) => {
    const {interactionId} = this.props;
    this.onClose(event);
    this.props.addSegment(id, interactionId);
  };

  removeSegment = segment => {
    const {interactionId} = this.props;
    this.props.removeSegment(segment, interactionId);
  };

  setNewSegment = name => {
    this.setState({newSegment: name});
  };

  createSegment = () => {
    const {interactionId} = this.props;
    this.setState({loadingCreate: true});
    this.props.createSegment(this.state.newSegment, interactionId).then(() => {
      this.setState({newSegment: '', loadingCreate: false});
      this.onClose();
    });
  };

  renderCreate() {
    const {t} = this.props;
    const {creating, loadingCreate} = this.state;

    return creating ? (
      <div className={css.createHolder}>
        <Input
          placeholder="New segment"
          disabled={loadingCreate}
          className={css.createInput}
          onChange={this.setNewSegment}
          submitOnEnter={true}
          onSubmit={this.createSegment}
        />
        <div>
          {loadingCreate ? (
            <Loader size="mini" active={true} inline className={css.createLoading} />
          ) : (
            <Icon name="circle check" className={css.createIcon} onClick={this.createSegment} />
          )}
        </div>
      </div>
    ) : (
      <div className={css.createHolder} onClick={this.onStartCreating}>
        <span className={css.createLegend}>{t('createNew')}</span>
        <Icon name="plus" color="blue" className={css.createIcon} />
      </div>
    );
  }

  renderPopup() {
    const {segments, t, addSegment, trigger} = this.props;
    const {isOpen, loadingCreate} = this.state;

    const openTrigger = trigger ? (
      <div onClick={this.opOpen}>{trigger}</div>
    ) : (
      <div className={acss.action} onClick={this.opOpen}>
        <div>{t('addToSegment')}</div>
        <Icon name="folder" className={acss.icon} color="blue" />
      </div>
    );
    return (
      <Popup
        trigger={openTrigger}
        position="bottom right"
        on="click"
        onClose={this.onClose}
        open={isOpen}
        style={{padding: 0}}
      >
        <Menu vertical borderless={true}>
          {segments.items().map(({id, name}) => {
            return (
              <Menu.Item
                key={id}
                onClick={this.addSegment.bind(null, id)}
                as="a"
                className={css.item}
              >
                {name}
              </Menu.Item>
            );
          })}
          <Menu.Item key="new" as="div" className={css.item}>
            {this.renderCreate()}
          </Menu.Item>
        </Menu>
      </Popup>
    );
  }

  render() {
    const {
      segmentIds,
      segments,
      t,
      addSegment,
      showHeader,
      trigger,
      footer,
      className,
      showBubble
    } = this.props;

    return (
      <div className={className}>
        {showHeader && (
          <ActionHeader title={t('segments')} action={this.renderPopup()} onClick={this.opOpen} />
        )}
        {trigger && this.renderPopup()}
        <div className={css.tagsHolder}>
          {(segmentIds || []).map(id => {
            const tag = segments.get(id);
            if (!id || isEmpty(tag)) {
              return null;
            }
            return (
              <CancellableCapsule
                id={id}
                name={tag.name}
                onClose={this.removeSegment}
                key={id}
                inline
                showBubble={showBubble}
              />
            );
          })}
        </div>
        {footer || null}
      </div>
    );
  }
}

export default translate(['dashboard'])(SegmentsEditor);

import React, {Component} from 'react';
import get from 'lodash.get';
import {translate} from 'react-i18next';
import isEmpty from 'is-empty';

import Defaults from '@Widget/config/defaults';
import {
  calculateTimeSlots,
  absDateAndTimeToEmbedded,
  utcToEmbeddedTZ,
  addMinutes,
  isDayClosed
} from '@Common/utils/date-utils';
import {getGuestsRange} from '@Common/utils/bookings-utils';
import {cdnURL} from '@Common/utils/statif-assets-utils';

import {ValidateForm} from '@Common/utils/validations';
import FormValidations from '@Common/constants/form-validations';

import Dropdown from '@Components/inputs/wrapped-dropdown';
import Input from '@Components/inputs/wrapped-input';
import Textarea from '@Components/inputs/wrapped-textarea';
import Calendar from '@Widget/components/modules/calendar/calendar';

import {Grid, Button, Modal} from 'semantic-ui-react';

import css from './create-booking.scss';
import styles from '@Common/styles/styles.scss';

class EditBooking extends Component {
  static defaultProps = {
    booking: {},
    options: {}
  };

  constructor(props) {
    super(props);
    const date = new Date(`${props.initialDate} UTC`).getTime();
    const slots = calculateTimeSlots(date, props.options);
    const labels = get(props.options, 'bookings.labels', []).map(label => label.display);
    this.state = {
      // booking
      startDate: date,
      endDate: date,
      email: '',
      name: '',
      notes: '',
      phone: '',
      label: labels[0] || '',
      time: slots[0],
      value: 2,
      // dropdown options
      valueRange: getGuestsRange(props.options),
      slots,
      labels,
      // status
      saving: false,
      errors: {}
    };
  }

  onSetField = (event, key) => {
    this.setState({[key]: event.target ? event.target.value : event});
  };

  setDate = (startDate, endDate) => {
    const {options} = this.props;
    const timeSlots = calculateTimeSlots(startDate, options);
    this.setState({
      startDate: startDate,
      endDate: endDate,
      timeSlots,
      time: timeSlots[0]
    });
  };

  onCreate = () => {
    const {options, onCreate} = this.props;
    const {
      name,
      email,
      startDate: sd,
      endDate: ed,
      value,
      label,
      time,
      phone,
      notes,
      errors
    } = this.state;

    const input = {email, name, phone};
    const foundErrors = ValidateForm(input, FormValidations.createBooking);
    this.setState({
      errors: foundErrors
    });

    if (!isEmpty(foundErrors)) {
      return;
    }

    const duration = get(options, 'bookings.duration', Defaults.defaultBookingDuration);
    const startDate = absDateAndTimeToEmbedded(new Date(sd), time, options.timezone);
    const endDate = utcToEmbeddedTZ(addMinutes(startDate, duration), options.timezone);

    const booking = {
      name,
      email,
      startDate,
      endDate,
      value,
      label,
      time,
      phone,
      notes
    };

    this.setState({saving: true});
    onCreate(booking).then(error => {
      this.setState({saving: false});
      if (!error) {
        this.props.onClose();
      } else {
        this.setState({errors: {form: error}});
      }
    });
  };

  disableDates = day => {
    const {
      options: {openHours},
      specialOpenHours
    } = this.props;
    return isDayClosed(day, {openHours, specialOpenHours});
  };

  render() {
    const {
      notes,
      name,
      email,
      startDate,
      endDate,
      value,
      label,
      time,
      phone,
      slots,
      valueRange,
      labels,
      saving,
      errors
    } = this.state;
    const {t, isMobile} = this.props;

    return (
      <Grid colums={16}>
        {!isMobile && (
          <Grid.Row>
            <Grid.Column width={16}>
              <div className={css.title}>{t('createReservation')}</div>
            </Grid.Column>
          </Grid.Row>
        )}
        <Grid.Row>
          <Grid.Column computer={8} tablet={8} mobile={16} className={css.calendarColumn}>
            <div className={css.label}>{t('selectDate')}</div>
            <Calendar
              className={css.calendar}
              startDate={startDate}
              endDate={endDate}
              onChange={this.setDate}
              disableDates={this.disableDates}
            />
          </Grid.Column>
          <Grid.Column computer={8} tablet={8} mobile={16}>
            <Grid colums={16}>
              <Grid.Row only="mobile" className={styles.MarginTop}>
                <Grid.Column width={16}>
                  <Dropdown
                    label={t('common:time')}
                    fluid
                    value={time}
                    name="time"
                    options={slots}
                    onChange={this.onSetField}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row only="mobile">
                <Grid.Column width={16}>
                  <Dropdown
                    fluid
                    label={t('common:guests')}
                    value={value}
                    name="value"
                    options={valueRange}
                    onChange={this.onSetField}
                  />
                </Grid.Column>
              </Grid.Row>

              <Grid.Row>
                <Grid.Column width={16}>
                  <Input
                    label={t('common:name')}
                    name="name"
                    onChange={this.onSetField}
                    value={name}
                    fluid
                    error={errors.name}
                  />
                </Grid.Column>
              </Grid.Row>

              <Grid.Row>
                <Grid.Column width={16}>
                  <Input
                    label={t('common:telephone')}
                    name="phone"
                    onChange={this.onSetField}
                    value={phone}
                    fluid
                    error={errors.phone}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column width={16}>
                  <Input
                    label={t('common:email')}
                    name="email"
                    onChange={this.onSetField}
                    value={email}
                    fluid
                    error={errors.email}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column width={16}>
                  <Dropdown
                    fluid
                    label={t('common:area')}
                    value={label}
                    name="label"
                    options={labels}
                    onChange={this.onSetField}
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Grid.Column>
        </Grid.Row>

        <Grid.Row only="tablet,computer">
          <Grid.Column computer={8} tablet={8} mobile={16}>
            <Dropdown
              label={t('common:time')}
              fluid
              value={time}
              name="time"
              options={slots}
              onChange={this.onSetField}
            />
          </Grid.Column>
          <Grid.Column computer={8} tablet={8} mobile={16}>
            <Dropdown
              fluid
              label={t('common:guests')}
              value={value}
              name="value"
              options={valueRange}
              onChange={this.onSetField}
            />
          </Grid.Column>
        </Grid.Row>

        <Grid.Row>
          <Grid.Column width={16}>
            <Textarea
              rows={3}
              label={t('notes')}
              value={notes}
              onChange={this.onSetField}
              name="notes"
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column computer={8} tablet={8} only="tablet,computer">
            <Button basic fluid onClick={this.props.onClose}>
              {t('cancel')}
            </Button>
          </Grid.Column>
          <Grid.Column computer={8} tablet={8} mobile={16}>
            <Button primary fluid loading={saving} onClick={this.onCreate}>
              {t('addReservation')}
            </Button>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

export default translate(['dashboard', 'common'])(EditBooking);

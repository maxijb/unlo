import React, {Component} from 'react';
import classnames from 'classnames';
import get from 'lodash.get';
import isEmpty from 'is-empty';
import {translate} from 'react-i18next';
import Gravatar from 'react-gravatar';
import {Grid, Button, Modal, Icon} from 'semantic-ui-react';

import {renderSegments} from '@Common/utils/segments-utils';
import {calculateTimeSlots} from '@Common/utils/date-utils';
import Dropdown from '@Components/inputs/wrapped-dropdown';
import Textarea from '@Components/inputs/wrapped-textarea';
import Checkbox from '@Components/inputs/wrapped-checkbox';
import {getGuestsRange} from '@Common/utils/bookings-utils';
import {cdnURL} from '@Common/utils/statif-assets-utils';
import BookingStatus from '@Components/forms/customers-table/customer-bookings-status';

import {GravatarFallback} from '@Common/constants/app';

import css from './edit-booking.scss';

class EditBooking extends Component {
  static defaultProps = {
    booking: {},
    options: {},
  };

  constructor(props) {
    super(props);
    this.state = {
      slots: calculateTimeSlots(props.booking.absStartDate, props.options),
      booking: {...props.booking},
      valueRange: getGuestsRange(props.options),
      labels: get(props.options, 'bookings.labels', []).map(label => label.display),
    };
  }

  onSetField = (value, key) => {
    const booking = {
      ...this.state.booking,
      [key]: value,
    };
    this.setState({booking});
  };

  onInitCancel = () => {
    this.setState({confirmCancellation: true});
  };

  onCloseCancel = () => {
    this.setState({confirmCancellation: false});
  };

  doCancelReservation = () => {
    this.setState({cancelling: true});
    this.props.onCancel(this.state.booking);
  };

  onUpdateReservation = () => {
    this.setState({updating: true});
    this.props.onUpdate(this.state.booking).then(() => {
      this.setState({updating: false});
    });
  };

  onSetNoShow = e => {
    const booking = {
      ...this.state.booking,
      status: e.target.checked ? 'NOSHOW' : 'PENDING',
    };
    this.setState({booking});
  };

  renderConfirmationModal() {
    const {t} = this.props;
    const {cancelling} = this.props;
    return (
      <Modal
        dimmer={'inverted'}
        size="tiny"
        onClose={this.onCloseCancel}
        open={true}
        closeIcon={<img src={cdnURL('/static/icons/dashboard/close.svg')} />}
      >
        <Modal.Header>{t('cancelReservation')}</Modal.Header>
        <Modal.Content>
          <div>{t('sureCancelReservation')}</div>
          <Grid>
            <Grid.Column width={8}>
              <Button fluid basic onClick={this.onCloseCancel}>
                {t('keepReservation')}
              </Button>
            </Grid.Column>
            <Grid.Column width={8}>
              <Button fluid negative onClick={this.doCancelReservation} loading={cancelling}>
                {t('doCancelReservation')}
              </Button>
            </Grid.Column>
          </Grid>
        </Modal.Content>
      </Modal>
    );
  }

  render() {
    const {
      booking: {identityId, absStartDate, value, label, time, notes, status},
      slots,
      valueRange,
      labels,
      confirmCancellation,
      updating,
    } = this.state;
    const {
      options,
      customers: {list, segments, loadingProfile},
      inline,
      t,
    } = this.props;
    const customer = list.get(identityId);

    return (
      <Grid
        colums={16}
        className={classnames(css.mainContainer, {
          [css.inline]: inline,
        })}
      >
        <Grid.Row className={css.headerRow}>
          {!customer && loadingProfile === identityId && <span>{t('loading')}</span>}
          {!customer && loadingProfile !== identityId && <span>{t('unavailable')}</span>}
          {customer && (
            <React.Fragment>
              <Grid.Column
                tablet={8}
                computer={8}
                mobile={16}
                className={css.columnName}
                textAlign={inline ? 'center' : 'left'}
              >
                <div className={css.nameHolder}>{customer.name}</div>
                <div>{renderSegments({customer, segments, t})}</div>
              </Grid.Column>
              <Grid.Column
                tablet={8}
                computer={8}
                mobile={16}
                className={css.columnContacts}
                textAlign={inline ? 'center' : 'left'}
              >
                <Gravatar
                  className={css.gravatar}
                  email={customer.email[0]}
                  default={GravatarFallback}
                />
                {!isEmpty(customer.email) && (
                  <div className={css.email}>
                    <Icon name="mail" className={css.itemIcon} />
                    {customer.email[0]}
                  </div>
                )}
                {!isEmpty(customer.phone) && (
                  <div className={css.phone}>
                    <Icon name="phone" className={css.itemIcon} />
                    {customer.phone}
                  </div>
                )}
              </Grid.Column>
              <Grid.Column width={16}>
                <BookingStatus bookings={customer.bookingsStatus} />
              </Grid.Column>
            </React.Fragment>
          )}
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={16}>
            <Dropdown
              fluid
              value={time}
              label={t('time')}
              name="time"
              options={slots}
              onChange={this.onSetField}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column mobile={16} computer={8} tablet={16}>
            <Dropdown
              fluid
              value={value}
              name="value"
              label={t('guests')}
              options={valueRange}
              onChange={this.onSetField}
            />
          </Grid.Column>
          <Grid.Column mobile={16} computer={8} tablet={16} className={css.field}>
            <Dropdown
              fluid
              value={label}
              name="label"
              label={t('area')}
              options={labels}
              onChange={this.onSetField}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={16}>
            <Textarea
              rows={3}
              label={t('notes')}
              value={notes}
              onChange={this.onSetField}
              name="notes"
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={16}>
            <Checkbox
              rows={3}
              label={t('markAsNoShow')}
              checked={status === 'NOSHOW'}
              onClick={this.onSetNoShow}
              id="noShow"
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column mobile={16} computer={8} tablet={16} className={css.field}>
            <Button primary fluid loading={updating} onClick={this.onUpdateReservation}>
              {t('updateReservation')}
            </Button>
          </Grid.Column>
          <Grid.Column mobile={16} computer={8} tablet={16} className={css.bottomField}>
            <Button negative fluid onClick={this.onInitCancel}>
              {t('cancelReservation')}
            </Button>
          </Grid.Column>
        </Grid.Row>
        {confirmCancellation && this.renderConfirmationModal()}
      </Grid>
    );
  }
}

export default translate(['dashboard'])(EditBooking);

import React from 'react';
import {Table, Grid, Button} from 'semantic-ui-react';
import isEmpty from 'is-empty';
import {get} from 'lodash';
import Router from 'next/router';

import {MacroInitCharacter} from '@Common/constants/macros';
import WrappedCheckbox from '@Components/inputs/wrapped-checkbox';

import css from './macros-config.scss';

const addon = <span className={css.slashAddon}>{MacroInitCharacter}</span>;

class MacroActionLine extends React.Component {
  static defaultProps = {
    macro: {id: '', shortcut: ''}
  };

  shouldComponentUpdate(nextProps) {
    return true;
    return (
      nextProps.macro !== this.props.macro ||
      nextProps.options.actionMacros !== this.props.options.actionMacros
    );
  }

  onToggle = () => {
    const {
      macro: {id},
      onToggle
    } = this.props;
    onToggle(id);
  };

  onSetup = () => {
    const {
      macro: {configurePage}
    } = this.props;
    if (configurePage) {
      Router.push(configurePage);
    }
  };

  render() {
    const {t, index, macro, description, options} = this.props;
    const needsSetup = typeof macro.condition === 'function' ? !macro.condition(options) : false;
    const active = needsSetup
      ? false
      : Boolean(get(options.actionMacros, [macro.id, 'active'], true));
    return (
      <Table.Row key={index} className={css.actionRow}>
        <Table.Cell className={css.actionName}>{description}</Table.Cell>
        <Table.Cell>/{macro.shortcut}</Table.Cell>
        <Table.Cell>
          {needsSetup && (
            <a onClick={this.onConfigure} onClick={this.onSetup}>
              {t('macroSettings.needsSetup')}
            </a>
          )}
          {!needsSetup && active && <span>{t('macroSettings.active')}</span>}
          {!needsSetup && !active && <span>{t('macroSettings.paused')}</span>}
        </Table.Cell>
        <Table.Cell>
          <WrappedCheckbox checked={active} disabled={needsSetup} toggle onClick={this.onToggle} />
        </Table.Cell>
      </Table.Row>
    );
  }
}

export default MacroActionLine;

import React from 'react';
import {Table, Grid, Button, Icon} from 'semantic-ui-react';
import isEmpty from 'is-empty';
import {get} from 'lodash';

import WrappedInput from '@Components/inputs/wrapped-input';
import {MacroInitCharacter} from '@Common/constants/macros';

import css from './macros-config.scss';

const addon = <span className={css.slashAddon}>{MacroInitCharacter}</span>;

class MacroStringLine extends React.Component {
  static defaultProps = {
    macro: {message: '', shortcut: ''}
  };

  shouldComponentUpdate(nextProps) {
    return nextProps.macro !== this.props.macro;
  }

  onShortcurt = event => {
    this.setLabelProp('shortcut', event);
  };

  onMessage = event => {
    this.setLabelProp('message', event);
  };

  onDelete = () => {
    const {index, onDelete} = this.props;
    onDelete(index);
  };

  setLabelProp = (key, event) => {
    const value = event.target ? event.target.value : event;
    const {index, setLabelField} = this.props;
    setLabelField(index, key, value);
  };

  render() {
    const {t, index, today, dayName, slots, onChangeTime, macro} = this.props;
    return (
      <Table.Row key={index}>
        <Table.Cell className={css.messageColumn}>
          <WrappedInput value={macro.message || ''} onChange={this.onMessage} fluid />
        </Table.Cell>
        <Table.Cell className={css.shortcutColumn}>
          <WrappedInput
            value={macro.shortcut || ''}
            onChange={this.onShortcurt}
            leftAddon={addon}
            className={css.shortcutInput}
            fluid
          />
        </Table.Cell>
        <Table.Cell className={css.deleteColumn}>
          <a onClick={this.onDelete}>
            <Icon name="trash alternate" color="blue" size="large" />
          </a>
        </Table.Cell>
      </Table.Row>
    );
  }
}

export default MacroStringLine;

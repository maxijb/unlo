import React from 'react';
import {generateGeolocationAddress} from '@Common/utils/map-utils';
import get from 'lodash.get';

import css from './map-form.scss';

export default class MapForm extends React.Component {
  componentDidMount() {
    const {t, options} = this.props;

    const coors = {
      lat: get(options, 'business.latitude') || -34,
      lng: get(options, 'business.longitude') || 150,
    };

    window.initMapCallback = () => {
      this.map = new google.maps.Map(document.getElementById('map'), {
        center: coors,
        zoom: 8,
        zoomControl: true,
        fullscreenControl: false,
      });

      this.marker = new google.maps.Marker({
        position: coors,
        map: this.map,
        draggable: true,
        animation: google.maps.Animation.DROP,
        title: t('yourBussines'),
      });

      this.marker.addListener('dragend', this.onMarkerMove);
    };

    if (!window.google) {
      var script = document.createElement('script'); // create a script DOM node
      //script.src =
      //  'https://maps.googleapis.com/maps/api/js?key=AIzaSyDf9iKJehCNUn7Iczt3N7sFjoHJqmq7kXM&callback=initMapCallback'; // set its src to the provided URL
      //document.head.appendChild(script);
    } else {
      window.initMapCallback();
    }
  }

  onMarkerMove = (evt, isGeocode) => {
    const coors = {
      lat: evt.latLng.lat(),
      lng: evt.latLng.lng(),
    };
    this.props.onSetField(coors.lat, 'latitude');
    this.props.onSetField(coors.lng, 'longitude');

    if (isGeocode) {
      this.map.setCenter(coors);
      this.marker.setPosition(coors);
    }
  };

  setMapLocation = (field, value) => {
    const {
      business,
      business: {latitude, longitude},
    } = this.props.options;

    if (latitude || longitude) {
      return;
    }

    if (business.address && business.city && business.country && window.google) {
      const geoLocate = generateGeolocationAddress(business);
      const geocoder = new google.maps.Geocoder();
      geocoder.geocode({address: geoLocate}, res => {
        if (res.length) {
          this.onMarkerMove({latLng: res[0].geometry.location}, true);
        }
      });
    }
  };

  render() {
    return <div id="map" className={css.map} />;
  }
}

import React from 'react';
import classnames from 'classnames';
import {Table, Grid, Button, Icon} from 'semantic-ui-react';
import {Draggable} from 'react-beautiful-dnd';
import isEmpty from 'is-empty';
import {get} from 'lodash';
import {translate} from 'react-i18next';

import {cdnURL} from '@Common/utils/statif-assets-utils';
import WrappedCheckbox from '@Components/inputs/wrapped-checkbox';
import Input from '@Components/inputs/wrapped-input';
import FileOrLinkInput from '@Components/inputs/file-or-link-input';

import css from './menu-list.scss';

class MenuItem extends React.Component {
  onToggle = () => {
    const {onToggle, index} = this.props;
    onToggle(index);
  };

  onDelete = () => {
    const {onDelete, index} = this.props;
    onDelete(index);
  };

  onSetName = event => {
    const {index} = this.props;
    this.props.onChange(index, 'name', event);
  };

  onSetLink = event => {
    const {index} = this.props;
    this.props.onChange(index, 'link', event);
  };

  render() {
    const {menu, index, t} = this.props;
    const active = menu.isOn !== false;
    return (
      <Draggable draggableId={`menu${index}`} index={index}>
        {(provided, snapshot) => {
          return (
            <tr
              className={classnames(css.item, {
                [css.isDragging]: snapshot.isDragging
              })}
              ref={provided.innerRef}
              {...provided.draggableProps}
              {...provided.dragHandleProps}
            >
              <td className={css.itemInner} width={'40%'}>
                <Input value={menu.name} onChange={this.onSetName} fluid />
              </td>
              <td className={css.itemInner} width={'40%'}>
                <FileOrLinkInput
                  value={menu.link}
                  onChange={this.onSetLink}
                  accept={'application/pdf,image/*'}
                  type="publicUpload"
                />
              </td>
              <td className={classnames(css.itemInner, css.minCell)}>
                <WrappedCheckbox toggle checked={active} onClick={this.onToggle} />
              </td>
              <td className={classnames(css.itemInner, css.draggable, css.minCell)} width={'100px'}>
                <Icon color="blue" name="arrows alternate" size="large" />
              </td>
              <td className={classnames(css.itemInner, css.minCell)}>
                <a onClick={this.onDelete}>
                  <Icon color="blue" name="trash alternate" size="large" />
                </a>
              </td>
            </tr>
          );
        }}
      </Draggable>
    );
  }
}

export default translate(['dashboard'])(MenuItem);

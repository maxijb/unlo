import React from 'react';
import classnames from 'classnames';
import {Table, Grid, Button} from 'semantic-ui-react';
import {Droppable} from 'react-beautiful-dnd';
import isEmpty from 'is-empty';
import {get} from 'lodash';
import {translate} from 'react-i18next';

import MenuItem from './menu-item';

import css from './menu-list.scss';

class MenuList extends React.Component {
  render() {
    const {menus, onToggle, onChange, onDelete, t, isWizard} = this.props;
    return (
      <div>
        <Table className={classnames(css.spaceTop, {[css.wizard]: isWizard})}>
          <Table.Body>
            <Table.Row>
              <Table.Cell className={classnames(css.labelCell, css.bigCell)}>
                {t('menuSettings.menuName')}
              </Table.Cell>
              <Table.Cell className={classnames(css.labelCell, css.bigCell)}>
                {t('menuSettings.fileOrLink')}
              </Table.Cell>
              <Table.Cell className={classnames(css.labelCell, css.minCell)}>
                {t('onOff')}
              </Table.Cell>
              <Table.Cell className={classnames(css.labelCell, css.minCell)}>
                {t('order')}
              </Table.Cell>
              <Table.Cell className={classnames(css.labelCell, css.minCell)} />
            </Table.Row>
          </Table.Body>
        </Table>
        <Droppable droppableId="menu-dnd">
          {(provided, snapshot) => {
            return (
              <table
                className={classnames(css.list, {
                  [css.wizard]: isWizard
                })}
                ref={provided.innerRef}
                {...provided.droppableProps}
              >
                <tbody>
                  {menus.map((menu, i) => {
                    return (
                      <MenuItem
                        menu={menu}
                        key={i}
                        index={i}
                        onToggle={onToggle}
                        onChange={onChange}
                        onDelete={onDelete}
                      />
                    );
                  })}
                  {provided.placeholder}
                </tbody>
              </table>
            );
          }}
        </Droppable>
      </div>
    );
  }
}

export default translate(['dashboard'])(MenuList);

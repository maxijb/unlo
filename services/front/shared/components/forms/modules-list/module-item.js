import React from 'react';
import Link from 'next/link';
import classnames from 'classnames';
import {Draggable} from 'react-beautiful-dnd';
import {get} from 'lodash';
import {translate} from 'react-i18next';
import {Icon} from 'semantic-ui-react';

import {getUI, getModuleKey} from '@Widget/utils/screen-utils';
import {cdnURL} from '@Common/utils/statif-assets-utils';
import {ActionMacros} from '@Common/constants/macros';
import WrappedCheckbox from '@Components/inputs/wrapped-checkbox';

import css from './modules-list.scss';

class ModuleItem extends React.Component {
  static defaultProps = {
    isDraggable: true
  };

  toggleModule = () => {
    const {module, toggleModule} = this.props;
    toggleModule(module);
  };

  render() {
    const {module, index, options, moduleNames, isDraggable, t} = this.props;
    const ui = getUI(module);
    const condition = get(ActionMacros.get(ui), 'condition', null);
    const needsSetup = typeof condition === 'function' ? !condition(options) : false;
    const active = module.isOn !== false;

    const name =
      ui === 'promotion'
        ? `${ui}: ${module.inlineTitle || module.title}`
        : get(moduleNames, ui, ui);

    const content = (
      <React.Fragment>
        <td className={css.itemInner} width={'50%'}>
          <div className={css.moduleName}>{name}</div>
        </td>
        <td className={css.itemInner} width={'20%'}>
          {needsSetup && (
            <Link href={module.configurePage}>
              <a>{t('macroSettings.needsSetup')}</a>
            </Link>
          )}
          {!needsSetup && active && <span>{t('macroSettings.active')}</span>}
          {!needsSetup && !active && <span>{t('macroSettings.paused')}</span>}
        </td>
        <td className={css.itemInner} width={'20%'}>
          <WrappedCheckbox
            toggle
            checked={needsSetup ? false : active}
            disabled={needsSetup}
            onClick={this.toggleModule}
          />
        </td>
        <td className={classnames(css.itemInner, css.draggable)} width={'10%'}>
          {isDraggable && <Icon color="blue" name="arrows alternate" size="large" />}
        </td>
      </React.Fragment>
    );

    if (!isDraggable) {
      return <tr className={css.item}>{content}</tr>;
    }

    return (
      <Draggable
        draggableId={getModuleKey(module)}
        index={index}
        isDragDisabled={module.ui === 'welcome'}
      >
        {(provided, snapshot) => {
          return (
            <tr
              className={classnames(css.item, {
                [css.isDragging]: snapshot.isDragging
              })}
              ref={provided.innerRef}
              {...provided.draggableProps}
              {...provided.dragHandleProps}
            >
              {content}
            </tr>
          );
        }}
      </Draggable>
    );
  }
}

export default translate(['dashboard'])(ModuleItem);

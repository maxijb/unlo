import React from 'react';
import {Table, Grid, Button} from 'semantic-ui-react';
import {Droppable} from 'react-beautiful-dnd';
import isEmpty from 'is-empty';
import {get} from 'lodash';
import {translate} from 'react-i18next';

import {getModuleKey} from '@Widget/utils/screen-utils';

import ModuleItem from './module-item';

import css from './modules-list.scss';

class ModulesList extends React.Component {
  render() {
    const {modules: rawModules, toggleModule, options, moduleNames, t} = this.props;
    const [welcomeModule, ...modules] = rawModules;
    return (
      <div>
        <Grid columns={16}>
          <Grid.Row className={css.header}>
            <Grid.Column width={8}>
              <div className={css.columnTitle}>{t('modules')}</div>
            </Grid.Column>
            <Grid.Column width={3}>
              <div className={css.columnTitle}>{t('status')}</div>
            </Grid.Column>
            <Grid.Column width={3}>
              <div className={css.columnTitle}>{t('onOff')}</div>
            </Grid.Column>
            <Grid.Column width={1}>
              <div className={css.columnTitle}>{t('order')}</div>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <table className={css.list}>
              <tbody>
                <ModuleItem
                  isDraggable={false}
                  moduleNames={moduleNames}
                  options={options}
                  module={welcomeModule}
                  key={getModuleKey(welcomeModule)}
                  index={'welcome'}
                  toggleModule={toggleModule}
                />
              </tbody>
            </table>
            <Droppable droppableId="module-dnd">
              {(provided, snapshot) => {
                return (
                  <table className={css.list} ref={provided.innerRef} {...provided.droppableProps}>
                    <tbody>
                      {modules.map((module, i) => {
                        return (
                          <ModuleItem
                            moduleNames={moduleNames}
                            options={options}
                            module={module}
                            key={getModuleKey(module)}
                            index={i}
                            toggleModule={toggleModule}
                          />
                        );
                      })}
                      {provided.placeholder}
                    </tbody>
                  </table>
                );
              }}
            </Droppable>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

export default translate(['dashboard'])(ModulesList);

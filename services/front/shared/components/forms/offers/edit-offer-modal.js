import React from 'react';
import classnames from 'classnames';
import deepExtend from 'deep-extend';
import {Modal, Grid, Form, Button} from 'semantic-ui-react';
import isEmpty from 'is-empty';
import {get} from 'lodash';
import {translate} from 'react-i18next';

import {cdnURL, UserFilesCdnURL} from '@Common/utils/statif-assets-utils';
import WrappedCheckbox from '@Components/inputs/wrapped-checkbox';
import WrappedInput from '@Components/inputs/wrapped-input';
import AvatarInput from '@Components/inputs/avatar-input';
import WrappedTextarea from '@Components/inputs/wrapped-textarea';
import Input from '@Components/inputs/wrapped-input';

import FakeBody from '@Widget/components/body/fake-body';
import Promotion from '@Widget/components/modules/promotion/promotion';

import css from './edit-offer-modal.scss';

class EditOfferModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {offer: deepExtend(props.offer)};
    this.placeholders = {
      inlineTitle: props.t('editOfferPlaceholder.title'),
      text: props.t('editOfferPlaceholder.text'),
      rules: props.t('editOfferPlaceholder.rules'),
      button: props.t('editOfferPlaceholder.button'),
      image: cdnURL('/static/placeholder-block.png')
    };
  }

  onFileUploaded = (err, data) => {
    if (data && data.filename) {
      this.onSetField(data.filename, 'image');
    }
  };

  onSetField = (event, field) => {
    const offer = {
      ...this.state.offer,
      [field]: event.target ? event.target.value : event
    };
    this.setState({offer});
  };

  onToggleReservations = () => {
    const {
      offer: {enableReservations}
    } = this.state;
    this.onSetField(!enableReservations, 'enableReservations');
  };

  onSave = () => {
    const {index, onSaveOffer} = this.props;
    onSaveOffer(index, {...this.state.offer});
  };

  render() {
    const {index, t, onClose, options} = this.props;
    const {offer} = this.state;
    const placeholders = this.placeholders;
    const active = offer.isOn !== false;

    return (
      <Modal
        dimmer={'inverted'}
        size="large"
        onClose={onClose}
        open={true}
        closeIcon={
          <img src={cdnURL('/static/icons/dashboard/close.svg')} className={css.closeButton} />
        }
      >
        <Modal.Content>
          <div className={css.modalTitle}>{t('offersSettings.editOffer')}</div>
          <Grid>
            <Grid.Column width={9}>
              <Form>
                <Form.Field style={{marginTop: '10px'}}>
                  <WrappedInput
                    label={t('offersSettings.offerTitle')}
                    value={offer.inlineTitle}
                    name="inlineTitle"
                    onChange={this.onSetField}
                    placeholder={placeholders.inlineTitle}
                  />
                </Form.Field>
                <Form.Field>
                  <WrappedTextarea
                    rows={3}
                    label={t('offersSettings.offerDescription')}
                    value={offer.text}
                    name="text"
                    onChange={this.onSetField}
                    placeholder={placeholders.text}
                  />
                </Form.Field>
                <Form.Field>
                  <WrappedTextarea
                    rows={2}
                    label={t('offersSettings.offerRules')}
                    value={offer.rules}
                    name="rules"
                    onChange={this.onSetField}
                    placeholder={placeholders.rules}
                  />
                </Form.Field>
                <Form.Field>
                  <WrappedInput
                    label={t('offersSettings.offerAction')}
                    value={offer.button || ''}
                    name="button"
                    onChange={this.onSetField}
                    placeholder={placeholders.button}
                  />
                </Form.Field>
              </Form>
              <Form.Field>
                <AvatarInput
                  type="promotion"
                  image={offer.image}
                  onFileUploaded={this.onFileUploaded}
                  label={t('offersSettings.offerImage')}
                />
              </Form.Field>
              <Form.Field>
                <WrappedCheckbox
                  checked={offer.enableReservations}
                  label={t('offersSettings.enableOfferReservation')}
                  onClick={this.onToggleReservations}
                />
              </Form.Field>
              <Form.Field>
                <Button primary onClick={this.onSave}>
                  {t('widgetSettings.buttonPublish')}
                </Button>
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={7}>
              <FakeBody options={options}>
                <Promotion config={offer} isWithinChat={false} placeholders={this.placeholders} />
              </FakeBody>
            </Grid.Column>
          </Grid>
        </Modal.Content>
      </Modal>
    );
  }
}

export default translate(['dashboard'])(EditOfferModal);

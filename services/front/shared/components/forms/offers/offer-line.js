import React from 'react';
import classnames from 'classnames';
import {Table, Grid, Button, Icon} from 'semantic-ui-react';
import {Draggable} from 'react-beautiful-dnd';
import isEmpty from 'is-empty';
import {get} from 'lodash';
import {translate} from 'react-i18next';

import ConfirmationModal from '@Components/modals/confirmation-modal';
import {cdnURL} from '@Common/utils/statif-assets-utils';
import WrappedCheckbox from '@Components/inputs/wrapped-checkbox';
import Input from '@Components/inputs/wrapped-input';

import css from './offer-line.scss';

class OfferLine extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onToggle = () => {
    const {onToggle, index} = this.props;
    onToggle(index);
  };

  onAbortDelete = () => {
    this.setState({deleting: false});
  };

  onDelete = () => {
    this.setState({deleting: true});
  };

  onDoDelete = () => {
    const {onDelete, index} = this.props;
    onDelete(index);
    this.onAbortDelete();
  };

  onEdit = () => {
    const {onEdit, index} = this.props;
    onEdit(index);
  };

  render() {
    const {deleting} = this.state;
    const {offer, index, t} = this.props;
    const active = offer.isOn !== false;

    return (
      <React.Fragment>
        <tr>
          <td width={'25%'}>
            {offer.title || offer.inlineTitle ? (
              <div className={css.ellipsis}>{offer.inlineTitle || offer.title}</div>
            ) : (
              <a onClick={this.onEdit}>{t('offersSettings.needsSetup')}</a>
            )}
          </td>
          <td width={'50%'}>
            <div className={css.ellipsis}>{offer.text}</div>
          </td>
          <td width={'15%'}>
            <WrappedCheckbox toggle checked={active} onClick={this.onToggle} />
          </td>
          <td width={'10%'}>
            <a onClick={this.onEdit}>
              <Icon name="edit" color="grey" size="large" />
            </a>
          </td>
          <td width={'10%'}>
            <a onClick={this.onDelete}>
              <Icon name="trash alternate" color="blue" size="large" />
            </a>
          </td>
        </tr>
        {deleting && (
          <ConfirmationModal
            onConfirm={this.onDoDelete}
            onClose={this.onAbortDelete}
            message={t('sureDeleteOffer')}
            title={t('deleteOffer')}
            yes={t('yesDelete')}
            no={t('noKeep')}
          />
        )}
      </React.Fragment>
    );
  }
}

export default translate(['dashboard'])(OfferLine);

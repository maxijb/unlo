import React from 'react';
import {Table, Grid, Button, Icon} from 'semantic-ui-react';
import isEmpty from 'is-empty';
import {get} from 'lodash';

import WrappedInput from '@Components/inputs/wrapped-input';

class ReservationLabelLine extends React.Component {
  static defaultProps = {
    label: {display: ''}
  };

  shouldComponentUpdate(nextProps) {
    return nextProps.label !== this.props.label;
  }

  onNameChange = event => {
    this.setLabelProp('display', event);
  };
  onMaxPeople = event => {
    this.setLabelProp('maxConcurrentPeople', event);
  };
  onMaxBookings = event => {
    this.setLabelProp('maxConcurrentBookings', event);
  };

  onDelete = () => {
    const {index, onDelete} = this.props;
    onDelete(index);
  };

  setLabelProp = (key, event) => {
    const value = event.target ? event.target.value : event;
    const {index, setLabelField} = this.props;
    setLabelField(index, key, value);
  };

  render() {
    const {t, index, today, dayName, slots, onChangeTime, label} = this.props;
    return (
      <Table.Row key={index}>
        <Table.Cell>
          <WrappedInput fluid value={label.display || ''} onChange={this.onNameChange} />
        </Table.Cell>
        <Table.Cell>
          <WrappedInput fluid value={label.maxConcurrentPeople || ''} onChange={this.onMaxPeople} />
        </Table.Cell>
        <Table.Cell>
          <WrappedInput
            fluid
            value={label.maxConcurrentBookings || ''}
            onChange={this.onMaxBookings}
          />
        </Table.Cell>
        <Table.Cell>
          <Icon color="blue" name="trash alternate" size="large" onClick={this.onDelete} />
        </Table.Cell>
      </Table.Row>
    );
  }
}

export default ReservationLabelLine;

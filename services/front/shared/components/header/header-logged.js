import React from 'react';
import GA from 'react-ga';
import Link from 'next/link';
import {Button} from 'semantic-ui-react';

import Logger from '@Common/logger';
import {getPageLink} from '@Common/utils/urls';

import {translate} from 'react-i18next';
import Router from 'next/router';

import css from './header.scss';

export class HeaderLogged extends React.Component {
  render() {
    const {api, t} = this.props;

    return (
      <div className={css.header}>
        {t('header.logo')}
        <Button primary onClick={api.extended.fullLogout}>
          {t('header.logout')}
        </Button>
      </div>
    );
  }
}

export default translate(['common'])(HeaderLogged);

import {mount} from 'enzyme';
import {noop, identity} from 'lodash';
import React from 'react';

import {HeaderLogged} from './header-logged';

describe('Hello logic', () => {
  const props = {
    i18n: {
      options: {},
      getFixedT: noop
    },
    t: identity,
    api: {extended: {fullLogout: jest.fn()}}
  };

  const wrapper = mount(<HeaderLogged {...props} t={identity} />);

  it('HeaderLogged rendered', () => {
    expect(wrapper.find('div').length >= 1).toBeTruthy();
  });
});

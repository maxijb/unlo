import React from 'react';
import Link from 'next/link';
import {Button} from 'semantic-ui-react';
import {translate} from 'react-i18next';
import classnames from 'classnames';
import Logger from '@Common/logger';
import Price from '@Components/product/price';
import ProductDropdown from '@Components/product/product-dropdown';
import {Modals} from '@Common/constants/app';
import {getPageLink} from '@Common/utils/urls';
import {LogoInverted, Close, RightChevron} from '../../../widget/components/display/svg-icons';
import SignupDialog from '../login/signup-dialog';
import SigninDialog from '../login/signin-dialog';
import TradeInDialog from '@Components/product/tradein-dialog';
import FreeShippingModal from '@Components/modals/free-shipping-modal';
import SellerDescriptionModal from '@Components/modals/seller-description-modal';
import WarrantyModal from '@Components/modals/warranty-modal';
import TestedModal from '@Components/modals/tested-modal';
import LowPricesModal from '@Components/modals/low-prices-modal';
import BatteryModal from '@Components/modals/battery-modal';
import FreeReturnsModal from '@Components/modals/free-return-modal';
import Waterproofing from '@Components/modals/waterproofing-modal';
import UnbundlingModal from '@Components/modals/unbundling-modal';
import PaypalModal from '@Components/modals/paypal-modal';
import PromoCodeModal from '@Components/modals/promocode-modal';
import ContactModal from '@Components/modals/contact-modal';
import SearchBar from './search-bar';

import styles from '@Common/styles/styles.scss';
import css from './header.scss';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isMenuOpen: false,
      isSearchOpen: false,
    };
  }

  refresh() {
    window.location.reload();
  }

  renderMobileMenu() {
    const {
      app: {menuItems, visibleLeaves},
      user,
      isCart,
      t,
      actions: {openSigninModal},
    } = this.props;

    return (
      <>
        <div className={css.headerMenu}>
          <div className={css.close}>
            <Close />
          </div>
          <div className={classnames(css.mobileLogoHolder, {[css.cart]: isCart})}>
            <LogoInverted />
          </div>
        </div>
        <div className={css.mobileItems}>
          {user.id ? (
            <Link href="/user/home" as="/user">
              <a>
                {t('myAccount')} <RightChevron color="white" />
              </a>
            </Link>
          ) : (
            <a onClick={openSigninModal}>
              {t('common:login.signin')}
              <RightChevron color="white" />
            </a>
          )}

          <Link href="/contact">
            <a>
              Contact Us <RightChevron color="white" />
            </a>
          </Link>

          <div>All our phones</div>
          {visibleLeaves.map(item => (
            <Link
              key={item.id}
              prefetch
              as={getPageLink('product', {product: item, t})}
              href={`/product?id=${item.id}`}
            >
              <a className={css.product}>
                {item.name} <RightChevron color="white" />
              </a>
            </Link>
          ))}
        </div>
      </>
    );
  }

  renderCommonMenu() {
    const {
      app: {menuItems},
      t,
    } = this.props;

    return (
      <>
        {menuItems.map(item => (
          <Link
            key={item.id}
            prefetch
            as={getPageLink('product', {product: item, t})}
            href={`/product?id=${item.id}`}
          >
            <a>{item.name}</a>
          </Link>
        ))}
      </>
    );
  }

  render() {
    const {
      api,
      actions: {
        openSignupModal,
        closeSignupModal,
        openSigninModal,
        closeSigninModal,
        setIsOpenTradeIn,
        setCurrentModal,
      },
      app: {
        openedSignupModal,
        openedSigninModal,
        menuItems,
        currentModal,
        visibleLeaves,
        promoCode,
      },
      forms: {signup, signin},
      user,
      t,
      cart,
      isIndex,
      isCart,
      isTablet,
      isMobile,
      total,
      product,
      openCartModal,
      cart: {isOpenTradeIn},
      context: {GA},
    } = this.props;

    const {isMenuOpen, isSearchOpen} = this.state;
    const closeCurrentModal = () => setCurrentModal(null);
    const cartNumber = (cart?.items || []).reduce((tot, it) => {
      if (it.type === 'product') {
        tot += it.quantity || 1;
      }
      return tot;
    }, 0);

    // Prepare cart icon
    const cartIcon = (
      <div className={classnames({[css.cartDropdown]: isMobile && isCart})}>
        <div className={classnames(css.cartHolder)}>
          <img className={css.cart} src={'/static/incredible/cart.svg'} alt="Cart" />

          {Boolean(cart?.items?.length) && <div className={css.cartNumber}>{cartNumber}</div>}
        </div>
        {isMobile && isCart && (
          <div className={css.total}>
            <Price value={total} />
          </div>
        )}
      </div>
    );

    return (
      <div
        className={classnames(css.header, 'header', {
          [css.isCart]: isCart,
          [css.fixed]: isSearchOpen,
        })}
      >
        <div className={classnames(styles.row, css.headerContent)}>
          {(isMobile || isTablet) && !isCart && (
            <div className={css.logoHolder}>
              <img
                className={css.menuIcon}
                src={'/static/incredible/hamburger-menu.svg'}
                alt="Menu"
                onClick={() => {
                  this.setState({isMenuOpen: !isMenuOpen});
                  GA.event({
                    category: 'navigation',
                    action: 'open-mobile-menu',
                  });
                }}
              />
            </div>
          )}

          <div
            className={classnames({
              [css.cart]: isCart,
              [css.mobileLogoHolder]: isMobile || isTablet,
              [css.logoHolder]: !isMobile && !isTablet,
            })}
          >
            <Link as="/" href="/">
              <img
                className={css.logo}
                alt="Incredible Phones"
                src={'/static/incredible/logo.svg'}
                onClick={() => {
                  GA.event({
                    category: 'navigation',
                    action: 'logo-click',
                  });
                }}
              />
            </Link>
            {!isMobile && !isTablet && !isCart && !isIndex && (
              <ProductDropdown visibleLeaves={visibleLeaves} isIndex={false} />
            )}
          </div>

          {((!isMobile && !isTablet) || isMenuOpen) && !isCart && (
            <div
              className={classnames(css.menu, {
                [css.mobileMenu]: isMobile || isTablet,
              })}
            >
              {this.renderCommonMenu()}
            </div>
          )}
          {(isMobile || isTablet) && isMenuOpen && (
            <div
              className={classnames(css.menu, css.mobileMenu)}
              onClick={() => this.setState({isMenuOpen: false})}
            >
              {this.renderMobileMenu()}
            </div>
          )}

          {isCart && !isMobile && !isTablet && (
            <div className={styles.MediumBold}>{t('checkoutXItems', {number: cartNumber})}</div>
          )}
          {/* Push content to right on tablets */}
          {isCart && isTablet && <div> </div>}

          {!isMobile && (
            <div className={css.actionsContainer}>
              {!user.id ? (
                <div>
                  <span
                    className={css.signInButton}
                    onClick={() => {
                      GA.event({
                        category: 'navigation',
                        action: 'open-signin-header',
                      });

                      openSigninModal();
                    }}
                  >
                    {t('common:login.signin')}
                  </span>
                </div>
              ) : (
                <Link href="/user/home" as="/user" prefetch>
                  <a className={css.userName}>
                    <span>{user.displayName}</span>
                  </a>
                </Link>
              )}
            </div>
          )}

          {!isCart && (isMobile || isTablet) && (
            <div>
              <a
                className={css.searchIcon}
                onClick={e => {
                  e.stopPropagation();
                  this.setState({isSearchOpen: !this.state.isSearchOpen});
                  GA.event({
                    category: 'navigation',
                    action: 'open-search-header',
                  });
                }}
              >
                <img src="/static/incredible/search.png" className={css.searchIcon} alt="Search" />
              </a>
            </div>
          )}

          {!isCart && (
            <div>
              <Link prefetch href={getPageLink('cart')}>
                <div
                  onClick={() => {
                    GA.event({
                      category: 'navigation',
                      action: 'open-cart-header',
                    });
                  }}
                >
                  {cartIcon}
                </div>
              </Link>
            </div>
          )}

          {isCart && isMobile && (
            <>
              <div></div>
              <div
                onClick={() => {
                  GA.event({
                    category: 'navigation',
                    action: 'open-cart-modal',
                  });
                  openCartModal();
                }}
              >
                {cartIcon}
              </div>
            </>
          )}

          {isSearchOpen && (
            <SearchBar items={visibleLeaves} onClose={() => this.setState({isSearchOpen: false})} />
          )}

          <SigninDialog
            submitForm={api.user.login}
            closeModal={closeSigninModal}
            isOpen={openedSigninModal}
            facebookLogin={api.user.facebookLogin}
            googleLogin={api.user.googleLogin}
            openSignupModal={openSignupModal}
            sendForgotPassword={api.user.sendForgotPassword}
            onSuccessLogin={this.refresh}
          />
          <SignupDialog
            submitForm={api.user.signup}
            closeModal={closeSignupModal}
            isOpen={openedSignupModal}
            facebookLogin={api.user.facebookLogin}
            googleLogin={api.user.googleLogin}
            openSigninModal={openSigninModal}
            onSuccessLogin={this.refresh}
          />
          <TradeInDialog
            closeModal={() => setIsOpenTradeIn(false)}
            isOpen={isOpenTradeIn}
            api={api}
            cart={cart}
          />
          <FreeShippingModal
            isOpen={currentModal === Modals.freeShipping}
            onClose={closeCurrentModal}
          />
          <WarrantyModal isOpen={currentModal === Modals.warranty} onClose={closeCurrentModal} />
          <Waterproofing
            isOpen={currentModal === Modals.waterproofing}
            onClose={closeCurrentModal}
          />
          <UnbundlingModal
            isOpen={currentModal === Modals.unbundling}
            onClose={closeCurrentModal}
          />
          <PaypalModal isOpen={currentModal === Modals.paypal} onClose={closeCurrentModal} />
          <TestedModal isOpen={currentModal === Modals.tested} onClose={closeCurrentModal} />
          <LowPricesModal isOpen={currentModal === Modals.lowPrices} onClose={closeCurrentModal} />
          <BatteryModal isOpen={currentModal === Modals.battery} onClose={closeCurrentModal} />
          <ContactModal
            isOpen={currentModal === Modals.contact}
            onClose={closeCurrentModal}
            api={api}
          />
          <FreeReturnsModal
            isOpen={currentModal === Modals.freeReturns}
            onClose={closeCurrentModal}
          />
          <PromoCodeModal
            isOpen={currentModal === Modals.promoCode}
            onClose={closeCurrentModal}
            promoCode={promoCode}
          />
          <SellerDescriptionModal
            isOpen={currentModal === Modals.sellerDescription}
            onClose={closeCurrentModal}
            seller={product?.seller}
          />
        </div>
      </div>
    );
  }
}

export default translate(['home', 'common'])(Header);

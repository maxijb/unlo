import React, {useState, useMemo, useEffect, useCallback} from 'react';
import {translate} from 'react-i18next';
import Input from '@Components/inputs/wrapped-input';
import Link from 'next/link';
import classnames from 'classnames';
import {Close, RightChevron} from '../../../widget/components/display/svg-icons';
import {getPageLink} from '@Common/utils/urls';

import styles from '@Common/styles/styles.scss';
import headerCss from './header.scss';
import css from './search-bar.scss';

function SearchBar({items, onClose, t}) {
  useEffect(() => {
    window.addEventListener('click', onClose);
    return () => {
      window.removeEventListener('click', onClose);
    };
  }, []);

  const [value, setValue] = useState('');
  const results = useMemo(() => {
    return items
      .filter(it => it.name.toLowerCase().indexOf(value.toLowerCase()) !== -1)
      .map(it => (
        <Link
          key={it.id}
          prefetch
          as={getPageLink('product', {product: it, t})}
          href={`/product?id=${it.id}`}
        >
          <a>
            {it.name}
            <RightChevron />
          </a>
        </Link>
      ));
  }, [value, items]);

  return (
    <div className={css.box}>
      <div className={classnames(css.header)} onClick={e => e.stopPropagation()}>
        <div>
          <Input
            placeholder={t('search')}
            onChange={val => setValue(val)}
            fluid
            className={css.input}
          />
        </div>
        <div onClick={onClose}>
          <Close />
        </div>
      </div>
      <div className={headerCss.mobileItems}>{results}</div>
    </div>
  );
}

export default translate(['common'])(SearchBar);

import {get} from 'lodash';
import Api from '../../actions/api-actions';

export async function getLoginProps(ctx) {
  const response = await ctx.dispatch(Api.user.parseToken({token: ctx.query.inviteToken}, ctx.req));

  return {inviteDetails: response.resp};
}

import isEmpty from 'is-empty';
import {get} from 'lodash';
import {getPageLink} from '@Common/utils/urls';
import Logger from '@Common/logger';

import Api from '../../actions/api-actions';

const WithActiveConfig = (ComposedComponent, useExistingConfig) => {
  const getOriginalProps = ComposedComponent.getInitialProps;

  ComposedComponent.getInitialProps = async ctx => {
    //await dispatch(Api.attributes.loadAllAttributes({}, req));
    let componentProps = {};

    // and call his page's getOriginalProps
    if (typeof getOriginalProps === 'function') {
      const moreProps = await getOriginalProps(ctx);
      componentProps = {
        ...componentProps,
        ...moreProps,
        forbidden: ctx.query.forbidden,
      };
    }

    return componentProps;
  };

  return ComposedComponent;
};

export default WithActiveConfig;

import isEmpty from 'is-empty';

import {Entities} from '@Common/constants/assets';
import {appCookieName} from '@Common/constants/app';
import Api from '../../actions/api-actions';
import Actions from '../../actions/app-actions';

const WithDashboardContext = ComposedComponent => {
  const getOriginalProps = ComposedComponent.getInitialProps;

  ComposedComponent.getInitialProps = async ctx => {
    const {req, dispatch, reduxStore, query} = ctx;
    const promises = [];

    let state = reduxStore.getState();
    if (!state.user.email) {
      const id = req && req.auth ? req.auth.id : state.user.id;
      if (id) {
        promises.push(dispatch(Api.user.getUserDetails({id}, req)));
      }
    }
    await Promise.all(promises);

    // on server side validate default app form cookie
    //if (req) {
    //  const appCookie = req.cookies[appCookieName];
    //
    //}

    // get active app from query string
    const target = {
      appId: Number(query.app) || state.app.selectedApp,
      campaignId: Number(query.campaign) || state.app.selectedCampaign
    };

    // otherwise we look for the first user asset
    if (!target.appId) {
      const assets = reduxStore.getState().user.assets.items();
      for (let asset of assets) {
        if (asset.type === Entities.app) {
          dispatch(Actions.selectActiveApp({appId: asset.id}));
          target.appId = asset.id;
          break;
        }
      }
    } else {
      dispatch(Actions.selectActiveApp({appId: target.appId}));
    }

    if (
      !isEmpty(ComposedComponent.allowedRoles) &&
      ComposedComponent.allowedRoles.indexOf(reduxStore.getState().user.appRole) === -1
    ) {
      ctx.query.forbidden = true;
    }

    // enrich the getOriginalProps params with a target
    ctx.target = target;

    let componentProps = {};
    if (typeof getOriginalProps === 'function') {
      componentProps = await getOriginalProps(ctx);
    }

    return {...componentProps, forbidden: ctx.query.forbidden};
  };

  return ComposedComponent;
};

export default WithDashboardContext;

import isEmpty from 'is-empty';

import Api from '../../actions/api-actions';
import Actions from '../../actions/app-actions';
import {clientCookieName} from '@Common/constants/app';

const WithPublicContext = ComposedComponent => {
  const getOriginalProps = ComposedComponent.getInitialProps;

  ComposedComponent.getInitialProps = async ctx => {
    const {req, res, dispatch, reduxStore, query} = ctx;
    const promises = [];

    if (query.cid && res && !query.redirected) {
      res.cookie(clientCookieName, query.cid);
      return res.redirect(req.originalUrl + '&redirected=1');
    }

    let state = reduxStore.getState();

    if (isEmpty(state.app.publicKeys)) {
      promises.push(dispatch(Api.app.getPublicKeys({}, req)));
    }

    if (isEmpty(state.app.menuItems)) {
      promises.push(dispatch(Api.app.getMenuItems({}, req)));
      promises.push(dispatch(Api.app.findAllVisibleLeaves({}, req)));
    }

    if (!state.cart.loaded) {
      promises.push(dispatch(Api.cart.readActiveForUser({}, req)));
      promises.push(dispatch(Api.cart.getActiveTradeInForUser({}, req)));
    }

    if (query.promoCode) {
      const response = await dispatch(Api.cart.applyPromoCode({promoCode: query.promoCode}, req));
      if (response.resp.ok && response.resp.promoCode) {
        dispatch(Actions.setPromoCodeModal({promoCode: response.resp.promoCode}));
      }
    }

    await Promise.all(promises);

    let componentProps = {};
    if (typeof getOriginalProps === 'function') {
      componentProps = await getOriginalProps(ctx);
    }

    return {...componentProps};
  };

  return ComposedComponent;
};

export default WithPublicContext;

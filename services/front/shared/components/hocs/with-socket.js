import React from 'react';
import io from 'socket.io-client/dist/socket.io.slim';

let socket = null;

export default function WithSocket(ComposedComponent) {
  class SocketWrapper extends React.Component {
    constructor(props) {
      super(props);

      const {
        app: {
          config: {sockets = {}}
        },
        target
      } = props;

      if (process.browser && target && target.appId && !socket) {
        socket = io(`${sockets.appDomain}/operator`, {
          path: sockets.path,
          query: {appId: target.appId}
        });
      }
    }

    render() {
      return <ComposedComponent {...this.props} socket={socket} />;
    }
  }

  if (ComposedComponent.getInitialProps) {
    SocketWrapper.getInitialProps = ComposedComponent.getInitialProps;
  } else {
    SocketWrapper.getInitialProps = null;
  }

  return SocketWrapper;
}

import React, {useMemo} from 'react';
import {UserFilesCdnURL} from '@Common/utils/statif-assets-utils';

const noop = () => {};

const Generated = [100, 150, 400, 'full'];

function Image({
  file,
  type = 'bitmap',
  size = 400,
  width = null,
  height = null,
  onClick = noop,
  alt = '',
  className,
  withFrame,
  as,
  style: origStyle,
}) {
  if (!file) {
    return null;
  }

  const maxSize = width || height ? Math.max(width, height) : size;
  const fileSize = useMemo(() => Generated.find(it => it >= maxSize || it === 'full'), [maxSize]);
  const altFile = useMemo(() => {
    if (alt) {
      return alt;
    }
    if (!file) {
      return '';
    }
    const parts = file.split('.');
    parts.pop();
    return parts.join('.');
  }, [file, alt]);

  const parts = (file || '').split('.');
  if (parts[parts.length - 1] === 'pdf') {
    return (
      <div
        style={{
          display: 'inline-block',
          background: '#ddd',
          border: '1px solid #ccc',
          width: `${width || size}px`,
          height: `${height || size}px`,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          ...origStyle,
        }}
        className={className}
      >
        PDF
      </div>
    );
  }

  const url =
    type === 'svg' || fileSize === 'full'
      ? UserFilesCdnURL(file)
      : UserFilesCdnURL(`${fileSize}x${fileSize}/${file}`);

  const style = onClick === noop ? {...origStyle} : {...origStyle, cursor: 'pointer'};

  if (as === 'div') {
    if (Number.isFinite(size) || (Number.isFinite(width) && Number.isFinite(height))) {
      style.width = width || size;
      style.height = height || size;
    }
    return (
      <div
        style={{
          backgroundImage: `url(${url})`,
          backgroundPosition: 'center center',
          backgroundSize: 'cover',
          ...(style || {}),
        }}
        className={className}
        onClick={onClick}
      />
    );
  }
  if (Number.isFinite(size) || (Number.isFinite(width) && Number.isFinite(height))) {
    style.maxWidth = width || size;
    style.maxHeight = height || size;
  }
  const html = (
    <img
      src={url}
      alt={altFile}
      className={withFrame ? '' : className}
      onClick={onClick}
      style={style}
    />
  );

  if (!withFrame) {
    return html;
  } else {
    const frameStyle = {
      ...style,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      height: height || size,
      width: width || size,
    };

    return (
      <div style={frameStyle} className={className}>
        {html}
      </div>
    );
  }
}

export default Image;

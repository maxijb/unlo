import React, {PureComponent} from 'react';
import classnames from 'classnames';
import {GravatarFallback} from '@Common/constants/app';
import {UserFilesCdnURL} from '@Common/utils/statif-assets-utils';
import css from './avatar.scss';

class Avatar extends PureComponent {
  render() {
    const {src, className} = this.props;

    return (
      <div style={{backgroundImage: `url(${src})`}} className={classnames(css.avatar, className)} />
    );
  }
}

export default Avatar;

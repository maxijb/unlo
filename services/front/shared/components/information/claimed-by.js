import React, {PureComponent} from 'react';
import {translate} from 'react-i18next';

class ClaimedBy extends PureComponent {
  render() {
    const {claimedBy, user, teamMembers, className, showUnclaimed, t} = this.props;

    if (!claimedBy && !showUnclaimed) {
      return null;
    }

    let text = claimedBy ? t('claimedByMe') : t('claimedByNoOne');
    if (claimedBy && user.id !== claimedBy) {
      const member = teamMembers.get(claimedBy);
      if (!member) {
        text = t('claimedBySomeoneElse');
      } else {
        const person =
          member.firstName || member.lastName
            ? `${member.firstName} ${member.lastName}`
            : member.username || member.email;

        text = t('claimedByPerson', {person});
      }
    }

    return <span className={className}>{text}</span>;
  }
}

export default translate(['dashboard'])(ClaimedBy);

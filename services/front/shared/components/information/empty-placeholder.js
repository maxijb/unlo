import React from 'react';
import classnames from 'classnames';
import {translate} from 'react-i18next';
import {cdnURL} from '@Common/utils/statif-assets-utils';

import css from './empty-placeholder.scss';

function EmptyPlaceholder({title, subtitle, t, className}) {
  const titleRender = typeof title === 'undefined' ? t('emptyDefaultTitle') : title;

  const subtitleRender = typeof subtitle === 'undefined' ? t('emptyDefaultSubtitle') : subtitle;

  return (
    <div className={classnames(css.frame, className)}>
      <img src={cdnURL('/static/illustrations/cloud.svg')} className={css.emptyImage} />
      {titleRender && <div className={css.title}>{titleRender}</div>}
      {subtitleRender && <div className={css.subtitle}>{subtitleRender}</div>}
    </div>
  );
}

export default translate(['dashboard'])(EmptyPlaceholder);

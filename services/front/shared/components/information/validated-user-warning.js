import React, {useState, useCallback} from 'react';
import classnames from 'classnames';
import {translate, Trans} from 'react-i18next';
import {Message, Loader} from 'semantic-ui-react';

import styles from '@Common/styles/styles.scss';

function ValidateEmailWarning({user = {}, api, t}) {
  const [hidden, setHidden] = useState(false);
  const [resent, setResent] = useState(false);
  const [loading, setLoading] = useState(false);
  const onClickResend = useCallback(async () => {
    setLoading(true);
    await api.user.resendValidationToken();
    setResent(true);
    setLoading(false);
  });
  if (user.validated || hidden) {
    return null;
  }

  return (
    <Message
      warning
      icon="inbox"
      header={t('pleaseValidateEmail')}
      content={
        <div style={{color: 'black'}}>
          <div>{t('pleaseValidateEmailDescription')}</div>
          {!resent ? (
            <div>
              <Trans i18nKey="pleaseValidateEmailResend">
                Text <a onClick={onClickResend}>link</a>
              </Trans>
              <Loader size="mini" active={loading} inline style={{marginLeft: '10px'}} />
            </div>
          ) : (
            <div>{t('pleaseValidateEmailResent')}</div>
          )}
        </div>
      }
      onDismiss={() => setHidden(true)}
    />
  );
}

export default translate(['dashboard'])(ValidateEmailWarning);

import React from 'react';
import classnames from 'classnames';
import {Icon} from 'semantic-ui-react';

import {cdnURL} from '@Common/utils/statif-assets-utils';
import css from './action-header.scss';

export default class ActionHeader extends React.Component {
  static defaultProps = {
    onClick: () => {}
  };

  renderIcon() {
    const {icon} = this.props;
    if (!icon) {
      return null;
    } else if (icon[0] === '/') {
      return <img src={cdnURL(icon)} className={css.icon} />;
    }

    return <Icon name={icon} className={css.icon} color="blue" />;
  }

  render() {
    const {title, icon, action, onClick} = this.props;

    return (
      <div
        className={classnames(css.actionHeader, {
          [css.separator]: !title && !action
        })}
      >
        <span className={css.title}>{title}</span>
        {(action || icon) && (
          <div className={css.action} onClick={onClick}>
            <div>{action}</div>
            {this.renderIcon()}
          </div>
        )}
      </div>
    );
  }
}

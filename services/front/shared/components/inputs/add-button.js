import React from 'react';
import classnames from 'classnames';
import {Icon} from 'semantic-ui-react';
import css from './wrapped-input.scss';
export default function AddButton({className, text, color = 'blue', onClick}) {
  return (
    <div className={classnames(className, css.addButton)} onClick={onClick}>
      <Icon name="plus circle" color={color} />
      <span>{text}</span>
    </div>
  );
}

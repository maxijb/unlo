import React, {useState} from 'react';
import classnames from 'classnames';
import {Icon, Button} from 'semantic-ui-react';
import ImgSelectorModal from '../modals/img-selector-modal';

import css from './wrapped-input.scss';

export default function AddImageButton({
  className,
  text,
  color = 'blue',
  onClick,
  api,
  actions,
  images,
  imageMap,
  onSelect,
  imagesTotalCount,
}) {
  const [isSelecting, setIsSelecting] = useState(false);

  return (
    <>
      <Button primary onClick={() => setIsSelecting(true)}>
        Add Image
      </Button>
      {isSelecting && (
        <ImgSelectorModal
          onClose={() => setIsSelecting(false)}
          api={api}
          actions={actions}
          images={images}
          imageMap={imageMap}
          imagesTotalCount={imagesTotalCount}
          onSelect={onSelect}
        />
      )}
    </>
  );
}

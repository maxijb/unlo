import React from 'react';

import {noop} from '@Common/utils/generic-utils';
import GenericInput from './generic-input';
import {Input} from 'semantic-ui-react';
import css from './wrapped-input.scss';
import GoogleMapsLoader from './google-maps-loader';

const AddressFormat = {
  street_number: 'short_name',
  route: 'long_name',
  neighborhood: 'long_name',
  sublocality_level_1: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name',
};

export default class WrappedInput extends React.Component {
  static defaultProps = {
    onChange: noop,
    onSubmit: noop,
    submitOnEnter: false,
    index: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      address: {
        street_number: null,
        route: null,
        locality: null,
        administrative_area_level_1: null,
        country: null,
        postal_code: null,
      },
    };
  }

  onFocus(evt) {
    // should happen in mobile
    // evt.target.select();
  }

  /*
  geolocate() {
    if (navigator.geolocation && window.google) {
      navigator.geolocation.getCurrentPosition(position => {
        const geolocation = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        };
        const circle = new google.maps.Circle({
          center: geolocation,
          radius: position.coords.accuracy,
        });
        autocomplete.setBounds(circle.getBounds());
      });
    }
  }
  */

  initAutocomplete() {
    // enable this to improve the location search
    // this.geolocate();

    const {index, initialValue} = this.props;
    const input = document.getElementById(`address-input-${index}`);

    input.value = initialValue || '';

    // Create the autocomplete object, restricting the search predictions to
    // geographical location types.
    const autocomplete = new google.maps.places.Autocomplete(input, {types: ['geocode']});

    //setTimeout(() => {
    //  input.autocomplete = 'nope';
    //}, 100);

    const fillInAddress = () => {
      // Get the place details from the autocomplete object.
      const place = autocomplete.getPlace();
      const address = {};

      // Get each component of the address from the place details,
      // and then fill-in the corresponding field on the form.
      for (const component of place.address_components) {
        const addressType = component.types[0];

        if (AddressFormat.hasOwnProperty(addressType)) {
          address[addressType] = component[AddressFormat[addressType]];
        }
      }
      address.textName = input.value;
      address.showTextName = `${address.street_number} ${address.route}`;
      input.value = address.showTextName;
      address.locality = address.locality || address.sublocality_level_1 || address.neighborhood;

      this.props.onChange(address);
    };

    // Avoid paying for data that you don't need by restricting the set of
    // place fields that are returned to just the address components.
    autocomplete.setFields(['address_component']);
    // When the user selects an address from the drop-down, populate the
    // address fields in the form.
    autocomplete.addListener('place_changed', fillInAddress);
  }

  onChange(e, a) {
    if (this.props.onChangeText) {
      this.props.onChangeText(e.target.value);
    }
  }

  render() {
    const {
      error,
      t,
      tReady,
      label,
      leftAddon,
      rightAddon,
      initialValue,
      errorBottom,
      errorPositive,
      inputStyle,
      classNameInput,
      onChange,
      submitOnEnter,
      index,
      ...props
    } = this.props;

    return (
      <GenericInput {...this.props}>
        <GoogleMapsLoader callback={() => this.initAutocomplete()} />
        <Input
          id={`address-input-${index}`}
          error={Boolean(error)}
          {...props}
          className={classNameInput}
          style={inputStyle}
          onChange={this.onChange.bind(this)}
          onKeyDown={submitOnEnter ? this.onKeyDown : noop}
          onFocus={this.onFocus}
          autoComplete="address"
        />
      </GenericInput>
    );
  }
}

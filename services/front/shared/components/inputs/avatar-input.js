import React from 'react';
import {translate} from 'react-i18next';
import {Input, Icon} from 'semantic-ui-react';
import classnames from 'classnames';

import {UserFilesCdnURL, cdnURL} from '@Common/utils/statif-assets-utils';

import WrappedFileInput from './wrapped-file-input';
import ErrorRenderer from './error-renderer';

import css from './wrapped-input.scss';

class AvatarInput extends React.Component {
  static defaultProps = {
    rounded: true,
    type: 'avatar',
    recommendedHeight: 200,
    recommendedWidth: 200
  };

  constructor(props) {
    super(props);
    this.state = {error: null};
  }

  onOpen = () => {
    this.fileInput.onClick();
  };

  callback = (err, data) => {
    this.props.onFileUploaded(err, data);
    this.setState({error: err});
  };

  render() {
    const {
      error,
      rounded,
      label,
      type,
      t,
      image,
      recommendedHeight,
      recommendedWidth,
      accept
    } = this.props;

    const {error: err} = this.state;

    return (
      <div className={css.avatarInputContainer}>
        {label && <div className={css.label}>{label}</div>}
        <div>
          <div className={css.avatarHolder} onClick={this.onOpen}>
            {image ? (
              <div
                style={{backgroundImage: `url(${UserFilesCdnURL(image)})`}}
                className={classnames(css.avatar, {
                  [css.rounded]: rounded
                })}
              />
            ) : (
              <img
                src={cdnURL('/static/icons/dashboard/uploadbox.svg')}
                className={classnames(css.avatar)}
              />
            )}
          </div>
          <div className={css.avatarInputLegend}>
            <div onClick={this.onOpen} className={css.uploadAction}>
              <Icon name="plus circle" className={css.createIcon} color="blue" />
              {t('uploadNewImage')}
              <div className={css.recommendation}>
                {t('recommendedSize', {width: recommendedWidth, height: recommendedHeight})}
              </div>
              <ErrorRenderer error={error || err} classes={css.avatarError} />
            </div>
            <WrappedFileInput
              callback={this.callback}
              type={type}
              ref={input => (this.fileInput = input)}
              accept={accept}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default translate(['common'])(AvatarInput);

import React, {PureComponent} from 'react';
import {translate} from 'react-i18next';
import {timeFrom} from '@Common/utils/date-utils';

class BrowserDetails extends PureComponent {
  render() {
    const {session, className} = this.props;
    const {browserName, browserMajor, osName, deviceType} = session || {};

    if (!browserName && !deviceType && !osName) {
      return <span>{t('unknown')}</span>;
    }

    return (
      <span className={className}>
        {deviceType && <b>{deviceType} | </b>}
        {browserName} {browserMajor} | {osName}
      </span>
    );
  }
}

export default translate(['common'])(BrowserDetails);

import React, {PureComponent} from 'react';
import classnames from 'classnames';
import {Icon} from 'semantic-ui-react';

import {cdnURL} from '@Common/utils/statif-assets-utils';
import css from './cancellable-capsule.scss';

export default class CancellableCapsule extends PureComponent {
  static defaultProps = {
    onClose: () => {}
  };

  onClose = () => {
    const {name, id, onClose} = this.props;
    onClose({id, name});
  };

  render() {
    const {name, inline, id, showBubble} = this.props;

    return (
      <div
        key={id || name}
        className={classnames(css.capsule, {
          [css.inline]: Boolean(inline),
          [css.bubble]: Boolean(showBubble)
        })}
      >
        <span className={css.name}>{name}</span>
        <Icon name="times circle" onClick={this.onClose} className={css.closeButton} />
      </div>
    );
  }
}

import React from 'react';
import {translate} from 'react-i18next';
import {timeFrom} from '@Common/utils/date-utils';

export default class DateFrom extends React.Component {
  render() {
    const {date, dateTo, className, shortFormat, t, isWidget} = this.props;
    const {unit, amount, direction} = timeFrom(date, dateTo);

    const prefix = isWidget ? '!@' : 'common:';
    const key = shortFormat
      ? `${prefix}timeDeltaShort.${unit}`
      : `${prefix}timeDelta.${direction}-${unit}`;

    return <span className={className}>{t(key, {count: amount})}</span>;
  }
}

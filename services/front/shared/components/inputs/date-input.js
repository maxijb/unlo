import React from 'react';

import Calendar from '@Widget/components/modules/calendar/calendar';
import {noop} from '@Common/utils/generic-utils';
import {getAbsDateOnly} from '@Common/utils/date-utils';

import GenericInput from './generic-input';
import {Input, Popup} from 'semantic-ui-react';
import css from './wrapped-input.scss';

export default class DateInput extends React.Component {
  static defaultProps = {
    onChange: noop,
    disablePastDates: false
  };

  constructor(props) {
    super(props);
    const date = new Date(props.value || new Date());
    this.state = {
      open: false,
      date: date.getTime(),
      strDate: props.value ? getAbsDateOnly(date) : ''
    };
  }

  onOpen = e => {
    setTimeout(() => {
      this.setState({open: true});
    }, 200);
  };

  onClose = () => {
    this.setState({open: false});
  };

  onChange = event => {
    this.props.onChange(event.target.value, this.props.name, event);
  };

  setDate = date => {
    this.props.onChange(getAbsDateOnly(new Date(date)), this.props.name);
    this.setState({
      date,
      open: false
    });
  };

  renderPopup() {
    const {disablePastDates} = this.props;
    const {date} = this.state;

    return (
      <Popup context={this.refs.input.inputRef} open={this.state.open} onClose={this.onClose}>
        <Calendar
          startDate={date}
          endDate={date}
          disablePastDates={disablePastDates}
          onChange={this.setDate}
          disableDates={noop}
        />
      </Popup>
    );
  }

  render() {
    const {
      error,
      t,
      tReady,
      label,
      leftAddon,
      rightAddon,
      errorBottom,
      errorPositive,
      inputStyle,
      onChange,
      ...props
    } = this.props;

    const {strDate} = this.state;

    return (
      <GenericInput {...this.props}>
        <Input
          error={Boolean(error)}
          {...props}
          style={inputStyle}
          onChange={this.onChange}
          onFocus={this.onOpen}
          ref="input"
        />
        {this.state.open && this.renderPopup()}
      </GenericInput>
    );
  }
}

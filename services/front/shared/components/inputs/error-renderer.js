import React from 'react';
import isEmpty from 'is-empty';
import {translate} from 'react-i18next';
import classnames from 'classnames';
import get from 'lodash.get';

import css from './error-renderer.scss';

class ErrorRenderer extends React.Component {
  render() {
    const {error, t, classes = []} = this.props;
    if (!error || isEmpty(error) || error === 'false') {
      return <span />;
    }

    let visibleError = '';
    if (React.isValidElement(error)) {
      visibleError = error;
    } else if (typeof error === 'string') {
      visibleError = t(`errors.${error}`) === `errors.${error}` ? error : t(`errors.${error}`);
    } else if (error.message || error.name || error.error || error.data) {
      visibleError = t([
        `errors.${get(error.data, 'form')}`,
        `errors.${error.message}`,
        `errors.${error.name}`,
        `errors.${error.error}`,
        'errors.genericError',
      ]);
    } else {
      visibleError = `${t('errors.genericError')}: ${t(
        error.error ? error.error.toString() : error.toString(),
      )}`;
    }

    const classNames = Array.isArray(classes)
      ? classes.map(name => css[name] || name)
      : [css[classes] || classes];

    return (
      <div
        className={classnames(css.errorMessage, ...classNames, {
          [css.center]: Boolean(this.props.center),
          [css.positive]: Boolean(this.props.positive),
          [css.pushTop]: Boolean(this.props.pushTop),
          [css.pushBottom]: Boolean(this.props.pushBottom),
        })}
      >
        {visibleError}
      </div>
    );
  }
}

export default translate(['common'])(ErrorRenderer);

import React from 'react';
import {translate} from 'react-i18next';
import {Loader} from 'semantic-ui-react';
import classnames from 'classnames';

import {UserFilesCdnURL, cdnURL} from '@Common/utils/statif-assets-utils';

import WrappedFileInput from './wrapped-file-input';
import ErrorRenderer from './error-renderer';
import WrappedInput from './wrapped-input';

import css from './wrapped-input.scss';

class FileOrLinkInput extends React.Component {
  static defaultProps = {
    value: '',
    type: 'unknown',
  };

  constructor(props) {
    super(props);
    this.state = {loading: false, error: null};
  }

  onOpen = () => {
    this.fileInput.onClick();
  };

  onFileUploaded = (err, data) => {
    this.setState({loading: false});
    if (data) {
      this.props.onChange(UserFilesCdnURL(data.filename));
    } else {
      this.setState({error: err});
    }
  };

  onUploadStart = () => {
    this.setState({loading: true, error: null});
  };

  render() {
    const {
      error,
      value,
      type,
      t,
      image,
      onChange,
      accept,
      size,
      placeholder,
      action,
      fileInputClassname,
    } = this.props;
    const {loading, error: uploadError} = this.state;

    const addon = loading ? (
      <Loader size="mini" active={loading} inline className={css.uploadLoadingIcon} />
    ) : (
      <img
        className={classnames(css.uploadInputIcon, fileInputClassname)}
        src={cdnURL('/static/icons/dashboard/upload.svg')}
        onClick={this.onOpen}
      />
    );

    return (
      <div className={css.avatarInputContainer}>
        <div className={css.avatarHolderFull}>
          <WrappedInput
            value={value}
            onChange={this.props.onChange}
            rightAddon={addon}
            fluid
            size={size}
            placeholder={placeholder}
            action={action}
          />
          <ErrorRenderer error={error || uploadError} />
        </div>
        <WrappedFileInput
          callback={this.onFileUploaded}
          onUploadStart={this.onUploadStart}
          type={type}
          showLoader={false}
          accept={accept}
          fluid
          ref={input => (this.fileInput = input)}
        />
      </div>
    );
  }
}

export default translate(['common'])(FileOrLinkInput);

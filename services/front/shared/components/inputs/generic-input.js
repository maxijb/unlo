import React from 'react';
import classnames from 'classnames';
import ErrorRenderer from './error-renderer';

import {Input} from 'semantic-ui-react';
import css from './wrapped-input.scss';
import fonts from '@Common/styles/styles.scss';

export default class GenericInput extends React.Component {
  render() {
    const {
      error,
      t,
      tReady,
      label,
      leftAddon,
      rightAddon,
      errorBottom,
      classNames = [],
      inline,
      noMargin,
      className,
      errorPositive,
      children,
      errorClassName,
      style,
      ...props
    } = this.props;

    return (
      <div
        className={classnames(css.inputWrapper, className, ...classNames, {
          [css.inline]: inline,
          [css.noMargin]: noMargin,
        })}
        style={style}
      >
        {label && (
          <div className={css.topHolder}>
            <label className={css.label}>{label}</label>
            {errorBottom !== true && (
              <ErrorRenderer
                classes={['top', css.topError, errorClassName || fonts.Mini]}
                error={error}
              />
            )}
          </div>
        )}
        <div className={css.input}>
          {children}
          {leftAddon || null}
          {rightAddon || null}
          {(!label || errorBottom === true) && (
            <ErrorRenderer
              error={error}
              positive={errorPositive}
              classes={[errorClassName || fonts.Mini]}
            />
          )}
        </div>
      </div>
    );
  }
}

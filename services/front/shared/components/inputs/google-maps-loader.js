import React, {useEffect} from 'react';
import {GoogleMapsApiKey} from '@Common/constants/app';

function GoogleMapsLoader({callback}) {
  useEffect(() => {
    if (!window.google) {
      window.awaitingCallbacks = [...(window.awaitingCallbacks || []), callback];
      window.initMapCallback = () => {
        (window.awaitingCallbacks || []).forEach(cb => cb());
      };
      var script = document.createElement('script'); // create a script DOM node
      script.src = `https://maps.googleapis.com/maps/api/js?key=${GoogleMapsApiKey}&callback=initMapCallback&libraries=places`;
      document.head.appendChild(script);
    } else {
      callback();
    }
  }, []);
  return null;
}

export default GoogleMapsLoader;

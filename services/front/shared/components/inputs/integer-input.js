import React from 'react';

import {noop} from '@Common/utils/generic-utils';
import GenericInput from './generic-input';
import {Input} from 'semantic-ui-react';
import classnames from 'classnames';
import {Loader} from 'semantic-ui-react';

import css from './integer-input.scss';

export default class WrappedInput extends React.Component {
  static defaultProps = {
    onChange: noop,
    onSubmit: noop,
    submitOnEnter: false
  };

  modifyValue = delta => {
    const {value, onSubmit, isLoading} = this.props;

    if (!Number.isFinite(value) || isLoading) {
      return;
    }

    const res = Number(value) + delta;
    if (res <= 0) {
      return;
    }
    onSubmit(Number(value) + delta);
  };

  render() {
    const {
      error,
      t,
      tReady,
      label,
      leftAddon,
      rightAddon,
      errorBottom,
      errorPositive,
      inputStyle,
      classNameInput,
      onChange,
      submitOnEnter,
      value,
      isLoading = false,
      ...props
    } = this.props;

    return (
      <GenericInput {...this.props}>
        <div className={css.holder}>
          <div className={css.button} onClick={() => this.modifyValue(-1)}>
            -
          </div>
          <div className={css.value}>
            {isLoading ? <Loader size="mini" active={true} inline /> : value}
          </div>
          <div className={css.button} onClick={() => this.modifyValue(1)}>
            +
          </div>
        </div>
      </GenericInput>
    );
  }
}

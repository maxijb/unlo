import React, {PureComponent} from 'react';
import classnames from 'classnames';
import {translate} from 'react-i18next';

import css from './pagination.scss';

class Pagination extends PureComponent {
  onPrevPage = () => {
    const {page, moveToPage} = this.props;
    if (page > 0) {
      moveToPage(page - 1);
    }
  };

  onNextPage = () => {
    const {page, totalCount, perPage, moveToPage} = this.props;
    if (page < Math.ceil(totalCount / perPage) - 1) {
      moveToPage(page + 1);
    }
  };

  render() {
    const {page, totalCount, perPage, t} = this.props;
    if (!totalCount) {
      return null;
    }

    return (
      <div className={css.pagination}>
        <span
          className={classnames(css.navButton, {
            [css.disabled]: page <= 0
          })}
          onClick={this.onPrevPage}
        >
          {'<'}
        </span>

        <span>
          {t('paginationPageOf', {page: page + 1, pages: Math.ceil(totalCount / perPage)})}
        </span>

        <span
          className={classnames(css.navButton, {
            [css.disabled]: page >= Math.ceil(totalCount / perPage) - 1
          })}
          onClick={this.onNextPage}
        >
          {'>'}
        </span>
      </div>
    );
  }
}

export default translate(['common'])(Pagination);

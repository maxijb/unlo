import React, {Component} from 'react';
import {translate} from 'react-i18next';
import {Button, Icon} from 'semantic-ui-react';
import ErrorRenderer from './error-renderer';

import css from './save-button.scss';

class SaveButton extends Component {
  onClick = () => {
    this.props.onClick(this.props.name);
  };

  render() {
    const {saving, t, onClick, savingSuccessful, savingError, text, name} = this.props;
    return (
      <div className={css.holder}>
        <Button
          primary
          loading={saving && (!name || saving === name)}
          onClick={this.onClick}
          className={css.button}
        >
          {t(text || 'widgetSettings.buttonPublish')}
        </Button>
        <div className={css.legend}>
          {savingSuccessful && (!name || savingSuccessful === name) && (
            <span>
              <Icon name="check circle" color="grey" />
              {t('widgetSettings.optionsSaved')}
            </span>
          )}
          {savingError && (!name || savingError === name) && <ErrorRenderer error={savingError} />}
        </div>
      </div>
    );
  }
}

export default translate(['dashboard'])(SaveButton);

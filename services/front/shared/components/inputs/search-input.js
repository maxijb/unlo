import React from 'react';

import {noop} from '@Common/utils/generic-utils';
import GenericInput from './generic-input';
import {Input, Icon, Button} from 'semantic-ui-react';
import css from './wrapped-input.scss';

export default class WrappedInput extends React.Component {
  static defaultProps = {
    onChange: noop
  };

  constructor(props) {
    super(props);
    this.state = {
      value: props.value
    };
  }

  onChange = event => {
    this.setState({value: event.target.value});
  };

  onSearch = () => {
    this.props.onSearch(this.state.value || '');
  };

  onKeyPress = event => {
    if (event.keyCode === 13) {
      this.onSearch();
    }
  };

  renderSearchButton = () => {};

  renderSearchIcon = () => {
    return (
      <span className={css.searchAddon} onClick={this.onSearch}>
        <Icon name="search" />
      </span>
    );
  };

  setValue = value => {
    this.setState({value});
  };

  getValue = () => {
    return this.state.value;
  };

  render() {
    const {
      error,
      t,
      tReady,
      label,
      leftAddon,
      rightAddon,
      errorBottom,
      errorPositive,
      inputStyle,
      onChange,
      onSearch,
      inline,
      placeholder,
      nativeLabel,
      addon,
      ...props
    } = this.props;

    const {value} = this.state;

    // removing on blur event when adding button
    //onBlur={this.onSearch}
    return (
      <GenericInput
        {...this.props}
        rightAddon={addon !== 'button' ? this.renderSearchIcon() : null}
      >
        <Input
          error={Boolean(error)}
          {...props}
          value={value}
          style={inputStyle}
          onChange={this.onChange}
          onKeyDown={this.onKeyPress}
          placeholder={placeholder || (t && t('search')) || ''}
          label={
            addon !== 'button' ? null : (
              <Button size="mini" onClick={this.onSearch} className={css.searchAddonButton} primary>
                {t('search')}
              </Button>
            )
          }
          labelPosition={addon !== 'button' ? null : 'right'}
        />
      </GenericInput>
    );
  }
}

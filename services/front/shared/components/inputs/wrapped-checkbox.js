import React from 'react';
import classnames from 'classnames';
import ErrorRenderer from './error-renderer';

import {Checkbox} from 'semantic-ui-react';

import css from './wrapped-input.scss';
import fonts from '@Common/styles/styles.scss';

export default class WrappedCheckbox extends React.Component {
  render() {
    const {
      error,
      t,
      checked,
      label,
      leftAddon,
      rightAddon,
      errorBottom,
      items = [],
      value,
      name,
      toggle,
      inline,
      noMargin,
      className,
      errorPositive,
      errorClassName,
      ...props
    } = this.props;

    return (
      <div className={classnames(css.inputWrapper, className, {[css.inline]: inline})}>
        <div className={css.input}>
          <Checkbox
            {...props}
            toggle={toggle}
            label={label}
            checked={Boolean(checked)}
            className={classnames({
              [css.label]: toggle,
              [css.noMarginLabel]: noMargin
            })}
          />
          {leftAddon || null}
          {rightAddon || null}
          {(!label || errorBottom === true) && (
            <ErrorRenderer
              error={error}
              positive={errorPositive}
              classes={[css.checkboxError, errorClassName || fonts.Mini]}
            />
          )}
        </div>
      </div>
    );
  }
}

import React from 'react';
import ColorPicker from 'react-simple-colorpicker';
import {throttle} from 'lodash';

import {rgbaToHex, isColor} from '@Common/utils/color-utils';

import ErrorRenderer from './error-renderer';
import Input from './wrapped-input';
import css from './wrapped-input.scss';

export default class WrappedColorPicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {showColorPicker: false};
  }

  onFocus = (...args) => {
    this.setState({showColorPicker: true});
    setTimeout(() => {
      window.addEventListener('click', this.onClickOut);
    }, 1);
  };

  onClickOut = e => {
    if (!e.target.closest(`.${css.colorPickerInput}`)) {
      this.setState({showColorPicker: false});
      window.removeEventListener('click', this.onClickOut);
    }
  };

  onColorChange = throttle(color => {
    this.props.onChange(rgbaToHex(color));
  }, 250);

  render() {
    const {value, onChange} = this.props;
    const {showColorPicker} = this.state;

    const addon = <div className={css.colorSwatch} style={{background: value}} />;

    return (
      <div ref={e => (this.elem = e)} className={css.colorPickerInput}>
        <Input {...this.props} value={value} onFocus={this.onFocus} rightAddon={addon} />
        {showColorPicker && (
          <div className={css.colorPickerContainer}>
            <ColorPicker
              color={isColor(value) ? value : '#000000'}
              onChange={this.onColorChange}
              onClick={this.onFocus}
            />
          </div>
        )}
      </div>
    );
  }
}

import React from 'react';
import classnames from 'classnames';
import {noop} from '@Common/utils/generic-utils';
import GenericInput from './generic-input';
import {Dropdown} from 'semantic-ui-react';
import css from './wrapped-input.scss';
import {getOptionsSelector} from '../../selectors/dropdown-selector';

export default class WrappedDropdown extends React.Component {
  static defaultProps = {
    onChange: noop,
    size: 'small'
  };

  onChange = (event, data) => {
    this.props.onChange(data.value, this.props.name);
  };

  formatOptions = () => {
    const {options, formatFunction, emptyOption, t} = this.props;
    const first = options ? options[0] : '';

    if (formatFunction || typeof first === 'string' || typeof first === 'number') {
      return getOptionsSelector({items: options}, 'items', formatFunction, emptyOption, t);
    }
    return options || [];
  };

  render() {
    const {
      error,
      t,
      tReady,
      label,
      leftAddon,
      rightAddon,
      errorBottom,
      onChange,
      options,
      formatFunction,
      errorPositive,
      selection = true,
      scrolling = true,
      emptyOption,
      inputStyle,
      size,
      className,
      search,
      ...props
    } = this.props;

    const items = this.formatOptions();

    return (
      <GenericInput {...this.props}>
        <Dropdown
          search={search || false}
          error={Boolean(error)}
          {...props}
          className={classnames(className, size)}
          options={items}
          onChange={this.onChange}
          selection={selection}
          scrolling={scrolling}
          style={inputStyle}
        />
      </GenericInput>
    );
  }
}

import React from 'react';
import {translate} from 'react-i18next';
import {Button, Loader} from 'semantic-ui-react';
import {safeJsonParse} from '@Common/utils/json';

import {CsrfTokenHeader} from '@Common/constants/app';
import {noop} from '@Common/utils/generic-utils';

import ErrorRenderer from './error-renderer';
import css from './wrapped-input.scss';
import {getToken} from '../../utils/secured-fetch';

export default class WrappedFileInput extends React.Component {
  static defaultProps = {
    showLoader: true,
    callback: noop,
    onUploadStart: noop,
  };

  constructor(props) {
    super(props);
    this.state = {loading: false};
  }

  onClick = () => {
    this.fileInput.click();
  };

  uploadImage = async () => {
    const {num} = this.state;
    const {callback, onUploadStart, endpoint} = this.props;
    var formData = new FormData(this.form);
    onUploadStart();

    this.setState({loading: true});
    const csrf = await getToken();
    fetch(endpoint || '/api/upload-file', {
      method: 'POST',
      body: formData,
      headers: {
        [CsrfTokenHeader]: csrf,
      },
    })
      .then(async response => {
        this.fileInput.type = '';
        this.fileInput.type = 'file';
        this.setState({loading: false, num: 1});
        if (response.ok) {
          this.setState({error: null});
          return response.json();
        } else {
          const b = await response.blob();
          let t = await b.text();
          if (t.indexOf('<html') !== -1) {
            t = response.statusText;
          }
          let error = (t && t.substr(0, 50)) || response.statusText || 'uploadError';
          const json = safeJsonParse(t);
          if (json?.message) {
            error = json.message;
          }
          this.setState({error});
          return {error};
        }
      })
      .then(resp => {
        if (typeof callback === 'function') {
          const hasError = !resp || resp.error;
          callback(hasError ? resp.error || true : null, hasError ? null : resp);
        }
      });
  };

  render() {
    const {t, type, showLoader, accept} = this.props;
    const {loading} = this.state;

    return (
      <div>
        <form
          ref={form => (this.form = form)}
          action="javascript:void(0);"
          encType="multipart/form-data"
        >
          <input
            type="file"
            name="file"
            accept={accept || 'image/*'}
            ref={elem => (this.fileInput = elem)}
            className={css.hidden}
            onChange={this.uploadImage}
          />
          <input type="hidden" name="type" value={type} />
          {showLoader && (
            <Loader size="mini" active={loading} inline style={{marginLeft: '16px'}} />
          )}
        </form>
      </div>
    );
  }
}

//export default translate(['common'])(WrappedFileInput);

import React from 'react';

import {noop} from '@Common/utils/generic-utils';
import GenericInput from './generic-input';
import {Input} from 'semantic-ui-react';
import css from './wrapped-input.scss';

export default class WrappedInput extends React.Component {
  static defaultProps = {
    onBlur: noop,
    onChange: noop,
    onSubmit: noop,
    submitOnEnter: false,
  };

  onChange = event => {
    this.props.onChange(event.target.value, this.props.name, event);
  };

  onBlur = event => {
    this.props.onBlur(event.target.value, this.props.name, event);
  };

  onKeyDown = event => {
    if (event.keyCode === 13) {
      this.props.onSubmit(event.target.value, this.props.name, event);
    }
  };

  render() {
    const {
      error,
      t,
      tReady,
      label,
      leftAddon,
      rightAddon,
      errorBottom,
      errorPositive,
      inputStyle,
      classNameInput,
      onChange,
      submitOnEnter,
      errorClassName,
      ...props
    } = this.props;

    return (
      <GenericInput {...this.props}>
        <Input
          error={Boolean(error)}
          {...props}
          className={classNameInput}
          style={inputStyle}
          onChange={this.onChange}
          onBlur={this.onBlur}
          onKeyDown={submitOnEnter ? this.onKeyDown : noop}
        />
      </GenericInput>
    );
  }
}

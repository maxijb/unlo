import React from 'react';
import ErrorRenderer from './error-renderer';

import {Radio} from 'semantic-ui-react';
import classnames from 'classnames';

import styles from '@Common/styles/styles.scss';
import css from './wrapped-input.scss';

export default class WrappedTextarea extends React.Component {
  onClick = (event, data) => {
    const {name} = this.props;
    this.props.onClick(data.value, name);
  };

  render() {
    const {
      error,
      t,
      tReady,
      label,
      leftAddon,
      rightAddon,
      errorBottom,
      items = [],
      value,
      name,
      errorPositive,
      ...props
    } = this.props;

    return (
      <div className={css.inputWrapper}>
        {label && (
          <React.Fragment>
            <label className={css.label}>{label}</label>
            {errorBottom !== true && <ErrorRenderer classes={['top']} error={error} />}
          </React.Fragment>
        )}
        <div className={css.input}>
          {items.map((item, i) => {
            let display = item.display || item;
            const checked = value && (value === item.value || value === item);
            if (typeof display !== 'string') {
              display = (
                <div className={styles.FlexCenter}>
                  <div className={classnames(css.radioSelector, {[css.checked]: checked})} />
                  <div>{display}</div>
                </div>
              );
            }
            return (
              <div key={`${i}${item.value || item}`} className={css.radioItem}>
                <Radio
                  {...props}
                  label={display}
                  name={name}
                  value={item.value || item}
                  checked={checked}
                  onClick={this.onClick}
                />
              </div>
            );
          })}
          {leftAddon || null}
          {rightAddon || null}
          {(!label || errorBottom === true) && (
            <ErrorRenderer error={error} positive={errorPositive} />
          )}
        </div>
      </div>
    );
  }
}

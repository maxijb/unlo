import React from 'react';
import {TextArea} from 'semantic-ui-react';

import {noop} from '@Common/utils/generic-utils';

import GenericInput from './generic-input';

import css from './wrapped-input.scss';

export default class WrappedTextarea extends React.Component {
  static defaultProps = {
    onChange: noop
  };

  constructor(props) {
    super(props);
    this.state = {value: props.initialValue || ''};
  }

  onChange = event => {
    const {onChange, name, initialValue} = this.props;
    const value = event.target ? event.target.value : event;
    if (typeof initialValue !== 'undefined') {
      this.setState({value});
    } else {
      this.props.onChange(value, this.props.name);
    }
  };

  render() {
    const {
      error,
      t,
      tReady,
      label,
      leftAddon,
      rightAddon,
      onChange,
      errorBottom,
      errorPositive,
      initialValue,
      className,
      ...props
    } = this.props;

    return (
      <GenericInput {...this.props}>
        <div className="ui input fluid">
          <TextArea
            {...props}
            value={typeof initialValue !== 'undefined' ? this.state.value || '' : props.value || ''}
            className={css.textarea}
            onChange={this.onChange}
            ref="component"
          />
        </div>
      </GenericInput>
    );
  }
}

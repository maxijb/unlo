import React from 'react';
import classnames from 'classnames';
import {translate} from 'react-i18next';
import {Grid} from 'semantic-ui-react';
import get from 'lodash.get';

import {setStyleVars} from '@Common/utils/dom-utils';

import TopMenu from '../menu/top-menu';
import LateralMenu from '../menu/lateral-menu';
import LateralSubMenu from '../menu/lateral-submenu';
import {Submenus, SubmenusSeller, SubmenusRetailer} from '../menu/menu-definition';
import Head from 'next/head';

import css from './dashboard-layout.scss';

export class DashboardLayout extends React.Component {
  componentDidMount() {
    const styleVars = get(this.props, 'config.options.styleVars', {});
    setStyleVars(document.body, styleVars);
  }

  renderForbidden() {
    const {t} = this.props;

    return (
      <Grid columns={16} padded>
        <Grid.Column width={16}>
          <h3>{t('errors.accessDenied')}</h3>
          <div>{t('errors.yourRoleDoesntAllow')}</div>
        </Grid.Column>
      </Grid>
    );
  }

  render() {
    const {
      user: {superAdmin},
      router,
      t,
      api: {
        extended: {fullLogout},
      },
      app: {
        selectedApp,
        config: {appName},
      },
      user: {assets},
      forbidden,
      children,
      isMobile,
      goBack,
    } = this.props;

    let submenus = superAdmin ? Submenus['/dashboard'] : SubmenusSeller['/portal'];

    const app = selectedApp ? assets.get(selectedApp) : null;

    return (
      <div className={css.dashboardLayout}>
        <Head>
          <title>
            {app ? `${app.name} | ` : ''}
            {appName}
          </title>
        </Head>
        <TopMenu {...this.props} logout={fullLogout} goBack={goBack} />
        {submenus && <LateralSubMenu items={submenus} router={router} />}
        <div
          className={classnames(css.dashboardContent, {
            [css.withSubmenu]: Boolean(submenus),
          })}
        >
          {forbidden ? this.renderForbidden() : children}
        </div>
      </div>
    );
  }
}

export default translate(['common'])(DashboardLayout);

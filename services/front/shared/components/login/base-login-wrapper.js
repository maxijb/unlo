import React from 'react';
import isEmpty from 'is-empty';
import Router from 'next/router';
import deepExtend from 'deep-extend';
import GA from 'react-ga';
import {GoogleAppId, FacebookAppId, FacebookApiVersion} from '@Common/constants/social';

import Logger from '@Common/logger';
import {noop} from '@Common/utils/generic-utils';
import {getPageLink} from '@Common/utils/urls';
import {ValidateForm} from '@Common/utils/validations';
import FormValidations from '@Common/constants/form-validations';
import {LoadingStates} from '@Common/constants/social';

const InitialState = {
  email: '',
  password: '',
  loading: null,
  errors: {},
  forgotPassword: false,
  forgotPasswordSent: false,
};

export default function BaseLoginWrapper(WrappedComponent) {
  return class extends React.Component {
    static defaultProps = {
      closeModal: noop,
      onSuccessLogin: noop,
      firstName: '',
      email: '',
      lastName: '',
    };

    constructor(props) {
      super(props);
      const {firstName, lastName, email} = props;
      this.state = deepExtend({...InitialState, firstName, lastName, email});
    }

    componentDidMount() {
      // Should run only once
      // Initializing social providers framework
      if (!window.fbAsyncInit) {
        // Init Facebook SDK
        window.fbAsyncInit = () => {
          FB.init({
            appId: FacebookAppId,
            cookie: true,
            xfbml: true,
            version: FacebookApiVersion,
          });

          FB.AppEvents.logPageView();
        };

        const fjs = document.getElementsByTagName('script')[0];
        if (!document.getElementById('facebook-jssdk')) {
          const js = document.createElement('script');
          js.id = 'facebook-jssdk';
          js.src = 'https://connect.facebook.net/en_US/sdk.js';
          fjs.parentNode.insertBefore(js, fjs);
        }

        // Init Google SDK
        const js2 = document.createElement('script');
        js2.id = 'google-jssdk';
        js2.onload = () => {
          // Retrieve the singleton for the GoogleAuth library and set up the client.
          gapi.load('auth2', function () {
            window.GOOGLE_AUTH = gapi.auth2.init({
              client_id: GoogleAppId,
              cookiepolicy: 'single_host_origin',
            });
          });
        };
        js2.src = 'https://apis.google.com/js/api:client.js';
        fjs.parentNode.insertBefore(js2, fjs);
      }
    }

    onSuccessfulSubmit = resp => {
      const {next = '', onSuccessLogin} = this.props;
      this.props.closeModal();
      GA.set({userId: resp.id});
      if (resp.action === 'signin') {
        GA.event({category: 'goal', action: 'signin', value: 1});
      }

      if (next) {
        window.location = decodeURIComponent(next);
      } else {
        if (resp.isSuperAdmin) {
          window.location = '/dashboard';
        } else if (resp.isSeller) {
          window.location = '/portal/orders';
        } else {
          onSuccessLogin();
        }
      }
    };

    onSubmit() {
      const {inviteToken} = this.props;
      const {errors, loading, email, password, firstName, lastName} = this.state;
      const input = {email, password, firstName, lastName};
      const foundErrors = ValidateForm(input, FormValidations.signup);
      this.setState({
        errors: foundErrors,
        loading: !isEmpty(foundErrors) ? null : LoadingStates.signup,
      });

      if (isEmpty(foundErrors)) {
        this.props.submitForm({...input, inviteToken}).then(({resp}) => {
          const errors = !resp.error ? {} : resp.errorData || {form: {...resp}};
          this.setState({loading: null, errors});
          if (!resp.error) {
            this.onSuccessfulSubmit(resp);
          }
        });
      }
    }

    onForgotPassword() {
      this.setState({forgotPassword: true});
    }

    onGoBackLogin = () => {
      this.setState({forgotPassword: false});
    };

    onForgotPasswordSubmit() {
      const {errors, loading, email} = this.state;
      const input = {email};
      const foundErrors = ValidateForm(input, FormValidations.forgotPassword);
      this.setState({
        errors: foundErrors,
        loading: !isEmpty(foundErrors) ? null : LoadingStates.forgotPassword,
      });

      if (isEmpty(foundErrors)) {
        this.props.sendForgotPassword(input).then(({resp}) => {
          const errors = !resp.error ? {} : resp.errorData || {form: {...resp}};
          this.setState({
            loading: null,
            errors,
            forgotPasswordSent: !resp.error,
          });
        });
      }
    }

    onFacebookLogin(e) {
      const {inviteToken} = this.props;
      e.preventDefault();
      if (!window.FB) {
        return;
      }

      this.setState({loading: LoadingStates.facebook});
      FB.login(
        ({authResponse, status}) => {
          if (authResponse && status === 'connected') {
            this.props.facebookLogin({...authResponse, inviteToken}).then(({resp}) => {
              this.setState({loading: null, errors: {facebook: resp.error}});
              if (!resp.error) {
                this.onSuccessfulSubmit(resp);
              }
            });
          } else {
            this.setState({
              loading: null,
              errors: {facebook: 'connectionFailed'},
            });
          }
        },
        {scope: 'public_profile,email'},
      );
    }

    onGoogleLogin(e) {
      const {inviteToken} = this.props;
      e.preventDefault();
      if (!window.GOOGLE_AUTH) {
        return;
      }

      this.setState({loading: LoadingStates.google});
      window.GOOGLE_AUTH.signIn().then(
        googleUser => {
          const token = googleUser.getAuthResponse().id_token;
          this.props.googleLogin({token, inviteToken}).then(({resp}) => {
            this.setState({loading: null, errors: {google: resp.error}});
            if (!resp.error) {
              this.onSuccessfulSubmit(resp);
            }
          });
        },
        error => {
          this.setState({
            loading: null,
            errors: {google: 'connectionFailed'},
          });
        },
      );
    }

    onUpdateForm(field, eventOrValue) {
      const value = eventOrValue.target ? eventOrValue.target.value : eventOrValue;
      this.setState({[field]: value});
    }

    onCloseModal() {
      this.props.closeModal();
      const state = deepExtend(InitialState);
      this.setState(state);
    }

    render() {
      return (
        <WrappedComponent
          {...this.props}
          form={this.state}
          closeModal={this.onCloseModal.bind(this)}
          onSubmit={this.onSubmit.bind(this)}
          onGoogleLogin={this.onGoogleLogin.bind(this)}
          onFacebookLogin={this.onFacebookLogin.bind(this)}
          onUpdateForm={this.onUpdateForm.bind(this)}
          onForgotPassword={this.onForgotPassword.bind(this)}
          onForgotPasswordSubmit={this.onForgotPasswordSubmit.bind(this)}
          onGoBackLogin={this.onGoBackLogin}
        />
      );
    }
  };
}

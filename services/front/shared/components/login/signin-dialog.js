import React from 'react';
import Link from 'next/link';
import classnames from 'classnames';
import {translate} from 'react-i18next';
import isEmpty from 'is-empty';
import Router from 'next/router';
import {Button, Header, Image, Modal, Form, Transition, Icon, Divider} from 'semantic-ui-react';

import Logger from '@Common/logger';
import {ValidateForm} from '../../../../common/utils/validations';
import FormValidations from '../../../../common/constants/form-validations';
import Input from '../inputs/wrapped-input';
import ErrorRenderer from '../inputs/error-renderer';
import {LoadingStates} from '../../../../common/constants/social';
import BaseLoginWrapper from './base-login-wrapper';

import styles from '@Common/styles/styles.scss';
import css from './signup-dialog.scss';

class SigninDialog extends React.Component {
  renderForgotPassword() {
    const {closeModal, isOpen, t, openSignupModal} = this.props;
    const {email, loading, errors, forgotPasswordSent} = this.props.form;
    return (
      <Modal onClose={closeModal} open={isOpen} size="tiny">
        <Modal.Header as="h3">{t('login.forgotPassword')}</Modal.Header>
        <Modal.Content>
          {forgotPasswordSent ? (
            <div>
              <Header size="medium">{t('login.forgotPasswordSent')}</Header>
              <a onClick={closeModal}>{t('login.youCanCloseWindow')}</a>
            </div>
          ) : (
            <Form onSubmit={this.props.onForgotPasswordSubmit}>
              <Header size="medium">{t('login.forgotPasswordSubtitle')}</Header>

              <Form.Field>
                <label>{t('login.email')}</label>
                <Input
                  size="large"
                  value={email}
                  error={errors.email}
                  disabled={Boolean(loading)}
                  onChange={this.props.onUpdateForm.bind(null, 'email')}
                />
              </Form.Field>
              <Button
                size="large"
                fluid
                type="submit"
                className="uppercase"
                disabled={Boolean(loading)}
                loading={loading === LoadingStates.forgotPassword}
              >
                {t('login.submit')}
              </Button>
              <ErrorRenderer error={errors.form} />
            </Form>
          )}
        </Modal.Content>
      </Modal>
    );
  }

  renderSignin() {
    const {
      closeModal,
      isOpen,
      t,
      form: {email, password, loading, errors},
      openSignupModal,
      onForgotPassword,
    } = this.props;

    return (
      <Modal onClose={closeModal} open={isOpen} size="tiny">
        <Modal.Content>
          <Form onSubmit={this.props.onSubmit}>
            <Form.Field>
              <label>{t('login.email')}</label>
              <Input
                size="large"
                value={email}
                error={errors.email}
                disabled={Boolean(loading)}
                onChange={this.props.onUpdateForm.bind(null, 'email')}
              />
            </Form.Field>
            <Form.Field>
              <label>{t('login.password')}</label>
              <Input
                size="large"
                type="password"
                autocomplete="new-password"
                value={password}
                error={errors.password}
                disabled={Boolean(loading)}
                onChange={this.props.onUpdateForm.bind(null, 'password')}
              />
            </Form.Field>
            <div className={classnames(styles.MarginHalfTop, styles.MarginBottom)}>
              <a onClick={onForgotPassword}>Forgot password?</a>
            </div>
            <Button
              size="large"
              fluid
              primary
              type="submit"
              className="uppercase"
              disabled={Boolean(loading)}
              loading={loading === LoadingStates.signup}
            >
              {t('login.signin')}
            </Button>
            <ErrorRenderer error={errors.form} />
            <div className={classnames(styles.MarginTop, styles.Centered)}>
              Don't have an account?
              <a onClick={openSignupModal}> Create one for free!</a>
            </div>
            <Divider horizontal>{t('login.or')}</Divider>
            <Button
              className={classnames(css.socialButton, 'uppercase')}
              size="large"
              icon
              labelPosition="left"
              color="facebook"
              fluid
              loading={loading === LoadingStates.facebook}
              onClick={this.props.onFacebookLogin}
            >
              <Icon name="facebook" /> {t('login.signinFacebook')}
            </Button>
            <ErrorRenderer error={errors.facebook} />
            <Button
              className={classnames(css.socialButton, 'uppercase')}
              size="large"
              icon
              labelPosition="left"
              color="google plus"
              disabled={Boolean(loading)}
              fluid
              loading={loading === LoadingStates.google}
              onClick={this.props.onGoogleLogin}
            >
              <Icon name="google" /> {t('login.signinGoogle')}
            </Button>
            <ErrorRenderer error={errors.google} />
          </Form>
        </Modal.Content>
      </Modal>
    );
  }

  render() {
    const {
      isOpen,
      form: {forgotPassword},
    } = this.props;

    return (
      <Transition visible={isOpen} animation="scale" duration={500}>
        {forgotPassword ? this.renderForgotPassword() : this.renderSignin()}
      </Transition>
    );
  }
}

export default translate(['common'])(BaseLoginWrapper(SigninDialog));

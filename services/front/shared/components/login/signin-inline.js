import React from 'react';
import Link from 'next/link';
import classnames from 'classnames';
import {translate} from 'react-i18next';
import isEmpty from 'is-empty';
import Router from 'next/router';
import {Button, Header, Image, Modal, Form, Transition, Icon, Divider} from 'semantic-ui-react';
import {Trans} from 'react-i18next';

import Logger from '@Common/logger';
import {ValidateForm} from '../../../../common/utils/validations';
import FormValidations from '../../../../common/constants/form-validations';
import Input from '../inputs/wrapped-input';
import ErrorRenderer from '../inputs/error-renderer';
import {LoadingStates} from '../../../../common/constants/social';
import BaseLoginWrapper from './base-login-wrapper';
import {AppColors} from '@Common/constants/app';

import css from './signup-dialog.scss';
import fonts from '@Common/styles/styles.scss';

class SigninDialog extends React.Component {
  renderForgotPassword() {
    const {closeModal, isOpen, t, openSignupModal, onGoBackLogin} = this.props;
    const {email, loading, errors, forgotPasswordSent} = this.props.form;
    return forgotPasswordSent ? (
      <div className={css.inlineSignupForm}>
        <div>{t('header.logo')}</div>
        <div className={classnames(fonts.H1, css.title)}>{t('login.forgotPasswordSent')}</div>
        <div className={fonts.Medium}>{t('login.forgotPasswordSentSubtitle')}</div>
        <Form.Field>
          <div className={fonts.SmallBold}>
            <a onClick={onGoBackLogin}>{t('login.goBackToLogin')}</a>
          </div>
        </Form.Field>
      </div>
    ) : (
      <Form onSubmit={this.props.onForgotPasswordSubmit} className={css.inlineSignupForm}>
        <div>{t('header.logo')}</div>
        <div className={classnames(fonts.H1, css.title)}>{t('login.resetPassword')}</div>
        <div className={fonts.Medium}>{t('login.resetPasswordInstructions')}</div>
        <Form.Field>
          <Input
            label={t('login.email')}
            size="large"
            value={email}
            error={errors.email}
            name="email"
            disabled={Boolean(loading)}
            onChange={this.props.onUpdateForm.bind(null, 'email')}
            errorBottom={true}
            errorClassName={fonts.Mini}
          />
        </Form.Field>
        <Form.Field>
          <Button
            size="large"
            fluid
            primary
            type="submit"
            disabled={Boolean(loading)}
            loading={loading === LoadingStates.forgotPassword}
          >
            {t('login.submit')}
          </Button>
          <ErrorRenderer error={errors.form} />
        </Form.Field>
        <Form.Field>
          <div className={fonts.SmallBold}>
            <a onClick={onGoBackLogin}>{t('login.goBackToLogin')}</a>
          </div>
        </Form.Field>
      </Form>
    );
  }

  renderSignin() {
    const {
      closeModal,
      isOpen,
      t,
      form: {email, password, loading, errors, showPass},
      openSignupModal,
      onForgotPassword,
    } = this.props;

    return (
      <Form onSubmit={this.props.onSubmit} className={css.inlineSignupForm}>
        <div>
          <img className={css.logo} src={'/static/incredible/logo.svg'} />
        </div>
        <div className={classnames(fonts.H1, css.title)}>{t('login.signin')}</div>
        <div className={fonts.Medium}>{t('login.enterYourDetailsSignIn')}</div>
        <Form.Field>
          <Input
            label={t('login.email')}
            size="large"
            value={email}
            error={errors.email}
            disabled={Boolean(loading)}
            onChange={this.props.onUpdateForm.bind(null, 'email')}
            errorBottom={true}
            errorClassName={fonts.Mini}
          />
        </Form.Field>
        <Form.Field>
          <Input
            label={t('login.password')}
            size="large"
            value={password}
            error={errors.password}
            disabled={Boolean(loading)}
            onChange={this.props.onUpdateForm.bind(null, 'password')}
            type={showPass ? 'text' : 'password'}
            errorBottom={true}
            errorClassName={fonts.Mini}
            rightAddon={
              <Icon
                name="eye slash outline"
                className={classnames(css.viewPass, {[css.showingPass]: showPass})}
                onClick={this.props.onUpdateForm.bind(null, 'showPass', !showPass)}
              />
            }
          />
        </Form.Field>
        <Form.Field>
          <Button
            size="large"
            fluid
            type="submit"
            primary
            disabled={Boolean(loading)}
            loading={loading === LoadingStates.signup}
          >
            {t('login.signin')}
          </Button>
          <ErrorRenderer error={errors.form} />
        </Form.Field>
        <Form.Field>
          <div className={classnames(fonts.SmallBold, css.firstLink)}>
            <a onClick={onForgotPassword}>{t('login.forgotPassword')}</a>
          </div>
        </Form.Field>
        <Form.Field>
          <div className={fonts.SmallBold}>
            <Trans i18nKey="login.dontHaveAccountCreate">
              {/* prettier-ignore */}
              Text
              <a onClick={openSignupModal} className={css.link}>
                link
              </a>
            </Trans>
          </div>
        </Form.Field>
        <Form.Field>
          <Divider horizontal>
            <span className={css.socialDivider}>{t('login.orSignin')}</span>
          </Divider>
        </Form.Field>
        <Button
          className={classnames(css.socialButton)}
          size="large"
          icon
          labelPosition="left"
          color="facebook"
          fluid
          loading={loading === LoadingStates.facebook}
          onClick={this.props.onFacebookLogin}
        >
          <Icon name="facebook" inverted /> {t('login.signinFacebook')}
        </Button>
        <ErrorRenderer error={errors.facebook} />
        <Button
          className={classnames(css.socialButton)}
          size="large"
          icon
          labelPosition="left"
          color="google plus"
          disabled={Boolean(loading)}
          fluid
          loading={loading === LoadingStates.google}
          onClick={this.props.onGoogleLogin}
        >
          <Icon name="google" inverted /> {t('login.signinGoogle')}
        </Button>
        <ErrorRenderer error={errors.google} />
      </Form>
    );
  }

  render() {
    const {
      isOpen,
      form: {forgotPassword},
    } = this.props;

    return (
      <Transition visible={isOpen} animation="scale" duration={500}>
        {forgotPassword ? this.renderForgotPassword() : this.renderSignin()}
      </Transition>
    );
  }
}

export default translate(['common'])(BaseLoginWrapper(SigninDialog));

import React from 'react';
import Link from 'next/link';
import classnames from 'classnames';
import {translate} from 'react-i18next';
import isEmpty from 'is-empty';
import Router from 'next/router';
import {Button, Header, Image, Modal, Form, Transition, Icon, Divider} from 'semantic-ui-react';

import Logger from '@Common/logger';
import {ValidateForm} from '../../../../common/utils/validations';
import FormValidations from '../../../../common/constants/form-validations';
import {LoadingStates} from '../../../../common/constants/social';
import Input from '../inputs/wrapped-input';
import ErrorRenderer from '../inputs/error-renderer';
import BaseLoginWrapper from './base-login-wrapper';

import css from './signup-dialog.scss';

export class SignupDialog extends React.Component {
  render() {
    const {closeModal, isOpen, t, openSigninModal} = this.props;
    const {email, password, loading, errors} = this.props.form;

    return (
      <Transition visible={isOpen} animation="scale" duration={500}>
        <Modal onClose={closeModal} open={isOpen} size="tiny">
          <Modal.Content>
            <Form onSubmit={this.props.onSubmit}>
              <Form.Field>
                <label>{t('login.email')}</label>
                <Input
                  size="large"
                  value={email}
                  error={errors.email}
                  disabled={Boolean(loading)}
                  onChange={this.props.onUpdateForm.bind(null, 'email')}
                />
              </Form.Field>
              <Form.Field>
                <label>{t('login.password')}</label>
                <Input
                  size="large"
                  type="password"
                  value={password}
                  error={errors.password}
                  disabled={Boolean(loading)}
                  onChange={this.props.onUpdateForm.bind(null, 'password')}
                />
              </Form.Field>
              <Form.Field>
                <Button
                  size="large"
                  fluid
                  primary
                  type="submit"
                  className="uppercase"
                  disabled={Boolean(loading)}
                  loading={loading === LoadingStates.signup}
                >
                  {t('login.signup')}
                </Button>
              </Form.Field>
              {errors.form && <ErrorRenderer error={errors.form} />}
              <div>
                Already have an account?
                <a onClick={openSigninModal}> Sign in instead</a>
              </div>
              <Divider horizontal>{t('login.or')}</Divider>
              <Button
                className={classnames(css.socialButton, 'uppercase')}
                size="large"
                icon
                labelPosition="left"
                color="facebook"
                fluid
                disabled={Boolean(loading)}
                loading={loading === LoadingStates.facebook}
                onClick={this.props.onFacebookLogin}
              >
                <Icon name="facebook" /> {t('login.signupFacebook')}
              </Button>
              <ErrorRenderer error={errors.facebook} />
              <Button
                className={classnames(css.socialButton, 'uppercase')}
                size="large"
                icon
                labelPosition="left"
                color="google plus"
                fluid
                disabled={Boolean(loading)}
                loading={loading === LoadingStates.google}
                onClick={this.props.onGoogleLogin}
              >
                <Icon name="google" /> {t('login.signupGoogle')}
              </Button>
              <ErrorRenderer error={errors.google} />
            </Form>
          </Modal.Content>
        </Modal>
      </Transition>
    );
  }
}

export default translate(['common'])(BaseLoginWrapper(SignupDialog));

import React from 'react';
import Link from 'next/link';
import classnames from 'classnames';
import {translate} from 'react-i18next';
import isEmpty from 'is-empty';
import Router from 'next/router';
import {Button, Header, Image, Modal, Form, Transition, Icon, Divider} from 'semantic-ui-react';
import {Trans} from 'react-i18next';

import {getPageLink} from '@Common/utils/urls';
import Logger from '@Common/logger';
import {ValidateForm} from '../../../../common/utils/validations';
import FormValidations from '../../../../common/constants/form-validations';
import {LoadingStates} from '../../../../common/constants/social';
import Input from '../inputs/wrapped-input';
import Checkbox from '../inputs/wrapped-checkbox';
import ErrorRenderer from '../inputs/error-renderer';
import BaseLoginWrapper from './base-login-wrapper';
import {Logo} from '@Widget/components/display/svg-icons';
import {AppColors} from '@Common/constants/app';

import fonts from '@Common/styles/styles.scss';
import css from './signup-dialog.scss';

export class SignupDialog extends React.Component {
  render() {
    const {closeModal, isOpen, t, openSigninModal} = this.props;
    const {email, password, loading, errors, showPass, terms} = this.props.form;

    return (
      <Form onSubmit={this.props.onSubmit} className={css.inlineSignupForm}>
        <div>
          <img className={css.logo} src={'/static/incredible/logo.svg'} />
        </div>
        <div className={classnames(fonts.H1, css.title)}>{t('login.signup')}</div>
        <div className={fonts.Medium}>{t('login.enterYourDetails')}</div>
        <Form.Field>
          <Input
            label={t('login.email')}
            size="large"
            value={email}
            error={errors.email}
            disabled={Boolean(loading)}
            onChange={this.props.onUpdateForm.bind(null, 'email')}
            errorBottom={true}
            errorClassName={fonts.Mini}
          />
        </Form.Field>
        <Form.Field>
          <Input
            label={t('login.password')}
            size="large"
            value={password}
            error={errors.password}
            disabled={Boolean(loading)}
            onChange={this.props.onUpdateForm.bind(null, 'password')}
            type={showPass ? 'text' : 'password'}
            errorBottom={true}
            errorClassName={fonts.Mini}
            rightAddon={
              <Icon
                name="eye slash outline"
                className={classnames(css.viewPass, {[css.showingPass]: showPass})}
                onClick={this.props.onUpdateForm.bind(null, 'showPass', !showPass)}
              />
            }
          />
        </Form.Field>
        <Form.Field>
          <Button
            size="large"
            fluid
            primary
            type="submit"
            disabled={Boolean(loading)}
            loading={loading === LoadingStates.signup}
          >
            {t('login.signup')}
          </Button>
          {errors.form && <ErrorRenderer error={errors.form} />}
        </Form.Field>
        <div className={css.terms}>
          <Trans i18nKey="login.byClickingTermsAndConditions">
            Text <a href={getPageLink('terms')}>link</a>
          </Trans>
        </div>
        <Form.Field>
          <div className={fonts.SmallBold}>
            <Trans i18nKey="login.signinInstead">
              {/* prettier-ignore */}
              Text
              <a onClick={openSigninModal} className={css.link}>
                link
              </a>
            </Trans>
          </div>
        </Form.Field>
        <Divider horizontal>
          <span className={css.socialDivider}>{t('login.or')}</span>
        </Divider>
        <Button
          className={classnames(css.socialButton)}
          size="large"
          icon
          labelPosition="left"
          color="facebook"
          fluid
          disabled={Boolean(loading)}
          loading={loading === LoadingStates.facebook}
          onClick={this.props.onFacebookLogin}
        >
          <Icon name="facebook" inverted /> {t('login.signupFacebook')}
        </Button>
        <ErrorRenderer error={errors.facebook} />
        <Button
          className={classnames(css.socialButton)}
          size="large"
          icon
          labelPosition="left"
          color="google plus"
          fluid
          disabled={Boolean(loading)}
          loading={loading === LoadingStates.google}
          onClick={this.props.onGoogleLogin}
        >
          <Icon name="google" inverted /> {t('login.signupGoogle')}
        </Button>
        <ErrorRenderer error={errors.google} />
      </Form>
    );
  }
}

export default translate(['common'])(BaseLoginWrapper(SignupDialog));

import React from 'react';
import classnames from 'classnames';
import isEmpty from 'is-empty';
import Link from 'next/link';
import get from 'lodash.get';
import {translate} from 'react-i18next';
import {Icon, Popup, Menu} from 'semantic-ui-react';

import WithSocket from '@Components/hocs/with-socket';
import {getPageLink} from '@Common/utils/urls';
import {noop} from '@Common/utils/generic-utils';
import {SocketEvents, SocketEventsPage} from '@Common/constants/socket-status';
import {cdnURL} from '@Common/utils/statif-assets-utils';
import {getVisibilityAPIVars} from '@Common/utils/dom-utils';

import css from './alert-notifications.scss';
import menuCss from './asset-menu.scss';

export class AlertNotifications extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      flickeringTitle: false
    };
  }

  componentDidMount() {
    const {socket, router, actions} = this.props;
    if (socket) {
      socket.on(SocketEvents.message, this.handleMessage);
      socket.on(SocketEvents.newInteraction, this.handleMessage);
      socket.on(SocketEvents.submitBooking, this.handleBooking);
    }

    this.evaluateNotifications();

    Object.assign(this, getVisibilityAPIVars());
    if (this.hidden) {
      document.addEventListener(this.visibilityChange, this.setWindowActive, false);
    }
  }

  componentWillUnmount() {
    const {socket} = this.props;
    if (socket) {
      socket.off(SocketEvents.message, this.handleMessage);
      socket.off(SocketEvents.newInteraction, this.handleMessage);
      socket.off(SocketEvents.submitBooking, this.handleBooking);
    }
  }

  setWindowActive = () => {
    const active = !document[this.hidden];
    this.props.actions.setWindowActive({active});
    setTimeout(() => {
      this.evaluateNotifications();
    }, 1000);

    if (active) {
      this.finishMarquee();
    }
  };

  evaluateNotifications = () => {
    const {
      router,
      actions,
      user: {notifications}
    } = this.props;

    Object.keys(SocketEventsPage).forEach(key => {
      if (router.pathname === SocketEventsPage[key]) {
        actions.removeNotification({type: key}).then(() => {
          if (!notifications.isEmpty() && this.props.user.notifications.isEmpty()) {
            this.changeFavicon(false);
          }
        });
      }
    });
  };

  handleBooking = booking => {
    this.handleNotification(SocketEvents.submitBooking);
    this.props.actions.addBooking(booking);
  };

  handleMessage = msg => {
    this.handleNotification(SocketEvents.message);
  };

  handleNotification = type => {
    const {
      user: {notifications},
      app: {isWindowActive},
      t
    } = this.props;

    if (notifications.isEmpty()) {
      this.changeFavicon(true);
    }
    this.props.actions.addNotification({type});
    this.refs.sound.play();
    if (!isWindowActive) {
      this.initTitleMarquee(t(`notificationWindowTitle.${type}`));
    }
  };

  initTitleMarquee = title => {
    if (this.state.flickeringTitle || !title) {
      return;
    }

    this.setState(
      {originalTitle: document.title, alternativeTitle: title, flickeringTitle: true},
      () => {
        this.continueMarquee();
      }
    );
  };

  continueMarquee = () => {
    const {originalTitle, alternativeTitle} = this.state;
    const title = document.title === originalTitle ? alternativeTitle : originalTitle;
    document.title = title;
    this.marqueeTimeout = setTimeout(() => this.continueMarquee(), 1000);
  };

  finishMarquee = () => {
    window.title = this.state.originalTitle;
    clearTimeout(this.marqueeTimeout);
    this.setState({flickeringTitle: false});
  };

  onToggle = () => {
    this.setState({open: !this.state.open});
  };

  changeFavicon = alert => {
    const src = alert
      ? '/static/landing/favicon/favicon-alert.ico'
      : '/static/landing/favicon/favicon.ico';

    const head = document.head || (document.head = document.getElementsByTagName('head')[0]);

    const link = document.createElement('link');
    const oldLink = document.getElementById('dynamic-favicon');
    link.id = 'dynamic-favicon';
    link.rel = 'shortcut icon';
    link.href = src;
    if (oldLink) {
      head.removeChild(oldLink);
    }
    head.appendChild(link);
  };

  renderMenu() {
    const {
      user: {notifications},
      t
    } = this.props;

    const items = notifications.items().map(n => (
      <Menu.Item key={n.id} as="a" className={classnames(menuCss.menuItem, css.menu)}>
        <Link href={SocketEventsPage[n.id]}>
          <span>{t(`dashboard:notificationMenu.${n.id}`, {count: n.count})}</span>
        </Link>
      </Menu.Item>
    ));

    return (
      <Menu vertical borderless={true} className={classnames(css.menu)}>
        {notifications.isEmpty() ? (
          <Menu.Item className={classnames(menuCss.menuItem, css.menu)}>
            {t('notificationMenu.notFound')}
          </Menu.Item>
        ) : (
          items
        )}
      </Menu>
    );
  }

  render() {
    const {open} = this.state;
    const {
      user: {notifications}
    } = this.props;
    const hasNotifications = !notifications.isEmpty();

    const trigger = (
      <div className={css.notificationHolder}>
        <Icon
          name="bell"
          className={classnames(css.bell, {
            [css.selected]: hasNotifications
          })}
          onClick={this.onToggle}
        />
        {hasNotifications > 0 && (
          <div className={css.notificationCount}>{notifications.length()}</div>
        )}
      </div>
    );

    return (
      <div>
        <Popup
          trigger={trigger}
          position="bottom center"
          open={open}
          on="click"
          onClose={this.onToggle}
          style={{padding: 0, width: '400px'}}
          size="large"
        >
          {this.renderMenu()}
        </Popup>
        <audio ref={'sound'} src={cdnURL('/static/notification.mp3')} autoPlay={false} />
      </div>
    );
  }
}

export default translate(['dashboard'])(WithSocket(AlertNotifications));

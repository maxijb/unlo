import React from 'react';
import Router from 'next/router';
import classnames from 'classnames';
import {translate} from 'react-i18next';
import get from 'lodash.get';
import isEmpty from 'is-empty';
import {Icon, Popup, Menu} from 'semantic-ui-react';
import {noop} from '@Common/utils/generic-utils';
import {DayStatus} from '@Common/constants/dates';
import {Entities} from '@Common/constants/assets';
import {cdnURL, UserFilesCdnURL} from '@Common/utils/statif-assets-utils';
import {hasRegularOpenHours, getAbsDateOnly} from '@Common/utils/date-utils';
import {AssetStatus} from '@Common/constants/assets';
import {getPageLink} from '@Common/utils/urls';
import css from './asset-menu.scss';

class AssetMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }

  selectApp = id => {
    const url = `${window.location.pathname}?app=${id}`;
    window.location = url;
  };

  acceptInvite = asset => {
    window.location = getPageLink('invite', asset.inviteToken);
  };

  onClose = e => {
    e.stopPropagation();
    this.setState({open: false});
  };

  onOpen = e => {
    e.stopPropagation();
    this.setState({open: true});
  };

  goToSettings = e => {
    Router.push(getPageLink('account'));
  };

  goToTeammates = e => {
    Router.push(getPageLink('teammates'));
  };

  render() {
    const {trigger, assets, onLogout, t, selectedApp, triggerClassName} = this.props;
    const {open} = this.state;

    const visibleAssets = assets.items().filter(a => a.type === Entities.app);

    const clickableTrigger = (
      <div onClick={this.onOpen} className={triggerClassName}>
        {trigger}
      </div>
    );
    return (
      <Popup
        trigger={clickableTrigger}
        position="bottom right"
        open={open}
        on="click"
        onClose={this.onClose}
        style={{padding: 0, width: '400px'}}
        size="large"
      >
        <Menu vertical borderless={true} className={css.menu}>
          {visibleAssets.map(asset => {
            // Do not show requested assets
            if (asset.status === AssetStatus.requested) {
              return null;
            }

            const avatar = asset.avatar ? UserFilesCdnURL(asset.avatar) : '/static/avatar.png';

            const isActive = asset.id === selectedApp;
            const isInvite = asset.status === AssetStatus.invited;

            return (
              <Menu.Item
                name={asset.name}
                key={asset.id}
                onClick={
                  isActive
                    ? noop
                    : isInvite
                    ? this.acceptInvite.bind(this, asset)
                    : this.selectApp.bind(this, asset.id)
                }
                as="a"
                className={classnames(css.menuItem, {
                  [css.active]: isActive,
                  [css.invite]: isInvite,
                })}
              >
                <img src={avatar} className={css.avatar} />
                <div className={css.menuText}>
                  {asset.name}
                  <div className={css.menuSubText}>
                    {isActive
                      ? 'Current workspace'
                      : isInvite
                      ? 'Accept invitation'
                      : 'Switch workspace'}
                  </div>
                </div>
                {asset.id === selectedApp && <Icon name="circle check" className={css.iconRight} />}
              </Menu.Item>
            );
          })}

          <Menu.Item name="logout" onClick={onLogout} as="a" className={css.menuItem}>
            <Icon name="power off" className={css.iconLeft} />
            <span className={css.menuText}>{t('header.logout')}</span>
          </Menu.Item>
        </Menu>
      </Popup>
    );
  }
}

export default translate(['dashboard'])(AssetMenu);

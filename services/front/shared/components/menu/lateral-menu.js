import React from 'react';
import classnames from 'classnames';
import {Button} from 'semantic-ui-react';
import Link from 'next/link';
import get from 'lodash.get';

import {getPageLink} from '@Common/utils/urls';
import {noop} from '@Common/utils/generic-utils';
import {AppColors} from '@Common/constants/app';
import {Logo, CloseMenu} from '@Widget/components/display/svg-icons';
import {Icon} from 'semantic-ui-react';

import {translate} from 'react-i18next';

import {MenuTopIcons, MenuBottomIcons} from './menu-definition';

import css from './lateral-menu.scss';

export class LateralMenu extends React.Component {
  renderIcons(icons) {
    const {
      router: {route},
      minified,
      t
    } = this.props;

    return icons.map(item => {
      const Icon = item.display;
      const selected = item.pattern && item.pattern.test(route);
      const iconColor = minified
        ? selected
          ? AppColors.accent
          : AppColors.black90
        : selected
        ? AppColors.accent
        : AppColors.white;
      const display = <Icon color={iconColor} />;
      return (
        <li
          key={item.href || item.onClick}
          className={classnames(css.menuItem, css[`${item.name}MenuIcon`], {
            [css.selected]: selected
          })}
        >
          {item.href && (
            <Link prefetch href={item.href}>
              <a>
                {display}
                {minified && <span>{t(`lateralMenu.${item.name}`)}</span>}
              </a>
            </Link>
          )}
          {item.onClick && (
            <a onClick={get(this.props, item.onClick, noop)}>
              {display}
              {minified && <span>{t(`lateralMenu.${item.name}`)}</span>}
            </a>
          )}
          {!minified && <div className={css.tooltip}>{t(`lateralMenu.${item.name}`)}</div>}
        </li>
      );
    });
  }

  renderIcon() {
    const {minified, isOpen, goBack, toggleMenu} = this.props;
    if (!minified) {
      return null;
    }

    if (isOpen) {
      return (
        <div onClick={toggleMenu}>
          <CloseMenu color={AppColors.black90} className={css.goBackIcon} />
        </div>
      );
    }

    if (!goBack) {
      return (
        <div onClick={toggleMenu}>
          <Logo color={AppColors.accent} className={css.logo} />
        </div>
      );
    }

    return (
      <div onClick={goBack}>
        <Icon name="arrow left" color={'blue'} className={css.goBackIcon} />
      </div>
    );
  }

  render() {
    const {
      router: {route},
      t,
      minified,
      goBack,
      isOpen,
      toggleMenu,
      user
    } = this.props;

    return (
      <div
        className={classnames(css.lateralMenuContainer, {
          [css.minified]: minified
        })}
      >
        {this.renderIcon()}
        {(!minified || isOpen) && user.id && (
          <div className={css.iconsListHolder}>
            <ul className={classnames(css.iconsList, css.topIcons)}>
              {this.renderIcons(MenuTopIcons)}
            </ul>

            <ul className={classnames(css.iconsList, css.bottomIcons)}>
              {this.renderIcons(MenuBottomIcons)}
            </ul>
          </div>
        )}
      </div>
    );
  }
}

export default translate(['common'])(LateralMenu);

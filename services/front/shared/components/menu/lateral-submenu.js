import React from 'react';
import classnames from 'classnames';
import {Button} from 'semantic-ui-react';
import Link from 'next/link';
import get from 'lodash.get';

import {getPageLink} from '@Common/utils/urls';
import {noop} from '@Common/utils/generic-utils';

import {translate} from 'react-i18next';

import {MenuTopIcons, MenuBottomIcons} from './menu-definition';

import css from './lateral-submenu.scss';

export class LateralSubMenu extends React.Component {
  renderItems(items) {
    const {
      router: {route},
      t,
    } = this.props;

    return items.map(item => {
      return (
        <li
          key={item.href || item.onClick}
          className={classnames(css.submenuItem, {[css.selected]: route === item.href})}
        >
          {item.href && (
            <Link prefetch href={item.href} as={item.as || item.href}>
              <a>{t(item.display)}</a>
            </Link>
          )}
          {item.onClick && <a onClick={get(this.props, item.onClick, noop)}>{t(item.display)}</a>}
        </li>
      );
    });
  }

  render() {
    const {items} = this.props;
    return (
      <div className={css.lateralSubMenuContainer}>
        <ul className={classnames(css.submenuList)}>{this.renderItems(items)}</ul>
      </div>
    );
  }
}

export default translate(['dashboard'])(LateralSubMenu);

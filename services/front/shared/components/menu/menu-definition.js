import React from 'react';
import {cdnURL} from '@Common/utils/statif-assets-utils';
import {
  ContactIcon,
  ConversationsIcon,
  BookingsIcon,
  SettingsIcon,
} from '@Components/display/admin-svg-icons';
import {Logo} from '@Widget/components/display/svg-icons';

export const MenuTopIcons = [
  {
    display: Logo,
    href: '/dashboard',
    name: 'dashboard',
    pattern: /\/dashboard\/?$/,
  },

  {
    display: ContactIcon,
    href: '/dashboard/contacts',
    name: 'contacts',
    pattern: /\/dashboard\/contacts(\/.*)?/,
  },
  {
    display: ConversationsIcon,
    href: '/dashboard/chat',
    name: 'conversations',
    pattern: /\/dashboard\/chat(\/.*)?/,
  },
  // Config widget by json
  //{
  //  display: <img src={cdnURL('/static/icons/menu/builder.svg')} />,
  //  href: '/dashboard/configure-widget',
  //  pattern: /\/dashboard\/configure-widget(\/.*)?/
  //},
  {
    display: BookingsIcon,
    href: '/dashboard/calendar',
    name: 'bookings',
    pattern: /\/dashboard\/calendar(\/.*)?/,
  },
];

export const MenuBottomIcons = [
  {
    display: SettingsIcon,
    href: '/dashboard/settings',
    name: 'settings',
    pattern: /\/dashboard\/settings(\/.*)?/,
  },
];

const submenus = {
  '/dashboard': [
    {
      href: '/dashboard/category',
      display: 'submenu.categories',
    },
    {
      href: '/dashboard/products',
      display: 'submenu.products',
    },
    //{
    //  href: '/dashboard/attributes',
    //  display: 'submenu.attributes',
    //},
    {
      href: '/dashboard/settings',
      display: 'submenu.settings',
    },
    {
      href: '/dashboard/orders',
      display: 'submenu.orders',
    },
    {
      href: '/dashboard/payments',
      display: 'submenu.payments',
    },
    {
      href: '/dashboard/tokens',
      display: 'submenu.tokens',
    },
  ],
};

const submenusSeller = {
  '/portal': [
    {
      href: '/dashboard/orders',
      as: '/portal/orders',
      display: 'submenu.orders',
    },
    {
      href: '/portal/settings',
      display: 'submenu.settings',
    },
    {
      href: '/dashboard/products',
      as: '/portal/products',
      display: 'submenu.products',
    },
    {
      href: '/dashboard/payments',
      as: '/portal/payments',
      display: 'submenu.payments',
    },
    {
      href: '/portal/api-keys',
      display: 'submenu.apiKeys',
    },
  ],
};

const submenusRetailer = {
  '/portal': [
    {
      href: '/portal/settings',
      display: 'submenu.settings',
    },

    {
      href: '/portal/api-keys',
      display: 'submenu.apiKeys',
    },
  ],
};

export const SubmenusRetailer = submenusRetailer;
export const SubmenusSeller = submenusSeller;
export const Submenus = submenus;

import React from 'react';
import classnames from 'classnames';
import {Grid, Icon} from 'semantic-ui-react';
import Link from 'next/link';
import get from 'lodash.get';
import {cdnURL, UserFilesCdnURL} from '@Common/utils/statif-assets-utils';

import {getPageLink} from '@Common/utils/urls';
import {noop} from '@Common/utils/generic-utils';

import {translate} from 'react-i18next';

import AssetsMenu from './asset-menu';
import Avatar from '@Components/information/avatar';
import AlertNotifications from './alert-notifications';
import LateralMenu from './lateral-menu';

import styles from '@Common/styles/styles.scss';
import css from './top-menu.scss';

export class TopMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isOpen: false};
  }

  toggleMenu = () => {
    this.setState({isOpen: !this.state.isOpen});
  };

  render() {
    const {
      router: {route},
      app: {selectedApp},
      user,
      user: {assets, superAdmin},
      t,
      logout,
      mainTitle,
      app,
      target,
      router,
      isMobile,
      goBack,
      refreshAction,
      loadingRefresh,
    } = this.props;

    const {isOpen} = this.state;
    const asset = (selectedApp && assets.get(selectedApp)) || {};
    const avatar = asset.avatar ? UserFilesCdnURL(asset.avatar) : '/static/avatar.png';

    const trigger = (
      <div className={css.assetMenuTrigger}>
        {!isMobile ? (
          <div>
            {user.firstName || user.lastName ? (
              <div className={css.userName}>{`${user.firstName} ${user.lastName}`}</div>
            ) : null}
            <div className={css.assetName}>{asset.name}</div>
          </div>
        ) : (
          <div />
        )}
        <Avatar src={avatar} className={css.avatar} />
      </div>
    );

    return (
      <div className={css.topMenuContainer}>
        <div className={css.mainTitleHolder}>
          <div className={styles.LargeNormal}>
            <img className={css.logo} src={'/static/incredible/logo.svg'} alt="Incredible Phones" />
            <span className={classnames(styles.ColorSubtext, styles.MarginLeft)}>
              {superAdmin ? t('dashboard') : t('thePortal')}
            </span>
          </div>
          {refreshAction && (
            <Icon
              name="refresh"
              className={classnames(css.refreshButton, {
                [css.loading]: loadingRefresh,
              })}
              onClick={refreshAction}
            />
          )}
        </div>
        {user.id && (
          <div className={css.rightActions}>
            <AssetsMenu
              selectedApp={selectedApp}
              assets={assets}
              trigger={trigger}
              triggerClassName={css.assetMenuTriggerContainer}
              onLogout={logout}
            />
          </div>
        )}
      </div>
    );
  }
}

export default translate(['dashboard'])(TopMenu);

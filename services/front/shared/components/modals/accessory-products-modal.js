import React, {useState, useMemo} from 'react';
import Router from 'next/router';
import {translate} from 'react-i18next';
import {Modal, Grid, Button, Loader} from 'semantic-ui-react';
import {withAppConsumer} from '@Components/app-context';
import GenericModal from './generic-modal';
import classnames from 'classnames';
import {AppColors} from '@Common/constants/app';
import {BlueOK} from '../../../widget/components/display/svg-icons';

import {findRelevantImages, getTagsSet} from '../../utils/attributes-utils';
import AccessoryProduct from '../product/accessory-product';
import Image from '../images/image';

import css from './accessory-products-modal.scss';
import modalCss from './generic-model.scss';
import styles from '@Common/styles/styles.scss';

function AccessoryProductsModal({
  onClose,
  cart,
  product,
  api,
  t,
  loadingAccessoryProducts,
  context: {GA},
}) {
  const images = useMemo(() => {
    return findRelevantImages(product.allImages, getTagsSet(product.defaultVersion.tags));
  }, [product.allImages, product.defaultVersion.tags]);

  const image = product.imageMap[images[0]?.img_id];
  console.log(product.accessoryProducts);
  return (
    <GenericModal onClose={onClose} open={true}>
      <Modal.Header className={modalCss.headerWithShadow}>
        <div className={classnames(styles.FlexSpaceBetween)}>
          <div className={classnames(styles.FlexCenter)}>
            <BlueOK color={AppColors.positive} />
            {image && <Image file={image} size={50} className={styles.MarginHalfLeft} />}
            <div className={styles.MarginLeft}>
              <div className={styles.Button}>{t('addedToCart')}</div>
              <div className={styles.ButtonRegular}>{product.product.name}</div>
            </div>
          </div>
          <div className={classnames(styles.FlexCenter, styles.ButtonRegular)}>
            <a onClick={onClose} className={classnames(styles.SecondaryLink, styles.MarginRight)}>
              {t('goBack')}
            </a>
            <Button
              primary
              onClick={() => {
                Router.push('/cart');
                GA.event({
                  category: 'cart',
                  action: 'continue-to-cart',
                  label: 'associate-product-modal',
                });
              }}
            >
              {t('continue')}
            </Button>
          </div>
        </div>
      </Modal.Header>
      <Modal.Content>
        <div className={styles.Large}>{t('weThinkYouWillLike')}</div>
        <div
          className={classnames(
            styles.ButtonRegular,
            styles.MarginHalfTop,
            styles.MarginDoubleBottom,
          )}
        >
          {t('appleQualityAccesories')}
        </div>
        {loadingAccessoryProducts && (
          <div className={classnames(styles.MarginDoubleTop, styles.MarginDoubleBottom)}>
            <Loader active={loadingAccessoryProducts} inline="centered" />
          </div>
        )}
        {product.accessoryProducts.map((it, i) => (
          <AccessoryProduct key={it.id} product={it} api={api} cart={cart} />
        ))}
      </Modal.Content>
    </GenericModal>
  );
}

export default withAppConsumer(translate(['home', 'common'])(AccessoryProductsModal));

import React, {useState, useMemo, useEffect} from 'react';
import {translate} from 'react-i18next';
import {Modal, Grid, Button, Table} from 'semantic-ui-react';
import GenericModal from './generic-modal';
import classnames from 'classnames';
import Image from '@Components/images/image';
import Option from '@Components/product/option';
import Price from '@Components/product/price';
import TagSelector from '@Components/product/tag-selector';

import css from './generic-model.scss';
import styles from '@Common/styles/styles.scss';

function preventBubbling(e) {
  e.stopPropagation();
}

function AppearanceModal({
  onClose,
  onChange,
  appearances,
  selectedTags,
  minimumPricePerCondition,
  variant,

  t,
}) {
  const appearanceOptions = useMemo(() => {
    return appearances.map(attr => {
      let minPrice = minimumPricePerCondition[attr.id] || null;

      if (selectedTags.has(attr.id)) {
        if (variant?.price && (!minPrice || minPrice < variant.price)) {
          minPrice = variant.price;
        }
      }
      return {
        ...attr,
        content: (
          <div>
            <div className={classnames(styles.FlexSpaceBetween, styles.MarginHalfBottom)}>
              <div className={styles.Button}>{t(attr.name)}</div>
              <div>
                <span className={styles.Button}>
                  {minPrice !== null && <Price value={minPrice} />}
                </span>
              </div>
            </div>
            {attr.name === 'Excellent' && (
              <div className={classnames(styles.Left, styles.Tiny, styles.ColorSubtext)}>
                <div>
                  <b>Screen:</b> Absolutely no scratches. <b>Body:</b> No scratches visible from a
                  distance of 8 inches or more. <b>Battery:</b> Great health that exceeds 80%
                  capacity relative to new. No fast drain.{' '}
                </div>
              </div>
            )}
            {attr.name === 'Good' && (
              <div className={classnames(styles.Left, styles.Tiny, styles.ColorSubtext)}>
                <div>
                  <b>Screen:</b> Light micro scratches that are not visible when the screen is on.{' '}
                  <b>Body:</b> No scratches visible from an arms length distance. <b>Battery:</b>{' '}
                  Great health that exceeds 80% capacity relative to new. No fast drain.{' '}
                </div>
              </div>
            )}
            {attr.name === 'Fair' && (
              <div className={classnames(styles.Left, styles.Tiny, styles.ColorSubtext)}>
                <div>
                  <b>Screen:</b> Moderate scratches that are not visible when the screen is on.{' '}
                  <b>Body:</b> May have visible signs of use, with small scratches that can be felt.{' '}
                  <b>Battery:</b> Great health that exceeds 80% capacity relative to new. No fast
                  drain.{' '}
                </div>
              </div>
            )}
          </div>
        ),
      };
    });
  });

  return (
    <GenericModal
      onClose={onClose}
      open={true}
      size="tiny"
      onClick={preventBubbling}
      onDoubleClick={preventBubbling}
    >
      <Modal.Header>
        <div className={classnames(styles.MediumBold, styles.MarginBottom, styles.centered)}>
          {t('selectAnAppearance')}
        </div>
        <div className={classnames(styles.Small, styles.MarginBottom, styles.centered)}>
          {t('selectAppearanceExplanation')}
        </div>
      </Modal.Header>
      <Modal.Content>
        <TagSelector
          vertical={true}
          padded={true}
          selectedTags={selectedTags}
          tags={appearanceOptions}
          onChange={value => {
            onChange(value);
            onClose();
          }}
          onClickSelected={onClose}
        />
      </Modal.Content>
    </GenericModal>
  );
}

export default translate(['home'])(AppearanceModal);

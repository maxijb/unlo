import React, {useState, useMemo, useEffect} from 'react';
import {translate} from 'react-i18next';
import {Modal, Grid, Button, Table} from 'semantic-ui-react';
import GenericModal from './generic-modal';
import classnames from 'classnames';
import {multiLineTrans} from '@Common/utils/generic-utils';

import css from './generic-model.scss';
import styles from '@Common/styles/styles.scss';

function BatteryModal({onClose, isOpen, t}) {
  return (
    <GenericModal onClose={onClose} open={isOpen} size="tiny">
      <Modal.Header>
        <div className={classnames(styles.MediumBold, styles.centered, styles.MarginBottom)}>
          {t('batteryLife')}
        </div>
        <div className={classnames(styles.Small, styles.ColorSubtext, styles.centered)}>
          {t('what80CapacityMeans')}
        </div>
      </Modal.Header>
      <Modal.Content>
        <div className={styles.sectionWithBackground}>
          {[0, 1, 2].map(i => (
            <li key={i} className={styles.ListItem}>
              {t(`batteryItems.${i}`)}
            </li>
          ))}
        </div>
      </Modal.Content>
    </GenericModal>
  );
}

export default translate(['home'])(BatteryModal);

import React, {PureComponent} from 'react';
import {translate} from 'react-i18next';
import {Modal, Grid, Button} from 'semantic-ui-react';
import GenericModal from './generic-modal';

import css from './generic-model.scss';

class ConfirmationModal extends PureComponent {
  render() {
    const {yes, no, onClose, onConfirm, message, title, open, t} = this.props;

    return (
      <GenericModal onClose={onClose} open={open !== false} size="tiny">
        <Modal.Header className={css.header}>{title}</Modal.Header>
        <Modal.Content className={css.content}>
          <Grid>
            <Grid.Row>
              <Grid.Column width={16}>
                <div className={css.message}>{message}</div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column width={8}>
                <Button fluid basic onClick={onClose}>
                  {no || t('no')}
                </Button>
              </Grid.Column>
              <Grid.Column width={8}>
                <Button fluid negative onClick={onConfirm}>
                  {yes || t('yes')}
                </Button>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Modal.Content>
      </GenericModal>
    );
  }
}

export default translate(['common'])(ConfirmationModal);

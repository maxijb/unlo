import React, {useState} from 'react';
import {translate} from 'react-i18next';
import {Modal, Button} from 'semantic-ui-react';
import GenericModal from './generic-modal';
import classnames from 'classnames';
import {multiLineTrans} from '@Common/utils/generic-utils';
import {SaleTag} from '../../../widget/components/display/svg-icons';
import Input from '@Components/inputs/wrapped-input';
import ErrorRenderer from '@Components/inputs/error-renderer';
import {Validators} from '@Common/utils/validations';

import css from './generic-model.scss';
import styles from '@Common/styles/styles.scss';

function ContactModal({onClose, isOpen, t, api}) {
  const [email, setEmail] = useState('');
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const send = async () => {
    const realEmail = (email || '').trim().toLowerCase();
    const error = Validators.validEmail('email', {email: realEmail});
    if (error) {
      return setError(error);
    } else {
      setLoading(true);
      await api.user.saveContact({email: realEmail});
      setLoading(false);
      onClose();
    }
  };
  return (
    <GenericModal onClose={onClose} open={isOpen} size="mini">
      <Modal.Header>
        <div className={classnames(styles.MediumBold, styles.centered)}>
          <SaleTag />
        </div>
      </Modal.Header>
      <Modal.Content>
        <div className={classnames(styles.MarginHalfBottom, styles.centered, styles.MediumBold)}>
          {t('signUpExtraDiscounts')}
        </div>
        <div
          className={classnames(
            styles.MarginBottom,
            styles.centered,
            styles.MediumBold,
            styles.ColorSubtext,
          )}
        >
          ({t('upTo70Off')})
        </div>
        <div className={classnames(styles.MarginBottom, styles.centered)}>
          <Input
            onChange={val => {
              setEmail(val);
              setError(null);
            }}
            placeholder={t('yourEmailAddress')}
            autoComplete="email"
            fluid
          />
          <ErrorRenderer error={error} className={styles.centered} />
        </div>
        <Button primary fluid className={classnames(styles.MarginBottom)} onClick={send}>
          {t('sendMeDeals')}
        </Button>
        <div className={styles.centered}>
          <a
            className={classnames(styles.MarginBottom, styles.centered, styles.SecondaryLink)}
            onClick={onClose}
          >
            {t('noImSavingEnough')}
          </a>
        </div>
      </Modal.Content>
    </GenericModal>
  );
}

export default translate(['home'])(ContactModal);

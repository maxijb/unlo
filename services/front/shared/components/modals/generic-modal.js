import React, {useEffect} from 'react';
import {Modal} from 'semantic-ui-react';

import {cdnURL} from '@Common/utils/statif-assets-utils';

import css from './generic-model.scss';

function preventBubbling(e) {
  e.stopPropagation();
}

export default function GenericModal(props) {
  const {children, isMobile, open, onClose, closeIcon, ...others} = props;

  const wrappedClose = () => {
    document.documentElement.style.overflowY = 'visible';
    onClose();
  };

  useEffect(() => {
    if (!open) {
      document.documentElement.style.overflowY = 'visible';
    } else {
      document.documentElement.style.overflowY = 'hidden';
    }
  }, [open]);

  useEffect(() => {
    return wrappedClose;
  }, []);

  return (
    <Modal
      {...others}
      open={open}
      onClose={wrappedClose}
      onClick={preventBubbling}
      onDoubleClick={preventBubbling}
      closeIcon={
        closeIcon || (
          <img
            src={cdnURL('/static/icons/dashboard/close.svg')}
            className={css.closeIcon}
            alt="Close"
          />
        )
      }
    >
      {children}
    </Modal>
  );
}

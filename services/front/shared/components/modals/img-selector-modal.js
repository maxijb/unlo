import React, {useState, useMemo, useEffect} from 'react';
import {translate} from 'react-i18next';
import {Modal, Grid, Button, Table} from 'semantic-ui-react';
import GenericModal from './generic-modal';
import isEmpty from 'is-empty';
import Pagination from '@Components/inputs/pagination';
import {DefaultPerPage} from '@Common/constants/app';

import OpenHoursInput from '../forms/configure-open-hours/open-hours-cell';
import {calculateTimeSlots, formatAllHoursAsString} from '@Common/utils/date-utils';
import AvatarInput from '../inputs/avatar-input';
import Input from '../inputs/wrapped-input';
import Image from '@Components/images/image';

import css from './generic-model.scss';

function preventBubbling(e) {
  e.stopPropagation();
}

function ImgSelectorModal({
  onClose,
  onSelect,
  images = [],
  api,
  actions,
  imageMap = {},
  imagesTotalCount,
}) {
  const [page, setPage] = useState(0);
  const [search, setSearch] = useState('');
  useEffect(() => {
    api.crud.imagesList({search, limit: 20, offset: page * 20});
  }, [search, page]);

  return (
    <GenericModal
      onClose={onClose}
      open={true}
      size="large"
      onClick={preventBubbling}
      onDoubleClick={preventBubbling}
    >
      <Modal.Header className={css.header}>Select image</Modal.Header>
      <Modal.Content className={css.content}>
        <div className={css.line}>
          <Input placeholder="Filter images" onChange={evt => setSearch(evt)} value={search} />
          <div>
            <AvatarInput
              recommendedHeight={800}
              recommendedWidth={800}
              onFileUploaded={(err, file) => {
                if (!err) {
                  actions.storeImageUploaded(file).then(() => {
                    onSelect(file.img_id);
                    onClose();
                  });
                }
              }}
            />
          </div>
        </div>
        <div>
          {images.map(img_id => {
            const image = imageMap[img_id];
            return (
              <div key={img_id} className={css.imgBlock} onClick={() => onSelect(img_id)}>
                <Image file={image?.name} size={100} />
                <div>
                  <div>{image.name}</div>
                  <div>Uploaded: {image.createdAt}</div>
                </div>
              </div>
            );
          })}
          <Pagination
            totalCount={imagesTotalCount}
            page={page}
            perPage={DefaultPerPage}
            moveToPage={num => {
              setPage(num);
            }}
          />
        </div>
      </Modal.Content>
    </GenericModal>
  );
}

export default translate(['dashboard'])(ImgSelectorModal);

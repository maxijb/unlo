import React, {useState, useMemo, useEffect} from 'react';
import {translate} from 'react-i18next';
import {Modal, Grid, Button, Table} from 'semantic-ui-react';
import GenericModal from './generic-modal';
import classnames from 'classnames';
import {multiLineTrans} from '@Common/utils/generic-utils';

import css from './generic-model.scss';
import styles from '@Common/styles/styles.scss';

function LowPricesModal({onClose, isOpen, t}) {
  return (
    <GenericModal onClose={onClose} open={isOpen} size="tiny">
      <Modal.Header>
        <div className={classnames(styles.MediumBold, styles.centered)}>
          {t('lowestPriceGuaranteed')}
        </div>
      </Modal.Header>
      <Modal.Content>
        <div className={classnames(styles.MarginBottom, styles.centered)}>
          {t('smallPricesSmall')}
        </div>
      </Modal.Content>
    </GenericModal>
  );
}

export default translate(['home'])(LowPricesModal);

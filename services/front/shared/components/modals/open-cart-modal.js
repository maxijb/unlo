import React, {useState, useMemo} from 'react';
import {translate} from 'react-i18next';
import {Modal, Grid, Button, Table} from 'semantic-ui-react';
import GenericModal from './generic-modal';
import classnames from 'classnames';
import Image from '@Components/images/image';
import Option from '@Components/product/option';
import Price from '@Components/product/price';
import CartTotals from '@Components/cart/cart-totals';
import TagSelector from '@Components/product/tag-selector';
import CartList from '@Components/cart/cart-list';

import {DefaultFastShippingRate, ShippingMethod} from '@Common/constants/app';

import css from './generic-model.scss';
import styles from '@Common/styles/styles.scss';

function preventBubbling(e) {
  e.stopPropagation();
}

function OpenCartModal({
  onClose,
  cart,
  subtotal,
  subtotalWithoutPromoCode,
  total,
  tax,
  api,
  t,
  items,
  promoCode,
  reference,
  shippingCharges,
}) {
  return (
    <GenericModal
      onClose={onClose}
      open={true}
      size="tiny"
      onClick={preventBubbling}
      onDoubleClick={preventBubbling}
      closeIcon={
        <a
          onClick={onClose}
          style={{
            position: 'absolute',
            right: 16,
            top: 16,
          }}
        >
          {t('back')}
        </a>
      }
    >
      <Modal.Header>
        <div className={classnames(styles.MediumBold)}>{t('yourOrder')}</div>
      </Modal.Header>
      <Modal.Content>
        <CartList
          items={items}
          reference={reference}
          api={api}
          cart={cart}
          shippingMethod={cart.shippingMethod}
        />
        <CartTotals
          subtotal={subtotal}
          subtotalWithoutPromoCode={subtotalWithoutPromoCode}
          shippingCharges={shippingCharges}
          includeTotal={false}
          tax={tax}
          cart={cart}
          nested={true}
          promoCode={promoCode}
        />
      </Modal.Content>
      <Modal.Actions>
        <div className={classnames(styles.FlexSpaceBetween, styles.MediumBold)}>
          <span>{t('total')}</span>
          <Price value={total} />
        </div>
      </Modal.Actions>
    </GenericModal>
  );
}

export default translate(['home'])(OpenCartModal);

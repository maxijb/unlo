import React, {useState, useMemo, useEffect} from 'react';
import {translate} from 'react-i18next';
import {Modal, Grid, Button, Table} from 'semantic-ui-react';
import GenericModal from './generic-modal';
import classnames from 'classnames';
import {multiLineTrans} from '@Common/utils/generic-utils';
import {CartErrorMessage} from '@Common/constants/errors';

import {Error} from '../../../widget/components/display/svg-icons';

import css from './generic-model.scss';
import styles from '@Common/styles/styles.scss';

function OrderErrorModal({onClose, isOpen, t, error}) {
  const isStockError = error?.error === CartErrorMessage.outOfStock;
  return (
    <GenericModal onClose={onClose} open={isOpen} size="tiny">
      <Modal.Header>
        <div className={classnames(styles.MarginHalfBottom, styles.centered)}>
          <Error />
        </div>
        <div className={classnames(styles.Large, styles.centered)}>
          {isStockError
            ? t('Oh no, one of your items is out of stock')
            : t('Oh no, your payment failed')}
        </div>
      </Modal.Header>
      <Modal.Content>
        {!isStockError ? (
          <>
            <div className={classnames(styles.MarginBottom, styles.centered)}>
              But hey, we can try this again! Please check that your payment and billing details are
              correct.
            </div>
            <div className={classnames(styles.MarginDoubleBottom, styles.centered)}>
              Or try a different payment method.
            </div>
          </>
        ) : (
          <div className={classnames(styles.MarginDoubleBottom, styles.centered)}>
            Please check your cart list to find the product with issues.
          </div>
        )}
        <div className={classnames(styles.centered, styles.MarginBottom)}>
          <Button primary onClick={onClose}>
            Let's try this again
          </Button>
        </div>
      </Modal.Content>
    </GenericModal>
  );
}

export default translate(['home'])(OrderErrorModal);

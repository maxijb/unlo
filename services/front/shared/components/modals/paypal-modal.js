import React, {useState, useMemo, useEffect} from 'react';
import {translate} from 'react-i18next';
import {Modal, Grid, Button, Table} from 'semantic-ui-react';
import GenericModal from './generic-modal';
import classnames from 'classnames';
import {multiLineTrans} from '@Common/utils/generic-utils';

import css from './generic-model.scss';
import styles from '@Common/styles/styles.scss';

function PaypalModal({onClose, isOpen, t}) {
  return (
    <GenericModal onClose={onClose} open={isOpen} size="tiny">
      <Modal.Header>
        <div className={classnames(styles.MediumBold, styles.centered)}>
          {t('payPalBuyerProtection')}
        </div>
      </Modal.Header>
      <Modal.Content>
        <div className={classnames(styles.MarginBottom, styles.centered)}>
          {t('GetWhatYouOrderedOrBack')}
        </div>
        <div className={classnames(styles.MarginBottom, styles.centered)}>
          {t('GetWhatYouOrderedOrBackLong')}
        </div>
      </Modal.Content>
    </GenericModal>
  );
}

export default translate(['home'])(PaypalModal);

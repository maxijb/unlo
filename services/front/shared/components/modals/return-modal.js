import React, {useState, useMemo} from 'react';
import {translate} from 'react-i18next';
import {Modal, Grid, Button} from 'semantic-ui-react';
import GenericModal from './generic-modal';
import classnames from 'classnames';

import Option from '@Components/product/option';
import Price from '@Components/product/price';
import TagSelector from '@Components/product/tag-selector';
import {ReturnReason} from '@Common/constants/orders';
import {Actors} from '@Common/constants/app';
import TextArea from '@Components/inputs/wrapped-textarea';

import OrderProduct from '@Components/order/order-product';
import {
  canBeReturned,
  canBeCancelled,
  countReturnsAndCancellations,
} from '@Common/utils/order-utils';

import css from './return-modal.scss';
import styles from '@Common/styles/styles.scss';

function preventBubbling(e) {
  e.stopPropagation();
}

function OpenCartModal({onClose, order, api, t}) {
  const [selected, setSelected] = useState(new Set());
  const [reason, setReason] = useState(ReturnReason.changeMind);
  const [explanation, setExplanation] = useState('');
  const [isSending, setIsSending] = useState(false);

  const startReturn = () => {
    setIsSending(true);
    api.cart
      .returnProducts({
        cart_id: Array.from(selected),
        reason,
        explanation,
      })
      .then(resp => {
        setIsSending(false);
      });
    onClose();
  };

  const reasons = useMemo(() => {
    return Object.keys(ReturnReason).map(r => ({
      content: (
        <div
          className={classnames(
            styles.Left,
            styles.MarginHalfTop,
            styles.MarginLeft,
            styles.MarginHalfBottom,
          )}
        >
          <div className={styles.Medium}>{t(`common:returnReason.${r}`)}</div>
          <div className={classnames(styles.ColorSubtext, styles.Tiny)}>
            {t(`common:returnReasonExplanation.${r}`)}
          </div>
        </div>
      ),
      id: r,
    }));
  }, []);

  const onToggle = id => {
    if (selected.has(id)) {
      selected.delete(id);
    } else {
      selected.add(id);
    }
    setSelected(new Set(selected));
  };

  const {canBeReturnedQuantity, canBeCancelledQuantity} = useMemo(() => {
    return countReturnsAndCancellations(order.products);
  }, [order.products]);

  let returnTextTag = 'itemReturn';
  if (canBeReturnedQuantity && canBeCancelledQuantity) {
    returnTextTag = 'itemCancelReturn';
  } else if (canBeCancelledQuantity > 0) {
    returnTextTag = 'itemCancel';
  }

  return (
    <GenericModal
      onClose={onClose}
      open={true}
      size="tiny"
      onClick={preventBubbling}
      onDoubleClick={preventBubbling}
    >
      <Modal.Header>
        <div className={classnames(styles.MediumBold)}>{t(returnTextTag)}</div>
      </Modal.Header>
      <Modal.Content>
        <div className={styles.MarginDoubleBottom}>
          <b>{t('itemsYouAreReturning')}: </b>
          <span>{t('xSelected', {number: selected.size})}</span>
        </div>
        {order.products.map(p => {
          if (!canBeReturned(p) && !canBeCancelled(p)) {
            return null;
          }

          return (
            <OrderProduct
              product={p}
              key={p.id}
              order={order}
              nested={false}
              onToggle={onToggle}
              selected={selected.has(p.id)}
              actor={Actors.buyer}
            />
          );
        })}

        <div className={classnames(styles.MediumBold, styles.MarginBottom, styles.MarginDoubleTop)}>
          {t('reasonForReturn')}
        </div>
        <TagSelector
          vertical
          tags={reasons}
          selectedTags={new Set([reason])}
          onChange={setReason}
          name="condition"
        />
        <TextArea
          className={css.textarea}
          rows={3}
          value={explanation}
          onChange={value => setExplanation(value)}
          placeholder={t('pleaseExplain')}
        />

        <Button
          loading={isSending}
          disabled={!selected?.size}
          primary
          fluid
          onClick={startReturn}
          className={styles.MarginDoubleTop}
        >
          {' '}
          {t('submitReturnRequest')}
        </Button>
        <div className={styles.Centered}>
          <div className={classnames(styles.ColorAccent, styles.MarginTop)}>
            {t('emailReturnShippingLabel')}
          </div>

          <div className={classnames(styles.ButtonMedium, styles.MarginTop)}>
            {t('eraseIphones')}
          </div>
          <div className={classnames(styles.Tiny, styles.ColorSubtext)}>
            {t('notEraseConsequence')}
          </div>
        </div>
      </Modal.Content>
    </GenericModal>
  );
}

export default translate(['home', 'common'])(OpenCartModal);

import React, {useState, useMemo, useEffect} from 'react';
import {translate} from 'react-i18next';
import {Modal, Grid, Button, Table} from 'semantic-ui-react';
import GenericModal from './generic-modal';
import classnames from 'classnames';
import {multiLineTrans} from '@Common/utils/generic-utils';

import css from './generic-model.scss';
import styles from '@Common/styles/styles.scss';

function FreeShippingModal({onClose, isOpen, seller, t}) {
  const name = seller?.name || t('professionalSeller');
  return (
    <GenericModal onClose={onClose} open={isOpen} size="tiny">
      <Modal.Header>
        <div className={classnames(styles.MediumBold, styles.centered, styles.ColorAccent)}>
          {name}
        </div>
      </Modal.Header>
      <Modal.Content>
        <div className={classnames(styles.Small, styles.MarginBottom, styles.centered)}>
          {multiLineTrans(seller?.description || t('professionalSellerLong', {name}))}
        </div>
      </Modal.Content>
    </GenericModal>
  );
}

export default translate(['home'])(FreeShippingModal);

import React, {PureComponent} from 'react';
import {get} from 'lodash';
import isEmpty from 'is-empty';
import {translate} from 'react-i18next';
import {Modal, Grid, Button, Form} from 'semantic-ui-react';
import GenericModal from './generic-modal';
import Input from '@Components/inputs/wrapped-input';
import ErrorRenderer from '@Components/inputs/error-renderer';
import Textarea from '@Components/inputs/wrapped-textarea';
import {cdnURL, UserFilesCdnURL} from '@Common/utils/statif-assets-utils';
import {ValidateForm} from '@Common/utils/validations';
import FormValidations from '@Common/constants/form-validations';

import {htmlEmailBody} from '@Common/../back/server/email/base-template';

import css from './generic-model.scss';
import scss from './send-email.scss';

class SendEmailModal extends PureComponent {
  constructor(props) {
    super(props);

    const emails = props.batchEmails || props.customer.email || [];

    this.state = {
      message: '',
      subject: props.subject || '',
      sendTo: emails.join(', '),
      sending: false,
      errors: {},
      sendingSuccesful: false
    };
  }

  onSetField = (value, name) => {
    this.setState({[name]: value, sendingSuccesful: false});
  };

  sendEmail = () => {
    const {app, t} = this.props;
    const {message, subject, sendTo} = this.state;
    const input = {message, subject, sendTo};
    const foundErrors = ValidateForm(input, FormValidations.sendEmail);

    if (isEmpty(foundErrors)) {
      this.setState({sending: true, sendingSuccesful: false});
      this.props.sendEmail({appId: app.id, ...input}).then(({resp}) => {
        const errors = !resp.error ? {} : resp.errorData || {form: {...resp}};
        this.setState({
          sending: false,
          errors,
          sendingSuccesful: resp.error ? false : t('emailSentSuccesfully')
        });
      });
    } else {
      this.setState({errors: foundErrors, sendingSuccesful: false});
    }
  };

  renderPreview() {
    const {app, customer, user, t, serverConfig, appConfig} = this.props;
    const {message} = this.state;

    const query = {
      app,
      userName: (customer && customer.name) || null,
      business: get(appConfig, 'options.business', {}),
      message,
      operator: user
    };

    const html = htmlEmailBody(query, {t, config: serverConfig.email}, false, true);

    return <div className={scss.emailPreview} dangerouslySetInnerHTML={{__html: html}} />;
  }

  render() {
    const {t, onClose, isMobile} = this.props;
    const {sendTo, subject, message, sending, errors, sendingSuccesful} = this.state;

    return (
      <GenericModal onClose={onClose} open={open !== false} size="large" isMobile={isMobile}>
        {!isMobile && <Modal.Header>{t('sendEmail')}</Modal.Header>}
        <Modal.Content>
          <Grid>
            <Grid.Column computer={8} mobile={16}>
              <Form.Field>
                <Input
                  fluid
                  label={t('sendTo')}
                  value={sendTo}
                  name="sendTo"
                  onChange={this.onSetField}
                  disabled={sending}
                  error={errors.sendTo}
                />
              </Form.Field>
              <Form.Field>
                <Input
                  fluid
                  label={t('subject')}
                  value={subject}
                  name="subject"
                  onChange={this.onSetField}
                  disabled={sending}
                  error={errors.subject}
                />
              </Form.Field>
              <Form.Field>
                <Textarea
                  fluid
                  label={t('message')}
                  value={message}
                  name="message"
                  onChange={this.onSetField}
                  disabled={sending}
                  rows={20}
                  error={errors.message}
                />
              </Form.Field>
              <Form.Field>
                <Button fluid primary loading={sending} disabled={sending} onClick={this.sendEmail}>
                  {t('sendEmail')}
                </Button>
                <ErrorRenderer
                  error={errors.form || sendingSuccesful}
                  positive={sendingSuccesful}
                />
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={8} className={scss.preview} only="computer">
              {this.renderPreview()}
            </Grid.Column>
          </Grid>
        </Modal.Content>
      </GenericModal>
    );
  }
}

export default translate(['dashboard'])(SendEmailModal);

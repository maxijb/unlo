import React, {Component} from 'react';
import {translate} from 'react-i18next';
import {Modal, Grid, Button, Table} from 'semantic-ui-react';
import GenericModal from './generic-modal';
import isEmpty from 'is-empty';

import OpenHoursInput from '../forms/configure-open-hours/open-hours-cell';
import {calculateTimeSlots, formatAllHoursAsString} from '@Common/utils/date-utils';
import css from './generic-model.scss';

class SpecialHoursModal extends Component {
  constructor(props) {
    super(props);
    const slots = calculateTimeSlots(undefined, {bookings: {duration: 0}}).map(it => ({
      value: it,
      key: it,
      text: it
    }));

    const wasDayClosed = isEmpty(props.initialHours) || !Array.isArray(props.initialHours);
    const today = wasDayClosed ? ['7:00', '24:00'] : [...props.initialHours];

    this.state = {
      wasDayClosed,
      today,
      slots
    };
  }

  onChangeTime = (day, num, value) => {
    const {today} = this.state;
    today[num] = value;
    this.setState({today});
  };

  onUnsplit = day => {
    const {today} = this.state;
    this.setState({today: today.slice(0, 2)});
  };

  onSplit = day => {
    const {today} = this.state;
    today[2] = today[1] || '7:00';
    today[3] = '24:00';
    this.setState({today});
  };

  onConfirm = e => {
    const {today} = this.state;
    this.props.onSelect(today, e);
    this.props.onClose();
  };

  preventBubbling = e => {
    e.stopPropagation();
  };

  render() {
    const {onClose, onSelect, open, t} = this.props;
    const {today, slots, wasDayClosed} = this.state;
    const isSplit = today.length > 2;
    return (
      <GenericModal
        onClose={onClose}
        open={true}
        size="tiny"
        onClick={this.preventBubbling}
        onDoubleClick={this.preventBubbling}
      >
        <Modal.Header className={css.header}>
          {wasDayClosed && <span>{t('openDay')} - </span>}
          {t('setSpecialHours')}
        </Modal.Header>
        <Modal.Content className={css.content}>
          <Table>
            <Table.Body>
              <Table.Row key={1}>
                <OpenHoursInput
                  index={0}
                  key={0}
                  num={0}
                  value={today[0]}
                  t={t}
                  label={'hoursSettings.from'}
                  slots={slots}
                  onChangeTime={this.onChangeTime}
                />
                <OpenHoursInput
                  index={0}
                  key={1}
                  num={1}
                  value={today[1]}
                  t={t}
                  label={'hoursSettings.to'}
                  slots={slots}
                  onChangeTime={this.onChangeTime}
                />
                <Table.Cell key="split">
                  {!isSplit && (
                    <Button basic onClick={this.onSplit}>
                      {t('hoursSettings.splitServiceShort')}
                    </Button>
                  )}
                </Table.Cell>
              </Table.Row>
              {isSplit && (
                <Table.Row key={2}>
                  <OpenHoursInput
                    index={0}
                    key={2}
                    num={2}
                    value={today[2]}
                    t={t}
                    label={'hoursSettings.from'}
                    slots={slots}
                    onChangeTime={this.onChangeTime}
                  />
                  <OpenHoursInput
                    index={0}
                    key={3}
                    num={3}
                    value={today[3]}
                    t={t}
                    label={'hoursSettings.to'}
                    slots={slots}
                    onChangeTime={this.onChangeTime}
                  />
                  <Table.Cell key="split">
                    {
                      <Button basic onClick={this.onUnsplit}>
                        {t('hoursSettings.unsplitServiceShort')}
                      </Button>
                    }
                  </Table.Cell>
                </Table.Row>
              )}
            </Table.Body>
          </Table>
          <Button fluid primary onClick={this.onConfirm}>
            {t('saveSpecialHours')}
          </Button>
        </Modal.Content>
      </GenericModal>
    );
  }
}

export default translate(['dashboard'])(SpecialHoursModal);

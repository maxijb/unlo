import React, {useState, useMemo, useEffect} from 'react';
import {translate} from 'react-i18next';
import {Modal, Grid, Button, Table} from 'semantic-ui-react';
import GenericModal from './generic-modal';
import classnames from 'classnames';
import {multiLineTrans} from '@Common/utils/generic-utils';

import css from './generic-model.scss';
import styles from '@Common/styles/styles.scss';

function UnbundlingModal({onClose, isOpen, t}) {
  return (
    <GenericModal onClose={onClose} open={isOpen} size="tiny">
      <Modal.Header>
        <div className={classnames(styles.MarginHalfBottom, styles.centered)}>
          <img src="/static/incredible/planet.png" style={{height: 70}} alt="Sustainable planet" />
        </div>
        <div className={classnames(styles.MediumBold, styles.centered)}>{t('cleanerPlanet')}</div>
      </Modal.Header>
      <Modal.Content>
        <div
          className={classnames(
            styles.Small,
            styles.MarginBottom,
            styles.centered,
            styles.ColorSubtext,
          )}
        >
          {multiLineTrans(t('cleanerPlanetLong'))}
        </div>
      </Modal.Content>
    </GenericModal>
  );
}

export default translate(['home'])(UnbundlingModal);

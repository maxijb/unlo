import React, {useState, useMemo, useEffect} from 'react';
import {translate} from 'react-i18next';
import {Modal, Grid, Button, Table} from 'semantic-ui-react';
import GenericModal from './generic-modal';
import classnames from 'classnames';
import {multiLineTrans} from '@Common/utils/generic-utils';

import css from './generic-model.scss';
import styles from '@Common/styles/styles.scss';

function WarrantyModal({
  onClose,
  isOpen,
  t,
}) {

  return (
    <GenericModal
      onClose={onClose}
      open={isOpen}
      size="tiny"
    >
      <Modal.Header>
        <div className={classnames(styles.MediumBold, styles.centered, styles.MarginBottom)}>
          {t('YouAreCovered')}
        </div>
        <div className={classnames(styles.Small, styles.ColorSubtext, styles.centered)}>
          {t('Every_device_is_backed')}
        </div>
      </Modal.Header>
      <Modal.Content>
        <div className={styles.sectionWithBackground}>
          {[0, 1, 2].map(i =>
            <li key={i} className={styles.ListItem}>
              {t(`warrantyItems.${i}`)}
            </li>
          )}
        </div>
        <div className={classnames(styles.Tiny, styles.MarginBottom, styles.centered, styles.ColorSubtext)}>
          {multiLineTrans(t('warrantyLong'))}
        </div>
      </Modal.Content>
    </GenericModal>
  );
}

export default translate(['home'])(WarrantyModal);

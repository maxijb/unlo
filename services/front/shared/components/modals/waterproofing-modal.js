import React, {useState} from 'react';
import {translate} from 'react-i18next';
import {Modal, Grid, Button, Table} from 'semantic-ui-react';
import GenericModal from './generic-modal';
import TagLink from '../display/tag-link';
import classnames from 'classnames';
import {multiLineTrans} from '@Common/utils/generic-utils';

import css from './generic-model.scss';
import styles from '@Common/styles/styles.scss';

function WaterproofingModal({onClose, isOpen, t}) {
  const [version, setVersion] = useState(67);

  return (
    <GenericModal onClose={onClose} open={isOpen} size="tiny">
      <Modal.Header>
        <div className={classnames(styles.MediumBold, styles.centered)}>{t('waterproofing')}</div>
      </Modal.Header>
      <Modal.Content>
        <div
          className={classnames(
            styles.Small,
            styles.MarginBottom,
            styles.centered,
            styles.ColorSubtext,
          )}
        >
          {multiLineTrans(t('waterproofingLong'))}
        </div>
        <div className={classnames(styles.MarginDoubleTop)}>
          <TagLink
            text="IP 67 Waterproofing"
            selected={version === 67}
            onClick={() => setVersion(67)}
          />
          <TagLink
            text="IP 68 Waterproofing"
            selected={version === 68}
            onClick={() => setVersion(68)}
          />
        </div>
        <div className={styles.sectionWithBackground} style={{marginBottom: 0}}>
          {version === 67 && (
            <ul>
              <li className={styles.ListItem}>
                <b>Waterproof for 30 minutes up to maximum depth of 1 meter</b>
              </li>
              <li className={styles.ListItem}>
                iPhone SE (2nd generation), iPhone XR, iPhone X, iPhone 8, iPhone 8 Plus, iPhone 7,
                and iPhone 7 Plus have a rating of IP67 under IEC standard 60529.
              </li>
            </ul>
          )}
          {version === 68 && (
            <ul>
              <li className={styles.ListItem}>
                <b>Waterproof for 30 minutes up to maximum depth of 2, 4 or 6 meters</b>
              </li>
              <li className={styles.ListItem}>
                iPhone XS, iPhone XS Max and iPhone 11 have a rating of IP68 under IEC standard
                60529 (maximum depth of 2 meters up to 30 minutes).
              </li>
              <li className={styles.ListItem}>
                iPhone 11 Pro and iPhone 11 Pro Max have a rating of IP68 under IEC standard 60529
                (maximum depth of 4 meters up to 30 minutes).
              </li>
              <li className={styles.ListItem}>
                iPhone 12, iPhone 12 Pro and iPhone 12 Pro Max have a rating of IP68 under IEC
                standard 60529 (maximum depth of 6 meters up to 30 minutes).
              </li>
            </ul>
          )}
        </div>
      </Modal.Content>
    </GenericModal>
  );
}

export default translate(['home'])(WaterproofingModal);

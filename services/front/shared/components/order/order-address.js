import React from 'react';
import {translate} from 'react-i18next';

import classnames from 'classnames';

import Address from '../cart/address';

import styles from '@Common/styles/styles.scss';
import css from './order.scss';

function OrderAddress({order, t, address = null, vertical = false}) {
  return (
    <div className={classnames(styles.sectionWithBackground, styles.MarginHalfBottom)}>
      {vertical && (
        <div className={classnames(styles.ColorDark, styles.MarginBottom, styles.FlexNoShrink)}>
          {t('shipsTo')}
        </div>
      )}
      <div className={styles.Flex}>
        {!vertical && (
          <div className={classnames(styles.ColorDark, styles.MarginRight, styles.FlexNoShrink)}>
            {t('shipsTo')}
          </div>
        )}
        <div className={styles.ColorSubtext}>
          <div>
            {order.firstName} {order.lastName}
          </div>
          <div>
            <Address {...(address || {})} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default translate(['home', 'common'])(OrderAddress);

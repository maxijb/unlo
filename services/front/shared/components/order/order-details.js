import React, {useMemo} from 'react';
import {translate} from 'react-i18next';
import Price from '../product/price';
import OrderProduct from './order-product';
import {DefaultCurrency, Actors} from '@Common/constants/app';
import {getPriceFn} from '@Common/utils/order-utils';
import classnames from 'classnames';
import OrderTotals from './order-totals';

import OrderAddress from './order-address';

import styles from '@Common/styles/styles.scss';
import css from './order.scss';

function OrderDetails({order, t, products = [], address = null, actor = Actors.buyer}) {
  const getPrice = getPriceFn(actor);

  const visibleProducts = useMemo(() => {
    return (order.products || products).sort((a, b) => {
      if (a.seller_id !== b.seller_id) {
        return a.seller_id - b.seller_id;
      }
      if (a.type === 'shipping') {
        return 1;
      }
      if (b.type === 'shipping') {
        return -1;
      }
      return getPrice(b) - getPrice(a);
    });
  }, [order.products, products]);

  return (
    <>
      <>
        <div className={classnames(styles.FlexSpaceBetween, styles.MarginBottom)}>
          <div className={styles.Button}>
            {t('orderNumberX')} {order.id}
          </div>

          <div>{new Intl.DateTimeFormat('en-US').format(new Date(order.createdAt))}</div>
        </div>
        <OrderAddress order={order} address={address} />
        <div className={classnames(styles.MarginBottom, styles.MarginTop)}>
          {visibleProducts.map(p => {
            if (p.type === 'product') {
              return <OrderProduct product={p} key={p.id} actor={actor} />;
            }
            if (p.type === 'shipping') {
              return (
                <div className={classnames(styles.FlexSpaceBetween, styles.MarginBottom)}>
                  {p.description}
                  <b>
                    <Price value={getPrice(p)} />
                  </b>
                </div>
              );
            }
          })}
        </div>
      </>

      <OrderTotals order={order} products={order.products || products} />
    </>
  );
}

export default translate(['home', 'common'])(OrderDetails);

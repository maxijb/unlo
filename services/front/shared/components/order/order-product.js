import React, {useMemo} from 'react';
import {translate} from 'react-i18next';

import Price from '../product/price';
import {DefaultCurrency, TrackingLinks} from '@Common/constants/app';
import {CartStatusColor, CartStatus, Actors} from '@Common/constants/app';
import Image from '../images/image';
import Tag from '../product/tag';
import Checkbox from '@Components/inputs/wrapped-checkbox';
import Link from 'next/link';
import {slugify, getPageLink} from '@Common/utils/urls';
import classnames from 'classnames';
import {noop} from '@Common/utils/generic-utils';
import {getRowValue} from '@Common/utils/order-utils';
import {getProductId} from '../../utils/cart-utils';

import css from './order-product.scss';
import styles from '@Common/styles/styles.scss';
import crudcss from '../crud/crud.scss';

function OrderProduct({
  product,
  t,
  order,
  returnOrder,
  nested = false,
  inline = false,
  className,
  onToggle,
  selected,
  shipping,
  seller,
  inAdmin,
  showAppearance,
  showStatus,
  crud,
  actor = Actors.seller,
  superAdmin = false,
}) {
  const appearance = useMemo(() => {
    return product?.attributes?.filter(a => a.type === 'appearance')?.[0]?.name || null;
  }, [product]);

  const differentCarrier = useMemo(() => {
    if (
      !inAdmin ||
      !product?.special_item?.overrideAttributes?.carrier ||
      !product.attributes?.length ||
      !crud?.attributes
    ) {
      return null;
    }

    const carrierId = product?.special_item?.overrideAttributes?.carrier;

    if (
      product.attributes.find(a => a.attr_id === carrierId) ||
      !crud.attributes?.[carrierId]?.name
    ) {
      return null;
    }

    return (
      <span className={styles.MarginHalfLeft}>
        {t('common:userSelectedName', {name: crud.attributes?.[carrierId]?.name})}
      </span>
    );
  }, [inAdmin, product, crud?.attributes]);

  return (
    <div
      className={classnames(css.orderProduct, className, {
        [css.nested]: nested,
        [css.inline]: inline,
      })}
      onClick={() => (onToggle || noop)(product.id)}
    >
      <div className={css.body}>
        <div className={classnames(styles.Flex, styles.FlexTop, styles.W100)}>
          {onToggle && <Checkbox checked={selected} className={css.checkbox} />}
          <div className={css.imageHolder}>
            <Image file={product.image} size={80} />
          </div>
          <div className={styles.FlexGrow}>
            <Link
              href={`/product?id=${product.category_id}-${
                getProductId(product, actor) || product.id
              }`}
              as={getPageLink('variant', {
                product: {
                  name: !inAdmin
                    ? product.description
                    : product.description_seller || product.description,
                  id: getProductId(product, actor) || product.id,
                  category_id: product.category_id,
                },
                t,
              })}
            >
              <span>
                {product.quantity > 1 && <b>{product.quantity} x </b>}
                <a>
                  {!inAdmin
                    ? product.description
                    : product.description_seller || product.description}
                </a>
                {differentCarrier}
              </span>
            </Link>
            {inAdmin && product.status && product.status > CartStatus.inOrder && (
              <div className={styles.MarginTop}>
                <Tag text={t(`common:cartStatus.${product.status}`)} />
                {returnOrder && (
                  <span className={styles.MarginLeft}>
                    <b>{t(`common:returnReason.${returnOrder.reason}`)}</b>
                    {returnOrder.explanation && ` | ${returnOrder.explanation}`}
                  </span>
                )}
              </div>
            )}
            {order && !nested && (
              <div className={classnames(styles.Tiny, styles.MarginHalfTop, styles.ColorSubtext)}>
                {t('orderNumberX', {number: order.id})}
              </div>
            )}
            {inAdmin && product.source_id && (
              <div
                className={classnames(
                  styles.FlexSpaceBetween,
                  styles.MarginHalfBottom,
                  styles.MarginTop,
                )}
              >
                <div>{t(`common:SKU`)}</div>
                <div className={styles.Button}>{product.source_id}</div>
              </div>
            )}
            {showAppearance && appearance && (
              <div
                className={classnames(
                  styles.FlexSpaceBetween,
                  styles.MarginHalfBottom,
                  styles.MarginTop,
                )}
              >
                <div>{t(`common:Appearance`)}</div>
                <div className={styles.Button}>{appearance}</div>
              </div>
            )}
            {showStatus && product.tracking && (
              <div className={styles.MarginTop}>
                <span>{t('trackingNumber')}: </span>
                <a
                  href={TrackingLinks[product?.tracking_carrier]?.replace(
                    '{{number}}',
                    product.tracking,
                  )}
                  target="_blank"
                  rel="noopener noreferer"
                >
                  <b>
                    {product.tracking_carrier && t(`common:${product.tracking_carrier}`)}{' '}
                    {product.tracking}
                  </b>
                </a>
              </div>
            )}
            {inline && (
              <div
                className={classnames(
                  styles.FlexSpaceBetween,
                  styles.MarginHalfBottom,
                  styles.MarginTop,
                )}
              >
                <div>{t(`common:price`)}</div>
                <div className={styles.Button}>
                  <Price currency={DefaultCurrency} value={getRowValue(product, actor)} />
                  {superAdmin &&
                    actor === Actors.seller &&
                    product.display_price &&
                    product.display_price !== product.price && (
                      <span className={classnames(styles.ColorSubtext)}>
                        {' - '}Sold for{' '}
                        <Price
                          currency={DefaultCurrency}
                          value={getRowValue(product, Actors.buyer)}
                        />
                      </span>
                    )}
                </div>
              </div>
            )}
            {inline && seller && (
              <div
                className={classnames(
                  styles.FlexSpaceBetween,
                  styles.MarginHalfBottom,
                  styles.Capitalize,
                )}
              >
                <div>{t(`common:seller`)}</div>
                <div className={styles.Button}>
                  <b>{seller.name}</b> ({seller.id})
                </div>
              </div>
            )}
            {shipping && (
              <div className={classnames(styles.FlexSpaceBetween, styles.MarginBottom)}>
                <div>{shipping.description}</div>
                <div className={styles.Button}>
                  <Price currency={DefaultCurrency} value={shipping.price} />
                </div>
              </div>
            )}
          </div>
        </div>
        {!inline && (
          <div className={styles.MarginLeft}>
            <Price
              currency={DefaultCurrency}
              value={getRowValue(product, actor)}
              className={styles.Button}
            />
          </div>
        )}
      </div>
    </div>
  );
}

export default translate(['home', 'common'])(OrderProduct);

import React, {useMemo} from 'react';
import {translate} from 'react-i18next';
import Price from '../product/price';

import {DefaultCurrency} from '@Common/constants/app';
import classnames from 'classnames';

import styles from '@Common/styles/styles.scss';
import css from './order.scss';

function OrderTotals({order, t, products = [], displayTotals, superAdmin}) {
  const {shipping, promoCode} = useMemo(() => {
    const shipping = products
      .filter(p => p.type === 'shipping')
      .reduce((agg, it) => {
        return agg + it.price;
      }, 0);

    const promoCode = products.find(p => p.type === 'code');
    return {shipping, promoCode};
  }, [products]);

  console.log(displayTotals?.subtotal, promoCode?.price, order.subtotal);

  return (
    <>
      <div className={classnames(styles.FlexSpaceBetween, styles.MarginBottom)}>
        <div>{t('subtotal')}</div>
        <div className={styles.Button}>
          <Price currency={DefaultCurrency} value={order.subtotal} />
          {superAdmin &&
            displayTotals?.total &&
            displayTotals?.subtotal !== order.subtotal &&
            displayTotals?.subtotal - promoCode?.price !== order.subtotal && (
              <span className={styles.ColorSubtext}>
                {' '}
                - Listed as <Price value={displayTotals?.subtotal} />
              </span>
            )}
        </div>
      </div>
      {shipping > 0 && (
        <div className={classnames(styles.FlexSpaceBetween, styles.MarginBottom)}>
          <div>{t(`totalShippingCost`)}</div>
          <div className={styles.Button}>
            <Price currency={DefaultCurrency} value={shipping} />
          </div>
        </div>
      )}
      {promoCode && (
        <div className={classnames(styles.FlexSpaceBetween, styles.MarginBottom, styles.FlexTop)}>
          <div>
            <div>
              {t('common:promoCode')}:{' '}
              <b className={styles.Emphasis}>
                {promoCode.special_item?.name || t('common:discount')}
              </b>
            </div>
            <div className={classnames(styles.Small, styles.ColorSubtext)}>
              {promoCode.description}
            </div>
          </div>
          <Price value={promoCode.price} className={styles.Emphasis} />
        </div>
      )}
      <div className={classnames(styles.FlexSpaceBetween, styles.MarginBottom)}>
        <div>{t('taxes')}</div>
        <div className={styles.Button}>
          <Price currency={DefaultCurrency} value={order.tax} />
          {superAdmin && displayTotals?.tax && displayTotals?.tax !== order.tax && (
            <span className={styles.ColorSubtext}>
              {' '}
              (<Price value={displayTotals?.tax} />)
            </span>
          )}
        </div>
      </div>
      {order.payment_description && (
        <div className={classnames(styles.FlexSpaceBetween, styles.MarginBottom)}>
          <div>{t('paymentMethod')}</div>
          <div className={styles.Button}>{order.payment_description}</div>
        </div>
      )}
      <div className={classnames(styles.FlexSpaceBetween, styles.MarginBottom)}>
        <div className={styles.Button}>{t('total')}</div>
        <div className={styles.Button}>
          <Price currency={DefaultCurrency} value={order.total} />
          {superAdmin && displayTotals?.total && displayTotals?.total !== order.total && (
            <span className={styles.ColorSubtext}>
              {' '}
              - Sold for <Price value={displayTotals?.total} />
            </span>
          )}
        </div>
      </div>
    </>
  );
}

export default translate(['home', 'common'])(OrderTotals);

import React, {useState, useMemo} from 'react';
import classnames from 'classnames';
import {translate} from 'react-i18next';
import Price from '../product/price';
import OrderProduct from './order-product';
import OrderStatus from '../cart/order-status';
import TradeInOrderStatus from '../cart/trade-in-order-status';

import OrderTotals from './order-totals';
import OrderAddress from './order-address';
import TagLink from '../display/tag-link';
import {DefaultCurrency, OrderType, Actors} from '@Common/constants/app';
import {formatDate} from '@Common/utils/date-utils';
import {
  canBeReturned,
  canBeCancelled,
  countReturnsAndCancellations,
} from '@Common/utils/order-utils';

import styles from '@Common/styles/styles.scss';
import css from './order.scss';

function Order(props) {
  const {
    order,
    t,
    products = [],
    address = null,
    onStartReturn,
    showStatus = true,
    actor = Actors.buyer,
  } = props;
  const [extended, setExtended] = useState(false);
  const isTradeIn = order.type === OrderType.tradeIn;

  if (isTradeIn) {
    return <TradeInOrder {...props} />;
  }

  const {canBeReturnedQuantity, canBeCancelledQuantity} = useMemo(() => {
    return countReturnsAndCancellations(order.products || products);
  }, [order.products, products]);

  let returnTextTag = 'startReturn';
  if (canBeReturnedQuantity && canBeCancelledQuantity) {
    returnTextTag = 'startReturnCancellation';
  } else if (canBeCancelledQuantity > 0) {
    returnTextTag = 'startCancellation';
  }

  return (
    <div className={css.order}>
      <div>
        <div class={css.header}>
          <div className={styles.ButtonMedium}>{t('orderNumberX', {number: order.id})}</div>
          <div className={classnames(css.date)}>
            {t('placedOnDate', {date: formatDate(order.createdAt)})}
          </div>
        </div>
        <div className={css.content}>
          <div className={css.products}>
            {(order.products || products).map(p => {
              if (p.type !== 'product') {
                return null;
              }
              return (
                <>
                  <OrderProduct
                    product={p}
                    key={p.id}
                    order={order}
                    nested={true}
                    className={styles.MarginDoubleBottom}
                    showStatus={showStatus}
                    actor={actor}
                  />
                  {showStatus && (
                    <div className={classnames(styles.MarginDoubleBottom, styles.MarginTop)}>
                      <OrderStatus order={p} className={css.MarginHalfTop} />
                    </div>
                  )}
                </>
              );
            })}
          </div>
          <div className={css.actions}>
            {(canBeReturnedQuantity > 0 || canBeCancelledQuantity > 0) && (
              <TagLink text={t(returnTextTag)} onClick={() => onStartReturn(order)} />
            )}
            <TagLink
              text={t('viewOrderDetails')}
              onClick={() => setExtended(!extended)}
              selected={extended}
            />
            {extended && (
              <>
                <OrderTotals order={order} products={order.products || products} />
                <OrderAddress order={order} address={address} />
              </>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export function TradeInOrder({order, t, products = [], address = null, showStatus = true}) {
  const [extended, setExtended] = useState(false);
  const isTradeIn = order.type === OrderType.tradeIn;
  console.log(order, products);
  return (
    <div className={css.order}>
      <div>
        {(order.products || products).map(p => {
          return (
            <div
              className={classnames(
                styles.MarginDoubleBottom,
                styles.FlexSpaceBetween,
                styles.FlexTop,
              )}
            >
              <div>
                <div className={styles.Button}>
                  {t('tradeInOrder')} {order.id}
                </div>
                <div>
                  {p.selections?.model} {p.selections?.capacity} {p.selections?.carrier} (
                  {p.selections?.condition} condition)
                </div>
              </div>
              <div className={classnames(styles.Button, styles.FlexNoShrink)}>
                {t('Earn')} <Price value={p.price} />
              </div>
            </div>
          );
        })}

        {showStatus && (
          <div className={classnames(styles.MarginBottom, styles.MarginTop, css.content)}>
            <div className={css.products}>
              <TradeInOrderStatus
                order={order.products || products}
                className={css.MarginHalfTop}
              />
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default translate(['home', 'common'])(Order);

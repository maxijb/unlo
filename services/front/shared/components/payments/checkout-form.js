import React from 'react';
import Link from 'next/link';
import {Loader, Segment} from 'semantic-ui-react';

import {getPageLink} from '@Common/utils/urls';
import Logger from '@Common/logger';
import ReactStripeCheckout from './react-stripe-checkout';
import ErrorRenderer from '../inputs/error-renderer';

class CheckoutForm extends React.Component {
  static defaultProps = {
    user: {}
  };

  onSubmit(token) {
    const {amount, performPayment} = this.props;
    console.log(token);
    performPayment({token: token.id, amount});
  }

  render() {
    const {
      lang,
      user,
      user: {payments},
      amount
    } = this.props;

    console.log('render', payments);

    return (
      <div>
        {!payments.success && !payments.loading && (
          <ReactStripeCheckout
            token={this.onSubmit.bind(this)}
            stripeKey="pk_test_7U7nsCvNGAxFHqxxqS9Fuylz"
            name="C99"
            description="Widget"
            amount={amount}
            locale={lang || 'auto'}
            email={user.email || ''}
            panelLabel="Sign up"
          >
            <a>PAY HERE PLEASE</a>
          </ReactStripeCheckout>
        )}
        {payments.loading && <Loader size="large" active inline="centered" />}
        <ErrorRenderer error={payments.error} />
        {payments.success && <div>successful payment</div>}
      </div>
    );
  }
}

export default CheckoutForm;

import React, {useState, useMemo} from 'react';
import {translate} from 'react-i18next';
import {withAppConsumer} from '@Components/app-context';

import {Grid, Icon} from 'semantic-ui-react';
import {Button, Transition} from 'semantic-ui-react';
import Image from '@Components/images/image';
import Dropdown from '@Components/inputs/wrapped-dropdown';

import Price from './price';
import classnames from 'classnames';

import styles from '@Common/styles/styles.scss';
import css from './accessory-product.scss';

function AccessoryProduct({product, t, api, cart, context: {GA}}) {
  const [item, setItem] = useState(0);
  const [isAdding, setIsAdding] = useState(false);
  const variant = product.variants?.[item];
  const options = useMemo(() => {
    return product.variants?.map((c, i) => ({value: i, text: c.name}));
  }, [product.variants]);

  if (!product.variants?.length) {
    return null;
  }

  const onBuy = async () => {
    setIsAdding(true);
    GA.event({
      category: 'cart',
      action: 'add-to-cart',
      label: 'associate-product',
      value: variant.product_id,
    });
    await api.cart.addForUser({
      quantity: 1,
      variant_id: variant.product_id,
      currentAttributes: variant.attr_id ? [variant.attr_id] : [],
    });
    setIsAdding(false);
  };

  const cartAddedId = useMemo(() => {
    return cart.items.find(it => it.variant_id === variant.product_id)?.cart_id;
  }, [cart.items, variant]);

  const onRemove = async () => {
    setIsAdding(true);
    GA.event({
      category: 'cart',
      action: 'remove-from-cart',
      label: 'associate-product',
      value: variant.product_id,
    });
    await api.cart.removeItem({
      cart_id: cartAddedId,
    });
    setIsAdding(false);
  };

  return (
    <div className={css.accessoryProduct}>
      <div className={css.head}>
        <Image size={80} file={variant.image} withFrame={true} className={css.img} />
        <div>
          <div className={css.name}>{product.name}</div>

          <Price value={variant.price} className={css.price} />
        </div>
      </div>
      <div className={css.body}>
        <div className={css.column}>
          {/*(product.variants?.length || 0) > 1 && (
            <Dropdown fluid value={item} options={options} onChange={setItem} />
          )*/}
        </div>
        <div className={css.column}>
          <Button fluid primary onClick={cartAddedId ? onRemove : onBuy} loading={isAdding}>
            {cartAddedId && <Icon name="check" color="white" />}
            {t(cartAddedId ? 'added' : 'add')}
          </Button>
        </div>
      </div>
    </div>
  );
}

export default withAppConsumer(translate(['home', 'common'])(AccessoryProduct));

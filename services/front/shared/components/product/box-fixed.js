import React, {useState, useMemo} from 'react';
import {translate} from 'react-i18next';

import {Grid} from 'semantic-ui-react';
import {Button, Transition} from 'semantic-ui-react';
import Router from 'next/router';
import Price from './price';
import classnames from 'classnames';
import {useSwipeable} from 'react-swipeable';

import styles from '@Common/styles/styles.scss';
import css from './buy-fixed.scss';

function BoxFixed(props) {
  const {children} = props;

  return (
    <>
      <div className={classnames(css.box, css.fixedBox)}>{children}</div>
    </>
  );
}

export default translate(['home', 'common'])(BoxFixed);

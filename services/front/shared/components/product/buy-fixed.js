import React, {useState, useMemo, useEffect} from 'react';
import {translate} from 'react-i18next';

import {Grid, Loader} from 'semantic-ui-react';
import {Button, Transition} from 'semantic-ui-react';
import Router from 'next/router';
import Price from './price';
import classnames from 'classnames';
import {useSwipeable} from 'react-swipeable';
import AccessoryProduct from './accessory-product';
import {LockClosed} from '../../../widget/components/display/svg-icons';
import {withAppConsumer} from '../app-context';

import styles from '@Common/styles/styles.scss';
import css from './buy-fixed.scss';

function BuyFixed(props) {
  const {
    variant,
    onBuy,
    t,
    isBuying,
    accessoryProducts,
    loadingAccessoryProducts,
    api,
    cart,
    isExtended,
    setIsExtended,
    context: {GA},
  } = props;

  useEffect(() => {
    if (isExtended) {
      document.documentElement.style.overflowY = 'hidden';
    } else {
      document.documentElement.style.overflowY = 'auto';
    }
  }, [isExtended]);

  const open = () => {
    setIsExtended(true);
  };

  const close = (isLeavingPage = false) => {
    if (!isLeavingPage) {
      setIsExtended(false);
    }
  };

  useEffect(() => {
    return () => close(true);
  }, []);

  const handlers = useSwipeable({
    onSwipedUp: () => open(),
    onSwipedDown: () => close(),
  });

  let subtext = null;
  if (variant?.stock) {
    if (variant.price && variant.original_price) {
      subtext = (
        <>
          <span className={styles.NextSpace}>{t('save')} </span>
          <span className={styles.NextSpace}>
            {' '}
            <Price value={variant?.original_price - variant?.price} />
          </span>
          <span className={styles.NextSpace}>
            {' '}
            (
            {Math.round(
              ((variant?.original_price - variant?.price) / variant.original_price) * 100,
            )}
            %)
          </span>
        </>
      );
    } else {
      subtext = (
        <>
          <img
            src="/static/incredible/warranty-small.svg"
            className={css.warrantyIcon}
            alt="1 year warranty"
          />
          {t('12MonthWarranty')}
        </>
      );
    }
  }

  return (
    <>
      <Transition visible={isExtended} animation="fade" duration={500}>
        <div className={css.modalBackground} />
      </Transition>

      <div
        className={classnames(css.box, {
          [css.extended]: isExtended,
        })}
      >
        <div {...handlers} className={css.topContent}>
          <div className={css.left}>
            {!variant?.stock || !variant?.price ? (
              <div>{t('outOfStock')}</div>
            ) : (
              <>
                <div>
                  <Price value={variant.price} className={css.price} />
                  <Price value={variant.original_price} className={css.originalPrice} />
                </div>
                <div className={css.warranty}>{subtext}</div>
              </>
            )}
          </div>
          {variant?.stock > 0 && variant?.price > 0 && !isExtended && (
            <div className={css.right}>
              <Button
                fluid
                primary
                onClick={() => {
                  onBuy(false);
                  open();
                }}
                loading={isBuying}
              >
                <LockClosed color="white" />{' '}
                <span className={styles.MarginHalfLeft}>{t('addToCart')}</span>
              </Button>
            </div>
          )}

          {isExtended && (
            <div className={css.right}>
              <Button
                fluid
                primary
                onClick={() => {
                  GA.event({
                    category: 'cart',
                    action: 'continue-to-cart',
                    label: 'buy-fixed',
                  });
                  Router.push('/cart');
                }}
                loading={isBuying}
              >
                {t('continue')}
              </Button>
            </div>
          )}
        </div>
        {isExtended && (
          <div className={css.addons}>
            <div className={styles.Large}>{t('weThinkYouWillLike')}</div>
            {loadingAccessoryProducts && (
              <div className={classnames(styles.MarginDoubleTop, styles.MarginDoubleBottom)}>
                <Loader active={loadingAccessoryProducts} inline="centered" />
              </div>
            )}
            {accessoryProducts.map((it, i) => (
              <AccessoryProduct key={it.id} product={it} api={api} cart={cart} />
            ))}
          </div>
        )}
      </div>
    </>
  );
}

export default withAppConsumer(translate(['home', 'common'], {withRef: true})(BuyFixed));

import React, {useState, useMemo, useEffect} from 'react';
import {withAppConsumer} from '../app-context';
import {DefaultCurrency, FallBackUnlocked, AppColors} from '@Common/constants/app';
import {translate} from 'react-i18next';
import classnames from 'classnames';
import Dropdown from '@Components/inputs/wrapped-dropdown';
import Image from '@Components/images/image';
import Price from './price';
import {UnlockedCarrierId, EmptyDropdownChoice, UnlockedSwatches} from '@Common/constants/app';
import {
  DownChevron,
  LockClosed,
  LockOpen,
  DealTag,
} from '../../../widget/components/display/svg-icons';
import GenericModal from '@Components/modals/generic-modal';

import {Modal, Button} from 'semantic-ui-react';

import css from './carrier-selector.scss';
import styles from '@Common/styles/styles.scss';

function CarrierSelector({
  allCarriers,
  selections = {},
  carriers = [],
  t,
  setSelections,
  category_id,
  api,
  onlySecondLine,
  isOpen = false,
  setIsOpen,
  context: {isMobile, isTablet},
}) {
  const [option, setOption] = useState(selections.carrier || EmptyDropdownChoice);

  const [prices, setPrices] = useState([]);

  useEffect(() => {
    if (isOpen && !onlySecondLine) {
      api.product.getMinimumPricePerCarrier({id: category_id, selections}).then(({resp}) => {
        if (resp?.prices) {
          setPrices(resp.prices);
        }
      });
    }
  }, [isOpen, selections]);

  const closeModal = () => setIsOpen(false);
  const select = id => {
    closeModal();
    setSelections('carrier', id);
  };

  const selectedCarrier = useMemo(() => {
    return allCarriers.filter(carrier => carrier.id === selections.carrier)[0];
  }, [allCarriers, selections.carrier]);

  const swatches = useMemo(() => {
    const results = new Set();
    carriers.forEach(c => {
      if (UnlockedSwatches.hasOwnProperty(c.id) || !c.thumbnail) {
        if (selectedCarrier && !UnlockedSwatches[c.id].includes(selectedCarrier.thumbnail)) {
          results.add(selectedCarrier.thumbnail);
        }
        UnlockedSwatches[c.id].forEach(s => results.add(s));
      } else if (c.thumbnail) {
        if (selectedCarrier && c.thumbnail !== selectedCarrier.thumbnail) {
          results.add(selectedCarrier.thumbnail);
        }
        results.add(c.thumbnail);
      }
    });
    return Array.from(results).map(r => {
      return {file: r, name: (r || '').split('/')?.[1]?.split('.')?.[0]};
    });
  }, [carriers]);

  const isUnlocked = useMemo(() => carriers.some(c => !c.thumbnail), [carriers]);

  const isDeal = useMemo(() => {
    if (isUnlocked && selectedCarrier && selections.carrier !== UnlockedCarrierId) {
      return true;
    }
  }, [carriers, isUnlocked, selectedCarrier]);

  const openModalCarriers = useMemo(() => {
    return allCarriers
      .filter(c => !!c.thumbnail)
      .map(c => {
        const item = {...c};
        item.price =
          prices.find(p => {
            if (p.name === c.name || FallBackUnlocked[c.name]?.includes(p.attr_id)) {
              return p;
            }
          })?.min_price || null;
        return item;
      });
  }, [prices]);

  const unlockedPrice = useMemo(() => {
    return prices.find(p => p.attr_id === UnlockedCarrierId)?.min_price || null;
  }, [prices]);

  const secondLine = (
    <div className={css.secondLineValue}>
      {swatches.map(swatch => (
        <Image
          key={swatch.file}
          file={swatch.file}
          alt={swatch.name}
          type="svg"
          className={classnames(css.carrierSwatch, css[swatch.name])}
        />
      ))}
      {(selections.carrier === UnlockedCarrierId || !selectedCarrier) && (
        <a className={css.moreButton} onClick={() => setIsOpen(true)}>
          + more
        </a>
      )}
    </div>
  );

  if (onlySecondLine) {
    return (
      <div class={css.onlySecondLine}>
        <div>
          {t(
            selections.carrier === UnlockedCarrierId || !selectedCarrier
              ? 'worksOnAnyCarrier'
              : 'worksOnCarrier',
            {carrier: t(selectedCarrier?.name)},
          )}
        </div>
        {secondLine}
      </div>
    );
  }

  return (
    <>
      <div>
        <div className={css.input} onClick={() => setIsOpen(true)}>
          <>
            <div className={css.firstLineValue}>
              {selections.carrier === UnlockedCarrierId || !selectedCarrier ? (
                <>
                  <div className={styles.FlexCenter}>
                    <span className={styles.FlexCenter}>
                      <LockOpen />
                      <span className={styles.MarginHalfLeft}>{t('fullyUnlocked')}</span>
                    </span>
                  </div>
                  <div className={css.legend}>
                    {t('seeLowerPrices')} <DownChevron className={css.chevron} />
                  </div>
                </>
              ) : (
                <>
                  <div className={styles.FlexCenter}>
                    <span className={styles.FlexCenter}>
                      {isUnlocked ? <LockOpen /> : <LockClosed />}
                      <span className={styles.MarginHalfLeft}>
                        {t(`worksOnCarrier`, {carrier: t(selectedCarrier.name)})}
                      </span>
                    </span>
                  </div>
                  <span className={css.legend}>
                    <DownChevron className={css.chevron} />
                  </span>
                </>
              )}
            </div>
          </>
        </div>
      </div>
      {isDeal && (
        <div
          className={classnames(
            styles.Tiny,
            styles.ColorSubtext,
            styles.MarginTop,
            styles.FlexCenter,
          )}
        >
          <div className={styles.MarginHalfRight}>
            <DealTag />
          </div>
          <div>
            <b>{t('weFoundADeal')} </b>
            <span>{t('unlockedBestDeal', {carrier: selectedCarrier.name})}</span>
          </div>
        </div>
      )}
      <GenericModal onClose={closeModal} open={isOpen} size="mini">
        <Modal.Header>
          <div className={classnames(styles.MediumBold, styles.MarginBottom, styles.centered)}>
            {t('selectACarrier')}
          </div>
          <div className={classnames(styles.Small, styles.centered)}>
            {t('selectCarrierExplanation')}
          </div>
        </Modal.Header>
        <Modal.Content>
          <div className={classnames(css.options, styles.SmallBold)}>
            <div
              className={classnames(css.option, styles.MediumBold)}
              key={UnlockedCarrierId}
              onClick={() => select(UnlockedCarrierId)}
            >
              <div className={styles.FlexCenter}>
                <LockOpen color={AppColors.accent} className={styles.MarginHalfRight} />
                <div>
                  <div className={css.spaced}>
                    <div>{t('fullyUnlocked')}</div>
                  </div>
                  <div className={classnames(css.spaced, styles.Small)}>
                    <div>{t('worksWithAnyCarrier')}</div>
                  </div>
                </div>
              </div>
              <div>
                <Price
                  outOfStock={true}
                  value={unlockedPrice}
                  outOfStockClassName={classnames(styles.ColorWhite60, styles.Small)}
                />
              </div>
            </div>
            {openModalCarriers.map(carrier => {
              return (
                <div className={css.option} key={carrier.id} onClick={() => select(carrier.id)}>
                  <Image
                    file={carrier.thumbnail}
                    type="svg"
                    width={70}
                    height={45}
                    className={css[carrier.slug]}
                  />
                  <div>
                    <Price
                      outOfStock={true}
                      value={carrier.price}
                      outOfStockClassName={classnames(styles.ColorWhite60, styles.Small)}
                    />
                  </div>
                </div>
              );
            })}
          </div>
        </Modal.Content>
      </GenericModal>
    </>
  );
}

export default withAppConsumer(translate(['home', 'common'])(CarrierSelector));

import React, {useState, useMemo, useEffect, useRef} from 'react';
import {translate} from 'react-i18next';
import {withAppConsumer} from '@Components/app-context';
import {Grid} from 'semantic-ui-react';
import {Button, Transition} from 'semantic-ui-react';
import Router from 'next/router';
import Price, {textPrice} from './price';
import CarrierSelector from './carrier-selector';
import Tag from './tag';
import Link from 'next/link';
import {getPageLink} from '@Common/utils/urls';
import ChatLink from '../display/chat-link';
import Image from '../images/image';

import {Paypal} from '../../../widget/components/display/svg-icons';
import Stars from '../display/stars';
import TrustPilotReviews from './trustpilot-reviews';
import classnames from 'classnames';
import {useSwipeable} from 'react-swipeable';
import AccessoryProductsModal from '../modals/accessory-products-modal';
import {
  Info,
  Shield,
  Truck,
  CreditCards,
  Ribbon,
  LockClosed,
} from '../../../widget/components/display/svg-icons';
import PurchaseBox from './purchase-box';

import {findRelevantImages, consolidateAttributesAsName} from '../../utils/attributes-utils';

import styles from '@Common/styles/styles.scss';
import css from './sticky-buy.scss';

const Margin = 20;

function StickyBuy({
  t,
  limitId = 'start_footer',
  onBuy,
  isBuying,
  product,
  actions,
  api,
  app,
  tags,
  cart,
  longName,
  selections,
  setSelections,
  isOpenCarriersModal,
  setIsOpenCarriersModal,
  isCategory,
  context: {isTablet},
  product: {imageMap, allImages},
  selectedAttr,
}) {
  const ref = useRef(null);
  const coors = useRef(null);
  const limit = useRef(null);
  const lastScrollY = useRef(null);
  const [style, setStyle] = useState({});

  const [showModal, setShowModal] = useState(false);

  const images = useMemo(() => {
    return findRelevantImages(allImages, tags);
  }, [allImages, tags]);

  const {price, original_price, stock} = product.defaultVersion?.variant || {};
  const percent = price && original_price ? 100 - (price * 100) / original_price : null;

  const carrierName = useMemo(() => {
    if (!selectedAttr) {
      return '';
    }
    const carrier = app.carriers.find(a => a.slug === selectedAttr);
    return carrier?.name || '';
  }, [selectedAttr, app.carriers]);

  return (
    <>
      <div style={style} ref={ref}>
        <CarrierSelector
          selections={selections}
          allCarriers={app.carriers}
          carriers={product.defaultVersion.tags?.filter(it => it.type === 'carrier')}
          onlySecondLine={true}
          api={api}
          category_id={product.id}
          isOpen={isOpenCarriersModal}
          setIsOpen={setIsOpenCarriersModal}
        />
        <h1>
          Refurbished {carrierName} {product.product.name}
        </h1>
        <h2
          className={classnames(
            styles.ButtonRegular,
            styles.MarginDoubleBottom,
            styles.MarginHalfTop,
          )}
        >
          {t('categorySubtitle', {name: product.product.name})}
        </h2>
        <div className={classnames(styles.Button, styles.MarginBottom)}>
          {t('chooseYourCarrier')}
        </div>
        <CarrierSelector
          selections={selections}
          allCarriers={app.carriers}
          carriers={product.defaultVersion.tags?.filter(it => it.type === 'carrier')}
          api={api}
          category_id={product.id}
          isOpen={isOpenCarriersModal}
          setIsOpen={setIsOpenCarriersModal}
          category_id={product.product.id}
          setSelections={(field, id) => {
            const carrier = app.carriers.find(a => a.id === id);
            window.location = `${getPageLink('category', {
              product: product.product,
              t,
            })}?attr=${carrier.slug}`;
          }}
        />

        <div className={classnames(css.categoryBox, styles.MarginDoubleTop)}>
          <div className={classnames(styles.FlexSpaceBetween, styles.FlexTop)}>
            <Image size={250} file={imageMap[images[0]?.img_id]} />
            <div className={classnames(styles.fluid)}>
              <div className={styles.MarginBottom}>
                {t('ourBestName', {name: product.product.name})}
              </div>
              <div className={classnames(styles.fluid, styles.MarginBottom, styles.MarginRight)}>
                <span className={styles.Huge}>{longName}</span>
              </div>
              <div>
                {price && stock ? (
                  <div className={styles.FlexCenter}>
                    <Price value={price} className={styles.Huge} />
                    {original_price && (
                      <Tag
                        text={
                          <span>
                            {t('saveX', {amount: textPrice({value: original_price - price})})} (
                            {Math.floor((1 - price / original_price) * 100)}%)
                          </span>
                        }
                        className={styles.MarginLeft}
                      />
                    )}
                  </div>
                ) : (
                  t('outOfStock')
                )}
                {original_price && (
                  <div className={css.originalPrice}>
                    {t('Original Price')}: <Price value={original_price} className={css.struk} />
                  </div>
                )}
              </div>
            </div>
          </div>
          <Link
            href={getPageLink('variant', {
              product: {
                name: product.product.name,
                category_id: product.product.id,
                id: product.defaultVersion?.variant?.id,
              },
              attributes: consolidateAttributesAsName(product.defaultVersion?.tags, t),
              t,
            })}
          >
            <Button fluid primary className={styles.MarginDoubleTop}>
              See more
            </Button>
          </Link>
          <div className={classnames(styles.FlexSpaceBetween, styles.MarginTop)}>
            <div className={classnames(styles.ColorAccent, styles.FlexAllCenter)}>
              <Paypal size={22} color="#4f91ff" />
              <span className={styles.MarginHalfLeft}> Buyer protection</span>
            </div>
            <div className={styles.ColorAccent}>30 days easy returns</div>
          </div>
        </div>
      </div>
      <div
        className={classnames(styles.MarginHalfBottom, {
          [styles.FlexCenter]: !isTablet,
        })}
      >
        <div className={classnames(styles.fluid, styles.FlexAllCenter)}>
          <ChatLink onlyContent={true} />
        </div>
      </div>
      {showModal && (
        <AccessoryProductsModal
          loadingAccessoryProducts={product.loadingAccessoryProducts}
          product={product}
          cart={cart}
          api={api}
          onClose={() => setShowModal(false)}
        />
      )}
    </>
  );
}

export default withAppConsumer(translate(['home', 'common'])(StickyBuy));

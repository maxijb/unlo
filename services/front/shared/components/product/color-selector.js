import React, {useMemo} from 'react';
import {DefaultCurrency} from '@Common/constants/app';
import {translate} from 'react-i18next';
import classnames from 'classnames';

import css from './color-selector.scss';

function ColorSelector({selectedTags = new Set(), colors = [], selections, setSelections, t}) {
  const colorName = useMemo(() => {
    return colors.filter(color => selectedTags.has(color.id))?.[0]?.name || '';
  }, [colors, selectedTags]);
  return (
    <div>
      <div className={css.colorName}>{colorName}</div>
      {colors.map((color, i) => (
        <div
          key={i}
          className={classnames(css.colorSwatch, {
            [css.selected]: selections.color
              ? color.id === selections.color
              : selectedTags.has(color.id),
          })}
          onClick={e => {
            e.stopPropagation();
            setSelections('color', color.id);
          }}
          title={t(color.name)}
        >
          <div style={{backgroundColor: color.thumbnail}} className={css.colorSwatch} />
        </div>
      ))}
    </div>
  );
}

export default translate(['home', 'common'])(ColorSelector);

import React, {useMemo} from 'react';
import {translate} from 'react-i18next';
import {withAppConsumer} from '../app-context';
import Link from 'next/link';
import classnames from 'classnames';
import {Trans} from 'react-i18next';
import Price from './price';
import Image from '@Components/images/image';
import {OpenNewTab, BlueOK} from '../../../widget/components/display/svg-icons';
import {Grid} from 'semantic-ui-react';
import {findRelevantImages} from '../../utils/attributes-utils';
import {Modals, AppColors} from '@Common/constants/app';

import styles from '@Common/styles/styles.scss';
import css from './competitor-prices.scss';

function CompetitorPrices({
  product,
  t,
  tags,
  name,
  api,
  actions,
  showImage = true,
  loadVariant,
  context: {GA},
}) {
  const {
    competitorPrices = [],
    cheapestOwnPrice,
    cheapestVariant,
    imageMap = {},
    allImages = [],
    isLoadingCompetitorPrices,
  } = product;

  const images = useMemo(() => {
    if (!showImage) {
      return null;
    }
    return findRelevantImages(allImages, tags);
  }, [allImages, tags, showImage]);

  const showingPrices = useMemo(() => {
    return competitorPrices.filter(p => p.price >= cheapestOwnPrice).slice(0, 3);
  }, [cheapestOwnPrice, competitorPrices]);

  const shouldShow = useMemo(() => {
    const resp =
      showingPrices.length >= 2 &&
      product?.defaultVersion?.variant?.stock > 0 &&
      product?.defaultVersion?.variant?.price > 0;
    GA.event({
      category: 'competitors-display',
      action: resp ? 'show-competitors' : 'hide-competitors',
      label: product?.product?.name || '',
      nonInteraction: true,
    });
    return resp;
  }, [showingPrices, product?.defaultVersion?.variant]);

  const capacity = useMemo(() => {
    return product.defaultVersion?.tags?.filter(t => t.type === 'capacity')?.[0]?.name;
  }, [product.defaultVersion]);

  if (!shouldShow) {
    return null;
  }

  const image = !showImage ? null : imageMap[images[0]?.img_id];
  const productName = `${name} ${capacity}`;

  return (
    <>
      <Grid columns={16} className={classnames(styles.MainGridWithBackground, css.grid)}>
        <Grid.Row>
          <Grid.Column width={16} className={css.column}>
            <div className={classnames(styles.FlexCenter, {})}>
              {image && (
                <div className={classnames(styles.FlexNoShrink, styles.MarginOneHalfRight)}>
                  <Image file={image} height={110} width={55} as="div" />
                </div>
              )}
              <div className={classnames(styles.FlexGrow, css.pricesHolder)}>
                <div className={css.banner}>
                  <BlueOK />
                  <Trans i18nKey="lowestPriceBanner">
                    Text
                    <a
                      onClick={() => actions.setCurrentModal(Modals.lowPrices)}
                      style={{marginLeft: 6, color: AppColors.dark}}
                    >
                      <b>link</b>
                    </a>
                  </Trans>
                </div>
                <div className={classnames({[css.loading]: isLoadingCompetitorPrices})}>
                  <div
                    className={classnames(
                      styles.MediumBold,
                      styles.MarginBottom,
                      styles.FlexSpaceBetween,
                      css.ownPrice,
                    )}
                    onClick={() => loadVariant(cheapestVariant)}
                  >
                    <span>{t('ourLowPrice')}</span>
                    <Price value={cheapestOwnPrice} className={styles.Large} />
                  </div>
                  {showingPrices.map(c => (
                    <div
                      key={c.source}
                      className={classnames(styles.MarginHalfBottom, styles.FlexSpaceBetween)}
                      onClick={() => {
                        GA.event({
                          category: 'inputs',
                          action: 'competitor-click',
                          label: c.source,
                          value: product?.product?.id,
                        });
                      }}
                    >
                      <a
                        href={c.url}
                        target="_blank"
                        rel="noopener noreferrer"
                        className={classnames(styles.SecondaryLink, styles.FlexCenter)}
                      >
                        <span className={styles.MarginHalfRight}>
                          {t(`competitor_${c.source}`)}
                        </span>
                        <OpenNewTab />
                      </a>

                      <Price value={c.price} />
                    </div>
                  ))}
                </div>
              </div>
            </div>
            {showImage && (
              <div className={classnames(styles.Tiny, styles.ColorSubtext, styles.MarginHalfTop)}>
                {t('actualPricesFor', {product: productName})}
              </div>
            )}
          </Grid.Column>
        </Grid.Row>
      </Grid>
      {!showImage && (
        <div
          className={classnames(
            styles.Tiny,
            styles.ColorSubtext,
            styles.MarginHalfTop,
            styles.Centered,
          )}
        >
          {t('actualPricesFor', {product: productName})}
        </div>
      )}
    </>
  );
}

export default withAppConsumer(translate(['home'])(CompetitorPrices));

import React from 'react';
import Listing from './listing';
import {translate} from 'react-i18next';
import classnames from 'classnames';
import {CarouselProvider, Slider, Slide} from 'pure-react-carousel';
import {withAppConsumer} from '../app-context';

import styles from '@Common/styles/styles.scss';
import css from './highlighted-products.scss';

function HighlightedProducts({
  products,
  t,
  title = null,
  context,
  isProductPage = false,
  className,
  page,
}) {
  const {isMobile, isTablet} = context;
  const numberOfSlides = isMobile ? 2 : isTablet ? 3 : 5;

  return (
    <div className={className}>
      <div
        className={classnames(styles.sectionTitle, {
          [styles.Centered]: !isMobile && !isProductPage,
        })}
      >
        {title}
      </div>
      <div className={styles.MarginDoubleTop}>
        <CarouselProvider
          totalSlides={products.list.length}
          visibleSlides={numberOfSlides}
          step={0}
          naturalSlideWidth={isMobile ? 220 : 400}
          isIntrinsicHeight={true}
        >
          <Slider>
            {products.list.map((it, i) => (
              <Slide key={it.id} index={i}>
                <Listing product={it} isMobile={isMobile} page={page} />
              </Slide>
            ))}
          </Slider>
        </CarouselProvider>
      </div>
    </div>
  );
}

export default withAppConsumer(translate(['home', 'common'])(HighlightedProducts));

import React, {useState, useMemo} from 'react';
import {translate} from 'react-i18next';
import Image from '@Components/images/image';
import {withAppConsumer} from '@Components/app-context';
import classnames from 'classnames';
import Carousel, {Dots, slidesToShowPlugin} from '@brainhubeu/react-carousel';
import {findRelevantImages} from '../../utils/attributes-utils';

import css from './image-box.scss';

function ImageBox(props) {
  const {imageMap, allImages, tags} = props;
  const [imgIndex, setImgIndex] = useState(0);
  const {isMobile} = props.context;

  const images = useMemo(() => {
    return findRelevantImages(allImages, tags);
  }, [allImages, tags]);

  return (
    <div>
      <div>
        <div className={css.screen}>
          <Carousel
            value={imgIndex}
            slides={images.map((img, i) => (
              <Image file={imageMap[img.img_id]} size={270} />
            ))}
            onChange={val => setImgIndex(val)}
            plugins={[
              'clickToChange',
              'centered',
              {
                resolve: slidesToShowPlugin,
                options: {
                  numberOfSlides: 2,
                },
              },
            ]}
          />
        </div>
        <div className={css.slider}>
          <Dots
            value={imgIndex}
            onChange={val => setImgIndex(val)}
            thumbnails={images.map((img, i) =>
              isMobile ? (
                <div
                  className={classnames(css.imageBullet, {
                    [css.selected]: i === imgIndex,
                  })}
                  onClick={() => setImgIndex(i)}
                />
              ) : (
                <Image
                  size={50}
                  className={classnames(css.thumbnail, {
                    [css.selected]: i === imgIndex,
                  })}
                  file={imageMap[img.img_id]}
                  onClick={() => setImgIndex(i)}
                  key={i}
                />
              ),
            )}
          />
        </div>
      </div>
    </div>
  );
}

export default withAppConsumer(translate(['home', 'common'])(ImageBox));

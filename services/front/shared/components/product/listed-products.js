import React from 'react';
import Listing from './product-listing';
import {translate} from 'react-i18next';
import classnames from 'classnames';
import {CarouselProvider, Slider, Slide} from 'pure-react-carousel';
import {withAppConsumer} from '../app-context';

import styles from '@Common/styles/styles.scss';
import css from './highlighted-products.scss';

function ListedProducts({
  products,
  t,
  title = null,
  context,
  isProductPage = false,
  className,
  page,
}) {
  const {isMobile, isTablet} = context;
  const numberOfSlides = isMobile ? 2 : isTablet ? 3 : 5;

  return (
    <div className={className}>
      <div
        className={classnames(styles.sectionTitle, {
          [styles.Centered]: !isMobile && !isProductPage,
        })}
      >
        {title}
      </div>
      <div
        className={classnames(styles.MarginDoubleTop, styles.Flex, styles.FlexTop, styles.FlexWrap)}
      >
        {products.list.map((it, i) => (
          <div style={{width: 100 / numberOfSlides + '%'}}>
            <Listing product={it} isMobile={isMobile} page={page} withBorder={true} />
          </div>
        ))}
      </div>
    </div>
  );
}

export default withAppConsumer(translate(['home', 'common'])(ListedProducts));

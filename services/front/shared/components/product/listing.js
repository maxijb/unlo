import {getPageLink} from '@Common/utils/urls';
import React, {useState, useMemo} from 'react';
import {DefaultCurrency} from '@Common/constants/app';
import {translate} from 'react-i18next';
import classnames from 'classnames';
import ColorSelector from './color-selector';
import Price from './price';
import Image from '../images/image';
import Link from 'next/link';
import {withAppConsumer} from '../app-context';

import css from './listing.scss';
import styles from '@Common/styles/styles.scss';

function Listing({product, t, isMobile, page, context: {GA}}) {
  const [selections, setSelections] = useState({color: product.attr_id});
  const setSelectedColor = (attr, value) => {
    setSelections({...selections, [attr]: value});
  };
  const image = useMemo(() => {
    return product.allColors.find(it => it.id === selections.color);
  }, [selections, product]);

  const allColors = useMemo(() => {
    return product.allColors.sort((a, b) => {
      if (a.id === product.attr_id) {
        return -1;
      }
      if (b.id === product.attr_id) {
        return 1;
      }
      return 0;
    });
  }, [product]);

  return (
    <div
      className={classnames(css.container)}
      onClick={() => {
        GA.event({
          category: 'navigation',
          action: 'highlighted-product-click',
          label: page || '',
        });
      }}
    >
      <Link
        prefetch
        as={getPageLink('product', {product, t})}
        href={`/product?id=${product.category_id}`}
      >
        <div className={css.product}>
          <div>
            <Image size={150} file={image?.image} />
          </div>
          <div className={css.name}>{product.name}</div>
          <div className={css.priceHolder}>
            <span className={css.price}>
              <span>{t('from')} </span>
              <Price value={product.price} />
            </span>
            <Price className={css.originalPrice} value={product.original_price} />
          </div>
          {/*!isMobile && (
            <ColorSelector
              selections={selections}
              colors={allColors}
              setSelections={setSelectedColor}
            />
          )*/}
        </div>
      </Link>
    </div>
  );
}

export default withAppConsumer(translate(['home', 'common'])(Listing));

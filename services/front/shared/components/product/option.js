import React from 'react';
import classnames from 'classnames';

import css from './tag.scss';

function Option({text, className, selected = false, content}) {
  return (
    <div
      className={classnames(css.option, className, {
        [css.selected]: !!selected,
      })}
    >
      {content}
    </div>
  );
}

export default Option;

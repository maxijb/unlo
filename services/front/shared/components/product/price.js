import React from 'react';
import {translate} from 'react-i18next';
import {DefaultCurrency} from '@Common/constants/app';

import css from './price.scss';

const formatterDecimals = new Intl.NumberFormat('en-US', {
  //style: 'currency',
  //currency: 'USD',
  minimumFractionDigits: 2,
  maximumFractionDigits: 2,
});

function Price({
  value,
  className,
  isFree,
  outOfStock,
  currency = DefaultCurrency,
  currencyClassName,
  t,
  format,
  outOfStockClassName,
}) {
  const text = formatPrice(value);

  return (
    <span className={className}>
      {!value && outOfStock ? (
        <span className={outOfStockClassName}>{t('outOfStockShort')}</span>
      ) : (
        <>
          {isFree && !value && <span className={css.free}>{t('free')}</span>}
          {Number(value) < 0 && <span>-</span>}
          <span className={currencyClassName}>{currency}</span>
          <span>{text}</span>
        </>
      )}
    </span>
  );
}

export default translate(['home', 'common'])(Price);

export function textPrice({value, currency = DefaultCurrency}) {
  const text = formatPrice(value);
  return `${Number(value) < 0 ? '-' : ''}${currency}${text}`;
}

function formatPrice(value) {
  const num = Number(value);
  if (Number.isNaN(num)) {
    return value;
  }
  return formatterDecimals.format(Math.abs(num));
}

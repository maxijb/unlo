import React, {useMemo, useState} from 'react';
import Router from 'next/router';
import classnames from 'classnames';
import {translate} from 'react-i18next';
import {Icon} from 'semantic-ui-react';

import {withAppConsumer} from '@Components/app-context';
import Dropdown from '@Components/inputs/wrapped-dropdown';
import Image from '@Components/images/image';
import {getPageLink} from '@Common/utils/urls';

import styles from '@Common/styles/styles.scss';
import css from './product-dropdown.scss';

function ProductDrodown({visibleLeaves, t, context: {GA}, isIndex = true}) {
  const options = useMemo(() => {
    return [
      isIndex
        ? {
            value: '',
            content: (
              <div className={styles.FlexCenter}>
                <Image
                  file="ixblack1.png"
                  width={20}
                  height={40}
                  className={styles.MarginHalfRight}
                />
                {t('shopIphonesByModel')}
              </div>
            ),
          }
        : {
            value: '',
            content: (
              <div className={styles.FlexCenter}>
                <Image
                  file="ixblack1.png"
                  width={20}
                  height={40}
                  className={styles.MarginHalfRight}
                />
                {t('shopIphonesByModel')}
              </div>
            ),
          },
      ...visibleLeaves.map(it => {
        return {text: it.name, value: it.id, key: it.id};
      }),
    ].filter(i => !!i);
  }, [visibleLeaves]);
  const [dropdownValue, setDropdownValue] = useState('');

  return (
    <Dropdown
      upward={false}
      fluid
      selection
      search
      placeholder={
        <div
          className={classnames(styles.FlexCenter, {
            [styles.ButtonRegular]: !isIndex,
          })}
        >
          {isIndex && (
            <>
              <Image
                file="ixblack1.png"
                width={20}
                height={40}
                className={styles.MarginHalfRight}
              />
              {t('shopIphonesByModel')}
            </>
          )}
          {!isIndex && <span className={styles.ColorSubtext}>{t('allOurPhones')}</span>}
        </div>
      }
      options={options}
      icon={isIndex ? 'dropdown' : <Icon name="search" className={css.searchIcon} />}
      value={dropdownValue}
      onChange={value => {
        GA.event({
          category: 'navigation',
          action: 'product-dropdown',
          label: isIndex ? 'index-navigation' : 'header-navigation',
          value: value || 0,
        });

        //setDropdownValue(value);
        const prod = visibleLeaves.find(it => it.id === value);
        if (value) {
          Router.push(`/product?id=${value}`, getPageLink('product', {product: prod, t}));
        }
      }}
      className={classnames({
        [css.productDropdown]: isIndex,
        [css.headerDropdown]: !isIndex,
      })}
    />
  );
}

export default withAppConsumer(translate(['home'])(ProductDrodown));

import React, {useState, useMemo} from 'react';
import {translate, Trans} from 'react-i18next';
import Image from '@Components/images/image';
import classnames from 'classnames';
import CarrierSelector from './carrier-selector';
import ColorSelector from './color-selector';
import TagSelector from './tag-selector';
import {Grid, Checkbox} from 'semantic-ui-react';
import {withAppConsumer} from '@Components/app-context';
import WrappedDropdown from '@Components/inputs/wrapped-dropdown';
import AppearanceModal from '@Components/modals/appearance-modal';
import {Button} from 'semantic-ui-react';
import Router from 'next/router';
import Price from './price';
import BuyFixed from './buy-fixed';
import Tag from './tag';
import {Info, Shield, Truck, Paypal} from '../../../widget/components/display/svg-icons';

import {Modals} from '@Common/constants/app';

import css from './purchase-box.scss';
import styles from '@Common/styles/styles.scss';

function PurchaseBox(props) {
  const {
    product,
    selections,
    variant,
    allAttributes,
    currentAttributes,
    accessoryProducts,
    tags,
    t,
    api,
    actions,
    allCarriers,
    minimumPricePerCondition,
    setSelections,
    context: {isMobile, isTablet, GA},
    setIsOpenTradeIn,
    cart,
    onBuy,
    isBuying,
    loadingPricePerCondition,
    isOpenCarriersModal,
    setIsOpenCarriersModal,
  } = props;

  const [trackedSpecialDeal, setTrackedSpecialDeal] = useState(false);

  const {appearanceOptions, selectedAppearanceName, optionsHaveLabel} = useMemo(() => {
    let selectedAppearanceName = null;
    let optionsHaveLabel = false;
    const list = (allAttributes.appearance || []).slice().reverse();
    const appearanceOptions = list
      .map((attr, i) => {
        let minPrice = minimumPricePerCondition[attr.id] || null;

        if (tags.has(attr.id)) {
          selectedAppearanceName = attr.name;

          if (variant?.price && (!minPrice || minPrice < variant.price)) {
            minPrice = variant.price;
          }
        }

        const nextPrice = minimumPricePerCondition[list[i + 1]?.id] || null;
        let label = null;

        if (!optionsHaveLabel && minPrice && nextPrice && minPrice <= nextPrice) {
          label = (
            <div className={classnames(styles.ColorNegative, styles.TinyBold)}>
              {t('specialDeal')}
            </div>
          );
          optionsHaveLabel = true;
          if (!trackedSpecialDeal) {
            GA.event({category: 'inputs', action: 'show-special-deal', label: attr.name});
            setTrackedSpecialDeal(true);
          }
        }

        const disabled = !loadingPricePerCondition && minPrice === null;

        return {
          ...attr,
          label,
          content: (
            <div
              onClick={() => {
                if (label) {
                  GA.event({category: 'inputs', action: 'click-special-deal', label: attr.name});
                }
              }}
            >
              <div className={css.appearanceName}>{t(attr.name)}</div>
              <span className={css.appearancePrice}>
                {minPrice !== null && <Price value={minPrice} />}
                {disabled && t('outOfStockShort')}
                <span className={css.placeholder}>.</span>
              </span>
            </div>
          ),
          disabled,
        };
      })
      .reverse();

    return {
      appearanceOptions,
      selectedAppearanceName,
      optionsHaveLabel,
    };
  }, [allAttributes.appearance, minimumPricePerCondition, tags]);

  const [isShowCarriersModal, setIsShowCarriersModal] = useState(false);

  const capacities = useMemo(() => {
    return allAttributes.capacity
      ?.map(attr => ({
        value: attr.id,
        text: t(attr.name),
      }))
      .sort((a, b) => {
        return parseInt(a.text) - parseInt(b.text);
      });
  }, [allAttributes.capacity]);

  return (
    <>
      <div className={css.box}>
        <div className={styles.section}>
          <Grid reversed="computer tablet">
            <Grid.Row>
              <Grid.Column width={10}>
                <ColorSelector
                  selectedTags={tags}
                  colors={allAttributes.color}
                  setSelections={setSelections}
                  selections={selections}
                />
              </Grid.Column>
              <Grid.Column width={6}>
                <WrappedDropdown
                  fluid
                  options={capacities}
                  value={selections.capacity}
                  onChange={value => setSelections('capacity', value)}
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>

        <div className={classnames(styles.section, styles.MarginTop)}>
          <CarrierSelector
            allCarriers={allCarriers}
            carriers={currentAttributes?.filter(it => it.type === 'carrier')}
            selections={selections}
            setSelections={setSelections}
            api={api}
            category_id={product.id}
            isOpen={isOpenCarriersModal}
            setIsOpen={setIsOpenCarriersModal}
          />
        </div>

        <div
          className={classnames({
            [styles.section]: true,
          })}
        >
          <div
            className={classnames(
              styles.sectionTitle,
              styles.MarginBottom,
              styles.FlexSpaceBetween,
              styles.FlexBaseline,
            )}
          >
            <div>
              {t('selectYourAppearance')}
              <span
                onClick={() => {
                  setIsShowCarriersModal(true);
                }}
              >
                <Info className={css.infoIcon} />
              </span>
            </div>
            {/*variant?.stock && variant.stock < 10 && (
              <div className={css.urgency}>{t('onlyXLeft', {stock: variant.stock})}</div>
            )*/}
          </div>
          <TagSelector
            className={css.appearanceTagSelector}
            selectedTags={tags}
            tags={appearanceOptions}
            onChange={value => setSelections('appearance', value)}
          />
          {selectedAppearanceName === 'Excellent' && (
            <div className={classnames(styles.MarginTop, styles.Tiny, styles.ColorSubtext)}>
              <div>
                <b>Screen:</b> Absolutely no scratches.{' '}
              </div>
              <div>
                <b>Body:</b> No scratches visible from a distance of 8 inches or more.{' '}
              </div>
              <div>
                <b>Battery:</b> Great health that exceeds{' '}
                <a onClick={() => actions.setCurrentModal(Modals.battery)}>80% capacity</a> relative
                to new. No fast drain.{' '}
              </div>
            </div>
          )}
          {selectedAppearanceName === 'Good' && (
            <div className={classnames(styles.MarginTop, styles.Tiny, styles.ColorSubtext)}>
              <div>
                <b>Screen:</b> Light micro scratches that are not visible when the screen is on.{' '}
              </div>
              <div>
                <b>Body:</b> No scratches visible from an arms length distance.{' '}
              </div>
              <div>
                <b>Battery:</b> Great health that exceeds{' '}
                <a onClick={() => actions.setCurrentModal(Modals.battery)}>80% capacity</a> relative
                to new. No fast drain.{' '}
              </div>
            </div>
          )}
          {selectedAppearanceName === 'Fair' && (
            <div className={classnames(styles.MarginTop, styles.Tiny, styles.ColorSubtext)}>
              <div>
                <b>Screen:</b> Moderate scratches that are not visible when the screen is on.{' '}
              </div>
              <div>
                <b>Body:</b> May have visible signs of use, with small scratches that can be felt.{' '}
              </div>
              <div>
                <b>Battery:</b> Great health that exceeds{' '}
                <a onClick={() => actions.setCurrentModal(Modals.battery)}>80% capacity</a> relative
                to new. No fast drain.{' '}
              </div>
            </div>
          )}
        </div>

        {isMobile && (
          <div className={styles.sectionWithBackground}>
            <div className={classnames(styles.FlexCenter, styles.MarginBottom)}>
              <Shield className={styles.MarginHalfRight} />
              {t('12MonthWarrantyIncluded')}
              <span onClick={() => actions.setCurrentModal(Modals.warranty)}>
                <Info className={css.infoIcon} />
              </span>
            </div>
            <div className={classnames(styles.FlexCenter, styles.MarginBottom)}>
              <Truck className={styles.MarginHalfRight} />

              <span>
                <b className={styles.ColorAccent}>Free</b>
                <span> shipping and 30-day returns</span>
                <span onClick={() => actions.setCurrentModal(Modals.freeShipping)}>
                  <Info className={css.infoIcon} />
                </span>
              </span>
            </div>
            <div className={styles.FlexCenter}>
              <Paypal size={18} className={styles.MarginHalfRight} />

              <span>{t('payPalBuyerProtection')}</span>
            </div>
          </div>
        )}

        {/*

        <div className={styles.sectionWithBackground}>
          <div className={css.affirmFirstLine}>
            <div className={styles.sectionTitle}>
              {t('payInInstallments')}
              <Info className={css.infoIcon} />
            </div>
            <img src="/static/incredible/affirm.svg" />
          </div>
          <div className={css.affirmSecondLine}>
            <div>
              {t('displayOnlyAffirm')}
              <span> </span> <a>{t('learnMore')}</a>
            </div>
            <div className={styles.MarginLeft}>
              <Checkbox toggle={true} />
            </div>
          </div>
        </div>
        */}
      </div>
      {isShowCarriersModal && (
        <AppearanceModal
          variant={variant}
          appearances={allAttributes.appearance}
          minimumPricePerCondition={minimumPricePerCondition}
          selectedTags={tags}
          onChange={value => setSelections('appearance', value)}
          onClose={() => setIsShowCarriersModal(false)}
        />
      )}
    </>
  );
}

export default withAppConsumer(translate(['home', 'common'])(PurchaseBox));

import React, {useState, useMemo, useEffect, useRef} from 'react';
import {translate} from 'react-i18next';
import {withAppConsumer} from '@Components/app-context';
import {Grid} from 'semantic-ui-react';
import {Button, Transition} from 'semantic-ui-react';
import Router from 'next/router';
import Price from './price';
import CarrierSelector from './carrier-selector';
import Tag from './tag';
import ChatLink from '../display/chat-link';
import DeliveryDate from '../display/delivery-date';
import Stars from '../display/stars';
import TrustPilotReviews from './trustpilot-reviews';
import classnames from 'classnames';
import {useSwipeable} from 'react-swipeable';
import AccessoryProductsModal from '../modals/accessory-products-modal';
import {
  Info,
  Shield,
  Truck,
  CreditCards,
  Ribbon,
  LockClosed,
} from '../../../widget/components/display/svg-icons';
import PurchaseBox from './purchase-box';

import styles from '@Common/styles/styles.scss';
import css from './sticky-buy.scss';

const Margin = 20;

function StickyBuy({
  t,
  limitId = 'start_footer',
  onBuy,
  isBuying,
  product,
  actions,
  api,
  app,
  tags,
  cart,
  longName,
  selections,
  setSelections,
  isOpenCarriersModal,
  setIsOpenCarriersModal,
  isCategory,
  context: {isTablet},
}) {
  const ref = useRef(null);
  const coors = useRef(null);
  const limit = useRef(null);
  const lastScrollY = useRef(null);
  const [style, setStyle] = useState({});

  /*
  const update = () => {
    if (lastScrollY.current === window.scrollY) {
      return;
    }
    lastScrollY.current = window.scrollY;
    const st = {...style};
    if (window.scrollY <= coors.current.top - Margin) {
      st.position = 'static';
    } else {
      // TODO: can we simplify this
      const limitRect = limit.current.getBoundingClientRect();
      st.position = 'fixed';
      st.top = `${Math.min(Margin, limitRect.top - coors.current.height - Margin * 2)}px`;
      st.height = `${coors.current.height}px`;
      st.width = `${coors.current.width}px`;
    }
    setStyle(st);
  };

  const calculate = () => {
    ref.current.style.position = 'static';
    const rect = ref.current.getBoundingClientRect();

    coors.current = {
      top: rect.top + window.scrollY,
      left: rect.left,
      height: rect.height,
      width: rect.width,
    };
    update();
  };

  useEffect(() => {
    limit.current = document.getElementById(limitId);
  }, [limitId]);

  // Initial measurements
  useEffect(() => {
    calculate();
  }, [ref.current, product.defaultVersion]);

  // Set update triggers
  useEffect(() => {
    window.addEventListener('scroll', update);
    window.addEventListener('resize', calculate);
    window.addEventListener('load', calculate);

    return () => {
      window.removeEventListener('scroll', update);
      window.removeEventListener('resize', calculate);
      window.removeEventListener('load', calculate);
    };
  }, []);
  */

  const [showModal, setShowModal] = useState(false);

  const {price, original_price, stock} = product.defaultVersion?.variant || {};
  const percent = price && original_price ? 100 - (price * 100) / original_price : null;
  const deliveryDate = (
    <div className={styles.MarginTop}>
      <DeliveryDate
        seller_id={product.defaultVersion?.variant?.owner}
        shippingMethodsBySeller={cart.shippingMethodsBySeller}
      />
    </div>
  );
  return (
    <>
      <div style={style} ref={ref}>
        <div
          className={classnames(styles.MarginHalfBottom, {
            [styles.FlexCenter]: !isTablet,
          })}
        >
          <div style={{width: isTablet ? '100%' : '50%', minWidth: 350}}>
            <ChatLink />
          </div>

          <div
            className={classnames({
              [styles.MarginLeft]: !isTablet,
              [styles.MarginDoubleTop]: isTablet,
            })}
            style={{transform: 'translateY(-7px)'}}
          >
            <Stars />
          </div>
        </div>
        <div className={classnames(styles.FlexSpaceBetween, styles.FlexTop)}>
          <div className={classnames(styles.fluid, styles.MarginDoubleBottom, styles.MarginRight)}>
            <span className={styles.Huge}>{longName}</span>
            {isTablet && deliveryDate}
            <CarrierSelector
              selections={selections}
              allCarriers={app.carriers}
              carriers={product.defaultVersion.tags?.filter(it => it.type === 'carrier')}
              onlySecondLine={true}
              api={api}
              category_id={product.id}
              isOpen={isOpenCarriersModal}
              setIsOpen={setIsOpenCarriersModal}
            />
          </div>
          <div className={css.priceHolder}>
            {price && stock ? <Price value={price} className={styles.Huge} /> : t('outOfStock')}
            {original_price && (
              <div className={css.originalPrice}>
                {t('Original Price')}: <Price value={original_price} className={css.struk} />
              </div>
            )}
            {!isTablet && deliveryDate}
          </div>
        </div>

        {!isCategory && (
          <>
            <div className={css.block}>
              <div>
                <PurchaseBox
                  isOpenCarriersModal={isOpenCarriersModal}
                  setIsOpenCarriersModal={setIsOpenCarriersModal}
                  onBuy={onBuy}
                  loadingPricePerCondition={product.loadingPricePerCondition}
                  isBuying={isBuying}
                  actions={actions}
                  api={api}
                  accessoryProducts={product.accessoryProducts}
                  allCarriers={app.carriers}
                  product={product.product}
                  tags={tags}
                  cart={cart}
                  selections={selections}
                  variant={product.defaultVersion.variant}
                  currentAttributes={product.defaultVersion.tags}
                  allAttributes={product.allAttributes}
                  minimumPricePerCondition={product.minimumPricePerCondition}
                  setSelections={setSelections}
                  setIsOpenTradeIn={actions.setIsOpenTradeIn}
                />

                <div>
                  <Button
                    fluid
                    primary
                    disabled={
                      !product.defaultVersion?.variant?.stock ||
                      !product.defaultVersion?.variant?.price
                    }
                    onClick={() => {
                      onBuy(false);
                      setShowModal(true);
                    }}
                    loading={isBuying}
                  >
                    <LockClosed color="white" />{' '}
                    <span className={styles.MarginHalfLeft}>{t('addToCart')}</span>
                  </Button>
                </div>
              </div>
            </div>
            <TrustPilotReviews />
          </>
        )}
      </div>
      {showModal && (
        <AccessoryProductsModal
          loadingAccessoryProducts={product.loadingAccessoryProducts}
          product={product}
          cart={cart}
          api={api}
          onClose={() => setShowModal(false)}
        />
      )}
    </>
  );
}

export default withAppConsumer(translate(['home', 'common'])(StickyBuy));

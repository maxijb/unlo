import React from 'react';
import {DefaultCurrency} from '@Common/constants/app';
import {translate} from 'react-i18next';
import classnames from 'classnames';

import css from './tag-selector.scss';

function TagsSelector({
  selectedTags,
  tags = [],
  t,
  onChange,
  onClickSelected = () => {},

  vertical = false,
  padded = false,
  name = null,
  className,
}) {
  return (
    <div
      className={classnames(css.tagsHolder, className, {
        [css.vertical]: vertical,
      })}
    >
      {tags.map((tag, i) => (
        <div
          key={i}
          className={classnames(css.tag, {
            [css.disabled]: tag.disabled,
            [css.selected]: selectedTags.has(tag.id),
            [css.padded]: padded,
          })}
          onClick={() => {
            if (!tag.disabled && !selectedTags.has(tag.id)) {
              onChange(tag.id, name);
            } else if (selectedTags.has(tag.id)) {
              onClickSelected();
            }
          }}
        >
          {tag.label && <div className={css.label}>{tag.label}</div>}
          <div>{tag.content || tag.name}</div>
        </div>
      ))}
    </div>
  );
}

export default translate(['home', 'common'])(TagsSelector);

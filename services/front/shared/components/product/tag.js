import React from 'react';
import classnames from 'classnames';

import css from './tag.scss';

function Tag({text, className}) {
  return <div className={classnames(css.tag, className)}>{text}</div>;
}

export default Tag;

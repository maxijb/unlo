import React from 'react';
import Listing from './listing';
import {translate} from 'react-i18next';
import classnames from 'classnames';
import {withAppConsumer} from '../app-context';
import {OKInspection} from '../../../widget/components/display/svg-icons';

import styles from '@Common/styles/styles.scss';
import css from './technical-specs.scss';

const InspectionItems = [
  'Network connection',
  'Battery capacity (80%+)',
  'Front and rear cameras work',
  'Buttons work',
  'No oxidation',
  'Screen is bright',
  'Vibration mechanism works',
  'SIM reader works',
  'Clean IMEI',
  'Fully sanitized',
  'Not blacklisted',
  'Factory reset',
  'Not financed',
  'Flash works',
  'Not lost or stolen',
  'TouchID / FaceID works',
  'Recognizes a charger',
  'Speakers work',
  'Microphone works',
  'WiFi and Bluetooth work',
  'Proximity sensor works',
];

function TechnicalInspection({t, centered = false}) {
  return (
    <>
      <div className={classnames(styles.MarginBottom, {[styles.Centered]: centered})}>
        {t('inspectionLong')}
      </div>
      <div className={classnames(css.line, styles.MarginTop)}>
        <div>{InspectionItems.map(item)}</div>
      </div>
    </>
  );
}

function item(it) {
  return (
    <div className={css.item} key={it}>
      <span>
        <OKInspection />
      </span>
      <span className={css.label}>{it}</span>
    </div>
  );
}

export default withAppConsumer(translate(['home', 'common'])(TechnicalInspection));

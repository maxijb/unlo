import React from 'react';
import Listing from './listing';
import {translate} from 'react-i18next';
import classnames from 'classnames';
import {withAppConsumer} from '../app-context';

import styles from '@Common/styles/styles.scss';
import css from './technical-specs.scss';

function TechnicalInspection({product, t}) {
  return (
    <>
      <div className={classnames(styles.MarginBottom, styles.ColorSubtext)}>
        <div className={css.lineSpec}>
          <table>
            {product?.specs?.map(({name, value}) => (
              <tr>
                <td className={classnames(css.tableName, css.td)}>{name}</td>
                <td className={classnames(styles.Tiny, css.td)}>{value}</td>
              </tr>
            ))}
          </table>
        </div>
      </div>
    </>
  );
}

export default withAppConsumer(translate(['home', 'common'])(TechnicalInspection));

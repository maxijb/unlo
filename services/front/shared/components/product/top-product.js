import React, {useMemo} from 'react';
import Link from 'next/link';
import Listing from './listing';
import {translate} from 'react-i18next';
import classnames from 'classnames';
import Price from './price';
import CompetitorPrices from './competitor-prices';
import {withAppConsumer} from '../app-context';
import Tag from './tag';
import Image from '../images/image';
import {getPageLink} from '@Common/utils/urls';
import {
  consolidateAttributesAsName,
  getProductNameWithAttributes,
  findRelevantImages,
  getTagsSet,
} from '../../utils/attributes-utils';

import styles from '@Common/styles/styles.scss';
import css from './highlighted-products.scss';

function TopProduct({product, t, context, actions}) {
  const {attrs, name, images} = useMemo(() => {
    const data = consolidateAttributesAsName(product.defaultVersion?.tags, t);
    const longName = getProductNameWithAttributes(product.product.name, data, t);
    return {
      attrs: data,
      name: longName,
      images: findRelevantImages(product.allImages, getTagsSet(product.defaultVersion?.tags)),
    };
  }, product);

  if (!product.defaultVersion?.variant) {
    return null;
  }

  const image = product.imageMap[images[0]?.img_id];

  const {price, original_price} = product.defaultVersion?.variant;
  const percent = 100 - (price * 100) / original_price;

  return (
    <>
      <Link
        prefetch
        href={`/product?id=${product.product.id}`}
        as={getPageLink('product', {product: product.product, t})}
      >
        <div
          className={classnames(styles.FlexCenter, styles.MarginDoubleBottom, styles.CursorPointer)}
        >
          {image && <Image size={180} file={image} className={styles.MarginDoubleBottom} />}
          <div className={styles.MarginDoubleBottom}>
            <div
              className={classnames(
                styles.ColorNegative,
                styles.MediumBold,
                styles.MarginHalfBottom,
              )}
            >
              {t('ourBestDeal')}
            </div>
            <div className={classnames(styles.Huge, styles.MarginHalfBottom)}>{name}</div>
            <div className={classnames(styles.MarginHalfBottom, styles.FlexCenter)}>
              <Price value={price} className={styles.Large} />
              <Tag
                className={styles.MarginLeft}
                text={
                  <span>
                    <span>{t('save')}</span>
                    <Price value={original_price - price} />
                    <span> ({Math.round(percent)}%)</span>
                  </span>
                }
              />
            </div>
            <div>
              {t('Orig')}: <Price value={original_price} />
            </div>
          </div>
        </div>
      </Link>
      <div className={styles.MarginDoubleTop}>
        <CompetitorPrices
          actions={actions}
          product={product}
          name={product.product.name}
          competitorPrices={product.competitorPrices}
          cheapestOwnPrice={product.cheapestOwnPrice}
          cheapestCapacity={product.cheapestCapacity}
          showImage={false}
        />
      </div>
    </>
  );
}

export default withAppConsumer(translate(['home', 'common'])(TopProduct));

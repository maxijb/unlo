import React, {useState, useEffect} from 'react';
import {withAppConsumer} from '../app-context';
import Link from 'next/link';
import classnames from 'classnames';
import {translate} from 'react-i18next';
import isEmpty from 'is-empty';
import Router from 'next/router';
import {Button, Header, Image, Modal, Form, Transition, Icon, Divider} from 'semantic-ui-react';

import Logger from '@Common/logger';
import TagsSelector from './tag-selector';
import Input from '../inputs/wrapped-input';
import Dropdown from '../inputs/wrapped-dropdown';
import Price, {textPrice} from './price';

import GenericModal from '@Components/modals/generic-modal';

import css from './tradein-dialog.scss';
import styles from '@Common/styles/styles.scss';

const ConditionLines = {
  broken: [1, 2, 3, 4, 5, 6, 7],
  fair: [1, 2, 3, 4, 5],
  good: [1, 2, 3, 4, 5],
  like_new: [1, 2, 3, 4],
};

function TradeInModal(props) {
  const {
    isOpen,
    errors = {},
    closeModal,
    t,
    api,
    cart,
    context: {GA},
  } = props;
  const [models, setModels] = useState([]);
  const [carriers, setCarriers] = useState([]);
  const [capacities, setCapacities] = useState([]);
  const [conditions, setConditions] = useState([]);
  const [isSetting, setIsSetting] = useState(false);
  const [selections, setSelections] = useState(
    cart.tradeInItems[0]?.selections || {condition: 'fair'},
  );
  const [price, setPrice] = useState(null);

  const setTradeIn = async () => {
    setIsSetting(true);
    GA.event({
      category: 'trade-in',
      action: 'accept-trade-in',
      label: selections?.model || 'model',
    });
    await api.cart.setTradeIn({
      selections,
      price,
      id: cart.tradeInItems[0]?.id || null,
    });
    setIsSetting(false);
    closeModal();
  };

  useEffect(() => {
    api.app.getTradeInOptions({type: 'model'}).then(res => {
      if (res?.resp?.options) {
        setModels(res.resp.options.map(a => ({text: a, value: a})));
      }
    });
    api.app.getTradeInOptions({type: 'condition'}).then(res => {
      if (res?.resp?.options) {
        setConditions(res.resp.options.map(a => ({id: a, content: t(a)})));
      }
    });
    if (selections.model) {
      api.app.getTradeInOptions({type: 'capacity', ...selections}).then(res => {
        if (res?.resp?.options) {
          setCapacities(res.resp.options.map(a => ({text: a, value: a})));
        }
      });
    }
    if (selections.capacity) {
      api.app.getTradeInOptions({type: 'carrier', ...selections}).then(res => {
        if (res?.resp?.options) {
          setCarriers(res.resp.options.map(a => ({text: a, value: a})));
        }
      });
    }
    if (selections.carrier && selections.condition && selections.capacity && selections.model) {
      api.app.getTradeInPrediction(selections).then(res => setPrice(res.resp.price));
    }
  }, []);

  const select = async (value, field) => {
    GA.event({
      category: 'trade-in',
      action: 'select-trade-in',
      label: field,
    });
    selections[field] = value;
    if (field === 'model') {
      selections.carrier = null;
      selections.capacity = null;
      selections.model = value;
      setCapacities([]);
      setCarriers([]);
      setPrice(null);
      api.app.getTradeInOptions({type: 'capacity', ...selections}).then(res => {
        if (res?.resp?.options) {
          setCapacities(res.resp.options.map(a => ({text: a, value: a})));
        }
      });
    }
    if (field === 'capacity') {
      selections.carrier = null;
      selections.capacity = value;
      setCarriers([]);
      setPrice(null);
      api.app.getTradeInOptions({type: 'carrier', ...selections}).then(res => {
        if (res?.resp?.options) {
          setCarriers(res.resp.options.map(a => ({text: a, value: a})));
        }
      });
    }

    if (field === 'carrier' || field === 'condition') {
      selections[field] = value;
    }

    if (selections.carrier && selections.condition) {
      api.app.getTradeInPrediction(selections).then(res => setPrice(res.resp.price));
    }

    setSelections({...selections});
  };

  return (
    <GenericModal onClose={closeModal} open={isOpen} size="tiny">
      <Modal.Header>
        <div className={classnames(styles.MediumBold, styles.centered)}>
          {t('eachCashOnATradeIn')}
        </div>
      </Modal.Header>
      <Modal.Content>
        <Dropdown
          name="model"
          options={models}
          fluid
          selection
          search
          onChange={select}
          className={classnames(styles.MarginBottom, styles.Medium)}
          placeholder={t('enterModel')}
          value={selections.model || ''}
        />
        <Dropdown
          name="capacity"
          options={capacities}
          disabled={!capacities.length}
          fluid
          selection
          search
          onChange={select}
          className={classnames(styles.MarginBottom, styles.Medium)}
          placeholder={t('enterCapacity')}
          value={selections.capacity || ''}
        />
        <Dropdown
          name="carrier"
          options={carriers}
          disabled={!carriers.length}
          fluid
          selection
          search
          onChange={select}
          className={classnames(styles.MarginBottom, styles.Medium)}
          placeholder={t('enterCarrier')}
          value={selections.carrier || ''}
        />
        <div className={classnames(styles.MediumBold, styles.MarginBottom)}>
          {t('selectCondition')}
        </div>
        <TagsSelector
          tags={conditions}
          selectedTags={new Set([selections.condition])}
          onChange={select}
          name="condition"
          className={styles.MarginDoubleBottom}
        />
        <div className={styles.sectionWithBackground}>
          <ul>
            {(ConditionLines[selections.condition] || []).map(i => (
              <li key={i} className={css.usps}>
                {t(`tradeInUSPS-${i}`, {context: selections.condition})}
              </li>
            ))}
          </ul>
        </div>
        {price !== null && (
          <div className={styles.MarginDoubleBottom}>
            <div className={styles.MediumBold}>{t('yourPhoneIsValuedAt')}</div>
            <div className={css.price}>
              <Price value={price} />
            </div>
            <div className={styles.Tiny}>
              {t('tradeInCashExplanation', {price: textPrice({value: price})})}
            </div>
          </div>
        )}
        <div className={styles.MarginBottom}>
          <Button fluid primary disabled={!price} onClick={setTradeIn} loading={isSetting}>
            {t('acceptMyTradeInOffer')}
          </Button>
        </div>
        <div className={classnames(styles.MarginBottom, styles.centered)}>
          <a className={styles.SecondaryLink} onClick={closeModal}>
            {t('noThanksPass')}
          </a>
        </div>
      </Modal.Content>
    </GenericModal>
  );
}

export default withAppConsumer(translate(['home'])(TradeInModal));

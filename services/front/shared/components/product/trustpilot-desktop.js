import React from 'react';
import classnames from 'classnames';
import {translate} from 'react-i18next';
import {DefaultCurrency} from '@Common/constants/app';
import {withAppConsumer} from '../app-context';

import styles from '@Common/styles/styles.scss';
import css from './trustpilot.scss';

function TrustPilot({t, className, context: {GA}}) {
  return (
    <div className={classnames(css.box, className)}>
      <div className={css.desktopTrustpilot}>
        <img src="/static/incredible/trustpilot-rating.png" alt="Trustpilot" />

        <div className={css.reviews}>
          <b>4.8</b> out of 5 on{' '}
          <a
            href="https://www.trustpilot.com/review/incrediblephones.com"
            rel="noopener noreferrer"
            target="blank"
            onClick={() => {
              GA.event({
                category: 'navigation',
                action: 'trustpilot-click',
              });
            }}
          >
            TrustPilot
          </a>{' '}
        </div>
      </div>
    </div>
  );
}

export default withAppConsumer(translate(['home'])(TrustPilot));

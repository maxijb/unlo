import React, {useState} from 'react';
import classnames from 'classnames';
import {translate} from 'react-i18next';
import {DefaultCurrency} from '@Common/constants/app';
import {withAppConsumer} from '../app-context';
import {Grid} from 'semantic-ui-react';
import Player from 'react-player';
import {PlayVideo} from '../../../widget/components/display/svg-icons';
import Stars from '@Components/display/stars';

import styles from '@Common/styles/styles.scss';
import css from './trustpilot.scss';

const Reviews = [
  {
    comment: 'I would definitely recommend Incredible to my friends.',
    name: 'David D.',
    image: '/static/reviews/david.png',
    video: 'RXtl0oSfyFY',
  },
  {
    comment:
      'I would never hesitate to buy from Incredible. They went above and beyond to make sure the iPhone was delivered safely and in a timely manner.',
    name: 'Dilasha N.',
    trustpilotRating: '5.0',
  },
  {
    comment: 'This truly is a reliable source to get your phone at.',
    name: 'Alanis',
    image: '/static/reviews/alanis.png',
    video: 'mQlGn2BPp4o',
  },
  {
    comment:
      "Outstanding, friendly service and an excellent product. They did everything they could to make sure I was happy. I can't recommend these folks highly enough.",
    name: 'T Sheridan',
    trustpilotRating: '5.0',
  },
  {
    comment: 'This is the second phone I got from Incredible.',
    name: 'Mykel',
    image: '/static/reviews/mykel.png',
    video: 'd7NEZPv0tAQ',
  },
];

function TrustPilot({t, className, singleColumn = false, context: {GA, isMobile}}) {
  const [videos, setVideos] = useState(new Set());

  return (
    <div className={classnames(className)}>
      <div className={classnames(css.desktopTrustpilot, styles.MarginBottom)}>
        <div
          className={classnames(css.reviews, {
            [css.singleColumn]: singleColumn,
          })}
        >
          {singleColumn && (
            <>
              <div className={styles.Huge}>The (Almost) 5 Star Phone Marketplace</div>
              <Stars
                size={30}
                includeLabel={false}
                className={classnames(styles.MarginHalfTop, styles.MarginHalfBottom)}
              />
            </>
          )}
          Incredible is rated <b>4.8</b> out of 5 on{' '}
          <a
            href="https://www.trustpilot.com/review/incrediblephones.com"
            rel="noopener noreferrer"
            target="blank"
            onClick={() => {
              GA.event({
                category: 'navigation',
                action: 'trustpilot-click',
              });
            }}
          >
            TrustPilot
          </a>{' '}
        </div>
      </div>
      <div
        className={classnames(css.reviews, {
          [css.singleColumn]: singleColumn,
        })}
      >
        {Reviews.slice(0, singleColumn ? 2 : Reviews.length).map((review, i) => (
          <div
            className={classnames(css.review, {
              [css.white]: singleColumn,
            })}
            key={i}
          >
            {review.image && !videos.has(i) && (
              <div
                className={css.image}
                onClick={() => {
                  videos.add(i);
                  setVideos(new Set(videos));
                }}
              >
                <PlayVideo className={css.play} />
                <img src={review.image} alt="Review" />
              </div>
            )}
            {review.image && videos.has(i) && (
              <div className={css.image}>
                <Player
                  url={`https://www.youtube.com/watch?v=${review.video}?rel=0`}
                  playing={true}
                  height={221}
                  width={450}
                  style={{
                    maxWidth: '100%',
                    margin: '0 auto',
                  }}
                  onEnded={() => {
                    videos.delete(i);
                    setVideos(new Set(videos));
                  }}
                />
              </div>
            )}
            <div className={css.comment}>"{review.comment}"</div>
            <div className={classnames(styles.FlexSpaceBetween, css.reviewData)}>
              <img src="/static/incredible/trustpilot-rating.png" />
              <span>by {review.name}</span>
            </div>
            {review.trustpilotRating && (
              <div className={classnames(styles.FlexSpaceBetween, css.reviewData)}>
                <span>Rated {review.trustpilotRating}</span>
                <img className={css.logo} src="/static/incredible/trustpilot.png" />
              </div>
            )}
          </div>
        ))}
      </div>
    </div>
  );
}

export default withAppConsumer(translate(['home'])(TrustPilot));

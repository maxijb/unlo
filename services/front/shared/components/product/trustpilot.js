import React from 'react';
import classnames from 'classnames';
import {translate} from 'react-i18next';
import {DefaultCurrency} from '@Common/constants/app';
import {withAppConsumer} from '../app-context';

import css from './trustpilot.scss';

function TrustPilot({t, className, context: {GA}}) {
  return (
    <div className={classnames(css.box, className)}>
      <div className={css.number}>4.8</div>
      <div className={css.right}>
        <div>{t('excellent')}</div>
        <div>
          <img src="/static/incredible/trustpilot-rating.png" alt="Trustpilot" />
        </div>
        <div className={css.reviews}>
          <a
            href="https://www.trustpilot.com/review/incrediblephones.com"
            rel="noopener noreferrer"
            target="blank"
            onClick={() => {
              GA.event({
                category: 'navigation',
                action: 'trustpilot-click',
              });
            }}
          >
            TrustPilot
          </a>{' '}
          {t('reviews')}
        </div>
      </div>
    </div>
  );
}

export default withAppConsumer(translate(['home'])(TrustPilot));

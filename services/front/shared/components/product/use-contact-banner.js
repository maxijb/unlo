import React, {useEffect, useState} from 'react';
import {ContactCaptureCookieName} from '@Common/constants/app';

let timeout = null;
export default function UseContactBanner(appState, userState, actions, context) {
  if (!process.browser) {
    return;
  }
  const trigger = () => {
    document.cookie = `${ContactCaptureCookieName}=1; max-age=${3600 * 24}`;
    actions.showContactModal();
    document.onmouseleave = null;
  };

  const unset = () => {
    document.onmouseleave = null;
    clearTimeout(timeout);
  };
  useEffect(() => {
    unset();
    if (!userState?.id && !appState.shownContactModal) {
      if (!context.isMobile && !context.isTablet) {
        document.onmouseleave = trigger;
      } else {
        timeout = setTimeout(trigger, 8000);
      }
    }
    return unset;
  }, [context.isMobile, context.isTablet]);
}

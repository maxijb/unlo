import React from 'react';

export default function () {
  return (
    <footer class="text-center-xs space--xs">
      <div class="container">
        <div class="row">
          <div class="col-sm-7">
            <span class="type--fine-print">
              © <span class="update-year" /> Wiri Inc.
            </span>{' '}
            <a class="type--fine-print" href="cookie-policy.html">
              Cookie Policy
            </a>
          </div>
          <div class="col-sm-5 text-right text-center-xs">
            <ul class="social-list list-inline list--hover">
              <li>
                <a href="https:/twitter.com/getwiri">
                  <i class="socicon socicon-twitter icon icon--xs" />
                </a>
              </li>
              <li>
                <a href="https://facebook.com/getwiri">
                  <i class="socicon socicon-facebook icon icon--xs" />
                </a>
              </li>
              <li>
                <a href="https://instagram.com/getwiri">
                  <i class="socicon socicon-instagram icon icon--xs" />
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  );
}

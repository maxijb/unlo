import React from 'react';
import {cdnURL} from '@Common/utils/statif-assets-utils';
import Head from 'next/head';

export default function () {
  return (
    <div class="nav-container">
      <Head>
        <link href="/static/landing/css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link
          href="/static/landing/css/stack-interface.css"
          rel="stylesheet"
          type="text/css"
          media="all"
        />
        <link
          href="/static/landing/css/bootstrap.css"
          rel="stylesheet"
          type="text/css"
          media="all"
        />
        <link href="/static/landing/css/theme.css" rel="stylesheet" type="text/css" media="all" />
        <link
          href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400,700&display=swap"
          rel="stylesheet"
        />
        <link
          href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700"
          rel="stylesheet"
        />
      </Head>

      <div class="bar bar--sm visible-xs">
        <div class="container">
          <div class="row">
            <div class="col-3 col-md-2">
              <a href="/">
                {' '}
                <img
                  class="logo"
                  alt="Wiri Logo"
                  src={cdnURL('/static/landing/images/wiri-logo.svg')}
                />
              </a>
            </div>
            <div class="col-9 col-md-10 text-right">
              <a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs hidden-sm">
                {' '}
                <i class="icon icon--sm stack-interface stack-menu" />{' '}
              </a>
            </div>
          </div>
        </div>
      </div>
      <nav id="menu1" class="bar bar-1 hidden-xs">
        <div class="container">
          <div class="row">
            <div class="col-lg-1 col-md-2 hidden-xs">
              <div class="bar__module">
                <a href="/">
                  {' '}
                  <img
                    class="logo"
                    alt="Wiri Logo"
                    src={cdnURL('/static/landing/images/wiri-logo.svg')}
                  />
                </a>
              </div>
            </div>
            <div class="col-lg-11 col-md-12 text-right text-left-xs text-left-sm">
              <div class="bar__module">
                <ul class="menu-horizontal text-center">
                  <li>
                    {' '}
                    <a href="#">Why Wiri</a>{' '}
                  </li>
                  <li>
                    {' '}
                    <a href="#">Features</a>{' '}
                  </li>
                </ul>
              </div>
              <div class="bar__module">
                <a class="btn btn--sm btn--primary-2" href="#customise-template">
                  {' '}
                  <span class="btn__text">Log in</span>{' '}
                </a>
                <a class="btn btn--sm btn--primary" href="#purchase-template">
                  {' '}
                  <span class="btn__text">Get Started</span>{' '}
                </a>
              </div>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
}

import React from 'react';
import {translate} from 'react-i18next';
import {Button} from 'semantic-ui-react';
import classnames from 'classnames';
import {textPrice} from '../product/price';
import {RightChevron} from '../../../widget/components/display/svg-icons';

import styles from '@Common/styles/styles.scss';

function TradeInSection({cart, setIsOpenTradeIn, t}) {
  if (!cart.tradeInItems?.length) {
    return (
      <div className={styles.sectionWithBackground} style={{margin: '16px 0 0 0'}}>
        <div className={styles.sectionTitle}>{t('earnCashOldPhone')}</div>
        <div>
          <Button fluid color="black" onClick={() => setIsOpenTradeIn(true)}>
            {t('checkTradeInPrices')}
          </Button>
        </div>
      </div>
    );
  }

  return (
    <div className={styles.sectionWithBackground} style={{margin: '16px 0 0 0'}}>
      <div className={classnames(styles.sectionTitle, styles.MarginHalfBottom)}>
        {t('earnOnTradeIn', {amount: textPrice({value: cart.tradeInItems[0].price})})}
      </div>
      <div className={classnames(styles.ColorSubtext, styles.Small, styles.FlexSpaceBetween)}>
        {t('tradeInWaste')}
        <div className={styles.circularButton} onClick={() => setIsOpenTradeIn(true)}>
          <RightChevron color="white" size={30} />
        </div>
      </div>
    </div>
  );
}

export default translate(['home'])(TradeInSection);

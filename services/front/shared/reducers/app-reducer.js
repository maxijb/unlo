import {rpcIds} from '../actions/api-actions';
import {actionTypes} from '../actions/app-actions';
import {Modals} from '@Common/constants/app';
import {handleActions} from 'redux-actions';
import reduceReducers from 'reduce-reducers';
import {createRPCReducer} from '../utils/reducer-utils';
import {arrayToMap} from '@Common/utils/generic-utils';

export const initialState = {
  timezones: [],
  carriers: [],
  menuItems: [],
  visibleLeaves: [],
  visibleLeavesMap: [],
  menuItemsMap: {},
  highlightedProducts: {
    isLoading: false,
    list: [],
  },
  publicKeys: {},
  currentModal: null,
};

export default reduceReducers(
  (state, action) => state || initialState,

  createRPCReducer(rpcIds['app.getPublicKeys'], {
    success: (state, action) => {
      return {...state, publicKeys: action.payload.resp};
    },
  }),
  createRPCReducer(rpcIds['app.getTimezonesList'], {
    success: (state, action) => {
      return {...state, timezones: action.payload.resp};
    },
  }),

  createRPCReducer(rpcIds['app.getCarriers'], {
    success: (state, action) => {
      return {...state, carriers: action.payload.resp};
    },
  }),

  createRPCReducer(rpcIds['app.getMenuItems'], {
    success: (state, action) => {
      return {
        ...state,
        menuItems: action.payload.resp,
        menuItemsMap: arrayToMap(action.payload.resp),
      };
    },
  }),

  createRPCReducer(rpcIds['app.findAllVisibleLeaves'], {
    success: (state, action) => {
      return {
        ...state,
        visibleLeaves: action.payload.resp,
        visibleLeavesMap: arrayToMap(action.payload.resp),
      };
    },
  }),

  createRPCReducer(rpcIds['app.getHighlightedProducts'], {
    start: (state, action) => {
      return {
        ...state,
        highlightedProducts: {
          isLoading: true,
          list: [],
        },
      };
    },
    success: (state, action) => {
      const resp = action.payload.resp;
      return {
        ...state,
        highlightedProducts: {
          isLoading: false,
          list: resp.products,
        },
      };
    },
    failure: (state, action) => {
      return {
        ...state,
        highlightedProducts: {
          isLoading: false,
          list: [],
        },
      };
    },
  }),

  handleActions(
    {
      [actionTypes.openSignupModal]: (state, action) => {
        return {...state, openedSignupModal: true, openedSigninModal: false};
      },
      [actionTypes.closeSignupModal]: (state, action) => {
        return {...state, openedSignupModal: false};
      },
      [actionTypes.openSigninModal]: (state, action) => {
        return {...state, openedSigninModal: true, openedSignupModal: false};
      },
      [actionTypes.closeSigninModal]: (state, action) => {
        return {...state, openedSigninModal: false};
      },
      [actionTypes.setPromoCodeModal]: (state, action) => {
        return {...state, currentModal: Modals.promoCode, promoCode: action.payload.promoCode};
      },
      [actionTypes.setCurrentModal]: (state, action) => {
        return {...state, currentModal: action.payload};
      },
      [actionTypes.showContactModal]: (state, action) => {
        if (state.currentModal) {
          return state;
        }
        return {...state, shownContactModal: true, currentModal: Modals.contact};
      },
    },
    initialState,
  ),
);

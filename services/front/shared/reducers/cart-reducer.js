import {rpcIds} from '../actions/api-actions';
import {actionTypes} from '../actions/app-actions';
import {handleActions} from 'redux-actions';
import reduceReducers from 'reduce-reducers';
import {createRPCReducer} from '../utils/reducer-utils';
import {formatState} from '@Common/utils/format-utils';
import {RequestErrorMessage} from '@Common/constants/errors';
import {DefaultShippingMethod, DefaultShippingCharges} from '@Common/constants/app';
import isEmpty from 'is-empty';
import {CartErrorMessage} from '@Common/constants/errors';

export const initialState = {
  items: [],
  loaded: false,
  reference: {},
  address: null,
  shippingMethod: DefaultShippingMethod,
  shippingCharges: DefaultShippingCharges,
  phone: '',
  shippingAddress: null,
  paymentMethod: null,
  loadingTaxRate: false,
  taxRate: null,
  taxRateAddress: null,
  differentShippingAddress: false,
  paymentType: null,
  paymentCard: {
    cardNumber: '',
    expiry: '',
    cvc: '',
  },
  shippingMethodsBySeller: {},
  isOrdering: false,
  orderErrors: [],
  isOpenTradeIn: false,
  tradeInItems: [],
  cartErrors: {},
};

export default reduceReducers(
  (state, action) => state || initialState,

  createRPCReducer(rpcIds['cart.placeOrder'], {
    start: (state, action) => {
      return {...state, isOrdering: true, errors: []};
    },
    success: (state, action) => {
      const {errors = [], ok} = action.payload.resp;
      const noErrors = isEmpty(errors);

      const newState = {
        ...state,
        isOrdering: false,
        orderErrors: errors,
        //shippingMethod: noErrors ? DefaultShippingMethod : state.shippingMethod,
        //shippingCharges: noErrors ? DefaultShippingCharges : state.shippingCharges,
      };

      /*
      if (noErrors) {
        newState.shippingMethod = initialState.shippingMethod;
        newState.shippingCharges = initialState.shippingCharges;
        newState.paymentCard = {...newState.paymentCard, ...initialState.paymentCard};
        newState.items = [];
        newState.tradeInItems = [];
      }
      */

      return newState;
    },
    failure: (state, action) => {
      const newState = {
        ...state,
        isOrdering: false,
        orderErrors: [RequestErrorMessage.unknownError],
      };
      return newState;
    },
  }),
  createRPCReducer(rpcIds['cart.getTaxRateByZipCode'], {
    start: (state, action) => {
      return {...state, loadingTaxRate: true};
    },
    failure: (state, action) => {
      return {
        ...state,
        loadingTaxRate: false,
        errorTaxRate: action.payload.resp?.message?.replace('Bad Request - ', ''),
        taxRateAddress: action.payload.address,
      };
    },
    success: (state, action) => {
      const {
        resp: {taxRate},
        address,
      } = action.payload;

      return {
        ...state,
        taxRateAddress: address,
        loadingTaxRate: false,
        errorTaxRate: null,
        taxRate,
      };
    },
  }),

  createRPCReducer(rpcIds['cart.addForUser'], {
    success: (state, action) => {
      const {
        variant_id,
        attributes,
        resp: {cart_id, quantity},
      } = action.payload;

      let found = false;
      for (const item of state.items) {
        if (item.cart_id === cart_id) {
          item.quantity = quantity;
          found = true;
          break;
        }
      }

      if (!found) {
        state.items.push({quantity, variant_id, cart_id, type: 'product'});
      }

      return {
        ...state,
        items: [...state.items],
      };
    },
  }),

  createRPCReducer(rpcIds['product.loadWithAttributes'], {
    success: (state, action) => {
      const {
        resp: {shippingMethodsBySeller, variant},
      } = action.payload;

      if (!variant || !shippingMethodsBySeller) {
        return state;
      }

      return {
        ...state,
        shippingMethodsBySeller: {
          ...state.shippingMethodsBySeller,
          [variant.owner]: shippingMethodsBySeller,
        },
      };
    },
  }),

  createRPCReducer(rpcIds['product.getMinimumDetails'], {
    success: (state, action) => {
      const {
        resp: {shippingMethodsBySeller, defaultVersion},
      } = action.payload;

      if (!defaultVersion?.variant || !shippingMethodsBySeller) {
        return state;
      }

      return {
        ...state,
        shippingMethodsBySeller: {
          ...state.shippingMethodsBySeller,
          [defaultVersion.variant.owner]: shippingMethodsBySeller,
        },
      };
    },
  }),

  createRPCReducer(rpcIds['cart.setTradeIn'], {
    success: (state, action) => {
      const {
        selections,
        resp: {price, id, order_id},
      } = action.payload;
      return {
        ...state,
        tradeInItems: [{selections, price, id, order_id}],
      };
    },
  }),

  createRPCReducer(rpcIds['cart.getPendingOrderDetails'], {
    success: (state, action) => {
      const {
        resp: {order, address},
      } = action.payload;
      return {
        ...state,
        taxRate: order.tax_rate,
        address: address,
        paymentType: order.payment_type,
      };
    },
  }),

  createRPCReducer(rpcIds['cart.getActiveTradeInForUser'], {
    success: (state, action) => {
      const {
        resp: {item},
      } = action.payload;
      if (!item) {
        return state;
      }
      return {
        ...state,
        tradeInItems: [item],
      };
    },
  }),

  createRPCReducer(rpcIds['cart.changeQuantity'], {
    start: (state, action) => {
      const {cart_id} = action.payload;

      const index = state.items.findIndex(it => it.cart_id === cart_id);
      if (index < 0) {
        return state;
      }
      state.items[index] = {...state.items[index], isLoading: true};

      return {
        ...state,
        items: [...state.items],
      };
    },
    failure: (state, action) => {
      const {cart_id, resp} = action.payload;

      const index = state.items.findIndex(it => it.cart_id === cart_id);
      if (index < 0) {
        return state;
      }
      state.items[index] = {
        ...state.items[index],
        isLoading: false,
        error: resp?.message,
        max_stock: resp?.data?.max_stock,
      };

      if (resp?.data?.max_stock) {
        state.items[index].quantity = resp?.data?.max_stock;
      }

      return {
        ...state,
        items: [...state.items],
      };
    },
    success: (state, action) => {
      const {cart_id, quantity} = action.payload;

      const index = state.items.findIndex(it => it.cart_id === cart_id);
      if (index < 0) {
        return state;
      }
      state.items[index] = {...state.items[index], quantity, isLoading: false, error: null};

      return {
        ...state,
        items: [...state.items],
      };
    },
  }),

  createRPCReducer(rpcIds['cart.removeItem'], {
    start: (state, action) => {
      const {cart_id} = action.payload;

      const index = state.items.findIndex(it => it.cart_id === cart_id);
      if (index < 0) {
        return state;
      }
      state.items[index] = {...state.items[index], isLoading: true};

      return {
        ...state,
        items: [...state.items],
      };
    },
    failure: (state, action) => {
      const {cart_id} = action.payload;

      const index = state.items.findIndex(it => it.cart_id === cart_id);
      if (index < 0) {
        return state;
      }
      state.items[index] = {...state.items[index], isLoading: false};

      return {
        ...state,
        items: [...state.items],
      };
    },
    success: (state, action) => {
      const {
        cart_id,
        resp: {removeSeller, seller_id},
      } = action.payload;

      const index = state.items.findIndex(it => it.cart_id === cart_id);
      if (index < 0) {
        return state;
      }
      state.items.splice(index, 1);

      if (removeSeller) {
        state.items = state.items.filter(it => it.seller_id !== seller_id);
      }

      return {
        ...state,
        items: [...state.items],
      };
    },
  }),

  createRPCReducer(rpcIds['cart.readActiveForUser'], {
    success: (state, action) => {
      const {cart, details, shippingMethodsBySeller} = action.payload.resp;
      let shippingMethod = state.shippingMethod;
      let shippingCharges = state.shippingCharges;
      const items = cart;

      return {
        ...state,
        items,
        reference: details,
        loaded: true,
        shippingMethod,
        shippingCharges,
        shippingMethodsBySeller,
      };
    },
  }),
  createRPCReducer(rpcIds['cart.setShipping'], {
    success: (state, action) => {
      const {shippingMethod, seller_id, cart_id} = action.payload.resp;

      const index = state.items.findIndex(
        it => it.type === 'shipping' && it.seller_id === seller_id,
      );
      if (index !== -1) {
        state.items.splice(index, 1);
      }

      state.items.push({
        seller_id: seller_id,
        type: 'shipping',
        cart_id,
        variant_id: shippingMethod.id,
        price: shippingMethod.price,
      });

      return {
        ...state,
        items: [...state.items],
      };
    },
  }),

  handleActions(
    {
      [actionTypes.setCartErrors]: (state, action) => {
        return {...state, cartErrors: action.payload};
      },
      [actionTypes.resetCart]: (state, action) => {
        return {...state, ...initialState};
      },

      [actionTypes.setUserEmail]: (state, action) => {
        return removeCartError({...state, email: action.payload}, 'email');
      },
      [actionTypes.setUserFirstName]: (state, action) => {
        return removeCartError({...state, firstName: action.payload}, 'firstName');
      },
      [actionTypes.setUserLastName]: (state, action) => {
        return removeCartError({...state, lastName: action.payload}, 'lastName');
      },
      [actionTypes.setUserPhone]: (state, action) => {
        const phone = action.payload;
        return removeCartError({...state, phone}, 'phone');
      },

      [actionTypes.setCartPaymentType]: (state, action) => {
        const paymentType = action.payload.value;
        return removeCartError({...state, paymentType}, 'paymentType');
      },

      [actionTypes.setCartPaymentCard]: (state, action) => {
        const {field, value} = action.payload;
        const paymentCard = {
          ...state.paymentCard,
          [field]: value,
        };
        return removeCartError({...state, paymentCard, paymentCardError: false}, 'cardReady');
      },

      [actionTypes.setCartAddress]: (state, action) => {
        const address = {
          ...action.payload,
          unit: state.address?.unit || null,
        };
        const postal_code = {address};
        let zip = state.paymentCard.zip || address.postal_code || '';
        return removeCartError(
          {
            ...state,
            address,
            paymentCard: {
              ...state.paymentCard,
              zip,
            },
          },
          'address',
        );
      },

      [actionTypes.setCartUnitAddress]: (state, action) => {
        const address = {...(state.address || {})};
        address.unit = action.payload;
        return {...state, address};
      },
      [actionTypes.setCartAddressInput]: (state, action) => {
        const address = {...(state.address || {})};
        const {value, field} = action.payload;
        address[field] = value;
        if (field === 'showTextName') {
          const parts = (value || '').split(' ');
          address.street_number = parts[0];
          address.route = parts.slice(1).join(' ');
          address.country = 'United States';
        }
        if (field === 'administrative_area_level_1') {
          address[field] = formatState(value);
        }
        if (field === 'postal_code' && (value || '').match(/^\d{5}-\d{4}/)) {
          address[field] = value.slice(0, 5);
        }
        return {...state, address};
      },

      [actionTypes.setCartShippingAddress]: (state, action) => {
        const shippingAddress = {
          ...action.payload,
          unit: state.shippingAddress?.unit || null,
        };
        return {...state, shippingAddress};
      },

      [actionTypes.setCartShippingUnitAddress]: (state, action) => {
        const shippingAddress = {...(state.shippingAddress || {})};
        shippingAddress.unit = action.payload;
        return {...state, shippingAddress};
      },

      [actionTypes.setIsDifferentShippingAddress]: (state, action) => {
        return {...state, differentShippingAddress: action.payload};
      },
      [actionTypes.setOrderError]: (state, action) => {
        if (action.payload?.error === CartErrorMessage.outOfStock) {
          const {error, max_stock, variant_id} = action.payload;
          state.items.forEach(item => {
            if (item.variant_id === variant_id) {
              item.error = error;
              item.max_stock = max_stock;
            }
          });
        }
        return {...state, orderErrors: [action.payload], items: [...state.items]};
      },

      [actionTypes.cartAddReference]: (state, action) => {
        const {id} = action.payload;

        return {
          ...state,
          reference: {
            ...state.reference,
            [id]: action.payload,
          },
        };
      },

      [actionTypes.setIsOpenTradeIn]: (state, action) => {
        return {...state, isOpenTradeIn: action.payload};
      },
    },
    initialState,
  ),
);

function removeCartError(state, field) {
  return {
    ...state,
    cartErrors: {
      ...state.cartErrors,
      [field]: null,
    },
  };
}

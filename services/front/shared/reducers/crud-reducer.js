import {rpcIds} from '../actions/api-actions';
import {actionTypes} from '../actions/app-actions';
import {handleActions} from 'redux-actions';
import reduceReducers from 'reduce-reducers';
import {createRPCReducer} from '../utils/reducer-utils';

export const initialState = {
  categories: {},
  children: [],
  itemImages: [],

  productMap: {},
  products: [],
  productsTotalCount: 0,
  imageMap: {},
  images: [],
  imagesTotalCount: 0,
  attributes: {},
  attributeTypes: [],
  imagesLastSearch: '',
  sellers: {},
  billables: {
    loading: false,
    list: [],
    totalCount: 0,
  },
  orders: {
    loading: false,
    list: [],
    totalCount: 0,
  },
};

export default reduceReducers(
  (state, action) => state || initialState,

  createRPCReducer(rpcIds['crud.attributeProductDelete'], {
    success: (state, action) => {
      const {id, product_id} = action.payload;
      const product = {...state.productMap[product_id]};
      const index = product.attributes.findIndex(a => a.id === id);
      product.attributes.splice(index, 1);
      return {
        ...state,
        productMap: {
          ...state.productMap,
          [product_id]: {...product, attributes: [...product.attributes]},
        },
      };
    },
  }),
  createRPCReducer(rpcIds['crud.attributeProductUpdate'], {
    success: (state, action) => {
      const {resp} = action.payload;
      const product = {...state.productMap[resp.product_id]};
      const index = product.attributes.findIndex(a => a.id === resp.id);
      product.attributes[index] = resp;
      return {
        ...state,
        productMap: {
          ...state.productMap,
          [resp.product_id]: {...product, attributes: [...product.attributes]},
        },
      };
    },
  }),
  createRPCReducer(rpcIds['crud.attributeProductCreate'], {
    success: (state, action) => {
      const {resp} = action.payload;
      const product = {...state.productMap[resp.product_id]};
      product.attributes.push(resp);
      return {...state, productMap: {...state.productMap, [resp.product_id]: product}};
    },
  }),
  createRPCReducer(rpcIds['crud.productCreate'], {
    success: (state, action) => {
      const {resp} = action.payload;
      return {
        ...state,
        products: [resp.id, ...state.products],
        productMap: {
          ...state.productMap,
          [resp.id]: {
            ...resp,
            attributes: [],
          },
        },
      };
    },
  }),
  createRPCReducer(rpcIds['crud.productDelete'], {
    success: (state, action) => {
      const index = state.products.findIndex(p => p.id === action.payload.id);
      if (index === -1) {
        return state;
      }

      state.products.splice(index, 1);
      delete state.productMap[action.payload.id];
      return {
        ...state,
        products: [...state.products],
        productMap: {
          ...state.productMap,
        },
      };
    },
  }),
  createRPCReducer(rpcIds['crud.productUpdate'], {
    success: (state, action) => {
      const {id, field, value} = action.payload;
      state.productMap[id][field] = value;
      return {...state, productMap: {...state.productMap}};
    },
  }),
  createRPCReducer(rpcIds['crud.sellerCreateProduct'], {
    success: (state, action) => {
      const {product} = action.payload.resp;
      state.products.unshift(product.id);
      state.productMap[product.id] = product;
      return {...state, productMap: {...state.productMap}, products: [...state.products]};
    },
  }),
  createRPCReducer(rpcIds['crud.productDelete'], {
    success: (state, action) => {
      const {id} = action.payload;
      const index = state.products.findIndex(p => p == id);
      const products = [...state.products];
      products.splice(index, 1);
      return {...state, products};
    },
  }),
  createRPCReducer(rpcIds['crud.productsList'], {
    success: (state, action) => {
      const {products, totalCount} = action.payload.resp;
      return {
        ...state,
        productMap: products,
        products: Object.keys(products),
        productsTotalCount: totalCount,
      };
    },
  }),
  createRPCReducer(rpcIds['crud.attributesList'], {
    success: (state, action) => {
      const resp = action.payload.resp;
      const types = resp.reduce((agg, r) => {
        agg.add(r.type);
        return agg;
      }, new Set());

      const map = resp.reduce((agg, r) => {
        agg[r.id] = r;
        return agg;
      }, {});

      return {
        ...state,
        attributes: map,
        attributeTypes: Array.from(types),
      };
    },
  }),
  createRPCReducer(rpcIds['crud.categoriesList'], {
    start: (state, action) => {
      return {
        ...state,
        children: [],
      };
    },
    success: (state, action) => {
      const resp = action.payload.resp;
      const cats = {...state.categories};
      resp.forEach(r => (cats[r.id] = r));
      return {
        ...state,
        children: resp.map(r => r.id),
        categories: cats,
      };
    },
  }),
  createRPCReducer(rpcIds['crud.saveTracking'], {
    success: updateOrderCart,
  }),
  createRPCReducer(rpcIds['crud.cancelOrderBySeller'], {
    success: updateOrderCart,
  }),
  createRPCReducer(rpcIds['crud.returnToSenderSeller'], {
    success: updateOrderCart,
  }),
  createRPCReducer(rpcIds['crud.updateOrderStatus'], {
    success: updateOrderCart,
  }),
  createRPCReducer(rpcIds['crud.addReturnLabel'], {
    success: updateOrderCart,
  }),
  createRPCReducer(rpcIds['crud.sellersList'], {
    success: (state, action) => {
      const {
        resp: {sellers},
      } = action.payload;
      return {
        ...state,
        sellers: {
          ...state.sellers,
          ...sellers,
        },
      };
    },
  }),
  createRPCReducer(rpcIds['crud.ordersList'], {
    start: (state, action) => {
      return {
        ...state,
        orders: {
          ...state.orders,
          loading: true,
        },
      };
    },
    success: (state, action) => {
      const {
        resp: {orders, totalCount, sellers, sources},
        offset,
      } = action.payload;
      return {
        ...state,
        sellers: {
          ...state.sellers,
          ...sellers,
        },
        orders: {
          ...state.orders,
          list: orders,
          loading: false,
          totalCount: !offset ? totalCount : state.orders.totalCount,
          sources: {
            ...(state.orders?.sources || {}),
            ...sources,
          },
        },
      };
    },
    failure: (state, action) => {
      return {
        ...state,
        orders: {
          ...state.orders,
          list: [],
          loading: false,
          totalCount: 0,
        },
      };
    },
  }),
  createRPCReducer(rpcIds['crud.billablesList'], {
    start: (state, action) => {
      return {
        ...state,
        billables: {
          ...state.billables,
          loading: true,
        },
      };
    },
    success: (state, action) => {
      const {
        resp: {rows, count},
        offset,
      } = action.payload;
      return {
        ...state,
        billables: {
          ...state.billables,
          list: rows,
          loading: false,
          totalCount: !offset ? count : state.billables.totalCount,
        },
      };
    },
    failure: (state, action) => {
      return {
        ...state,
        billables: {
          ...state.billables,
          list: [],
          loading: false,
          totalCount: 0,
        },
      };
    },
  }),
  createRPCReducer(rpcIds['crud.saveBillable'], {
    success: (state, action) => {
      const {
        resp: {billable},
      } = action.payload;
      return {
        ...state,
        billables: {
          ...state.billables,
          list: [billable, ...state.billables.list],
          totalCount: state.billables.totalCount + 1,
        },
      };
    },
  }),

  createRPCReducer(rpcIds['crud.categoryImageCreate'], {
    success: (state, action) => {
      return {
        ...state,
        itemImages: [...state.itemImages, action.payload.resp],
      };
    },
  }),
  createRPCReducer(rpcIds['crud.categoryImagesList'], {
    start: (state, action) => {
      return {
        ...state,
        itemImages: [],
      };
    },
    success: (state, action) => {
      const {allImages, imageMap} = action.payload.resp;
      return {
        ...state,
        imageMap: {
          ...state.imageMap,
          ...imageMap,
        },
        itemImages: allImages,
      };
    },
  }),
  createRPCReducer(rpcIds['crud.categoryGet'], {
    success: updateCategory,
  }),
  createRPCReducer(rpcIds['crud.categoryCreate'], {
    success: (state, action) => {
      const resp = action.payload.resp;
      return updateCategory(
        {
          ...state,
          children: [...state.children, resp.id],
        },
        action,
      );
    },
  }),
  createRPCReducer(rpcIds['crud.categoryDelete'], {
    success: (state, action) => {
      const id = action.payload.id;
      const cats = {...state.categories};
      delete cats[id];
      const childrenIndex = state.children.findIndex(c => c === id);
      const children = [...state.children];
      console.log('000index', childrenIndex);
      children.splice(childrenIndex, 1);
      return {
        ...state,
        children,
        categories: cats,
      };
    },
  }),
  createRPCReducer(rpcIds['crud.categoryImageUpdate'], {
    success: (state, action) => {
      const {id, resp} = action.payload;
      const itemImages = [...state.itemImages];
      const index = itemImages.findIndex(it => it.id === id);
      itemImages[index] = resp;
      return {
        ...state,
        itemImages,
      };
    },
  }),
  createRPCReducer(rpcIds['crud.categoryImageDelete'], {
    success: (state, action) => {
      const {id, resp} = action.payload;
      const itemImages = [...state.itemImages];
      const index = itemImages.findIndex(it => it.id === id);
      itemImages.splice(index, 1);
      return {
        ...state,
        itemImages,
      };
    },
  }),
  createRPCReducer(rpcIds['crud.imagesList'], {
    start: (state, action) => {
      const {search} = action.payload;
      return {
        ...state,
        imagesLastSearch: search,
      };
    },
    success: (state, action) => {
      const {
        resp: {rows, count},
        search,
        offset,
      } = action.payload;
      if (search != state.imagesLastSearch) {
        return state;
      }
      const newIds = rows.map(r => r.id);
      return {
        ...state,
        imageMap: {
          ...state.imageMap,
          ...rows.reduce((agg, it) => {
            agg[it.id] = it;
            return agg;
          }, {}),
        },
        images: newIds,
        imagesTotalCount: count,
      };
    },
  }),

  createRPCReducer(rpcIds['crud.categoryUpdate'], {
    success: updateCategory,
  }),

  handleActions(
    {
      [actionTypes.storeImageUploaded]: (state, action) => {
        const imageMap = {...state.imageMap};
        const {filename, img_id} = action.payload;
        imageMap[img_id] = {id: img_id, name: filename};
        return {...state, imageMap};
      },
    },
    initialState,
  ),
);

function updateCategory(state, action) {
  const resp = action.payload.resp;
  return {
    ...state,
    categories: {
      ...state.categories,
      [resp.id]: resp,
    },
  };
}

function updateOrderCart(state, action) {
  const {newData, cart_ids, order_id} = action.payload.resp;
  if (!order_id) {
    return state;
  }
  const ids = new Set(cart_ids);

  const orderIndex = state.orders.list.findIndex(o => o.id === order_id);

  state.orders.list[orderIndex] = {
    ...state.orders.list[orderIndex],
    cart: state.orders.list[orderIndex].cart.map(c => {
      if (ids.has(c.id)) {
        return {...c, ...newData};
      }
      return c;
    }),
  };

  return {
    ...state,
    orders: {
      ...state.orders,
      list: [...state.orders.list],
    },
  };
}

import {rpcIds} from '../actions/api-actions';
import {actionTypes} from '../actions/app-actions';
import {handleActions} from 'redux-actions';
import reduceReducers from 'reduce-reducers';
import {createRPCReducer} from '../utils/reducer-utils';
import deepExtend from 'deep-extend';

const initialState = {
  signup: {
    email: '',
    password: '',
    errors: {},
  },
  signin: {
    email: '',
    password: '',
    errors: {},
  },
};

export default reduceReducers(
  (state, action) => state || initialState,
  /*
  createRPCReducer(rpcIds['cart.isOrderMine'], {
    success: (state, action) => {
      const order = action.payload.resp.order;
      if (!order) {
        return state;
      }
      return {
        ...state,
        signup: {
          ...state.signup,
          email: order.email,
          firstName: order.firstName,
          lastName: order.lastName,
        },
      };
    },
  }),
  */
  createRPCReducer(rpcIds['user.login'], {
    start: (state, action) => {
      return {
        ...state,
        signin: {
          ...state.signin,
          loadingSignin: true,
        },
      };
    },
    success: (state, action) => {
      return {
        ...state,
        signin: {
          ...state.signup,
          loadingSignin: false,
        },
      };
    },
    failure: (state, action) => {
      const {resp = {}} = action.payload;
      const errors = resp.errorData ? resp.errorData : {form: {...resp}};

      return {
        ...state,
        signin: {
          ...state.signin,
          errors,
          loadingSignin: false,
        },
      };
    },
  }),

  handleActions(
    {
      [actionTypes.closeSignupModal]: (state, action) => {
        return {...state, signup: deepExtend(initialState.signup)};
      },
      [actionTypes.closeSigninModal]: (state, action) => {
        return {...state, signin: deepExtend(initialState.signin)};
      },
      [actionTypes.updateSigninForm]: (state, action) => {
        return {
          ...state,
          signin: {
            ...state.signin,
            ...action.payload,
          },
        };
      },
    },
    initialState,
  ),
);

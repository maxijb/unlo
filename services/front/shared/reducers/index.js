import {combineReducers} from 'redux';
import appReducer from './app-reducer';
import formsReducer from './forms-reducer';
import userReducer from './user-reducer';
import productReducer from './product-reducer';
import reportsReducer from './reports-reducer';
import cartReducer from './cart-reducer';
import orderReducer from './order-reducer';
import crudReducer from './crud-reducer';

export default combineReducers({
  app: appReducer,
  forms: formsReducer,
  user: userReducer,
  reports: reportsReducer,
  product: productReducer,
  cart: cartReducer,
  order: orderReducer,
  crud: crudReducer,
});

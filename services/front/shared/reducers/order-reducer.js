import {rpcIds} from '../actions/api-actions';
import {actionTypes} from '../actions/app-actions';
import {handleActions} from 'redux-actions';
import reduceReducers from 'reduce-reducers';
import {createRPCReducer} from '../utils/reducer-utils';
import {arrayToMap} from '@Common/utils/generic-utils';

export const initialState = {
  orders: [],
  addresses: {},
  carts: [],
  isLoading: false,
};

export default reduceReducers(
  (state, action) => state || initialState,

  createRPCReducer(rpcIds['order.listOrdersForUser'], {
    start: (state, action) => {
      return {...state, isLoading: true};
    },
    success: (state, action) => {
      const {carts, orders, addresses} = action.payload.resp;
      return {...state, orders, addresses, isLoading: false};
    },
    failure: (state, action) => {
      return {...state, isLoading: false};
    },
  }),
  createRPCReducer(rpcIds['cart.returnProducts'], {
    success: (state, action) => {
      const {products, orders: ordersRaw} = action.payload.resp;
      const productsMap = arrayToMap(products);
      const orders = [...state.orders];
      const orderIds = new Set(ordersRaw);

      for (let order of orders) {
        if (orderIds.has(order.id)) {
          order.products = order.products.map(p => {
            if (productsMap.hasOwnProperty(p.id)) {
              return {...p, status: productsMap[p.id].status};
            }
            return p;
          });
        }
      }
      return {...state, orders};
    },
  }),

  handleActions(
    {
      //[actionTypes.openSignupModal]: (state, action) => {
      //  return {...state, openedSignupModal: true, openedSigninModal: false};
      //},
    },
    initialState,
  ),
);

import {rpcIds} from '../actions/api-actions';
import {actionTypes} from '../actions/app-actions';
import {PRODUCT_ITEMS_PER_PAGE} from '@Common/constants/app';
import {handleActions} from 'redux-actions';
import reduceReducers from 'reduce-reducers';
import {createRPCReducer} from '../utils/reducer-utils';

export const initialState = {
  product: {},
  imageMap: {},
  allImages: [],
  allCategories: [],
  defaultVersion: {},
  allAttributes: [],
  isLoading: false,
  isError: false,
  cheapestVariant: null,
  competitorPrices: [],
  isLoadingCompetitorPrices: false,
  loadingAccessoryProducts: false,
  loadingPricePerCondition: true,
  accessoryProducts: [],
  seller: null,
  minimumPricePerCondition: {},
  relatedProducts: {
    isLoading: false,
    list: [],
  },
  categoryProducts: {
    isLoading: false,
    list: [],
    pagination: {
      offset: 0,
      count: 0,
      limit: PRODUCT_ITEMS_PER_PAGE,
    },
  },
  specs: [],
};

export default reduceReducers(
  (state, action) => state || initialState,

  createRPCReducer(rpcIds['product.getCompetitorPrices'], {
    start: (state, action) => {
      return {
        ...state,
        isLoadingCompetitorPrices: true,
      };
    },
    success: (state, action) => {
      const {competitorPrices, cheapestRelevantVariant} = action.payload.resp;
      return {
        ...state,
        isLoadingCompetitorPrices: false,
        competitorPrices,
        cheapestVariant: cheapestRelevantVariant?.[0]?.id || null,
        cheapestOwnPrice: cheapestRelevantVariant?.[0]?.price || null,
      };
    },
    failure: (state, action) => {
      return {
        ...state,
        isLoadingCompetitorPrices: false,
        competitorPrices: [],
      };
    },
  }),

  createRPCReducer(rpcIds['product.getAccessoryProducts'], {
    start: (state, action) => {
      return {
        ...state,
        loadingAccessoryProducts: true,
        accessoryProducts: [],
      };
    },
    success: (state, action) => {
      const {accessoryProducts} = action.payload.resp;
      return {
        ...state,
        loadingAccessoryProducts: false,
        accessoryProducts,
      };
    },
    failure: (state, action) => {
      return {
        ...state,
        loadingAccessoryProducts: false,
      };
    },
  }),

  createRPCReducer(rpcIds['product.relatedProducts'], {
    start: (state, action) => {
      return {
        ...state,
        relatedProducts: {
          isLoading: true,
          list: [],
        },
      };
    },
    success: (state, action) => {
      const resp = action.payload.resp;
      return {
        ...state,
        relatedProducts: {
          isLoading: false,
          list: resp.products,
        },
      };
    },
    failure: (state, action) => {
      return {
        ...state,
        relatedProducts: {
          isLoading: false,
          list: [],
        },
      };
    },
  }),

  createRPCReducer(rpcIds['product.getCategoryProducts'], {
    start: (state, action) => {
      return {
        ...state,
        categoryProducts: {
          ...state.categoryProducts,
          isLoading: true,
          // list: [],
        },
      };
    },
    success: (state, action) => {
      const resp = action.payload.resp;
      return {
        ...state,
        categoryProducts: {
          isLoading: false,
          list: Object.values(resp.variants),
          pagination: resp.pagination,
        },
      };
    },
    failure: (state, action) => {
      return {
        ...state,
        categoryProducts: {
          ...state.categoryProducts,
          isLoading: false,
          list: [],
        },
      };
    },
  }),

  createRPCReducer(rpcIds['product.loadWithAttributes'], {
    start: (state, action) => {
      return {...state, isLoading: true};
    },
    success: (state, action) => {
      const {
        resp: {tags, variant, shippingMethodsBySeller, seller},

        lastClicked,
      } = action.payload;

      const newState = {...state, defaultVersion: {tags, variant}, seller, isLoading: false};
      return newState;
    },
    failure: (state, action) => {
      return {...state, defaultVersion: {}, isLoading: false};
    },
  }),

  createRPCReducer(rpcIds['product.getMinimumPricePerCondition'], {
    start: (state, action) => {
      return {...state, loadingPricePerCondition: true};
    },
    success: (state, action) => {
      const minimumPricePerCondition = action.payload.resp.reduce((agg, it) => {
        agg[it.attr_id] = it.min_price;
        return agg;
      }, {});
      return {...state, minimumPricePerCondition, loadingPricePerCondition: false};
    },
    failure: (state, action) => {
      return {...state, loadingPricePerCondition: false, minimumPricePerCondition: {}};
    },
  }),

  createRPCReducer(rpcIds['product.getVersionWithAttributes'], {
    start: (state, action) => {
      return {...state, isLoading: true};
    },
    success: (state, action) => {
      const resp = action.payload.resp;
      return {
        ...state,
        ...resp,
        isLoading: false,
        lastClicked: null,
      };
    },
    failure: (state, action) => {
      return {
        ...state,
        ...initialState,
        isLoading: false,
      };
    },
  }),

  createRPCReducer(rpcIds['product.getMinimumDetails'], {
    start: (state, action) => {
      return {...state, isLoading: true};
    },
    success: (state, action) => {
      const resp = action.payload.resp;
      return {
        ...state,
        ...resp,
        isLoading: false,
        specs: resp.specs,
      };
    },
    failure: (state, action) => {
      return {
        ...state,
        ...initialState,
        isLoading: false,
        specs: [],
      };
    },
  }),

  handleActions(
    {
      //[actionTypes.openSignupModal]: (state, action) => {
      //  return {...state, openedSignupModal: true, openedSigninModal: false};
      //},
    },
    initialState,
  ),
);

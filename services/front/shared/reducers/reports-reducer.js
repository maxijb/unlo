import {rpcIds} from '../actions/api-actions';
import {actionTypes} from '../actions/app-actions';
import {handleActions} from 'redux-actions';
import reduceReducers from 'reduce-reducers';
import {createRPCReducer} from '../utils/reducer-utils';
import {PRISTINE_FIELD} from '@Common/constants/input';
import {SocketStatus} from '@Common/constants/socket-status';
import {OrderedHash} from '@Common/utils/ordered-hash';

const initialState = {
  interactionsChart: {
    loaded: false,
    items: [],
    error: false,
  },
  bookingsChart: {
    loaded: false,
    items: [],
    error: false,
  },
  contactsChart: {
    loaded: false,
    items: [],
    error: false,
  },
  sessionsChart: {
    loaded: false,
    items: [],
    error: false,
  },
  sessionsByURLChart: {
    loaded: false,
    items: [],
    error: false,
  },
  conversationByOperator: {
    loaded: false,
    items: [],
    error: false,
  },
};

export default reduceReducers(
  (state, action) => state || initialState,
  /*
  createRPCReducer(rpcIds['report.conversationByOperator'], {
    start: (state, action) => {
      return {
        ...state,
        conversationByOperator: {
          ...state.conversationByOperator,
          loaded: false,
          error: false,
          loading: true
        }
      };
    },
    success: (state, action) => {
      return {
        ...state,
        conversationByOperator: {
          ...state.conversationByOperator,
          ...action.payload.resp,
          loaded: true,
          error: false,
          loading: false
        }
      };
    },
    failure: (state, action) => {
      return {
        ...state,
        conversationByOperator: {
          ...state.conversationByOperator,
          error: action.payload.resp.error,
          loading: false,
          loaded: false
        }
      };
    }
  }),
  */

  handleActions(
    {
      //[actionTypes.receiveNewStatus]: (state, action) => {
      //  const {
      //    type,
      //    data: {publicId}
      //  } = action.payload;
      //  return {
      //    ...state,
      //    list: state.list.extend(publicId, {
      //      online: type === SocketStatus.connect
      //    })
      //  };
      //},
    },
    initialState,
  ),
);

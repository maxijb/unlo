import {rpcIds} from '../actions/api-actions';
import {actionTypes} from '../actions/app-actions';
import {handleActions} from 'redux-actions';
import reduceReducers from 'reduce-reducers';
import {createRPCReducer} from '../utils/reducer-utils';
import deepExtend from 'deep-extend';
import deepEqual from 'deep-equal';
import get from 'lodash.get';

import {ConversationStatus, DefaultConversationSearchOptions} from '@Common/constants/interactions';
import {OrderedHash} from '@Common/utils/ordered-hash';
import {AssetFieldsToBeExtracted, AssetStatus} from '@Common/constants/assets';

export const initialState = {
  id: null,
  displayName: null,
  email: null,
  firstName: null,
  lastName: null,
  phone: null,
};

export default reduceReducers(
  (state, action) => state || initialState,

  createRPCReducer(rpcIds['user.logout'], {
    success: (state, action) => {
      return deepExtend(initialState);
    },
  }),

  createRPCReducer(rpcIds['cart.getPendingOrderDetails'], {
    success: (state, action) => {
      const {
        resp: {order},
      } = action.payload;
      return {
        ...state,
        firstName: order.firstName,
        lastName: order.lastName,
        email: order.email,
        phone: order.phone,
      };
    },
  }),

  createRPCReducer(rpcIds['user.signup'], {
    success: setUser,
  }),

  createRPCReducer(rpcIds['user.getUserOwnMinimumDetails'], {
    success: setUser,
  }),

  createRPCReducer(rpcIds['user.getUserOwnExtendedDetails'], {
    success: (state, action) => {
      return {...state, ...action.payload.resp};
    },
  }),

  createRPCReducer(rpcIds['user.login'], {
    success: setUser,
  }),

  createRPCReducer(rpcIds['user.facebookLogin'], {
    success: setUser,
  }),

  createRPCReducer(rpcIds['user.googleLogin'], {
    success: setUser,
  }),

  createRPCReducer(rpcIds['user.validateEmailToken'], {
    success: (state, action) => {
      return {
        ...state,
        validated: 1,
      };
    },
  }),

  createRPCReducer(rpcIds['user.getUserDetails'], {
    success: (state, action) => {
      const {assets, ...other} = action.payload.resp;
      return {
        ...state,
        ...other,
        assets: new OrderedHash(assets),
      };
    },
  }),

  handleActions(
    {
      [actionTypes.setUserEmail]: (state, action) => {
        return {...state, email: action.payload};
      },
      [actionTypes.setUserFirstName]: (state, action) => {
        return {...state, firstName: action.payload};
      },
      [actionTypes.setUserLastName]: (state, action) => {
        return {...state, lastName: action.payload};
      },
      [actionTypes.setUserPhone]: (state, action) => {
        const phone = action.payload;
        return {...state, phone};
      },

      [actionTypes.setUserId]: (state, action) => {
        return {...state, id: action.payload.id};
      },

      [actionTypes.updateUserInfo]: (state, action) => {
        return {
          ...state,
          ...action.payload,
        };
      },
    },
    initialState,
  ),
);

function setUser(state, action) {
  return {
    ...state,
    id: action.payload.resp.id,
    displayName: action.payload.resp.displayName,
    superAdmin: action.payload.resp.superAdmin,
  };
}

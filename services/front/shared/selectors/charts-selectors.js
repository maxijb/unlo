import isEmpty from 'is-empty';
import {createSelector} from 'reselect';
import {DayMs} from '@Common/constants/dates';

const getItems = data => data.items;
const getStart = data => data.start;
const getEnd = data => data.end;

export const getChartSelector = createSelector(
  [getItems, getStart, getEnd],
  (items, start, end) => {
    let max = 0;
    const chartItems = [];

    const lastX = new Date(end).getTime();
    const firstX = new Date(start).getTime();
    const delta = DayMs;

    if (isEmpty(items) || new Date(items[0].date).getTime() !== firstX) {
      items.unshift({date: start, count: 0});
    }

    items.forEach((it, i) => {
      max = Math.max(it.count, max);
      const x = new Date(it.date).getTime();
      const nextX = items[i + 1] ? new Date(items[i + 1].date).getTime() : Infinity;
      chartItems.push({
        y: it.count,
        x
      });

      let workX = x + delta;
      while (workX < nextX && workX < lastX) {
        chartItems.push({y: 0, x: workX});
        workX += delta;
      }
    });

    return {max, chartItems};
  }
);

import React from 'react';
import {createSelector} from 'reselect';

const getOptions = (data, name, fn, emptyOption, t) => [data[name], fn, emptyOption, t];
const getPeriod = data => data.period;
const getMinimum = data => data.minimum;
const getMaximum = data => data.maximum;
const getT = data => data.t;

export const getOptionsSelector = createSelector(getOptions, ([options, fn, emptyOption, t]) => {
  const opts = (options || []).map(item => {
    return (fn || extractDefault)(item, t);
  });
  return !emptyOption ? opts : [{key: '--EMPTY--', value: '', text: emptyOption}].concat(opts);
});

export const durationSelector = createSelector(
  [getPeriod, getMinimum, getMaximum, getT],
  (period, minimum, maximum, t) => {
    const options = [];

    let time = minimum;
    while (time <= maximum) {
      options.push({
        key: time,
        value: time,
        text: time % 60 === 0 ? t('xHours', {count: time / 60}) : t('xMinutes', {count: time})
      });
      time += period;
    }

    return options;
  }
);

export const extractDefault = (it, t) => {
  const text = t ? t(it.name || it) : it;
  return {key: it, text: <span>{text}</span>, value: it};
};
export const extractIdName = it => ({key: it.id, text: it.name, value: it.id});

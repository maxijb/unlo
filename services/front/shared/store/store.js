import {createStore, applyMiddleware} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';
import reducer from '../reducers/index';
import {OrderedHash} from '@Common/utils/ordered-hash';

export function initializeStore(initialState = {}) {
  return createStore(
    reducer,
    initOrderedHashes(initialState),
    composeWithDevTools(applyMiddleware(thunkMiddleware))
  );
}

function initOrderedHashes(state) {
  if (!process.browser) {
    return state;
  }

  Object.keys(state).forEach(branch => {
    Object.keys(state[branch]).forEach(key => {
      const obj = state[branch][key];
      if (obj && Array.isArray(obj.keys) && typeof obj.values === 'object' && !obj.get) {
        state[branch][key] = new OrderedHash({keys: obj.keys, values: obj.values});
      }
    });
  });

  return {...state};
}

import React from 'react';
import {get} from 'lodash';
import {initializeStore} from './store';
import {initialState as InitialUserState} from '../reducers/user-reducer';
import {initialState as InitialAppState} from '../reducers/app-reducer';
import Api from '../actions/api-actions';

const isServer = typeof window === 'undefined';
const __NEXT_REDUX_STORE__ = '__NEXT_REDUX_STORE__';

function getOrCreateStore(initialState) {
  // Always make a new store if server, otherwise state is shared between requests
  if (isServer) {
    return initializeStore(initialState);
  }

  // Create store if unavailable on the client and set it on the window object
  if (!window[__NEXT_REDUX_STORE__]) {
    window[__NEXT_REDUX_STORE__] = initializeStore(initialState);
  }
  return window[__NEXT_REDUX_STORE__];
}

export default App => {
  return class AppWithRedux extends React.Component {
    static async getInitialProps(appContext) {
      // We capture here everthing we need from the Req object to be in the initial redux state
      // initialReduxState must include user id via cookies, and url params
      const id = get(appContext, 'ctx.req.auth.id', null);
      const sessionId = get(appContext, 'ctx.req.sessionId', null);
      const clientId = get(appContext, 'ctx.req.clientId', null);
      const activeAB = get(appContext, 'ctx.req.activeAB', null);
      const lang = get(appContext, 'ctx.req.i18n.language', null);
      const env = get(process, 'env.NODE_ENV', 'development');
      const shownContactModal = get(appContext, 'ctx.req.cookies.unlo-contact-modal');
      // server side config persisted to redux
      const config = {};
      const serverConfig = get(appContext, 'ctx.req.Config');

      if (serverConfig) {
        const {
          domain,
          fullDomain,
          mailDomain,
          mailLinkDomain,
          protocol,
          cdnDomain,
          appName,
          GATrackingId,
          useCohere,
          isProd,
          AdWordsConversionTrackingId,
          useSentry,
        } = serverConfig.get('server');
        const {sentry} = serverConfig.get('social');
        config.sockets = serverConfig.get('sockets');
        config.email = {domain, mailDomain, mailLinkDomain, protocol};
        config.cdn = {cdnDomain, protocol};
        config.fullDomain = fullDomain;
        config.useCohere = useCohere;
        config.protocol = protocol;
        config.isProd = isProd;
        config.appName = appName;
        config.GATrackingId = GATrackingId;
        config.AdWordsConversionTrackingId = AdWordsConversionTrackingId;
        config.sentry = sentry?.client || null;
      }

      const reduxStore = getOrCreateStore({
        user: {
          ...InitialUserState,
          id,
          sessionId,
          clientId,
        },
        app: {
          ...InitialAppState,
          activeAB,
          lang,
          env,
          config,
          shownContactModal: !!shownContactModal,
        },
      });

      // Provide the store to getInitialProps of pages
      appContext.ctx.reduxStore = reduxStore;
      appContext.ctx.dispatch = reduxStore.dispatch;

      if (id) {
        await reduxStore.dispatch(Api.user.getUserOwnMinimumDetails({}, appContext?.ctx?.req));
      }

      let appProps = {};
      if (typeof App.getInitialProps === 'function') {
        appProps = await App.getInitialProps(appContext);
      }

      return {
        ...appProps,
        initialReduxState: reduxStore.getState(),
      };
    }

    constructor(props) {
      super(props);
      this.reduxStore = getOrCreateStore(props.initialReduxState);
    }

    render() {
      return <App {...this.props} reduxStore={this.reduxStore} />;
    }
  };
};

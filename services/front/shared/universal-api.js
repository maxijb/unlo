import Api from '../../common/api/api';
import {CsrfTokenHeader} from '../../common/constants/app';
import {ABForceHeaderName} from '../../common/constants/ab-tests';
import QS from 'qs';
import {formatUri} from '../../common/utils/urls';
import Logger from '@Common/logger';
import SecuredFetch from './utils/secured-fetch';
import serverFetch from 'node-fetch';
import {SocketEvents} from '@Common/constants/socket-status';

export default startApiKeys(Api);
/* Recursively inits each api function, exposing it to the server- and client-side */
function startApiKeys(base, prefix = '') {
  if (typeof base !== 'object') {
    throw `${base} should be an object to init apis`;
  }
  const uapi = Object.keys(base).reduce((prev, key) => {
    // We ignore the key middleware as it's a protected keyword to define behavior in the api
    if (key === 'middleware' || key === 'method') {
      return prev;
    }

    if (typeof base[key] === 'function') {
      throw `Api element "${key}" may be missing a controller field`;
    }

    // if no controller is defined, go deeper
    if (!base[key].hasOwnProperty('controller') && !base[key].hasOwnProperty('socketController')) {
      prev[key] = startApiKeys(base[key], `${prefix}${key}.`);
    } else {
      const item = base[key];
      const uri = formatUri(item.path || `${prefix || ''}${key}`);
      if (!process.browser) {
        prev[key] = initExposeController(item, uri);
      } else if (item.hasOwnProperty('controller')) {
        prev[key] = initFetchController(item, uri);
      }
    }
    return prev;
  }, {});

  return uapi;
}

/* From the browser each action will send an AJAX request */
function initFetchController(item, uri) {
  const method = item.method || 'GET';
  return function (query) {
    // if no socket is defined we use HTTP contorller
    let request;
    if (method === 'GET') {
      request = SecuredFetch(`${uri}?${QS.stringify(query)}`, {
        headers: {
          'Content-Type': 'application/json',
        },
      });
    } else {
      request = SecuredFetch(`${uri}`, {
        method: method.toLowerCase(),
        headers: query.raw
          ? {}
          : {
              'Content-Type': 'application/json',
            },
        body: query.raw || JSON.stringify(query),
      });
    }

    return request
      .then(async response => {
        let resp;
        let clone;
        if (!response.ok) {
          clone = response.clone();
        }
        try {
          resp = await response.json();
        } catch (e) {
          const b = await clone.text();
          resp = {error: b};
        }

        return {
          resp,
          request: {
            status: response.status,
            statusText: response.statusText,
          },
        };
      })
      .catch((error, ...params) => {
        Logger.error(error, params);
        // TODO: should it be {resp: error} ???
        return Promise.reject(error);
      });
  };
}

/* In the server each action will just call a controller */
function initExposeController(item, uri) {
  const method = item.method || 'GET';
  return (query, req) => {
    if (!req) {
      throw new Error(
        `"req" must be sent as last param of every server-side requests called from getOriginalProps (controller ${item.controller})`,
      );
    }
    const config = req.Config.get('server');

    const toSend = {
      uri: `http://${config.hosts.backend}:${config.ports.backend}${uri}`,
      method,
      headers: {
        ...req.headers,
        'Content-Type': 'application/json',
        [CsrfTokenHeader]: config.session.whitelistedCsrfToken,
      },
    };

    // if we need to force an AB cookie
    if (req.cookieAB) {
      toSend.headers[ABForceHeaderName] = req.cookieAB;
    }

    if (method === 'GET') {
      toSend.uri += `?${QS.stringify(query)}`;
    } else {
      toSend.body = JSON.stringify(query);
    }

    return serverFetch(toSend.uri, toSend)
      .then(response => {
        return Promise.all([
          response.json(),
          {
            status: response.status,
            statusText: response.statusText,
          },
        ]);
      })
      .then(([resp, req]) => ({resp, request: req}))
      .catch((error, ...params) => {
        Logger.error(error, params);
        return Promise.resolve({resp: {error}});
      });

    // return item.controller(query, req).then(response => ({resp: response}));
    // Commenting out to let the error broadcast to the API-actions level
    // .catch(error => {
    //  Logger.error(error);
    //  return {};
    // });
  };
}

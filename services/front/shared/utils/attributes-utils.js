import {UnlockedCarrierId} from '@Common/constants/app';

export const consolidateAttributes = (current, field = 'id') => {
  if (current?.length) {
    const carrier = current.some(it => it.id === UnlockedCarrierId)
      ? UnlockedCarrierId
      : current.find(it => it.type === 'carrier')?.[field];

    const color = current.find(it => it.type === 'color')?.[field];
    const capacity = current.find(it => it.type === 'capacity')?.[field];
    const appearance = current.find(it => it.type === 'appearance')?.[field];

    return {
      carrier,
      color,
      capacity,
      appearance,
    };
  }

  return {};
};

export const consolidateAttributesAsName = (current, t) => {
  if (current?.length) {
    const carrier = current.some(it => it.id === UnlockedCarrierId)
      ? t('Unlocked')
      : t(current.find(it => it.type === 'carrier')?.name);

    const color = t(current.find(it => it.type === 'color')?.name);
    const capacity = t(current.find(it => it.type === 'capacity')?.name);
    const appearance = t(current.find(it => it.type === 'appearance')?.name);

    return {
      carrier,
      color,
      capacity,
      appearance,
    };
  }

  return {};
};

export const getProductNameWithAttributes = (name, attrs, t) => {
  return `${name} ${attrs.capacity || ''}${attrs.color ? ` - ${attrs.color}` : ''}${
    attrs.carrier === t('Unlocked') ? ` - Fully Unlocked` : ''
  }`;
};

export const getProductNameWithAttributesForOrder = (name, attrs, t) => {
  const carrier = attrs.carrier ? ` - ${attrs.carrier}` : '';
  return `${name} ${attrs.capacity || ''}${attrs.color ? ` - ${attrs.color}` : ''}${
    attrs.carrier === t('Unlocked') ? ` - Fully Unlocked` : carrier
  }`;
};

export const getProductNameWithAttributesForGoogle = (name, attrs, t) => {
  return `Apple ${name} ${attrs.capacity || ''}${attrs.color ? ` ${attrs.color}` : ''}${
    attrs.carrier === t('Unlocked')
      ? ` - Fully Unlocked`
      : attrs.carrier
      ? ` - ${t(attrs.carrier)}`
      : ''
  } - Refurbished 4G LTE Smartphone with Warranty`;
};

export const findRelevantImages = (allImages, tags = new Set()) => {
  return (allImages || [])
    .filter(img => {
      return tags.has(img.attr1) && tags.has(img.attr2) && tags.has(img.attr3);
    })
    .sort((a, b) => a.order - b.order);
};

export const getTagsSet = tags => {
  if (typeof tags?.has === 'function') {
    return tags;
  }
  return new Set([null, ...(tags || [])].map(tag => tag?.id || null));
};

export function getIncredibleIdFromObjects(category, attributes) {
  const attrs = {category: category.slug};
  for (const attr of attributes) {
    attrs[attr.type] = attr.slug;
  }
  return getIncredibleIdFromSlugs(attrs);
}

export function getIncredibleIdFromSlugs(myAttrs) {
  return `${myAttrs.category}-${myAttrs.capacity}-${myAttrs.color}-${myAttrs.carrier}-${myAttrs.appearance}`;
}

export function parseIncredibleId(id) {
  const [category, ...attributes] = id.split('-');
  return {category, attributes};
}

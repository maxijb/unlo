import {Actors} from '@Common/constants/app';

export const getCartAllProductIds = carts => {
  const set = new Set();
  for (const c of carts) {
    set.add(getCartActualProductId(c));
    set.add(getCartDisplayProductId(c));
  }
  return Array.from(set).filter(r => !!r);
};

export const getProductId = (c, actor = Actors.buyer) => {
  return actor === Actors.buyer ? getCartDisplayProductId(c) : getCartActualProductId(c);
};

export const getCartActualProductId = c => {
  if (!c) {
    return null;
  }

  if (typeof c.get === 'function') {
    return c.get('variant_id') || c.get('product_id');
  }

  return c.variant_id || c.product_id;
};
export const getCartDisplayProductId = c => {
  if (!c) {
    return null;
  }

  if (typeof c.get === 'function') {
    return c.get('display_product_id') || c.get('variant_id') || c.get('product_id');
  }

  return c.display_product_id || c.variant_id || c.product_id;
};

export const calculateDisplaySubtotal = (items, reference) => {
  return calculateDisplaySubtotalAndShipping(items, reference).subtotal;
};

// Calculates cart totals while still in flight
// and price hasn't been applied to the cart yet
// that's why we read prices from the 'reference' map object
//
// If searching to calculate established order totals use order-utils.getActualOrderTotals
export const calculateDisplaySubtotalAndShipping = (items, reference) => {
  let shippingChargesTemp = 0;
  let subtotalProducts = 0;
  let subtotalWithoutPromoCode = 0;
  let promoCode = null;
  const subtotalTemp = items.reduce((sum, item) => {
    let cost = item.price;
    if (item.type === 'shipping') {
      shippingChargesTemp += item.price;
      subtotalWithoutPromoCode += item.price;
    } else if (item.type === 'product') {
      cost = (reference[getCartDisplayProductId(item)]?.price || 0) * item.quantity;
      subtotalProducts += cost;
      subtotalWithoutPromoCode += cost;
    } else if (item.type === 'code') {
      promoCode = item;
    }
    return sum + cost;
  }, 0);
  return {
    subtotalWithoutPromoCode,
    subtotal: subtotalTemp,
    shippingCharges: shippingChargesTemp,
    subtotalProducts,
    promoCode,
  };
};

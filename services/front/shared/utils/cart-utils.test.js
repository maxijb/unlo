import {getCartAllProductIds} from './cart-utils';

describe('getCartAllProductIds tests', () => {
  it('mixed case', () => {
    const result = getCartAllProductIds([
      {product_id: 2},
      {display_product_id: 4, variant_id: 3},
      {display_product_id: 5, variant_id: 4},
    ]);

    expect(result).toEqual([2, 3, 4, 5]);
  });

  it('with models', () => {
    const result = getCartAllProductIds([
      {get: () => 2},
      {display_product_id: 4, variant_id: 3},
      {display_product_id: 5, variant_id: 4},
      {get: () => 6},
    ]);

    expect(result).toEqual([2, 3, 4, 5, 6]);
  });
});

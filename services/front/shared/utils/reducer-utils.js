import decamelize from 'decamelize';
import {RPCActionPrefix} from '../../../common/constants/app';
import {rpcNames} from '../actions/api-actions';

/* Creates an RPC reducer
 * @param {string} rpcId
 * @param {reducer} reducers including optional keys: request, success and failure
 * @return {function} combined reducer
 */
export function createRPCReducer(rpcId, reducers = {}) {
  const names = rpcNames[rpcId];
  if (!names) {
    throw new Error(`Unexistent rpcId: ${rpcId}`);
  }
  const stages = ['start', 'success', 'failure'];
  Object.keys(reducers).forEach(key => {
    if (stages.indexOf(key) === -1) {
      throw new Error(
        `createRPCReducer doesn't accept "${key}" as a valid reducer key. Use [request, success, failure] as possible values`
      );
    }
  });

  return function rpcReducer(state = {}, action = {}) {
    // it'll dismiss all non-rpc actions
    if (action.type && action.type.substr(0, RPCActionPrefix.length) === RPCActionPrefix) {
      if (names.start === action.type && reducers.start) {
        return reducers.start(state, action);
      }
      if (names.success === action.type && reducers.success) {
        return reducers.success(state, action);
      }
      if (names.failure === action.type && reducers.failure) {
        return reducers.failure(state, action);
      }
    }
    return state;
  };
}

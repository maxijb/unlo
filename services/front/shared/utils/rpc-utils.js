import decamelize from 'decamelize';
import {RPCActionPrefix} from '../../../common/constants/app';
import {rpcNames} from '../actions/api-actions';

export function getRPCId(prefix, key) {
  return decamelize(`${prefix}${key}`).replace(/\./g, '_').toUpperCase();
}

/* Creates RPC action names for each rpcId
 * @param {object} rpcIds => {fecthAction: 'fecthAction'}
 * @return {object} {fecthAction: {
 *   stop: '@moon-rpc/FETCH_ACTION_STOP'
 *   start: '@moon-rpc/FETCH_ACTION_START'
 *   request: '@moon-rpc/FETCH_ACTION_REQUEST'
 *   success: '@moon-rpc/FETCH_ACTION_SUCCESS'
 *   failure: '@moon-rpc/FETCH_ACTION_FAILURE'
 * }}
 */
export function createActionNames(rpcIds) {
  return Object.keys(rpcIds).reduce((prev, id) => {
    const stages = ['start', 'success', 'failure'];
    prev[rpcIds[id]] = stages.reduce((group, stage) => {
      group[stage] = `${RPCActionPrefix}${rpcIds[id]}_${stage.toUpperCase()}`;
      return group;
    }, {});
    return prev;
  }, {});
}

export function createActionName(rpcId, stage) {
  return `${RPCActionPrefix}${rpcId}_${stage.toUpperCase()}`;
}

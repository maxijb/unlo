import {CsrfTokenURL, CsrfTokenHeader, CsrfTokenRefreshMs} from '../../../common/constants/app';
let CsrfTokenTimeout = null;
let CsrfToken = null;

/* Exported module: wrapps fetch to include csrf-token
 * If no token is found, it'll retry after getting a new token
 * @param url {string}
 * @param options {object} fetch native options
 * @param isRetry {boolean} decide whether to rerty on failed csrf or not (just once)
 */
const SecuredFetch = async (url, options, isRetry) => {
  const token = await getToken();
  options.headers = {
    ...options.headers,
    [CsrfTokenHeader]: token
  };
  // TODO: add fetch polifyl when necessary
  return fetch(url, options).then(async response => {
    // we'll just retry with a new csrf token once
    if (response.status === 403 && !isRetry) {
      const json = await response.clone().json();
      if (json && json.error === 'csrf') {
        CsrfToken = null;
        return SecuredFetch(url, options, true);
      }
    } else {
      // when user changes server will send a new csrf-token
      for (const entry of response.headers.entries()) {
        if (entry[0] === CsrfTokenHeader) {
          CsrfToken = entry[1];
        }
      }
    }
    return response;
  });
};

/* Gets a new csrf token
 * it will peiodically get a new token 1 minute before the previous token expires
 */
export async function getToken(refresh) {
  // if there's no token stored or we're refeshing by time
  if (!CsrfToken || refresh) {
    // fetch new token and store
    const tokenResponse = await fetch(CsrfTokenURL);
    let json;
    try {
      json = await tokenResponse.json();
    } catch (e) {
      return null;
    }

    CsrfToken = json.token;

    // Set timeout for next token
    clearTimeout(CsrfTokenTimeout);
    CsrfTokenTimeout = setTimeout(() => {
      getToken(true);
    }, CsrfTokenRefreshMs);
  }

  return CsrfToken;
}

// Get the new token on page load (just client side)
if (process.browser) {
  getToken();
}

export default SecuredFetch;

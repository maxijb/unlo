import React, {Component} from 'react';
import classnames from 'classnames';

import {clientCookieName} from '../../../../common/constants/app';
import {InputTypes, UITypes} from '@Common/constants/input';
import {WidgetPosition} from '@Widget/config/widget-ui-constants';
import {getUI, getCurrentScreen} from '../../utils/screen-utils';
import Modules from '../modules/modules';

import css from './body.scss';
import cssVariables from '../root/_vars-widget.scss';

export default class Body extends Component {
  renderUI(module, config, steps, key) {
    if (!module || !module.ui) {
      console.warn("Tried to render a module that doesn't exist", module);
      return null;
    }
    const UI = module.ui;
    const extraData = typeof module.extraData === 'function' ? module.extraData(this.props) : {};
    return (
      <UI
        key={key}
        {...this.props}
        steps={steps}
        config={config || {}}
        {...extraData}
        isClient={true}
      />
    );
  }

  renderScreen(screen, steps, hasGoBack) {
    const {
      status: {currentAction}
    } = this.props;

    const uis = hasGoBack ? [this.renderUI(Modules[UITypes.goBack], null, steps, '-1back')] : [];

    if (currentAction === UITypes.chat) {
      return this.renderUI(Modules[UITypes.chat], null, steps);
    }

    return uis.concat(
      screen.map((item, i) => {
        const ui = getUI(item);
        const config = typeof item === 'string' ? {ui: item} : item;
        return this.renderUI(Modules[ui], config, steps, `${i}${ui}`);
      })
    );
  }

  render() {
    const {
      socket,
      options: {screens, inline, bodyClassName, withinIframe, position},
      status: {currentAction, currentStep, maxWidth, maxHeight},
      isMobile,
      windowHeight,
      windowWidth
    } = this.props;

    const {screen, steps, showFooter} = getCurrentScreen(screens, currentAction, currentStep);
    const isChat = currentAction === UITypes.chat || screen.some(s => getUI(s) === UITypes.chat);
    const hasGoBack = false; //!isChat && screen !== screens[0];

    let style;
    if (isMobile) {
      style = {
        maxHeight: '100%',
        maxWidth: '100%',
        bottom: 0,
        right: 0,
        left: 0,
        top: 0,
        width: '100%',
        height: '100%'
      };
    } else {
      const maxBodyHeight = maxHeight
        ? `${
            maxHeight -
            parseInt(cssVariables.iframeViewportMargin) -
            parseInt(cssVariables.bodyRealBottom)
          }px`
        : `calc(100vh - ${parseInt(cssVariables.bodyRealBottom) * 2}px)`;

      const maxBodyWidth = maxWidth
        ? `${
            maxWidth -
            parseInt(cssVariables.iframeViewportMargin) -
            parseInt(cssVariables.iframeInnerPadding)
          }px`
        : `calc(100vw - ${cssVariables.iframeInnerPadding})`;

      style = {
        maxHeight: maxBodyHeight,
        maxWidth: maxBodyWidth
      };
    }

    return (
      <div
        ref={body => (this.body = body)}
        className={classnames(css.body, bodyClassName, {
          [css.isMobile]: isMobile,
          [css.inline]: inline,
          [css.left]: position === WidgetPosition.left,
          [css.hasGoBack]: hasGoBack,
          [css.standAlone]: !withinIframe,
          [css.hasChat]:
            currentAction === UITypes.chat || screen.some(s => getUI(s) === UITypes.chat)
        })}
        style={style}
      >
        <div
          ref={frame => (this.bodyFrame = frame)}
          className={classnames(css.bodyFrame, {
            [css.hasGoBack]: hasGoBack,
            [css.hasChat]:
              currentAction === UITypes.chat || screen.some(s => getUI(s) === UITypes.chat)
          })}
        >
          {this.renderScreen(screen, steps, hasGoBack)}
        </div>
        {showFooter && this.renderUI(Modules.footer, {ui: 'footer'}, [], 'footer')}
      </div>
    );
  }
}

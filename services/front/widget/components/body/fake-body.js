import React from 'react';
import classnames from 'classnames';

import css from './fake-body.scss';
import style from '../root/style.scss';
import Parent from '../root/parent';

export default class Body extends Parent {
  render() {
    const {CSSloaded} = this.state;
    return (
      <div
        id={style.customerWidget}
        className={style.customerWidget}
        ref={elem => (this.elem = elem)}
      >
        {CSSloaded && <div className={css.fakeBody}>{this.props.children}</div>}
      </div>
    );
  }
}

import React from 'react';
import classnames from 'classnames';
import {UserFilesCdnURL} from '@Common/utils/statif-assets-utils';
import {getAvatar} from '@Common/utils/message-utils';
import Gravatar from 'react-gravatar';
import {GravatarFallback} from '@Common/constants/app';

import css from './avatar.scss';

export default function Avatar({
  src,
  options = {},
  operators = {},
  owner,
  size = 43,
  className = '',
  style = {},
  user = {}
}) {
  if (size) {
    style.borderRadius = style.width = style.height = `${size}px`;
  }
  if (!src) {
    src = getAvatar({options, operators, owner, user, includeUserGravatar: true});
  }

  if (typeof src === 'string') {
    style.backgroundImage = `url(${src})`;
    return <div className={classnames(css.avatar, className)} style={style} />;
  }

  return (
    <Gravatar
      className={classnames(css.avatar, className)}
      email={src ? src.email : null}
      default={GravatarFallback}
    />
  );
}

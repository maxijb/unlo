import React from 'react';
import classnames from 'classnames';
import {noop} from '@Common/utils/generic-utils';

import css from './label.scss';

export default function Label({text, color, background, className = '', style = {}, onClick}) {
  if (color) {
    style.color = color;
  }
  if (background) {
    style.background = background;
  }

  return (
    <div
      className={classnames(css.label, className, {[css.clickable]: !!onClick})}
      onClick={onClick || noop}
      style={style}
    >
      {text}
    </div>
  );
}

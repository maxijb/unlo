import React from 'react';

export default class GenericComponent extends React.Component {
  shouldComponentUpdate(oldProps) {
    if (this.checkUpdateFields) {
      for (const field of this.checkUpdateFields) {
        if (oldProps[field] !== this.props[field]) {
          return true;
        }
      }
    } else {
      return oldProps !== this.props;
    }
    return false;
  }
}

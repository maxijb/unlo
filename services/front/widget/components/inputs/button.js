import React, {Component} from 'react';
import classnames from 'classnames';
import get from 'lodash.get';

import {noop} from '@Common/utils/generic-utils';

import css from './input.scss';

export default class Button extends Component {
  static defaultProps = {
    kind: 'primary',
    disabled: false,
    onClick: noop
  };

  render() {
    const {kind, disabled, onClick, inline, grow, children, equal, className, ribbon} = this.props;
    return (
      <div
        className={classnames(css.buttonContainer, className, {
          [css.inline]: inline,
          [css.grow]: grow,
          [css.equal]: equal
        })}
      >
        <button
          className={classnames(css.button, css[`button-${kind}`], {
            [css.disabled]: disabled
          })}
          disabled={disabled}
          onClick={onClick}
        >
          {children}
        </button>
        {ribbon && <div className={css.buttonRibbon} />}
      </div>
    );
  }
}

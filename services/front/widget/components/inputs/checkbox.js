import React, {Component} from 'react';
import classnames from 'classnames';
import get from 'lodash.get';

import {noop} from '@Common/utils/generic-utils';

import css from './input.scss';

export default class Checkbox extends Component {
  static defaultProps = {
    type: 'text',
    checked: false,
    error: null,
    onChange: noop,
    onBlur: noop,
    onFocus: noop
  };

  render() {
    const {checked, id, error, label, disabled, onChange, onFocus, onBlur} = this.props;

    return (
      <div className={css.inputContainer}>
        <input
          className={css.checkbox}
          type="checkbox"
          id={id}
          checked={checked}
          disabled={disabled}
          onChange={onChange}
          onFocus={onFocus}
          onBlur={onBlur}
        />
        {label && (
          <label className={css.checkboxLabel} htmlFor={id}>
            {label}
          </label>
        )}
        {error && <div className={css.error}>{error}</div>}
      </div>
    );
  }
}

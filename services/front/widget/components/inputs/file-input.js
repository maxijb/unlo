import React from 'react';

import {noop} from '@Common/utils/generic-utils';

import css from './input.scss';

export default class FileInput extends React.Component {
  static defaultProps = {
    callback: noop,
    onUploadStart: noop,
  };

  constructor(props) {
    super(props);
    this.state = {loading: false};
  }

  onClick = () => {
    this.fileInput.click();
  };

  uploadImage = () => {
    const {num} = this.state;
    const {callback, onUploadStart} = this.props;
    var formData = new FormData(this.form);

    onUploadStart(formData);

    //this.setState({loading: true});
  };

  render() {
    const {t, type, showLoader} = this.props;
    const {loading} = this.state;

    return (
      <div>
        <form
          ref={form => (this.form = form)}
          action="javascript:void(0);"
          encType="multipart/form-data"
        >
          <input
            type="file"
            name="file"
            accept="image/*"
            ref={elem => (this.fileInput = elem)}
            className={css.hidden}
            onChange={this.uploadImage}
          />
          <input type="hidden" name="type" value={type} />
        </form>
      </div>
    );
  }
}

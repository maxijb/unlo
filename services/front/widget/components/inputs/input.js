import React, {Component} from 'react';
import classnames from 'classnames';
import get from 'lodash.get';

import {noop} from '@Common/utils/generic-utils';

import css from './input.scss';

export default class Input extends Component {
  static defaultProps = {
    placeholder: '',
    type: 'text',
    value: '',
    error: null,
    onChange: noop,
    onBlur: noop,
    onFocus: noop
  };

  render() {
    const {
      placeholder,
      label,
      type,
      value,
      error,
      disabled,
      onChange,
      onFocus,
      onBlur
    } = this.props;
    return (
      <div className={css.inputContainer}>
        {label && <div className={css.label}>{label}</div>}
        <input
          className={css.input}
          type={type}
          value={value}
          disabled={disabled}
          placeholder={placeholder}
          onChange={onChange}
          onFocus={onFocus}
          onBlur={onBlur}
        />
        {error && <div className={css.error}>{error}</div>}
      </div>
    );
  }
}

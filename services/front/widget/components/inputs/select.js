import React, {Component} from 'react';
import classnames from 'classnames';
import get from 'lodash.get';

import {SelectTip} from '../display/svg-icons';
import {noop} from '@Common/utils/generic-utils';

import css from './input.scss';

export default class Select extends Component {
  static defaultProps = {
    placeholder: '',
    value: '',
    label: '',
    error: null,
    onChange: noop,
    onBlur: noop,
    onFocus: noop,
    items: []
  };

  renderOptions() {
    const {items} = this.props;
    const opts = [];
    items.forEach(item => {
      opts.push(
        <option key={item.value || item.display || item} value={item.value || item.display || item}>
          {item.display || item}
        </option>
      );
    });
    return opts;
  }

  render() {
    const {
      items,
      label,
      placeholder,
      value,
      error,
      disabled,
      onChange,
      onFocus,
      onBlur
    } = this.props;
    return (
      <div className={classnames(css.inputContainer, css.selectContainer)}>
        {label && <div className={css.label}>{label}</div>}
        <div className={css.selectInputContainer}>
          <SelectTip className={css.selectpTip} />
          <select
            className={css.select}
            value={value}
            disabled={disabled}
            placeholder={placeholder}
            onChange={onChange}
            onFocus={onFocus}
            onBlur={onBlur}
          >
            {this.renderOptions()}
          </select>
        </div>
        {error && <div className={css.error}>{error}</div>}
      </div>
    );
  }
}

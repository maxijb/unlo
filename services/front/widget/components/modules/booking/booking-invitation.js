import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import tUtils from 'timestamp-utils';

import {noop} from '@Common/utils/generic-utils';

import moduleCss from '../module.scss';

import GenericModule from '../generic-module';
import Promotion from '../promotion/promotion';
import {PromotionAction, PromotionLinkTarget} from '../../../config/widget-ui-constants';
import {defaultPrintDate, defaultPrintTime} from '@Common/utils/date-utils';
import BookingItem from './booking-item';
import Button from '../../inputs/button';

import css from './booking-invitation.scss';

export default class BookingInvitation extends GenericModule {
  static propTypes = {
    config: PropTypes.shape({
      action: PropTypes.string,
      href: PropTypes.string,
      template: PropTypes.string,
      title: PropTypes.string,
      target: PropTypes.string,
      image: PropTypes.string,
      text: PropTypes.string,
      button: PropTypes.node
    }),
    isWithinChat: PropTypes.bool
  };

  static defaultProps = {
    ...GenericModule.defaultProps,
    methods: {
      goToNextStep: noop
    }
  };

  createBooking = () => {
    this.openBooking(null);
  };

  openBooking = id => {
    const {
      config: {actionName}
    } = this.props;

    if (id) {
      this.props.methods.goToNextStep(actionName, {selectedBooking: id});
    } else {
      this.props.methods.goToNextStep(actionName);
    }
  };

  render() {
    const {
      config: {action, title, text, image, button, template},
      status: {bookings},
      messages,
      children,
      isWithinChat,
      options
    } = this.props;

    const items = bookings.list.items();

    return (
      <Promotion
        config={{
          title: '!@bookingInvitationTitle',
          subtitle: '!@bookingInvitationDescription'
        }}
      >
        <Button onClick={this.createBooking}>
          {this.print('!@bookingInvitationActionButton')}
        </Button>
        {!items.length ? null : (
          <div className={css.reservationsTitle}>{this.print('!@yourReservations')}</div>
        )}
        <table className={moduleCss.moduleTable}>
          <tbody>
            {items.map(booking => (
              <BookingItem
                key={booking.id}
                booking={booking}
                options={options}
                openBooking={this.openBooking}
                messages={messages}
              />
            ))}
          </tbody>
        </table>
      </Promotion>
    );
  }
}

import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import tUtils from 'timestamp-utils';

import {cdnURL} from '@Common/utils/statif-assets-utils';
import {noop} from '@Common/utils/generic-utils';

import moduleCss from '../module.scss';

import {defaultPrintDateWithDayName, defaultPrintTime} from '@Common/utils/date-utils';

import css from './booking-item.scss';

export default class BookingItem extends PureComponent {
  static defaultProps = {
    booking: {},
    openBooking: noop,
    options: {},
    messages: {}
  };

  openBooking = () => {
    const {
      booking: {id},
      openBooking
    } = this.props;

    openBooking(id);
  };

  render() {
    const {booking, options, messages} = this.props;

    return (
      <tr key={booking.id} onClick={this.openBooking} className={css.bookingItem}>
        <td className={css.date}>{defaultPrintDateWithDayName(booking.startDate, messages)}</td>
        <td className={css.time}>{defaultPrintTime(booking.startDate)}</td>
        <td className={css.people}>
          <img src={cdnURL('/static/icons/dashboard/people.png')} className={css.icon} />
          {booking.value}
        </td>
        <td className={css.label}>
          <img src={cdnURL('/static/icons/dashboard/area.png')} className={css.icon} />
          {booking.label}
        </td>
      </tr>
    );
  }
}

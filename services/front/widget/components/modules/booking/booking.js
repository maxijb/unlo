import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import isEmpty from 'is-empty';
import get from 'lodash.get';

import Defaults from '@Widget/config/defaults';
import {MessageOwners} from '@Common/constants/socket-status';
import {noop} from '@Common/utils/generic-utils';
import {getGuestsRange} from '@Common/utils/bookings-utils';
import {PRISTINE_FIELD} from '@Common/constants/input';
import {
  embeddedToAbsUTC,
  utcToEmbeddedTZ,
  addMinutes,
  isDayClosed,
  calculateTimeSlots,
  absDateAndTimeToEmbedded,
  defaultPrintDateWithDayName,
  defaultAbsPrintTime
} from '@Common/utils/date-utils';
import {getGoogleCalendarLink} from '@Common/utils/urls';

import GenericModule from '../generic-module';
import Contact from '../contact/contact';
import Promotion from '../promotion/promotion';
import {PromotionAction, PromotionLinkTarget} from '../../../config/widget-ui-constants';
import Button from '../../inputs/button';
import Select from '../../inputs/select';
import Calendar from '../calendar/calendar';
import MiniHeader from '../header/mini-header';
import {Guests, Area, Calendar as CalendarIcon, Clock} from '../../display/svg-icons';

import css from './booking.scss';
import moduleCss from '../module.scss';

const Steps = {
  date: {
    number: 1,
    title: '!@bookingModuleTitle',
    description: '!@bookingModuleDateDescription'
  },
  time: {
    number: 2,
    title: '!@bookingModuleTitle',
    description: '!@bookingModuleTimeDescription'
  },
  contact: {
    number: 3,
    numberPromotion: 1,
    title: '!@bookingModuleTitle',
    promotionTitle: '!@claimOffer',
    description: '!@bookingModuleContactDescription',
    promotionDescription: '!@bookingModuleContactDescription'
  },
  confirmation: {
    number: 4,
    numberPromotion: 2,
    title: '!@bookingModuleTitleReserved',
    promotionTitle: '!@offerSuccess',
    description: '!@bookingSuccessDescription',
    promotionDescription: '!@offerSuccessDescription'
  }
};

export default class Booking extends GenericModule {
  static mustBeWrappedByMessage = true;
  static mustWaitAfterMessage = true;
  static propTypes = {
    config: PropTypes.shape({
      action: PropTypes.string,
      href: PropTypes.string,
      template: PropTypes.string,
      title: PropTypes.string,
      target: PropTypes.string,
      image: PropTypes.string,
      text: PropTypes.string,
      button: PropTypes.node
    }),
    isWithinChat: PropTypes.bool
  };

  static defaultProps = {
    ...GenericModule.defaultProps,
    methods: {
      goToNextStep: noop,
      submitBooking: noop,
      goToFirstScreen: noop
    },
    loadBooking: noop
  };

  constructor(props) {
    super(props);

    const {
      status,
      status: {
        bookings: {list},
        actionData: {selectedBooking, promotion}
      },
      options,
      options: {openHours, bookings, timezone},
      methods,
      loadBooking,
      isWithinChat,
      msg
    } = props;

    const date = embeddedToAbsUTC(utcToEmbeddedTZ(new Date(), timezone));
    const timeSlots = calculateTimeSlots(date.getTime(), {openHours, bookings});
    const labels = get(bookings, 'labels');
    const isOnlyPromotion = promotion && promotion.enableReservations === false;
    // When a booking is selected, try to get the data from there
    // Only in chat and full we populate the settings from the msg
    const bookingId = selectedBooking || get(msg, 'data.id');

    const booking = list && list.get ? list.get(bookingId) : null;
    const data = bookingId && booking ? {...booking} : null;
    if (booking) {
      data.startDate = embeddedToAbsUTC(data.startDate);
      data.endDate = embeddedToAbsUTC(data.endDate);
    } else if (!booking && bookingId) {
      this.loadBooking(props, bookingId);
    }
    const isFull = !isEmpty(bookingId);

    const identity =
      booking && booking.identityId
        ? get(status, ['user', 'identities', booking.identityId], null)
        : null;

    this.state = {
      // initial state
      // we show the confirmation if there's an id but it's not selected to edit
      selecting: isFull && !selectedBooking ? 'confirmation' : isOnlyPromotion ? 'contact' : 'date',
      loadingBooking: isFull && isEmpty(data),
      notAvailable: isFull && isEmpty(data),
      errors: {},
      bookingId,

      // Setting default values
      value: 2,
      time: timeSlots[0],
      startDate: date,
      endDate: date,
      label: Array.isArray(labels) ? labels[0].display || labels[0] : null,

      // Overriding defaults with loaded data if available
      ...(isFull ? data : {}),
      // loading identity
      ...identity,

      // More options
      valueRange: getGuestsRange(options),
      timeSlots,
      labels
    };
  }

  loadBooking(props, bookingId) {
    const {methods, loadBooking} = props;
    // methods.loadbooking is available on widget, loadBooking on dashboard
    (methods.loadBooking || loadBooking)(bookingId);
    setTimeout(() => this.removeLoadingMessage(), 15000);
  }

  componentWillReceiveProps(nextProps) {
    const {
      status: {user, bookings},
      msg
    } = nextProps;
    if (this.state.loadingBooking && bookings !== this.props.status.bookings) {
      if (get(bookings, ['list', 'get'])) {
        const booking = bookings.list.get(this.state.bookingId);
        if (booking) {
          this.setState({...booking, loadingBooking: false, notAvailable: false});

          if (booking.identityId) {
            const identity = get(user, ['identities', booking.identityId], null);
            this.setState({...identity});
          }
        }
      }
    }

    // when updating booking on a remote location, data.id might change
    const bookingId = get(msg, 'data.id');
    if (bookingId && bookingId !== get(this.props, 'msg.data.id')) {
      this.setState({selecting: 'confirmation', bookingId});
      if (!bookings.list.get(bookingId)) {
        this.setState({loadingBooking: true});
        this.loadBooking(nextProps, bookingId);
      }
    }
  }

  removeLoadingMessage() {
    this.setState({loadingBooking: false});
  }

  next = () => {
    let {selecting, time} = this.state;
    if (selecting === 'date') {
      selecting = 'time';
    } else if (selecting === 'time') {
      if (!time || !time.trim()) {
        return this.setState({errors: {time: this.print('!@bookingErrorTimeRequired')}});
      }
      selecting = 'contact';
    }
    this.selectScreen(selecting);
  };

  back = () => {
    let {selecting} = this.state;
    if (selecting === 'contact') {
      selecting = 'time';
    } else if (selecting === 'time') {
      selecting = 'date';
    }
    this.selectScreen(selecting);
  };

  edit = () => {
    this.selectScreen('date');
  };

  selectScreen = (selecting, data) => {
    this.setState({
      selecting,
      // reset state in between steps
      cancelling: false,
      errors: {},
      // extra properties that are needed for the next step
      ...data
    });
  };

  setDate = (startDate, endDate) => {
    const {
      options: {openHours, bookings, specialOpenHours}
    } = this.props;
    const timeSlots = calculateTimeSlots(startDate, {openHours, specialOpenHours, bookings});
    this.setState({
      startDate: new Date(startDate),
      endDate: new Date(endDate),
      timeSlots,
      time: timeSlots[0]
    });
  };

  setTime = evt => {
    this.setState({time: evt.target.value});
  };

  setValue = evt => {
    this.setState({value: evt.target.value});
  };

  setLabel = evt => {
    this.setState({label: evt.target.value});
  };

  initBooking = () => {
    this.refs.contact.submit();
  };

  submitBooking = contactInfo => {
    const {startDate: sd, endDate: ed, time, label, value, id, token} = this.state;
    const {
      status: {
        bookings: {list},
        actionData: {selectedBooking, promotion}
      },
      conversation,
      msg,
      isWithinChat,
      methods: {submitBooking},
      options: {timezone, bookings}
    } = this.props;

    const isOnlyPromotion = promotion && promotion.enableReservations === false;
    const duration = get(bookings, 'duration', Defaults.defaultBookingDuration);

    const preloadedData = selectedBooking
      ? list.get(selectedBooking) || {}
      : get(msg, 'data') || {};

    const startDate = absDateAndTimeToEmbedded(sd, time, timezone);
    const endDate = utcToEmbeddedTZ(addMinutes(startDate, duration), timezone);

    const data = isOnlyPromotion
      ? {
          promotion: promotion.title || promotion.inlineTitle,
          status: 'VOUCHER',
          ...contactInfo.data
        }
      : {
          startDate,
          endDate,
          timezone,
          time,
          label,
          value,
          promotion: promotion ? promotion.title || promotion.inlineTitle : undefined,
          id: preloadedData.id,
          token: preloadedData.id ? list.get(preloadedData.id).token : null,
          ...contactInfo.data
        };

    if (isWithinChat) {
      submitBooking({
        id: msg.id,
        interactionId: conversation.id,
        data
      }).then(this.updateBookingId);
    } else {
      submitBooking({data}).then(this.updateBookingId);
    }

    this.setState({selecting: 'confirmation', ...contactInfo.data});
  };

  updateBookingId = resp => {
    if (resp && resp.id) {
      this.setState({bookingId: resp.id});
    }
  };

  disableDates = day => {
    return isDayClosed(day, this.props.options);
  };

  cancelBooking = () => {
    this.setState({cancelling: true});
  };

  doCancelReservation = () => {
    const {bookingId: id} = this.state;
    const {
      status: {
        bookings: {list}
      },
      methods: {cancelBooking}
    } = this.props;

    const booking = list.get(id);
    if (booking && booking.token) {
      this.setState({doCancelling: true});
      const {token} = booking;

      cancelBooking({token, id}).then(error => {
        this.setState({doCancelling: false});
        this.setState({isCancelled: true});
      });
    }
  };

  renderCalendarLinks() {
    const {startDate: sd, endDate: ed, time} = this.state;
    const {
      options: {timezone, bookings},
      options
    } = this.props;

    const duration = get(bookings, 'duration', Defaults.defaultBookingDuration);
    const startDate = new Date(absDateAndTimeToEmbedded(sd, time, timezone));
    const endDate = addMinutes(startDate, duration);

    return (
      <div className={css.calendarLinks}>
        <a
          href={getGoogleCalendarLink(startDate, endDate, options)}
          target="_blank"
          rel="nofollow noreferrer"
        >
          {this.print('!@addToGoogleCalendar')}
        </a>
      </div>
    );
  }

  renderDate() {
    const {isWithinChat} = this.props;
    const {startDate, endDate} = this.state;

    return (
      <div key="date" className={classnames({[moduleCss.innerModule]: !isWithinChat})}>
        <Calendar
          onChange={this.setDate}
          startDate={startDate.getTime()}
          endDate={endDate.getTime()}
          disableDates={this.disableDates}
          dayLabels={this.getMessage('shorterDays')}
          monthLabels={this.getMessage('months')}
        />
        <div className={css.buttonHolder}>
          <Button onClick={this.next}>{this.print('!@bookingModuleNextButton')}</Button>
        </div>
      </div>
    );
  }

  renderTime() {
    const {isWithinChat} = this.props;
    const {time, value, label, labels, valueRange, timeSlots, errors} = this.state;
    return (
      <div key="time" className={classnames({[moduleCss.innerModule]: !isWithinChat})}>
        <Select
          items={timeSlots}
          error={errors.time}
          value={time}
          label={this.print('!@bookingTimeLabel')}
          onChange={this.setTime}
        />
        <Select
          items={valueRange}
          value={value}
          label={this.print('!@bookingValueLabel')}
          onChange={this.setValue}
        />
        {!isEmpty(labels) && labels.length > 1 && (
          <Select
            items={labels}
            label={this.print('!@bookingLabelLabel')}
            value={label}
            onChange={this.setLabel}
          />
        )}
        <div className={css.buttonHolder}>
          <Button grow inline kind="secondary" onClick={this.back}>
            {this.print('!@bookingModuleBackButton')}
          </Button>
          <Button grow inline onClick={this.next}>
            {this.print('!@bookingModuleNextButton')}
          </Button>
        </div>
      </div>
    );
  }

  renderContact() {
    const {methods, ...props} = this.props;
    const {
      status: {
        actionData: {promotion}
      }
    } = this.props;
    const {name, email} = this.state;

    const isOnlyPromotion = promotion && promotion.enableReservations === false;
    //TODO :  pass booking identity when editing
    //name={name}
    //email={email}
    return (
      <div key="contact" className={classnames({[moduleCss.innerModule]: !props.isWithinChat})}>
        <Contact
          {...props}
          isWithinChat={true}
          showButton={false}
          showPhone={true}
          ref="contact"
          requireName={true}
          isBooking={true}
          methods={{
            submitContactData: this.submitBooking
          }}
        />
        <div className={css.buttonHolder}>
          {!isOnlyPromotion && (
            <Button inline grow equal kind="secondary" onClick={this.back}>
              {this.print('!@bookingModuleBackButton')}
            </Button>
          )}
          <Button inline grow equal onClick={this.initBooking}>
            {this.print(isOnlyPromotion ? '!@offerClaimButton' : '!@bookingModuleBookButton')}
          </Button>
        </div>
      </div>
    );
  }

  renderFacts() {
    const {
      state,
      state: {selecting, startDate}
    } = this;
    return (
      <div className={css.facts}>
        {selecting !== 'date' && (
          <div>
            <CalendarIcon />
            {defaultPrintDateWithDayName(startDate)}
          </div>
        )}
        {(selecting === 'contact' || selecting === 'confirmation') && (
          <React.Fragment>
            <div>
              <Clock />
              {state.time}
            </div>
            {state.label && (
              <div>
                <Area />
                {state.label}
              </div>
            )}
            <div>
              <Guests /> {state.value}
            </div>
          </React.Fragment>
        )}
      </div>
    );
  }

  renderCancelled() {
    const {
      methods: {goToFirstScreen},
      isWithinChat
    } = this.props;

    return (
      <div className={css.header}>
        <div className={css.title}>{this.print('!@bookingIsCancelled')}</div>
        <div>bla bla</div>
        {!isWithinChat && (
          <Button kind="primary" className={css.finalButton} onClick={goToFirstScreen}>
            {this.print('!@takeMeBack')}
          </Button>
        )}
      </div>
    );
  }

  renderConfirmation() {
    const {
      state,
      state: {
        startDate,
        loadingBooking,
        notAvailable,
        cancelling,
        doCancelling,
        isCancelled,
        status
      },
      props: {
        isWithinChat,
        options,
        methods: {goToFirstScreen},
        status: {
          actionData: {promotion}
        }
      }
    } = this;

    let content = <div>{this.print('!@bookingLoadingMessage')}</div>;
    const isOnlyPromotion = promotion && promotion.enableReservations === false;

    if (isCancelled || status === 'CANCELLED') {
      return this.renderCancelled();
    } else if (!loadingBooking) {
      content = notAvailable ? (
        <div>{this.print('!@bookingNotAvailable')}</div>
      ) : (
        <div
          className={classnames({
            [moduleCss.innerModule]: !isWithinChat,
            [css.inChat]: isWithinChat,
            [css.successDetails]: true
          })}
        >
          {isOnlyPromotion ? (
            this.print('!@offerSuccessExplanation')
          ) : (
            <React.Fragment>
              <div className={css.successHeader}>
                <div>{this.print('!@bookingTableReservationFor')}</div>
                <div className={css.name}>{state.displayName || state.name}</div>
                <div>
                  <a onClick={this.edit}>{this.print('!@bookingEdit')}</a>
                  {isWithinChat && (
                    <a style={{marginLeft: '20px'}} onClick={this.cancelBooking}>
                      {this.print('!@bookingCancel')}
                    </a>
                  )}
                </div>
              </div>
              {this.renderFacts()}
              {this.renderCalendarLinks()}
            </React.Fragment>
          )}
          {!isWithinChat && (
            <React.Fragment>
              <Button kind="primary" className={css.finalButton} onClick={goToFirstScreen}>
                {this.print('!@takeMeBack')}
              </Button>
              {!isOnlyPromotion && (
                <Button
                  kind="secondary"
                  className={css.finalButton}
                  onClick={this.cancelBooking}
                  loading={doCancelling}
                >
                  {this.print('!@bookingCancel')}
                </Button>
              )}
            </React.Fragment>
          )}
          {cancelling && (
            <div className={css.confirmCancellation}>
              <div>{this.print('!@sureCancelReservation')}</div>
              <div>
                <a onClick={this.doCancelReservation}>
                  {this.print('!@doCancelReservationWidget')}
                </a>
              </div>
            </div>
          )}
        </div>
      );
    }

    //if (isWithinChat) {
    return <React.Fragment key="confirmation">{content}</React.Fragment>;
    //}

    //return (
    //    <div className={css.header}>
    //      <div className={css.title}>{this.print('!@bookingSuccessTitle')}</div>
    //      <div className={css.statusBar}>{this.print('!@bookingSuccessDescription')}</div>
    //
    //    //      {content}
    //    //    </div>
    //);
  }

  renderStatusBar() {
    const {
      config: {text, title},
      status: {
        actionData: {promotion}
      },
      isWithinChat
    } = this.props;

    const {selecting, date, time, label, value} = this.state;
    const step = Steps[selecting];

    const isOnlyPromotion = promotion && promotion.enableReservations === false;
    const message = this.print(
      text,
      isOnlyPromotion ? step.promotionDescription : step.description
    );

    if (isWithinChat) {
      return (
        <div className={css.statusBar} key="statusBar">
          {step.number < Steps.confirmation.number && (
            <div className={css.stepTitle}>{this.print('!@stepN', null, {step: step.number})}</div>
          )}
          {message}
        </div>
      );
    }

    const titleText = this.print(title, isOnlyPromotion ? step.promotionTitle : step.title);
    return (
      <div className={css.header}>
        <div className={css.title}>{titleText}</div>
        <div>{message}</div>
        {selecting !== 'confirmation' && !isOnlyPromotion && this.renderFacts()}
      </div>
    );
  }

  renderMiniHeader() {
    const {
      methods: {goToFirstScreen, close},
      status: {
        actionData: {promotion}
      },
      isMobile
    } = this.props;
    const {selecting} = this.state;
    const step = Steps[selecting];

    const isOnlyPromotion = promotion && promotion.enableReservations === false;

    const subtitle =
      selecting !== 'confirmation'
        ? this.print('!@stepNofX', {
            step: isOnlyPromotion ? step.numberPromotion : step.number,
            total: isOnlyPromotion ? 2 : 3
          })
        : this.print(!isOnlyPromotion ? '!@bookingCompleted' : '!@offerClaimed');

    return (
      <MiniHeader
        title={this.print(!isOnlyPromotion ? '!@bookingOnlineReservation' : '!@claimOffer')}
        subtitle={subtitle}
        goBackAction={goToFirstScreen}
        isMobile={isMobile}
        close={close}
      />
    );
  }

  render() {
    const {
      config: {title},
      status,
      isWithinChat,
      status: {
        actionData: {promotion}
      }
    } = this.props;
    const {selecting, isCancelled} = this.state;

    const isOnlyPromotion = promotion && promotion.enableReservations === false;

    return (
      <div>
        {!isWithinChat && this.renderMiniHeader()}
        {promotion && (
          <div className={css.promotionHolder}>
            <Promotion
              {...this.props}
              isWithinChat={true}
              config={{
                ...promotion,
                rules: null,
                template: 'image-right',
                button: null,
                action: null,
                inlineTitle: (
                  <div className={css.promotionTitle}>
                    {promotion.title || promotion.inlineTitle}
                  </div>
                ),
                title: null,
                text: null
              }}
            />
          </div>
        )}
        <div
          className={classnames({
            [css.bookingModule]: !isWithinChat,
            [css.isWithinChat]: isWithinChat
          })}
        >
          {!isCancelled && this.state.status !== 'CANCELLED' && this.renderStatusBar()}
          {[
            selecting === 'date' && this.renderDate(),
            selecting === 'time' && this.renderTime(),
            selecting === 'contact' && this.renderContact(),
            selecting === 'confirmation' && this.renderConfirmation()
          ]}
          {promotion && promotion.text && <div className={css.promotionText}>{promotion.text}</div>}
          {promotion && promotion.rules && <div className={css.rules}>{promotion.rules}</div>}
        </div>
      </div>
    );
  }
}

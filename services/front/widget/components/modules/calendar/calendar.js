import React, {PureComponent} from 'react';
import classnames from 'classnames';
import {noop} from '@Common/utils/generic-utils';
import {number, func, bool, arrayOf, string} from 'prop-types';
import {
  initMonth,
  parseRange,
  getDays,
  dateIsBetween,
  dateIsOut,
  getDateWithoutTime
} from '@Common/utils/date-utils';
import t from 'timestamp-utils';

// Components
import DateDetails from './date-details';
import Navigation from './navigation';

import css from './calendar.scss';

class Calendar extends PureComponent {
  state = {};

  constructor(props) {
    super(props);
    const {startDate, endDate, initialWeekDay, initialTimestamp, onLoad, period} = props;
    t.setTimezone('UTC');
    const newState = initMonth(startDate || initialTimestamp, initialWeekDay, period);
    this.state = {
      ...newState,
      ...parseRange(startDate, endDate)
    };
    onLoad(newState);
  }

  setTimestamp = ts => {
    const {period, initialWeekDay, onLoad} = this.props;
    const newState = initMonth(ts, initialWeekDay, period);
    this.setState(newState);
    onLoad(newState);
  };

  onClickDay = day => {
    const {startDate, endDate} = this.state;
    if (!this.props.range) {
      return this.update({startDate: day, endDate: day});
    }

    if (!startDate) {
      this.update({startDate: day});
    } else if (startDate && !endDate) {
      this.update(parseRange(startDate, day));
    } else {
      this.update({startDate: day, endDate: null});
    }
  };

  changeMonth = ({
    yearOffset = 0,
    monthOffset = 0,
    weekOffset = 0,
    period = 'month',
    cb = noop
  }) => {
    const {initialWeekDay} = this.props;
    const {firstMonthDay} = this.state;
    const timestamp =
      period === 'month'
        ? t.add(firstMonthDay, {months: monthOffset, years: yearOffset})
        : t.add(firstMonthDay, {days: weekOffset * 7});

    const newState = initMonth(timestamp, initialWeekDay, period);
    cb({...newState, timestamp});
    this.setState(newState);
  };

  update = ({startDate, endDate}) => {
    const sDate = startDate === undefined ? this.props.startDate : startDate;
    const eDate = endDate === undefined ? this.props.endDate : endDate;
    this.props.onChange(sDate, eDate);
  };

  disableDates = (day, today) => {
    const {disableDates, disablePastDates} = this.props;
    if (disablePastDates && day < today) {
      return true;
    }
    return disableDates(day);
  };

  getClassNames = (day, today, number) => {
    const {firstMonthDay, lastMonthDay} = this.state;
    const {disableDates, startDate, endDate, period} = this.props;
    const sDate = getDateWithoutTime(startDate);
    const eDate = getDateWithoutTime(endDate);

    const conditions = {
      [css['rlc-day-disabled']]: this.disableDates(day, today),
      [css['rlc-day-today']]: day === today,
      [css['rlc-day-inside-selection']]: dateIsBetween(day, sDate, eDate),
      [css['rlc-day-out-of-month']]:
        period === 'month' && dateIsOut(day, firstMonthDay, lastMonthDay),
      [css['rlc-day-selected']]: !endDate && sDate === day,
      [css['rlc-day-start-selection']]: endDate && sDate === day,
      [css['rlc-day-end-selection']]: endDate && eDate === day,
      [css['rlc-first-of-month']]: number === 1
      //[css[`rlc-day-${day}`]]: true
    };

    const classnames = Object.entries(conditions).reduce((prev, [className, valid]) => {
      if (valid) {
        prev.push(className);
      }
      return prev;
    }, []);

    return classnames;
  };

  renderWeekLabels = () => {
    const {maxWeekDayLabelLength} = this.props;
    const {dayLabels, initialWeekDay} = this.props;

    const labels = [];
    for (let i = initialWeekDay; i < initialWeekDay + 7; i++) {
      labels.push(dayLabels[i % 7]);
    }
    return (
      <div className={css['rlc-days-label']}>
        {labels.map(label => (
          <div className={css['rlc-day-label']} key={label}>
            {!maxWeekDayLabelLength ? label : label.slice(0, maxWeekDayLabelLength)}
          </div>
        ))}
      </div>
    );
  };

  renderDays = today => {
    const {firstDayToDisplay, firstMonthDay, lastMonthDay} = this.state;
    const {renderDay, period} = this.props;

    const allDays = getDays(firstDayToDisplay, period);

    const days = allDays.map(day => {
      const d = new Date(day);
      const number = d.getUTCDate(day);
      const month = d.getUTCMonth(day) + 1;
      const year = d.getUTCFullYear(day);
      const weekDay = d.getUTCDay(day);
      const outOfMonth = month !== new Date().getMonth() + 1;

      //const number = parseInt(t.getDay(day), 10);
      //const month = t.getMonth(day);
      //const year = t.getYear(day);
      //const weekDay = t.getWeekDay(day);
      return (
        <div
          className={classnames(
            css['rlc-day'],
            css[`day-${Math.ceil(allDays.length / 7)}rows`],
            ...this.getClassNames(day, today, number)
          )}
          key={day}
          onClick={() => !this.disableDates(day, today) && this.onClickDay(day)}
        >
          <div className={css.background}>
            <img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
          </div>
          {renderDay ? (
            renderDay(number, month, year, day, weekDay, day === today, outOfMonth, day < today)
          ) : (
            <span className={css.dayNumber}>{number}</span>
          )}
        </div>
      );
    });

    if (period === 'week' && renderDay) {
      const hoursLabels = (
        <div key="hours" className={classnames(css['rlc-day'], css['day-1rows'], css.hoursLabels)}>
          {renderDay('hours')}
        </div>
      );
      return [hoursLabels, ...days];
    }

    return days;
  };

  render = () => {
    const {month, year} = this.state;
    const {
      startDate: sDate,
      endDate: eDate,
      showDetails,
      onChange,
      disableDates,
      displayTime,
      dayLabels,
      monthLabels,
      timezone,
      showWeekLabels,
      className: classes,
      showNavigation,
      ...props
    } = this.props;
    const today = getDateWithoutTime(new Date().getTime());
    return (
      <div className={classnames(css['rlc-calendar'], classes)}>
        {showDetails && (
          <React.Fragment>
            <div className="rlc-details">
              {sDate && (
                <DateDetails
                  dayLabels={dayLabels}
                  monthLabels={monthLabels}
                  date={sDate}
                  displayTime={displayTime}
                  onTimeChange={date => this.update({startDate: date})}
                />
              )}
              {eDate && (
                <DateDetails
                  dayLabels={dayLabels}
                  monthLabels={monthLabels}
                  date={eDate}
                  displayTime={displayTime}
                  onTimeChange={date => this.update({endDate: date})}
                />
              )}
            </div>
          </React.Fragment>
        )}
        {showNavigation && (
          <Navigation
            monthLabels={monthLabels}
            month={month}
            year={year}
            onChange={this.changeMonth}
          />
        )}
        {showWeekLabels && this.renderWeekLabels()}

        <div className={css['rlc-days']}>{this.renderDays(today)}</div>
      </div>
    );
  };
}

Calendar.defaultProps = {
  showNavigation: true,
  startDate: null,
  endDate: null,
  showDetails: false,
  showWeekLabels: true,
  period: 'month',
  onLoad: noop,
  onChange: noop,
  disableDates: () => false,
  disablePastDates: true,
  maxWeekDayLabelLength: 2,
  displayTime: false,
  classnames: [],
  // first day shown in the calendar (0 sunday, 1, monday, 6 saturday)
  initialWeekDay: 0,
  dayLabels: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
  monthLabels: [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ],
  timezone:
    typeof window !== 'undefined' && window.Intl
      ? Intl.DateTimeFormat().resolvedOptions().timeZone
      : 'UTC'
};

Calendar.propTypes = {
  startDate: number,
  maxWeekDayLabelLength: number,
  initialWeekDay: number,
  endDate: number,
  onChange: func,
  disableDates: func,
  displayTime: bool,
  disablePastDates: bool,
  showDetails: bool,
  dayLabels: arrayOf(string),
  monthLabels: arrayOf(string),
  timezone: string,
  range: bool
};

export default Calendar;

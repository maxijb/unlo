import React, {Component} from 'react';
import {bool, arrayOf, string, number, func} from 'prop-types';
import times from 'lodash.times';
import t from 'timestamp-utils';
import {formatTime} from '@Common/utils/date-utils';

import css from './calendar.scss';

class DateDetails extends Component {
  onHoursChange = e => {
    const {date, onTimeChange} = this.props;
    onTimeChange(t.setHours(date, parseInt(e.target.value, 10)));
  };

  onMinutesChange = e => {
    const {date, onTimeChange} = this.props;
    onTimeChange(t.setMinutes(date, parseInt(e.target.value, 10)));
  };

  render = () => {
    const {date, displayTime, dayLabels, monthLabels} = this.props;
    const hours = t.getHours(date);
    const minutes = t.getMinutes(date);

    return (
      <div className={css['rlc-date-details-wrapper']}>
        <div className={css['rlc-date-details']}>
          <div className={css['rlc-date-number']}>{t.getDay(date)}</div>
          <div className={css['rlc-date-day-month-year']}>
            <div className={css['rlc-detail-day']}>{dayLabels[t.getWeekDay(date)]}</div>
            <div className={css['rlc-detail-month-year']}>
              {monthLabels[t.getMonth(date) - 1]}{' '}
              <span className={css['rlc-detail-year']}>{t.getYear(date)}</span>
            </div>
          </div>
        </div>
        {displayTime && (
          <div className={css['rlc-date-time-selects']}>
            <select onChange={this.onHoursChange} value={hours}>
              {times(24).map(hour => (
                <option value={formatTime(hour)} key={hour}>
                  {formatTime(hour)}
                </option>
              ))}
            </select>
            <span className={css['rlc-time-separator']}>:</span>
            <select onChange={this.onMinutesChange} value={minutes}>
              {times(60).map(minute => (
                <option value={formatTime(minute)} key={minute}>
                  {formatTime(minute)}
                </option>
              ))}
            </select>
          </div>
        )}
      </div>
    );
  };
}

DateDetails.propTypes = {
  date: number,
  displayTime: bool,
  dayLabels: arrayOf(string),
  monthLabels: arrayOf(string),
  onTimeChange: func
};

export default DateDetails;

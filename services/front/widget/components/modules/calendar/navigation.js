import React, {Component} from 'react';
import classnames from 'classnames';
import {func, arrayOf, string} from 'prop-types';
import {ArrowLeft} from '../../display/svg-icons';

import css from './calendar.scss';

class Navigation extends Component {
  prevYear = () => this.props.onChange({yearOffset: -1});
  prevMonth = () => this.props.onChange({monthOffset: -1});

  nextYear = () => this.props.onChange({yearOffset: 1});
  nextMonth = () => this.props.onChange({monthOffset: 1});

  render = () => {
    const {monthLabels, month, year, showYearNavigation} = this.props;

    return (
      <div className={css['rlc-month-and-year-wrapper']}>
        <div className={classnames(css['rlc-navigation-button-wrapper'], css['rlc-prevs'])}>
          {showYearNavigation && (
            <div
              className={classnames(css['rlc-navigation-button'], css['rlc-prev-year'])}
              onClick={this.prevYear}
            >
              {'<<'}
            </div>
          )}
          <div
            className={classnames(css['rlc-navigation-button'], css['rlc-prev-month'])}
            onClick={this.prevMonth}
          >
            <ArrowLeft />
            {/*'<'*/}
          </div>
        </div>
        <div className={css['rlc-month-and-year']}>
          {monthLabels[month - 1]} <span>{year}</span>
        </div>
        <div className={classnames(css['rlc-navigation-button-wrapper'], css['rlc-nexts'])}>
          <div
            className={classnames(css['rlc-navigation-button'], css['rlc-next-month'])}
            onClick={this.nextMonth}
          >
            <ArrowLeft />
            {/*'>'*/}
          </div>
          {showYearNavigation && (
            <div
              className={classnames(css['rlc-navigation-button'], css['rlc-next-year'])}
              onClick={this.nextYear}
            >
              {'>>'}
            </div>
          )}
        </div>
      </div>
    );
  };
}

Navigation.propTypes = {
  monthLabels: arrayOf(string),
  month: string,
  year: string,
  onChange: func
};

export default Navigation;

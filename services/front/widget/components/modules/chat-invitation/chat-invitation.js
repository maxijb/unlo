import React, {Component} from 'react';
import classnames from 'classnames';
import {noop} from '@Common/utils/generic-utils';
import {AppColors} from '@Common/constants/app';

import GenericModule from '../generic-module';
import ConversationItem from './conversation-item';
import Button from '../../inputs/button';
import Label from '../../display/label';

import css from '../module.scss';
import cicss from './chat-invitation.scss';

export default class ChatInvitation extends GenericModule {
  static defaultProps = {
    ...GenericModule.defaultProps,
    methods: {
      initConversation: noop,
      getOperator: noop
    }
  };

  constructor(props) {
    super(props);
    this.state = {showAll: false};
  }

  showAll = () => {
    this.setState({showAll: true});
  };

  initNewConversation = () => {
    const {
      ui,
      methods: {initConversation}
    } = this.props;
    initConversation(null, ui);
  };

  renderConversations() {
    const {
      status: {
        chat: {conversations},
        operators
      },
      ui,
      methods: {initConversation, getOperator},
      config,
      messages,
      options
    } = this.props;

    const {showAll} = this.state;

    const items = conversations.items();
    const content = [];
    const min = this.state.showAll ? 0 : Math.max(0, items.length - 3);
    for (let i = items.length - 1; i >= min; i--) {
      const item = items[i];
      content.push(
        <ConversationItem
          key={item.id}
          getOperator={getOperator}
          operators={operators}
          conversation={item}
          initConversation={initConversation}
          ui={ui}
          config={config}
          messages={messages}
          options={options}
          t={this.print}
        />
      );
    }

    return (
      <div>
        <ul>{content}</ul>
        {!showAll && items.length > 3 && (
          <div className={cicss.showAllLabel}>
            <Label
              text={this.print('!@showAllLabel')}
              color={AppColors.black60}
              background={AppColors.white20}
              onClick={this.showAll}
            />
          </div>
        )}
      </div>
    );
  }

  render() {
    const {initConversation, goToNextStep, ui} = this.props;

    return (
      <div className={css.module}>
        <div className={css.moduleTitle}>{this.print('!@chatInvitationTitle')}</div>
        {this.renderConversations()}
        <div>
          <Button kind="secondary" onClick={this.initNewConversation}>
            {this.print('!@newConversation')}
          </Button>
        </div>
      </div>
    );
  }
}

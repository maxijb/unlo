/* eslint-env node, jest */
import {mount} from 'enzyme';
import React from 'react';

import ChatInvitation from './chat-invitation';

describe('ChatInvitation Component', () => {
  const props = {};

  const wrapper = mount(<ChatInvitation {...props} />);

  it('ChatInvitation module rendered', () => {
    expect(wrapper.length).toBe(1);
  });
});

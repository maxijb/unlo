import React, {Component} from 'react';
import PropTypes from 'prop-types';
import get from 'lodash.get';

import Avatar from '../../display/avatar';
import {getOperatorName} from '@Common/utils/message-utils';
import {MessageOwners} from '@Common/constants/socket-status';
import {AppColors} from '@Common/constants/app';
import DateFrom from '@Components/inputs/date-from';
import Label from '../../display/label';
import ConversationLastMessage from './conversation-last-message';
import css from './conversation-item.scss';

export default class ConversationItem extends Component {
  static propTypes = {
    conversation: PropTypes.object,
    initConversation: PropTypes.func.isRequired,
    ui: PropTypes.object
  };

  static defaultProps = {
    conversation: {}
  };

  componentWillMount() {
    const {operators, conversation, getOperator} = this.props;
    const owner = get(conversation, 'messageOwner');
    if (
      owner &&
      owner !== MessageOwners.bot &&
      owner !== MessageOwners.client &&
      !operators.hasOwnProperty(owner)
    ) {
      getOperator(owner);
    }
  }

  initConversation = () => {
    const {conversation, initConversation, ui} = this.props;
    initConversation(conversation.id, ui);
  };

  getOperatorName = owner => {
    const {options, messages, operators} = this.props;
    return getOperatorName({owner, options, messages, operators});
  };

  renderNewMessages() {
    const {
      conversation: {lastSeenByUser, lastMessageId, messageOwner},
      t
    } = this.props;
    if (
      messageOwner !== MessageOwners.client &&
      lastMessageId &&
      (lastSeenByUser || 0) < lastMessageId
    ) {
      return <Label text={t('!@unreadMessages')} background={AppColors.negative} />;
    }

    return null;
  }

  render() {
    const {conversation, config, messages, options, operators, t} = this.props;

    if (!conversation) {
      return null;
    }

    return (
      <li className={css.conversationItem} onClick={this.initConversation}>
        <Avatar
          className={css.avatar}
          owner={conversation.messageOwner}
          options={options}
          operators={operators}
        />
        <div className={css.information}>
          <div className={css.informationLine}>
            <div className={css.lastMessageAuthor}>
              {this.getOperatorName(conversation.messageOwner)}
            </div>
            {this.renderNewMessages()}
            <DateFrom
              date={conversation.updatedAt}
              shortFormat
              className={css.lastMessageDate}
              t={t}
              isWidget={true}
            />
          </div>
          <ConversationLastMessage
            conversation={conversation}
            config={config}
            messages={messages}
          />
        </div>
      </li>
    );
  }
}

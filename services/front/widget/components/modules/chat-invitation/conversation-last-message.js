import React from 'react';
import PropTypes from 'prop-types';
import Emoji from 'react-emoji-render';

import GenericModule from '../generic-module';

import css from './conversation-item.scss';

export default class ConversationLastMessage extends GenericModule {
  static defaultProps = {
    ...GenericModule.defaultProps,
    conversation: {
      message: ''
    }
  };

  static propTypes = {
    conversation: PropTypes.object
  };

  renderMessage() {
    const {
      conversation: {message}
    } = this.props;

    if (typeof message === 'string') {
      return this.print(message);
    }
    if (!message) {
      return '';
    }
    if (message.display) {
      return this.print(message.display);
    } else if (message.prev) {
      return this.print(message.prev);
    } else if (message.ui) {
      return this.print(`!@defaultModuleDisplayMessage.${message.ui}`) || message.ui;
    }
    return JSON.stringify(message);
  }

  render() {
    return (
      <span className={css.lastMessage}>
        <Emoji text={this.renderMessage()} className={css.emoji} />
      </span>
    );
  }
}

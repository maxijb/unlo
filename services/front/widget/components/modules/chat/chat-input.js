import React from 'react';
import get from 'lodash.get';
import classnames from 'classnames';
import Textarea from 'react-textarea-autosize';
import {Picker} from './emojis/index';

import {MacroInitCharacter} from '@Common/constants/macros';
import {ArrowLeft, Emoji, Attachment, Preloader} from '../../display/svg-icons';
import FileInput from '../../inputs/file-input';
import css from './chat-input.scss';

export default class ChatInput extends React.Component {
  static defaultProps = {
    sendTyping: () => {},
    handleSubmit: () => {},
    initialValue: '',
    minRows: 1,
    maxRows: 5
  };

  constructor(props) {
    super(props);
    this.state = {
      field: props.initialValue || '',
      isOpenEmoji: false
    };
    this.dragCounter = 0;
  }

  componentWillReceiveProps(nextProps) {
    //this.focus();
    const element = nextProps.parent;

    if (!this.props.parent && element) {
      element.addEventListener('dragover', this.dragOver, true);
      element.addEventListener('dragleave', this.dragLeave, true);
      element.addEventListener('dragenter', this.dragEnter, true);
      element.addEventListener('drop', this.drop, true);
    }
  }

  componentWillUnmount() {
    //this.focus();
    const element = this.props.parent;
    if (element) {
      element.removeEventListener('dragover', this.dragOver);
      element.removeEventListener('dragleave', this.dragLeave);
      element.removeEventListener('dragenter', this.dragEnter);
      element.removeEventListener('drop', this.drop);
    }
  }

  dragOver = e => {
    e.preventDefault();
  };
  dragLeave = e => {
    e.preventDefault();
    this.dragCounter--;
    if (this.dragCounter <= 0) {
      this.setState({dropping: false});
    }
  };
  dragEnter = e => {
    e.preventDefault();
    this.setState({dropping: true});
    this.dragCounter++;
  };
  drop = e => {
    e.preventDefault();
    this.dragCounter = 0;
    this.setState({dropping: false});
    this.onFileDropped(e.dataTransfer.files);
  };

  openUpload = () => {
    this.fileInput.onClick();
  };

  focus = () => {
    if (this.input && this.input._ref) {
      this.input._ref.focus();
    }
  };

  handleChange = value => {
    const str = value.target ? value.target.value : value;
    this.setState({field: str});
  };

  handleSubmit = (event, value) => {
    const {field} = this.state;
    this.props.handleSubmit(value || field);
    this.setState({field: ''});
  };

  handleKeyDown = (event, data) => {
    if (event.keyCode === 13 && !event.shiftKey) {
      event.preventDefault();
      this.handleSubmit();
    } else {
      this.props.sendTyping();
    }
  };

  openEmojis = e => {
    //e.stopPropagation();
    this.setState({isOpenEmoji: !this.state.isOpenEmoji});
    this.element.addEventListener('click', this.maybeCloseEmojis);
  };

  maybeCloseEmojis = e => {
    if (!e.target.closest(`.${css.emojiPicker}`)) {
      e.stopPropagation();
      this.closeEmojis();
    }
  };

  closeEmojis = () => {
    this.setState({isOpenEmoji: false});
    this.element.removeEventListener('click', this.maybeCloseEmojis);
  };

  selectEmoji = emoji => {
    const {field} = this.state;
    this.closeEmojis();
    this.focus();
    this.setState({field: field ? field + ' ' + emoji.native : emoji.native}, () => {
      if (!field) {
        this.handleSubmit();
      }
    });
  };

  onFileDropped = files => {
    const {isClient} = this.props;
    [...files].forEach(async file => {
      if (!file.type.match('image.*')) {
        this.setState({errorUpload: true});
        return;
      }
      const formData = new FormData();
      formData.append('file', file);
      formData.append('type', isClient ? 'publicUpload' : 'chat');
      await this.uploadFile(formData);
    });
  };

  uploadFile = formData => {
    this.setState({uploading: true});
    return this.props.uploadFile(formData).then(response => {
      const filename = get(response, 'resp.filename');
      if (filename) {
        this.handleSubmit(null, {ui: 'promotion', config: {image: filename}});
      } else {
        this.setState({errorUpload: true});
      }
      this.setState({uploading: false});
    });
  };

  closeUploadError = () => {
    this.setState({errorUpload: null});
  };

  addText = key => {
    const {field} = this.state;
    this.setState({
      field: `${field}${field && field.substr(-1) !== ' ' ? ' ' : ''}${MacroInitCharacter}${key}`
    });
  };

  render() {
    const {field, isOpenEmoji, uploading, dropping, errorUpload} = this.state;
    const {placeholder, t, minRows, maxRows, isClient} = this.props;
    let content;
    if (dropping) {
      content = (
        <div className={css.uploadHolder}>
          <div className={css.fakeTextarea}>Upload image</div>
          <div className={css.buttonsHolder}>
            <div onClick={this.openUpload} className={css.actionButton}>
              <Attachment className={css.icon} />
            </div>
          </div>
        </div>
      );
    } else {
      content = (
        <React.Fragment>
          {errorUpload && (
            <div className={css.errorUpload} onClick={this.closeUploadError}>
              {t('!@chatInputErrorUpload')}
            </div>
          )}
          <Textarea
            className={css.textarea}
            value={field}
            minRows={minRows}
            maxRows={maxRows}
            onChange={this.handleChange}
            onKeyDown={this.handleKeyDown}
            placeholder={placeholder || ''}
            ref={el => (this.input = el)}
          />
          <div className={css.buttonsHolder}>
            <div onClick={this.openUpload} className={css.actionButton}>
              {uploading ? <Preloader className={css.icon} /> : <Attachment className={css.icon} />}
            </div>
            <div onClick={this.openEmojis} className={css.actionButton}>
              <Emoji className={classnames(css.icon, css.emojiIcon)} />
            </div>
            <div
              onClick={this.handleSubmit}
              className={classnames(css.sendButton, css.actionButton)}
            >
              <ArrowLeft className={css.icon} />
            </div>
          </div>
          <div className={css.emojiPicker}>
            {isOpenEmoji && (
              <Picker
                className={css.emojiPicker}
                set="twitter"
                title={t('!@selectEmoji')}
                showPreview={false}
                showSkinTones={false}
                emojiSize={24}
                onSelect={this.selectEmoji}
                native={true}
              />
            )}
          </div>
          <FileInput
            onUploadStart={this.uploadFile}
            type={isClient ? 'publicUpload' : 'chat'}
            ref={input => (this.fileInput = input)}
          />
        </React.Fragment>
      );
    }

    return (
      <div
        className={classnames(css.chatInput, {[css.dropping]: dropping})}
        ref={el => (this.element = el)}
      >
        {content}
      </div>
    );
  }
}

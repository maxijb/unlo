import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import get from 'lodash.get';

import {noop} from '@Common/utils/generic-utils';
import {MessageOwners} from '@Common/constants/socket-status';
import {isMineAsClient, isMineAsOperator} from '@Common/utils/message-utils';
import {compareLocalDates} from '@Common/utils/date-utils';
import Modules from '../modules';
import ChatMessage from './chat-message';
import ChatTyping from './chat-typing';

import css from './chat.scss';
import messageCss from './chat-message.scss';

export default class ChatMessageList extends Component {
  static propTypes = {
    conversation: PropTypes.object,
    isClient: PropTypes.bool,
    t: PropTypes.func
  };

  static defaultProps = {
    t: arg => arg,
    methods: {
      getOperator: noop,
      addDataToMessage: noop,
      setActiveConversationStatus: noop
    }
  };

  constructor(props) {
    super(props);
    this.requestedOperators = {};
  }

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate({conversation}) {
    if (conversation.messages.length() < this.props.conversation.messages.length()) {
      this.scrollToBottom();
    }
  }

  getOperator = id => {
    if (!this.requestedOperators.hasOwnProperty(id)) {
      this.requestedOperators[id] = true;
      this.props.methods.getOperator(id);
    }
  };

  scrollToBottom = () => {
    this.messagesContainer.scrollTop = 99999999999999;
  };

  shouldShowDates() {
    const msg = this.props.conversation.messages.getFirst();
    if (msg && msg.updatedAt) {
      return compareLocalDates(msg.updatedAt, new Date());
    }
    return false;
  }

  renderMessages() {
    const {
      conversation,
      isClient,
      options,
      messages,
      status: {operators, user},
      t
    } = this.props;
    const isMine = isClient ? isMineAsClient : isMineAsOperator;
    const html = [];

    const showDates = this.shouldShowDates();
    let lastDate = null;

    conversation.messages.items().forEach((msg, i, list) => {
      const {message, owner, id, tempId, updatedAt} = msg;

      const messageProps = {
        options,
        messages,
        owner,
        operators,
        // always client user
        user,
        getOperator: this.getOperator,
        conversation,
        isClient,
        isMine: isMine(owner),
        status: msg.status
      };

      if (showDates && compareLocalDates(lastDate, updatedAt)) {
        lastDate = new Date(updatedAt);
        html.push(
          <div className={css.date} key={lastDate.getTime()}>
            {t(`!@months.${lastDate.getMonth()}`)} {lastDate.getDate()}
          </div>
        );
      }

      // if it's aplain message
      if (typeof message === 'string') {
        html.push(<ChatMessage key={tempId || id} message={t(message)} {...messageProps} />);
      } else if (message) {
        // if it's an object with a ui message

        // render previous message if defined
        if (message.prev) {
          html.push(
            <ChatMessage key={`${tempId || id}-prev`} message={t(message.prev)} {...messageProps} />
          );
        }

        // generate the UI content
        if (message.ui && Modules.hasOwnProperty(message.ui)) {
          const UI = Modules[message.ui].ui;
          const content = (
            <UI
              {...this.props}
              key={tempId || id}
              msg={msg}
              config={message.config}
              isWithinChat={true}
              isClient={isClient}
              onDoneWithMessage={this.scrollToBottom}
            />
          );

          // based on the UI configuration, wrapp the content or not
          html.push(
            <ChatMessage
              key={tempId || id}
              message={content}
              isUI={true}
              {...messageProps}
              visibleBubble={UI.mustBeWrappedByMessage !== false}
            />
          );
        }
      }

      if (messageProps.isMine && i === list.length - 1 && msg.status) {
        html.push(
          <div key="status" className={css.messageStatus}>
            {t(`!@${msg.status}`)}
          </div>
        );
      }
    });

    return html;
  }

  render() {
    const {className, typing} = this.props;
    return (
      <div
        className={`${css.messagesContainer} ${className}`}
        ref={ref => (this.messagesContainer = ref)}
      >
        {this.renderMessages()}
        {typing && <ChatTyping />}
      </div>
    );
  }
}

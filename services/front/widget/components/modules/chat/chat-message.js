import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {MessageOwners, MessageStatus} from '@Common/constants/socket-status';
import {getOperatorName} from '@Common/utils/message-utils';
import Avatar from '../../display/avatar';
import Emoji from 'react-emoji-render';

import css from './chat-message.scss';

export default class ChatMessage extends Component {
  static propTypes = {
    message: PropTypes.node.isRequired,
    isMine: PropTypes.bool,
    isUI: PropTypes.bool,
    isClient: PropTypes.bool,
    visibleBubble: PropTypes.bool
  };

  static defaultProps = {
    visibleBubble: true
  };

  componentWillMount() {
    const {operators, owner, getOperator} = this.props;
    if (
      owner &&
      owner !== MessageOwners.bot &&
      owner !== MessageOwners.client &&
      (!operators || !operators.hasOwnProperty(owner))
    ) {
      getOperator(owner);
    }
  }

  shouldComponentUpdate(nextProps) {
    return (
      nextProps.message !== this.props.message ||
      nextProps.operators !== this.props.operators ||
      nextProps.options !== this.props.options ||
      nextProps.status !== this.props.status
    );
  }

  renderBubble() {
    const {message, isMine, isUI, isClient, visibleBubble} = this.props;
    return (
      <div
        className={classnames({
          [css.bubble]: visibleBubble,
          [css.isMine]: isMine,
          [css.isClient]: isClient,
          [css.isOperator]: !isClient
        })}
      >
        {isUI && !isClient ? (
          <div className={css.UIdimmer}>{message}</div>
        ) : typeof message === 'string' ? (
          <Emoji text={message} className={css.withEmoji} onlyEmojiClassName={css.emojiLarger} />
        ) : (
          message
        )}
      </div>
    );
  }

  renderAvatar() {
    const {owner, options, operators, isMine, user} = this.props;
    return (
      <Avatar
        className={classnames(css.avatar, {[css.myAvatar]: isMine})}
        user={user}
        owner={owner}
        options={options}
        operators={operators}
      />
    );
  }

  render() {
    const {isMine, isUI, isClient, visibleBubble, status} = this.props;
    const showAvatar = isClient ? !isMine && visibleBubble : visibleBubble;
    return (
      <div
        className={classnames(css.chatMessage, css[status], {
          [css.isMine]: isMine,
          [css.UIholder]: isUI,
          [css.failed]: status === MessageStatus.failed
        })}
      >
        {showAvatar && this.renderAvatar()}
        {this.renderBubble()}
        {/*isUI && !isClient && <div className={css.UIcover} />*/}
      </div>
    );
  }
}

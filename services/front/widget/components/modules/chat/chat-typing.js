import React, {Component} from 'react';

import css from './chat.scss';

export default class ChatMessage extends Component {
  componentDidMount() {
    this.element.scrollIntoView({behavior: 'smooth'});
  }

  render() {
    return (
      <div
        className={css.typing}
        ref={el => {
          this.element = el;
        }}
      >
        Typing...
      </div>
    );
  }
}

import React from 'react';
import isEmpty from 'is-empty';
import get from 'lodash.get';

import {MessageOwners} from '@Common/constants/socket-status';
import {UITypes} from '@Common/constants/input';
import {WaitTypes, WaitBetweenMessages} from '@Common/constants/bot';
import {noop} from '@Common/utils/generic-utils';
import {
  getNextScriptIndex,
  evaluateBotMessage,
  findStepByName,
  parseBotIfRule,
  userHasContact
} from '@Common/utils/bot-utils';
import {OrderedHash} from '@Common/utils/ordered-hash';
import {getAvatar, getOperatorName} from '@Common/utils/message-utils';

import Modules from '../modules';
import GenericModule from '../generic-module';
import ChatMessageList from './chat-message-list';
import Avatar from '../../display/avatar';
import MiniHeader from '../header/mini-header';
import ChatInput from './chat-input';

import css from './chat.scss';

class Chat extends GenericModule {
  static defaultProps = {
    ...GenericModule.defaultProps,
    methods: {
      initChatConnection: noop,
      updateInteraction: noop,
      sendBotMessage: noop,
      addDataToMessage: noop,
      uploadFile: noop
    },
    conversation: {
      messages: new OrderedHash()
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      field: ''
    };
  }

  componentWillMount() {
    const {
      methods: {initChatConnection, updateInteraction},
      status: {
        chat: {
          activeInteraction: {id},
          conversation
        }
      },
      options: {conversationScript}
    } = this.props;

    initChatConnection();
    if (id && (!conversation || conversation.doNotUpdate)) {
      updateInteraction(id);
    }
  }

  componentDidMount() {
    const {
      conversation,
      options: {conversationScript}
    } = this.props;

    // onload we only process the script when the conversation is empty
    // we check 'message' and 'messages' in case one of them is still loading or failed
    if (
      !conversation.loading &&
      !conversation.message &&
      conversation.messages.isEmpty() &&
      !isEmpty(conversationScript)
    ) {
      this.processConversationScript(conversationScript);
    }
  }

  componentWillUnmount() {
    clearTimeout(this.contactTimeout);
    clearTimeout(this.scriptTimeout);
    clearTimeout(this.teamTimeout);
  }

  componentDidUpdate(oldProps) {
    const {
      conversation: {waiting},
      conversationId
    } = oldProps;
    const {
      conversation: {waiting: nWaiting, scriptStep},
      conversationId: nConversationId,
      options: {conversationScript}
    } = this.props;

    if (
      conversationId === nConversationId &&
      waiting &&
      nWaiting === false &&
      scriptStep !== false
    ) {
      this.processConversationScript(conversationScript, scriptStep);
    }
  }

  processConversationScript(script, index = [0]) {
    const {status, options, conversation} = this.props;
    let current = get(script, index);

    if (!current) {
      return;
    }

    const prevMessage = this.props.conversation.messages.getLast();
    const context = {status, options, conversation, message: prevMessage};

    // goto modules allow to redirect the regular flow of the script
    if (
      current.hasOwnProperty('goto') &&
      (!current.while || parseBotIfRule(current.while, prevMessage.data, context))
    ) {
      const name = current.goto;
      const gotoIndex = findStepByName(script, name);
      if (gotoIndex !== null) {
        return this.processConversationScript(script, gotoIndex);
      }
    }

    // get the next element in the script and modify the index if it's a condition element
    const newIndex = evaluateBotMessage(current, index, prevMessage, context);
    const nextIndex = getNextScriptIndex(script, newIndex);

    // getChatMessage is a static method that UI Modules may have to modify how the message will be rendered BEFORE being
    // persisted in the message list
    const ui = get(current, 'ui');
    const getChatMessage =
      current && current.ui && get(Modules, [current.ui, 'ui', 'getChatMessage']);
    if (typeof getChatMessage === 'function') {
      current = getChatMessage({...this.props, message: current});
    }

    this.props.methods.sendBotMessage(current, nextIndex);

    // if we need to wait we won't continue until wait is set to false
    if (current.wait === WaitTypes.input) {
      return;
    }

    // We usually wait until we process the next message
    // until it's a wait message, in which case we deliver it immediately
    const nextElement = get(script, nextIndex);
    if (nextElement && nextElement.wait === WaitTypes.input) {
      this.processConversationScript(script, nextIndex);
      return;
    }

    // Otherwise we'll send the next iteration of the flow
    clearTimeout(this.scriptTimeout);
    this.scriptTimeout = setTimeout(() => {
      this.processConversationScript(script, nextIndex);
    }, WaitBetweenMessages);
  }

  handleSubmit = field => {
    const {conversation, status} = this.props;

    // if it's capture input the last message, we populate its data
    const lastMsg = this.props.conversation.messages.getLast();
    if (get(lastMsg, 'message.ui') === UITypes.captureInput) {
      const {
        status: {
          chat: {
            activeInteraction: {id: interactionId}
          }
        },
        methods: {addDataToMessage}
      } = this.props;

      addDataToMessage({
        id: lastMsg.id,
        interactionId,
        data: {selected: field}
      });
    } else {
      // otherwise we just create the new message
      this.props.methods.sendMessage(field, MessageOwners.client);
      // Stop script immediately
      clearTimeout(this.scriptTimeout);
      // if this the first message by the client, let them know
      // it might take some time for the team to answer and please leave your contact if  we  don't have it
      if (!conversation.hasClientMessage) {
        clearTimeout(this.teamTimeout);
        this.teamTimeout = setTimeout(() => {
          this.props.methods.sendBotMessage('!@teamWillReachOut');
        }, WaitBetweenMessages);

        clearTimeout(this.contactTimeout);
        this.contactTimeout = setTimeout(() => {
          if (!userHasContact(status.user)) {
            this.props.methods.sendBotMessage({
              prev: '!@provideContactStayInTouch',
              ui: 'contact',
              post: '!@contactDataSubmitted'
            });
          } else {
            this.props.methods.sendBotMessage(
              this.print('!@contactWillNotifyByEmail', status.user)
            );
          }
        }, WaitBetweenMessages * 2);
      }
    }
  };

  /* On all the other methods, the action creators are received as props, passed by the redux container */
  /* 't' is always available to get traductions */
  render() {
    const {
      methods: {goToFirstScreen, sendTyping, uploadFile, close},
      status: {
        chat: {typing},
        operators
      },
      conversation,
      options,
      messages,
      isMobile
    } = this.props;
    const owner = get(conversation, 'operator');
    return (
      <div className={css.chatContainer} ref={el => (this.element = el)}>
        <MiniHeader
          image={getAvatar({options, owner, operators})}
          title={getOperatorName({options, owner, operators, messages})}
          subtitle="online"
          goBackAction={goToFirstScreen}
          isMobile={isMobile}
          close={close}
        />
        <ChatMessageList {...this.props} isClient={true} typing={typing} t={this.print} />
        <ChatInput
          sendTyping={sendTyping}
          uploadFile={uploadFile}
          handleSubmit={this.handleSubmit}
          placeholder={this.print('!@chatWritePlaceholder')}
          t={this.print}
          parent={this.element}
          isClient={true}
        />
      </div>
    );
  }
}

export default Chat;

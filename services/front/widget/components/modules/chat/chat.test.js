/* eslint-env node, jest */
import {mount} from 'enzyme';
import React from 'react';

import Chat from './chat';

describe('Chat Component', () => {
  const props = {};

  const wrapper = mount(<Chat {...props} />);

  it('Chat module rendered', () => {
    expect(wrapper.length).toBe(1);
  });
});

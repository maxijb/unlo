import React from 'react';
import classnames from 'classnames';
import get from 'lodash.get';

import {APP_PREFIX} from '@Common/constants/app';
import {PRISTINE_FIELD} from '@Common/constants/input';
import {WaitBetweenMessages} from '@Common/constants/bot';

import GenericModule from '../generic-module';
import Input from '../../inputs/input';
import Checkbox from '../../inputs/checkbox';
import Button from '../../inputs/button';

import css from './contact.scss';
import moduleCss from '../module.scss';

export default class Contact extends GenericModule {
  static defaultProps = {
    ...GenericModule.defaultProps,
    methods: {
      submitContactData: () => {}
    },
    showButton: true,
    requireName: false,
    isBooking: false
  };

  constructor(props) {
    super(props);
    const originalData = {
      value: props.email || get(props.status, 'user.email') || '',
      name: props.name || get(props.status, 'user.name') || '',
      phone: props.phone || get(props.status, 'user.phone') || '',
      consent: props.consent || get(props.status, 'user.consent') || false,
      extendedConsent: props.extendedConsent || get(props.status, 'user.extendedConsent') || false,
      identityId: props.identityId || get(props.status, 'user.identityId') || null
    };

    this.state = {
      originalData,
      ...originalData,
      errors: {
        value: null,
        consent: null,
        extendedConsent: null
      },
      disabled: false
    };
  }

  changeName = evt => {
    this.setState({name: evt.target.value});
  };

  changeEmail = evt => {
    this.setState({value: evt.target.value});
  };

  changePhone = evt => {
    this.setState({phone: evt.target.value});
  };

  changeConsent = evt => {
    this.setState({consent: evt.target.checked});
  };

  changeExtendedConsent = evt => {
    this.setState({extendedConsent: evt.target.checked});
  };

  submit = () => {
    const {value, consent, extendedConsent, name, phone, identityId, originalData} = this.state;
    const {
      msg,
      methods: {submitContactData},
      status: {
        chat: {
          activeInteraction: {id: interactionId}
        }
      },
      requireName,
      isBooking
    } = this.props;

    if (!value) {
      return this.setState({errors: {value: this.print('!@contactErrorEmailRequired')}});
    }

    if (isBooking && !name) {
      return this.setState({errors: {name: this.print('!@contactErrorNameForBooking')}});
    }

    if (!consent) {
      return this.setState({errors: {consent: this.print('!@contactErrorConsent')}});
    }

    submitContactData({
      id: msg.id,
      interactionId,
      data: {
        displayName: name,
        name: name && name === originalData.name ? PRISTINE_FIELD : name,
        email: value && value === originalData.value ? PRISTINE_FIELD : value,
        phone: phone && phone === originalData.phone ? PRISTINE_FIELD : phone,
        consent: Boolean(consent),
        extendedConsent: Boolean(extendedConsent),
        identityId
      }
    });
    this.setState({errors: {}, disabled: true});
    if (msg.message.post) {
      setTimeout(() => {
        this.props.methods.sendBotMessage(msg.message.post);
      }, WaitBetweenMessages);
    }
  };

  render() {
    const {
      status,
      params,
      isWithinChat,
      showButton,
      showPhone,
      requireName,
      msg: {data, id}
    } = this.props;
    const {value, consent, extendedConsent, errors, disabled, name, phone} = this.state;

    const isFull = Boolean(get(data, 'email', false));
    const isDisabled = isFull || disabled;

    return (
      <div
        className={classnames(css.contactFrame, {
          [moduleCss.module]: !isWithinChat,
          [moduleCss.withinChat]: isWithinChat
        })}
      >
        {requireName && (
          <div className={css.field}>
            <Input
              placeholder={this.print('!@contactNamePlaceholder')}
              value={isFull ? data.name : name}
              error={errors.name}
              onChange={this.changeName}
              disabled={isDisabled}
            />
          </div>
        )}
        {showPhone && (
          <div className={css.field}>
            <Input
              placeholder={this.print('!@contactPhonePlaceholder')}
              value={isFull && data.phone !== PRISTINE_FIELD ? data.phone : phone}
              error={errors.phone}
              onChange={this.changePhone}
              disabled={isDisabled}
            />
          </div>
        )}
        <div className={css.field}>
          <Input
            placeholder={this.print('!@contactPlaceholder')}
            value={isFull && data.email !== PRISTINE_FIELD ? data.email : value}
            error={errors.value}
            onChange={this.changeEmail}
            disabled={isDisabled}
          />
        </div>
        {!this.props.consent && !isFull && !get(status, 'user.consent') && (
          <div className={css.field}>
            <Checkbox
              id={`${APP_PREFIX}-consent-${id}`}
              checked={isFull ? data.consent : consent}
              error={errors.consent}
              onChange={this.changeConsent}
              label={this.print('!@contactConsent')}
              disabled={isDisabled}
            />
          </div>
        )}
        {!this.props.extendedConsent && !isFull && !get(status, 'user.extendedConsent') && (
          <div className={css.field}>
            <Checkbox
              id={`${APP_PREFIX}-consent-comms-${id}`}
              checked={isFull ? data.extendedConsent : extendedConsent}
              error={errors.extendedConsent}
              onChange={this.changeExtendedConsent}
              label={this.print('!@contactConsentComms')}
              disabled={isDisabled}
            />
          </div>
        )}
        {showButton && !isFull && (
          <div className={css.field}>
            <Button onClick={this.submit} disabled={isDisabled} kind="primary">
              {this.print('!@contactSubmit')}
            </Button>
          </div>
        )}
      </div>
    );
  }
}

/* eslint-env node, jest */
/* eslint-env node, jest */
import {mount} from 'enzyme';
import React from 'react';

import Contact from './contact';

describe('Contact Component', () => {
  const props = {};

  const wrapper = mount(<Contact {...props} />);

  it('Contact module rendered', () => {
    expect(wrapper.length).toBe(1);
  });
});

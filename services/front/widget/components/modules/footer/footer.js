import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import {Logo} from '../../display/svg-icons';

import css from './footer.scss';

import GenericModule from '../generic-module';

export default class Promotion extends GenericModule {
  static propTypes = {};

  static defaultProps = {
    ...GenericModule.defaultProps
  };

  onClick = () => {
    window.open('http://wiri.io', '_blank');
  };

  render() {
    return (
      <div className={classnames(css.footerContainer)} onClick={this.onClick}>
        <Logo className={css.icon} color={'#ADADAD'} />
        <div className={css.title}>{this.print('!@poweredBy')}</div>
      </div>
    );
  }
}

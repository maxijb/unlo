import React, {Component} from 'react';
import get from 'lodash.get';
import {Messages, DefaultStatus, default as DefaultOptions} from '../../config/defaults';

export default class GenericModule extends Component {
  static defaultProps = {
    status: DefaultStatus,
    messages: Messages,
    options: DefaultOptions,
    msg: {
      message: {}
    },
    config: {},
    params: []
  };

  constructor(props) {
    super(props);

    const name = this.constructor.name;
    if (!props.config) {
      console.warn(`${name} component: No props.config has been defined`);
    }

    if (!props.options) {
      console.warn(`${name} component: No props.options have been defined`);
    }

    if (!props.messages) {
      console.warn(`${name} component: No props.messages have been defined`);
    }
  }

  shouldComponentUpdate = (oldProps, oldState) => {
    if (this.checkUpdateFields) {
      for (const field of this.checkUpdateFields) {
        if (oldProps[field] !== this.props[field]) {
          return true;
        }
      }
      return false;
    }
    return oldProps !== this.props || oldState !== this.state;
  };

  getOption = field => {
    return (
      get(this.props.config, field) || get(this.props.options, field) || get(DefaultOptions, field)
    );
  };

  getMessage = field => {
    return get(this.props.config, field) || get(this.props.messages, field) || get(Messages, field);
  };

  interpolateText = (text, data) => {
    if (typeof text !== 'string') {
      return text;
    }
    const variables = text.match(/{{([a-zA-Z0-9.-_\s]*)}}/g);
    if (variables) {
      variables.forEach(vari => {
        const key = vari.substr(2, vari.length - 4).trim();
        text = text.replace(vari, get(data, key, ''));
      });
    }
    return text;
  };

  /* Prints a translated message
   * @param str {string} main key
   * @fallback str {string [optional]} fallback to a second key if param is not found
   * @data {object} data to be interpolated
   */

  print = (str, fallback, data) => {
    let text = str || fallback;
    if (text && text.substr(0, 2) === '!@') {
      text = this.getMessage(text.substr(2)) || text;
    }

    // fallback is an optional parameter
    if (!data && typeof fallback === 'object') {
      data = fallback;
    }

    text = this.interpolateText(text, data);

    return typeof this.props.options.print === 'function' ? this.props.options.print(text) : text;
  };

  render() {
    return <span />;
  }
}

import React from 'react';
import classnames from 'classnames';

import GenericModule from '../generic-module';
import css from '../module.scss';

export default class BackButton extends GenericModule {
  static defaultProps = {
    ...GenericModule.defaultProps,
    methods: {
      goToFirstScreen: () => {}
    }
  };

  render() {
    const {
      methods: {goToFirstScreen}
    } = this.props;

    return (
      <div className={css.goBack}>
        <a onClick={goToFirstScreen}>{this.print('!@goBack')}</a>
      </div>
    );
  }
}

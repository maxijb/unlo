/* eslint-env node, jest */
/* eslint-env node, jest */
import {mount} from 'enzyme';
import React from 'react';

import GoBack from './go-back-button';

describe('GoBack Component', () => {
  const props = {};

  const wrapper = mount(<GoBack {...props} />);

  it('GoBack module rendered', () => {
    expect(wrapper.length).toBe(1);
  });
});

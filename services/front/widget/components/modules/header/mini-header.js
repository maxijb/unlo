import React, {PureComponent} from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import Avatar from '../../display/avatar';
import {ArrowLeft, Close} from '../../display/svg-icons.js';
import css from './mini-header.scss';
import welcomecss from './welcome.scss';

export default class Header extends PureComponent {
  static propTypes = {
    title: PropTypes.node,
    subtitle: PropTypes.node,
    goBackAction: PropTypes.func,
    close: PropTypes.func,
    isMobile: PropTypes.bool
  };

  render() {
    const {image, title, subtitle, goBackAction, close, isMobile} = this.props;

    return (
      <div
        className={classnames(css.header, {
          [css.isMobile]: isMobile
        })}
      >
        <a onClick={goBackAction}>
          <ArrowLeft />
        </a>
        <div className={css.information}>
          <div className={css.title}>{title}</div>
          <div className={css.subtitle}>{subtitle}</div>
        </div>
        {image && <Avatar src={image} />}
        {isMobile && (
          <div className={classnames(welcomecss.closeButton, css.closeButton)} onClick={close}>
            <Close />
          </div>
        )}
      </div>
    );
  }
}

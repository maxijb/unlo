import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {Close} from '../../display/svg-icons';

import {noop} from '@Common/utils/generic-utils';

import css from './welcome.scss';

import GenericModule from '../generic-module';

export default class Promotion extends GenericModule {
  static propTypes = {};

  static defaultProps = {
    ...GenericModule.defaultProps
  };

  render() {
    const {
      isMobile,
      methods: {close}
    } = this.props;
    return (
      <div
        className={classnames(css.welcomeContainer, {
          [css.isMobile]: isMobile
        })}
      >
        <div className={css.title}>{this.print('!@welcomeTitle')}</div>
        <div className={css.subtitle}>{this.print('!@welcomeSubtitle')}</div>
        {isMobile && (
          <div className={css.closeButton} onClick={close}>
            <Close />
          </div>
        )}
      </div>
    );
  }
}

import {InputTypes, UITypes} from '@Common/constants/input';
import {OrderedHash} from '@Common/utils/ordered-hash';

import Chat from '../modules/chat/chat';
import ChatInvitation from '../modules/chat-invitation/chat-invitation';
import Rate from '../modules/rate/rate';
import Contact from '../modules/contact/contact';
import GoBackButton from '../modules/go-back-button/go-back-button';
import Promotion from '../modules/promotion/promotion';
import Menu from '../modules/promotion/menu';
import OpenHours from '../modules/promotion/open-hours';
import Options from '../modules/options/options';
import Booking from '../modules/booking/booking';
import BookingInvitation from '../modules/booking/booking-invitation';
import CaptureInput from '../modules/options/capture';
import Welcome from '../modules/header/welcome';
import Footer from '../modules/footer/footer';
import MapInvitation from '../modules/promotion/map-invitation';
import MapDisplay from '../modules/promotion/map-display';

export default {
  [UITypes.map]: {
    ui: MapDisplay
  },
  [UITypes.mapInvitation]: {
    ui: MapInvitation
  },
  [UITypes.contact]: {
    ui: Contact
  },
  [UITypes.rate]: {
    ui: Rate
  },
  [UITypes.footer]: {
    ui: Footer
  },
  [UITypes.menu]: {
    ui: Menu
  },
  [UITypes.welcome]: {
    ui: Welcome
  },
  [UITypes.promotion]: {
    ui: Promotion
  },
  [UITypes.openHours]: {
    ui: OpenHours
  },
  [UITypes.booking]: {
    ui: Booking
  },
  [UITypes.bookingInvitation]: {
    ui: BookingInvitation
  },
  [UITypes.chatInvitation]: {
    ui: ChatInvitation
  },
  [UITypes.options]: {
    ui: Options
  },
  [UITypes.captureInput]: {
    ui: CaptureInput
  },
  [UITypes.chat]: {
    ui: Chat,
    extraData: props => {
      const {
        chat: {
          conversations,
          activeInteraction: {id}
        }
      } = props.status;
      return {
        conversationId: id,
        conversation: conversations.get(id)
      };
    }
  },
  [UITypes.goBack]: {
    ui: GoBackButton
  }
};

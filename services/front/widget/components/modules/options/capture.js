import React, {Component} from 'react';
import get from 'lodash.get';
import isEmpty from 'is-empty';
import classnames from 'classnames';

import {isMineAsClient, isMineAsOperator} from '@Common/utils/message-utils';
import {noop} from '@Common/utils/generic-utils';

import ChatMessage from '../chat/chat-message';
import GenericModule from '../generic-module';

import css from './options.scss';

export default class CaptureInput extends GenericModule {
  /* Indicates that if must not be wrapped by message in the chat-list */
  static mustBeWrappedByMessage = false;
  static defaultProps = {
    ...GenericModule.defaultProps,
    methods: {
      addDataToMessage: noop
    }
  };

  renderSelected(selected) {
    const {
      isClient,
      msg: {id, tempId, owner}
    } = this.props;
    return (
      <ChatMessage
        key={`capture-${tempId || id}`}
        message={selected}
        isMine={isClient}
        isClient={isClient}
        isUI={false}
      />
    );
  }

  render() {
    const {
      msg: {data, message, id}
    } = this.props;

    const selected = get(data, 'selected', null);

    if (selected) {
      return this.renderSelected(selected);
    }

    return <div key={`capture-${id}`} />;
  }
}

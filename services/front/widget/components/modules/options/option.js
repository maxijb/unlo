import React from 'react';
import classnames from 'classnames';

import {capitalizeFirst} from '@Common/utils/str-utils';
import Button from '../../inputs/button';
import {noop} from '@Common/utils/generic-utils';

import css from './options.scss';

export default class Option extends React.PureComponent {
  static defaultProps = {
    onClick: noop,
    t: x => x
  };

  selectOption = () => {
    const {onClick, opt, payload} = this.props;
    onClick(opt, payload);
  };

  render() {
    const {opt, selected, disabled, onClick, kind, t} = this.props;
    const value = capitalizeFirst(opt);
    return (
      <Button
        key={opt}
        kind={'secondary'}
        className={classnames(css.option, css[kind], {
          [css.disabled]: selected && selected !== opt,
          [css.selected]: selected === opt
        })}
        onClick={this.selectOption}
      >
        {t(value)}
      </Button>
    );
  }
}

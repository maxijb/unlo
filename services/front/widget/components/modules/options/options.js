import React, {Component} from 'react';
import get from 'lodash.get';
import isEmpty from 'is-empty';
import classnames from 'classnames';

import {isMineAsClient, isMineAsOperator} from '@Common/utils/message-utils';
import {noop} from '@Common/utils/generic-utils';

import ChatMessage from '../chat/chat-message';
import GenericModule from '../generic-module';
import Option from './option';

import css from './options.scss';

export default class Options extends GenericModule {
  /* Indicates that if must not be wrapped by message in the chat-list */
  static mustBeWrappedByMessage = false;
  static defaultProps = {
    ...GenericModule.defaultProps,
    methods: {
      addDataToMessage: noop,
      setActiveConversationStatus: noop
    }
  };

  /* Static method that allows to control how a message for this componetn will be added to the list of messages
   * this runs before the component is rendered, so all it's dependent methods must be static as well
   */
  static getChatMessage = props => {
    const options = Options.getAvailableOptions(props);
    return {...props.message, options};
  };

  static getAvailableOptions = props => {
    const message = props.message ? props.message : get(props, 'msg.message', {});
    const name = get(message, 'name', 'loop');
    const previouslySelected = get(props.conversation, ['previouslySelected', name], {});
    return Options.getDefaultOptions(message).filter(
      opt => !previouslySelected.hasOwnProperty(opt)
    );
  };

  static getDefaultOptions = message => {
    const {params, options} = message;
    return params || options || [];
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.msg !== this.props.msg) {
      this.setState({options: Options.getAvailableOptions(nextProps)});
    }
  }

  componentWillMount() {
    const {
      msg: {
        message,
        message: {name = 'loop', loop}
      },
      conversation,
      methods: {setActiveConversationStatus}
    } = this.props;

    if (loop) {
      const options = Options.getAvailableOptions(this.props);

      setActiveConversationStatus({
        defaultOptions: {
          ...conversation.defaultOptions,
          [name]: options
        }
      });
      this.setState({options});
    } else {
      this.setState({options: Options.getDefaultOptions(message)});
    }
  }

  selectOption = opt => {
    const {
      status: {
        chat: {
          activeInteraction: {id: interactionId}
        }
      },
      msg: {
        id,
        data,
        message: {name = 'loop', loop}
      },
      conversation,
      methods: {addDataToMessage, setActiveConversationStatus}
    } = this.props;

    // disable options after selecting one
    if (get(data, 'selected')) {
      return;
    }

    if (loop) {
      setActiveConversationStatus({
        previouslySelected: {
          [name]: {
            ...get(conversation, ['previouslySelected', name], {}),
            [opt]: true
          }
        }
      });
    }

    addDataToMessage({
      id,
      interactionId,
      data: {selected: opt}
    });
  };

  renderSelected(selected) {
    const {
      isClient,
      msg: {id, tempId, owner},
      options,
      status: {operators},
      methods: {getOperator}
    } = this.props;
    const isMine = isClient ? isMineAsClient : isMineAsOperator;
    return (
      <ChatMessage
        key={tempId || id}
        message={this.print(selected)}
        isMine={isClient}
        isClient={isClient}
        isUI={false}
        options={options}
        operators={operators}
        getOperator={getOperator}
      />
    );
  }

  render() {
    const {
      msg,
      msg: {data, message},
      options: {settings = {}},
      conversation
    } = this.props;

    const {options = []} = this.state;

    const selected = get(data, 'selected', null);

    if (selected) {
      return this.renderSelected(selected);
    }

    if (conversation.messages.getLast().id !== msg.id) {
      return null;
    }

    const optionsTitle = get(msg, 'message.optionsTitle', '!@optionsChooseOrType');
    const kind = get(msg, 'message.kind', '');
    return (
      <div className={classnames(css.optionsHolder, css[kind])}>
        {!settings.disallowOptionsInput && optionsTitle && (
          <div className={css.optionsTitle}>{this.print(optionsTitle)}</div>
        )}
        {options.map(opt => {
          const isSelected = selected === opt;
          const isDisabled = selected && selected !== opt;
          return (
            <Option
              opt={opt}
              key={opt}
              kind={kind}
              disabled={isDisabled}
              selected={isSelected}
              onClick={this.selectOption}
              t={this.print}
            />
          );
        })}
      </div>
    );
  }
}

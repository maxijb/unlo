import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import isEmpty from 'is-empty';

import {generateAddress, getGoogleMapsLink} from '@Common/utils/map-utils';
import {UserFilesCdnURL} from '@Common/utils/statif-assets-utils';
import GenericModule from '../generic-module';
import Option from '../options/option';
import Promotion from './promotion';
import ChatMessage from '../chat/chat-message';
import {Location} from '../../display/svg-icons.js';

import css from './map.scss';

export default class MapDisplay extends GenericModule {
  static propTypes = {
    options: PropTypes.object,
    isWithinChat: PropTypes.bool
  };

  static defaultProps = {
    ...GenericModule.defaultProps
  };

  render() {
    const {options, onDoneWithMessage} = this.props;

    const {latitude, longitude} = options.business || {};

    return (
      <div>
        <div>{this.print('!@ourLocation')}</div>
        <div className={css.displayAddress}>{generateAddress(options.business)}</div>
        {latitude && longitude && (
          <div>
            <a
              href={getGoogleMapsLink(latitude, longitude)}
              target="_blank"
              rel="noreferrer noopener"
            >
              <span className={css.staticMapContainer}>
                <img
                  src={`https://api.mapbox.com/styles/v1/mapbox/streets-v11/static/${longitude},${latitude},15,0,0/246x170@2x?access_token=pk.eyJ1IjoiNDBiaXRjb2lucyIsImEiOiJjazRzczNwdzQwMHVpM21ub2JycnNwdnowIn0.0tmxbyceUE76puTpMsWYOg`}
                  className={css.map}
                  onLoad={onDoneWithMessage}
                />
                <Location className={css.marker} />
                <span className={css.markerShadow} />
              </span>
              {this.print('!@clickToOpenInGoogleMaps')}
            </a>
          </div>
        )}
      </div>
    );
  }
}

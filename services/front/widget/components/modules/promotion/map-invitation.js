import React, {Component} from 'react';
import get from 'lodash.get';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import isEmpty from 'is-empty';

import {cdnURL} from '@Common/utils/statif-assets-utils';
import {generateAddress} from '@Common/utils/map-utils';
import GenericModule from '../generic-module';
import Option from '../options/option';
import Promotion from './promotion';
import {PromotionAction, PromotionLinkTarget} from '../../../config/widget-ui-constants';
import {getGoogleMapsLink} from '@Common/utils/map-utils';

import css from './map.scss';

export default class MenuPromotion extends GenericModule {
  static propTypes = {
    options: PropTypes.object,
    isWithinChat: PropTypes.bool
  };

  static defaultProps = {
    ...GenericModule.defaultProps
  };

  render() {
    const {options} = this.props;
    const {latitude, longitude} = options.business || {};

    const config = {
      image: cdnURL('/static/map-invitation.png'),
      inlineTitle: (
        <div>
          <div className={css.mapInvitationTitle}>Where are we located?</div>
          <div className={css.mainAddress}>{generateAddress(options.business)}</div>
        </div>
      ),
      text: <div className={css.directions}>{get(options.business, 'directions')}</div>,
      template: 'image-left',
      button: '!@openInGoogleMaps',
      ribbon: false,
      action: PromotionAction.link,
      target: PromotionLinkTarget.blank,
      href: getGoogleMapsLink(latitude, longitude)
    };

    return <Promotion config={config} />;
  }
}

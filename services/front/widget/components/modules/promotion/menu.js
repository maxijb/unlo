import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import isEmpty from 'is-empty';

import {UserFilesCdnURL} from '@Common/utils/statif-assets-utils';
import GenericModule from '../generic-module';
import Option from '../options/option';
import Promotion from './promotion';

import css from './promotion.scss';

export default class MenuPromotion extends GenericModule {
  static propTypes = {
    options: PropTypes.object,
    isWithinChat: PropTypes.bool
  };

  static defaultProps = {
    ...GenericModule.defaultProps
  };

  selectMenu = (name, payload = {}) => {
    window.open(payload.link, '_blank');
  };

  renderWithinChat() {
    const {
      isWithinChat,
      options: {menuDescription = '', menuList = []}
    } = this.props;

    return (
      <div>
        <div className={css.chatMenuDescription}>{menuDescription}</div>
        {menuList.map((menu, i) => {
          return (
            <a
              key={i}
              onClick={this.selectMenu.bind(this, menu.name, menu)}
              className={css.chatOption}
            >
              {menu.name}
            </a>
          );
        })}
      </div>
    );
  }

  render() {
    const {
      isWithinChat,
      options: {menuTitle = '', menuList = [], menuDescription = '', menuImage = ''}
    } = this.props;

    if (isWithinChat) {
      return this.renderWithinChat();
    }

    const config = {
      image: menuImage,
      inlineTitle: menuTitle,
      text: menuDescription,
      template: 'image-left'
    };

    return (
      <Promotion config={config} className={css.menu}>
        <div className={css.buttonHolder}>
          {menuList.map((menu, i) => {
            return <Option opt={menu.name} payload={menu} key={i} onClick={this.selectMenu} />;
          })}
        </div>
      </Promotion>
    );
  }
}

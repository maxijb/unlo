import React, {Component} from 'react';
import deepEqual from 'deep-equal';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import isEmpty from 'is-empty';

import GenericModule from '../generic-module';
import Button from '../../inputs/button';
import Label from '../../display/label';
import Promotion from './promotion';
import {calculateOpenNow} from '@Common/utils/date-utils';

import css from './open-hours.scss';
import moduleCss from '../module.scss';

export default class OpenHours extends GenericModule {
  static propTypes = {
    config: PropTypes.shape({
      hours: PropTypes.array,
      action: PropTypes.string,
      href: PropTypes.string,
      template: PropTypes.string,
      title: PropTypes.string,
      target: PropTypes.string,
      image: PropTypes.string,
      text: PropTypes.string,
      button: PropTypes.node
    }),
    isWithinChat: PropTypes.bool
  };

  static defaultProps = {
    ...GenericModule.defaultProps
  };

  static defaultProps = {
    config: {}
  };

  render() {
    const {
      config,
      config: {hours: configHours},
      options: {openHours, timezone},
      messages
    } = this.props;

    // use local config or global options
    const hours = configHours || openHours;

    if (isEmpty(hours)) {
      return null;
    }

    const titleAddon = !calculateOpenNow(hours, timezone) ? null : (
      <Label text={this.print('!@openNow')} />
    );

    return (
      <Promotion
        {...this.props}
        config={{
          ...config,
          titleAddon,
          title: config.title || '!@openingHours'
        }}
      >
        <table className={moduleCss.moduleTable}>
          <tbody>
            {hours.map((day, i) => {
              if (deepEqual(hours[i], hours[i - 1])) {
                return null;
              }

              let content = [];

              if (isEmpty(day)) {
                content = [<td className={css.closedRange}>{this.print('!@closed')}</td>, <td />];
              } else {
                for (let j = 0; j < day.length; j++) {
                  content.push(
                    <span key={j} className={j % 2 === 0 ? css.startRange : css.endRange}>
                      {day[j]}
                    </span>
                  );
                }
                if (day.length === 2) {
                  content = [<td>{content}</td>, <td />];
                } else {
                  content = [
                    <td>
                      <span key={1}>{content[0]}</span>
                      <span key={2}>{content[1]}</span>
                    </td>,
                    <td>
                      <span key={1}>{content[2]}</span>
                      <span key={2}>{content[3]}</span>
                    </td>
                  ];
                }
              }

              const days = [i];
              for (let j = i + 1; j < hours.length; j++) {
                if (deepEqual(hours[j], hours[i])) {
                  days.push(j);
                } else {
                  break;
                }
              }

              return (
                <tr key={i}>
                  <td className={css.days}>
                    {days
                      .map(d => this.getMessage([days.length > 1 ? 'shortDays' : 'days', d]))
                      .join(', ')}
                  </td>
                  {content}
                </tr>
              );
            })}
          </tbody>
        </table>
      </Promotion>
    );
  }
}

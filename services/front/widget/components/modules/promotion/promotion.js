import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import {noop} from '@Common/utils/generic-utils';

import css from './promotion.scss';
import moduleCss from '../module.scss';

import {cdnURL, UserFilesCdnURL} from '@Common/utils/statif-assets-utils';
import GenericModule from '../generic-module';
import {PromotionAction, PromotionLinkTarget} from '../../../config/widget-ui-constants';
import Button from '../../inputs/button';

export default class Promotion extends GenericModule {
  static propTypes = {
    config: PropTypes.shape({
      action: PropTypes.string,
      href: PropTypes.string,
      template: PropTypes.string,
      title: PropTypes.string,
      target: PropTypes.string,
      image: PropTypes.string,
      text: PropTypes.string,
      button: PropTypes.string
    }),
    placeholders: PropTypes.shape({
      action: PropTypes.string,
      title: PropTypes.string,
      image: PropTypes.string,
      text: PropTypes.string,
      button: PropTypes.string
    }),
    isWithinChat: PropTypes.bool,
    onDoneWithMessage: noop
  };

  static defaultProps = {
    ...GenericModule.defaultProps,
    methods: {
      goToNextStep: noop
    },
    placeholders: {}
  };

  executeAction = () => {
    const {
      config,
      config: {action, href, target, steps, actionName}
    } = this.props;
    // link actions
    if (action === PromotionAction.link && href) {
      if (target === PromotionLinkTarget.blank || !target) {
        window.open(href, '_blank');
      } else if (target === PromotionLinkTarget.history && window.history) {
        window.history.pushState(null, '', href);
      } else {
        window.location = href;
      }
    } else if (action === PromotionAction.actionStep) {
      this.props.methods.goToNextStep(actionName, {promotion: config});
    }
  };

  render() {
    const {
      config,
      config: {
        action,
        title,
        subtitle,
        inlineTitle,
        text,
        image,
        button,
        template = 'image-top',
        rules,
        titleAddon,
        ribbon
      },
      placeholders,
      children,
      isWithinChat,
      onDoneWithMessage,
      className
    } = this.props;

    return (
      <div
        className={classnames(css.promotion, css[action], css[template], className, {
          [moduleCss.module]: !isWithinChat,
          [css.clickable]: !button && action,
          [css.isWithinChat]: isWithinChat
        })}
        onClick={button ? noop : this.executeAction}
      >
        <div className={classnames(css.layout, css[`layout-${template}`])}>
          {(title || placeholders.title) && !inlineTitle && (
            <div className={classnames(moduleCss.moduleTitle, css[`text-${template}`])}>
              <div>
                {typeof title === 'string'
                  ? this.print(title, placeholders.title)
                  : title || placeholders.title}
              </div>
              {titleAddon || null}
            </div>
          )}
          {(subtitle || placeholders.subtitle) && (
            <div className={classnames(moduleCss.moduleSubtitle, css[`text-${template}`])}>
              {typeof subtitle === 'string'
                ? this.print(subtitle, placeholders.subtitle)
                : subtitle || placeholders.subtitle}
            </div>
          )}
          {(image ||
            text ||
            rules ||
            inlineTitle ||
            placeholders.text ||
            placeholders.image ||
            placeholders.rules ||
            placeholders.inlineTitle) && (
            <div className={css.body}>
              {(image || placeholders.image) && (
                <img
                  className={classnames(css.image, css[`image-${template}`])}
                  src={image ? UserFilesCdnURL(image) : placeholders.image}
                  onLoad={onDoneWithMessage}
                />
              )}
              <div className={css.textContentHolder}>
                {(inlineTitle || placeholders.inlineTitle) && (
                  <div
                    className={classnames(
                      moduleCss.moduleTitle,
                      css.inlineTitle,
                      css[`text-${template}`]
                    )}
                  >
                    {typeof inlineTitle === 'string'
                      ? this.print(inlineTitle, placeholders.inlineTitle)
                      : inlineTitle || placeholders.inlineTitle}
                  </div>
                )}
                {(text || placeholders.text) && (
                  <div className={classnames(css.text, css[`text-${template}`])}>
                    {typeof text === 'string'
                      ? this.print(text, placeholders.text)
                      : text || placeholders.text}
                  </div>
                )}
                {(rules || placeholders.text) && (
                  <div className={classnames(css.rules, css[`text-${template}`])}>
                    {typeof rules === 'string'
                      ? this.print(rules, placeholders.rules)
                      : rules || placeholders.rules}
                  </div>
                )}
              </div>
            </div>
          )}
          {children || null}
          {(button || placeholders.button) && (
            <div className={css.buttonHolder}>
              <Button onClick={this.executeAction} ribbon={typeof ribbon ? ribbon : true}>
                {this.print(button, placeholders.button)}
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

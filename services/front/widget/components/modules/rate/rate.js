import React from 'react';
import GenericModule from '../generic-module';
import {noop} from '@Common/utils/generic-utils';
import classnames from 'classnames';
import get from 'lodash.get';

import {UITypes} from '@Common/constants/input';
import {Smiley0, Smiley1, Smiley2, Smiley3, Smiley4} from '../../display/svg-icons';

import {RatingAfterSubmittedStatus} from '../../../config/widget-ui-constants';

import css from './rate.scss';
import moduleCss from '../module.scss';

const Smileys = [null, Smiley0, Smiley1, Smiley2, Smiley3, Smiley4];

export default class RateComponent extends GenericModule {
  static defaultProps = {
    ...GenericModule.defaultProps,
    methods: {
      goToNextStep: () => {},
      rate: () => {}
    }
  };

  getValue() {
    const {msg, status, isWithinChat} = this.props;
    return isWithinChat ? get(msg, 'data.value') : status.rate.value;
  }

  renderTitle() {
    const {
      isWithinChat,
      msg: {
        message: {params}
      }
    } = this.props;

    const title =
      params && params[0]
        ? params[0]
        : !isWithinChat
        ? this.print('!@rateTitle')
        : this.print('!@rateTitleChat');

    return (
      <div className={classnames(css.rateTitle, {[moduleCss.moduleTitle]: !isWithinChat})}>
        {title}
      </div>
    );
  }

  renderThankYouMessage(showRating, areRatingsDisabled, value) {
    const {
      config,
      status,
      steps,
      methods: {goToNextStep}
    } = this.props;

    const rating = !showRating
      ? null
      : this.getOption('ratings').items.find(it => it.value === value);

    let content = <div>{this.print('!@thankYouMessage')}</div>;
    // if we find steps we turn this into a link
    if ((config.steps && config.steps.length) || (steps && steps.length)) {
      content = (
        <div>
          <a onClick={goToNextStep.bind(null, UITypes.rate)}>{this.print('!@thankYouMessage')}</a>
        </div>
      );
    }

    return (
      <div className={classnames(css.rateFrame, moduleCss.module)}>
        {showRating && this.renderRating(areRatingsDisabled, value, rating, true)}
        {content}
      </div>
    );
  }

  renderRating = (areRatingsDisabled, selectedValue, rating, index) => {
    const {
      status,
      msg,
      methods: {rate = noop},
      config
    } = this.props;

    const Smiley = Smileys[rating.value];
    return (
      <div
        className={classnames(css.rate, {
          [css.selected]: rating.value === selectedValue,
          [css.disabled]: areRatingsDisabled
        })}
        key={rating.value}
        onClick={areRatingsDisabled ? null : rate.bind(null, rating.value, msg, config)}
      >
        {Smiley ? <Smiley /> : rating.display}
      </div>
    );
  };

  render() {
    const {
      status,
      msg: {
        message: {params}
      },
      isWithinChat
    } = this.props;

    const value = this.getValue();
    // we disable rating if they've been selected and we don't allow modifications as per options
    const areRatingsDisabled = value && !this.getOption('allowModifyRating');

    //if (value && !isWithinChat) {
    //  switch (this.getOption('showRatingAfterSubmit')) {
    //    case false:
    //      return null;
    //    case RatingAfterSubmittedStatus.thankYouMessage:
    //      return this.renderThankYouMessage();
    //    case RatingAfterSubmittedStatus.thankYouMessageWithRating:
    //      return this.renderThankYouMessage(true, areRatingsDisabled, value);
    //  }
    //}

    return (
      <div
        className={classnames(css.rateFrame, {
          [moduleCss.module]: !isWithinChat,
          [moduleCss.withinChat]: isWithinChat
        })}
      >
        {this.renderTitle()}
        <div className={css.rateContainer}>
          {this.getOption('ratings').items.map(
            this.renderRating.bind(null, areRatingsDisabled, value)
          )}
        </div>
      </div>
    );
  }
}

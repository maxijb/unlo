import React, {Component} from 'react';
import isEmpty from 'is-empty';
import {noop} from '@Common/utils/generic-utils';
import get from 'lodash.get';

import {cdnURL} from '@Common/utils/statif-assets-utils';
import {clientCookieName, APP_PREFIX} from '../../../../common/constants/app';
import {setStyleVars, getVisibilityAPIVars} from '@Common/utils/dom-utils';
import {WidgetPosition} from '@Widget/config/widget-ui-constants';
import Trigger from '../trigger/trigger';
import Body from '../body/body';

import css from './style.scss';
import cssVariables from './_vars-widget.scss';

export default class WidgetParent extends Component {
  static defaultProps = {
    methods: {
      setWindowActive: noop
    },
    status: {}
  };

  constructor(props) {
    super(props);
    this.state = {CSSloaded: false};
  }

  componentDidMount() {
    const {
      options: {styleVars},
      status = {}
    } = this.props;

    const windowWidth = status.maxWidth || window.innerWidth;
    const windowHeight = status.maxHeight || window.innerHeight;
    const isMobile =
      windowWidth <= parseInt(cssVariables.mobileBreakpoint) ||
      windowHeight <= parseInt(cssVariables.mobileBreakpoint);

    this.setStyleVars(styleVars);
    this.setState({CSSloaded: true, windowHeight, windowWidth, isMobile});

    // handle cross-browser visiblity api
    Object.assign(this, getVisibilityAPIVars());

    if (this.hidden) {
      document.addEventListener(this.visibilityChange, this.setWindowActive, false);
    }
  }

  componentWillReceiveProps(nextProps) {
    const {status = {}} = nextProps;
    const {oldstatus = {}} = this.props;
    if (
      status.maxWidth &&
      status.maxHeight &&
      (status.maxHeight !== oldstatus.maxHeight || status.maxWidth !== oldstatus.maxWidth)
    ) {
      this.setState({
        windowWidth: status.maxWidth,
        windowHeight: status.maxHeight,
        isMobile:
          status.maxWidth <= parseInt(cssVariables.mobileBreakpoint) ||
          status.maxHeight <= parseInt(cssVariables.mobileBreakpoint)
      });
    }
  }

  componentDidUpdate() {
    const {trigger = {}, body = {}} = this.refs;
    const {isMobile} = this.state;
    const {options} = this.props;
    const {windowHeight, windowWidth} = this.state;

    let height, width, right, bottom, left;
    if (body.body) {
      if (isMobile) {
        height = windowHeight;
        width = windowWidth;
      } else {
        const bottom = parseInt(window.getComputedStyle(body.body).bottom) || 0;
        height = body.body.clientHeight + bottom + 10;
        width = body.body.clientWidth + 10;
      }
    } else if (trigger.trigger) {
      width = trigger.trigger.clientWidth + 10;
      height = trigger.trigger.clientHeight + 10;
    }

    if (body.body && isMobile) {
      left = 'auto';
      right = 0;
      bottom = 0;
    } else {
      const horizontalOffset =
        isMobile && typeof options.mobileTriggerHorizontalOffset !== 'undefined'
          ? options.mobileTriggerHorizontalOffset || 0
          : options.triggerHorizontalOffset || 0;

      const lateralSpace = isMobile
        ? parseInt(cssVariables.mobileLateralSpace) + horizontalOffset
        : parseInt(cssVariables.lateralSpace) + horizontalOffset;

      const verticalOffset =
        isMobile && typeof options.mobileTriggerVerticalOffset !== 'undefined'
          ? options.mobileTriggerVerticalOffset || 0
          : options.triggerVerticalOffset || 0;

      const verticalSpace = isMobile
        ? parseInt(cssVariables.mobileVerticalSpace) + verticalOffset
        : parseInt(cssVariables.verticalSpace) + verticalOffset;

      right = options.position === WidgetPosition.left ? 'auto' : lateralSpace;
      left = options.position !== WidgetPosition.left ? 'auto' : lateralSpace;
      bottom = verticalSpace;
    }

    if (
      window.parent !== window.self &&
      width &&
      height &&
      (width !== this.width ||
        height !== this.height ||
        left !== this.left ||
        right !== this.right ||
        bottom !== this.bottom)
    ) {
      this.height = height;
      this.width = width;
      this.right = right;
      this.left = left;
      this.bottom = bottom;
      this.props.methods.postMessage({
        action: 'wiri-resizeIframe',
        width,
        height,
        right,
        bottom,
        left
      });
    }
  }

  componentWillUnmount() {
    document.removeEventListener(this.visibilitychange, this.setWindowActive);
  }

  playSound = () => {
    if (this.refs.sound) {
      this.refs.sound.play();
    }
  };

  setWindowActive = () => {
    const active = !document[this.hidden];
    const {
      status: {isTitleNotificationEnabled}
    } = this.props;
    this.props.methods.setWindowActive(active);
    if (active && isTitleNotificationEnabled) {
      this.props.methods.postMessage({action: 'wiri-stopTitleNotification'});
      this.props.methods.setIsTitleNotificationEnabled(false);
    }
  };

  setStyleVars(styleVars) {
    setStyleVars(this.elem, styleVars);
  }

  scrollToTop = () => {
    const bodyFrame = get(this, 'refs.body.bodyFrame');
    if (bodyFrame) {
      bodyFrame.scrollTop = 0;
    }
  };

  render() {
    const {status, options} = this.props;
    const {CSSloaded, windowWidth, windowHeight, isMobile} = this.state;

    return (
      <div id={css.customerWidget} className={css.customerWidget} ref={elem => (this.elem = elem)}>
        {CSSloaded && (
          <React.Fragment>
            {status.isOpen && (
              <Body
                {...this.props}
                ref="body"
                windowWidth={windowWidth}
                windowHeight={windowHeight}
                isMobile={isMobile}
              />
            )}
            {options.showTrigger && (
              <Trigger
                {...this.props}
                ref="trigger"
                windowWidth={windowWidth}
                windowHeight={windowHeight}
                isMobile={isMobile}
              />
            )}
            <audio ref={'sound'} src={cdnURL('/static/notification.mp3')} autoPlay={false} />
          </React.Fragment>
        )}
      </div>
    );
  }
}

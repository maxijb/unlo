import React, {Component} from 'react';

import io from 'socket.io-client/dist/socket.io.slim';
import deepExtend from 'deep-extend';
import get from 'lodash.get';
import isEmpty from 'is-empty';

import {WaitTypes} from '@Common/constants/bot';
import {debounceByKey} from '@Common/utils/debounce';
import {processInteractionMessage} from '@Common/utils/macros-utils';
import {OrderedHash} from '@Common/utils/ordered-hash';
import {clientCookieName} from '@Common/constants/app';
import {InputTypes, UITypes, DefaultLang} from '@Common/constants/input';
import {isConditionMessage} from '@Common/utils/bot-utils';
import {parseFragment} from '@Common/utils/urls';
import {
  SocketEvents,
  SocketStatus,
  EventWidgetHandler,
  TempConversationId,
  MessageStatus,
  MessageOwners,
} from '@Common/constants/socket-status';

import Queue from '../../connections/queue';
import {initApiActions} from '../../connections/widget-api-actions';
import Store from '../../store/store';
import {
  default as Defaults,
  Messages as DefaultMessages,
  DefaultStatus,
} from '../../config/defaults';
import {WidgetConfig} from '../../config/env';
import {initCDN} from '@Common/utils/statif-assets-utils';
import {generateTempInteractionId} from '../../utils/interaction-utils';
import Parent from './parent';

export default class WidgetRoot extends Component {
  constructor(props) {
    super(props);
    const state = {
      options: {
        ...Defaults,
        ...props.options,
      },
      status: {
        ...DefaultStatus,
        // hardcodes whatever initial status is passed as props
        // include maxWidth and maxHeight
        ...props.status,
      },
    };

    // Sets the store
    this.store = new Store(this, state);
    this.actions = this.store.actions;
    this.Api = initApiActions(props.Api, this.store);

    // if some methods are provided bind them to root and override default methods
    // used in admin phase
    if (props.methods) {
      const methods = Object.keys(props.methods).reduce((prev, key) => {
        prev[key] = props.methods[key].bind(this);
        return prev;
      }, {});
      this.publicMethods = {...this.publicMethods, ...methods};
    }

    // Sets the instance in the wrapper so it can pass options
    if (props.wrapper) {
      props.wrapper.__private.setInstance(this);
    }
  }

  async componentDidMount() {
    const {appId, campaignId, serverOptions, options, messages, user, url} = this.props;
    // we pass the fragment in case the url has some action to be performed
    const {fragment} = this.store.state.status;
    const target = this.getTarget();

    // Connect to backend and get options and publicId
    const {resp} = await this.Api.clientConnect({
      ...target,
      serverOptions,
      fragment,
      url: (url || '').substr(0, 512),
    });

    // if we have a publicId it means we didn't get an error
    if (resp && resp.publicId) {
      target.lang = resp.lang || target.lang || DefaultLang;
      const publicId = resp.publicId;

      // init safe socket (only connects when required)
      this.socket = new this.props.SafeSocket(`${WidgetConfig.domain}/widget`, {
        path: WidgetConfig.socketPath,
        query: {...target, publicId},
      });
      // init paths from cdn
      initCDN(WidgetConfig);

      this.queue = new Queue(
        this.socket,
        this.store,
        this.socketEventHandlers.handleMessageAcknoledgement,
      );

      // export defaults with server-side options, and then apply local options
      const settings = deepExtend({}, Defaults, resp.options, options);

      // set the state and options
      this.actions.setState({
        options: settings,
        messages: deepExtend({}, DefaultMessages, resp.messages, messages),
        serverOptions,
        target,
        status: {
          ...this.store.state.status,
          publicId,
          bookings: {
            list: new OrderedHash(resp.bookings),
          },
          user: {
            ...resp.user,
            identities: resp.identities,
          },
          loaded: true,
          chat: {
            activeInteraction: {},
            conversations: new OrderedHash(
              (resp.conversations || []).map(item => ({
                ...processInteractionMessage(item),
                messages: new OrderedHash(item.messages),
              })),
            ),
          },
          // hardcodes whatever initial status is passed as props
          ...this.props.status,
        },
      });

      if (!isEmpty(resp.conversations)) {
        this.publicMethods.initChatConnection();
      }

      if (resp.forceAction) {
        this.applyForcedAction(resp.forceAction);
      }

      if (settings.open) {
        this.publicMethods.open();
      }
    }
  }

  componentWillUnmount() {
    if (this.socket) {
      this.socket.close();
    }
  }

  /* Normalizes the `target` prop to include `appId`, `campaignId`, and `lang`
   * which can also be in the root options object
   */
  getTarget() {
    const {target, appId, campaignId, lang} = this.props;

    const newTarget = {...target};
    if (appId) {
      newTarget.appId = appId;
    }
    if (campaignId) {
      newTarget.campaignId = campaignId;
    }
    if (lang) {
      newTarget.lang = lang;
    }

    return newTarget;
  }

  applyBatchActionsToSocket = action => {
    // preventing to subscribe more than once
    if (!this.store.state.status.socketsEventsLoaded) {
      Object.keys(EventWidgetHandler).forEach(key => {
        this.socket[action](key, this.socketEventHandlers[EventWidgetHandler[key]]);
      });
      this.actions.setStatus({socketsEventsLoaded: true});
    }
  };

  onWindowBlur = () => {
    this.actions.setStatus({focusedWindow: false});
  };

  onWindowFocus = () => {
    this.actions.setStatus({focusedWindow: true});
  };

  applyForcedAction = forcedAction => {
    const {action, id} = forcedAction;
    if (action && id) {
      this.publicMethods.open();
      if (action === UITypes.chat) {
        this.publicMethods.initConversation(id);
      } else if (action === UITypes.booking) {
        this.publicMethods.editBooking(id);
      }
    }
  };

  /* ----------------------------------- SOCKETS EVENTS HANDLERS --------------------------------- */
  socketEventHandlers = {
    handleConnection: (...args) => {
      this.publicMethods.updateInteraction(
        get(this.store.state.status, 'chat.activeInteraction.id'),
      );
    },
    /* Receives message from remote source */
    handleMessage: message => {
      const {interactionId} = message;
      const {
        status: {
          chat: {activeInteraction},
          isWindowActive,
          isOpen,
        },
        options: {soundOnMessage},
        messages,
      } = this.store.state;

      if (!isWindowActive) {
        this.publicMethods.setIsTitleNotificationEnabled(true);
        this.publicMethods.postMessage({
          action: 'wiri-startTitleNotification',
          title: messages.unreadMessages,
        });
      }

      if (!isOpen) {
        this.publicMethods.open();
      }

      this.actions.addMessageToConversation({
        interactionId: message.interactionId,
        message: {
          ...message,
          status:
            message.owner === MessageOwners.client
              ? MessageStatus.received
              : MessageStatus.incoming,
        },
      });

      if (activeInteraction.id === message.interactionId) {
        this.actions.setChatStatus({typing: false});

        if (isWindowActive) {
          this.publicMethods.markMessageAsViewed(activeInteraction.id, message.id);
        }
      }

      if (message.owner !== MessageOwners.client && soundOnMessage && this.Parent) {
        this.Parent.playSound();
      }
    },

    handleMessageAcknoledgement: (message, error, ack) => {
      if (error) {
        // TODO: handle retries
        return this.actions.setChatMessageStatus({
          interactionId: message.interactionId,
          id: message.id,
          status: MessageStatus.failed,
        });
      }

      const {activeInteraction, conversations} = this.store.state.status.chat;

      // update with new interactionId if we were using a temporary one
      if (
        ack.interactionId &&
        ack.tempInteractionId &&
        message.isTempInteractionId &&
        conversations.get(ack.tempInteractionId)
      ) {
        this.actions.setChatStatus({
          // rename the existing conversation
          conversations: conversations.rename(ack.tempInteractionId, ack.interactionId),
          // if the temporary id is the one selected right now, update it
          // othwerwise return the same object activeInteraction
          activeInteraction:
            ack.tempInteractionId === activeInteraction.id
              ? {id: ack.interactionId, isTempInteractionId: false}
              : activeInteraction,
        });
      }

      // Flag the message as read
      this.actions.setChatMessageStatus({
        id: message.id,
        renameId: ack.id,
        interactionId: ack.interactionId,
        status: MessageStatus.received,
      });
    },

    handleViewedMessage: ({interactionId, messageId}) => {
      this.actions.setViewedMessageByOperator({interactionId, messageId});
    },

    handleRemoteAddDataToMessage: ({interactionId, id, data}) => {
      this.actions.addDataToMessage({interactionId, id, data});
    },

    handleRemoteSubmitContactData: ({interactionId, id, data}) => {
      this.actions.addDataToMessage({
        interactionId,
        id,
        data,
        event: SocketEvents.submitContactData,
      });
    },

    handleTyping: ({interactionId}) => {
      const {
        typingTimeout,
        activeInteraction: {id},
      } = this.store.state.status.chat;

      clearTimeout(typingTimeout);

      if (id && interactionId === id) {
        const timeout = setTimeout(() => {
          this.actions.setChatStatus({typing: false});
        }, 8000);

        this.actions.setChatStatus({typing: true, typingTimeout: timeout});
      }
    },

    handleErrors: (...args) => {
      console.log('----------ERRO', args);
    },
  };

  /* ----------------------------------- PUBLIC METHODS --------------------------------- */

  publicMethods = {
    setStatus: status => {
      this.actions.setStatus(status);
    },
    postMessage: msg => {
      if (typeof window === 'undefined' || window.parent === window.self) {
        return;
      }
      window.parent.postMessage(msg, '*');
    },
    setIsTitleNotificationEnabled: val => {
      this.actions.setIsTitleNotificationEnabled(val);
    },
    setWindowActive: active => {
      this.actions.setWindowActive({active});

      if (active) {
        const chat = this.store.state.status.chat;
        const activeInteraction = get(chat, 'activeInteraction.id');

        if (activeInteraction) {
          // get the active conversation
          const conversation = chat.conversations.get(activeInteraction);
          // find the last sent/received message id
          const messageId = conversation.messages.findFromBottom(
            m => m.status === MessageStatus.incoming,
          );
          if (messageId && messageId !== conversation.lastSeenByUser) {
            this.publicMethods.markMessageAsViewed(activeInteraction, messageId);
          }
        }
      }
    },
    markMessageAsViewed: (interactionId, messageId) => {
      this.actions.setChatStatus({
        conversations: this.store.state.status.chat.conversations.extend(interactionId, {
          lastSeenByUser: messageId,
        }),
      });
      this.Api.viewMessage({interactionId, messageId, socket: this.socket});
    },
    /* Opens the widget (show the body)*/
    open: () => {
      this.actions.setStatus({isOpen: true});
    },
    /* Closes the widget (goes to trigger only view)*/
    close: () => {
      this.actions.setStatus({isOpen: false});
    },
    /* Extends the current status with new fields (deep extend)
     * @param opts {object}
     */
    setOptions: (opts, msgs = {}, newStatus) => {
      const {state} = this.store;
      const options = deepExtend({}, state.options, opts);
      const messages = deepExtend({}, state.messages, msgs);
      const status = newStatus ? {...state.status, ...newStatus} : state.status;
      this.actions.setState({options, messages, status});
      // Only update colors if sent as part of the new options
      if (opts.styleVars && this.Parent) {
        this.Parent.setStyleVars(options.styleVars);
      }
    },

    setMessages: msgs => {
      const messages = deepExtend({}, this.store.state.options, msgs);
      this.actions.setState({messages});
    },

    // ----------------------------------- BOOKING ---------------------------------
    loadBooking: bookingId => {
      this.Api.loadBooking({
        bookingId,
        appId: this.store.state.target.appId,
        socket: this.socket,
      });
    },
    editBooking: id => {
      this.publicMethods.goToNextStep('booking-invitation', {selectedBooking: id});
    },
    // ----------------------------------- CHAT ---------------------------------
    initChatConnection: () => {
      this.applyBatchActionsToSocket('on');
      // prepare to know wheter the window is active
      window.addEventListener('blur', this.onWindowBlur);
      document.addEventListener('blur', this.onWindowBlur);
      window.addEventListener('focus', this.onWindowFocus);
      document.addEventListener('focus', this.onWindowFocus);
    },
    endChatConnection: () => {
      this.applyBatchActionsToSocket('off');
    },

    /* this updates our knowledge about the conversations and its messages
     * triggered on chat's componentDidMount and handle reconnection
     */
    updateInteraction: interactionId => {
      if (interactionId) {
        this.actions.setChatConversationStatus({interactionId, data: {loading: interactionId}});
        this.socket.emit(SocketEvents.updateInteraction, {interactionId}, (err, data = {}) => {
          if (data && data.interaction) {
            this.actions.updateConversation({
              interactionId,
              data: {...data.interaction, loading: false},
            });
          }
        });
      }
    },

    initConversation: (id, ui) => {
      this.queue.reset();
      this.actions.setChatStatus({
        activeInteraction: {id: id || Math.random(), isTempInteractionId: !id},
      });
      this.actions.setStatus({currentAction: UITypes.chat});
    },

    sendBotMessage: (text, scriptStep) => {
      if (text.wait === WaitTypes.input) {
        this.actions.setActiveConversationStatus({waiting: true, scriptStep});
      } else if (isConditionMessage(text)) {
        this.actions.setActiveConversationStatus({scriptStep});
      } else {
        this.publicMethods.sendMessage(text, MessageOwners.bot, scriptStep);
      }
    },

    sendMessage: (text, owner = MessageOwners.client, scriptStep) => {
      if (!text) {
        return;
      }

      const {
        status,
        status: {currentAction, currentStep, chat},
      } = this.store.state;

      let {id: interactionId, isTempInteractionId} = chat.activeInteraction;

      // create message object
      const tempId = new Date().getTime();
      const message = {
        id: tempId,
        tempId,
        message: text,
        owner,
        status: MessageStatus.sending,
      };

      // if there no interaction yet
      if (!interactionId) {
        interactionId = generateTempInteractionId();
        isTempInteractionId = true;

        // if it's a new conversation and the originating action is not chat, then we'll flag the previous interactionId as parent
        if (currentAction !== InputTypes[chat] && get(status[currentAction], 'interactionId')) {
          message.parent = status[currentAction].interactionId;
        }

        // add it to state and clean current input value
      }

      this.actions.setChatStatus({
        activeInteraction: {
          id: interactionId,
          isTempInteractionId,
        },
      });

      // add message to conversation
      this.actions.addMessageToConversation({
        interactionId,
        message,
        scriptStep,
      });

      const conversation = chat.conversations.get(interactionId) || {};
      if (conversation.hasSentMessages || owner === MessageOwners.client) {
        const toSend = conversation.hasSentMessages
          ? message
          : this.store.state.status.chat.conversations.get(interactionId).messages.items();
        this.actions.setActiveConversationStatus({hasSentMessages: true});
        this.queue.enqueue(toSend);
      }
    },

    sendTyping: () => {
      const {
        activeInteraction: {id},
        conversations,
      } = this.store.state.status.chat;
      const conversation = conversations.get(id);

      if (conversation) {
        const messageId = (conversation.messages.getLast() || {}).id || 0;
        debounceByKey(
          () => {
            this.socket.emit(SocketEvents.typing, {interactionId: id});
          },
          3000,
          'typing',
          id,
          messageId,
        );
      }
    },

    submitContactData: data => {
      this.publicMethods.addDataToMessage(data, SocketEvents.submitContactData, true);
    },

    /* Creates a new book
     * if data.interaction is present, it means we're in a chat interaction
     * otherwise it's a standalone request
     */
    submitBooking: data => {
      const {target} = this.store.state;
      return this.Api.createBooking({
        ...data.data,
        target,
        socket: this.socket,
        interactionId: data.interactionId,
        messageId: data.id,
      }).then(({resp}) => {
        // only for chat, we update the message reference
        if (resp && resp.id && data.interactionId) {
          this.publicMethods.addDataToMessage({
            ...data,
            data: {id: resp.id},
          });
        }

        return resp;
      });
    },

    cancelBooking: data => {
      const {target} = this.store.state;
      return this.Api.cancelBooking({
        ...target,
        ...data,
        socket: this.socket,
      });
    },

    /* Add data to a dynamic message
     * @param data {object} {id, interactionId, data}
     * @param socketEvent {string} is the type of event sent to the server
     */
    addDataToMessage: (data, socketEvent = SocketEvents.addDataToMessage, forceUpload = false) => {
      this.actions.addDataToMessage({...data, event: socketEvent});

      const chat = this.store.state.status.chat;
      const interactionId = get(chat, 'activeInteraction.id');
      const conversation = chat.conversations.get(interactionId) || {};
      const hasSentMessages = conversation.hasSentMessages;
      // if meesages have not been set for this conversarion, send all of them now
      if (
        !forceUpload &&
        hasSentMessages === false &&
        conversation.messages &&
        !conversation.messages.isEmpty()
      ) {
        const msgs = conversation.messages.items();
        this.actions.setActiveConversationStatus({hasSentMessages: true});
        this.queue.enqueue(msgs);
      } else {
        // otherwise just emit the addDataToMessage message
        this.socket.emit(socketEvent, data);
      }
    },

    setActiveConversationStatus: data => {
      this.actions.setActiveConversationStatus(data);
    },

    uploadFile: formData => {
      return this.Api.uploadFile({raw: formData});
    },

    // ----------------------------------- RATE ---------------------------------
    /* Sends a value numeric rating to the API
     * @param ratign {integer}
     */
    rate: (rating, message, config) => {
      const {
        options,
        target,
        status: {
          interactions,
          publicId,
          currentStep,
          currentAction,
          rate: {interactionId, token},
          chat,
        },
      } = this.store.state;

      // if it's in a chat message
      // TODO: find a better condition
      if (!isEmpty(message) && (Object.keys(message) > 1 || !isEmpty(message.message))) {
        const msg = {
          data: {value: rating},
          id: message.id,
          interactionId: get(chat.activeInteraction, 'id'),
        };
        this.publicMethods.addDataToMessage(msg, SocketEvents.rateInChat);
      } else {
        // if it's a standalone rate widget
        this.actions.rateSelected({value: rating, config});

        this.Api.rate({
          ...target,
          publicId,
          token,
          interactionId,
          parent:
            // if there is no rate.interactionId yet, use the active chat or the last interactionId as parent
            // if they exist
            !interactionId && interactions.length
              ? chat.activeInteraction || interactions[interactions.length - 1].id
              : null,
          value: rating,
          scale: options.scale,
        }).then(({resp, request}) => {
          if (request.status === 200) {
            this.actions.setRateStatus({
              interactionId: resp.id,
              token: resp.token,
            });
          }
        });
      }
    },

    // ----------------------------------- NAVIAGATION ---------------------------------

    goToFirstScreen: () => {
      this.actions.resetStatus();
    },

    goToNextStep: (action, actionData = {}) => {
      console.log(action, actionData);
      const {currentStep, currentAction} = this.store.state.status;
      if (currentAction) {
        this.actions.setStatus({
          currentStep: (currentStep || 0) + 1,
          actionData,
        });
      } else {
        this.actions.setStatus({
          currentAction: action,
          currentStep: 0,
          actionData,
        });
      }
      this.Parent.scrollToTop();
    },

    /* ----------------------------------- OPERATOR --------------------------------- */
    getOperator: operator => {
      const {
        target: {appId},
      } = this.store.state;
      this.Api.getOperator({appId, operator, socket: this.socket});
    },
  };

  /* ----------------------------------- RENDER --------------------------------- */
  render() {
    const {appId, campaignId} = this.props;
    const {
      status: {loaded},
    } = this.store.state;

    if (!loaded) {
      return null;
    }
    return (
      <Parent {...this.store.state} methods={this.publicMethods} ref={p => (this.Parent = p)} />
    );
  }
}

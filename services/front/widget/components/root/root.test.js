/* eslint-env node, jest */
import {mount} from 'enzyme';
import React from 'react';

import Api from '../../connections/__mocks__/mock-widget-api';
import SafeSocket from '../../connections/__mocks__/mock-safe-socket';

import Root from './root';

describe('Root Component', () => {
  Api.clientConnect.mockClear();

  const props = {
    Api,
    SafeSocket
  };

  const wrapper = mount(<Root {...props} />);

  it('root rendered', () => {
    expect(wrapper.length).toBe(1);
  });

  it('Client connect called', () => {
    expect(Api.clientConnect.mock.calls.length).toBe(1);
  });

  it('Parent component rendered after connecting', () => {
    // after it re-renders
    wrapper.update();
    expect(wrapper.find('WidgetParent').length).toBe(1);
  });
});

import React, {Component} from 'react';
import classnames from 'classnames';

import {WidgetPosition} from '@Widget/config/widget-ui-constants';
import {Logo, Close} from '../display/svg-icons';
import css from './trigger.scss';

export default class Trigger extends Component {
  onClick = () => {
    const {
      status: {isOpen},
      methods: {open, close}
    } = this.props;
    if (!isOpen) {
      open();
    } else {
      close();
    }
  };

  render() {
    const {
      appId,
      campaignId,
      options,
      status,
      socket,
      isMobile,
      options: {inline},
      status: {isOpen}
    } = this.props;
    return (
      <div
        className={classnames(css.trigger, {
          [css.standAlone]: !options.withinIframe,
          [css.left]: options.position === WidgetPosition.left,
          [css.isMobile]: isMobile,
          [css.inline]: inline
        })}
        onClick={this.onClick}
        ref={trigger => (this.trigger = trigger)}
      >
        {isOpen ? <Close className={css.close} /> : <Logo className={css.logo} />}
      </div>
    );
  }
}

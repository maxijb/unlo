import {RatingAfterSubmittedStatus} from './widget-ui-constants';
import {OrderedHash} from '@Common/utils/ordered-hash';

export default {
  // if the widget should be visible at all
  visible: true,
  position: 'right',
  showTrigger: true,
  inline: false,
  // ui widges on each screen and steps
  screens: [
    [
      {ui: 'welcome'},
      {
        ui: 'chat-invitation',
        steps: [[{ui: 'chat'}]]
      },
      {
        ui: 'open-hours'
      },
      {
        ui: 'booking-invitation',
        actionName: 'booking-invitation',
        steps: [{ui: 'booking'}],
        action: 'action-step'
      }
    ]
  ],
  // ----------------------------- RATE COMPONENT ---------------------------------
  // if rate component is visible use these rating to show values
  ratings: {
    scale: 5,
    items: [
      {value: 1, display: '1'},
      {value: 2, display: '2'},
      {value: 3, display: '3'},
      {value: 4, display: '4'},
      {value: 5, display: '5'}
    ]
  },
  // should we show the ratings widget after the first submit?
  // Possible values: (true, false, RatingAfterSubmittedStatus.thankYouMessage, RatingAfterSubmittedStatus.thankYouMessageWithRating)
  showRatingAfterSubmit: RatingAfterSubmittedStatus.thankYouMessageWithRating,
  // should we allow to re-submit after a rating has been clicked?
  allowModifyRating: false,
  // ----------------------------- RATE COMPONENT ---------------------------------
  bookings: {
    duration: 45,
    defaultTimeInterval: 15,
    minValue: 1,
    maxValue: 10
  },
  styleVars: {
    'theme-color': '#4578db',
    'theme-text-color': 'white',
    'theme-color-hover': '#6A93E2',
    'theme-color-active': '#3a63b4'
  },
  // ----------------------------- MENU COMPONENT ---------------------------------
  menuImage: 'wiri-owned/1566077883539.svg',
  menuTitle: '!@wantToSeeOurMenu',
  // ----------------------------- BOOKING COMPONENT ---------------------------------
  defaultBookingDuration: 30,
  // ----------------------------- CAHT COMPONENT ---------------------------------
  botName: 'Bot',
  mainBotQuestions: [
    {
      question: 'Can I see the menu?',
      answer: 'Yes of course. Here you are.',
      ui: 'menu',
      active: true
    },
    {
      question: 'Where are you located?',
      answer: 'Here are the details and map of our location.',
      ui: 'map',
      active: true
    },
    {
      question: 'What time are you open?',
      answer: 'These are our opening hours',
      ui: 'openHours',
      active: true
    },
    {
      question: 'I would like to place a reservation',
      answer: "Great. Just fill in the details below and we'll take care of it.",
      ui: 'booking',
      active: true
    }
  ],
  openHours: [],
  soundOnMessage: true,
  //----------------------- TRIGGER -------------------------
  triggerVerticalOffset: 0,
  triggerHorizontalOffset: 0,
  mobileTriggerVerticalOffset: undefined,
  mobileTriggerHorizontalOffset: undefined
};

export const Messages = {
  chatInputErrorUpload:
    'Error uploading file. Only jpg, gif and png file formats with a maximum file size of 2Mb are allowed.',
  welcomeTitle: 'Hi! Welcome',
  welcomeSubtitle:
    'We are here to help you! Ask us anything or click around the options below to learn more about us.',
  poweredBy: 'Powered by Wiri',
  // ----------------------------- OPEN HOURS ---------------------------------
  openNow: 'Now Open',
  theseAreOpenHours: 'These are our opening hours',
  // ----------------------------- MENU COMPONENT ---------------------------------
  menuPreMessage: 'Yes of course. Click on the links below to see them.',
  // ----------------------------- RATE COMPONENT ---------------------------------
  rateTitle: 'How did we do today?',
  rateTitleChat:
    'Your feedback helps us improve our service and support. Please take a minute to rate your experience with our customer support.',
  afterRateSubmitted: 'Please let us know how to improve',
  thankYouMessage: 'Thanks for your feedback.',
  // ----------------------------- CHAT INVITATION COMPONENT ---------------------------------
  chatInvitationTitle: 'Your conversations',
  newConversation: 'Start new conversation',
  you: 'You',
  operator: 'Operator',
  bot: 'Bot',
  unreadMessages: 'Unread messages',
  showAllLabel: 'See all conversations',
  // ----------------------------- GO BACK COMPONENT ---------------------------------
  goBack: '< Go Back',
  // ----------------------------- CHAT COMPONENT ---------------------------------
  optionsChooseOrType: 'Choose an option or type your own message.',
  chatWritePlaceholder: 'Write your reply...',
  selectEmoji: 'Select emoji',
  botGreeting: 'How can we help you today?',
  okShowMeMoreOptions: 'OK, show me more options',
  anythingElseICanHelp: 'Anything else I can help you with?',
  received: 'Received, not viewed yet',
  viewed: 'Viewed',
  failed: 'Failed',
  sending: 'Sending',
  incoming: 'Incoming',
  teamWillReachOut:
    'A staff member will reply to your question. They usually respond in a few minutes',
  provideContactStayInTouch:
    'Would you like to leave us your contact details so we can get in touch with you?',
  contactDataSubmitted: "Great! Your data was submitted correctly. We'll get back to you soon.",
  contactWillNotifyByEmail: "We'll notify you at {{email}} once a reply becomes available",
  // ----------------------------- CONTACT COMPONENT ---------------------------------
  contactPlaceholder: 'Enter email',
  contactConsent: 'I agree to my personal data being stored and used to receive the newsletter',
  contactConsentComms: 'I agree to receive information and commercial offers',
  contactSubmit: 'Submit',
  contactErrorConsent: 'We need your consent to store your email',
  contactErrorEmailRequired: 'Your email is required to contact you later',
  contactErrorNameForBooking: 'Your name is required to continue',
  contactNamePlaceholder: 'Name',
  contactPhonePlaceholder: 'Phone number',
  bookingErrorTimeRequired: 'This field is required',
  // ----------------------------- BOKKING INVITATION MESSAGES ---------------------------------
  bookingInvitationTitle: 'Book a table',
  bookingInvitationDescription: 'Follow a few simple steps to book your table with us.',
  bookingInvitationActionButton: 'Create a new reservation',
  yourReservations: 'Your reservations',
  // ----------------------------- OPEN HOURS MESSAGES ---------------------------------
  openingHours: 'Our Opening Hours',
  // ----------------------------- MENU MESSAGES ---------------------------------
  menu: 'Menu',
  wantToSeeOurMenu: 'Want to see our Menu?',
  // ----------------------------- OFFERS MESSAGES ---------------------------------
  claimOffer: 'Get this offer',
  offerClaimButton: 'Submit',
  offerClaimed: 'Email submited',
  offerSuccess: 'Success!!!',
  offerSuccessDescription: 'You are entitled to this offer.',
  offerSuccessExplanation: "You'll receive instructions in your email on how to claim the offer.",

  // ----------------------------- BOKKING MESSAGES ---------------------------------
  bookingChatIntroduction: 'Great! To place your reservation please follow the steps below.',
  bookingModuleTitle: 'Book a table',
  bookingModuleTitleReserved: 'Your table is reserved',
  bookingModuleDateDescription:
    'Please select the date you want to book your table for and click next.',
  bookingModuleTimeDescription:
    'Please let us know the following details to complete your reservation.',
  bookingModuleContactDescription:
    'To confirm your reservation just leave us your full name and email below you will get an email notification with the details of your reservation.',
  bookingModuleNextButton: 'Next',
  bookingModuleBackButton: 'Back',
  bookingSuccessTitle: 'Success',
  bookingSuccessDescription:
    'Your table has been booked, check your email with your booking details.',
  bookingSuccessReservationMessage:
    'Table reservation for {{name}}, for {{value}} people on {{date}} at {{time}}',
  bookingModuleBookButton: 'Confirm',
  bookingLoadingMessage: 'A booking was completed. Loading data...',
  bookingNotAvailable: 'A booking was completed, but the data is not available',
  stepN: 'Step {{step}}',
  bookingTimeLabel: 'Select time',
  bookingValueLabel: 'Select number of guests',
  bookingLabelLabel: 'Select eating area',
  bookingThankYou: 'Fantastic your reservation is completed. Thank you, and see you here soon.',
  bookingTableReservationFor: 'Table reservation for',
  bookingEdit: 'Edit',
  stepNofX: 'Step {{step}} of {{total}}',
  bookingCompleted: 'Completed',
  bookingOnlineReservation: 'Online booking',
  ok: 'Ok',
  takeMeBack: 'OK, take me back',
  bookingCancel: 'Cancel reservation',
  sureCancelReservation: 'Are you sure you want to cancel this reservation?',
  doCancelReservationWidget: 'Yes, I am sure',
  bookingIsCancelled: 'Your booking has been cancelled',
  addToGoogleCalendar: 'Add to Google Calendar',
  addToAppleCalendar: 'Add to Apple Calendar',
  // ----------------------------- MAP ---------------------------------
  ourLocation: 'Our location:',
  clickToOpenInGoogleMaps: 'Click to open in Google Maps ',
  openInGoogleMaps: 'Open in Google Maps ',
  // ----------------------------- COMMON MESSAGES ---------------------------------
  months: [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ],
  shortMonths: [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sept',
    'Oct',
    'Nov',
    'Dec'
  ],
  days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
  shortDays: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
  shorterDays: ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'],
  closed: 'Closed',

  defaultModuleDisplayMessage: {
    options: 'Operator provided options',
    rate: 'Operator asked your satisfaction level',
    'capture-input': 'Waiting for user input',
    contact: 'Operator asked for contact information',
    booking: 'A reservation was started',
    menu: 'Operator showed the menu',
    'open-hours': 'Opening hours were shown',
    hours: 'Opening hours were shown',
    promotion: 'Information was provided',
    map: 'Our location'
  },
  defaultModuleDescriptionMessage: {
    map: 'Launch location details and map in chat',
    rate: 'Launch conversation rating survey',
    contact: 'Request contact information from user',
    booking: 'Launch booking process',
    hours: 'Show opening hours table',
    menu: 'Show the menu',
    promotion: 'Show some information'
  },
  defaultModuleName: {
    'chat-invitation': 'Conversations',
    'open-hours': 'Opening hours',
    'booking-invitation': 'My reservations',
    'map-invitaton': 'Location'
  },
  timeDeltaShort: {
    seconds: 'Now',
    minutes: '{{count}}m',
    hours: '{{count}}h',
    days: '{{count}} day',
    weeks: '{{count}} week',
    months: '{{count}} month',
    years: '{{count}} year',

    days_plural: '{{count}} days',
    weeks_plural: '{{count}} weeks',
    months_plural: '{{count}} months',
    years_plural: '{{count}} years'
  },
  timeDelta: {
    'past-seconds': 'A few seconds ago',
    'past-minutes': '{{count}} minute ago',
    'past-hours': '{{count}} hour ago',
    'past-days': '{{count}} day ago',
    'past-weeks': '{{count}} week ago',
    'past-months': '{{count}} month ago',
    'past-years': '{{count}} year ago',

    'past-minutes_plural': '{{count}} minutes ago',
    'past-hours_plural': '{{count}} hours ago',
    'past-days_plural': '{{count}} days ago',
    'past-weeks_plural': '{{count}} weeks ago',
    'past-months_plural': '{{count}} months ago',
    'past-years_plural': '{{count}} years ago',

    'forward-seconds': 'A few seconds from now',
    'forward-minutes': '{{count}} minute from now',
    'forward-hours': '{{count}} hour from now',
    'forward-days': '{{count}} day from now',
    'forward-weeks': '{{count}} week from now',
    'forward-months': '{{count}} month from now',
    'forward-years': '{{count}} year from now',

    'forward-minutes_plural': '{{count}} minutes from now',
    'forward-hours_plural': '{{count}} hours from now',
    'forward-days_plural': '{{count}} days from now',
    'forward-weeks_plural': '{{count}} weeks from now',
    'forward-months_plural': '{{count}} months from now',
    'forward-years_plural': '{{count}} years from now'
  }
};

export const DefaultStatus = {
  loaded: false,
  currentStep: 0,
  currentAction: null,
  // data for current action
  actionData: {},
  rate: {},
  chat: {
    conversations: new OrderedHash(),
    activeInteraction: {}
  },
  bookings: {
    list: new OrderedHash(),
    selected: null
  },
  interactions: [],
  operators: {},
  isWindowActive: true
};

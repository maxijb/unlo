export const WidgetConfig =
  process.env.NODE_ENV === 'production'
    ? {
        domain: 'https://wiri.io',
        appDomain: 'https://wiri.io',
        socketPath: '/api/socket.io',
        protocol: 'https',
        cdnDomain: 'wiri.io'
      }
    : {
        domain: 'http://api:3001',
        appDomain: 'http://localhost:8000',
        socketPath: '/api/socket.io',
        protocol: 'http',
        cdnDomain: 'localhost:8000'
      };

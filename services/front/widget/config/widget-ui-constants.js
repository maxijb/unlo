export const RatingAfterSubmittedStatus = {
  true: true,
  false: false,
  thankYouMessage: 'thankYouMessage',
  thankYouMessageWithRating: 'thankYouMessageWithRating'
};

export const PromotionTemplate = {
  imageTop: 'image-top',
  imageLeft: 'image-left',
  imageRight: 'image-right'
};

export const PromotionAction = {
  link: 'link',
  actionStep: 'action-step',
  nextStep: 'nextStep'
};

export const PromotionLinkTarget = {
  self: 'self',
  blank: 'blank',
  history: 'history'
};

export const WidgetPosition = {
  left: 'left',
  right: 'right'
};

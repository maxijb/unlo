export default class SafeSocket {
  constructor(url, {path, query}) {
    this.options = {
      url,
      path,
      query
    };
    Object.assign(this, {
      connect: jest.fn(),
      on: jest.fn(),
      off: jest.fn(),
      emit: jest.fn(),
      close: jest.fn()
    });
  }
}

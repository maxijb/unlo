export default {
  clientConnect: jest.fn(() => {
    return Promise.resolve({
      resp: {
        publicId: 45
      }
    });
  })
};

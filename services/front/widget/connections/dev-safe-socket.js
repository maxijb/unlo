import {SocketEvents} from '@Common/constants/socket-status';
import get from 'lodash.get';
/* This is used to embed a dummy socket interface to development widget instances
 */

export default class DevSafeSocket {
  constructor(url, {path, query}) {}

  connect() {}

  on(...args) {}

  off(...args) {}

  emit(event, ...args) {
    const cb = args.pop();
    if (event === SocketEvents.updateInteraction) {
      if (get(args, [0, 'interactionId']) === 'empty') {
        return cb(null, null);
      }
      cb(null, {
        interaction: {
          id: -1,
          messages: [
            {
              interaction: -1,
              id: -2,
              owner: 'client',
              message: "I'll be there tomorrow"
            },
            {
              interaction: -1,
              id: -1,
              owner: 'bot',
              message: 'Your table will be ready!'
            }
          ]
        }
      });
    }
  }

  close() {}
}

import {startApiKeys} from '@Widget/connections/widget-api';
import PublicAPI from '@Common/api/public-api';

const noop = () => Promise.resolve({});
const noopified = startApiKeys(PublicAPI, '', noop);

export default {
  ...noopified,
  clientConnect: ({appId, campaignId, lang}) => {
    const bookingDate = new Date().getTime() + 24 * 60 * 60 * 1000;
    return Promise.resolve({
      resp: {
        publicId: -1,
        appId,
        campaignId,
        lang,
        bookings: [
          {
            id: -1,
            appId,
            startDate: bookingDate,
            endDate: bookingDate,
            time: '12:00',
            value: 4,
            label: 'Bar',
            email: 'test@example.com',
            name: 'Jonas Smith'
          }
        ],
        conversations: [
          {
            id: -1,
            hasMessage: true,
            message: 'Your table will be ready!',
            type: 'chat'
          }
        ],
        user: {
          consent: true,
          extendedConsent: false,
          name: 'Jonas Smith',
          email: 'test@example.com'
        }
      }
    });
  }
};

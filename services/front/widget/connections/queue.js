import isEmpty from 'is-empty';
import {SocketEvents} from '@Common/constants/socket-status';
import {isConditionMessage} from '@Common/utils/bot-utils';

export default class Queue {
  constructor(socket, store, cb) {
    this.socket = socket;
    this.blocked = false;
    this.queue = [];
    this.store = store;
    this.cb = cb;
  }

  reset() {
    this.blocked = false;
    this.queue = [];
  }

  enqueue(msg) {
    this.queue = this.queue.concat(msg);
    this.tryToSend();
  }

  tryToSend() {
    if (!isEmpty(this.queue) && !this.blocked) {
      const msg = this.queue.shift();
      // Filter non graphicla messages from being sent to the server
      if (isConditionMessage(msg.message)) {
        return this.tryToSend();
      }
      this.blocked = true;
      const {
        id: interactionId,
        isTempInteractionId
      } = this.store.state.status.chat.activeInteraction;
      // send object to WS server
      const message = {...msg, interactionId};
      delete message.isTempInteractionId;
      if (isTempInteractionId) {
        message.isTempInteractionId = true;
      }
      this.socket.emit(SocketEvents.message, message, this.callback.bind(this, message));
    }
  }

  callback = (message, ...params) => {
    this.cb(message, ...params);
    setTimeout(() => {
      this.blocked = false;
      this.tryToSend();
    }, 50);
  };
}

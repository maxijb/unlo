import SafeSocket from './__mocks__/mock-safe-socket';
import Store from '../store/store';
import Defaults, {DefaultStatus} from '../config/defaults';
import Queue from './queue';

const view = {forceUpdate: jest.fn()};
const initialState = {
  options: Defaults,
  status: DefaultStatus
};
const store = new Store(view, initialState);
const socket = new SafeSocket('/test', {});
const cb = jest.fn();

describe('Queue of messages', () => {
  const queue = new Queue(socket, store, cb);
  it('stores resources', () => {
    expect(queue.store === store).toBeTruthy();
    expect(queue.cb === cb).toBeTruthy();
    expect(queue.queue).toEqual([]);
    expect(queue.blocked).toEqual(false);
  });

  it('enqueus msg', () => {
    queue.blocked = true;
    queue.enqueue({msg: 45});
    expect(queue.queue).toEqual([{msg: 45}]);
  });

  it('enqueus multiple msg', () => {
    queue.enqueue([{msg: 145}, {msg: 134}]);
    expect(queue.queue).toEqual([{msg: 45}, {msg: 145}, {msg: 134}]);
  });

  it('sends a message', () => {
    queue.blocked = false;
    queue.tryToSend();
    expect(queue.queue).toEqual([{msg: 145}, {msg: 134}]);
  });
});

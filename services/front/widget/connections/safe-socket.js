import io from 'socket.io-client/dist/socket.io.slim';

/* Connect to websocket only when it's necesary
 * This class exposes all used socket.io methods with a thin wrapper
 * in a way that the actual socket connection is not made until required
 */

export default class SafeSocket {
  constructor(url, {path, query}) {
    this.options = {
      url,
      path,
      query
    };
    this.socket = null;
  }

  connect() {
    if (this.socket) {
      return;
    }

    const {url, path, query} = this.options;
    this.socket = io(url, {
      path,
      query
    });
  }

  on(...args) {
    this.connect();
    return this.socket.on(...args);
  }

  off(...args) {
    this.connect();
    return this.socket.off(...args);
  }

  emit(...args) {
    // TODO: check if this is necessary
    // this.connect();
    return this.socket.emit(...args);
  }

  close() {
    if (this.socket) {
      this.socket.close();
    }
  }
}

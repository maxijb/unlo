export const initApiActions = (api, store) => {
  return Object.keys(api).reduce((prev, key) => {
    prev[key] = payload => {
      store.reducer(store, store.state, {type: `${key}.start`, payload});
      return api[key](payload).then(response => {
        const type = !response.resp || response.resp.error ? 'failure' : 'success';
        store.reducer(store, store.state, {
          type: `${key}.${type}`,
          payload: {...payload, resp: response.resp}
        });
        return response;
      });
    };
    return prev;
  }, {});
};

//this.actions = Object.keys(actions).reduce((api, key) => {
//      api[key] = payload => {
//        this.reducer(this, this.state, {type: key, payload});
//      };
//      return api;
//    }, {});

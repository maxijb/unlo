/* globals fetch */
import QS from 'qs';

import PublicAPI from '@Common/api/public-api';
import {PUBLIC_API_PREFIX} from '@Common/constants/app';
import {formatUri} from '@Common/utils/urls';
import {SocketEvents} from '@Common/constants/socket-status';

import {WidgetConfig} from '../config/env';

/* Expose the public API based on the definition set in /api/public-api
 * Create an a hook for each endpoint and exposes those hooks to the client widget
 */
export default startApiKeys(PublicAPI);

/* Recursively inits each api function, exposing it to the server- and client-side
 * @param base {object} branch of the aopi we're visiting in this iteration
 * @param prefix {string} prefix for urls
 * @param hardcodedCb {function} if we're mocking the CB we'll pass in some dummy func
 */
export function startApiKeys(base, prefix = '', hardcodedCb = false) {
  const uapi = Object.keys(base).reduce((prev, key) => {
    // We ignore the key middleware as it's a protected keyword to define behavior in the api
    if (key === 'middleware' || key === 'method') {
      return prev;
    }

    if (!base[key].hasOwnProperty('controller') && !base[key].hasOwnProperty('socketController')) {
      prev[key] = startApiKeys(base[key], `${prefix}${key}.`, hardcodedCb);
    } else {
      const item = base[key];
      const uri = formatUri(item.path || `${prefix || ''}${key}`, PUBLIC_API_PREFIX);
      prev[key] = hardcodedCb || initFetchController(item, uri);
    }
    return prev;
  }, {});

  return uapi;
}

/* From the browser each action will send an AJAX request */
function initFetchController(item, uri) {
  const method = item.method || 'GET';
  return query => {
    let request;

    // This part is only related to sockets
    // if a .socket is found within the query we assume it needs to be broadcasted via sockets
    if (query && query.socket && query.socket.socket) {
      const {socket, broadcast, ...other} = query;
      return new Promise((resolve, reject) => {
        // use socketController when defined
        // fallback to http controller
        const handler = item.socketController
          ? {socketController: item.socketController}
          : {controller: item.controller};

        socket.emit(
          SocketEvents.apiMethod,
          {
            ...other,
            socket: {
              ...handler,
              broadcast
            }
          },
          (err, data) => {
            if (!err) {
              resolve({resp: data});
            } else {
              resolve({
                resp: err.error || err
              });
            }
          }
        );
      });
    }

    if (method === 'GET') {
      request = fetch(`${WidgetConfig.domain}${uri}?${QS.stringify(query)}`, {
        headers: {
          'Content-Type': 'application/json'
        },
        credentials: 'include',
        mode: 'cors'
      });
    } else {
      request = fetch(`${WidgetConfig.domain}${uri}`, {
        method: method.toLowerCase(),
        headers: query.raw
          ? {}
          : {
              'Content-Type': 'application/json'
            },
        body: query.raw || JSON.stringify(query),
        credentials: 'include',
        mode: 'cors'
      });
    }

    return request
      .then(async response => {
        let resp;
        let clone;
        if (!response.ok) {
          clone = response.clone();
        }
        try {
          resp = await response.json();
        } catch (e) {
          const b = await clone.text();
          resp = {error: b};
        }

        return {
          resp,
          request: {
            status: response.status,
            statusText: response.statusText
          }
        };
      })
      .catch((error, ...params) => {
        return Promise.reject({resp: {error}});
      });
  };
}

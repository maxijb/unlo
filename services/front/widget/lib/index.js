import React from 'react';
import ReactDOM from 'react-dom';

import Widget from '../components/root/root';
import Api from '../connections/widget-api';
import SafeSocket from '../connections/safe-socket';

export class WidgetWrapper {
  constructor(props) {
    this.__private = {
      setInstance: instance => {
        this.__private.instance = instance;
        Object.assign(this, instance.publicMethods);
      }
    };

    const defaultProps = {
      ...props,
      wrapper: this,
      Api,
      SafeSocket
    };

    // add a div and then dump inside
    const div = document.createElement('div');
    document.querySelector(props.selector || 'body').appendChild(div);
    ReactDOM.render(<Widget {...defaultProps} />, div);
  }
}
export default WidgetWrapper;

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
// This code executes in an iframe /public/widget
// as soon as the library is loaded
/////////////////////////////////////////////////////////////

if (typeof window !== 'undefined' && window.self !== window.top) {
  window.top.postMessage({action: 'wiri-ready'}, '*');

  window.addEventListener('message', msg => {
    const action = msg.data && msg.data.action;

    if (!action) {
      return;
    } else if (action === 'wiri-initProps') {
      window.widget = new WidgetWrapper({
        ...msg.data.props,
        options: {
          ...(msg.data.props ? msg.data.props.options : {}),
          withinIframe: true
        }
      });
    } else if (action === 'wiri-setStatus' && window.widget) {
      const {action, ...data} = msg.data;
      window.widget.setStatus(data);
    }
  });
}

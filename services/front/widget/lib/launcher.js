import {WidgetConfig as config} from '../config/env';
import {WidgetViewportId, WidgetMobileHtmlClass} from '@Common/constants/widget';
import cssVariables from '../components/root/_vars-widget.scss';
import {resolveOptions} from '../utils/launcher-utils';

export class WidgetWrapper {
  constructor(props = {}) {
    // Add mobile style
    const style = document.createElement('style');
    style.innerHTML = `
    html.${WidgetMobileHtmlClass}, html.${WidgetMobileHtmlClass} > body {
      position: static !important;
      overflow: hidden !important;
      scroll-behavior: smooth;
    }
    html.${WidgetMobileHtmlClass} > body {
      height: 0 !important;
      margin: 0 !important;
    }
    `;
    document.head.appendChild(style);

    const iframe = document.createElement('iframe');
    iframe.style.position = 'absolute';
    iframe.style.height = '100%';
    iframe.style.width = '100%';
    iframe.style.border = 0;
    iframe.style.minHeight = 0;
    iframe.style.minWidth = 0;
    this.iframe = iframe;

    // Frame is a div containing an iframe
    const frame = document.createElement('div');
    this.frame = frame;
    iframe.src = `${config.protocol}://${config.cdnDomain}/public/widget`;
    frame.style.position = 'fixed';
    frame.style.width = '1px';
    frame.style.height = '1px';
    frame.style.zIndex = (props.options && props.options.zIndex) || Number.MAX_SAFE_INTEGER;

    // Add to body
    document.querySelector(props.selector || 'body').appendChild(frame);
    frame.appendChild(iframe);

    // Initialize viewport variables
    this.viewports = [];
    this.wiriViewportActive = false;

    this.props = props;
    this.widgetOrigin = `${config.protocol}://${config.cdnDomain}`;
    this.WindowDimensions = this.getMaxDimensions();

    // Getting messages from widget
    window.addEventListener('message', (msg, origin) => {
      const action = msg.data && msg.data.action;
      if (!action || msg.origin !== this.widgetOrigin) {
        return;
      } else if (action === 'wiri-ready') {
        this.ready(msg);
      } else if (action === 'wiri-resizeIframe') {
        this.resizeIframe(msg);
      } else if (action === 'wiri-startTitleNotification') {
        this.startTitleNotification(msg);
      } else if (action === 'wiri-stopTitleNotification') {
        this.stopTitleNotification(msg);
      }
    });

    // Resize window will inform the iframe
    window.addEventListener('resize', () => {
      this.WindowDimensions = this.getMaxDimensions();
      iframe.contentWindow.postMessage(
        {
          action: 'wiri-setStatus',
          ...this.WindowDimensions
        },
        this.widgetOrigin
      );
    });
  }

  getMaxDimensions() {
    return {
      maxWidth: window.innerWidth,
      maxHeight: window.innerHeight
    };
  }

  ready = msg => {
    // widget ready
    this.iframe.contentWindow.postMessage(
      {
        action: 'wiri-initProps',
        props: {
          ...resolveOptions(this.props),
          status: {
            ...this.props.status,
            ...this.WindowDimensions,
            fragment: window.location.hash
          },
          url: window.location.href
        }
      },
      this.widgetOrigin
    );
  };

  // widget resize must resize the iframe
  resizeIframe = msg => {
    const frame = this.frame;
    const {width, height, right, bottom, left} = msg.data;
    //console.log(msg.data);
    if (height >= this.WindowDimensions.maxHeight) {
      //console.log('----resize--max dimensions and tocuhmove');
      if (frame.style.height === '100%') {
        return;
      }
      this.setWiriViewport();
      document.documentElement.classList.add(WidgetMobileHtmlClass);
      frame.style.right = 0;
      frame.style.left = 0;
      frame.style.bottom = 0;
      frame.style.top = 0;
      frame.style.width = '100%';
      frame.style.height = '100%';
    } else {
      this.removeWiriViewport();
      document.documentElement.classList.remove(WidgetMobileHtmlClass);
      //console.log('----resize--to', width, height);
      frame.style.right = right === 'auto' ? 'auto' : `${right}px`;
      frame.style.left = left === 'auto' ? 'auto' : `${left}px`;
      frame.style.top = `auto`;
      frame.style.bottom = `${bottom}px`;
      frame.style.width = `${width}px`;
      frame.style.height = `${height}px`;
    }
  };

  startTitleNotification = msg => {
    this.originalTitle = document.title;
    this.alternativeTitle = msg.data.title;
    this.continueNotificationTitle();
  };

  continueNotificationTitle = () => {
    const title =
      document.title === this.originalTitle ? this.alternativeTitle : this.originalTitle;

    document.title = title;

    this.notificationTitleTimeout = setTimeout(() => {
      this.continueNotificationTitle();
    }, 1000);
  };

  stopTitleNotification = msg => {
    document.title = this.originalTitle;
    clearTimeout(this.notificationTitleTimeout);
  };

  setWiriViewport = () => {
    if (!this.wiriViewportActive) {
      this.wiriViewportActive = true;
      // Remove other viewports
      [].slice.call(document.getElementsByTagName('meta')).forEach(e => {
        if ('viewport' === e.name) {
          this.viewports.push(e.cloneNode());
          e.parentNode.removeChild(e);
        }
      });

      const t = document.createElement('meta');
      (t.id = WidgetViewportId),
        (t.name = 'viewport'),
        (t.content = 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'),
        document.getElementsByTagName('head')[0].appendChild(t);
    }
  };

  removeWiriViewport = () => {
    if (this.wiriViewportActive) {
      this.wiriViewportActive = false;
      var t = document.getElementById(WidgetViewportId);
      if (t) {
        t.parentNode.removeChild(t);
      }

      for (let l = this.viewports.length, i = 0; i < l; i++) {
        document.getElementsByTagName('head')[0].appendChild(this.viewports.pop());
      }
    }
  };
}

export default WidgetWrapper;

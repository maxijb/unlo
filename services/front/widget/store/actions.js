export default {
  // ----------------------------- GENERAL
  setState: 'setState',
  setStatus: 'setStatus',
  setChatStatus: 'setChatStatus',
  setRateStatus: 'setRateStatus',
  setChatStatus: 'setChatStatus',
  setWindowActive: 'setWindowActive',
  resetStatus: 'resetStatus',
  // ----------------------------- CHAT
  setChatMessageStatus: 'setChatMessageStatus',
  addMessageToConversation: 'addMessageToConversation',
  setChatConversationStatus: 'setChatConversationStatus',
  updateConversation: 'updateConversation',
  setActiveConversationStatus: 'setActiveConversationStatus',
  markViewedMessage: 'markViewedMessage',
  setViewedMessageByOperator: 'setViewedMessageByOperator',
  // ----------------------------- RATE
  rateSelected: 'rateSelected',
  rateSelectedInMessage: 'rateSelectedInMessage',
  // ----------------------------- OPTTIONS
  addDataToMessage: 'addDataToMessage',
  // ----------------------------- CART
  cartAddReference: 'cartAddReference',
  // ----------------------------- USER CONTACT
  updateUserContact: 'updateUserContact',
  updateUserInfo: 'updateUserInfo',
  // ----------------------------- NOTIFICATIONS
  setIsTitleNotificationEnabled: 'setIsTitleNotificationEnabled',
  showContactModal: 'showContactModal',
};

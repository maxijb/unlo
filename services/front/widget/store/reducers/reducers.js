import get from 'lodash.get';
import isEmpty from 'is-empty';

import {InputTypes, PRISTINE_FIELD} from '@Common/constants/input';
import {processMessage} from '@Common/utils/macros-utils';
import {OrderedHash} from '@Common/utils/ordered-hash';
import {MessageOwners} from '@Common/constants/socket-status';
import {FieldsUserContact} from '@Common/constants/public-user';
import {sortByStartDate} from '@Common/utils/date-utils';
import {MessageStatus, SocketEvents} from '@Common/constants/socket-status';

import wrapReducers from './wrap-reducers';
import actions from '../actions';

const UnwrappedReducers = {
  // ---------------------------------------------- OPERATORS ---------------------------
  ['getOperator.start']: (state, action) => {
    return Reducers.setStatus(state, {
      operators: {
        ...state.status.operators,
        [action.payload.operator]: {loading: true}
      }
    });
  },
  ['getOperator.success']: (state, action) => {
    return Reducers.setStatus(state, {
      operators: {
        ...state.status.operators,
        ...action.payload.resp.operators
      }
    });
  },
  // ---------------------------------------------- BASIC STATE FUNCTIONS ---------------------------
  [actions.resetStatus]: (state, action) => {
    return Reducers[actions.setStatus](state, {
      currentAction: null,
      currentStep: 0,
      actionData: {},
      chat: {
        ...state.status.chat,
        activeInteraction: {}
      }
    });
  },
  [actions.markViewedMessage]: (state, action) => {
    return state;
    const chat = state.status.chat;
    const activeInteraction = get(chat, 'activeInteraction.id');

    if (!activeInteraction) {
      return state;
    }

    // get the active conversation
    const conversation = chat.conversations.get(activeInteraction);
    // find the last sent/received message id
    const messageId = conversation.messages.findFromBottom(m => m.status !== MessageStatus.sending);
    if (messageId !== conversation.lastViewed) {
      return Reducers.setChatStatus({
        conversations: chat.conversations.extend(activeInteraction, {
          lastViewed: messageId
        })
      });
    }

    return state;
  },
  [actions.setWindowActive]: (state, action) => {
    return {
      ...state,
      status: {
        ...state.status,
        isWindowActive: Boolean(action.payload.active)
      }
    };
  },
  [actions.setIsTitleNotificationEnabled]: (state, action) => {
    return {
      ...state,
      status: {
        ...state.status,
        isTitleNotificationEnabled: action.payload
      }
    };
  },
  [actions.setState]: (state, action) => {
    return {
      ...state,
      ...action.payload
    };
  },
  [actions.setStatus]: (state, action) => {
    return {
      ...state,
      status: {
        ...state.status,
        ...action.payload
      }
    };
  },
  [actions.setRateStatus]: (state, action) => {
    return {
      ...state,
      status: {
        ...state.status,
        rate: {
          ...state.status.rate,
          ...action.payload
        }
      }
    };
  },
  [actions.setChatStatus]: (state, action) => {
    return {
      ...state,
      status: {
        ...state.status,
        chat: {
          ...state.status.chat,
          ...action.payload
        }
      }
    };
  },
  // ---------------------------------------------- CHAT  ---------------------------
  [actions.setChatMessageStatus]: (state, action) => {
    const {id, interactionId, status, renameId} = action.payload;
    const {conversations} = state.status.chat;

    const conversation = conversations.get(interactionId);

    if (conversation) {
      const messages = renameId
        ? conversation.messages.rename(id, renameId, {status})
        : conversation.messages.extend(id, {status});

      const newData = {messages};

      if (renameId && conversation.lastSeenByUser === renameId) {
        conversation.lastSeenByUser = id;
      }

      // rename lastmessage of conversation too
      if (renameId && conversation.lastMessageId === id) {
        newData.lastMessageId = renameId;
      }

      return Reducers[actions.setChatStatus](state, {
        conversations: conversations.extend(interactionId, newData)
      });
    }
    return state;
  },

  /* Adds a new message to the list of a converstion */
  [actions.addMessageToConversation]: (state, action) => {
    const {
      status: {
        isWindowActive,
        chat: {conversations, activeInteraction}
      }
    } = state;

    const {interactionId, message, status, scriptStep} = action.payload;
    const conversation = conversations.get(interactionId) || {};
    const owner = message.owner;
    message.updatedAt = new Date();

    const newData = {
      // when a user sends a message it will be flagged as sent forever, as we want to send all messages
      // after the user has started an engagemente
      hasSentMessages:
        conversation.hasSentMessages || owner === MessageOwners.client || !isEmpty(message.data),
      // keep track of whether the user has entered free text to capture contact
      // or show that the team might reach out later
      hasClientMessage: conversation.hasClientMessage || owner === MessageOwners.client,
      // when a user is the owner of a message it means we mustt end any "waiting for input" we were doing
      // if it's someone else we don't change the value
      waiting: owner === MessageOwners.client ? false : conversation.waiting,
      // Update the current step of the script if the bot is sending the index
      scriptStep: owner === MessageOwners.bot && scriptStep ? scriptStep : conversation.scriptStep,
      // TODO: message status
      messages: (conversation.messages || new OrderedHash()).insert(message),
      // display message
      message: message.message,
      // display message owner
      messageOwner: owner,
      // last message id
      lastMessageId: message.id,
      updatedAt: new Date(),
      lastSeenByUser:
        isWindowActive && interactionId === get(activeInteraction, 'id')
          ? message.id
          : conversation.lastSeenByUser
    };

    if (owner !== MessageOwners.client && owner !== MessageOwners.bot) {
      newData.operator = owner;
    }

    return Reducers.setChatStatus(state, {
      conversations: conversations.moveToBottom(interactionId, newData)
    });
  },

  [actions.setChatConversationStatus]: (state, action) => {
    const {interactionId, data} = action.payload;
    // if it's loading and there is no converstaion yet with this id (new conversation)
    // assign empty messages
    if (data.loading && !state.status.chat.conversations.get(interactionId)) {
      data.messages = new OrderedHash();
    }
    return Reducers.setChatStatus(state, {
      conversations: state.status.chat.conversations.extend(interactionId, data)
    });
  },
  /* messages from operator */
  [actions.setViewedMessageByOperator]: (state, action) => {
    const {interactionId, messageId} = action.payload;
    if (!interactionId || !messageId) {
      return state;
    }

    const {
      status: {
        chat: {conversations, activeInteraction}
      }
    } = state;

    const conversation = conversations.get(interactionId);
    if (!conversation) {
      return state;
    }

    const newData = {
      lastSeenByOperator: Math.max(messageId, conversation.lastSeenByOperator || 0)
    };

    if (interactionId === get(activeInteraction, 'id')) {
      newData.messages = conversation.messages.extend(messageId, {status: MessageStatus.viewed});
    }

    return Reducers.setChatConversationStatus(state, {interactionId, data: newData});
  },

  [actions.updateConversation]: (state, action) => {
    const {interactionId, data} = action.payload;
    if (data) {
      data.hasSentMessages = false;
      data.hasClientMessage = false;
      const msgs = (data.messages || []).map(processMessage);
      // find operator and if it has sent messages yet
      for (let i = msgs.length - 1; i >= 0; i--) {
        const owner = msgs[i].owner;
        if (owner && owner !== MessageOwners.client && owner !== MessageOwners.bot) {
          data.operator = owner;
        }
        if (owner === MessageOwners.client) {
          data.hasSentMessages = true;
          data.hasClientMessage = true;
        }
        if (!isEmpty(msgs[i].data)) {
          data.hasSentMessages = true;
        }
        if (data.operator && data.hasSentMessages) {
          break;
        }
      }

      // capture state of last message
      const lastMessage = msgs[msgs.length - 1];
      if (lastMessage) {
        if (lastMessage.owner === MessageOwners.client) {
          lastMessage.status =
            data.lastSeenByOperator >= lastMessage.id
              ? MessageStatus.viewed
              : MessageStatus.received;
        }
        data.lastSeenByUser = lastMessage.id;
      }

      data.messages = new OrderedHash(msgs);

      return Reducers.setChatConversationStatus(state, {interactionId, data: processMessage(data)});
    }
    // TODO: handle error
    return state;
  },

  // ---------------------------------------------- RATE  ---------------------------
  [actions.rateSelected]: (state, action) => {
    // Optimistically update widget UI
    const {value, config} = action.payload;
    let newState = Reducers.setRateStatus(state, {payload: {value}});

    const {
      status: {currentAction, currentStep}
    } = state;

    if (!isEmpty(get(config, 'steps'))) {
      // if we are not in another path, set the action to rate and the first items
      // but if currentAction is defined, just move to the next step
      if (!currentAction) {
        newState = Reducers.setStatus(newState, {
          currentAction: InputTypes.rate,
          currentStep: 0
        });
      } else if (currentAction !== InputTypes.chat) {
        newState = Reducers.setStatus(newState, {
          currentStep: (currentStep || 0) + 1
        });
      }
    }

    return newState;
  },

  // ----------------------------------------- USER CONTACT ---------------------------
  [actions.updateUserContact]: (state, action) => {
    const contact = action.payload;

    const newData = {};
    ['identityId', ...FieldsUserContact].forEach(field => {
      if (typeof contact[field] !== 'undefined' && contact[field] !== PRISTINE_FIELD) {
        newData[field] = contact[field];
      }
    });

    if (!isEmpty(newData)) {
      return {
        ...state,
        status: {
          ...state.status,
          user: {
            ...state.status.user,
            ...newData
          }
        }
      };
    }

    return state;
  },
  // ----------------------------------------- BOOKINGS ---------------------------
  ['loadBooking.success']: (state, action) => {
    const booking = get(action.payload, 'resp.booking');

    // First add the loaded identity if available
    const identities = get(action.payload, 'resp.identity');
    const newState = {
      ...state,
      status: {
        ...state.status,
        user: {
          ...state.status.user,
          identities: {
            ...state.status.user.identities,
            ...identities
          }
        }
      }
    };

    if (booking) {
      return Reducers[actions.editBooking](newState, booking);
    }
    return state;
  },

  ['createBooking.success']: (state, action) => {
    const {resp, ...data} = action.payload;
    if (resp && resp.id && data.status !== 'VOUCHER') {
      return Reducers[actions.editBooking](state, {
        ...data,
        identityId: resp.identityId,
        id: resp.id,
        token: resp.token
      });
    }
    return state;
  },

  ['cancelBooking.success']: (state, action) => {
    const id = action.payload.id;
    const booking = state.status.bookings.list.get(id);
    if (booking) {
      return {
        ...state,
        status: {
          ...state.status,
          bookings: {
            ...state.status.bookings,
            list: state.status.bookings.list.remove(id)
          }
        }
      };
    }
    return state;
  },

  [actions.editBooking]: (state, action) => {
    const newState = {
      ...state,
      status: {
        ...state.status,
        bookings: {
          ...state.status.bookings,
          list: state.status.bookings.list.extendAndSort(
            action.payload.id,
            action.payload,
            sortByStartDate
          )
        }
      }
    };
    return Reducers.updateUserContact(newState, action);
  },
  // ----------------------------------------- OPTIONS ---------------------------
  [actions.addDataToMessage]: (state, action) => {
    const {interactionId, id, data, event} = action.payload;
    const newState =
      event !== SocketEvents.submitContactData ? state : Reducers.updateUserContact(state, data);

    const conversation = newState.status.chat.conversations.get(interactionId);
    if (conversation) {
      const chatData = {messages: conversation.messages.extendDataField(id, data)};
      // only remove waitting if the data comes to the latest message
      const last = conversation.messages.getLast();
      if (last && last.id === id) {
        chatData.waiting = false;
      }
      return Reducers.setChatConversationStatus(newState, {interactionId, data: chatData});
    }
    return newState;
  },

  [actions.setActiveConversationStatus]: (state, action) => {
    const data = action.payload;
    const interactionId = get(state.status.chat.activeInteraction, 'id');
    if (interactionId) {
      return Reducers.setChatConversationStatus(state, {interactionId, data});
    }
    return state;
  }
};

const Reducers = wrapReducers(UnwrappedReducers);

export default UnwrappedReducers;

import {OrderedHash} from '@Common/utils/ordered-hash';

import Reducers from './reducers';
import actions from '../actions';

const state = {
  status: {
    chat: {
      activeInteraction: {},
      conversations: new OrderedHash()
    },
    rate: {},
    bookings: {
      list: new OrderedHash()
    },
    user: {
      identities: {
        34: {id: 34}
      }
    }
  }
};

describe('Testing reducers function', () => {
  it('set status', () => {
    expect(Reducers[actions.setStatus](state, {payload: {open: true}})).toEqual({
      ...state,
      status: {
        ...state.status,
        open: true
      }
    });
  });

  it('set chat status', () => {
    expect(Reducers[actions.setChatStatus](state, {payload: {activeInteraction: {id: 1}}})).toEqual(
      {
        ...state,
        status: {
          ...state.status,
          chat: {
            ...state.status.chat,
            activeInteraction: {id: 1}
          }
        }
      }
    );
  });

  it('set rate status', () => {
    expect(Reducers[actions.setRateStatus](state, {payload: {activeInteraction: {id: 1}}})).toEqual(
      {
        ...state,
        status: {
          ...state.status,
          rate: {
            ...state.status.rate,
            activeInteraction: {id: 1}
          }
        }
      }
    );
  });

  it('set setIsTitleNotificationEnabled status', () => {
    expect(Reducers[actions.setIsTitleNotificationEnabled](state, {payload: true})).toEqual({
      ...state,
      status: {
        ...state.status,
        isTitleNotificationEnabled: true
      }
    });
  });

  it('set chat message status with null message', () => {
    expect(
      Reducers[actions.setChatMessageStatus](state, {
        payload: {status: true, id: 1, interactionId: 10}
      })
    ).toEqual(state);
  });

  it('set chat message status with actual message', () => {
    const localState = {
      status: {
        chat: {
          conversations: new OrderedHash([
            {
              id: 1,
              messages: new OrderedHash([{id: 10}])
            }
          ])
        }
      }
    };

    expect(
      Reducers[actions.setChatMessageStatus](localState, {
        payload: {status: 'sent', id: 10, interactionId: 1}
      })
    ).toMatchObject({
      status: {
        chat: {
          conversations: {
            values: {
              1: {
                id: 1,
                messages: {
                  values: {
                    10: {
                      id: 10,
                      status: 'sent'
                    }
                  }
                }
              }
            }
          }
        }
      }
    });
  });

  it('addMessageToConversation from bot', () => {
    expect(
      Reducers[actions.addMessageToConversation](state, {
        payload: {
          status: true,
          message: {
            id: 1,
            message: 'test',
            owner: 'bot'
          },
          interactionId: 2,
          scriptStep: [1]
        }
      })
    ).toMatchObject({
      ...state,
      status: {
        ...state.status,
        chat: {
          ...state.status.chat,
          conversations: {
            values: {
              2: {
                messages: {
                  values: {
                    1: {message: 'test', id: 1, owner: 'bot'}
                  }
                },
                hasSentMessages: false,
                scriptStep: [1]
              }
            }
          }
        }
      }
    });
  });

  it('addMessageToConversation without status', () => {
    expect(
      Reducers[actions.addMessageToConversation](state, {
        payload: {
          message: {
            id: 1,
            message: 'test',
            owner: 'client'
          },
          interactionId: 2
        }
      })
    ).toMatchObject({
      ...state,
      status: {
        ...state.status,
        chat: {
          ...state.status.chat,
          conversations: {
            values: {
              2: {
                messages: {
                  values: {
                    1: {message: 'test', id: 1, owner: 'client'}
                  }
                },
                hasSentMessages: true,
                waiting: false,
                id: 2
              }
            }
          }
        }
      }
    });
  });

  it('addMessageToConversation from operator', () => {
    expect(
      Reducers[actions.addMessageToConversation](state, {
        payload: {
          message: {
            id: 1,
            message: 'test',
            owner: 14
          },
          interactionId: 2
        }
      })
    ).toMatchObject({
      ...state,
      status: {
        ...state.status,
        chat: {
          ...state.status.chat,
          conversations: {
            values: {
              2: {
                messages: {
                  values: {
                    1: {message: 'test', id: 1, owner: 14}
                  }
                },
                operator: 14,
                hasSentMessages: false,
                id: 2
              }
            }
          }
        }
      }
    });
  });

  it('setChatConversationStatus with', () => {
    expect(
      Reducers[actions.setChatConversationStatus](state, {
        payload: {
          data: {
            message: 'test'
          },
          interactionId: 2
        }
      })
    ).toMatchObject({
      ...state,
      status: {
        ...state.status,
        chat: {
          ...state.status.chat,
          conversations: {
            values: {
              2: {
                message: 'test'
              }
            }
          }
        }
      }
    });
  });

  it('setActiveConversationStatus with no active conversation', () => {
    expect(
      Reducers[actions.setActiveConversationStatus](state, {
        payload: {
          message: 'test'
        }
      })
    ).toMatchObject(state);
  });

  it('setActiveConversationStatus', () => {
    const localState = {
      ...state,
      status: {
        chat: {
          activeInteraction: {
            id: 2
          },
          conversations: new OrderedHash([{id: 2}])
        }
      }
    };

    expect(
      Reducers[actions.setActiveConversationStatus](localState, {
        payload: {
          message: 'test'
        }
      })
    ).toMatchObject({
      ...localState,
      status: {
        chat: {
          activeInteraction: {
            id: 2
          },
          conversations: {
            values: {
              2: {
                message: 'test'
              }
            }
          }
        }
      }
    });
  });

  it('rateSelected with no currentAction only if config steps', () => {
    expect(
      Reducers[actions.rateSelected](state, {
        payload: {
          value: 3
        }
      })
    ).toEqual({
      ...state,
      status: {
        ...state.status,
        rate: {
          value: 3
        }
      }
    });

    expect(
      Reducers[actions.rateSelected](state, {
        payload: {
          value: 3,
          config: {
            steps: ['chat']
          }
        }
      })
    ).toEqual({
      ...state,
      status: {
        ...state.status,
        currentAction: 'rate',
        currentStep: 0,
        rate: {
          value: 3
        }
      }
    });
  });

  it('rateSelected with currentAction !== chat only if config.steps', () => {
    const localState = {
      ...state,
      status: {
        ...state.status,
        currentAction: 'chat-invitation',
        currentStep: 0
      }
    };

    expect(
      Reducers[actions.rateSelected](localState, {
        payload: {
          value: 3
        }
      })
    ).toEqual({
      ...state,
      status: {
        ...state.status,
        currentAction: 'chat-invitation',
        currentStep: 0,
        rate: {
          value: 3
        }
      }
    });

    expect(
      Reducers[actions.rateSelected](localState, {
        payload: {
          value: 3,
          config: {
            steps: ['chat-invitation']
          }
        }
      })
    ).toEqual({
      ...state,
      status: {
        ...state.status,
        currentAction: 'chat-invitation',
        currentStep: 1,
        rate: {
          value: 3
        }
      }
    });
  });

  it('rateSelected with currentAction === chat', () => {
    const localState = {
      ...state,
      status: {
        ...state.status,
        currentAction: 'chat',
        currentStep: 1
      }
    };

    expect(
      Reducers[actions.rateSelected](localState, {
        payload: {
          value: 3
        }
      })
    ).toEqual({
      ...state,
      status: {
        ...state.status,
        currentAction: 'chat',
        currentStep: 1,
        rate: {
          value: 3
        }
      }
    });
  });
});

describe('updateConversation reducer', () => {
  it('parses data and message when they are json ', () => {
    expect(
      Reducers[actions.updateConversation](state, {
        payload: {
          data: {
            message: 'test',
            messages: [{message: '{"tero": "toro"}', data: '{"test": 678}', id: 34}]
          },
          interactionId: 2
        }
      })
    ).toMatchObject({
      ...state,
      status: {
        ...state.status,
        chat: {
          ...state.status.chat,
          conversations: {
            values: {
              2: {
                hasSentMessages: true,
                message: 'test',
                messages: {
                  values: {
                    34: {message: {tero: 'toro'}, data: {test: 678}, id: 34}
                  }
                }
              }
            }
          }
        }
      }
    });
  });

  it('flags hasSentMessages when a client message is in the list ', () => {
    expect(
      Reducers[actions.updateConversation](state, {
        payload: {
          data: {
            message: 'test',
            messages: [
              {owner: 'client', message: '{"tero": "toro"}', data: '{"test": 678}', id: 34}
            ]
          },
          interactionId: 2
        }
      })
    ).toMatchObject({
      ...state,
      status: {
        ...state.status,
        chat: {
          ...state.status.chat,
          conversations: {
            values: {
              2: {
                hasSentMessages: true,
                message: 'test',
                messages: {
                  values: {
                    34: {message: {tero: 'toro'}, data: {test: 678}, id: 34}
                  }
                }
              }
            }
          }
        }
      }
    });
  });

  it('returns strings if not valid json', () => {
    expect(
      Reducers[actions.updateConversation](state, {
        payload: {
          data: {
            message: 'test',
            messages: [{id: 23, message: '{maxi', data: null}]
          },
          interactionId: 2
        }
      })
    ).toMatchObject({
      ...state,
      status: {
        ...state.status,
        chat: {
          ...state.status.chat,
          conversations: {
            values: {
              2: {
                hasSentMessages: false,
                message: 'test',
                messages: {
                  values: {
                    23: {
                      message: '{maxi',
                      data: null,
                      id: 23
                    }
                  }
                }
              }
            }
          }
        }
      }
    });
  });

  it('hasSentMessage is true if there is a client message', () => {
    expect(
      Reducers[actions.updateConversation](state, {
        payload: {
          data: {
            message: 'test',
            messages: [{id: 23, message: '{maxi', data: '{"testdjcnjdns": 678', owner: 'client'}]
          },
          interactionId: 2
        }
      })
    ).toMatchObject({
      ...state,
      status: {
        ...state.status,
        chat: {
          ...state.status.chat,
          conversations: {
            values: {
              2: {
                hasSentMessages: true,
                message: 'test',
                messages: {
                  values: {
                    23: {
                      message: '{maxi',
                      data: '{"testdjcnjdns": 678',
                      id: 23
                    }
                  }
                }
              }
            }
          }
        }
      }
    });
  });
});

describe('actions.addDataToMessage reducer', () => {
  const localState = {
    status: {
      chat: {
        conversations: new OrderedHash([
          {id: 1, messages: new OrderedHash([{ui: 'options', id: 10}])}
        ])
      }
    }
  };

  const actual = Reducers[actions.addDataToMessage](localState, {
    payload: {
      interactionId: 1,
      id: 10,
      data: {selected: 'test'}
    }
  });

  const expected = {ui: 'options', id: 10, data: {selected: 'test'}};

  it('updates message in interaction/id with new data', () => {
    expect(actual.status.chat.conversations.get(1)).toMatchObject({waiting: false});
    expect(actual.status.chat.conversations.get(1).messages.get(10)).toEqual(expected);
  });
});

describe('loadBooking.success reducer', () => {
  const actual = Reducers['loadBooking.success'](state, {
    payload: {
      resp: {
        booking: {id: 38},
        identity: {1: {id: 1, name: 'test'}}
      }
    }
  });

  const expected = {
    ...state,
    status: {
      bookings: {
        list: {
          keys: [38],
          values: {
            38: {id: 38}
          }
        }
      },
      user: {
        identities: {
          34: {id: 34},
          1: {id: 1, name: 'test'}
        }
      }
    }
  };

  it('udpates the bookings and identities records', () => {
    expect(actual).toMatchObject(expected);
  });
});

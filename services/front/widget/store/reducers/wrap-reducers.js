export default function (unwrappedReducers) {
  return Object.keys(unwrappedReducers).reduce((prev, key) => {
    prev[key] = (state, action) => {
      const newAction = !action.hasOwnProperty('payload') ? {payload: {...action}} : action;

      return unwrappedReducers[key](state, newAction);
    };
    return prev;
  }, {});
}

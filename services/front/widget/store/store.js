import reducers from './reducers/reducers';
import actions from './actions';
import {debounceByKey} from '@Common/utils/debounce';

const reducer = (store, state, action = {}) => {
  if (action.type && reducers.hasOwnProperty(action.type)) {
    const newState = reducers[action.type](state, action);
    if (newState !== store.state) {
      store.state = newState;
      //console.log('------------------ FORCE UPDATE', action.type);
      // TODO: throttle these updates
      store.view.forceUpdate();
    }
  }

  //console.log('new action -> ', action, 'new state =>', store.state);
};

export default class Store {
  constructor(view, initialState) {
    this.state = initialState;
    this.view = view;
    this.reducer = reducer.bind(this);
    this.actions = Object.keys(actions).reduce((api, key) => {
      api[key] = payload => {
        this.reducer(this, this.state, {type: key, payload});
      };
      return api;
    }, {});
  }
}

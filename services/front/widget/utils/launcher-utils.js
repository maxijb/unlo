const resolveFunctionProps = [
  'triggerVerticalOffset',
  'triggerHorizontalOffset',
  'mobileTriggerVerticalOffset',
  'mobileTriggerHorizontalOffset'
];

const excludeOptionsFromWidget = [];

export const resolveOptions = props => {
  if (!props || !props.options) {
    return props;
  }

  const p = {
    ...props,
    options: {
      ...props.options
    }
  };

  resolveFunctionProps.forEach(prop => {
    if (typeof p.options[prop] === 'function') {
      p.options[prop] = p.options[prop]();
    }
  });

  excludeOptionsFromWidget.forEach(prop => {
    delete p.options[prop];
  });

  return p;
};

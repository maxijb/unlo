import {UITypes} from '@Common/constants/input';

export const getUI = it => {
  if (!it) {
    return null;
  }
  return it.ui || it;
};

export const getUIActionName = it => {
  if (!it) {
    return null;
  }
  return it.actionName || getUI(it);
};

export const getCurrentScreen = (screens, currentAction, currentStep = 0) => {
  let screen = screens[0];
  let steps = null;
  let showFooter = true;
  if (currentAction) {
    const path = screens[0].find(it => getUIActionName(it) === currentAction);
    if (path && path.steps) {
      steps = path.steps;
      screen = steps[currentStep] || steps[0];
    }
    //showFooter = false;
  } else if (currentStep && currentStep < screens.length) {
    screen = screens[currentStep];
  }
  // in case there's only one screen in the step we allow for it NOT to be an array
  if (!Array.isArray(screen)) {
    screen = [screen];
  }

  if (screen.length && getUI(screen[screen.length - 1]) === 'chat') {
    showFooter = false;
  }
  return {screen, steps, showFooter};
};

export const getModuleKey = module => {
  const ui = getUI(module);
  if (ui === UITypes.promotion) {
    return `${ui}: ${module.title}`;
  }
  return ui;
};

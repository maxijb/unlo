import {getCurrentScreen} from './screen-utils';

describe('Testing getCurrentScreen function', () => {
  const screens = [
    [
      {
        ui: 'rate',
        steps: [['rate'], ['rate', {ui: 'chat'}]]
      },
      'chat-invitation'
    ]
  ];

  it('Basic case with not input should return screen 0', () => {
    expect(getCurrentScreen(screens)).toEqual({screen: screens[0], steps: null, showFooter: true});
  });

  it('Basic case with not input with invalid screen number', () => {
    expect(getCurrentScreen(screens, null, 34)).toEqual({
      screen: screens[0],
      steps: null,
      showFooter: true
    });
  });

  it('Rate screen with no current goes to the first rate step', () => {
    expect(getCurrentScreen(screens, 'rate')).toEqual({
      screen: ['rate'],
      steps: [['rate'], ['rate', {ui: 'chat'}]],
      showFooter: true
    });
  });

  it('Rate screen in step 1', () => {
    expect(getCurrentScreen(screens, 'rate', 1)).toEqual({
      screen: ['rate', {ui: 'chat'}],
      steps: [['rate'], ['rate', {ui: 'chat'}]],
      showFooter: false
    });
  });
});

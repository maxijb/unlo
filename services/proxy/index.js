const proxy = require('express-http-proxy');
const wsProxy = require('http-proxy-middleware');
const app = require('express')();
const fetchConfig = require('zero-config');
const path = require('path');

const config = fetchConfig(path.join(__dirname, '../common'), {
  dcValue: 'dc',
  env: {NODE_ENV: 'development'},
  loose: false,
});

const serverConfig = config.get('server');
const socketConfig = config.get('sockets');

app.use(
  socketConfig.path,
  wsProxy({
    target: `http://localhost:${serverConfig.ports.backend}${socketConfig.path}`,
    ws: true,
  }),
);

app.use(
  ['/api/*', '/backend/*', '/sitemap.xml'],
  proxy(`localhost:${serverConfig.ports.backend}`, {
    proxyReqPathResolver: req => req.originalUrl,
    limit: '50mb',
  }),
);

app.use('/', proxy(`localhost:${serverConfig.ports.frontend}`));

app.listen(serverConfig.ports.proxy, err => {
  if (err) {
    throw err;
  }
  console.log(`> Ready on http://localhost:${serverConfig.ports.proxy}`);
});
